<?php
/**
 * Yab Framework
 *
 * @category   Yab
 * @package    Yab_Session
 * @author     Yann BELLUZZI
 * @copyright  (c) 2010 YBellu
 * @license    http://www.ybellu.com/yab-framework/license.html
 * @link       http://www.ybellu.com/yab-framework 
 */
 
class Yab_Session extends Yab_Object {

	const SESSION_YAB_KEY = 'yab';

	public function __construct() {

		try {
		
			if(!isset($_SESSION))
				session_start();

		} catch(Yab_Exception $e) {
		
			$_SESSION = array();
		
		}
		
		$_SESSION[self::SESSION_YAB_KEY] = 1;

		$this->bind($_SESSION);		

	}
	public function setFlashError($key,$error){
		if($key && !is_array($key) && $error && !is_array($error)){
			$errors = $this->has('errors')?$this->flash('errors'):array();
			$errors[$key][] = $error;
			$this->set('errors', $errors);
		}
	}
	public function setFlashErrors($new_errors,$key_errors = null){
		if(is_array($new_errors)){
			foreach($new_errors as $key => $value){
				$key = ($key_errors)?$key_errors:$key;
				if(is_array($value)) {
					$this->setFlashErrors($value,$key);
				}else {
					$errors = $this->has('errors')?$this->get('errors'):array();
					$errors[$key][] = $value;
					$this->set('errors', $errors);
				}
			}
		}
	}

}

// Do not clause PHP tags unless it is really necessary