<?php
/**
 * Yab Framework
 *
 * @category   Yab_Controller
 * @package    Yab_Controller_Action
 * @author     Yann BELLUZZI
 * @copyright  (c) 2010 YBellu
 * @license    http://www.ybellu.com/yab-framework/license.html
 * @link       http://www.ybellu.com/yab-framework 
 */
class Yab_Controller_Action {

	const ACTION_MODE_CREATE = "add";
	const ACTION_MODE_UPDATE = "edit";
	const ACTION_MODE_DELETE = "delete";
	const ACTION_MODE_SHOW = "show";

	const MODE_CREATE = "create";
	const MODE_UPDATE = "update";
	const MODE_DELETE = "delete";

	const LABEL_HISTO = "Historisation";

	// use Yab_Mvc;
	protected $_request = null;
	protected $_response = null;
	protected $_view = null;
	final public function __construct(Yab_Controller_Request $request, Yab_Controller_Response $response) {
		$this->_request = $request;
		$this->_response = $response;
		
		$this->_view = new Yab_View ();
		
		$this->_layout = Yab_Loader::getInstance ()->getLayout ()->setEnabled ( ! $request->isAjax () );
		
		try {
			
			$filter_lc = new Yab_Filter_LowerCase ( array (
					'separator' => '_' 
			) );
			
			$controller = $filter_lc->filter ( str_replace ( '_', DIRECTORY_SEPARATOR, $request->getController () ) );
			
			$action = $filter_lc->filter ( $request->getAction () );
			
			$this->_view->setFile ( 'View' . DIRECTORY_SEPARATOR . $controller . DIRECTORY_SEPARATOR . $action . '.html' );
		} catch ( Yab_Exception $e ) {
			
			// No default view
		}
		
		$this->_view->set ( 'layout', $this->_layout );
		$this->_view->set ( 'request', $this->_request );
		$this->_view->set ( 'response', $this->_response );
		$this->_view->set ( 'base_url', $this->_request->getBaseUrl () );
		
		$this->_init ();
	}
	protected function _init() {
	}
	public function preDispatch() {
	}
	public function preRender() {
	}
	public function render() {
		$this->_view->bufferize ();
		
		$this->_response->append ( ( string ) $this->_view );
		
		return $this;
	}
	public function postRender() {
	}
	public function renderLayout() {
		if (! $this->_layout->getEnabled () || ! $this->_layout->getFile ())
			return $this;
		
		$this->_layout->populate ( $this->_view->toArray () );
		$this->_layout->set ( 'view', ( string ) $this->_response );
		$this->_layout->bufferize ();
		
		$this->_response->clear ()->append ( ( string ) $this->_layout );
		
		return $this;
	}
	public function postDispatch() {
	}
	final public function getView() {
		return $this->_view;
	}
	
	// Proxy to loader
	final public function __call($method, array $args) {
		$loader = Yab_Loader::getInstance ();
		
		return $loader->invoke ( $loader, $method, $args );
	}
	
	public function actionSynchro() {
		$uri =  strtolower("/".$this->_request->getController()."/".$this->_request->getAction());
		require YAB_PATH . DIRECTORY_SEPARATOR."Slim".DIRECTORY_SEPARATOR."Slim.php";
		
		$app = new Slim/Slim();
		echo YAB_PATH ;

		die();
		$response = $app->response();
		$response['Content-Type'] = 'application/json';
		$app->get($uri, function() {
			$nomModel = "Model_".$this->_request->getController();
			$model = new $nomModel();
			$result = $model->fetchAll()->toArray();
			foreach ($result as $key => $value) {
				$result[$key] = $value->toArray();
			}
			$synchro = array();
			$synchro["id"] = null;
			$synchro["date_historique"] = date("Y-m-d");
			$synchro["categorie_object"] = strtolower($this->_request->getController());
			$synchro["liste"] = true;
			$synchro["data_modified"] = json_encode($result); 
			echo json_encode($synchro);
		});
		$app->post($uri, function() {
			$json = utf8_encode($app->request()->getBody());
			$tab = json_decode($json, true, 10);
			$nomModel = "Model_".$this->_request->getController();
			$model = new $nomModel();
			$model->populate($tab)->save();
			echo true;
		});
		$app->post($uri."/multi", function () {
			$json = utf8_encode($app->request()->getBody());
			$tab = json_decode($json, true, 10);
			$nomModel = "Model_".$this->_request->getController();
			$model = new $nomModel();
			foreach($tab as $tab2) {
				$model->populate($tab2)->save();
			}
			echo true;
		});
		$app->delete($uri, function () {
			
		});
		$app->delete($uri."/multi", function() {
					
		});
		
		//Delta
		$app->get($uri."/:date_historique", function($date_historique) {
			$model = new Model_Fsn_Historique();
			$result = $model->listHistoriqueDelta($date_historique, strtolower($this->_request->getController()))->toArray();
			foreach ($result as $key => $value) {
				$result[$key] = $value->toArray();
			}
			echo json_encode($result);
		});
		$app->run();
		die();
	}
}
