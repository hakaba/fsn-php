<?php
/**
 * Yab Framework
 *
 * @category   Yab_I18n_Adapter
 * @package    Yab_I18n_Adapter_Ini
 * @author     Yann BELLUZZI
 * @copyright  (c) 2010 YBellu
 * @license    http://www.ybellu.com/yab-framework/license.html
 * @link       http://www.ybellu.com/yab-framework 
 */

 class Yab_I18n_Adapter_Ini extends Yab_I18n_Adapter_Abstract {

	private $_files = null;

	private $_datas = array();

	 private $_repo = null;

	final public function setFile($files) {
		$file = trim((string) $files);

		if(!Yab_Loader::getInstance()->isFile($file))
			throw new Yab_Exception('"'.$file.'" is not a valid readable language file');

		$this->_files = $file;

		$this->_datas = parse_ini_file($this->_files, true);

		return $this;

	}

	 final public function setRepository($repo,$reset = true) {
	 	$repo = trim((string) $repo);
	 	if(!Yab_Loader::getInstance()->isDir($repo))
			throw new Yab_Exception('"'.$repo.'" is not a directory');

		 if($reset){
			 $this->_files = array();
			 $this->_repo = $repo;
		 }
		$content_repo = array_diff(scandir($repo),array(".",".."));
		 foreach($content_repo as $item){
			 try{
				 if(preg_match("/.*(.ini)$/",$item)) {
					 $file = $repo.DIRECTORY_SEPARATOR.$item;
					 $this->_files[] = $file;
					 $this->_datas = array_merge($this->_datas, parse_ini_file($file, true));
				 }
				 else
					 self::setRepository($repo.DIRECTORY_SEPARATOR.$item,false);
			 }catch(Yab_Exception $e){}
		 }

		return $this;
	 }

	 final public function getRepository() {
		 return $this->_repo;

	 }

	final public function getFile() {
		return $this->_files;

	}

	protected function _say($key) {
		$key = (string) $key;
	
		if(!array_key_exists($key, $this->_datas))
			throw new Yab_Exception('"'.$key.'" is not defined in the language file');
			
		$languages = $this->_datas[$key];
	
		if(!array_key_exists($this->_language, $languages))
			throw new Yab_Exception('"'.$this->_language.'" is not a defined language for this key in the language file');

		return $languages[$this->_language];
	
	}

}
 
// Do not clause PHP tags unless it is really necessary