<?php
/**
 * Costream
 *
 * @category   Extend Yab_Validator
 * @package    FSN
 * @author     Cyril GICQUEL
 * @copyright  (c) 2015 SOGETI
 * @date       21/05/2015 
 */

class Validator_NotPosterior extends Yab_Validator_Abstract {

	const NOT_EXISTS = 'Code Application IUA "$1" doesn\'t exists in database';
  //const NOT_UNIQUE = '"$1" already exists "$2" in database';

	public function _validate($value) {

		$db = Yab_Loader::getInstance()->getRegistry()->get('db');
    
    $value = trim($value);
    
    if ($value != '') {
			
      $statement = $db->prepare('
        SELECT
          app.code_application_mega
        FROM appli app
        LEFT JOIN department dep ON dep.id_department = app.id_department
        INNER JOIN type_application tap ON tap.id_type_appli = app.id_type_appli
        WHERE app.visible = 1
      ')->where('code_application_mega = "'.$value.'" ') ;
				
			if(!($statement instanceof Yab_Db_Statement))
				throw new Yab_Exception('statement must be an instance of Yab_Db_Statement');
			
			$statement->bind('?', $value);
      
			if(!count($statement))
			   $this->addError('NOT_EXISTS', self::NOT_EXISTS, $value);
			
	   }	
	}

}

// Do not clause PHP tags unless it is really necessary