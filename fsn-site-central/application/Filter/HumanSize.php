<?php

class Filter_HumanSize extends Yab_Filter_Abstract {

  public function _filter($value) {

	  $units = array('o', 'ko', 'Mo', 'Go', 'To', 'Po');

    $start = array_search($this->get('unit'), $units);

    for($i = $start; $value > 1024; $i++)
      $value /= 1024;

		return round($value, 2).' '.$units[$i];

  }

}