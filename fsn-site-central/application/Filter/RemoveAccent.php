<?php

class Filter_RemoveAccent extends Yab_Filter_Abstract {

  public function _filter($str, $charset='utf-8') {

    $str = htmlentities($str, ENT_NOQUOTES, $charset);
    
    $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
    $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
    $str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères
    $str = preg_replace(' ', '_', $str); // remplace les espace par des "_"
    
    return $str;

  }

}