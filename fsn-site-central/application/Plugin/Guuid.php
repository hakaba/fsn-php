<?php

class Plugin_Guuid extends Yab_Controller_Plugin_Abstract {


  public function preDispatch(Yab_Controller_Request $request, Yab_Controller_Response $response) { }
  public function preRender(Yab_Controller_Request $request, Yab_Controller_Response $response){ }
	public function postRender(Yab_Controller_Request $request, Yab_Controller_Response $response){}
	public function postDispatch(Yab_Controller_Request $request, Yab_Controller_Response $response){}
  
  
  public static function GetUUID()
  {
      
      // substr(sha1(Account::GetUsername() . Account::GetUserID()), 18, 8) . "-" .
  
      $guuid = substr(sha1(rand()), 18, 8) . "-" .
        substr(md5(time()), rand() % 28, 4) . "-" . 
        substr(md5(date("Y")), rand() % 28, 4) . "-" .
        substr(sha1(rand()), 20, 4) . "-" .
        substr(sha1(rand() % PHP_INT_MAX), 17, 12);
      
      return $guuid ;
  }

}
