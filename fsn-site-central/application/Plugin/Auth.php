<?php

class Plugin_Auth extends Yab_Controller_Plugin_Abstract {
	
	private $oAccount=null;

	public function setAccount($oAccount){
		$_SESSION['oAccount']=serialize($oAccount);
		$this->oAccount=$oAccount;
	}

	public function getAccount(){
		return $this->oAccount;
	}

	/**
	* methode appele a la connexion
	* @access public
	* @return bool retourne true/false selon que la personne est ou non authentifiee
	*/
	public function isConnected(){
		if(!$this->_isConnected()){
			return false;
		}

		$this->setAccount(unserialize($_SESSION['oAccount']));

		//ajouter critere supp pour verification de l'authentification
		return true;
	}
	/**
	* verifie si le couple login/pass est present dans le tableau
	* @access public
	* @param array $tElements tableau respectant la structure suivante: $array[login][pass]
	* @param string $sLogin login a verifier
	* @param string $sPass mot de passe a verifier
	* @return bool retourne true/false selong le couple login/mot de passe est correcte ou non
	*/
	public function checkLoginPass($tElements,$sLogin,$sPass){
		return $this->verifLoginPass($tElements,$sLogin,$sPass);
	}
	/**
	* verifie si le couple login/pass est present dans le tableau
	* @access public
	* @param array $tElements tableau respectant la structure suivante: $array[login][pass]
	* @param string $sLogin login a verifier
	* @param string $sPass mot de passe a verifier
	* @return bool retourne true/false selong le couple login/mot de passe est correcte ou non
	*/
	public function verifLoginPass($tElements,$sLogin,$sPass){
		if(isset($tElements[$sLogin][$sPass])){
			$this->_connect();
			$this->setAccount($tElements[$sLogin][$sPass]);
			
			return true;
		}
		return false;
	}
	
	/**
	* methode appele a la deconnexion
	* @access public
	*/
	public function logout(){
		$this->_disconnect();
		_root::redirect('auth::login');
	}
	
}
