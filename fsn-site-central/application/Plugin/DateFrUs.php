<?php

class Plugin_DateFrUs extends Yab_Controller_Plugin_Abstract {


  public function preDispatch(Yab_Controller_Request $request, Yab_Controller_Response $response) { }
  public function preRender(Yab_Controller_Request $request, Yab_Controller_Response $response){ }
	public function postRender(Yab_Controller_Request $request, Yab_Controller_Response $response){}
	public function postDispatch(Yab_Controller_Request $request, Yab_Controller_Response $response){}
  
  /*
   *V�rifie qu'une date au format JJ/MM/AA ou JJ/MM/AAA est valide
  *
  * @param	string	$date
  * @returnboolean $teste
  */
  public function estDateFrValide($date) {
  	if(empty($date))
  		return false;
  
  	$champsDate = explode("/", $date);
  	
  	if (count($champsDate) == 3)
  	{
  		$jour = $champsDate[0];
  		$mois = $champsDate[1];
  		$annee=$champsDate[2];
  	}
  	else 
  		return false;
  	
  	if(!is_numeric($jour) ||!is_numeric($mois) ||! is_numeric($annee))
  		return false;
  	
  	return checkdate($mois, $jour, $annee);
  }
  
  public function DateFormatVerifFR($date) {
  	if(empty($date)) {
  		return false;
  	}
  
  	$champsDate = explode("/", $date);
  
  	$jour 		= $champsDate[0];
  	$mois 		= $champsDate[1];
  	$annee		= $champsDate[2];
  
  	$date_verif	= $annee . $mois . $jour;
  	if (!is_numeric($date_verif))								// vérifie la nature du contenu, strictement numérique
  	{
  		return false;
  	}
  
  	return $date_verif;
  }
  
  public function DateFormatVerifUS($date) {
  	if(empty($date)) {
  		return false;
  	}
  
  	$champsDate = explode("-", $date);
  
  	$annee		= $champsDate[0];
  	$mois 		= $champsDate[1];
  	$jour 		= $champsDate[2];
  	
  	
  
  	$date_verif	= $annee . $mois . $jour;
  	if (!is_numeric($date_verif))								// vérifie la nature du contenu, strictement numérique
  	{
  		return false;
  	}
  
  	return $date_verif;
  }
  
  /*
   * ConvertitDateFR_US : Convertit une cha�ne contenant une date (et �ventuellement une heure) au format fran�ais en date au format US
  * 						Important pour l'enregistrement dans mySQL
  * 						ex: "10/01/2014 08:00:00" -> "2014-01-10 08:00:00"
  * 						ATTENTION : pas de v�rification de la validit� en tant que date de $dateFR
  * @param	string	$dateFR	: (OBLIGATOIRE) Date (et �ventuellement une heure) au format fran�ais
  * @return	string Date au format US
  */
  public function convertitDateFRUs($dateFR) {
  	if (empty($dateFR))
  	{
  		return $dateFR;
  	}
  
  	if (strpos($dateFR, "-"))
  	{
  		// La date est d�j� au format US
  		return $dateFR;
  	}
  
  	$sepDateHeure = explode(" ", $dateFR);
  	$dateUS = implode('-', array_reverse(explode('/', $sepDateHeure[0])));
  	if (count($sepDateHeure) > 1)
  	{
  		// on rajoute l'heure
  		$dateUS = $dateUS." ".$sepDateHeure[1];
  	}
  
  	return $dateUS;
  }
  
  /*
   * ConvertitDateFR_US : Convertit une cha�ne contenant une date (et �ventuellement une heure) au format US en date au format fran�ais
  * 						Important pour l'enregistrement dans mySQL
  * 						ex: "2014-01-10 08:00:00" -> "10/01/2014 08:00:00"
  * 						ATTENTION : pas de v�rification de la validit� en tant que date de $dateUS
  * @param	string	$dateUS	: (OBLIGATOIRE) Date (et �ventuellement une heure) au format US
  * @return	string Date au format fran�ais
  */
  public function convertitDateUsFr($dateUS)
  {
  
  	if (empty($dateUS))
  	{
  		return $dateUS;
  	}
  
  	if (strpos($dateUS, "/"))
  	{
  		// La date est d�j� au format fran�ais
  		return $dateUS;
  	}
  
  	$sepDateHeure = explode(" ", $dateUS);
  	$dateFR = implode('/', array_reverse(explode('-', $sepDateHeure[0])));
  	if (count($sepDateHeure) > 1)
  	{
  		// on rajoute l'heure
  		$dateFR = $dateFR." ".$sepDateHeure[1];
  	}
  
  	return $dateFR;
  }

}