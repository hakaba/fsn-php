<?php
class Plugin_Historique extends Yab_Controller_Plugin_Abstract {
	public function preDispatch(Yab_Controller_Request $request, Yab_Controller_Response $response) {
	}
	public function preRender(Yab_Controller_Request $request, Yab_Controller_Response $response) {
	}
	public function postRender(Yab_Controller_Request $request, Yab_Controller_Response $response) {
	}
	public function postDispatch(Yab_Controller_Request $request, Yab_Controller_Response $response) {
	}
	public function addhistory($module, $action, $formvalue) {
		if (! empty ( $module ) && ! empty ( $action ) && ! empty ( $formvalue )) {
			
			// Set de la date
			$date_historique = date ( 'Y-m-d H:i:s' );
			// Set du user (login qui modifie)
			$registry = $this->getRegistry ();
			$session = $this->getSession ();
			$user_info = $session->get ( 'session' );
			$login = $user_info ['login'];
			$user_id = $user_info ['id'];
			
			// identification des informations utilisateur (entiteadmin_id )
			$user = new Model_User ();
			$users = $user->fetchAll ()->where ( 'id = ' . $user_id )->toRow ();
			
			$entiteadmin_id = (! empty ( $user_info ['entiteadmin_id'] )) ? $user_info ['entiteadmin_id'] : $users ['entiteadmin_id'];
			$sitefouille_id = $session->has ( 'sitefouille_id' ) ? $session->get ( 'sitefouille_id' ) : null;
		
			// jfb 2016-04-26 fiche mantis 231 début : historiser les sites de fouille avec un sitefouille_id à null
			if ($module == 'Sitefouille') {
				$sitefouille_id = null;
			}
			// jfb 2016-04-26 fiche mantis 231 fin
			
			// Set des données modifées
			$data_modified = json_encode ( $formvalue, JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE );
			// Set du commentaire
			$description = null;
			
			$fsn_historique = new Model_Fsn_Historique ();
			$form_historique = new Form_Fsn_Historique ( $fsn_historique );
			
			$form_historique_response = array (
					'categorie_object' => strtolower ( $module ),
					'type_action' => $action,
					'date_historique' => $date_historique,
					'login' => $login,
					'data_modified' => $data_modified,
					'description' => $description,
					'entiteadmin_id' => $entiteadmin_id,
					'sitefouille_id' => $sitefouille_id 
			);
			
			$fsn_historique->populate ( $form_historique_response )->save ();
			
			return true;
		} else {
			
			return false;
		}
	}
}
