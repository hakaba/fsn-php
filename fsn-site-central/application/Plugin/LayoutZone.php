<?php

class Plugin_LayoutZone extends Yab_Controller_Plugin_Abstract {

	public function preDispatch(Yab_Controller_Request $request, Yab_Controller_Response $response) {}
	
	public function postDispatch(Yab_Controller_Request $request, Yab_Controller_Response $response) {
		
		$loader = Yab_Loader::getInstance();

		$registry = $loader->getRegistry();
		
		$body = (String) $response;
		
		$zones = $registry->has('zones') ? $registry->get('zones', 'Array') : array();
		
		foreach($zones as $key => $content)
				$body = preg_replace('#<!\-\-\s*'.preg_quote($key, '#').'\s*\-\->#i', $content, $body);
		
		$body = preg_replace('#\<!\-\-\s*[a-z0-9_]+\s*\-\-\>#i', '', $body);
		
		$response->clear();
		
		$response->append($body);
		
	}
	
	public function postRender(Yab_Controller_Request $request, Yab_Controller_Response $response) {}
	public function preRender(Yab_Controller_Request $request, Yab_Controller_Response $response) {}

}