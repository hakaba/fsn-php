<?php
/**
 * Plugin GeoConversion - systeme expert 
 * Convertion de coordonn�e de g�olocalisation   
 * @category Plugin GeoConversion
 * @package Plugin_GeoConversion
 * @author Cyrille GICQUEL
 * @link http://archaero.com/elinik.htm
 *   
 */

class Plugin_GeoConversion extends Yab_Controller_Plugin_Abstract {
  
  public function preDispatch(Yab_Controller_Request $request, Yab_Controller_Response $response) { }
  public function preRender(Yab_Controller_Request $request, Yab_Controller_Response $response){ }
	public function postRender(Yab_Controller_Request $request, Yab_Controller_Response $response){}
	public function postDispatch(Yab_Controller_Request $request, Yab_Controller_Response $response){}
  
  /**
   * Convert : Conversion de coodonn�es(X,Y)
   * 
   * @param : $coordonees array(x,y), tableau avec les valeur X et Y 
   * @param : $type => format des coordonn��es en entr�e, par default Lambert I 
   * @param : $result => format des coordonn��es en sortie, par default Lambert I              
   */     
  public function Convert($coordonees, $type='RGF93', $render='WGS84' ){
    /**
     *      
     * Si y >6000000, pas de prob�me, 'Lambert-93/RGF93'
     * Si y <400000, on est en Lambert Zone X ; deviner le X d'apr�s indices (plut�t Lille ou Marseille ?)
     * Si 1000000< y <1500000, on est en Lambert I Carto (Nord France)
     * Si 1500000< y <2000000, on est en Lambert II &#233;tendu
     * Si 2000000< y <2400000, on est en Lambert II Carto (Centre France) ou Lambert II &#233;tendu
     * Si 2400000< y <3000000, on est en Lambert II &#233;tendu
     * Si 3000000< y <3400000, on est en Lambert III Carto (Sud France)
     * Si 4000000< y, on est en Lambert IV Carto (Corse) 
     *       
     */
     
    
      
  }
  
  private function WGS84_to_RGF93($latitude,$longitude){
  
    // r�cup�ration des coordonn�es
    //  $latitude = $_GET[latitude];
    //  $longitude = $_GET[longitude];    
  
    // d�finition des constantes
    $c = 11754255.426096; //constante de la projection
    $e = 0.0818191910428158; //premi�re exentricit� de l'ellipso�de
    $n = 0.725607765053267; //exposant de la projection
    $xs = 700000; //coordonn�es en projection du pole
    $ys = 12655612.049876; //coordonn�es en projection du pole
  
    // pr�-calculs
    $lat_rad= $latitude/180*PI(); //latitude en rad
    $lat_iso= atanh(sin($lat_rad))-$e*atanh($e*sin($lat_rad)); //latitude isom�trique

    //calcul
    $x = (($c*exp(-$n*($lat_iso)))*sin($n*($longitude-3)/180*PI())+$xs);
    $y = ($ys-($c*exp(-$n*($lat_iso)))*cos($n*($longitude-3)/180*PI()));
    
    return array(
      'lambert93' => array(
        'x' => $X,
        'y' => $Y
      ),
      'wgs84' => array(
        'lat' => $latitude,
        'long' => $longitude
      )
    );

  }
  
  private function RGF93_to_WGS84($X, $Y){
  
    // r�cup�ration des coordonn�es
    // $X = $_GET[X];
    // $Y = $_GET[Y];
    
    // d�finition des constantes
    $c = 11754255.426096; //constante de la projection
    $e = 0.0818191910428158; //premi�re exentricit� de l'ellipso�de
    $n = 0.725607765053267; //exposant de la projection
    $xs = 700000; //coordonn�es en projection du pole
    $ys = 12655612.049876; //coordonn�es en projection du pole
    
    
    // pr�-calcul
    $a = (log($c/(sqrt(pow(($X-$xs),2)+pow(($Y-$ys),2))))/$n) ;
    
    // calcul
    $longitude = ((atan(-($X-$xs)/($Y-$ys)))/$n+3/180*PI())/PI()*180 ;
    $latitude = asin(tanh((log($c/sqrt(pow(($X-$xs),2)+pow(($Y-$ys),2)))/$n)+$e*atanh($e*(tanh($a+$e*atanh($e*(tanh($a+$e*atanh($e*(tanh($a+$e*atanh($e*(tanh($a+$e*atanh($e*(tanh($a+$e*atanh($e*(tanh($a+$e*atanh($e*sin(1))))))))))))))))))))))/PI()*180 ;
      
    return array(
      'lambert93' => array(
        'x' => $X,
        'y' => $Y
      ),
      'wgs84' => array(
        'lat' => $latitude,
        'long' => $longitude
      )
    );
  
  }
  
  private function NTF_to_LAMBERT() {
  
  }
  
  private function LAMBERT_to_NTF() {
  
  }
  
  function lambert93ToWgs84($x, $y){
    
    $x = number_format($x, 10, '.', '');
    $y = number_format($y, 10, '.', '');
    
    $b6 = 6378137.0000;
    $b7 = 298.257222101;
    $b8 = 1/$b7;
    $b9 = 2*$b8-$b8*$b8;
    $b10 = sqrt($b9);
    $b13 = 3.000000000;
    $b14 = 700000.0000;
    $b15 = 12655612.0499;
    $b16 = 0.7256077650532670;
    $b17 = 11754255.426096;
    
    $delx = $x - $b14;
    $dely = $y - $b15;
    
    $gamma = atan( -($delx) / $dely );
    $r = sqrt(($delx*$delx)+($dely*$dely));
    $latiso = log($b17/$r)/$b16;
    
    $sinphiit0 = tanh($latiso+$b10*atanh($b10*sin(1)));
    $sinphiit1 = tanh($latiso+$b10*atanh($b10*$sinphiit0));
    $sinphiit2 = tanh($latiso+$b10*atanh($b10*$sinphiit1));
    $sinphiit3 = tanh($latiso+$b10*atanh($b10*$sinphiit2));
    $sinphiit4 = tanh($latiso+$b10*atanh($b10*$sinphiit3));
    $sinphiit5 = tanh($latiso+$b10*atanh($b10*$sinphiit4));
    $sinphiit6 = tanh($latiso+$b10*atanh($b10*$sinphiit5));
    
    $longrad = $gamma/$b16+$b13/180*pi();
    $latrad = asin($sinphiit6);
    
    $long = ($longrad/pi()*180);
    $lat = ($latrad/pi()*180);
    
    return array(
      'lambert93' => array(
        'x' => $x,
        'y' => $y
      ),
      'wgs84' => array(
        'lat' => $lat,
        'long' => $long
      )
    );
  }
  
}