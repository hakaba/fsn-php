<?php
/* This PHP class was written originally by Stefan de Konink, November 2009.
 * Permission hereby granted to use this class in any code modified or unmodified. 
 */

class Plugin_OpenStreetMap extends Yab_Controller_Plugin_Abstract {

  public function preDispatch(Yab_Controller_Request $request, Yab_Controller_Response $response) { }
  public function preRender(Yab_Controller_Request $request, Yab_Controller_Response $response){ }
	public function postRender(Yab_Controller_Request $request, Yab_Controller_Response $response){}
	public function postDispatch(Yab_Controller_Request $request, Yab_Controller_Response $response){}  

  private static $errno = 0;

	private function _curl($server,$request) {
  
    $proxy_host = 'proxyitx.fr.sogeti.com:8080';
    $proxy_ident = 'cgicquel:Ikkyo1984' ;
    $useragent = 'PHPOSM 0.1' ;
    
      
		$ch = curl_init(); 
		
    curl_setopt($ch, CURLOPT_URL ,$server.$request );
		curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxy_ident); 
    curl_setopt($ch, CURLOPT_PROXY, $proxy_host );
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_USERAGENT ,$useragent );
    
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
		
    if( ! $result = curl_exec($ch))
    {
        trigger_error(curl_error($ch));
    } 
    
    self::$errno = curl_getinfo($ch, CURLINFO_HTTP_CODE );
    
    return $result;
	}

	private function getObject($type, $id, $extra = null) {
		if (!is_integer($id) || $id < 1) {
			self::$errno = 500;
			return;
		}

		$url = $type.'/'.$id.($extra==null?'':'/'+$extra);
		return self::curl($url, null);
	}

	public function getNode($id) {
		return self::getObject('node', $id);
	}

	public function getWay($id, $full = false) {
		return self::getObject('way', $id, ($full?'full':''));
	}

	public function getRelation($id, $full = false) {
		return self::getObject('relation', $id, ($full?'full':''));
	}

  /**
   * Extract from Services_OpenStreetMap_Nominatim
   *
   * @category   Services
   * @package    Services_OpenStreetMap
   * @subpackage Services_OpenStreetMap_Nominatim
   * @author     Ken Guest <kguest@php.net>
   * @license    BSD http://www.opensource.org/licenses/bsd-license.php
   * @link       Nominatim.php
   * @link       http://wiki.openstreetmap.org/wiki/Nominatim
   */    
    
    /**
     * The server to connect to
     *
     * @var string
     */
    protected $server = 'http://nominatim.openstreetmap.org/';

    /**
     * Format to perform queries in (xml|json|html). Defaults to 'xml'
     *
     * @var string
     */
    protected $format = 'json';

    /**
     * If 1, include a breakdown of the address into elements.
     *
     * @var int
     */
    protected $addresssdetails = 0;

    /**
     * Preferred language order. Standard rfc2616 string or a simple comma
     * separated list of language codes.
     *
     * @var string
     */
    protected $accept_language = 'fr';

    /**
     * Email address to be sent as a part of the query string, recommended to
     * be set if sending large numbers of requests/searches.
     *
     * @var string
     */
    protected $email_address = null;

    /**
     * Output polygon outlines for items found.
     *
     * @var null|boolean
     */
    protected $polygon = null;

    /**
     * The preferred area to find search results
     * <left>,<top>,<right>,<bottom>
     *
     * @var null|string
     */
    protected $viewbox = null;

    /**
     * If true, restrict results to those within the bounding box/view box.
     *
     * @var null|boolean
     */
    protected $bounded = null;

    /**
     * Remove duplicates?
     *
     * @var null|boolean
     */
    protected $dedupe = null;

    /**
     * Maximum number of entries to retrieve.
     *
     * @var int
     */
    protected $limit = null;

    /**
     * CSVs of valid country codes to restrict search to.
     *
     * @var string
     */
    protected $countryCodes = null;

    /**
     * The transport to use
     *
     * @var Services_OpenStreetMap_Transport
     */
    protected $transport = null;

    /**
     * Build query portion for request.
     *
     * @param string $place Name of location/place to search for
     *
     * @return string
     */
    private function _buildQuery($place)
    {
        $format = $this->format;
        $limit = $this->limit;
        $accept_language = $this->accept_language;
        $polygon = $this->polygon;
        $viewbox = $this->viewbox;
        $bounded = $this->bounded;
        $dedupe = $this->dedupe;

        $q = $place;

        $params = compact(
            'q',
            'format',
            'limit',
            'polygon',
            'viewbox',
            'bounded',
            'dedupe'
        );
        $params['accept-language'] = $accept_language;
        if ($this->email_address !== null) {
            $params['email'] = $this->email_address;
        }
        if ($this->countryCodes !== null) {
            $params['countrycodes'] = $this->countryCodes;
        }
        $query = http_build_query($params);
        
        return $query;
    }

    /**
     * Reverse geocode a lat/lon pair.
     *
     * Perform a reverse search/geoencoding.
     *
     * @param string $lat            Latitude
     * @param string $lon            Longitude
     * @param bool   $addressdetails Include address details, defaults to true.
     * @param int    $zoom           Zoom level, defaults to 18.
     *
     * @return void
     *
     * @see setAcceptLanguage
     * @see setFormat
     */
    public function reverseGeocode($lat, $lon, $addressdetails = 1, $zoom = 18)
    {

        $format = $this->format;
        if ($format == 'html') {
            echo 'html format not accepted for reverseGeocode' ;
        }
        $params = compact('format', 'lat', 'lon', 'addressdetails');
        $params['accept-language'] = $this->accept_language;
        if ($this->email_address !== null) {
            $params['email'] = $this->email_address;
        }
        $query = http_build_query($params);
        $url = $this->server . 'reverse?' . $query;

        $reversegeocode = null;
        $response = $this->getOsmTransport()->getOsmResponse($url);
        if ($format == 'xml') {
            $xml = simplexml_load_string($response->getBody());
            $reversegeocode = $xml->xpath('//reversegeocode');
        } elseif ($format == 'json' || $format == 'jsonv2') {
            $reversegeocode = json_decode($response->getBody());
        }
        return $reversegeocode;
    }

    /**
     * Search
     *
     * @param string  $place Name of place to geocode
     * @param integer $limit Maximum number of results to retrieve (optional)
     *
     * @return void
     */
    public function searchOsm($place, $format='json', $limit = null)
    {
        if ($limit !== null) {
            $this->setOsmLimit($limit);
        }

        $format = !empty($format) ? $format : $this->format;
        
        $query = $this->_buildQuery($place);
        $server = $this->server ;
        $request = 'search?' . $query;
        $url = $server.$request ;

        $response = $this->_curl($server,$request);

        /*
        if ($format == 'xml') {
            $xml = simplexml_load_string($response);
            $places = $xml->xpath('//place');
            return $places;
        } elseif ($format == 'json' || $format == 'jsonv2') {
            $places = json_decode($response);
            return $places;
        } elseif ($format == 'html') {
            return $response;
        }
        */
        return $response;        
    }

    /**
     * Set format for data to be received in.
     *
     * Format may be one of: html, json, jsonv2, xml
     *
     * @param string $format Format for data.
     *
     * @return Services_OpenStreetMap_Nominatim
     */
    public function setOsmFormat($format)
    {
        switch($format) {
        case 'html':
        case 'json':
        case 'jsonv2':
        case 'xml':
            $this->format = $format;
            break;
        default:
                sprintf('Unrecognised format (%s)', $format) ;            
        }
        return $this;
    }

    /**
     * Get which format is set for this instance (xml, json, html)
     *
     * @return string
     */
    public function getOsmFormat()
    {
        return $this->format;
    }

    /**
     * Set limit of entries to retrieve.
     *
     * @param integer $limit Maximum number of entries to retrieve
     *
     * @return Services_OpenStreetMap_Nominatim
     */
    public function setOsmLimit($limit)
    {
        if (is_numeric($limit)) {
            $this->limit = $limit;
        } else {
          die ('Limit must be a numeric value') ;            
        }
        return $this;
    }

    /**
     * Get Limit
     *
     * @return integer
     */
    public function getOsmLimit()
    {
        return $this->limit;
    }

    /**
     * Set Transport object.
     *
     * @param Services_OpenStreetMap_Transport $transport transport object
     *
     * @return Services_OpenStreetMap_Nominatim
     */
    public function setOsmTransport($transport)
    {
        $this->transport = $transport;
        return $this;
    }

    /**
     * Get current Transport object.
     *
     * @return Services_OpenStreetMap_Transport
     */
    public function getOsmTransport()
    {
        return $this->transport;
    }

    /**
     * Set which server to connect to.
     *
     * Possible values are 'nominatim', 'mapquest' and any other valid
     * endpoint specified as an URL.
     *
     * @param string $server Server URL or shorthand (nominatim / mapquest)
     *
     * @return Services_OpenStreetMap_Nominatim
     */
    public function setOsmServer($server)
    {
        switch($server) {
        case 'nominatim':
            $this->server = 'http://nominatim.openstreetmap.org/';
            return $this;
            break;
        case 'mapquest':
            $this->server = 'http://open.mapquestapi.com/nominatim/v1/';
            return $this;
            break;
        default:
            $parsed = parse_url($server);
            if (isset($parsed['scheme'])
                && isset($parsed['host'])
                && isset($parsed['path'])
            ) {
                $this->server = $server;
            } else {
                die(
                    'Server endpoint invalid'
                );
            }
            return $this;
        }
    }

    /**
     * Set referred language order for showing search results.
     *
     * This overrides the browser value.
     * Either uses standard rfc2616 accept-language string or a simple comma
     * separated list of language codes.
     *
     * @param string $language language code
     *
     * @return Services_OpenStreetMap_Nominatim
     */
    public function setOsmAcceptLanguage($language)
    {
        $this->accept_language = $language;
        return $this;
    }

    /**
     * Retrieve server endpoint.
     *
     * @return string
     */
    public function getOsmServer()
    {
        return $this->server;
    }

    /**
     * Set email address.
     *
     * @param string $email Valid email address
     *
     * @return Services_OpenStreetMap_Nominatim
     * @throws Services_OpenStreetMap_RuntimeException If email address invalid
     */
    public function setOsmEmailAddress($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            throw new Services_OpenStreetMap_RuntimeException(
                sprintf("Email address '%s' is not valid", $email)
            );
        }
        $this->email_address = $email;
        return $this;
    }

    /**
     * Set country codes to limit search results to.
     *
     * @param string $codes CSV list of country codes.
     *
     * @return Services_OpenStreetMap_Nominatim
     */
    public function setOsmCountryCodes($codes)
    {
        if ($codes == '') {
            $this->countryCodes = null;
        } else {
            $this->countryCodes = $codes;
        }
        return $this;
    }

    /**
     * Retrieve set email address.
     *
     * From OSM documentation:
     * If you are making large numbers of request please include a valid
     * email address or alternatively include your email address as
     * part of the User-Agent string.  This information will be kept
     * confidential and only used to contact you in the event of a
     * problem, see Usage Policy for more details.
     *
     * @return string|null
     */
    public function getOsmEmailAddress()
    {
        return $this->email_address;
    }

    /**#@+
     * @link http://tools.ietf.org/html/rfc2616
     * @access public
     */
    /**
     * Ok
     */
    const OK = 200;
    /**
     * Unauthorised, e.g. login credentials wrong.
     */
    const UNAUTHORISED = 401;
    /**
     * Resource not found.
     */
    const NOT_FOUND = 404;
    /**
     * Resource no longer available.
     */
    const GONE = 410;
    /**#@-*/

    
  /**
   * Decodes a polyline that was encoded using the Google Maps method.
   *
   * The encoding algorithm is detailed here:
   * http://code.google.com/apis/maps/documentation/polylinealgorithm.html
   *
   * This function is based off of Mark McClure's JavaScript polyline decoder
   * (http://facstaff.unca.edu/mcmcclur/GoogleMaps/EncodePolyline/decode.js)
   * which was in turn based off Google's own implementation.
   *
   * This function assumes a validly encoded polyline.  The behaviour of this
   * function is not specified when an invalid expression is supplied.
   *
   * @param String $encoded the encoded polyline.
   * @return Array an Nx2 array with the first element of each entry containing
   *  the latitude and the second containing the longitude of the
   *  corresponding point.
   */
  function decodePolylineToArray($encoded)
  {
    $length = strlen($encoded);
    $index = 0;
    $points = array();
    $lat = 0;
    $lng = 0;
  
    while ($index < $length)
    {
      // Temporary variable to hold each ASCII byte.
      $b = 0;
  
      // The encoded polyline consists of a latitude value followed by a
      // longitude value.  They should always come in pairs.  Read the
      // latitude value first.
      $shift = 0;
      $result = 0;
      do
      {
        // The `ord(substr($encoded, $index++))` statement returns the ASCII
        //  code for the character at $index.  Subtract 63 to get the original
        // value. (63 was added to ensure proper ASCII characters are displayed
        // in the encoded polyline string, which is `human` readable)
        $b = ord(substr($encoded, $index++)) - 63;
  
        // AND the bits of the byte with 0x1f to get the original 5-bit `chunk.
        // Then left shift the bits by the required amount, which increases
        // by 5 bits each time.
        // OR the value into $results, which sums up the individual 5-bit chunks
        // into the original value.  Since the 5-bit chunks were reversed in
        // order during encoding, reading them in this way ensures proper
        // summation.
        $result |= ($b & 0x1f) << $shift;
        $shift += 5;
      }
      // Continue while the read byte is >= 0x20 since the last `chunk`
      // was not OR'd with 0x20 during the conversion process. (Signals the end)
      while ($b >= 0x20);
  
      // Check if negative, and convert. (All negative values have the last bit
      // set)
      $dlat = (($result & 1) ? ~($result >> 1) : ($result >> 1));
  
      // Compute actual latitude since value is offset from previous value.
      $lat += $dlat;
  
      // The next values will correspond to the longitude for this point.
      $shift = 0;
      $result = 0;
      do
      {
        $b = ord(substr($encoded, $index++)) - 63;
        $result |= ($b & 0x1f) << $shift;
        $shift += 5;
      }
      while ($b >= 0x20);
  
      $dlng = (($result & 1) ? ~($result >> 1) : ($result >> 1));
      $lng += $dlng;
  
      // The actual latitude and longitude values were multiplied by
      // 1e5 before encoding so that they could be converted to a 32-bit
      // integer representation. (With a decimal accuracy of 5 places)
      // Convert back to original values.
      $points[] = array($lat * 1e-5, $lng * 1e-5);
    }
  
    return $points;
  }

	public function getLat() {
		return $this->lat;
	}

	public function setLat($lat) {
		if (-90.0 <= $lat && $lat <= 90.0) {
			$this->lat = $lat;
		} else {
			die('latitude value outside of acceptable range (-90.0 <= value <= 90.0)');
		}
	}

	public function getLon() {
		return $this->lon;
	}

	public function setLon($lon) {
		if (-180.0 <= $lon && $lon < 180.0) {
			$this->lon = $lon;
		} else {
			die('longitude value outside of acceptable range (-180.0 <= value < 180.0)');
		}
	}  

}

?>
