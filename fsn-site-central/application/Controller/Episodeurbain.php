<?php

class Controller_Episodeurbain extends Yab_Controller_Action {

	public function actionIndex() {

		$episodeurbain = new Model_Episodeurbain();

		$episodeurbains = $episodeurbain->fetchAll();

		$this->_view->set('episodeurbains', $episodeurbains);
	}

	public function actionAdd() {

		$episodeurbain = new Model_Episodeurbain();

		$form = new Form_Episodeurbain($episodeurbain);
    
    if($form->isSubmitted() && $form->isValid()) {

      // Ajout du GUUID 
      $generateGuuid = new Plugin_Guuid() ;
      $guuid = $generateGuuid->GetUUID() ;
      $form->setElement('id', array('type'=>'text', 'value' => $guuid ) ) ;
      $formvalue=$form->getValues();
      
      $episodeurbain->populate($formvalue)->save();
      
      // Historisation de la modif
      $id_episodeurbain = $episodeurbain->get('id');
      $formvalue['id'] = $id_episodeurbain ;
      $historisation = new Plugin_Historique() ;
      $historisation->addhistory('Episodeurbain',$mode,$formvalue) ;
      
			$this->getSession()->set('flash_title', ''); 
      $this->getSession()->set('flash_status', 'info'); 
      $this->getSession()->set('flash_message', 'episodeurbain as been added');

			$this->forward('Episodeurbain', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$episodeurbain = new Model_Episodeurbain($this->_request->getParams());

		$form = new Form_Episodeurbain($episodeurbain);
    
		if($form->isSubmitted() && $form->isValid()) {

      $formvalue=$form->getValues();
			$episodeurbain->populate($formvalue)->save();
            
      // Historisation de la modif
      $id_episodeurbain = $episodeurbain->get('id');
      $formvalue['id'] = $id_episodeurbain ;
      $historisation = new Plugin_Historique() ;
      $historisation->addhistory('Episodeurbain',$mode,$formvalue) ;
      
			$this->getSession()->set('flash_title', ''); 
      $this->getSession()->set('flash_status', 'info'); 
      $this->getSession()->set('flash_message', 'episodeurbain as been edited');

			$this->forward('Episodeurbain', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$episodeurbain = new Model_Episodeurbain($this->_request->getParams());

		$episodeurbain->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'episodeurbain as been deleted');

		$this->forward('Episodeurbain', 'index');

	}

}