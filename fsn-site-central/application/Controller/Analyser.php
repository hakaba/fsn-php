<?php

class Controller_Analyser extends Yab_Controller_Action {

	public function actionIndex() {

		$this->forward('Analyser_Sitefouille', 'index' );
    // $this->_view->setFile('View\Analyser\index.html');

	}

  /**
 * Neutralisation des actions "parasites"
 * redirection vers page d'accueil   
 */      
  public function actionNoAction() {
    
      $flash_message = 'flash_no_action' ;
      $flash_status = 'alert' ;
      $this->getSession()->set('flash_message', $flash_message);
      $this->getSession()->set('flash_status', $flash_status);
		
    	$this->forward('Index', 'index');
      
	}


}