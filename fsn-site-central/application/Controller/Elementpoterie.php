<?php

class Controller_Elementpoterie extends Yab_Controller_Action {

	public function actionIndex() {

		$elementpoterie = new Model_Elementpoterie();

		$elementpoteries = $elementpoterie->fetchAll();

		$this->_view->set('elementpoteries', $elementpoteries);
	}

	public function actionAdd() {

		$elementpoterie = new Model_Elementpoterie();

    $mode = 'add' ;
		$form = new Form_Elementpoterie($elementpoterie);
    $formvalue=$form->getValues();
    
		if($form->isSubmitted() && $form->isValid()) {

			$elementpoterie->populate($form->getValues())->save();

      // Historisation de la modif
      $id_elementpoterie = $elementpoterie->get('id');
      $formvalue['id'] = $id_elementpoterie ;
      $historisation = new Plugin_Historique() ;
      $historisation->addhistory('Elementpoterie',$mode,$formvalue) ;	
      
			$this->getSession()->set('flash_title', ''); 
      $this->getSession()->set('flash_status', 'info'); 
      $this->getSession()->set('flash_message', 'elementpoterie as been added');

			$this->forward('Elementpoterie', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$elementpoterie = new Model_Elementpoterie($this->_request->getParams());
    
    $mode = 'edit' ;
		$form = new Form_Elementpoterie($elementpoterie);
    $formvalue=$form->getValues();

		if($form->isSubmitted() && $form->isValid()) {

			$elementpoterie->populate($form->getValues())->save();
      
      // Historisation de la modif
      $id_elementpoterie = $elementpoterie->get('id');
      $formvalue['id'] = $id_elementpoterie ;
      $historisation = new Plugin_Historique() ;
      $historisation->addhistory('Elementpoterie',$mode,$formvalue) ;	
      
			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'elementpoterie as been edited');

			$this->forward('Elementpoterie', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$elementpoterie = new Model_Elementpoterie($this->_request->getParams());

		$elementpoterie->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'elementpoterie as been deleted');

		$this->forward('Elementpoterie', 'index');

	}

}