<?php

class Controller_Homogeneite extends Yab_Controller_Action {

	public function actionIndex() {

		$homogeneite = new Model_Homogeneite();

		$homogeneites = $homogeneite->fetchAll();

		$this->_view->set('homogeneites', $homogeneites);
	}

	public function actionAdd() {

		$homogeneite = new Model_Homogeneite();

		$form = new Form_Homogeneite($homogeneite);

		if($form->isSubmitted() && $form->isValid()) {

			$homogeneite->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'homogeneite as been added');

			$this->forward('Homogeneite', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$homogeneite = new Model_Homogeneite($this->_request->getParams());

		$form = new Form_Homogeneite($homogeneite);

		if($form->isSubmitted() && $form->isValid()) {

			$homogeneite->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'homogeneite as been edited');

			$this->forward('Homogeneite', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$homogeneite = new Model_Homogeneite($this->_request->getParams());

		$homogeneite->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'homogeneite as been deleted');

		$this->forward('Homogeneite', 'index');

	}

}