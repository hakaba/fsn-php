<?php

class Controller_Typetraitement extends Yab_Controller_Action {

	public function actionIndex() {

		$typetraitement = new Model_Typetraitement();

		$typetraitements = $typetraitement->fetchAll();

		$this->_view->set('typetraitements', $typetraitements);
	}

	public function actionAdd() {

		$typetraitement = new Model_Typetraitement();

		$form = new Form_Typetraitement($typetraitement);

		if($form->isSubmitted() && $form->isValid()) {

			$typetraitement->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'typetraitement as been added');

			$this->forward('Typetraitement', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$typetraitement = new Model_Typetraitement($this->_request->getParams());

		$form = new Form_Typetraitement($typetraitement);

		if($form->isSubmitted() && $form->isValid()) {

			$typetraitement->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'typetraitement as been edited');

			$this->forward('Typetraitement', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$typetraitement = new Model_Typetraitement($this->_request->getParams());

		$typetraitement->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'typetraitement as been deleted');

		$this->forward('Typetraitement', 'index');

	}

}