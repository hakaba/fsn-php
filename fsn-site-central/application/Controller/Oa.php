<?php

class Controller_Oa extends Yab_Controller_Action {

	public function actionIndex($layout=true) {

		$this->getLayout()->setEnabled($layout);
		$session = $this->getSession();
        $sitefouille_id = $session->has('sitefouille_id') ? $session->get('sitefouille_id') : null ;
        
		$oa = new Model_Oa();
        
		//$oas = !empty($sitefouille_id) ? $oa->getOaDetail()->where('sitefouille_id = "'.$sitefouille_id.'" ') : $oa->getOaDetail() ;
		$oas = $oa->getOaDetail() ; // le critère sur sitefouille_id est redondant avec celui du model Fai getOaDetail
		
		$this->_view->set('oas_index', $oas);
	}

	public function actionListSimple($layout=false, $sitefouille_id=null) {

		$this->getLayout()->setEnabled($layout);
			$oa = new Model_Oa();

			$oas = $oa->getOaDetail();

		if (!empty($sitefouille_id) ) {
		  $oas = $oas->where('sitefouille_id = "'.$sitefouille_id.'" ' ) ;
		}

		$this->_view->set('oas', $oas);
	}
  
    public function actionListSimpleEntiteAdmin($layout=false, $entiteadmin_id=null ) {
  
		$this->getLayout()->setEnabled($layout);
			$oa = new Model_Oa();

			$oas = $oa->fetchAll();

		if (!empty($entiteadmin_id) ) {
		  $oas = $oas->where('entiteadmin_id = '.$entiteadmin_id ) ;
		}
		$this->_view->set('oas', $oas);
	}

	public function actionAdd() {

		// appel fichier internationalisation
		$registry = Yab_Loader::getInstance() -> getRegistry();
		$i18n = $registry -> get('i18n');
		$filter_no_html = new Yab_Filter_NoHtml();
		
		$mode = self::ACTION_MODE_CREATE ;
		$errors_messages = '';

		$loader = Yab_Loader::getInstance();
		$session = $loader->getSession();
		$sitefouille_id = $session->get('sitefouille_id');
		if (empty($sitefouille_id)) {
			$this->getSession()->set('flash_status', 'warning');
			$this->getSession()->setFlashError('keysf', 'Veuillez sélectionner un site de fouille !');
			$this->forward('Oa', 'index');
		}
		
		$oa = new Model_Oa();
		$form = new Form_Oa($oa);
		$formvalue = $form->getValues();
		
		$loader = Yab_Loader::getInstance();
		$session = $loader->getSession();
		$courant_sitefouille_id = $session->get('sitefouille_id'); 
		
		//Relations
		$participation = new Model_Participation();
		$form_part = new Form_Participation($participation);
		
		$add_ant = new Model_Superposition();
		$form_add_ant = new Form_Superposition($add_ant);

		if($form->isSubmitted()) {

			// Ajout du GUUID
			$generateGuuid = new Plugin_Guuid() ;
			$guuid = $generateGuuid->GetUUID() ;

			try{
				
				//Recuperation entiteadmin_id dans variables de session
				$user_session= $this->getSession()->get('session') ;
				$entiteadmin_id= $user_session['entiteadmin_id'];
				$formvalue['entiteadmin_id']= $entiteadmin_id;
				$formvalue['id']= $guuid;
				$formvalue['sitefouille_id']= $courant_sitefouille_id;

				// Control champs requis
				function isNumber($idElt, &$formvalue, &$form, &$errors_messages, $message){
					$eltvalue = addslashes($formvalue[$idElt]);
					if(!empty($eltvalue)){
						if($form->getElement($idElt)->getErrors()) $errors_messages[$idElt] = $message;
					}
				}
				function isUnique($idElt, &$formvalue, &$oa, &$errors_messages, $message){
					$eltvalue = addslashes($formvalue[$idElt]);
					if(!empty($eltvalue)){	
						$req = $oa->fetchAll()->where(addslashes($idElt).' ="'.$eltvalue.'" ');
						// $req = $oa->fetchAll()->where(addslashes($idElt).' ="'.$eltvalue.'" and sitefouille_id ="'.$formvalue['sitefouille_id'].'" ');
						$nb_row = $req->count();
						if($nb_row > 0){
							$errors_messages[$idElt] = $message;
						}
					}
				}

				if($form->getElement('identification')->getErrors() || $form->getElement('debut')->getErrors() || $form->getElement('fin')->getErrors()){
					$errors_messages[] = $filter_no_html->filter( $i18n -> say('verror_requisAll') );
				}

				isNumber('surfacefouille', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('oa_requisSurfacefouille') ));
				isUnique('identification', $formvalue, $oa , $errors_messages, $filter_no_html->filter( $i18n -> say('oa_uniciteIdentification') ));
				
				//Controle date debut < fin

				$transformDateFrUs = new Plugin_DateFrUs();
				$dDebut = new DateTime($transformDateFrUs->convertitDateFRUs($formvalue['debut']));
				$dFin = new DateTime($transformDateFrUs->convertitDateFRUs($formvalue['fin']));				
				
				if($dDebut > $dFin){
					$errors_messages['debut'] = $filter_no_html->filter( $i18n -> say('oa_superieurDatation') );
				}
				
				//Trois param($donnees, $field, $fieldMessage, $errors_messages)
				Plugin_Fonctions::novirgule($formvalue['identification'], 'identification', 'novirguleinfield', $errors_messages);
				
				if(!empty($errors_messages)) throw new Exception();
			
				// Transformation des Dates
                if($formvalue['debut'])
                    $formvalue['debut'] = $transformDateFrUs->convertitDateFRUs($formvalue['debut']);
                else
                    $formvalue['debut'] = null;
                if($formvalue['fin'])
                    $formvalue['fin'] = $transformDateFrUs->convertitDateFRUs($formvalue['fin']);
                else
                    $formvalue['fin'] = null;

				// Insertion des données dans la BDD
				$oa->populate($formvalue)->save();

				// ajout des relations en bdd
				$historisation = new Plugin_Historique() ;
				$generateGuuid = new Plugin_Guuid();
				$title_relations = ["participation" => "_pparticipation_"];
				$regex_relations = array();
				foreach($title_relations as $table => $title){
					$regex_relations[$table] = "#^([a-z0-9_]+)(".$title.")([0-9]+)$#";
				}

				$relations = array();
				foreach($_POST as $key => $value){
					foreach($regex_relations as $table => $regex) {
						if (preg_match($regex, $key, $res)) {
							$relations[$table][$res[3]][$res[1]] = $value;
						}
					}
				}
				
				foreach($relations as $table => $relationsTable){
										
					foreach($relationsTable as $relation){
						$nomModel = "Model_Participation";			
						$nomForm = "Form_Participation";	
						$controller_courant = "Participation";
						
						$model = new $nomModel();
						$form_courant = new $nomForm($model);
						
						$relation['oa_id'] = $oa->get('id');
						$relation['id'] = $generateGuuid->GetUUID();
						$date_debut_save = $relation['debut']; // jfb 2016-04-26 mantis 239 : sauvegarde date début
						$date_fin_save = $relation['fin']; // jfb 2016-04-26 mantis 239 : sauvegarde date fin
						$relation['debut'] = $transformDateFrUs->convertitDateFRUs($relation['debut']);
						$relation['fin'] = $transformDateFrUs->convertitDateFRUs($relation['fin']);
						
						$model->populate($relation)->save();
						
						// Historisation des relations						
						unset($relation['id']);
                        foreach($relation as $key => $value)
                            $form_courant->getElement($key)->set('value',$value);
                        $relation = $form_courant->getTypedValues($relation);
                        $relation['id'] = $model->get('id');
                        $relation['debut'] = $date_debut_save; // jfb 2016-04-26 mantis 239 : restauration date début
                        $relation['fin'] = $date_fin_save; // jfb 2016-04-26 mantis 239 : restauration date fin
						$historisation->addhistory($controller_courant, self::MODE_CREATE, $relation); 
					}
				}
				
				// Historisation de la modif
				$id_oa = $oa->get('id');
				$formvalue['id'] = $id_oa ;
				
				$formvalue = $form->getTypedValues($formvalue);
				$historisation->addhistory('Oa', self::MODE_CREATE, $formvalue) ;

				$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); 	$this->getSession()->set('flash_message', 'oa ajoutée');

				$this->forward('Oa', 'index');
			}catch(Exception $e){
				$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'warning'); $this->getSession()->set('flash_message', '');
				$relation_error = Yab_Loader::getInstance()->getRegistry()->get('i18n')->say('warning_modif_not_saved');
				$this->getSession()->setFlashError("panel_relations", str_replace("__PART__", "interventions", $relation_error));
				$this->getSession()->setFlashErrors($errors_messages);
			}
		}
		
		// $this->_view->set('helper_form', new Yab_Helper_Form($form));
		$this->_view->set('helper_model_oa', $oa);
		$this->_view->set('helper_form', $form);
		$this->_view->set('form_add_ant', $form_add_ant);
		$this->_view->set('helper_form_part', $form_part);
		$this->_view->set('courant_sitefouille_id', $courant_sitefouille_id);

	}

	public function actionShow(){

		$mode = self::ACTION_MODE_SHOW ;

		$oa = new Model_Oa($this->_request->getParams());
		$form = new Form_Oa($oa);
		$formvalue= $form->getValues();
		
		$oa_id = $oa->get('id') ;

		$participation = new Model_Participation();
		$oa_participants = $participation->getListParticipation()->where('oa_id = "'.$oa_id.'" ')->toArray();
		
		function getFieldValueById(&$oa, $table, $tablecolum, $eltId, $valsearch, &$retour){
			$req = $oa->getTable($table)->fetchAll()->where(addslashes($tablecolum).'= "'.$oa->get($eltId).'" ');	
			$nb_row = $req->count();
			if($nb_row > 0){
				foreach($req as $row){
					$retour = $row[$valsearch];
				}
			} else $retour = '';
		}
		
		getFieldValueById($oa, 'Model_Oastatut', 'id', 'oastatut_id', 'oastatut', $oastatut_id_name);
		getFieldValueById($oa, 'Model_Nature', 'id', 'nature_id', 'nature', $nature_id_name);
		
		//$this->_view->set('helper_form', new Yab_Helper_Form($form));
		$this->_view->set('helper_form', $form);
		$this->_view->set('oastatut_id_name',$oastatut_id_name) ;
		$this->_view->set('nature_id_name',$nature_id_name) ;
		$this->_view->set('oa_id', $oa_id);
		$this->_view->set('oa_participants', $oa_participants);
	}
	
	public function actionEdit() {

		// appel fichier internationalisation
		$registry = Yab_Loader::getInstance() -> getRegistry();
		$i18n = $registry -> get('i18n');
		$filter_no_html = new Yab_Filter_NoHtml();

		$mode = self::ACTION_MODE_UPDATE ;
		$errors_messages = '';

		$oa = new Model_Oa($this->_request->getParams());
		$form = new Form_Oa($oa);
		$formvalue = $form->getValues();
		$id = $oa->get('id');

		$courant_sitefouille_id = $oa->get('sitefouille_id');
		
		//Relations
		$participation = new Model_Participation();
		$form_part = new Form_Participation($participation);
		
		$add_ant = new Model_Superposition();
		$form_add_ant = new Form_Superposition($add_ant);

		if($form->isSubmitted()) {
			try{
				
				$formvalue['sitefouille_id'] = $courant_sitefouille_id;
				
				// Control champs requis
				function isNumber($idElt, &$formvalue, &$form, &$errors_messages, $message){
					$eltvalue = addslashes($formvalue[$idElt]);
					if(!empty($eltvalue)){
						if($form->getElement($idElt)->getErrors()) $errors_messages[$idElt] = $message;
					}
				}
				function isUnique($idElt, &$formvalue, &$oa, &$id, &$errors_messages, $message){
					$eltvalue = addslashes($formvalue[$idElt]);
					if(!empty($eltvalue)){
						$req = $oa->fetchAll()->where(addslashes($idElt).' ="'.$eltvalue.'" and id !="'.$id.'" ');
						// $req = $oa->fetchAll()->where(addslashes($idElt).' ="'.$eltvalue.'" and id !="'.$id.'" and sitefouille_id ="'.$formvalue['sitefouille_id'].'" ');
						$nb_row = $req->count();
						if($nb_row > 0){
							$errors_messages[$idElt] = $message;
						}
					}
				}
				if($form->getElement('identification')->getErrors() || $form->getElement('debut')->getErrors() || $form->getElement('fin')->getErrors()){
					$errors_messages[] = $filter_no_html->filter( $i18n -> say('verror_requisAll') );
				}

				isNumber('surfacefouille', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('oa_requisSurfacefouille') ));
				isUnique('identification', $formvalue, $oa , $id , $errors_messages, $filter_no_html->filter( $i18n -> say('oa_uniciteIdentification') ));
				
                $transformDateFrUs = new Plugin_DateFrUs();
				//Controle date debut < fin
				$dDebut = new DateTime($transformDateFrUs->convertitDateFRUs($formvalue['debut']));
				$dFin = new DateTime($transformDateFrUs->convertitDateFRUs($formvalue['fin']));				
				
				if($dDebut > $dFin){
					$errors_messages['debut'] = $filter_no_html->filter( $i18n -> say('oa_superieurDatation') );
				}
				
				//Trois param($donnees, $field, $fieldMessage, $errors_messages)
				Plugin_Fonctions::novirgule($formvalue['identification'], 'identification', 'novirguleinfield', $errors_messages);
				
				if(!empty($errors_messages)) throw new Exception();
				
				// Transformation des Dates
                if($formvalue['debut'])
                    $formvalue['debut'] = $transformDateFrUs->convertitDateFRUs($formvalue['debut']);
                else
                    $formvalue['debut'] = null;
			
                if($formvalue['fin'])
                    $formvalue['fin'] = $transformDateFrUs->convertitDateFRUs($formvalue['fin']);
                else
                    $formvalue['fin'] = null;

				$oa->populate($formvalue)->save();
				
				// ajout des relations en bdd
				$historisation = new Plugin_Historique() ;
				$this->deleteRelationsOa($oa->get('id'));
				
				$generateGuuid = new Plugin_Guuid();
				$title_relations = ["participation" => "_pparticipation_"];
				$regex_relations = array();
				foreach($title_relations as $table => $title){
					$regex_relations[$table] = "#^([a-z0-9_]+)(".$title.")([0-9]+)$#";
				}

				$relations = array();
				foreach($_POST as $key => $value){
					foreach($regex_relations as $table => $regex) {
						if (preg_match($regex, $key, $res)) {
							$relations[$table][$res[3]][$res[1]] = $value;
						}
					}
				}
				
				foreach($relations as $table => $relationsTable){
										
					foreach($relationsTable as $relation){
						$nomModel = "Model_Participation";	
						$nomForm = "Form_Participation";	
						$controller_courant = "Participation";	
						
						$model = new $nomModel();					
						$form_courant = new $nomForm($model);
						
						$relation['oa_id'] = $oa->get('id');
						$relation['id'] = $generateGuuid->GetUUID();
						$date_debut_save = $relation['debut']; // jfb 2016-04-26 mantis 239 : sauvegarde date début
						$date_fin_save = $relation['fin']; // jfb 2016-04-26 mantis 239 : sauvegarde date fin
						$relation['debut'] = $transformDateFrUs->convertitDateFRUs($relation['debut']);
						$relation['fin'] = $transformDateFrUs->convertitDateFRUs($relation['fin']);
						
						$model->populate($relation)->save();
						
						// Historisation des relations						
						unset($relation['id']);
                        foreach($relation as $key => $value)
                            $form_courant->getElement($key)->set('value',$value);
                        $relation = $form_courant->getTypedValues($relation);
                        $relation['id'] = $model->get('id');
                        $relation['debut'] = $date_debut_save; // jfb 2016-04-26 mantis 239 : restauration date début
                        $relation['fin'] = $date_fin_save; // jfb 2016-04-26 mantis 239 : restauration date fin
                        $historisation->addhistory($controller_courant, self::MODE_CREATE, $relation) ;	
					}
				}
				
				// Historisation de la modif
				$id_oa = $oa->get('id');
				$formvalue['id'] = $id_oa ;
				
				$formvalue = $form->getTypedValues($formvalue);
				$historisation->addhistory('Oa', self::MODE_UPDATE, $formvalue) ;

				$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); 	$this->getSession()->set('flash_message', 'oa modifiée');

				$this->forward('Oa', 'index');
			}catch(Exception $e){
				$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'warning'); $this->getSession()->set('flash_message', '');
				$relation_error = Yab_Loader::getInstance()->getRegistry()->get('i18n')->say('warning_modif_not_saved');
				$this->getSession()->setFlashError("panel_relations", str_replace("__PART__", "interventions", $relation_error));
				$this->getSession()->setFlashErrors($errors_messages);
			}
		}
		// $this->_view->set('helper_form', new Yab_Helper_Form($form));
		$this->_view->set('helper_model_oa', $oa);
		$this->_view->set('helper_form', $form);
		$this->_view->set('form_add_ant', $form_add_ant);
		$this->_view->set('helper_form_part', $form_part);
		$this->_view->set('courant_sitefouille_id', $courant_sitefouille_id);
		
		$oa_id = $oa->get('id');
		$this->_view->set('oa_id', $oa_id);
	}

	private function deleteRelationsOa($oa_id){
        $histo = array();
        $historisation = new Plugin_Historique();
		
        $m_participation = new Model_Participation();
        // jfb 2016-04-29 mantis 250 début
        // $histo['Participation'] = $m_participation->getListParticipation()->where('oa_id = "'.$oa_id.'" ')->toArray();
        $histo['Participation'] = $m_participation->getListParticipationHisto()->where('oa_id = "'.$oa_id.'" ')->toArray();
        // jfb 2016-04-29 mantis 250 fin
        
        foreach($histo as $name => $relations){ 
			foreach($relations as $relation){
				$historisation->addhistory($name, self::MODE_DELETE, $relation->getAttributes());
            }
        }
  		
		$m_participation->deleteRelations($oa_id);      
    }
	
	public function actionDelete() {

		$oa = new Model_Oa($this->_request->getParams());
        $form = new Form_Oa($oa);
		$formvalue = $form->getValues();
		
		$formvalue['id'] = $oa->get('id');
		$formvalue['sitefouille_id'] = $oa->get('sitefouille_id');

// jfb 2016-04-18 correctif fiche mantis 99 début
		$message = ''; // init. à blanc message de deletion

		try {
        	$oa->delete(); // try delete avant historisation

			// historisation début
        	$historisation = new Plugin_Historique();		
			$formvalue = $form->getTypedValues($formvalue);
        	$historisation->addhistory('Oa', self::MODE_DELETE, $formvalue);
        	// historisation fin
        	     
        	$message='oa supprimée';
		} catch (Exception $e) {
			$message = 'Suppression oa impossible';
		}

		//$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'oa as been deleted');
		$this->getSession()->set('flash_title', '');
		$this->getSession()->set('flash_status', 'info');
		$this->getSession()->set('flash_message', $message);
// jfb 2016-04-18 correctif fiche mantis 99 fin
		
		$this->forward('Oa', 'index');

	}

}