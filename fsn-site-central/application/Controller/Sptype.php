<?php

class Controller_Sptype extends Yab_Controller_Action {

	public function actionIndex() {

		$sptype = new Model_Sptype();

		$sptypes = $sptype->fetchAll();

		$this->_view->set('sptypes', $sptypes);
	}

	public function actionAdd() {

		$sptype = new Model_Sptype();

		$form = new Form_Sptype($sptype);

		if($form->isSubmitted() && $form->isValid()) {

			$sptype->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'sptype as been added');

			$this->forward('Sptype', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$sptype = new Model_Sptype($this->_request->getParams());

		$form = new Form_Sptype($sptype);

		if($form->isSubmitted() && $form->isValid()) {

			$sptype->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'sptype as been edited');

			$this->forward('Sptype', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$sptype = new Model_Sptype($this->_request->getParams());

		$sptype->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'sptype as been deleted');

		$this->forward('Sptype', 'index');

	}

}