<?php

class Controller_Topographie extends Yab_Controller_Action {

	public function actionIndex() {

		$topographie = new Model_Topographie();

		$topographies = $topographie->fetchAll();

		$this->_view->set('topographies', $topographies);
	}

	public function actionAdd() {

		$topographie = new Model_Topographie();

		$form = new Form_Topographie($topographie);

		if($form->isSubmitted() && $form->isValid()) {

			$topographie->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'topographie as been added');

			$this->forward('Topographie', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$topographie = new Model_Topographie($this->_request->getParams());

		$form = new Form_Topographie($topographie);

		if($form->isSubmitted() && $form->isValid()) {

			$topographie->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'topographie as been edited');

			$this->forward('Topographie', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$topographie = new Model_Topographie($this->_request->getParams());

		$topographie->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'topographie as been deleted');

		$this->forward('Topographie', 'index');

	}

}