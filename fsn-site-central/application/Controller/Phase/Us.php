<?php

class Controller_Phase_Us extends Yab_Controller_Action {

	public function actionIndex() {

		$phase_us = new Model_Phase_Us();

		$phase_uses = $phase_us->fetchAll();

		$this->_view->set('phase_uses', $phase_uses);
	}

	public function actionAdd() {

		$phase_us = new Model_Phase_Us();

		$form = new Form_Phase_Us($phase_us);

		if($form->isSubmitted() && $form->isValid()) {

			$phase_us->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'phase_us as been added');

			$this->forward('Phase_Us', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$phase_us = new Model_Phase_Us($this->_request->getParams());

		$form = new Form_Phase_Us($phase_us);

		if($form->isSubmitted() && $form->isValid()) {

			$phase_us->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'phase_us as been edited');

			$this->forward('Phase_Us', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$phase_us = new Model_Phase_Us($this->_request->getParams());

		$phase_us->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'phase_us as been deleted');

		$this->forward('Phase_Us', 'index');

	}

}