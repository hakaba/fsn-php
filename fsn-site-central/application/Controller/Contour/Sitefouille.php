<?php

class Controller_Contour_Sitefouille extends Yab_Controller_Action {

	public function actionAjaxGetContours() {
		$this->getLayout()->setEnabled(false) ;
		$this->_view->setEnabled(false) ;
		$sitefouille = new Model_Sitefouille();
		
		$sf = (!empty($_POST['values'])) ? $_POST['values'] : null;
		//$contours = array();
		$contours['entity'] = array();
		$count = 0;
		if ($sf != null) {
			$sf_contours = new Model_Contour_Sitefouille();
			$values = $sf_contours->fetchAll()->where('sitefouille_id="'.$sf.'"')->orderBy('sequence');
			$sf_contours = $values->toArray();
			$current_sf = new Model_Sitefouille($sf);
			if (!empty($sf_contours)) {
				$contours['entity'][$count]['name'] = $current_sf['nomabrege'];
				$contours['entity'][$count]['id'] = $current_sf['id'];
				$contours['entity'][$count]['categorie'] = 'sf';
				$str = array();
				$val = 0;
				$poly = array();
				foreach ($sf_contours as $item) {
	
					//$contours[$current_us['identification']][$point][] = ;
					//$contours[$current_us['identification']][$point][] =
					//$contours['us'][$count]['points'][]  = '['.$item['x'] . ', ' . $item['y'].']';
					$contours['entity'][$count]['points'][$val]['x']  = $item['x'];
					$contours['entity'][$count]['points'][$val]['y']  =  $item['y'];
					$poly[] = array($item['x'],$item['y']);
					$str[] = '['.$item['x'] . ', ' . $item['y'].']';
					$val++;
				}
				//$contours['us'][$count]['points'][] = $this->setPoly($str);
				$contours['entity'][$count]['poly'] = $poly;
				$count++;
			}
		}
		echo json_encode($contours);
	}
	
	public function actionAjaxSetContours() {
		$this->getLayout()->setEnabled(false) ;
		$this->_view->setEnabled(false) ;
		$sitefouille = new Model_Sitefouille();
		$tab = (!empty($_POST['contour_modif'])) ? $_POST['contour_modif'] : null;
		if (!empty($tab)) {
			$id = $tab[count($tab)-1];
			unset($tab[count($tab)-1]);
			$sf = new Model_Sitefouille($id);
			//$id = $sf->get('id');
			if (!empty($id)) {
				$contour_fetch = new Model_Contour_Sitefouille();
				$contour_test = $contour_fetch->fetchAll()->where('sitefouille_id="'.$id.'"');
				$contour_test = $contour_test->toArray();
				if (empty($contour_test)) {
					$generateGuuid = new Plugin_Guuid() ;
	
					$info_contour = array();
					$info_contour['sitefouille_id'] = $id;
	
					foreach ($tab as $cle => $value ) {
						$contour = new Model_Contour_Sitefouille();
						$guuid = $generateGuuid->GetUUID() ;
						$info_contour['id'] = $guuid;
						$coor = explode('-', $value);
						$info_contour['x'] = trim($coor[0]);
						$info_contour['y'] = trim($coor[1]);
						$info_contour['sequence'] = $cle + 1;
							
						$contour->populate($info_contour)->save();
					}
					$result = array();
					$result[] = $sf->get('id');
					$result[] = "les contours du site de fouille enregistres avec succes";
					echo json_encode($result);
				} else {
					$result = array();
					$result[] = 'vide';
					$result[] = "Les contours du site existent déjà ";
					echo json_encode($result);
				}
	
			} else {
				$result = array();
				$result[] = 'vide';
				$result[] = 'probleme avec Identificant su site de fouille';
				echo json_encode($result);
	
			}
	
		}
	}
	
	public function actionAjaxEditContours() {
		$this->getLayout()->setEnabled(false) ;
		$this->_view->setEnabled(false) ;
		$sitefouille = new Model_Sitefouille();
		$tab = (!empty($_POST['contour_modif'])) ? $_POST['contour_modif'] : null;
		if (!empty($tab)) {
			$id = $tab[count($tab)-1];
			unset($tab[count($tab)-1]);
			
			$sf = new Model_Sitefouille($id);
			
			$contour_fetch = new Model_Contour_Sitefouille();
			$contour_test = $contour_fetch->fetchAll()->where('sitefouille_id="'.$sf->get('id').'"');
			$contour_test = $contour_test->toArray();
			
			if (!empty($contour_test)) {
				foreach ($contour_test as $item) {
					$element = new Model_Contour_Sitefouille($item['id']);
					$element->delete();
				}
			}
			
			$generateGuuid = new Plugin_Guuid() ;
	
			$info_contour = array();
			$info_contour['sitefouille_id'] = $sf->get('id');
	
			foreach ($tab as $cle => $value ) {
				$contour = new Model_Contour_Sitefouille();
				$guuid = $generateGuuid->GetUUID() ;
				$info_contour['id'] = $guuid;
				$coor = explode('-', $value);
				$info_contour['x'] = trim($coor[0]);
				$info_contour['y'] = trim($coor[1]);
				$info_contour['sequence'] = $cle + 1;
					
				$contour->populate($info_contour)->save();
			}
	
			echo "Modification du site de fouille avec succes";
		} else {
			echo 'une erreur est survenue';
		}
			
	
	}
	
	public function actionAjaxDeleteContours() {
		$this->getLayout()->setEnabled(false) ;
		$this->_view->setEnabled(false) ;
	
		$sitefouille = new Model_Sitefouille();
		$id = (!empty($_POST['id'])) ? $_POST['id'] : null;
		if (!empty($id)) {
			$sf = new Model_Sitefouille($id);
	
			$contour_fetch = new Model_Contour_Sitefouille();
			$contour_test = $contour_fetch->fetchAll()->where('sitefouille_id="'.$sf->get('id').'"');
			$contour_test = $contour_test->toArray();
			if (!empty($contour_test)) {
				foreach ($contour_test as $item) {
					$element = new Model_Contour_Sitefouille($item['id']);
					$element->delete();
				}
			}
	
			echo "les contours du site de fouille supprimes avec succes";
		} else {
			echo 'une erreur est survenue';
		}
	}
	
	
	public function actionIndex() {

		$contour_sitefouille = new Model_Contour_Sitefouille();

		$contour_sitefouilles = $contour_sitefouille->fetchAll();

		$this->_view->set('contour_sitefouilles', $contour_sitefouilles);
	}

	public function actionAdd() {

		$contour_sitefouille = new Model_Contour_Sitefouille();

		$form = new Form_Contour_Sitefouille($contour_sitefouille);

		if($form->isSubmitted() && $form->isValid()) {

			$contour_sitefouille->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'contour_sitefouille as been added');

			$this->forward('Contour_Sitefouille', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$contour_sitefouille = new Model_Contour_Sitefouille($this->_request->getParams());

		$form = new Form_Contour_Sitefouille($contour_sitefouille);

		if($form->isSubmitted() && $form->isValid()) {

			$contour_sitefouille->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'contour_sitefouille as been edited');

			$this->forward('Contour_Sitefouille', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$contour_sitefouille = new Model_Contour_Sitefouille($this->_request->getParams());

		$contour_sitefouille->delete();

		$this->getSession()->set('flash', 'contour_sitefouille as been deleted');

		$this->forward('Contour_Sitefouille', 'index');

	}

}