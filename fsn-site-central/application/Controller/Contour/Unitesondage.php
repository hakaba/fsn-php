<?php

class Controller_Contour_Unitesondage extends Yab_Controller_Action {
	public function actionAjaxGetContours() {
		$this->getLayout()->setEnabled(false) ;
		$this->_view->setEnabled(false) ;
		$sitefouille = new Model_Sitefouille();
		$unitesondage = (!empty($_POST['values'])) ? $_POST['values'] : null;
		//$contours = array();
		$contours['us'] = array();
		$count = 0;
		foreach ($us as $element){
			$unitesondage_contours = new Model_Contour_Unitesondage();
			$unitesondage_contours = $unitesondage_contours->fetchAll()->where('unitesondage_id="'.$element.'"')->orderBy('num_sequence');
			$unitesondage_contours = $unitesondage_contours->toArray();
			$current_us = new Model_Unitesondage($element);
			if (!empty($unitesondage_contours)) {
				$contours['entity'][$count]['name'] = $current_us['identification'];
				$contours['entity'][$count]['id'] = $current_us['id'];
				$contours['entity'][$count]['categorie'] = 'unitesondage';
				$str = array();
				$val = 0;
				$poly = array();
				foreach ($unitesondage_contours as $item) {

					//$contours[$current_us['identification']][$point][] = ;
					//$contours[$current_us['identification']][$point][] =
					//$contours['us'][$count]['points'][]  = '['.$item['x'] . ', ' . $item['y'].']';
					$contours['entity'][$count]['points'][$val]['x']  = $item['x'];
					$contours['entity'][$count]['points'][$val]['y']  =  $item['y'];
					$poly[] = array($item['x'],$item['y']);
					$str[] = '['.$item['x'] . ', ' . $item['y'].']';
					$val++;
				}
				//$contours['us'][$count]['points'][] = $this->setPoly($str);
				$contours['entity'][$count]['poly'] = $poly;
				$count++;
			}
		}
		echo json_encode($contours);
	}

	public function actionAjaxSetContours() {
		$this->getLayout()->setEnabled(false) ;
		$this->_view->setEnabled(false) ;
		$sitefouille = new Model_Sitefouille();
		$tab = (!empty($_POST['contour_modif'])) ? $_POST['contour_modif'] : null;
		if (!empty($tab)) {
			$identification = $tab[count($tab)-1];
				
			unset($tab[count($tab)-1]);
			$us_fetch = new Model_Us();
			$us = $us_fetch->fetchAll()->where('identification="'.$identification.'"');
			$us = $us->toArray();
			if (!empty($us)) {
				$us_id = $us[0]['id'];

				$contour_fetch = new Model_Contour_Us();
				$contour_test = $contour_fetch->fetchAll()->where('us_id="'.$us_id.'"');
				$contour_test = $contour_test->toArray();
				if (empty($contour_test)) {
					$generateGuuid = new Plugin_Guuid() ;

					$info_contour = array();
					$info_contour['us_id'] = $us_id;

					foreach ($tab as $cle => $value ) {
						$contour = new Model_Contour_Us();
						$guuid = $generateGuuid->GetUUID() ;
						$info_contour['id'] = $guuid;
						$coor = explode('-', $value);
						$info_contour['x'] = trim($coor[0]);
						$info_contour['y'] = trim($coor[1]);
						$info_contour['sequence'] = $cle + 1;
							
						$contour->populate($info_contour)->save();
					}
					$result = array();
					$result[] = $us_id;
					$result[] = "les contours de l'us ".$identification." enregistres avec succes";
					echo json_encode($result);
				} else {
					$result = array();
					$result[] = 'vide';
					$result[] = "les contours de l'us ".$identification." existent déjà ";
					echo json_encode($result);
				}

			} else {
				$result = array();
				$result[] = 'vide';
				$result[] = 'cet Identificant est incorrect';
				echo json_encode($result);

			}

		}
	}

	public function actionAjaxEditContours() {
		$this->getLayout()->setEnabled(false);
		$this->_view->setEnabled(false);
		$sitefouille = new Model_Sitefouille();
		$tab = (!empty($_POST['contour_modif'])) ? $_POST['contour_modif'] : null;
		if (!empty($tab)) {
			$id = $tab[count($tab)-1];
			unset($tab[count($tab)-1]);

			$unitesondage = new Model_Unitesondage($id);

			$contour_fetch = new Model_Contour_Unitesondage();
			$contour_test = $contour_fetch->fetchAll()->where('unitesondage_id="'.$unitesondage->get('id').'"');
			$contour_test = $contour_test->toArray();
			if (!empty($contour_test)) {
				foreach ($contour_test as $item) {
					$element = new Model_Contour_Unitesondage($item['id']);
					$element->delete();
				}
			}
			$generateGuuid = new Plugin_Guuid();

			$info_contour = array();
			$info_contour['unitesondage_id'] = $unitesondage->get('id');

			foreach ($tab as $cle => $value ) {
				$contour = new Model_Contour_Unitesondage();
				$guuid = $generateGuuid->GetUUID();
				$info_contour['id'] = $guuid;
				$coor = explode('-', $value);
				$info_contour['x'] = trim($coor[0]);
				$info_contour['y'] = trim($coor[1]);
				$info_contour['num_sequence'] = $cle + 1;
					
				$contour->populate($info_contour)->save();
			}

			echo "Modification de l'unite de sondage ".$unitesondage->get('identification')." avec succes";
		} else {
			echo 'une erreur est survenue';
		}
	}

	
	public function actionAjaxDeleteContours() {
		$this->getLayout()->setEnabled(false) ;
		$this->_view->setEnabled(false) ;

		$sitefouille = new Model_Sitefouille();
		$id = (!empty($_POST['id'])) ? $_POST['id'] : null;
		if (!empty($id)) {
			$unitesondage = new Model_Unitesondage($id);

			$contour_fetch = new Model_Contour_Unitesondage();
			$contour_test = $contour_fetch->fetchAll()->where('unitesondage_id="'.$unitesondage->get('id').'"');
			$contour_test = $contour_test->toArray();
			if (!empty($contour_test)) {
				foreach ($contour_test as $item) {
					$element = new Model_Contour_Unitesondage($item['id']);
					$element->delete();
				}
			}

			echo "les contours de l'unite de sondage ".$us->get('identification')." supprimes avec succes";
		} else {
			echo 'une erreur est survenue';
		}
	}


	public function actionIndex() {

		$contour_us = new Model_Contour_Unitesondage();

		$contour_uses = $contour_us->fetchAll();

		$this->_view->set('contour_unitesondages', $contour_uses);
	}

	public function actionAdd() {

		$contour_us = new Model_Contour_Unitesondage();

		$form = new Form_Contour_Unitesondage($contour_us);

		if($form->isSubmitted() && $form->isValid()) {

			$contour_us->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'contour_unitesondage has been added');

			$this->forward('Contour_Unitesondage', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$contour_us = new Model_Contour_Unitesondage($this->_request->getParams());

		$form = new Form_Contour_Unitesondage($contour_us);

		if($form->isSubmitted() && $form->isValid()) {

			$contour_us->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'contour_unitesondage has been edited');

			$this->forward('Contour_Unitesondage', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$contour_us = new Model_Contour_Unitesondage($this->_request->getParams());

		$contour_us->delete();

		$this->getSession()->set('flash', 'contour_unitesondage has been deleted');

		$this->forward('Contour_Unitesondage', 'index');

	}

}