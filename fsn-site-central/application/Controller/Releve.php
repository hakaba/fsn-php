<?php

class Controller_Releve extends Yab_Controller_Action {

	public function actionIndex() {

		$releve = new Model_Releve();

		$releves = $releve->fetchAll();

		$this->_view->set('releves', $releves);
	}

	public function actionAdd() {

		$releve = new Model_Releve();

		$form = new Form_Releve($releve);

		if($form->isSubmitted() && $form->isValid()) {

			$releve->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'releve as been added');

			$this->forward('Releve', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$releve = new Model_Releve($this->_request->getParams());

		$form = new Form_Releve($releve);

		if($form->isSubmitted() && $form->isValid()) {

			$releve->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'releve as been edited');

			$this->forward('Releve', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$releve = new Model_Releve($this->_request->getParams());

		$releve->delete();

		$this->getSession()->set('flash', 'releve as been deleted');

		$this->forward('Releve', 'index');

	}

}