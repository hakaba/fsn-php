<?php

class Controller_Log extends Yab_Controller_Action {

	public function actionIndex() {

		$log = new Model_Log();
		
		$session = $this->getSession();
		if($session->has('unitesondage_id')) {
			$logs = $log -> getListLog($session->get('unitesondage_id'));
		} else {
			$logs = $log->fetchAll();
		}

		$this->_view->set('logs', $logs);
	}

	public function actionAdd() {

		$mode = self::ACTION_MODE_CREATE;
		
		$log = new Model_Log();
		$form_add = new Form_Log($log, $mode);
		
		$form_add->setElement('numero_ident', array(
				'type' => 'text',
				'id' => 'numero_ident',
				'label' => $form_add->getElement('identification')->get('label'),
				'value' => isset($_POST['numero_ident']) ? $_POST['numero_ident'] : $form_add->getElement('identification')->get('numero'),
				'validators' => array('NotEmpty','Int'),
				'errors' => array('NotEmpty' => array('Le champ identification ne doit pas être vide'),
						'Int' => array('Ce champs doit être un entier')),
		));

		if($form_add->isSubmitted() && $form_add->isValid()) {

			$formvalues = $form_add->getValues();
			//print_r($formvalues);
			$log->populate($formvalues)->save();
			
			$this->getSession()->set('flash_title', '');
			$this->getSession()->set('flash_status', 'info');
			$this->getSession()->set('flash_message', 'log has been added');
			
			//$this->forward('Log', 'index');

		} else {
			if($form_add->isValid() && !empty($_POST)) {
				$formvalues = $form_add->getValues();
				unset($formvalues['numero_ident']);
				// Ajout du GUUID
				$generateGuuid = new Plugin_Guuid();
				$guuid = $generateGuuid -> GetUUID();
				//$form->setElement('id', array('type'=>'text', 'value' => $guuid ) ) ;
				$formvalues['id'] = $guuid;
				$formvalues['identification'] = $form_add->getElement('identification')->get('value');
				
				$formvalues = $form_add -> setNullValues($formvalues);
				$log->populate($formvalues)->save();
				
				$this->getSession()->set('flash_title', '');
				$this->getSession()->set('flash_status', 'info');
				$this->getSession()->set('flash_message', 'log has been added');
				
				//$session = $this->getSession();
				//$this->forward('Log', 'index');
			}
		}
		
		$this->_view->set('form_add', $form_add);

	}
	
	public function actionShow($id = null) {
	
		$mode = self::ACTION_MODE_SHOW;
		
		$session = $this->getSession();
	
		$params = $this->_request->getParams();
		$log_id = $params[0];
	
		$modellog = new Model_log( $this->_request->getParams () );
		$form = new Form_log($modellog, $mode);
		//$formvalues = $form->getValues();
		$id_log = $modellog->get ('id');
	
		$tranchee = null;
		$i18n = Yab_Loader::getInstance()->getRegistry()->get('i18n');
		
		$couchelog = new Model_Couchelog();
		if($couchelog->getListCouches($id_log)->count() > 0) {
			// pour les couches:
			$couches = $couchelog -> getListCouches($id_log);
			$form -> setElement('couches', array(
					'type' => 'text',
					'id' => 'tranchee',
					'label' => $i18n -> say('couches'),
					'placeholder' => $i18n -> say('couches'),
					'tooltip' => $i18n -> say('couches'),
					'value' => $couches,
					'validators' => array(),
					'errors' => array(),
			));
		}
	
		$session = $this->getSession();
		$session['log_id'] = $log_id;
		
		$this->_view->set('form_show', $form);
	
	}

	public function actionEdit() {

		$mode = self::ACTION_MODE_UPDATE;
		
		$params = $this->_request->getParams();
		$log_id = $params[0];
		
		$log = new Model_Log($params);
		$form = new Form_Log($log, $mode);

		if($form->isSubmitted() && $form->isValid()) {

			$log->populate($form->getValues())->save();
			
			$this->getSession()->set('flash_title', '');
			$this->getSession()->set('flash_status', 'info');
			$this->getSession()->set('flash_message', 'log as been edited');
			if($this->getSession()->has('unitesondage_id')) {
				$this->forward('Unitesondage', 'edit', array($this->getSession()->get('unitesondage_id')));
			} else {
				$this->forward('Log', 'index');
			}

		}

		$session = $this->getSession();
		$session['log_id'] = $log_id;
		
		$this->_view->set('form_edit', $form);

	}

	public function actionDelete() {

		$log = new Model_Log($this->_request->getParams());
		$form = new Form_Log($log);
		$formvalues = $form->getValues();
		
		$formvalues ['id'] = $log->get ('id');//echo $formvalues['id'];

		$message = '';
		
		try {
			$log->delete();
			
			// historisation début
			$historisation = new Plugin_Historique();
			$formvalues = $form->getTypedValues($formvalues);
			$historisation->addhistory('Uslog', self::MODE_DELETE, $formvalues);
			// historisation fin
			$message = 'Log supprimé';
		} catch (Exception $e) {
			$message = 'Log n\'a pas pu etre supprimé';
		}

		$this->getSession()->set('flash_title', '');
		$this->getSession()->set('flash_status', 'info');
		$this->getSession()->set('flash_message', $message);

		//$this->forward('Log', 'index');

	}

}