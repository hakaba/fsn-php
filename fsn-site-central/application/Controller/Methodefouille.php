<?php

class Controller_Methodefouille extends Yab_Controller_Action {

	public function actionIndex() {

		$methodefouille = new Model_Methodefouille();

		$methodefouilles = $methodefouille->fetchAll();

		$this->_view->set('methodefouilles', $methodefouilles);
	}

	public function actionAdd() {

		$methodefouille = new Model_Methodefouille();

		$form = new Form_Methodefouille($methodefouille);

		if($form->isSubmitted() && $form->isValid()) {

			$methodefouille->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'methodefouille as been added');

			$this->forward('Methodefouille', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$methodefouille = new Model_Methodefouille($this->_request->getParams());

		$form = new Form_Methodefouille($methodefouille);

		if($form->isSubmitted() && $form->isValid()) {

			$methodefouille->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'methodefouille as been edited');

			$this->forward('Methodefouille', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$methodefouille = new Model_Methodefouille($this->_request->getParams());

		$methodefouille->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'methodefouille as been deleted');

		$this->forward('Methodefouille', 'index');

	}

}