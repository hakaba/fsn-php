<?php

class Controller_Contexte extends Yab_Controller_Action {

	public function actionIndex() {

		$contexte = new Model_Contexte();

		$contextes = $contexte->fetchAll();

		$this->_view->set('contextes', $contextes);
	}

	public function actionAdd() {

		$contexte = new Model_Contexte();

		$form = new Form_Contexte($contexte);

		if($form->isSubmitted() && $form->isValid()) {

			$contexte->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'contexte as been added');

			$this->forward('Contexte', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$contexte = new Model_Contexte($this->_request->getParams());

		$form = new Form_Contexte($contexte);

		if($form->isSubmitted() && $form->isValid()) {

			$contexte->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'contexte as been edited');

			$this->forward('Contexte', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$contexte = new Model_Contexte($this->_request->getParams());

		$contexte->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'contexte as been deleted');

		$this->forward('Contexte', 'index');

	}

}