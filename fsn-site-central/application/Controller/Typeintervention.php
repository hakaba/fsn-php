<?php

class Controller_Typeintervention extends Yab_Controller_Action {

	public function actionIndex() {

		$typeintervention = new Model_Typeintervention();

		$typeinterventions = $typeintervention->fetchAll();

		$this->_view->set('typeinterventions', $typeinterventions);
	}

	public function actionAdd() {

		$typeintervention = new Model_Typeintervention();

		$form = new Form_Typeintervention($typeintervention);

		if($form->isSubmitted() && $form->isValid()) {

			$typeintervention->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'typeintervention as been added');

			$this->forward('Typeintervention', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$typeintervention = new Model_Typeintervention($this->_request->getParams());

		$form = new Form_Typeintervention($typeintervention);

		if($form->isSubmitted() && $form->isValid()) {

			$typeintervention->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'typeintervention as been edited');

			$this->forward('Typeintervention', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$typeintervention = new Model_Typeintervention($this->_request->getParams());

		$typeintervention->delete();

		$this->getSession()->set('flash', 'typeintervention as been deleted');

		$this->forward('Typeintervention', 'index');

	}

}