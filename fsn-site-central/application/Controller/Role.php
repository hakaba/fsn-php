<?php

class Controller_Role extends Yab_Controller_Action {

	public function actionIndex() {

		$role = new Model_Role();

		$roles = $role->fetchAll();

		$this->_view->set('roles', $roles);
	}

	public function actionAdd() {

		$role = new Model_Role();

		$form = new Form_Role($role);

		if($form->isSubmitted() && $form->isValid()) {

			$role->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'role as been added');

			$this->forward('Role', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$role = new Model_Role($this->_request->getParams());

		$form = new Form_Role($role);

		if($form->isSubmitted() && $form->isValid()) {

			$role->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'role as been edited');

			$this->forward('Role', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$role = new Model_Role($this->_request->getParams());

		$role->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'role as been deleted');

		$this->forward('Role', 'index');

	}

}