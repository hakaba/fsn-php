<?php

class Controller_Etattraitement extends Yab_Controller_Action {

	public function actionIndex() {

		$etattraitement = new Model_Etattraitement();

		$etattraitements = $etattraitement->fetchAll();

		$this->_view->set('etattraitements', $etattraitements);
	}

	public function actionAdd() {

		$etattraitement = new Model_Etattraitement();

		$form = new Form_Etattraitement($etattraitement);

		if($form->isSubmitted() && $form->isValid()) {

			$etattraitement->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'etattraitement as been added');

			$this->forward('Etattraitement', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$etattraitement = new Model_Etattraitement($this->_request->getParams());

		$form = new Form_Etattraitement($etattraitement);

		if($form->isSubmitted() && $form->isValid()) {

			$etattraitement->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'etattraitement as been edited');

			$this->forward('Etattraitement', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$etattraitement = new Model_Etattraitement($this->_request->getParams());

		$etattraitement->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'etattraitement as been deleted');

		$this->forward('Etattraitement', 'index');

	}

}