<?php

class Controller_Metier extends Yab_Controller_Action {

	public function actionIndex() {

		$metier = new Model_Metier();

		$metiers = $metier->fetchAll();

		$this->_view->set('metiers', $metiers);
	}

	public function actionAdd() {

		$metier = new Model_Metier();

		$form = new Form_Metier($metier);

		if($form->isSubmitted() && $form->isValid()) {

			$metier->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'metier as been added');

			$this->forward('Metier', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$metier = new Model_Metier($this->_request->getParams());

		$form = new Form_Metier($metier);

		if($form->isSubmitted() && $form->isValid()) {

			$metier->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'metier as been edited');

			$this->forward('Metier', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$metier = new Model_Metier($this->_request->getParams());

		$metier->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'metier as been deleted');

		$this->forward('Metier', 'index');

	}

}