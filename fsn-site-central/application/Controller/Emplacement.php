<?php


class Controller_Emplacement extends Yab_Controller_Action {

	public function actionGestion() {
		$this->getSession()->set("titre", "Gestion des emplacements");
		$this->_view->setFile('View/emplacement/gestion.php');
	}
	
	public function actionAjaxAjouter() {
		$emplacement = new Model_Emplacement();
		$types = $emplacement->getTypeEnfant($_POST["id"]) ;
		
		// Si l'utilisateur ne peut pas ajouter plus de sous emplacements
		if (count($types) == 1 && is_null($types[0]["id"]) && is_null($types[0]["categorie_value"])) {
			echo "null" ;
			exit ;
		}
		
		$this->_view->set("types", $types) ;
		$this->getLayout()->setEnabled(false) ;
		$this->_view->setFile('View/emplacement/ajax_ajouter.php') ;
	}
	
	public function actionAjaxModifier() {
		$emplacement = new Model_Emplacement();
        $emplacement_post = $emplacement->getDescriptionById($_POST["id"]) ; 
		$description = $emplacement_post[0]["description"] ;
		
		$types = $emplacement->getEmplacementTypeAll();
		
		$this->_view->set("types", $types) ;
		$this->_view->set("description", $description) ;
		$this->getLayout()->setEnabled(false) ;
		$this->_view->setFile('View/emplacement/ajax_modifier.php') ;
	}
	
	public function actionAjaxGetDescription() {
		$emplacement = new Model_Emplacement();
		 $emplacement = $emplacement->getEmplacementById($_POST["id"]) ;
		
		 $this->_view->set("emplacement", $emplacement) ;
		 $this->getLayout()->setEnabled(false) ;
		$this->_view->setFile("View/emplacement/ajax_get_description.php") ;
	}
	
	private function getRacine() {
		$user_session = $this->getSession()->get("session");
		$entiteadmin_id=$user_session["entiteadmin_id"];
		$emplacement = new Model_Emplacement();
		$racine=$emplacement->getVerifRacine($entiteadmin_id);
		if ($racine->toArray()==null) {
			$organisme=$emplacement->getNomOrganisme($entiteadmin_id);
			$organisme=$organisme->toArray();
			$racine_id=$this->sauver($organisme[0]["nom"]);
		} else {
			$racine=$racine->toArray();
			$racine_id=$racine[0]["id"];
		}
		return $racine_id;
	}
	
	private function sauver($nom) {
		$ref=new Model_Fsn_Categorie();
		$type=$ref->isExiste("emplacement_type", "racine");
		$type_id=$type[0]["id"];
		$user_session = $this->getSession()->get("session");
		$entiteadmin_id=$user_session["entiteadmin_id"];
		$emplacement = new Model_Emplacement();
		$insertion = array() ;
		$insertion["nom"] = $nom;
		$insertion["type_id"] = $type_id;
		$insertion["parent_id"] ="null";
		$insertion["entiteadmin_id"] = $entiteadmin_id;
		$insertion["description"] = "Emplacement Racine";
		$emplacement->populate($insertion)->save() ;
		return $emplacement->id ;
		
	}
	
	public function actionTest(){
		$this->getSession()->set("titre", "Gestion des emplacements") ;
		$this->getLayout()->setFile("View/layout_fsn.html") ;
		$this->_view->setFile('View/emplacement/test.html') ;
	}
	
	public function actionAjaxSauver() {
		
		$user_session = $this->getSession()->get("session");
		$emplacement = new Model_Emplacement();
		$this->getRacine();
		$this->getLayout()->setEnabled(false) ;
		$this->_view->setEnabled(false) ;
		
		if ($_POST["action"] == "ajouter") {
		
			if($_POST["parent_id"]==0)
				$_POST["parent_id"]=$this->getRacine();
			$insertion = array() ;
			$insertion["nom"] = $_POST["label"] ;
			$insertion["type_id"] = $_POST["type_id"] ;
			$insertion["parent_id"] = $_POST["parent_id"] ;
			$insertion["entiteadmin_id"] = $user_session["entiteadmin_id"] ;
			$insertion["description"] = $_POST["description"] ;
				
			$emplacement->populate($insertion)->save() ;
			echo $emplacement->id ;
		} else if ($_POST["action"] == "modifier") {
				
			$update= array() ;
			$update["id"] = $_POST["id"] ;
			$update["nom"] = $_POST["label_modified"] ;
			$update["entiteadmin_id"] = $user_session["entiteadmin_id"] ;
			$update["description"] = $_POST["description_modified"] ;
				
			$emplacement->populate($update)->save() ;
				
		} else if ($_POST["action"] == "supprimer") {
			if (!isset($emplacement->getEnfant($_POST["id"])[0])) {
				try {
					$emplacement->delete("id = '" . $_POST["id"] . "'") ;
				} catch (Exception $e) {
					echo 2 ;
				}
			} else {
				echo 0 ;
			}
		}
	}
	
	public function actionEnfant($id=null) {
			
		$emplacement = new Model_Emplacement();
		
		$this->getLayout()->setEnabled(false) ;
		$this->_view->setEnabled(false) ;
		$enfants = $emplacement->getEnfant($id);
		$table=array();
		$compt=0;
		foreach ($enfants as $enfant) {
			$table[$compt]["label"]=Plugin_Fonctions::html($enfant["nom"]);
			$table[$compt]["id"]=$enfant["id"];
			if ($enfant["nb_enfant"]!=0) {
				$table[$compt]["items"][0]["value"]=$this->getRequest()->getBaseUrl() . "/emplacement/enfant/".$enfant["id"];
				$table[$compt]["items"][0]["label"]="Loading...";
			}
			$compt++;
		}
		echo json_encode($table);
	}
	
	public function actionGetSite() {
		$user_session = $this->getSession()->get("session");	 
		$emplacement = new Model_Emplacement();
		
		$this->getLayout()->setEnabled(false) ;
		$this->_view->setEnabled(false) ;
		 
		 $session=$this->getSession();
		 $entiteadmin_id = $user_session["entiteadmin_id"] ;
		 $sites = $emplacement->getSite($entiteadmin_id);
		 
		 $organisme=$emplacement->getNomOrganisme($entiteadmin_id);
		 $organisme=$organisme->toArray();
		 $racine=$organisme[0]["nom"];
		 	
		 $table[0]["label"]=$racine;
		 $table[0]["id"]=0;
		 $table[0]["expanded"]="true";
		 $compt=0;
		 foreach ($sites as $site) {
			$table[0]["items"][$compt]["label"]=Plugin_Fonctions::html($site["nom"]);
			$table[0]["items"][$compt]["id"]=$site["id"];
			$table[0]["items"][$compt]["items"]=array();
			$table[0]["items"][$compt]["items"][0]["value"]=$this->getRequest()->getBaseUrl() . "/emplacement/enfant/".$site["id"];
			$table[0]["items"][$compt]["items"][0]["label"]="Loading...";
			$compt++;
		 }
		 echo json_encode($table);
	}
	
	
	
	public function actionIndex() {

		$emplacement = new Model_Emplacement();

		$emplacements = $emplacement->fetchAll();

		$this->_view->set('emplacements', $emplacements);
	}

	public function actionAdd() {
		    
        $mode = 'add' ;
        
		$emplacement = new Model_Emplacement();

		$form = new Form_Emplacement($emplacement,$mode);

		if($form->isSubmitted() && $form->isValid()) {
      
      // Ajout du GUUID 
      $generateGuuid = new Plugin_Guuid() ;
      $guuid = $generateGuuid->GetUUID() ;
      $form->setElement('id', array('type'=>'text', 'value' => $guuid ) ) ;

			$emplacement->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'emplacement as been added');

			$this->forward('Emplacement', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

    public function actionEdit() {

        $mode = 'edit' ;
        
        $emplacement = new Model_Emplacement($this->_request->getParams());

        $form = new Form_Emplacement($emplacement, $mode);

        if($form->isSubmitted() && $form->isValid()) {

            $emplacement->populate($form->getValues())->save();

            $this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'emplacement as been edited');

            $this->forward('Emplacement', 'index');

        }

        $this->_view->set('helper_form', new Yab_Helper_Form($form));

    }
    
    public function actionShow() {

        $mode = 'show' ;
        
        $emplacement = new Model_Emplacement($this->_request->getParams());

        $form = new Form_Emplacement($emplacement, $mode);

        if($form->isSubmitted()) {

            $this->forward('Emplacement', 'index');

        }

        $this->_view->set('helper_form', new Yab_Helper_Form($form));

    }

	public function actionDelete() {

		$emplacement = new Model_Emplacement($this->_request->getParams());

		$emplacement->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'emplacement as been deleted');

		$this->forward('Emplacement', 'index');

	}
	
	public function actionAjaxGetER() {
	
		$emplacement = new Model_Emplacement();
	
		$user_session = $this->getSession()->get('session') ;
		$entiteadmin_id = $user_session['entiteadmin_id'] ;
	
		$racine=$emplacement->getRacine($entiteadmin_id);
		$id=(isset($_POST["id"])  and $_POST["id"]!=0 ) ? $_POST["id"] : $racine["id"];
		//$id=(isset($_POST["id"]) ) ? $_POST["id"] : 32;
	
		$sites = $emplacement->getParent($entiteadmin_id, null, $id);
	
		$emps=array();
	
		//recupération des noms d'emplacement pour la concaténation
		foreach ($sites as $site ) {
			$emps[$site["id"]]=$site["nom"];
		}
		$result=array();
		$compt=0;
	
		$this->setERTable($id, $result, $compt, "");
	
		foreach ($sites as $site) {
			$tab=explode("-", $site["path"]);
				
			$chemin="";
			//on supprime la racine pour tous les enfants
			if (count($tab!=1))
				unset($tab[0]);
			foreach($tab as $cle => $item) {
				if (count($tab)==$cle)
					$chemin.=$emps[$item];
				else
					$chemin.=$emps[$item]." > ";
			}
			$this->setERTable($site["id"], $result, $compt, $chemin);
		}
		$this->getLayout()->setEnabled(false) ;
		$this->_view->set("data", $result);
	}
	
	public function setERTable($id, &$result, &$compt, $chemin) {
		$er=new Model_Elementrecueilli();
		$ers=$er->fetchAll()->where("emplacement_id='".$id."'");
	
		foreach ($ers as $er) {
			$result[$compt]["id"]=$er["id"];
			$result[$compt]["numero"]=$er["numero"];
			$result[$compt]["description"]=(strlen($er["description"])>45) ? substr($er["description"], 0, 45).". . .": $er["description"];
			$result[$compt]["chemin"]=$chemin;
			$result[$compt]["origine"]=(empty($chemin)) ? true : false;
			$compt++;
		}
		
		$ers=$er->getByEmplacement($id);
		
		foreach ($ers as $er) {
			$result[$compt]["id"]=$er["er_id"];
			$result[$compt]["numero"]=$er["numero"];
			$result[$compt]["description"]=(strlen($er["description"])>45) ? substr($er["description"], 0, 45).". . .": $er["description"];
			$result[$compt]["chemin"]=$chemin." > " . $er["nom"];
			$result[$compt]["origine"]=false;
			$compt++;
		}
	}
	
	public function actionGenererQRCode() {
		$emplacement = new Model_Emplacement();
		
		$user_session = $this->getSession()->get('session') ;
		$entiteadmin_id = $user_session['entiteadmin_id'] ;
		
		$racine=$emplacement->getRacine($entiteadmin_id);
		$id=(isset($_GET["id"])  and $_GET["id"]!=0 ) ? $_GET["id"] : $racine["id"];
		
		$loader = Yab_Loader::getInstance();
		$config = $loader->getConfig();
		$url = "http://" . $config["ip"] . ":" . $config["port"] . $config["context"] . "/emplacement/code" ;
	
		//	$emps = $emplacement->getEnfant($id) ;
		$emps = $emplacement->getParent($entiteadmin_id, null, $id);
		$emp_courant = $emplacement->getEmplacementById($id) ;	
		
		$this->getLayout()->setEnabled(false) ;
		$this->_view->set("url", $url) ;
		$this->_view->set("emps", $emps) ;
		$this->_view->set("emp_courant", $emp_courant) ;
	}

}