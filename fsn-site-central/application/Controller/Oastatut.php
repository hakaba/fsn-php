<?php

class Controller_Oastatut extends Yab_Controller_Action {

	public function actionIndex() {

		$oastatut = new Model_Oastatut();

		$oastatuts = $oastatut->fetchAll();

		$this->_view->set('oastatuts', $oastatuts);
	}

	public function actionAdd() {

		$oastatut = new Model_Oastatut();

		$form = new Form_Oastatut($oastatut);

		if($form->isSubmitted() && $form->isValid()) {

			$oastatut->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'oastatut as been added');

			$this->forward('Oastatut', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$oastatut = new Model_Oastatut($this->_request->getParams());

		$form = new Form_Oastatut($oastatut);

		if($form->isSubmitted() && $form->isValid()) {

			$oastatut->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'oastatut as been edited');

			$this->forward('Oastatut', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$oastatut = new Model_Oastatut($this->_request->getParams());

		$oastatut->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'oastatut as been deleted');

		$this->forward('Oastatut', 'index');

	}

}