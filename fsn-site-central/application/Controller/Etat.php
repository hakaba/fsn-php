<?php

class Controller_Etat extends Yab_Controller_Action {

	public function actionIndex() {

		$etat = new Model_Etat();

		$etats = $etat->fetchAll();

		$this->_view->set('etats', $etats);
	}

	public function actionAdd() {

		$etat = new Model_Etat();

		$form = new Form_Etat($etat);

		if($form->isSubmitted() && $form->isValid()) {

			$etat->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'etat as been added');

			$this->forward('Etat', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$etat = new Model_Etat($this->_request->getParams());

		$form = new Form_Etat($etat);

		if($form->isSubmitted() && $form->isValid()) {

			$etat->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'etat as been edited');

			$this->forward('Etat', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$etat = new Model_Etat($this->_request->getParams());

		$etat->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'etat as been deleted');

		$this->forward('Etat', 'index');

	}

}