<?php

class Controller_Caracterepate extends Yab_Controller_Action {

	public function actionIndex() {

		$caracterepate = new Model_Caracterepate();

		$caracterepates = $caracterepate->fetchAll();

		$this->_view->set('caracterepates', $caracterepates);
	}

	public function actionAdd() {

		$caracterepate = new Model_Caracterepate();

		$form = new Form_Caracterepate($caracterepate);

		if($form->isSubmitted() && $form->isValid()) {

			$caracterepate->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'caracterepate as been added');

			$this->forward('Caracterepate', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$caracterepate = new Model_Caracterepate($this->_request->getParams());

		$form = new Form_Caracterepate($caracterepate);

		if($form->isSubmitted() && $form->isValid()) {

			$caracterepate->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'caracterepate as been edited');

			$this->forward('Caracterepate', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$caracterepate = new Model_Caracterepate($this->_request->getParams());

		$caracterepate->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'caracterepate as been deleted');

		$this->forward('Caracterepate', 'index');

	}

}