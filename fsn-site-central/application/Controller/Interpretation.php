<?php

class Controller_Interpretation extends Yab_Controller_Action {

	public function actionIndex() {

		$interpretation = new Model_Interpretation();

		$interpretations = $interpretation->fetchAll();

		$this->_view->set('interpretations', $interpretations);
	}

	public function actionAdd() {

		$interpretation = new Model_Interpretation();

		$form = new Form_Interpretation($interpretation);

		if($form->isSubmitted() && $form->isValid()) {

			$interpretation->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'interpretation as been added');

			$this->forward('Interpretation', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$interpretation = new Model_Interpretation($this->_request->getParams());

		$form = new Form_Interpretation($interpretation);

		if($form->isSubmitted() && $form->isValid()) {

			$interpretation->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'interpretation as been edited');

			$this->forward('Interpretation', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$interpretation = new Model_Interpretation($this->_request->getParams());

		$interpretation->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'interpretation as been deleted');

		$this->forward('Interpretation', 'index');

	}

}