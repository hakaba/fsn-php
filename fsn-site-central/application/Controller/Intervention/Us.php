<?php

class Controller_Intervention_Us extends Yab_Controller_Action {

	// jfb 2016-04-21 correctif fiche mantis 230 début
	//const LABEL_HISTO = "Intervention Us";
	const LABEL_HISTO = "Intervention_Us";
	// jfb 2016-04-21 correctif fiche mantis 230 fin
	
	public function actionIndex() {

		$intervention_us = new Model_Intervention_Us();

		$intervention_uses = $intervention_us->fetchAll();

		$this->_view->set('intervention_uses', $intervention_uses);
	}

    public function actionAdd($intervenant_id=null, $us_id=null) {

		$intervention_us = new Model_Intervention_Us();

        $mode = 'add' ;
        
        if (!empty($intervenant_id) ) {$intervention_us->set('intervenant_id', $intervenant_id) ; }
        if (!empty($us_id) ) {$intervention_us->set('us_id', $us_id) ; }
		$form_intervention_us = new Form_Intervention_Us($intervention_us, $mode);

		if($form_intervention_us->isSubmitted() && $form_intervention_us->isValid()) {
            // Ajout du GUUID 
            $generateGuuid = new Plugin_Guuid() ;
            $guuid = $generateGuuid->GetUUID() ;
            $form_intervention_us->setElement('id', array('type'=>'text', 'value' => $guuid ) ) ;

			$intervention_us->populate($form_intervention_us->getValues())->save();

			$this->getSession()->set('flash', 'intervention_us as been added');

			$this->forward('Intervention_Us', 'index');

		}

		$this->_view->set('helper_form_intervention_us', new Yab_Helper_Form($form_intervention_us));

	}
	
    public function actionAddAjax($intervenant_id=null, $us_id=null) {

        $intervention_us = new Model_Intervention_Us();

        $mode = 'add' ;
        
        $form_intervention_us = new Form_Intervention_Us($intervention_us, $mode);
        if (!empty($intervenant_id) ) {            
            $intervention_us->set('intervenant_id', $intervenant_id) ;
            $form_intervention_us->getElement('intervenant_id')->set('value',$intervenant_id); 
        }
        if (!empty($us_id) ) {
            $intervention_us->set('us_id', $us_id) ;
            $form_intervention_us->getElement('us_id')->set('value',$us_id);
            $form_intervention_us->getElement('us_id')->set('readonly','readonly'); 
        }
        
        if($form_intervention_us->isSubmitted() && $form_intervention_us->isValid()) {
                
            // Ajout du GUUID
            $generateGuuid = new Plugin_Guuid() ;
            $guuid = $generateGuuid->GetUUID() ;
            $form_intervention_us->setElement('id', array('type'=>'text', 'value' => $guuid ) ) ;

            $intervention_us->populate($form_intervention_us->getValues())->save();

            $this->getSession()->set('flash', 'intervention_us as been added');

            // $this->forward('Intervention_Us', 'index');

        }
        $this->_view->set('helper_form_intervention_us', new Yab_Helper_Form($form_intervention_us));

    }
    
    public function actionEdit() {

		$intervention_us = new Model_Intervention_Us($this->_request->getParams());

		$mode = 'edit' ;
		
		$form_intervention_us = new Form_Intervention_Us($intervention_us, $mode);

		if($form_intervention_us->isSubmitted() && $form_intervention_us->isValid()) {

			$intervention_us->populate($form_intervention_us->getValues())->save();

			$this->getSession()->set('flash', 'intervention_us as been edited');

			$this->forward('Intervention_Us', 'index');

		}

		$this->_view->set('helper_form_intervention_us', new Yab_Helper_Form($form_intervention_us));

	}
  
	public function actionShow() {

		$intervention_us = new Model_Intervention_Us($this->_request->getParams());
    
        $mode = 'show' ;
    
		$form_intervention_us = new Form_Intervention_Us($intervention_us, $mode);

		$this->_view->set('form_intervention_us', $form_intervention_us);

	}

	public function actionDelete() {

		$intervention_us = new Model_Intervention_Us($this->_request->getParams());

		$intervention_us->delete();

		$this->getSession()->set('flash', 'intervention_us as been deleted');

		$this->forward('Intervention_Us', 'index');

	}

    public function actionListInterventions($layout=false, $us_id=null, $v_action=false){
        $this->getLayout()->setEnabled($layout);
        $newInt = new Model_Intervention_Us();
        $form_intervention = new Form_Intervention_Us($newInt);
        $us_interventions = $newInt->getInterventions($us_id);

        $this->_view->set('form_intervention',$form_intervention);
        $this->_view->set('us_interventions', $us_interventions);
        $this->_view->set('v_action', $v_action);

        if (!$layout) { $this->_view->setFile('View/intervention/us/list_simple.html') ; }
    }

}