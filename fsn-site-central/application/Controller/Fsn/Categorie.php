<?php

class Controller_Fsn_Categorie extends Yab_Controller_Action {

	public function actionGestion($reference = null) {
  
		$fsn_categorie = new Model_Fsn_Categorie ();		
	
  	$liste = $fsn_categorie->getListReference ();

		$this->getSession ()->set('titre', 'GESTION DES LISTES DE REFERENCE');
		$this->_view->set ( 'liste', $liste );
        $this->_view->set ( 'existe', '');
		$this->_view->set ( 'reference', $reference);
	}
	
	public function actionAjaxAfficher() {
    $fsn_categorie = new Model_Fsn_Categorie ();
		if (isset ( $_POST ['nameRef'] )) { $type = $_POST ['nameRef']; }			
		else { $type = null; }
    			
		$liste = $fsn_categorie->getListByType ( $type );
		$maxordre = $fsn_categorie->getMaxOrdre ( $type );
    
    $this->_view->set ( 'liste', $liste );
		$this->_view->set ( 'maxordre', $maxordre );
		
    /* Recuperation du nombre de relation possible avec cette Categorie */
    $fsn_categorie_relation = new Model_Fsn_Categorie_Relation();
    $nb_relation = $fsn_categorie_relation->getListRelations()->where('fcb.categorie_key = "'.$type.'" AND fcr.visible = 1' )->count() ;
    $this->_view->set ( 'nb_relation', $nb_relation );  
		
		$this->getLayout()->setEnabled(false) ;
		$this->_view->setFile('View/fsn/categorie/ajax_afficher.html') ;
	}
	
	public function actionAjaxSauver() {
		$fsn_categorie = new Model_Fsn_Categorie ();
		if (isset ( $_POST ['nameRef'] ) && ! empty ( $_POST ['nameRef'] ) && isset ( $_POST ['statut'] )) {
			$notify = null;
			$notification = null ;
            $historisation = new Plugin_Historique();
			if ($_POST ['statut'] == 'liste') {
				$success = true;
			}
			
			if ($_POST ['statut'] == 'insert' && ! empty ( $_POST ['libelle'] ) && $_POST ['visible'] != '') {
				try {
					$insertion = Array ();
                    $insertion_post = explode ( "_", $_POST ["nameRef"] ) ;
					$insertion ["categorie_object"] = $insertion_post[0];
					$insertion ["categorie_type"] = $_POST ["nameRef"];
					$insertion ["categorie_key"] = $this->_calculeKey ( $_POST ["libelle"] );
					$insertion ["categorie_value"] = $_POST ['libelle'];
					$insertion ["ordre"] = $_POST ["ordreMax"] + 1;
					if ($_POST ['visible'] == 'true')
						$insertion ["visible"] = 1;
					else
					   $insertion ["visible"] = 0;
				   
					   // $existe = $fsn_categorie->isExiste($_POST["nameRef"], $this->_calculeKey($_POST["libelle"])) ;
						$existe = $fsn_categorie->referenceExiste($_POST["nameRef"], $_POST["libelle"]) ;
					if(empty($existe)) {
                        // Mise à jours du champs old_id
						$fsn_categorie->populate( $insertion )->save() ;
                        $id_fsn_categorie = $fsn_categorie->get('id');
                        $fsn_categorie->set('old_id',$id_fsn_categorie)->save() ;
                        // historisation
                        $historisation->addhistory('Categorie', self::MODE_CREATE, $fsn_categorie->toArray());
						$notification = "nouvelle référence créée." ;
					} else
						$notification = "La référence est déjà existante." ;
					
				} catch ( Exception $e ) {
					$notification =  "Échec : la référence n'a pas été créée." ;
				}
			}
			
			if ($_POST ['statut'] == 'updateLibelle') {
				try {
					
					$update["id"] = $_POST ['id'];
					$update ["categorie_value"] = $_POST ['libelle'];
					
					$fsn_categorie->populate( $update)->save() ;
                    // historisation
                    $historisation->addhistory('Categorie', self::MODE_UPDATE, $fsn_categorie->toArray());
					$notification =  "la référence a été modifiée." ;

				} catch ( Exception $e ) {
					$notification = "Échec : la référence n'a pas été modifiée." ;
				}
			}
			
			if ($_POST ['statut'] == 'updateVisible') {
				try {
					$update["id"] = $_POST ['id'];
					if ($_POST ['visible'] == 'true')
						$update ["visible"] = 1;
					else
						$update ["visible"] = 0;
					
					$update ["ordre"] = $_POST ["ordreMax"] + 1;
					
					$fsn_categorie->populate( $update)->save() ;
                    // historisation
                    $historisation->addhistory('Categorie', self::MODE_UPDATE, $fsn_categorie->toArray());
					$notification =  "la référence a été modifiée." ;
				} catch ( Exception $e ) {
					$notification = "Échec : la référence n'a pas été modifiée." ;
				}
			}
			
			if ($_POST ['statut'] == 'updateOrdre') {
				try {
					$ordre = json_decode ( $_POST ['ordre'], true );
					
					foreach ( $ordre as $id => $v ) {
						$update ["ordre"] = $v;
						$updateResult = $fsn_categorie->update ( $update, " id = '" . $id . "' " );
					}
				} catch ( Exception $e ) {
					$notification = "Il s'est produit une erreur lors de la modification de l'ordre." ;
				}
			}
			
			if ($_POST ['statut'] == 'delete') {
				try {
					$reference = new Model_Fsn_Categorie ($_POST["id"]);
                    // historisation
                    $historisation->addhistory('Categorie', self::MODE_DELETE, $reference->toArray());
                    $reference->delete() ;
					$notification = "la référence a été supprimée." ;
				} catch ( Exception $e ) {
					$notification = "Échec : la référence est utilisée par l'application." ;
				}
			}
			
			$this->actionAjaxAfficher ();
			if (!empty($notification) ) {
        $this->getSession()->set('flash_title', ''); 
  			$this->getSession()->set('flash_message', $notification);
        $this->getSession()->set('flash_status', 'success');
      } 
		}
	}
	
	public function actionAjaxRelier() {
		$fsn_categorie = new Model_Fsn_Categorie();
		$fsn_categorie_relation = new Model_Fsn_Categorie_Relation();
		
    /* Récupération de l'élément à relier */
    $element_b = $_POST["element_b"] ;
		
    $reference_categorie_type = $fsn_categorie_relation->getCategorieTypeRelation($element_b) ;
    
    /* Construction d'un tableau par type de categorie possible à mettre en relation 
    *  -> avec le détail des liens possibles (type_relation) par categorie
    *  -> avec le détail des éléments de cette categorie       
    */
    foreach($reference_categorie_type as $reference_type_value ){
      
      $element_a_type = $reference_type_value->get('element_a_type');
      $element_a_key = $reference_type_value->get('element_a_key');
      $type_relation_key = $reference_type_value->get('type_relation_key');

      /* Liste des relations existante avec element_b choisi */
      $list_relations = $fsn_categorie_relation->getListRelations()->where('element_b = '.$element_b.' AND fca.categorie_type = "'.$element_a_key.'" ' ) ;
      
      /* Liste des éléments de la catégorie "element_a" sauf lui-même  
      *  dans le cas où la categorie de element_a et element_b est la même
      */
      $element_b_key = $reference_type_value->get('element_b_key');
      $list_categorie = $fsn_categorie->getListByType($element_a_key)->where('id != "'.$element_b.'" ') ;      

      /* Liste des relations potentielles avec element_b choisi et les element de la liste détail de element_a */
      $list_potentiels = $this->_getListSansRelations($list_categorie, $list_relations) ;
      
      /* Liste des relations existante avec les parents de element_b choisi */
      $list_relations_parents = $this->_getParentsRelations($element_b) ;
      $list_potentiels = $this->_getListSansRelations($list_potentiels, $list_relations_parents) ;
      
      /* Tableau récapitulatif de toutes les infos */      
      $list_reference_type[$element_a_key] = array (
        'element_a' => $reference_type_value->get('element_a'),
        'element_a_type' => $element_a_type,
        'element_a_key' => $element_a_key,
        'element_a_value' => $reference_type_value->get('element_a_value'),
        //'reference' => $reference_type_value->get('element_a_type'),
        'reference' => $element_a_key,
        'nom' => $reference_type_value->get('element_a_value'), 
        'type_relation' => array ( 
          $type_relation_key => array (
            'type_relation' => $reference_type_value->get('type_relation'), 
          	'type_relation_key' => $type_relation_key,
          	'type_relation_value' => $reference_type_value->get('type_relation_value'),
          )
        ),
        'element_a_list_relations_parent' => $list_relations_parents,
        'element_a_list_relations' => $list_relations->toArray(),
        'element_a_list_potentiel' => $list_potentiels,
        
      );
        
    }
    
		$ordre = json_decode ( $_POST ['ordre'], true );

		$categorie_type = $fsn_categorie->getTypeById($element_b) ;
		
		if (!empty($_POST["reference"]))
			$categorie_type = $_POST["reference"] ;
		
    if ($_POST["action"] == "changerReference") {
      /* identification de "categorie_key" de la categorie choisie = categorie_a_key */
			$categorie_a_key = $_POST['reference'] ;
		} else {
      $categorie_a_key = '' ;   
    }

		if ($_POST["action"] == "supprimer") {
        /* Appel du Model_Fsn_Categorie_Relation */
        $relation_id = $_POST ['relation_id'] ;
        
        $this->_modifRelation($relation_id, null, 'delete', 'delete') ; 
        
    } else  if ($_POST["action"] == "updateVisible") {
      /* Appel du Model_Fsn_Categorie_Relation */
      $relation_id = ( !empty( $_POST ['relation_id']) )? $_POST ['relation_id'] : null ;
      $visible = ($_POST ['visible'] == 'true') ? 1 : 0 ;
      $ordre = ($fsn_categorie->getOrdreRMax($element_b, $categorie_type)+1) ;  
      
      $modification['id'] = $relation_id ;      
      $modification['visible'] = $visible;      
      // $form['ordre'] = $ordre;
      
      $this->_modifRelation($relation_id, $modification, 'edit', 'save') ; 

		} else if ($_POST["action"] == "updateDescription") {
      /* Appel du Model_Fsn_Categorie_Relation */
      $visible = ($_POST ['visible'] == 'true') ? 1 : 0 ;
      $relation_id = ( !empty( $_POST ['relation_id']) )? $_POST ['relation_id'] : null ;
      
      $modification['description'] = $_POST['description'] ;
      
      $this->_modifRelation($relation_id, $modification, 'edit', 'save') ;
      
		} else  if ($_POST["action"] == "updateOrdre") {
		/* Mise à jours de l'ordre de tri des relation */
    	foreach ( $ordre as $element_a => $v) {
				$update ["ordre"] = $v;
				$fsn_categorie_relation->update( $update, " element_b = '" . $element_b . "' AND element_a = '" . $element_a . "' " );
			}
		} 
    
    if ($_POST["action"] == "ajouter" && !empty($_POST["element_a"])) {
      /* Idenfication du type_relation_id */
			$detail_type_relation = $fsn_categorie->fetchAll()
        ->where('categorie_type = "categorie_relation" AND categorie_key ="'.$_POST['type_relation'].'" ')->toRow();
      $type_relation_id = $detail_type_relation['id'] ;
      
      $insertion["element_a"] = $_POST["element_a"] ;
			$insertion["type_relation"] = $_POST["type_relation"] ;
      $insertion["type_relation_id"] = $type_relation_id ;					
			$insertion["element_b"] = $element_b ;
      $insertion["visible"] = ($_POST["visible"] == 'true') ? 1 : 0 ;     
      $insertion["ordre"] = ($fsn_categorie->getOrdreRMax($element_b, $categorie_type)+1) ;        
			$insertion["description"] = $_POST["description"] ;				
			
      $this->_modifRelation(null, $insertion, 'create', 'save') ;			
		}
		
		/* Recuperation du nombre de relation possible avec cette Categorie */
    $relations = $fsn_categorie_relation->getListRelations()->where('fcr.element_b = '.$element_b.' ' ) ;
		
    /* Variables pour la view */
	  $this->_view->set ( 'list_reference_type', $list_reference_type);
		$this->_view->set ( 'relations', $relations);
		/*  "categorie_a_key" est egal à la "categorie_key" de la categorie choisie dont depend 
    *   "element_a" de la relation.
    *   categorie_a_key equivaut à categorie_type de element_a  */
		$this->_view->set ( 'categorie_a_key', $categorie_a_key);
    $this->_view->set ( 'categorie_type', $categorie_type);
		$this->_view->set ( 'element_b', $element_b);
	
		$this->getLayout()->setEnabled(false) ;
		$this->_view->setFile("View/fsn/categorie/ajax_relier.html") ;
	}

	/*
  * Liste des relations potentielles entre les éléments d'une categorie et une autre
  * NON ENCORE définies.  
  */
  private function _getListSansRelations($liste, $relations) {
		
    $referencesNonReliees = is_object($liste) ? $liste->toArray() : $liste ;
    		
		foreach($referencesNonReliees as $i => $v) {
			foreach($relations as $relation) {
				if($v['id'] == $relation['element_a'])
					unset($referencesNonReliees[$i]) ;
			}
		}
		return $referencesNonReliees ;
	}

  private function _getParentsRelations($element_b) {
  
    $fsn_categorie_relation = new Model_Fsn_Categorie_Relation();
    
    $list_relations_parents = $fsn_categorie_relation->getListRelations()->where('element_a = '.$element_b ) ;
		
    $list_ascendants = array ();   
    if(!empty($list_relations_parents) ) {
      foreach($list_relations_parents as $parent ) {
        
        $element_b_parent = $parent->get('element_b') ;
        array_push($list_ascendants, array( 'element_a' => $element_b_parent) );
        $list_grand_parents = $this->_getParentsRelations($element_b_parent) ;
        foreach($list_grand_parents as $grand_parent ){
          $element_a_grand_parent = $grand_parent['element_a'] ;
          array_push($list_ascendants, array( 'element_a' => $element_a_grand_parent)  );
        }
  		}
    }		
		
		
    return $list_ascendants ;
	}    
  private function _calculeKey($str, $charset = 'utf-8') {

		// jfb 2016-06 mantis 311 début
  		$generateGuuid = new Plugin_Guuid();
  		$guuid = $generateGuuid->GetUUID();
  		return $guuid;
		// jfb 2016-06 mantis 311 fin
		
		$str = htmlentities ( $str, ENT_NOQUOTES, $charset );
		
		$str = preg_replace ( '#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str );
		$str = preg_replace ( '#&([A-za-z]{2})(?:lig);#', '\1', $str ); // pour les ligatures e.g. '&oelig;'
		$str = preg_replace ( '#&[^;]+;#', '', $str ); // supprime les autres caract�res
		$str = preg_replace ( "#[^a-zA-Z ]#", "", $str );
		
		return str_replace ( " ", "_", strtolower($str) );
	}

  public function _modifRelation($relation_id = null, $modif=null, $mode='edit', $action='save') {

    /* Si il n'y a rien à modifier alors suppression */
    if ( !empty($relation_id) && empty($modif) ) { $action=='delete' ; }
    
    
    if ( empty($relation_id ) && $action=='delete' ) {
      /* Si suppression mais rien indiqué alors rien à supprimer */
      return ;      
    } else if(!empty($relation_id ) ) {
      /* Appel du Model_Fsn_Categorie_Relation */
      $current_categorie_relation = new Model_Fsn_Categorie_Relation($relation_id); 
      $formvalue_relation = $current_categorie_relation->toArray() ;
    } else {
      /* Appel du Model_Fsn_Categorie_Relation */
      $current_categorie_relation = new Model_Fsn_Categorie_Relation();
      $form_relation = new Form_Fsn_Categorie_Relation($current_categorie_relation, $mode);
      $formvalue_relation = $form_relation->getValues();    
    }

   /* Control des champs à modifier */ 
    if(!empty($modif) ) {
      foreach($modif as $key => $value ) {
        if(isset($formvalue_relation[$key]) ) {
          // $form_relation->getElement($key)->set('value',$value);
          $formvalue_relation[$key] = $value ;
        }                
      }
    } 

    if ($action != 'delete') {  $current_categorie_relation->populate($formvalue_relation)->save();    }
    
    /* Historisation de la modification 
    *   APRES une creation / modification
    *   AVANT une suppression    
    */ 
    $formvalue_relation['id'] = $current_categorie_relation->get('id');
    $historisation = new Plugin_Historique() ;
    $historisation->addhistory('Fsn_Categorie_Relation',$mode,$formvalue_relation) ;
  
    if ($action=='delete') {  $current_categorie_relation->delete();    } 
 
    /*
    $this->getSession()->set('flash_title', ''); 
    $this->getSession()->set('flash_status', 'info'); 
    $this->getSession()->set('flash_message', 'la Référence est modifiée');
    */
    return ;
        
  }
}