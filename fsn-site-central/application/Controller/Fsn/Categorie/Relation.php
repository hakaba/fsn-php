<?php

class Controller_Fsn_Categorie_Relation extends Yab_Controller_Action {

	public function actionIndex() {

		// $this->getLayout()->setFile("View/layout_fsn.html" );

		$fsn_categorie_relation = new Model_Fsn_Categorie_Relation();

		$categorie_relations = array() ;

		// liste des racines parents des categories
		$list_racines = $fsn_categorie_relation->getListRacines() ;

		foreach ($list_racines as $parent ) {
			/**
			 *  Création d'un id unique aleatoire
			 */
			$id = 100000 + mt_rand(1, 100000) ;
			$parent->set('id',$id);
			$categorie_relations[] = $parent->toArray() ;
		}

		// complément de toutes les relations
		$fsn_categorie_relations = $fsn_categorie_relation->getListRelations();

		foreach ($fsn_categorie_relations as $value ) {
			$categorie_relations[] = $value->toArray() ;
		}

		$categorie_relations = $this->_array_orderby($categorie_relations, 'element_b_id', SORT_ASC ) ;

		$first_element = $categorie_relations[0]['element_b_id']  ;

		$categorie_relations_tree = array();

		$categorie_relations_tree = $this->_buildTree($categorie_relations, $first_element);

		$this->getSession ()->set('titre', 'GESTION DES RELATIONS ENTRE DE REFERENCE');

		$this->_view->set('categorie_relations', $categorie_relations_tree);

		$this->_view->set('fsn_categorie_relations', $fsn_categorie_relations);
		
		$this->getSession ()->set('titre', 'Relations entre Cat&eacute;gories');
		
	}

	public function actionList() {
		$this->getLayout()->setFile("View/layout_fsn.html" );
		 
		$fsn_categorie_relation = new Model_Fsn_Categorie_Relation();

		// $fsn_categorie_relations = $fsn_categorie_relation->fetchAll();
		$fsn_categorie_relations = $fsn_categorie_relation->getListRelations();

		$this->_view->set('fsn_categorie_relations', $fsn_categorie_relations);

		$this->getSession ()->set('titre', 'GESTION DES RELATIONS ENTRE DE REFERENCE');
	}

	public function actionAddRelation($fsn_categorie_id=null) {
		$this->getLayout()->setFile("View/layout_fsn.html" );
		/*
		 // Si la categorie (element_a) existe
		 if(!empty($fsn_categorie_id) ) {
		 // 1 - identification du categorie_type de cette categorie
		 $element_a_categorie = new Model_Fsn_Categorie($fsn_categorie_id);
		 $element_a_categories = $element_a_categorie->fetchAll();
		 $element_a_categorie_type = $element_a_categories->get('categorie_type');

		 // 2 - identification id de cette categorie( # categorie_type)
		 $fsn_categorie = new Model_Fsn_Categorie();
		 $fsn_categorie = new Model_Fsn_Categorie();
		 $fsn_categories = $fsn_categorie->fetchAll()->where('categorie_key = "'.$element_a_categorie_type.'"') ;
		 $id_categorie = $fsn_categories->get('id');

		 // 3 - identification des relations possibles
		 $relation_possible = new Model_Fsn_Categorie_Relation();
		 $relation_possibles = $relation_possible->fetchAll()->where('element_a = "'.$id_categorie.'" ');

		 }
		 */
		$fsn_categorie_relation = new Model_Fsn_Categorie_Relation();
		$mode = 'add' ;

		$form = new Form_Fsn_Categorie_Relation($fsn_categorie_relation, $mode, $fsn_categorie_id);

		if($form->isSubmitted() && $form->isValid()) {

			$fsn_categorie_relation->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', '');
			$this->getSession()->set('flash_status', 'info');
			$this->getSession()->set('flash_message', 'la relation est enregistr&eacute;e');

			$this->forward('Fsn_Categorie_Relation', 'list');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionAdd() {
		$this->getLayout()->setFile("View/layout_fsn.html" );

		$fsn_categorie_relation = new Model_Fsn_Categorie_Relation();
		$mode = 'add' ;

		$form = new Form_Fsn_Categorie_Relation($fsn_categorie_relation, $mode);

		if($form->isSubmitted() && $form->isValid()) {

			$fsn_categorie_relation->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', '');
			$this->getSession()->set('flash_status', 'info');
			$this->getSession()->set('flash_message', 'la relation est enregistr&eacute;e');

			$this->forward('Fsn_Categorie_Relation', 'list');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$this->getLayout()->setFile("View/layout_fsn.html" );

		$fsn_categorie_relation = new Model_Fsn_Categorie_Relation($this->_request->getParams());

		$form = new Form_Fsn_Categorie_Relation($fsn_categorie_relation);

		if($form->isSubmitted() && $form->isValid()) {

			$fsn_categorie_relation->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', '');
			$this->getSession()->set('flash_status', 'info');
			$this->getSession()->set('flash_message', 'la relation est enregistr&eacute;e');

			$this->forward('Fsn_Categorie_Relation', 'list');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionConfigAdd() {

		$fsn_categorie_relation = new Model_Fsn_Categorie_Relation();
		$mode = 'config' ;

		$form = new Form_Fsn_Categorie_Relation($fsn_categorie_relation, $mode);

		if($form->isSubmitted() && $form->isValid()) {

			$fsn_categorie_relation->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'fsn_categorie_relation as been added');

			$this->forward('Fsn_Categorie_Relation', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));
		$this->_view->setFile("View/fsn/categorie/relation/add.html") ;

	}

	public function actionConfigEdit() {

		$fsn_categorie_relation = new Model_Fsn_Categorie_Relation($this->_request->getParams());
		$mode = 'config' ;

		$form = new Form_Fsn_Categorie_Relation($fsn_categorie_relation, $mode);

		if($form->isSubmitted() && $form->isValid()) {

			$fsn_categorie_relation->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'fsn_categorie_relation as been edited');

			$this->forward('Fsn_Categorie_Relation', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));
		$this->_view->setFile("View/fsn/categorie/relation/edit.html") ;

	}

	public function actionDelete() {

		$fsn_categorie_relation = new Model_Fsn_Categorie_Relation($this->_request->getParams());

		$fsn_categorie_relation->delete();

		$this->getSession()->set('flash', 'fsn_categorie_relation as been deleted');

		$this->forward('Fsn_Categorie_Relation', 'index');

	}
	
  private function _array_orderby()
	{
		$args = func_get_args();
		$data = array_shift($args);
		foreach ($args as $n => $field) {
			if (is_string($field)) {
				$tmp = array();
				foreach ($data as $key => $row)
				$tmp[$key] = $row[$field];
				$args[$n] = $tmp;
			}
		}
		$args[] = &$data;
		call_user_func_array('array_multisort', $args);
		return array_pop($args);
	}

	private function _buildTree(array $elements, $element_parent = 0 ) {
		// 'categorie_type' # Racine de l'arborescence des categorie
		ini_set('xdebug.max_nesting_level', 600);
		ini_set('memory_limit', '256M');
		$branch=array() ;
		 
		foreach ($elements as $key_element => $element) {
			$type_relation = $element['type_relation'] ;
			// print $key_element.' '.$element['element_a_id'].' '.$type_relation.' '.$element['element_b_id'].'<br>' ;
			if ($element['element_b_id'] == $element_parent ) {
				if ( $element['element_b_id'] != $element['element_a_id'] ) {
					$children = $this->_buildTree($elements, $element['element_a_id']);
					if ($children) {
						$element[$type_relation] = $children;
					}
				}
				$branch[] = $element;
			}
		}

		return $branch;
	}
 
}