<?php

class Controller_Fsn_Categorie_Color extends Yab_Controller_Action {

	public function actionIndex() {

		$fsn_categorie_color = new Model_Fsn_Categorie_Color();

		$fsn_categorie_colors = $fsn_categorie_color->fetchAll();

		$this->_view->set('fsn_categorie_colors', $fsn_categorie_colors);
	}

	public function actionAdd() {

		$fsn_categorie_color = new Model_Fsn_Categorie_Color();
    $mode = 'add' ;
		$form = new Form_Fsn_Categorie_Color($fsn_categorie_color, $mode );

		if($form->isSubmitted() && $form->isValid()) {

			$form->remElement('categorie_type') ;
      
      $fsn_categorie_color->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); 
      $this->getSession()->set('flash_status', 'success'); 
      $this->getSession()->set('flash_message', 'fsn_categorie_color as been added');
      $this->getSession()->set('flash', 'fsn_categorie_color as been added');

			$this->forward('Fsn_Categorie_Color', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$fsn_categorie_color = new Model_Fsn_Categorie_Color($this->_request->getParams());

		$mode = 'edit' ;
		$form = new Form_Fsn_Categorie_Color($fsn_categorie_color, $mode );

		if($form->isSubmitted() && $form->isValid()) {

			$form->remElement('categorie_type') ;
      
      $fsn_categorie_color->populate($form->getValues())->save();
      
      $this->getSession()->set('flash_title', ''); 
      $this->getSession()->set('flash_status', 'success'); 
      $this->getSession()->set('flash_message', 'fsn_categorie_color as been edited');
			$this->getSession()->set('flash', 'fsn_categorie_color as been edited');

			$this->forward('Fsn_Categorie_Color', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$fsn_categorie_color = new Model_Fsn_Categorie_Color($this->_request->getParams());

		$fsn_categorie_color->delete();
    
    $this->getSession()->set('flash_title', '');
    $this->getSession()->set('flash_status', 'warning');
    $this->getSession()->set('flash_message', 'fsn_categorie_color as been deleted');
		$this->getSession()->set('flash', 'fsn_categorie_color as been deleted');

		$this->forward('Fsn_Categorie_Color', 'index');

	}

}