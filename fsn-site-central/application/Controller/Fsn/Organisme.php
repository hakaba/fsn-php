<?php

class Controller_Fsn_Organisme extends Yab_Controller_Action {

	public function actionIndex() {

		$fsn_organisme = new Model_Fsn_Organisme();

		// $fsn_organismes = $fsn_organisme->fetchAll();
		$fsn_organismes = $fsn_organisme -> getOrganismeDetail();

		$this -> getSession() -> set('titre', 'GESTION DES ORGANISMES'); //EDEM
		// $this -> getLayout() -> setFile("View/layout_fsn.html");
		$this -> _view -> set('fsn_organismes_index', $fsn_organismes);
	}

	public function actionAdd() {
		
		// appel fichier internationalisation
		$registry = Yab_Loader::getInstance()->getRegistry();
		$i18n = $registry->get('i18n');
		$filter_no_html = new Yab_Filter_NoHtml();
		
		$mode = self::ACTION_MODE_CREATE ;
		$errors_messages = '';
		
		$fsn_organisme = new Model_Fsn_Organisme();
		$form = new Form_Fsn_Organisme($fsn_organisme, $mode);
		$formvalue = $form->getValues();
		
		if ($form -> isSubmitted()) {
			
			try{
				
				//Default type_id: Organisme Externe
				$formvalue['type_id'] = 2337;
				
				// Control champs requis
				function isNumber($idElt, &$formvalue, &$form, &$errors_messages, $message){
					$eltvalue = addslashes($formvalue[$idElt]);
					if(!empty($eltvalue)){
						if($form->getElement($idElt)->getErrors()) $errors_messages[$idElt] = $message;
					}
				}					
				function isUnique($idElt, &$formvalue, &$fsn_organisme, &$errors_messages, $message){
					$eltvalue = addslashes($formvalue[$idElt]);
					if(!empty($eltvalue)){	
						$req = $fsn_organisme->fetchAll()->where(addslashes($idElt).' ="'.$eltvalue.'" ');
						$nb_row = $req->count();
						if($nb_row > 0){
							$errors_messages[$idElt] = $message;
						}
					}
				}

				if($form->getErrors()){
					$errors_messages[] = $filter_no_html->filter( $i18n -> say('verror_requisAll') );
				}
								
				isUnique('nom', $formvalue, $fsn_organisme , $errors_messages, $filter_no_html->filter( $i18n -> say('sitefouille_uniciteNom') ));
				isUnique('nomabrege', $formvalue, $fsn_organisme , $errors_messages, $filter_no_html->filter( $i18n -> say('sitefouille_uniciteNomAbrege') ));
				
				isNumber('cp', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('intervenant_requisCp') ));	
				
				//Caractères spéciaux interdits: Trois param($donnees, $field, $fieldMessage, $errors_messages)
				Plugin_Fonctions::nospecialcharacter($formvalue['nomabrege'], 'nomabrege', 'fsn_organisme_nopecialcaractere', $errors_messages);				
				
				if(!empty($errors_messages)) throw new Exception();				
				
				$fsn_organisme -> populate($formvalue) -> save();
				
				// Historisation de la modif
				$id_fsn_organisme = $fsn_organisme -> get('id');
				$formvalue['id'] = $id_fsn_organisme;
				$historisation = new Plugin_Historique();
				
				$formvalue = $form->getTypedValues($formvalue);
				$historisation -> addhistory('Fsn_Organisme', self::MODE_CREATE, $formvalue);				

				$this -> getSession() -> set('flash_title', ''); $this -> getSession() -> set('flash_status', 'info'); $this -> getSession() -> set('flash_message', 'organisme ajouté');
				
				$this -> forward('Fsn_Organisme', 'index');

			} catch(Exception $e){
				$this -> getSession() -> set('flash_title', ''); $this -> getSession() -> set('flash_status', 'warning'); $this -> getSession() -> set('flash_message', '');
				$this->getSession()->setFlashErrors($errors_messages);
			}
		}

		// $this -> _view -> set('helper_form', new Yab_Helper_Form($form));
		$this -> _view -> set('helper_form', $form);

	}

	public function actionEdit() {
		
		// appel fichier internationalisation
		$registry = Yab_Loader::getInstance()->getRegistry();
		$i18n = $registry->get('i18n');
		$filter_no_html = new Yab_Filter_NoHtml();
		
		$mode = self::ACTION_MODE_UPDATE ;
		$errors_messages = '';
		
		$fsn_organisme = new Model_Fsn_Organisme($this -> _request -> getParams());
		$form = new Form_Fsn_Organisme($fsn_organisme, $mode);
		$id = $fsn_organisme->get('id');
		$formvalue = $form->getValues();
		
		if ($form -> isSubmitted()) {
			
			try{
				
				//Default type_id: Organisme Externe
				$formvalue['type_id'] = 2337;
				
				// Control champs requis
				function isNumber($idElt, &$formvalue, &$form, &$errors_messages, $message){
					$eltvalue = addslashes($formvalue[$idElt]);
					if(!empty($eltvalue)){
						if($form->getElement($idElt)->getErrors()) $errors_messages[$idElt] = $message;
					}
				}					
				function isUnique($idElt, &$formvalue, &$fsn_organisme, &$id, &$errors_messages, $message){
					$eltvalue = addslashes($formvalue[$idElt]);
					if(!empty($eltvalue)){	
						$req = $fsn_organisme->fetchAll()->where(addslashes($idElt).' ="'.$eltvalue.'" and id !="'.$id.'" ');
						$nb_row = $req->count();
						if($nb_row > 0){
							$errors_messages[$idElt] = $message;
						}
					}
				}

				if($form->getErrors()){
					$errors_messages[] = $filter_no_html->filter( $i18n -> say('verror_requisAll') );
				}
								
				isUnique('nom', $formvalue, $fsn_organisme , $id, $errors_messages, $filter_no_html->filter( $i18n -> say('sitefouille_uniciteNom') ));
				isUnique('nomabrege', $formvalue, $fsn_organisme , $id, $errors_messages, $filter_no_html->filter( $i18n -> say('sitefouille_uniciteNomAbrege') ));
				
				isNumber('cp', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('intervenant_requisCp') ));	
				
				//Caractères spéciaux interdits: Trois param($donnees, $field, $fieldMessage, $errors_messages)
				Plugin_Fonctions::nospecialcharacter($formvalue['nomabrege'], 'nomabrege', 'fsn_organisme_nopecialcaractere', $errors_messages);
				
				if(!empty($errors_messages)) throw new Exception();
				
				$fsn_organisme -> populate($formvalue) -> save();
				
				// Historisation de la modif
				$id_fsn_organisme = $fsn_organisme -> get('id');
				$formvalue['id'] = $id_fsn_organisme;
				$historisation = new Plugin_Historique();
				
				$formvalue = $form->getTypedValues($formvalue);
				$historisation -> addhistory('Fsn_Organisme', self::MODE_UPDATE, $formvalue);
				
				$this -> getSession() -> set('flash_title', ''); $this -> getSession() -> set('flash_status', 'info'); $this -> getSession() -> set('flash_message', 'organisme modifié');
				
				$this -> forward('Fsn_Organisme', 'index');

			} catch(Exception $e){
				$this -> getSession() -> set('flash_title', ''); $this -> getSession() -> set('flash_status', 'warning'); $this -> getSession() -> set('flash_message', '');
				$this->getSession()->setFlashErrors($errors_messages);
			}
		}

		// $this -> _view -> set('helper_form', new Yab_Helper_Form($form));
		$this -> _view -> set('helper_form',$form);

	}

	public function actionShow() {
		
		$mode = self::ACTION_MODE_SHOW ;

		$fsn_organisme = new Model_Fsn_Organisme($this->_request->getParams());
		$form = new Form_Fsn_Organisme($fsn_organisme, $mode);
		$formvalue= $form->getValues();
		
		$id_fsn_organisme = $fsn_organisme->get('id');
		
		// $this->_view->set('helper_form', new Yab_Helper_Form($form));
		$this->_view->set('helper_form', $form);
        $this->_view->set('fsn_organisme_id', $id_fsn_organisme);

	}

	public function actionDelete() {

		$user_info = $this -> getSession() -> get('session');
		$login = $user_info['login'];

		$fsn_organisme = new Model_Fsn_Organisme($this -> _request -> getParams());
		$form = new Form_Fsn_Organisme($fsn_organisme);
		$formvalue = $form->getValues();
		
// jfb 2016-04-19 correctif fiche mantis 99 début
		$message = ''; // init. à blanc message de deletion
		
		try {
			$fsn_organisme -> delete(); // try delete avant historisation
				
			// historisation début
			$historisation = new Plugin_Historique();
			$formvalue = $form->getTypedValues($formvalue);
    	    $historisation->addhistory('Fsn_Organisme', self::MODE_DELETE, $formvalue);
    	    // historisation fin

    	    $message='organisme supprimé';
		} catch (Exception $e) {
    	   	$message = 'Suppression organisme impossible';
		}
    	    	
		//$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info');	$this->getSession()->set('flash_message', 'organisme as been deleted');
		$this->getSession()->set('flash_title', '');
		$this->getSession()->set('flash_status', 'info');
		$this->getSession()->set('flash_message', $message);
// jfb 2016-04-19 correctif fiche mantis 99 fin
		
		$this -> forward('Fsn_Organisme', 'index');

	}

	private function _AddEntiteAdmin($id_fsn_organisme) {

		$mode = self::ACTION_MODE_CREATE ;

		$fsn_entiteadmin = new Model_Fsn_Entiteadmin();
		$form = new Form_Fsn_Entiteadmin($fsn_entiteadmin);
		$formvalue = $form->getValues();

		$form -> getElement('organisme_id') -> set('value', $id_fsn_organisme);

		if ($form -> isValid()) {

			$fsn_entiteadmin->populate($formvalue)->save();

			// Historisation de la modif
			$id_fsn_entiteadmin = $fsn_entiteadmin->get('id');
			$formvalue['id'] = $id_fsn_entiteadmin;
			$historisation = new Plugin_Historique();
			
			$formvalue = $form->getTypedValues($formvalue);
			$historisation -> addhistory('Fsn_Entiteadmin', self::MODE_CREATE, $formvalue);

			// $this->getSession()->set('flash', 'fsn_entiteadmin as been added');

			// $this->forward('Fsn_Entiteadmin', 'index');

			return;

		}

		return;

	}

}
