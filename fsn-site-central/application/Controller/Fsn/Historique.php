<?php
use Trismegiste\Prolog\Inner\Variable;
class Controller_Fsn_Historique extends Yab_Controller_Action {

	/**
	 * Neutralisation des actions "parasites"
	 * redirection vers page d'accueil
	 */
	public function actionNoAction() {
		$flash_message = 'flash_no_action';
		$flash_status = 'alert';
		$this->getSession ()->set ( 'flash_message', $flash_message );
		$this->getSession ()->set ( 'flash_status', $flash_status );

		$this->forward ( 'Index', 'index' );
	}
	public function actionIndex() {
		// // $this -> getLayout() -> setFile("View/layout_fsn.html");  //EDEM  MODEL PAGE
		$this->getSession ()->set('titre', 'HISTORIQUE');  //EDEM  TITRE PAGE
		$fsn_historique = new Model_Fsn_Historique ();
		$form = new Form_Fsn_HistoriqueSearch ( $fsn_historique );
		$formvalue = $form->getValues ();

		$categorie_object = (! empty ( $formvalue ['categorie_object'] )) ? $formvalue ['categorie_object'] : null;
		$type_action = (! empty ( $formvalue ['type_action'] )) ? $formvalue ['type_action'] : null;
		$date_historique = (! empty ( $formvalue ['date_historique'] )) ? $formvalue ['date_historique'] : null;
		$login = (! empty ( $formvalue ['login'] )) ? $formvalue ['login'] : null;
		$data_modified = null;

		if ($form->isSubmitted () && $form->isValid () && ! empty ( $categorie_object )) {
			// Personalisation suivant le categorie_object choisie
			/*
      if ($categorie_object == 'sitefouille') {

				$id = $formvalue ['id'];
				$hostname = $formvalue ['hostname'];
				$user = $formvalue ['user'];

				$data_modified = null;

				if (! empty ( $id_monitors ))
					$data_modified .= '%"id_monitors":"' . $id_monitors . '"%';

				if (! empty ( $hostname ))
					$data_modified .= '%"hostname":"' . $hostname . '"%';

				if (! empty ( $user ))
					$data_modified .= '%"user":"' . $user . '"%';
			} elseif ($categorie_object == 'us') {

				$identification = $formvalue ['identification'];

				$data_modified = null;

				if (! empty ( $identification ))
					$data_modified .= '%"identification":"' . $identification . '"%';
			}
      */

			$fsn_historiques = $fsn_historique->listHistorique ( $categorie_object, $type_action, $date_historique, $login, $data_modified );
		} else {
					
			$fsn_historiques = $fsn_historique->listHistorique ();
		}
		$this->_view->set ( 'form', $form );

		$this->_view->set ( 'fsn_historiques_index', $fsn_historiques );
	}
	public function actionListSimple($layout=false,$categorie_object=NULL, $type_action=NULL, $date_historique=NULL, $login=NULL, $data_modified=NULL) {
            
        $this->getLayout()->setEnabled($layout);
        
        $fsn_historique = new Model_Fsn_Historique ();
        $fsn_historiques = $fsn_historique->listHistorique ( $categorie_object, $type_action, $date_historique, $login, $data_modified );
        $this->_view->set ( 'fsn_historiques', $fsn_historiques );
        
        $session = $this->getSession ();
        $user_info = $session->get('session');
        $entiteadmin_id = $user_info['entiteadmin_id'] ;

        $user = new Model_User();
        $users = (!empty($entiteadmin_id)) ? $user->fetchAll()->where('entiteadmin_id IN ( 0, '.$entiteadmin_id.' )') : $user->fetchAll() ;
        foreach ($users as $user ) {
            $list_user[$user->get('login')] = $user->toArray() ;
        }
        $this->_view->set('list_user', $list_user);

    }
    public function actionAdd($categorie_object, $type_action, $formvalue) {
		if (! empty ( $categorie_object ) && ! empty ( $type_action ) && ! empty ( $formvalue )) {

			// Set de la date
			$date_historique = date ( 'Y-m-d H:i:s' );
			// Set du user (login qui modifie)
			$registry = $this->getRegistry ();
			$session = $this->getSession ();
			$user_info = $session->get ( 'session' );
			$login = $user_info ['login'];
			// Set des donn‚es modif‚es

			$data_modified = json_encode ( $formvalue, JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE );
			// Set du commentaire
			$description = null;

			$fsn_historique = new Model_Fsn_Historique ();
			$form_historique = new Form_Fsn_Historique ( $fsn_historique );

			$form_historique_response = array (
					'categorie_object' => ucfirst ( $categorie_object ),
					'type_action' => $type_action,
					'date_historique' => $date_historique,
					'login' => $login,
					'data_modified' => $data_modified,
					'description' => $description
			);

			$fsn_historique->populate ( $form_historique_response )->save ();

			// $this->forward('Costream_History', 'index');
			$this->getSession ()->set ( 'flash_title', '' );
			$this->getSession ()->set ( 'flash_status', 'info' );
			$this->getSession ()->set ( 'flash_message', 'import' );
			return true;

		} else {

			$this->getSession ()->set ( 'flash_title', '' );
			$this->getSession ()->set ( 'flash_status', 'alert' );
			$this->getSession ()->set ( 'flash_message', 'import' );
			return false;


		}

	}
	public function actionEdit() {
		//// $this -> getLayout() -> setFile("View/layout_fsn.html");   //EDEM  MODEL AFFICHAGE PAGE
		$this->getSession ()->set('titre', 'DETAIL HISTORIQUE');   //EDEM  TITRE PAGE
		$fsn_historique = new Model_Fsn_Historique ( $this->_request->getParams () );

		$form = new Form_Fsn_Historique ( $fsn_historique );

		if ($form->isSubmitted () && $form->isValid ()) {

			$formvalue = $form->getValues ();
			$form_response = array (
					'description' => $formvalue ['description']
			);

			$fsn_historique->populate ( $form_response )->save ();

			$this->getSession ()->set ( 'flash_title', '' );
			$this->getSession ()->set ( 'flash_status', 'info' );
			$this->getSession ()->set ( 'flash_message', 'fsn_historique as been edited' );

			$this->forward ( 'Fsn_Historique', 'Index' );
		}

		$this->_view->set ( 'helper_form', new Yab_Helper_Form ( $form ) );
	}

/* jfb 2016-03-30
 *  public function actionShow($layout=false, $historique_id) {
 * permutation des deux paramètres pour placer layout non transmis en dernière position
 */
    public function actionShow($historique_id, $layout=false) { //jfb
    		 
        $this->getLayout()->setEnabled($layout);            
            
        $this->getSession ()->set('titre', 'DETAIL HISTORIQUE');   //EDEM  TITRE PAGE
        $fsn_historique = new Model_Fsn_Historique ( $historique_id);
        
        $form = new Form_Fsn_Historique ( $fsn_historique );
        
        $this->_view->set ( 'form_historique_show', $form );
        
        $this->_view->set ( 'fsn_historique', $fsn_historique );
    }

	/**
	 * Neutralisation des actions "parasites"
	 * redirection vers page d'accueil
	 */
	public function actionDelete() {

		/*
		 * $fsn_historique = new Model_Fsn_Historique($this->_request->getParams());
		 * $fsn_historique->delete();
		 * $this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'fsn_historique as been deleted');
		 */
		$this->forward ( 'Fsn_Historique', 'NoAction' );
	}
	public function actionViewData($module, $data) {
		$Controller = 'Fsn_' . ${module};
		$Model = 'Model_' . ${Controller};
		$Form = 'Form_' . ${Controller};
		$costream_module = new ${Model} ( $data );

		// $form = new Form_Fsn_History($fsn_historique);
		$form = new ${Form} ( $costream_module );

		if ($form->isSubmitted () && $form->isValid ()) {

			// $costream_module->populate($form->getValues())->save();
			$this->forward ( 'Fsn_History', 'index' );
		}

		$this->_view->set ( 'form', $form );
	}
    public function actionDownloadimport(){
		$objImport = new Model_Import();
		$table_tmp = $session = $this->getSession ()->get("table_tmp");
		$filename="export_".date("Y-m-d_H-i-s")."_".$table_tmp.".csv";
		header('Content-Type: application/csv');
		header('Content-Disposition: attachment; filename='.$filename);
		header('Pragma: no-cache');

		echo $objImport->exportCSVImport($table_tmp);
		die();
	}
	
	public function actionImport() {
		
		$fsn_historique = new Model_Fsn_Historique ();
		$form = new Form_Fsn_Import ( $fsn_historique );
		$objImport = new Model_Import();

		$formvalue = $form->getValues ();

		$registry = Yab_Loader::getInstance ()->getRegistry ();
		$flash_message = null;
		$flash_status = null;

		$session = $this->getSession ();
		$objImport->session=$session;
		$sitefouille_id = $session->has ( 'sitefouille_id' ) ? $session->get ( 'sitefouille_id' ) : null;
		$this->_view->set(("afficheForm"), 1);
		if ($form->isSubmitted ()) {

			$formvalue = $form->getValues();

			$module = $formvalue ['module'];
			$importfile = $formvalue ['importfile'];
			$firstlineheader = $formvalue ['firstlineheader'];
			$action = $formvalue ['type_action'];
			$date_modify = $formvalue ['date_historique'];
			$login_modify = $formvalue ['login'];
			$comment = $formvalue ['description'];
			$entiteadmin_modify = $formvalue ['entiteadmin']; // jfb 2016-04-22 cf. form import.php
				
			$config = $this->getConfig();
			//$path = dirname(YAB_PATH) .  DIRECTORY_SEPARATOR . 'tmp' .DIRECTORY_SEPARATOR . $config['environement'] . DIRECTORY_SEPARATOR . $importfile['name'];
      $path = $_SERVER['DOCUMENT_ROOT'].'/tmp/'.$config['environement'].'/'. $importfile['name'];

			if (file_exists($path)){
				unlink($path);
			}

			move_uploaded_file($importfile["tmp_name"],$path);
			chmod($path, 0777);
			$importfile ['tmp_name'] =$path;


// 			/* Historisation de l'action */
// 			$this->actionAdd ( $module, $action, $formvalue );

			/* Vérififcation & Traitement particulier suivant le module choisie */
			if ($module == 'sf') {
				/*
				 * * Import des Site de Fouille : Sitefouille
				 */
				//on bloque 'laffichage du formulaire dans la vue
				$this->_view->set(("afficheForm"), 0);

				$objImport->setNameMdel("Model_Sitefouille");
				$objImport->setTable("sitefouille");
				//création de la table temporaire
				$user_session = $session->get("session") ;
				$objImport->createNameTableTmp($user_session["login"]);

				//importation du fichier CSV
				$objImport->importCSVTmp($importfile["tmp_name"],$firstlineheader);
				$objImport->_setUUID("id");
				$objImport->_checkNotNull("nom");
				$objImport->_checkNotNull("nomabrege");
				//vérification spécifique au module fonctionnel choisi
				$objImport->_checkNumeric("surfaceestime");
				$objImport->_checkFloat("lambert_x");
				$objImport->_checkFloat("lambert_y");
				$objImport->_checkNumeric("surfaceestime");
				$objImport->_checkLength("nomabrege", 5);
				$objImport->_checkLength("nom", 40);
				$objImport->_checkLength("numerosite", 11);

				if($objImport->erreur()){

					$this->getSession ()->set ( 'flash_message', "Votre fichier contient des erreurs, merci de le corriger puis de le re-emporté" );
					$this->getSession ()->set ( 'flash_title', "L'import a échoué");
					$this->getSession ()->set ( 'flash_status', "danger" );
					$this->_view->set("listeImport", $objImport->listeImport());

				}else {

					$this->getSession ()->set ( 'flash_message', "Votre fichier a correctement été importé" );
					$this->getSession ()->set ( 'flash_title', "L'import a réussis");
					$this->getSession ()->set ( 'flash_status', "success" );

					$objImport->_delField("tmp_erreur");
					$objImport->_delField("tmp_id");
					$objImport->insertVraiTable();
				}


			}else if($module=="us"){

				/*
				* * Import des Us
				*/
				//on bloque 'laffichage du formulaire dans la vue
				$this->_view->set(("afficheForm"), 0);


				$objImport->setNameMdel("Model_Us");
				$objImport->setTable("us");
				//création de la table temporaire
				$user_session = $session->get("session") ;
				$objImport->createNameTableTmp($user_session["login"]);

				$objImport->importCSVTmp($importfile["tmp_name"],$firstlineheader);


				$objImport->_setUUID("id");

				$objImport->_checkStringExistEtranger("contexte_id", "fsn_categorie", "categorie_value");
				$objImport->_checkStringExistEtranger("consistance_id", "fsn_categorie", "categorie_value");
				$objImport->_checkStringExistEtranger("methodefouille_id", "fsn_categorie", "categorie_value");
				$objImport->_checkStringExistEtranger("sitefouille_id", "sitefouille", "nomabrege");
				$objImport->_checkStringExistEtranger("homogeneite_id", "fsn_categorie", "categorie_value");

				$objImport->_checkMois("instformdeb_mm");
				$objImport->_checkMois("instformfin_mm");
				$objImport->_checkMois("taq_mm");
				$objImport->_checkMois("tpq_mm");
				$objImport->_checkMois("tpq_mm");
				$objImport->_checkJour("instformdeb_jj");
				$objImport->_checkJour("instformfin_jj");
				$objImport->_checkJour("taq_jj");
				$objImport->_checkJour("tpq_jj");

				$objImport->_bool("elementmineral_tuiles");
				$objImport->_bool("elementmineral_brique");
				$objImport->_bool("elementorganique_charbon");
				$objImport->_bool("elementorganique_cendre");
				$objImport->_bool("elementmineral_platre");
				$objImport->_bool("materielisole");
				$objImport->_bool("materielconserve_poterie");
				$objImport->_bool("materielconserve_osanimal");
				$objImport->_bool("materielconserve_construc");

				$result = $objImport->listeImport();


				if($objImport->erreur()){

					$this->getSession ()->set ( 'flash_message', "Votre fichier contient des erreurs, merci de le corriger puis de le re-emporté" );
					$this->getSession ()->set ( 'flash_title', "L'import a échoué");
					$this->getSession ()->set ( 'flash_status', "danger" );
					$this->_view->set("listeImport", $objImport->listeImport());


				}else {

					$this->getSession ()->set ( 'flash_message', "Votre fichier a correctement été importé" );
					$this->getSession ()->set ( 'flash_title', "L'import a réussis");
					$this->getSession ()->set ( 'flash_status', "success" );
					$objImport->_replaceByAnId("contexte_id", "categorie_value", "id", "fsn_categorie");
					$objImport->_replaceByAnId("consistance_id", "categorie_value", "id", "fsn_categorie");
					$objImport->_replaceByAnId("methodefouille_id", "categorie_value", "id", "fsn_categorie");
					$objImport->_replaceByAnId("sitefouille_id", "nomabrege", "id", "sitefouille");
					$objImport->_replaceByAnId("homogeneite_id", "categorie_value", "id", "fsn_categorie");

					$objImport->_delField("tmp_erreur");
					$objImport->_delField("tmp_id");

					$objImport->insertVraiTable();
					$this->forward ( 'Fsn_Historique', 'import' );
				}

			}else if ($module=="categorie_relation"){
				//on bloque 'laffichage du formulaire dans la vue
				$this->_view->set(("afficheForm"), FALSE);

				$objImport->setNameMdel("Model_Fsn_Categorie_Relation");
				$objImport->setTable("fsn_categorie_relation");
				//création de la table temporaire
				$user_session = $session->get("session") ;
				$objImport->createNameTableTmp($user_session["login"]);

				$objImport->importCSVTmp($importfile["tmp_name"],$firstlineheader);
				$objImport->_setUUID("id");
				$objImport->_checkStringExistEtranger("element_a", "fsn_categorie", "categorie_value");
				$objImport->_checkStringExistEtranger("type_relation", "fsn_categorie", "categorie_value");
				$objImport->_checkStringExistEtranger("type_relation_id", "fsn_categorie", "categorie_value");
				$objImport->_checkStringExistEtranger("element_b", "fsn_categorie", "categorie_value");
				$objImport->_bool("visible");
				$objImport->_checkNumeric("ordre");

				if($objImport->erreur()){

					$this->getSession ()->set ( 'flash_message', "Votre fichier contient des erreurs, merci de le corriger puis de le re-emporté" );
					$this->getSession ()->set ( 'flash_title', "L'import a échoué");
					$this->getSession ()->set ( 'flash_status', "danger" );
					$this->_view->set("listeImport", $objImport->listeImport());

				}else {

					$this->getSession ()->set ( 'flash_message', "Votre fichier a correctement été importé" );
					$this->getSession ()->set ( 'flash_title', "L'import a réussis");
					$this->getSession ()->set ( 'flash_status', "success" );
					$objImport->_replaceByAnId("element_a", "categorie_value", "id", "fsn_categorie");
					$objImport->_replaceByAnId("element_b", "categorie_value", "id", "fsn_categorie");
					$objImport->_replaceByAnId("type_relation_id", "categorie_value", "id", "fsn_categorie");
					$objImport->_delField("tmp_erreur");
					$objImport->_delField("tmp_id");

					$objImport->insertVraiTable();
					$this->forward ( 'Fsn_Historique', 'import' );
				}

			}else if ($module=="participation"){
				//on bloque 'laffichage du formulaire dans la vue
				$this->_view->set(("afficheForm"), FALSE);

				$objImport->setNameMdel("Model_Participation");
				$objImport->setTable("participation");
				//création de la table temporaire
				$user_session = $session->get("session") ;
				$objImport->createNameTableTmp($user_session["login"]);

				$objImport->importCSVTmp($importfile["tmp_name"],$firstlineheader);
				$objImport->_setUUID("id");
				$objImport->_checkStringExistEtranger("intervenant_id", "intervenant", "nom");
				$objImport->_checkStringExistEtranger("oa_id", "oa", "identification");
				$objImport->_checkDate("debut");
				$objImport->_checkDate("fin");
				$objImport->_checkStringExistEtranger("role_id", "role", "role");

				if($objImport->erreur()){

					$this->getSession ()->set ( 'flash_message', "Votre fichier contient des erreurs, merci de le corriger puis de le re-emporté" );
					$this->getSession ()->set ( 'flash_title', "L'import a échoué");
					$this->getSession ()->set ( 'flash_status', "danger" );
					$this->_view->set("listeImport", $objImport->listeImport());

				}else {

					$this->getSession ()->set ( 'flash_message', "Votre fichier a correctement été importé" );
					$this->getSession ()->set ( 'flash_title', "L'import a réussis");
					$this->getSession ()->set ( 'flash_status', "success" );
					$objImport->_replaceByAnId("intervenant_id", "nom", "id", "intervenant");
					$objImport->_replaceByAnId("oa_id", "identification", "id", "oa");
					$objImport->_replaceByAnId("role_id", "role", "id", "role");
					$objImport->_delField("tmp_erreur");
					$objImport->_delField("tmp_id");
					$objImport->insertVraiTable();
					$this->forward ( 'Fsn_Historique', 'import' );
				}
			}else if ($module == "oa"){
				//on bloque 'laffichage du formulaire dans la vue
				$this->_view->set(("afficheForm"), FALSE);

				$objImport->setNameMdel("Model_Oa");
				$objImport->setTable("oa");
				//création de la table temporaire
				$user_session = $session->get("session") ;
				$objImport->createNameTableTmp($user_session["login"]);

				$objImport->importCSVTmp($importfile["tmp_name"],$firstlineheader);
				$objImport->_setUUID("id");
				$objImport->_checkStringExistEtranger("sitefouille_id", "sitefouille", "nomabrege");
				$objImport->_checkStringExistEtranger("oastatut_id", "fsn_categorie", "categorie_value");
				$objImport->_checkStringExistEtranger("nature_id", "fsn_categorie", "categorie_value");
				$objImport->_checkStringExistEtranger("entiteadmin_id", "fsn_entiteadmin", "id");
				$objImport->_checkDate("debut");
				$objImport->_checkDate("fin");

				if($objImport->erreur()){

					$this->getSession ()->set ( 'flash_message', "Votre fichier contient des erreurs, merci de le corriger puis de le re-emporté" );
					$this->getSession ()->set ( 'flash_title', "L'import a échoué");
					$this->getSession ()->set ( 'flash_status', "danger" );
					$this->_view->set("listeImport", $objImport->listeImport());

				}else {

					$this->getSession ()->set ( 'flash_message', "Votre fichier a correctement été importé" );
					$this->getSession ()->set ( 'flash_title', "L'import a réussis");
					$this->getSession ()->set ( 'flash_status', "success" );
					$objImport->_replaceByAnId("sitefouille_id", "nomabrege", "id", "sitefouille");
					$objImport->_replaceByAnId("oastatut_id", "categorie_value", "id", "fsn_categorie", 'oa_statut');
					$objImport->_replaceByAnId("nature_id", "categorie_value", "id", "fsn_categorie", "oa_nature");
					$objImport->_replaceByAnId("entiteadmin_id", "id", "id", "fsn_entiteadmin");
					$objImport->_delField("tmp_erreur");
					$objImport->_delField("tmp_id");
					$objImport->insertVraiTable();
					$this->forward ( 'Fsn_Historique', 'import' );
				}
			}else if ($module=="intervention"){
				//on bloque 'laffichage du formulaire dans la vue
				$this->_view->set(("afficheForm"), FALSE);

				$objImport->setNameMdel("Model_Intervention_Us");
				$objImport->setTable("intervention_us");
				//création de la table temporaire
				$user_session = $session->get("session") ;
				$objImport->createNameTableTmp($user_session["login"]);

				$objImport->importCSVTmp($importfile["tmp_name"],$firstlineheader);
				$objImport->_setUUID("id");
				$objImport->_checkStringExistEtranger("intervenant_id", "intervenant", "nom");
				$objImport->_checkStringExistEtranger("us_id", "us", "identification");
				$objImport->_checkStringExistEtranger("type_id", "fsn_categorie", "categorie_value");
				$objImport->_checkDate("debut");
				$objImport->_checkDate("fin");

				if($objImport->erreur()){

					$this->getSession ()->set ( 'flash_message', "Votre fichier contient des erreurs, merci de le corriger puis de le re-emporté" );
					$this->getSession ()->set ( 'flash_title', "L'import a échoué");
					$this->getSession ()->set ( 'flash_status', "danger" );
					$this->_view->set("listeImport", $objImport->listeImport());

				}else {

					$this->getSession ()->set ( 'flash_message', "Votre fichier a correctement été importé" );
					$this->getSession ()->set ( 'flash_title', "L'import a réussis");
					$this->getSession ()->set ( 'flash_status', "success" );
					$objImport->_replaceByAnId("intervenant_id", "nom", "id", "intervenant");
					$objImport->_replaceByAnId("us_id", "identification", "id", "us");
					$objImport->_replaceByAnId("type_id", "categorie_value", "id", "fsn_categorie", "intervention_type");

					$objImport->_delField("tmp_erreur");
					$objImport->_delField("tmp_id");
					$objImport->insertVraiTable();
					$this->forward ( 'Fsn_Historique', 'import' );
				}
			}else if ($module=="intervenant"){
				//on bloque 'laffichage du formulaire dans la vue
				$this->_view->set(("afficheForm"), FALSE);

				$objImport->setNameMdel("Model_Intervenant");
				$objImport->setTable("intervenant");
				//création de la table temporaire
				$user_session = $session->get("session") ;
				$objImport->createNameTableTmp($user_session["login"]);

				$objImport->importCSVTmp($importfile["tmp_name"],$firstlineheader);
				$objImport->_setUUID("id");

				$objImport->_checkNotNull("nom");

				$objImport->_checkStringExistEtranger("metier_id", "fsn_categorie", "categorie_value");
				$objImport->_checkStringExistEtranger("organisme_id", "fsn_organisme", "nom");

				if($objImport->erreur()){

					$this->getSession ()->set ( 'flash_message', "Votre fichier contient des erreurs, merci de le corriger puis de le re-emporté" );
					$this->getSession ()->set ( 'flash_title', "L'import a échoué");
					$this->getSession ()->set ( 'flash_status', "danger" );
					$this->_view->set("listeImport", $objImport->listeImport());

				}else {

					$this->getSession ()->set ( 'flash_message', "Votre fichier a correctement été importé" );
					$this->getSession ()->set ( 'flash_title', "L'import a réussis");
					$this->getSession ()->set ( 'flash_status', "success" );
					$objImport->_replaceByAnId("metier_id", "categorie_value", "id", "fsn_categorie", "intervenant_metier");
					$objImport->_replaceByAnId("organisme_id", "nom", "id", "fsn_organisme");

					$objImport->_delField("tmp_erreur");
					$objImport->_delField("tmp_id");
					$objImport->insertVraiTable();
					$this->forward ( 'Fsn_Historique', 'import' );
				}
			}else if ($module=="fai"){
				//on bloque 'laffichage du formulaire dans la vue
				$this->_view->set(("afficheForm"), FALSE);

				$objImport->setNameMdel("Model_Fai");
				$objImport->setTable("fai");
				//création de la table temporaire
				$user_session = $session->get("session") ;
				$objImport->createNameTableTmp($user_session["login"]);

				$objImport->importCSVTmp($importfile["tmp_name"],$firstlineheader);
				$objImport->_setUUID("id");
				$objImport->_checkStringExistEtranger("sitefouille_id", "sitefouille", "nomabrege");
				$objImport->_checkStringExistEtranger("typefai_id", "fsn_categorie", "categorie_value");

				if($objImport->erreur()){

					$this->getSession ()->set ( 'flash_message', "Votre fichier contient des erreurs, merci de le corriger puis de le re-emporté" );
					$this->getSession ()->set ( 'flash_title', "L'import a échoué");
					$this->getSession ()->set ( 'flash_status', "danger" );
					$this->_view->set("listeImport", $objImport->listeImport());

				}else {

					$this->getSession ()->set ( 'flash_message', "Votre fichier a correctement été importé" );
					$this->getSession ()->set ( 'flash_title', "L'import a réussis");
					$this->getSession ()->set ( 'flash_status', "success" );
					$objImport->_replaceByAnId("sitefouille_id", "nomabrege", "id", "sitefouille");
					$objImport->_replaceByAnId("typefai_id", "categorie_value", "id", "fsn_categorie", "fai_type");

					$objImport->_delField("tmp_erreur");
					$objImport->_delField("tmp_id");
					$objImport->insertVraiTable();
					$this->forward ( 'Fsn_Historique', 'import' );
				}
			}else if ($module=="elementrecueilli"){
				//on bloque 'laffichage du formulaire dans la vue
				$this->_view->set(("afficheForm"), FALSE);

				$objImport->setNameMdel("Model_Elementrecueilli");
				$objImport->setTable("elementrecueilli");
				//création de la table temporaire
				$user_session = $session->get("session") ;
				$objImport->createNameTableTmp($user_session["login"]);

				$objImport->importCSVTmp($importfile["tmp_name"],$firstlineheader);
				$objImport->_setUUID("id");
				$objImport->_checkStringExistEtranger("us_id", "us", "identification");
				$objImport->_checkStringExistEtranger("intervenant_id", "intervenant", "nom");
				$objImport->_checkStringExistEtranger("fonction_id", "fsn_categorie", "categorie_value");
				$objImport->_checkStringExistEtranger("classe_id", "fsn_categorie", "categorie_value");
				$objImport->_checkStringExistEtranger("etat_id", "fsn_categorie", "categorie_value");
				$objImport->_checkStringExistEtranger("emplacement_id", "emplacement", "nom");

				$user_session = $this->getSession()->get("session") ;
				$entiteadmin_id = $user_session["entiteadmin_id"] ;
				$objImport->_checkStringConteneur("conteneur_id", "conteneur", "nom", $entiteadmin_id);

				if($objImport->erreur()){

					$this->getSession ()->set ( 'flash_message', "Votre fichier contient des erreurs, merci de le corriger puis de le re-emporté" );
					$this->getSession ()->set ( 'flash_title', "L'import a échoué");
					$this->getSession ()->set ( 'flash_status', "danger" );
					$this->_view->set("listeImport", $objImport->listeImport());

				}else {

					$this->getSession ()->set ( 'flash_message', "Votre fichier a correctement été importé" );
					$this->getSession ()->set ( 'flash_title', "L'import a réussis");
					$this->getSession ()->set ( 'flash_status', "success" );
					$objImport->_replaceByAnId("us_id", "identification", "id", "us");
					$objImport->_replaceByAnId("intervenant_id", "nom", "id", "intervenant");
					$objImport->_replaceByAnId("fonction_id", "categorie_value", "id", "fsn_categorie", "elementrecueilli_fonction");
					$objImport->_replaceByAnId("classe_id", "categorie_value", "id", "fsn_categorie", "elementrecueilli_classe");
					$objImport->_replaceByAnId("etat_id", "categorie_value", "id", "fsn_categorie", "elementrecueilli_etat");
					$objImport->_replaceByAnId("statut_id", "categorie_value", "id", "fsn_categorie", "elementrecueilli_statut");
					$objImport->_replaceByAnId("emplacement_id", "nom", "id", "emplacement");
					$objImport->_replaceByAnId("elementpoterie_id", "categorie_value", "id", "fsn_categorie", "elementpoterie");
					$objImport->_replaceByAnIdConteneur("conteneur_id", "nom", "id", "conteneur", $entiteadmin_id);
					$objImport->_delField("tmp_erreur");
					$objImport->_delField("tmp_id");
					$objImport->insertVraiTable();
					$this->forward ( 'Fsn_Historique', 'import' );
				}

			}else if ($module =="ea"){
				//on bloque 'laffichage du formulaire dans la vue
				$this->_view->set(("afficheForm"), FALSE);

				$objImport->setNameMdel("Model_Fsn_Entiteadmin");
				$objImport->setTable("fsn_entiteadmin");
				//création de la table temporaire
				$user_session = $session->get("session") ;
				$objImport->createNameTableTmp($user_session["login"]);

				$objImport->importCSVTmp($importfile["tmp_name"],$firstlineheader);
				$objImport->_setUUID("id");

				$objImport->_checkStringExistEtranger("organisme_id", "fsn_organisme", "nom");

				if($objImport->erreur()){

					$this->getSession ()->set ( 'flash_message', "Votre fichier contient des erreurs, merci de le corriger puis de le re-emporté" );
					$this->getSession ()->set ( 'flash_title', "L'import a échoué");
					$this->getSession ()->set ( 'flash_status', "danger" );
					$this->_view->set("listeImport", $objImport->listeImport());

				}else {

					$this->getSession ()->set ( 'flash_message', "Votre fichier a correctement été importé" );
					$this->getSession ()->set ( 'flash_title', "L'import a réussis");
					$this->getSession ()->set ( 'flash_status', "success" );
					$objImport->_replaceByAnId("organisme_id", "nom", "id", "fsn_organisme");

					$objImport->_delField("tmp_erreur");
					$objImport->_delField("tmp_id");
					$objImport->insertVraiTable();
					$this->forward ( 'Fsn_Historique', 'import' );
				}
			}else if ($module=="document"){
				//on bloque 'laffichage du formulaire dans la vue
				$this->_view->set(("afficheForm"), FALSE);

				$objImport->setNameMdel("Model_Document");
				$objImport->setTable("document");
				//création de la table temporaire
				$user_session = $session->get("session") ;
				$objImport->createNameTableTmp($user_session["login"]);

				$objImport->importCSVTmp($importfile["tmp_name"],$firstlineheader);
				$objImport->_setUUID("id");

				if($objImport->erreur()){

					$this->getSession ()->set ( 'flash_message', "Votre fichier contient des erreurs, merci de le corriger puis de le re-emporté" );
					$this->getSession ()->set ( 'flash_title', "L'import a échoué");
					$this->getSession ()->set ( 'flash_status', "danger" );
					$this->_view->set("listeImport", $objImport->listeImport());

				}else {

					$this->getSession ()->set ( 'flash_message', "Votre fichier a correctement été importé" );
					$this->getSession ()->set ( 'flash_title', "L'import a réussis");
					$this->getSession ()->set ( 'flash_status', "success" );

					$objImport->_delField("tmp_erreur");
					$objImport->_delField("tmp_id");
					$objImport->insertVraiTable();
					$this->forward ( 'Fsn_Historique', 'import' );
				}
			}else if ($module=="usfai"){
				//on bloque 'laffichage du formulaire dans la vue
				$this->_view->set(("afficheForm"), FALSE);

				$objImport->setNameMdel("Model_Fai_Us");
				$objImport->setTable("fai_us");
				//création de la table temporaire
				$user_session = $this->getSession()->get("session") ;
				$objImport->createNameTableTmp($user_session["login"]);

				$objImport->importCSVTmp($importfile["tmp_name"],$firstlineheader);
				$objImport->_setUUID("id");

				$objImport->_checkStringExistEtranger("fai_id", "fai", "identification");
				$objImport->_checkStringExistEtranger("us_id", "us", "identification");

				if($objImport->erreur()){

					$this->getSession ()->set ( 'flash_message', "Votre fichier contient des erreurs, merci de le corriger puis de le re-emporté" );
					$this->getSession ()->set ( 'flash_title', "L'import a échoué");
					$this->getSession ()->set ( 'flash_status', "danger" );
					$this->_view->set("listeImport", $objImport->listeImport());

				}else {

					$this->getSession ()->set ( 'flash_message', "Votre fichier a correctement été importé" );
					$this->getSession ()->set ( 'flash_title', "L'import a réussis");
					$this->getSession ()->set ( 'flash_status', "success" );

					$objImport->_replaceByAnId("fai_id", "identification", "id", "fai");
					$objImport->_replaceByAnId("us_id", "identification", "id", "us");
					$objImport->_delField("tmp_erreur");
					$objImport->_delField("tmp_id");

					$objImport->insertVraiTable();
					$this->forward ( 'Fsn_Historique', 'import' );
				}

			}else if ($module=="superposition"){
				//on bloque 'laffichage du formulaire dans la vue
				$this->_view->set(("afficheForm"), FALSE);


				$objImport->setNameMdel("Model_Superposition");
				$objImport->setTable("superposition");
				//création de la table temporaire
				$user_session = $session->get("session") ;
				$objImport->createNameTableTmp($user_session["login"]);

				$objImport->importCSVTmp($importfile["tmp_name"],$firstlineheader);


				$objImport->_checkStringExistEtranger("anterieur_id", "us", "identification");
				$objImport->_checkStringExistEtranger("posterieur_id", "us", "identification");
				$objImport->_checkStringExistEtranger("sptype_id", "fsn_categorie", "categorie_value");

				if($objImport->erreur()){

					$this->getSession ()->set ( 'flash_message', "Votre fichier contient des erreurs, merci de le corriger puis de le re-emporté" );
					$this->getSession ()->set ( 'flash_title', "L'import a échoué");
					$this->getSession ()->set ( 'flash_status', "danger" );

					$this->_view->set("listeImport", $objImport->listeImport());


				}else {

					$this->getSession ()->set ( 'flash_message', "Votre fichier a correctement été importé" );
					$this->getSession ()->set ( 'flash_title', "L'import a réussis");
					$this->getSession ()->set ( 'flash_status', "success" );

					$objImport->_replaceByAnId("anterieur_id", "identification", "id", "us");
					$objImport->_replaceByAnId("posterieur_id", "identification", "id", "us");
					$objImport->_replaceByAnId("sptype_id", "categorie_value", "id", "fsn_categorie", "us_superposition_type");
					$objImport->_delField("tmp_erreur");
					$objImport->_delField("tmp_id");

					$objImport->insertVraiTable();
					$this->forward ( 'Fsn_Historique', 'import' );
				}
			}

		}

		$this->_view->set ( 'form', $form );
		$this->_view->set ( 'helper_form', new Yab_Helper_Form ( $form ) );

	}
	/**
	 * Fonction qui permet l'appel ajax au service web d'import
	 */
	public function actionAjaxImport() {

		// Adresse du serveur
		$webServiceAdress = 'http://'.Yab_Loader::getInstance()->getConfig()['ip'].':'.Yab_Loader::getInstance()->getConfig()['port'];
		
		// Url du web service d'import des donnees
		$url = $webServiceAdress.'/'.$_POST["module"].'/import/csv';
		
		// Header permettant d'envoyer les donnees multipart
		$header = array('Content-Type: multipart/form-data');
		
		// Repertoire temporaire de stockage du fichier
		$tmp_dir = dirname(YAB_PATH) . DIRECTORY_SEPARATOR .'tmp'.DIRECTORY_SEPARATOR.$_FILES['file']['name'];
		
		// On deplace le fichier temporaire dans le dossier tmp de yab
		move_uploaded_file($_FILES['file']['tmp_name'], $tmp_dir);
		
		// Envoie du fichier au web service d'import en curl
		$cfile = new CURLFile($tmp_dir,'text/csv',$tmp_dir);
		$data = array('file'=>$cfile);
		$resource = curl_init();
		curl_setopt($resource, CURLOPT_URL, $url);
		curl_setopt($resource, CURLOPT_HTTPHEADER, $header);
		curl_setopt($resource, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($resource, CURLOPT_POST, 1);
		curl_setopt($resource, CURLOPT_POSTFIELDS, $data);

		$result = curl_exec($resource);
		
		curl_close($resource);
		
		// Suppresion du fichier temporaire
		unlink($tmp_dir);
		
		// Envoie du resultat a l'ajax
		echo($result);
		die();
	}
}
