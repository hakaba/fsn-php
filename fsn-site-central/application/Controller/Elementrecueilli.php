<?php
class Controller_Elementrecueilli extends Yab_Controller_Action {
	public function filesUploadForm($name, $destination, $tabExtenssions = null) {
		if (isset ( $_POST ["btSubmit"] )) {
			foreach ( $_FILES [$name] ['error'] as $cle => $valeur ) {
				if (! is_uploaded_file ( $_FILES [$name] ['tmp_name'] [$cle] )) { // Vérifie que le fichier a été téléchargé par HTTP POST
					$error = "Downloading files does not succeed.";
				} else if ($valeur > 0) {
					$error = "Erreur lors du transfert du fichier : " . $_FILES ['photos'] ['tmp_name'] [$cle];
				}
			}
			if (! isset ( $error )) {
				foreach ( $_FILES [$name] ['error'] as $cle => $valeur ) {
					$extension_upload = strtolower ( substr ( strrchr ( $_FILES [$name] ['name'] [$cle], '.' ), 1 ) );
					if (($tabExtenssions != null and in_array ( $extension_upload, $tabExtenssions )) or $tabExtenssions == null) {
						$nom = $destination . "/" . $_FILES [$name] ['name'] [$cle] /*. $cle . ".{$extension_upload}"*/;
						$resultat = move_uploaded_file ( $_FILES [$name] ['tmp_name'] [$cle], $nom );
						if ($resultat) {
						}
					}
				}
			}
		}
	}
	public function listdir($dir, $format = null) {
		$list = array ();
		if (! file_exists ( $dir ))
			mkdir ( $dir );
		if ($dossier = opendir ( $dir )) {
			while ( false !== ($fichier = readdir ( $dossier )) ) {
				if ($fichier != '.' && $fichier != '..') {
					if ($format != null) {
						if (stripos ( $fichier, $format ))
							$list [] = $fichier;
					} else {
						$list [] = $fichier;
					}
				}
			}
			closedir ( $dossier );
		}
		return $list;
	}
	public function actionPhotoexportform() {
		$extensions_valides = array (
				'jpg',
				'jpeg',
				'gif',
				'png' 
		);
		$this->filesUploadForm ( "photos", "photo", $extensions_valides );
		$this->forward ( 'Elementrecueilli', 'index' );
	}

	public function actionGestionphoto() {
		$id = $this->_request->getParams ();
		$elementrecueilli = new Model_Elementrecueilli ( $id );
		$form = new Form_Elementrecueilli ( $elementrecueilli, new Model_Us($elementrecueilli->get('us_id')));
		$this->_view->set ( 'photogrammetries', $this->listdir ( "../photogrammetrie/elementrecueilli/" . $form->getElement ( 'numero' )->get ( 'value' ), "" ) );
		$this->_view->set ( 'helper_form', $form );
		$this->_view->set ( 'id', $id [0] );
	}
	
	public function actionGestionphotojson() {
		$id = $this->_request->getParams ();
		$elementrecueilli = new Model_Elementrecueilli ( $id );
		echo json_encode($this->listdir("../photogrammetrie/elementrecueilli/" . $elementrecueilli->get('numero'),""));
		die();
	}
	
	public function actionUnzip($id = null, $file = null) {
		$elementrecueilli = new Model_Elementrecueilli ( array($id) );
		$form = new Form_Elementrecueilli ( $elementrecueilli , new Model_Us($elementrecueilli->get('us_id')));
		$dir = "../photogrammetrie/elementrecueilli/" . $form->getElement ( 'numero' )->get ( 'value' ).'/'.$file;
		$models = array_merge($this->listdir($dir, '.wrl'), $this->listdir($dir, '.x3d'));
		$loader = Yab_Loader::getInstance();
		$config = $loader->getConfig();
		$this->_view->set ( 'nomZip', $file);
		$this->_view->set ( 'models', $models );
		$this->_view->set ( 'helper_form', $form );
		$this->_view->set ( 'id', $id);
	}
	
	public function actionUnzipjson($id = null, $file = null) {
		$elementrecueilli = new Model_Elementrecueilli ( array($id) );
		$dir = "../photogrammetrie/elementrecueilli/" . $elementrecueilli->get( 'numero' ).'/'.$file;
		$models = array_merge($this->listdir($dir, '.wrl'), $this->listdir($dir, '.x3d'));
		$loader = Yab_Loader::getInstance();
		$config = $loader->getConfig();
		echo json_encode($models);
		die();
	}
	
	public function actionModel($id = null, $file = null, $filename = null) {
		$loader = Yab_Loader::getInstance();
		$config = $loader->getConfig();
		ob_clean();
		$elementrecueilli = new Model_Elementrecueilli ( array($id) );
		$in = $config['localisation']."/photogrammetrie/elementrecueilli/" . $elementrecueilli->get('numero' ).'/'.$file.'/'.$filename;
		$ext = strtolower(pathinfo($in, PATHINFO_EXTENSION));
		if ($ext=='wrl' or $ext=='x3d') {
			$out = sys_get_temp_dir()."/".$elementrecueilli->get('numero' ).'-'.$file.'-'.$filename.'-'.time().'.html';
			exec("aopt -i '$in' -N '$out'");
			$content = file_get_contents($out);
			unlink($out);
			echo $content;
		} else
			header("location:/photogrammetrie/elementrecueilli/" . $form->getElement('numero')->get('value').'/'.$file.'/'.$filename);
			die();
	}
	
	public function actionDownloadmodel($id = null, $file = null) {
		$elementrecueilli = new Model_Elementrecueilli ( array($id) );
		$form = new Form_Elementrecueilli ( $elementrecueilli );
		$dir = "../photogrammetrie/elementrecueilli/" . $form->getElement ( 'numero' )->get ( 'value' ).'/'.$file;
		$tmp_file = sys_get_temp_dir()."/".$file.'-'.time().".zip";
		$zip = new ZipArchive();
		if ($zip->open($tmp_file, ZipArchive::CREATE) == TRUE) {
			$fichiers = scandir($dir.'/');
			unset($fichiers[0], $fichiers[1]);
			foreach($fichiers as $f) {
				if (!$zip->addFile($dir.'/'.$f, $f)) {
					die('Impossible d&#039;ajouter &quot;'.$f.'&quot;.<br/>');
				}
			}
			$zip->close();
		} else {
			die('Erreur, impossible de créer l&#039;archive.');
		}
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-type: application/octet-stream");
		header('Content-Disposition: attachment; filename="'.$file.'.zip"');
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ".filesize($tmp_file));
		ob_clean();
		$content = file_get_contents($tmp_file);
		unlink($tmp_file);
		echo $content;
		die();
	}
	
	public function actionExportmodeleform() {
		$elementrecueilli = new Model_Elementrecueilli ( $_POST ['id'] );
		$form = new Form_Elementrecueilli ( $elementrecueilli );
		$extensions_valides = array ('zip');
		$path = "../photogrammetrie/elementrecueilli/". $form->getElement ( 'numero' )->get ( 'value' );
		$this->filesUploadForm ( "photos", $path, $extensions_valides );
		$this->unzip($path);
		$this->forward ( 'Elementrecueilli', 'gestionphoto', array ($_POST ['id']));
	}
	
	public function actionUnlinkmodel($elementrecueilliid = null, $model = null) {
		$elementrecueilli = new Model_Elementrecueilli ( $elementrecueilliid );
		$form = new Form_Elementrecueilli ( $elementrecueilli );
		$this->rrmdir ( "../photogrammetrie/elementrecueilli/" . $form->getElement ( 'numero' )->get ( 'value' ) . "/" . $model );
		$this->forward ( 'Elementrecueilli', 'gestionphoto', array ($elementrecueilliid));
	}
	
	public function unzip($path) {
		$zips = $this->listdir($path, "zip");
		foreach ($zips as $z) {
			$zip = new ZipArchive();
			$zip_file = $path.'/'.$z;
			if ($zip->open($zip_file) === TRUE) {
				$zip->extractTo($path.'/'.basename($z, 'zip').'/');
				$zip->close();
				unlink($zip_file);
			} else {
				unlink($zip_file);
				die( "Échec de l'extraction de $z : fichier zip invalide.");
			}
		}
	}
	
	public function actionIndex() {
		$elementrecueilli = new Model_Elementrecueilli ();
		$us = new Model_Us ();
		
		$session = $this->getSession ();
		
		// Gestion des filtres avant la récupération des données depuis la base
		
		$filtres = array (
				1 => "Éléments isolés",
				2 => "Élément non-isolés",
				3 => "Éléments Poteries",
				4 => "Éléments non-poteries" 
		);
		
		// Site fouillé
		$sitefouille_id = $session->has ( 'sitefouille_id' ) ? $session->get ( 'sitefouille_id' ) : null;
		
		$uses = $us->getBySitefouilleAll ( $sitefouille_id );
		
		// US
		if (isset ( $_POST ["us_id"] )) {
			$us_id = $_POST ["us_id"];
			$session ["us_id"] = $us_id;
		} else if (isset ( $session ["us_id"] ))
			$us_id = $_SESSION ["us_id"];
		else
			$us_id = null;
			
			// Filtres par éléments
		if (isset ( $_POST ["filtre_el"] )) {
			$filtre_el = $_POST ["filtre_el"];
			$session ["filtre_el"] = $filtre_el;
		} else if (isset ( $session ["filtre_el"] ))
			$filtre_el = $_SESSION ["filtre_el"];
		else
			$filtre_el = null;
			
			// Meilleur gestion du tri par numéro (Les autres champs peuvent être trier par Yab)
		$tri = "numero";

		$requeteur = "requete_" . "elementrecueilli";
		$requeteurSession = ($session->has($requeteur) && !empty($session->get($requeteur))) ? $session->get($requeteur) : null;
		if (!is_null($requeteurSession) && !empty($requeteurSession)) {
			$posRequeteur = strpos($requeteurSession,"SELECT a1.id FROM ");
			if (($posRequeteur === false) || ($posRequeteur != 0)) {
				$this->getSession()->set('flash_title', '');
				$this->getSession()->set('flash_status', 'warning');
				$this->getSession()->set('flash_message', "Requête non conforme ! " . $requeteurSession);
				$session[$requeteur] = '';
			}
		}
		
		// Récupération des éléments recueillis
		$elementrecueillis = $elementrecueilli->getAll ( $sitefouille_id, $us_id, $filtre_el, $tri );

		$session[$requeteur] = '';
		
		$this->_view->set ( 'uses', $uses );
		$this->_view->set ( 'filtres', $filtres );
		$this->_view->set ( 'sitefouille_id', $sitefouille_id );
		$this->_view->set ( 'us_id', $us_id );
		$this->_view->set ( 'filtre_el', $filtre_el );
		$this->_view->set ( 'elementrecueillis', $elementrecueillis );
		
		$loader = Yab_Loader::getInstance ();
		$config = $loader->getConfig ();
		$this->_view->set ( 'config', $config );
	}
	
	// Action de mise à jour de la liste des traitements après selection de l'ER en association
	public function actionUpdateListTraitement() {
		$this->getLayout ()->setEnabled ( false );
		$elementrecueilli = new Model_Elementrecueilli ();
		
		$idER_CURRENT = $_GET ["ider_current"];
		$idER_ASSOCIE = $_GET ["ider_associe"];
		
		$traitements = $elementrecueilli->getABothTraitements ( $idER_CURRENT, $idER_ASSOCIE );
		
		$this->_view->set ( 'traitements', $traitements );
		$this->_view->setFile ( 'View/association/er/updateListTraitement.html' );
	}
	public function actionAdd() {
		$idUS = isset ( $_GET ["idUS"] ) ? $_GET ["idUS"] : null;
		
		$elementrecueilli = new Model_Elementrecueilli ();
		$us = new Model_Us ( $idUS );
		$form = new Form_Elementrecueilli ( $elementrecueilli, $us );
		
		$elementpoterie = new Model_Elementpoterie ();
		$form_poterie = new Form_Elementpoterie ( $elementpoterie );
		
		$er_nm = new Model_Er_Naturematiere ();
		$form_nm = new Form_Er_Naturematiere ( array () );
		
		if ($form->isSubmitted ()) {
			
			$form_poterie = $this->setPrecisionProduction ( $form_poterie );
			
			$form_nm = $this->setNatureMatiere ( $form_nm );
			
			$er_valid = false;
			$poterie_valid = null;
			
			$generateGuuid = new Plugin_Guuid ();
			$guuid = $generateGuuid->GetUUID ();
			
			// test de validation et retrait des données absentes de la table elementrecueilli
			if ($form->isValid ()) {
				$er_valid = true;
				$insert_values = $form->getValues ();
				unset ( $insert_values ['us_identification'] );
				unset ( $insert_values ['numero_isole'] );
				unset ( $insert_values ['numero_non_isole'] );
				unset ( $insert_values ['emplacement_choisi'] );
				
				$insert_values ['id'] = $guuid;
			}
			
			$class = $form->getElement ( 'classe_id' )->get ( 'value' );
			
			// traitement relatif aux éléments de classe poterie
			if (! empty ( $class )) {
				if ($this->isPoterie ( $class ) == 1) {
					if ($form_poterie->isValid ()) {
						$poterie_valid = true;
						$poterie_values = $form_poterie->getValues ();
						$poterie_values ['id'] = $guuid;
						$insert_values ['elementpoterie_id'] = $guuid;
					} else {
						$poterie_valid = false;
					}
				}
			}
			
			// insertion et historisation des données
			if ($er_valid && $poterie_valid === null) {
				$transformDateFrUs = new Plugin_DateFrUs ();
				$insert_values ['datedecouverte'] = $transformDateFrUs->convertitDateFRUs ( $insert_values ['datedecouverte'] );
				$insert_values ['elementpoterie_id'] = null;
				$insert_values = $this->setNullValues ( $insert_values );
				$elementrecueilli->populate ( $insert_values )->save ();
				$this->saveNatureMatiere ( $form_nm, $guuid, $generateGuuid );
				
				$historisation = new Plugin_Historique ();
				$formvalue = $form->getTypedValues ( $insert_values );
				$historisation->addhistory ( 'elementrecueilli', self::MODE_CREATE, $formvalue );
			} else if ($er_valid && $poterie_valid) {
				$transformDateFrUs = new Plugin_DateFrUs ();
				$insert_values ['datedecouverte'] = $transformDateFrUs->convertitDateFRUs ( $insert_values ['datedecouverte'] );
				$insert_values ['elementpoterie_id'] = $guuid;
				$insert_values = $this->setNullValues ( $insert_values );
				$poterie_values = $this->setNullValues ( $poterie_values );
				$elementpoterie->populate ( $poterie_values )->save ();
				$elementrecueilli->populate ( $insert_values )->save ();
				$this->saveNatureMatiere ( $form_nm, $guuid, $generateGuuid );
				
				$historisation = new Plugin_Historique ();
				$formvalue_er = $form->getTypedValues ( $insert_values );
				$formvalue_er ['elementpoterie_id'] = $guuid;
				$formvalue_poterie = $form_poterie->getTypedValues ( $poterie_values );
				$formvalue_er += $formvalue_poterie;
				$historisation->addhistory ( 'elementrecueilli', self::MODE_CREATE, $formvalue_er );
				
				// $historisation->addhistory('Elementpoterie', self::MODE_CREATE, $formvalue);
			} else {
				$this->getSession ()->set ( 'flash_status', 'warning' );
				$this->getSession ()->setFlashErrors ( $form->getErrors () );
				if ($poterie_valid === false) {
					$this->getSession ()->setFlashErrors ( $form_poterie->getErrors () );
					if (! $form_poterie->getElement ( 'proportionconserve' )->isValid ())
						$this->getSession ()->setFlashErrors ( array (
								'proportionconserve' => 'Le champ proportion conservée doit être un nombre décimal' 
						) );
				}
			}
			
			// traitement après validations
			if ($er_valid && ($poterie_valid === null || $poterie_valid === true)) {
				
				// Debut création arborescence multimédia
				// Recuperation nomabrege de l'organisme
				$user_session = $this->getSession ()->get ( 'session' );
				$entiteadmin_id = $user_session ['entiteadmin_id'];
				
				$organismes = $elementrecueilli->getEntiteOrganisme ()->where ( 'enti.id="' . $entiteadmin_id . '" ' );
				$nb_row = $organismes->count ();
				
				if ($nb_row > 0) {
					foreach ( $organismes as $organisme ) {
						$arborescence = '../mediatheque/albums/' . $organisme ['nomabrege_organisme'] . '/elementrecueilli/' . $insert_values ['numero'];
					}
					
					if (! is_dir ( $arborescence )) {
						mkdir ( $arborescence, 0777, true );
					}
				}
				// Fin création arborescence multimédia
				
				$this->getSession ()->set ( 'flash', 'elementrecueilli added' );
				
				$this->forward ( 'Elementrecueilli', 'index' );
			}
		}
		
		$this->_view->set ( 'helper_form', new Yab_Helper_Form ( $form ) );
		$this->_view->set ( 'form', $form );
		$this->_view->set ( 'form_poterie', $form_poterie );
		$this->_view->set ( 'form_nm', $form_nm );
		
		$registry = $this->getRegistry ();
		$this->getLayout ()->setEnabled ( true );
		
		$this->_view->set ( 'registry', $registry );
		$this->_view->set ( 'i18n', $registry->get ( 'i18n' ) );
		$this->_view->set ( 'filter_html', new Yab_Filter_Html () );
		$this->_view->set ( 'filter_no_html', new Yab_Filter_NoHtml () );
		$this->_view->set ( 'form_errors', $form->getErrors () );
		$this->_view->set('mode', "add");
	}
	
	// insertion de naturematiere et historisation
	public function saveNatureMatiere($form_nm, $guuid, $generateurId) {
		$nm = $form_nm->getElement ( 'naturematiere_list' )->get ( 'value' );
		$nm_principal = $form_nm->getElement ( 'naturematiere_principal' )->get ( 'value' );
		if (! empty ( $nm )) {
			foreach ( $nm as $cle => $value ) {
				$model = new Model_Er_Naturematiere ();
				$insert_nm = array ();
				$insert_nm ['er_id'] = $guuid;
				$insert_nm ['naturematiere_id'] = ( int ) $value; // jfb 2016-06-02 mantis 291
				$insert_nm ['principal'] = $nm_principal == $value ? 1 : 0;
				$insert_nm ['id'] = $generateurId->GetUUID ();
				$model->populate ( $insert_nm )->save ();
				$historisation = new Plugin_Historique ();
				$formvalue = $form_nm->getTypedValues ( $insert_nm );
				$historisation->addhistory ( 'Er_Naturematiere', self::MODE_CREATE, $formvalue );
			}
		}
	}
	
	// on met null à la place de toutes les valeurs vides sauf 0
	public function setNullValues($values) {
		foreach ( $values as $key => $value ) {
			if (empty ( $value ) and $value !== 0)
				$values [$key] = null;
		}
		return $values;
	}
	
	// recupération des précisions production correspondant ç ka production sélectionnées
	public function setPrecisionProduction($form) {
		$relations = new Model_Fsn_Categorie_Relation ();
		$form->getElement ( 'precisionproduction_id' )->set ( 'fake_options', array (
				null => 'Non déterminé' 
		) );
		$form->getElement ( 'precisionproduction_id' )->set ( 'options', $relations->getListRelations ()->where ( 'fcb.id = "' . $form->getElement ( 'production_id' )->get ( 'value' ) . '" ' )->setKey ( 'element_a_id' )->setValue ( 'element_a_value' ) );
		return $form;
	}
	
	// recupération de la liste des naturematiere prépondérantes à partir du post
	public function setNatureMatiere($form) {
		if (isset ( $_POST ['naturematiere_list'] ) and ! empty ( $_POST ['naturematiere_list'] )) {
			$where = '';
			$compt = 0;
			
			foreach ( $_POST ['naturematiere_list'] as $cle => $value ) {
				if ($compt > 0)
					$where .= ' or ';
				$where .= 'id = "' . $value . '"';
				$compt ++;
			}
			
			$item = new Model_Er_Naturematiere ();
			
			$form->getElement ( 'naturematiere_principal' )->set ( 'options', $item->getTable ( 'Model_Naturematiere' )->fetchAll ()->where ( $where )->setKey ( 'id' )->setValue ( 'naturematiere' ) );
		}
		return $form;
	}
	public function actionAjaxIsPoterie() {
		$this->getLayout ()->setEnabled ( false );
		$this->_view->setEnabled ( false );
		$id = null;
		if (isset ( $_POST ['id'] ))
			$id = $_POST ['id'];
		else if (isset ( $_GET ['id'] ))
			$id = $_GET ['id'];
		echo $this->isPoterie ( $id );
	}
	
	// dit si une classe est poterie
	public function isPoterie($id) {
		$classe = new Model_Fsn_Categorie ( $id );
		
		$poterie = 'p_poterie_de_terre';
		$result = 0;
		if ($classe->has ( 'categorie_key' ) and $classe->get ( 'categorie_key' ) == $poterie) {
			$result = 1;
		}
		return $result;
	}
	
	// retourne la liste de précisionsproduction de la production donnée en appel ajax
	public function actionAjaxGetPrecisionProduction() {
		$this->getLayout ()->setEnabled ( false );
		$this->_view->setEnabled ( false );
		
		$id = null;
		if (isset ( $_POST ['id'] ))
			$id = $_POST ['id'];
		else if (isset ( $_GET ['id'] ))
			$id = $_GET ['id'];
		
		$relations = new Model_Fsn_Categorie_Relation ();
		$result = $relations->getListRelations ()->where ( 'fcb.id = "' . $id . '"' );
		$select = '<select name="precisionproduction_id" id="precisionproduction_id">';
		$select .= '<option value="" >Non déterminé</option>';
		
		foreach ( $result as $key => $value ) {
			$select .= '<option value="' . $value ["element_a_id"] . '" >' . $value ["element_a_value"] . ' </option>';
		}
		
		$select .= '</select>';
		echo $select;
	}
	public function actionEdit() {
		$elementrecueilli = new Model_Elementrecueilli ( $this->_request->getParams () );
		$us = new Model_Us ( $elementrecueilli->get ( 'us_id' ) );
		$form = new Form_Elementrecueilli ( $elementrecueilli, $us );
		
		if ($this->isPoterie ( $elementrecueilli->get ( 'classe_id' ) )) {
			$elementpoterie = new Model_Elementpoterie ( $elementrecueilli->get ( 'id' ) );
			$form_poterie = new Form_Elementpoterie ( $elementpoterie, $mode = 'add' );
		} else {
			$elementpoterie = new Model_Elementpoterie ();
			$form_poterie = new Form_Elementpoterie ( $elementpoterie, $mode = 'add' );
		}
		
		$er_nm = new Model_Er_Naturematiere ();
		$er_nm = $er_nm->fetchAll ()->where ( 'er_id="' . $elementrecueilli->get ( 'id' ) . '"' );
		
		// liste naturematiere de elementrecueilli en cours
		$er_nm_id = null;
		// naturematiere principale de elementrecueilli en cours
		$principal = null;
		// liste des id des association naturematiere et elementrecueilli en cours
		$all_id = null;
		
		foreach ( $er_nm as $key => $value ) {
			$er_nm_id [] = ($value ['naturematiere_id']);
			$all_id [] = $value ['id'];
			if ($value ['principal'] == 1)
				$principal = $value ['naturematiere_id'];
		}
		
		$form_nm = new Form_Er_Naturematiere ( $er_nm_id, $principal );
		$this->_view->set('es_id', $elementrecueilli->get('id'));
		
		if ($form->isSubmitted ()) {
			
			$form_poterie = $this->setPrecisionProduction ( $form_poterie );
			
			$form_nm = $this->setNatureMatiere ( $form_nm );
			$er_valid = false;
			$poterie_valid = null;
			
			$generateGuuid = new Plugin_Guuid ();
			
			// test de validité et épuration des données
			if ($form->isValid ()) {
				
				$er_valid = true;
				$insert_values = $form->getValues ();
				$insert_values ['id'] = $elementrecueilli->get ( 'id' );
				unset ( $insert_values ['us_identification'] );
				unset ( $insert_values ['numero_isole'] );
				unset ( $insert_values ['numero_non_isole'] );
				unset ( $insert_values ['emplacement_choisi'] );
			}
			
			$class = $form->getElement ( 'classe_id' )->get ( 'value' );
			
			// test pour la classe poterie
			if (! empty ( $class )) {
				if ($this->isPoterie ( $class ) == 1) {
					if ($form_poterie->isValid ()) {
						$poterie_valid = true;
						$poterie_values = $form_poterie->getValues ();
						$poterie_values ['id'] = $elementrecueilli->get ( 'id' );
						$insert_values ['elementpoterie_id'] = $elementrecueilli->get ( 'id' );
					} else {
						$poterie_valid = false;
					}
				}
			}
			
			// insertion des données et historisations
			if ($er_valid && $poterie_valid === null) {
				
				$transformDateFrUs = new Plugin_DateFrUs ();
				$insert_values ['datedecouverte'] = $transformDateFrUs->convertitDateFRUs ( $insert_values ['datedecouverte'] );
				$insert_values ['elementpoterie_id'] = null;
				
				$insert_values = $this->setNullValues ( $insert_values );
				
				$elementrecueilli->populate ( $insert_values )->save ();
				
				$historisation = new Plugin_Historique ();
				
				$formvalue_er = $form->getTypedValues ( $insert_values );
				$formvalue_er ['elementpoterie_id'] = null;
				$historisation->addhistory ( 'elementrecueilli', self::MODE_UPDATE, $formvalue_er );
				
				if ($elementpoterie->has ( 'id' ))
					$elementpoterie->delete ();
				
				if (! empty ( $er_nm_id )) {
					$this->deleteNatureMatiere ( $all_id );
					$er = new Model_Elementrecueilli ();
					$er->deleteFromErNM ( $elementrecueilli->get ( 'id' ) );
				}
				
				$this->saveNatureMatiere ( $form_nm, $elementrecueilli->get ( 'id' ), $generateGuuid );
			} else if ($er_valid && $poterie_valid) {
				
				$transformDateFrUs = new Plugin_DateFrUs ();
				$insert_values ['datedecouverte'] = $transformDateFrUs->convertitDateFRUs ( $insert_values ['datedecouverte'] );
				$insert_values ['elementpoterie_id'] = $elementrecueilli->get ( 'id' );
				$insert_values = $this->setNullValues ( $insert_values );
				
				if (! empty ( $er_nm_id )) {
					$this->deleteNatureMatiere ( $all_id );
					$er = new Model_Elementrecueilli ();
					$er->deleteFromErNM ( $elementrecueilli->get ( 'id' ) );
				}
				
				$poterie_values = $this->setNullValues ( $poterie_values );
				$elementpoterie->populate ( $poterie_values )->save ();
				$elementrecueilli->populate ( $insert_values )->save ();
				
				$historisation = new Plugin_Historique ();
				$formvalue_er = $form->getTypedValues ( $insert_values );
				$formvalue_er ['elementpoterie_id'] = $elementrecueilli->get ( 'id' );
				$formvalue_poterie = $form_poterie->getTypedValues ( $poterie_values );
				$formvalue_er += $formvalue_poterie;
				$historisation->addhistory ( 'elementrecueilli', self::MODE_UPDATE, $formvalue_er );
				
				$this->saveNatureMatiere ( $form_nm, $elementrecueilli->get ( 'id' ), $generateGuuid );
			} else {
				
				$this->getSession ()->set ( 'flash_status', 'warning' );
				$this->getSession ()->setFlashErrors ( $form->getErrors () );
				if ($poterie_valid === false) {
					$this->getSession ()->setFlashErrors ( $form_poterie->getErrors () );
					if (! $form_poterie->getElement ( 'proportionconserve' )->isValid ())
						$this->getSession ()->setFlashErrors ( array (
								'proportionconserve' => 'Le champ proportion conservée doit être un nombre décimal' 
						) );
				}
			}
			
			// traitement après validation
			if ($er_valid && ($poterie_valid === null || $poterie_valid === true)) {
				
				// Debut création arborescence multimédia
				// Recuperation nomabrege de l'organisme
				$user_session = $this->getSession ()->get ( 'session' );
				$entiteadmin_id = $user_session ['entiteadmin_id'];
				
				$organismes = $elementrecueilli->getEntiteOrganisme ()->where ( 'enti.id="' . $entiteadmin_id . '" ' );
				$nb_row = $organismes->count ();
				
				if ($nb_row > 0) {
					foreach ( $organismes as $organisme ) {
						$arborescence = '../mediatheque/albums/' . $organisme ['nomabrege_organisme'] . '/elementrecueilli/' . $insert_values ['numero'];
					}
					
					if (! is_dir ( $arborescence )) {
						mkdir ( $arborescence, 0777, true );
					}
				}
				// Fin création arborescence multimédia
				
				$this->getSession ()->set ( 'flash', 'elementrecueilli as been added' );
				
				$this->forward ( 'Elementrecueilli', 'index' );
			}
		}
		
		$this->_view->set ( 'helper_form', new Yab_Helper_Form ( $form ) );
		$this->_view->set ( 'form', $form );
		$this->_view->set ( 'form_poterie', $form_poterie );
		$this->_view->set ( 'form_nm', $form_nm );
		
		$registry = $this->getRegistry ();
		$this->getLayout ()->setEnabled ( true );
		
		$this->_view->set ( 'registry', $registry );
		$this->_view->set ( 'i18n', $registry->get ( 'i18n' ) );
		$this->_view->set ( 'filter_html', new Yab_Filter_Html () );
		$this->_view->set ( 'filter_no_html', new Yab_Filter_NoHtml () );
		$this->_view->set ( 'form_errors', $form->getErrors () );
		$this->_view->set('mode', 'edit');
		$this->_view->set('er_id', $elementrecueilli->get('id'));
		
		$this->_view->setFile ( 'View/elementrecueilli/add.html' );
	}
	private function deleteNatureMatiere($all_id) {
		foreach ( $all_id as $key => $value ) {
			
			$model = new Model_Er_Naturematiere ( $value );
			$insert_nm ['er_id'] = $model->has ( 'er_id' ) ? $model->get ( 'er_id' ) : null;
			$insert_nm ['naturematiere_id'] = $model->has ( 'naturematiere_id' ) ? ( int ) $model->get ( 'er_id' ) : null;
			$insert_nm ['principal'] = $model->has ( 'principal' ) ? ( bool ) $model->get ( 'principal' ) : 0;
			$insert_nm ['id'] = $model->has ( 'id' ) ? $model->get ( 'id' ) : null;
			
			$historisation = new Plugin_Historique ();
			$historisation->addhistory ( 'Er_Naturematiere', self::MODE_DELETE, $insert_nm );
		}
	}
	public function actionDelete() {
		$elementrecueilli = new Model_Elementrecueilli ( $this->_request->getParams () );
		$us = new Model_Us ( $elementrecueilli->get ( 'us_id' ) );
		
		$form = new Form_Elementrecueilli ( $elementrecueilli, $us, $mode = 'delete' );
		$formvalue = $form->getValues ();
		
		$formvalue ['id'] = $elementrecueilli->get ( 'id' );
		unset ( $formvalue ['us_identification'] );
		unset ( $formvalue ['numero_isole'] );
		unset ( $formvalue ['numero_non_isole'] );
		unset ( $formvalue ['emplacement_choisi'] );
		
		$poterie_values = array ();
		
		// test pour la classe poterie
		if ($this->isPoterie ( $elementrecueilli->get ( 'classe_id' ) )) {
			
			$elementpoterie = new Model_Elementpoterie ( $elementrecueilli->get ( 'id' ) );
			$form_poterie = new Form_Elementpoterie ( $elementpoterie, $mode = '' );
			
			$poterie_values = $form_poterie->getValues ();
			
			$poterie_values ['id'] = $elementpoterie->get ( 'id' );
		} else {
			$elementpoterie = new Model_Elementpoterie ();
			$form_poterie = new Form_Elementpoterie ( $elementpoterie, $mode = 'add' );
		}
		
		$message = '';
		
		try {
			$elementrecueilli->delete ();
			
			$historisation = new Plugin_Historique ();
			$formvalue_er = $form->getTypedValues ( $formvalue );
			
			if ($elementpoterie->has ( 'id' )) {
				
				// $elementpoterie->delete();
				
				$formvalue_poterie = $form_poterie->getTypedValues ( $poterie_values );
				$formvalue_er += $formvalue_poterie;
			}
			
			$historisation->addhistory ( 'elementrecueilli', self::MODE_DELETE, $formvalue_er );
			
			$message = 'elementrecueilli supprimé';
		} catch ( Exception $e ) {
			$message = 'élément recueilli non supprimé';
		}
		
		$this->getSession ()->set ( 'flash_title', '' );
		$this->getSession ()->set ( 'flash_status', 'info' );
		$this->getSession ()->set ( 'flash_message', $message );
		
		$this->forward ( 'Elementrecueilli', 'index' );
	}
	public function actionGenererQRCode() {
		$elementrecueilli = new Model_Elementrecueilli ();
		
		$loader = Yab_Loader::getInstance ();
		$config = $loader->getConfig ();
		$url = "http://" . $config ["ip"] . ":" . $config ["port"] . $config ["context"] . "/elementrecueilli/code";
		
		$where = "";
		for($i = 0; $i < count ( $_POST ["qrcodes"] ); $i ++) {
			if ($i == 0)
				$where .= "id = '" . $_POST ["qrcodes"] [$i] . "'";
			else
				$where .= " || id = '" . $_POST ["qrcodes"] [$i] . "'";
		}
		
		$ers = $elementrecueilli->fetchAll ()->where ( $where )->toArray ();
		
		$this->getLayout ()->setEnabled ( false );
		$this->_view->set ( "url", $url );
		$this->_view->set ( "ers", $ers );
	}
	public function actionShow() {
		$elementrecueilli = new Model_Elementrecueilli ( $this->_request->getParams () );
		$us = new Model_Us ( $elementrecueilli->get ( 'us_id' ) );
		$form = new Form_Elementrecueilli ( $elementrecueilli, $us, $mode = 'show' );
		$formvalue = $form->getValues ();
		
		$idER = $elementrecueilli->get ( 'id' );
		$er = new Model_Elementrecueilli ();
		// $traitements = $er->getTraitements($idER) ;
		$associations = $er->getAssociations ( $idER )->toArray ();
		
		$traitement = new Model_Traitement ();
		$_SESSION ['idER2'] = $idER;
		$traitements = $traitement->getTraitementDetail ()->toArray ();
		
		if ($this->isPoterie ( $elementrecueilli->get ( 'classe_id' ) )) {
			$elementpoterie = new Model_Elementpoterie ( $elementrecueilli->get ( 'id' ) );
			$form_poterie = new Form_Elementpoterie ( $elementpoterie, $mode = 'show' );
		} else {
			$elementpoterie = new Model_Elementpoterie ();
			$form_poterie = new Form_Elementpoterie ( $elementpoterie, $mode = 'show' );
		}
		
		$er_nm = new Model_Er_Naturematiere ();
		$er_nm = $er_nm->fetchAll ()->where ( 'er_id="' . $elementrecueilli->get ( 'id' ) . '"' );
		
		// liste naturematiere de elementrecueilli en cours
		$er_nm_id = null;
		// naturematiere principale de elementrecueilli en cours
		$principal = null;
		
		foreach ( $er_nm as $key => $value ) {
			$er_nm_id [] = ($value ['naturematiere_id']);
			if ($value ['principal'] == 1)
				$principal = $value ['naturematiere_id'];
		}
		
		$form_nm = new Form_Er_Naturematiere ( $er_nm_id, $principal, $mode = 'show' );
		
		// Recuperation nomabrege de l'organisme
		$user_session = $this->getSession ()->get ( 'session' );
		$entiteadmin_id = $user_session ['entiteadmin_id'];
		
		$organismes = $elementrecueilli->getEntiteOrganisme ()->where ( 'enti.id="' . $entiteadmin_id . '" ' );
		$nb_row = $organismes->count ();
		
		if ($nb_row > 0) {
			foreach ( $organismes as $organisme ) {
				$codeorganisme = $organisme ['nomabrege_organisme'];
			}
		} else
			$codeorganisme = "UASD";
			
			// Recupération arborescence pour affichage images de l'arborescence multimédia
		$arborescence = "../mediatheque/albums/" . $codeorganisme . "/elementrecueilli/" . $formvalue ['numero'] . "/*.{jpg,jpeg,gif,png,PNG}";
		$repertoire = "../mediatheque/albums/" . $codeorganisme . "/elementrecueilli/" . $formvalue ['numero'] . "/";
		$files = glob ( $arborescence, GLOB_BRACE );
		
		$this->_view->set ( 'helper_form', new Yab_Helper_Form ( $form ) );
		$this->_view->set ( 'form', $form );
		$this->_view->set ( 'form_poterie', $form_poterie );
		$this->_view->set ( 'form_nm', $form_nm );
		
		$registry = $this->getRegistry ();
		$this->getLayout ()->setEnabled ( true );
		
		$this->_view->set ( 'registry', $registry );
		$this->_view->set ( 'i18n', $registry->get ( 'i18n' ) );
		$this->_view->set ( 'filter_html', new Yab_Filter_Html () );
		$this->_view->set ( 'filter_no_html', new Yab_Filter_NoHtml () );
		$this->_view->set ( 'form_errors', $form->getErrors () );
		$this->_view->set ( 'files', $files );
		$this->_view->set ( 'repertoire', $repertoire );
		$this->_view->set ( 'traitements', $traitements );
		$this->_view->set ( 'associations', $associations );
		$this->_view->set ( 'er', $er );
		$this->_view->set('er_id', $elementrecueilli->get('id'));
		$this->_view->set('mode', $mode);
		
		// $this->_view->set('codesite', $formvalue['numero']);
		// $this->_view->set('codeorganisme', $codeorganisme);
		
		$this->_view->setFile ( 'View/elementrecueilli/add.html' );
		$this->_view->set ( 'photogrammetries', $this->listdir ( "photogrammetries" ) );
	}
	public function actionListImport() {
		$elementrecueilli = new Model_Elementrecueilli ();
		
		$elementrecueillis = $elementrecueilli->fetchAll ();
		
		$this->_view->set ( 'elementrecueillis', $elementrecueillis );
	}
	public function actionTest() {
		// $this->getLayout()->setFile("View/layout_fsn.html") ;
		$this->_view->setFile ( 'View/elementrecueilli/test.php' );
		$this->getSession ()->set ( "titre", "test" );
	}
	public function actionAjaxAssocier() {
		$elementrecueilli = new Model_Elementrecueilli ();
		$association_er = new Model_Association_Er ();
		$form_allAssociations = new Form_Association_Er ( $association_er );
		$this->getLayout ()->setEnabled ( false );
		
		$idER = $_POST ["idER"];
		$numeroER = $_POST ["numeroER"];
		
		$er = new Model_Elementrecueilli ( $idER );
		$us = new Model_Us ( $er->get ( 'us_id' ) );
		$idUS = $us->get ( 'id' );
		$identificationUS = $us->get ( 'identification' );
		
		$typeasso_id = (isset ( $_POST ["typeasso_id"] )) ? $_POST ["typeasso_id"] : "";
		$traitement_id = $_POST ["traitement_id"];
		$action = $_POST ["action"];
		
		$types = $elementrecueilli->getTypesAssociations ();
		$traitements = $elementrecueilli->getTraitements ( $idER );
		
		$historisation = new Plugin_Historique ();
		
		if ($action == "ajouter") {
			$insertion = array ();
			$generateGuuid = new Plugin_Guuid ();
			$guuid = $generateGuuid->GetUUID ();
			
			$insertion ["id"] = $guuid;
			$insertion ["er_ancien_id"] = $_POST ["er_ancien_id"];
			$insertion ["er_nouveau_id"] = $idER;
			$insertion ["typeasso_id"] = $typeasso_id;
			$insertion ["traitement_id"] = $_POST ["traitement_id"];
			$insertion ["commentaire"] = $_POST ["association_commentaire"];
			
			$association_er->populate ( $insertion )->save ();
			
			$formvalue = $form_allAssociations->getTypedValues ( $insertion );
			$historisation->addhistory ( 'association_er', self::MODE_CREATE, $formvalue );
		} else if ($action == "modifier") {
			$er_ancien_id = $_POST ["er_ancien_id"];
			$er_nouveau_id = $_POST ["er_nouveau_id"];
			
			// Récupération id de l'association_er à modifier et selection de sa ligne en BDD
			$associatER = $association_er->fetchAll ()->where ( "er_ancien_id = '" . $er_ancien_id . "' AND er_nouveau_id = '" . $er_nouveau_id . "' " )->toRow ();
			$model_associatER = new Model_Association_Er ( $associatER->get ( 'id' ) );
			$form_oneAssociation = new Form_Association_Er ( $model_associatER );
			
			$update = array ();
			$update ["id"] = $model_associatER->get ( 'id' );
			$update ["er_ancien_id"] = $er_ancien_id;
			$update ["er_nouveau_id"] = $er_nouveau_id;
			$update ["typeasso_id"] = $associatER->get ( 'typeasso_id' );
			$update ["traitement_id"] = $associatER->get ( 'traitement_id' );
			$update ["commentaire"] = $_POST ["association_commentaire"];
			
			$model_associatER->populate ( $update )->save ();
			
			$formvalue = $form_oneAssociation->getTypedValues ( $update );
			$historisation->addhistory ( 'association_er', self::MODE_UPDATE, $formvalue );
		} else if ($action == "supprimer") {
			$er_ancien_id = $_POST ["er_ancien_id"];
			$er_nouveau_id = $_POST ["er_nouveau_id"];
			
			// Récupération id de l'association_er à modifier et selection de sa ligne en BDD
			$associatER = $association_er->fetchAll ()->where ( "er_ancien_id = '" . $er_ancien_id . "' AND er_nouveau_id = '" . $er_nouveau_id . "' " )->toRow ();
			$model_associatER = new Model_Association_Er ( $associatER->get ( 'id' ) );
			$form_oneAssociation = new Form_Association_Er ( $model_associatER );
			
			$association_er->delete ( "er_ancien_id = '" . $er_ancien_id . "' AND er_nouveau_id = '" . $er_nouveau_id . "'" );
			
			$formvalue = $form_oneAssociation->getValues ();
			$formvalue ['id'] = $model_associatER->get ( 'id' );
			$formvalue ["er_ancien_id"] = $er_ancien_id;
			$formvalue ["er_nouveau_id"] = $er_nouveau_id;
			$formvalue ["typeasso_id"] = $associatER->get ( 'typeasso_id' );
			$formvalue ["traitement_id"] = $associatER->get ( 'traitement_id' );
			$formvalue ["commentaire"] = $associatER->get ( 'commentaire' );
			
			$formvalue = $form_oneAssociation->getTypedValues ( $formvalue );
			$historisation->addhistory ( 'association_er', self::MODE_DELETE, $formvalue );
		}
		
		$associations = $elementrecueilli->getAssociations ( $idER );
		
		/*
		 * jfb 2016-06-02 mantis 272 début
		 * $estAssocie = $elementrecueilli->estAssocie($idER) ;
		 * // jfb 2016-06-02 mantis 272 fin
		 */
		$associationsInverses = array ();
		/*
		 * jfb 2016-06-02 mantis 272 début
		 * if (count($estAssocie) != 0)
		 * $associationsInverses = $elementrecueilli->getAssociations($estAssocie["er_nouveau_id"]) ;
		 * // jfb 2016-06-02 mantis 272 fin
		 */
		
		$this->_view->set ( "associations", $associations );
		$this->_view->set ( "associationsInverses", $associationsInverses );
		$this->_view->set ( "types", $types );
		$this->_view->set ( "idER", $idER );
		$this->_view->set ( "idUS", $idUS );
		$this->_view->set ( "identificationUS", $identificationUS );
		$this->_view->set ( "numeroER", $numeroER );
		$this->_view->set ( "traitements", $traitements );
		$this->_view->set ( "traitement_id", $traitement_id );
		$this->_view->set ( "typeasso_id", $typeasso_id );
	}
	public function actionAjaxAutocom() {
		$er = new Model_Elementrecueilli ();
		$term = $_GET ["term"];
		$table = array ();
		$ider = $_GET ["ider"];
		$idus = $_GET ["idus"];
		
		if (is_numeric ( $term )) {
			$search = $_GET ["us_num"] . "." . $term . "%";
			$searchisole = $_GET ["us_num"] . ".lot." . $term . "%";
			$ers = $er->fetchAll ()->where ( "(numero like '" . $search . "') or (numero like '" . $searchisole . "') and (us_id='" . $idus . "')" );
			foreach ( $ers as $er ) {
				if ($er ["id"] != $ider) {
					$x ["label"] = $er->get ( "numero" );
					if (! is_null ( $er->get ( "description" ) ))
						$x ["label"] .= " - " . substr ( $er->get ( "description" ), 0, 20 );
					if (strlen ( $er->get ( "description" ) ) > 20)
						$x ["label"] .= "...";
					$x ["value"] = $er->get ( "numero" );
					$x ["id"] = $er->get ( 'id' );
					$table [] = $x;
				}
			}
		} else {
			$search = "%" . $term . "%";
			$ers = $er->fetchAll ()->where ( " (description like '" . $search . "') and us_id='" . $idus . "' and id!='" . $ider . "'" );
			foreach ( $ers as $er ) {
				if ($er ["id"] != $ider) {
					$x ["label"] = $er->get ( "numero" );
					if (! is_null ( $er->get ( "description" ) ))
						$x ["label"] .= " - " . substr ( $er->get ( "description" ), 0, 20 );
					if (strlen ( $er->get ( "description" ) ) > 20)
						$x ["label"] .= "...";
					$x ["value"] = $er->get ( "numero" );
					$x ["id"] = $er->get ( 'id' );
					$table [] = $x;
				}
			}
		}
			
		$this->getLayout()->setEnabled(false) ;
		$this->_view->setEnabled(false) ;
			
		echo json_encode($table);
	}
	
	public function actionAjaxAutocomAllUS() {
		$er = new Model_Elementrecueilli();
		$term = $_GET["term"];
		$table = array();
		$ider = $_GET["ider"];
		$idus = $_GET["idus"];
		
		$us = new Model_Us();
		
		// Site fouillé
		$session = $this->getSession();
		$sitefouille_id = $session->has('sitefouille_id') ? $session->get('sitefouille_id') :	null ;
	
		$uses = $us->getBySitefouilleAll($sitefouille_id) ;		
		
		if (is_numeric($term)) {
			
			foreach($uses as $use){
				$search = $use->get('identification').".".$term."%";
				$searchisole = $use->get('identification').".lot.".$term."%";
				$idsUS = $use->get('id');
				$ers=$er->fetchAll()->where("(numero like '".$search."') or (numero like '".$searchisole."') and (us_id='".$idsUS."')");

				foreach($ers as $er) {
					
					if ($er["id"] !=$ider) {
						$x["label"]= $er->get("numero") ;
						if (!is_null($er->get("description")))
							$x["label"] .= " - " . substr($er->get("description"), 0, 20) ;
						if (strlen($er->get("description")) > 20)
							$x["label"] .= "..." ;
						$x["value"] = $er->get("numero");
						$x["id"] = $er->get('id');
						$table[] = $x;
					}
				}
			}
		} else {
			foreach($uses as $use){
				$search="%".$term."%";
				$idsUS = $use->get('id');
				
				$ers=$er->fetchAll()->where(" (description like '".$search."') and us_id='".$idsUS."' and id!='".$ider."'");
				foreach($ers as $er) {
					if ($er["id"] !=$ider) {
						$x["label"]= $er->get("numero") ;
						if (!is_null($er->get("description")))
							$x["label"] .= " - " . substr($er->get("description"), 0, 20) ;
						if (strlen($er->get("description")) > 20)
							$x["label"] .= "..." ;
						$x["value"] = $er->get("numero");
						$x["id"] = $er->get('id');
						$table[] = $x;
					}
				}
			}
		}
		
		$this->getLayout ()->setEnabled ( false );
		$this->_view->setEnabled ( false );
		
		echo json_encode ( $table );
	}
	public function actionAjaxVisualiserAssociations() {
		$elementrecueilli = new Model_Elementrecueilli ();
		$this->getLayout ()->setEnabled ( false );
		
		$idER = $_POST ["idER"];
		
		$associations = $elementrecueilli->getAssociations ( $idER );
		
		$estAssocie = $elementrecueilli->estAssocie ( $idER );
		$associationsInverses = array ();
		if (count ( $estAssocie ) != 0)
			$associationsInverses = $elementrecueilli->getAssociations ( $estAssocie ["er_nouveau_id"] );
		
		$this->_view->set ( "associations", $associations );
		$this->_view->set ( "associationsInverses", $associationsInverses );
		$this->_view->set ( "idER", $idER );
	}
	public function actionAjaxAfficherEmplacement() {
		$elementrecueilli = new Model_Elementrecueilli ();
		
		$emplacement = $this->getChemin ( $_POST ["idER"] );
		
		$this->getLayout ()->setEnabled ( false );
		$this->_view->set ( "emplacement", $emplacement );
	}
	public function getChemin($id = null) {
		$er = (is_null ( $id )) ? null : new Model_Elementrecueilli ( $id );
		$chemin = "";
		if (! is_null ( $er )) {
			if (! is_null ( $er ["emplacement_id"] ))
				$chemin = $this->setChemin ( $er ["emplacement_id"] );
			else if (! is_null ( $er ["conteneur_id"] )) {
				$conteneur = new Model_Conteneur ( $er ["conteneur_id"] );
				$chemin = $this->setChemin ( $conteneur ["emplacement_id"] );
				$chemin .= "> " . $conteneur ["nom"];
			}
		}
		return $chemin;
	}
	public function setChemin($id) {
		$emplacement = new Model_Emplacement ();
		$user_session = $this->getSession ()->get ( 'session' );
		$entiteadmin_id = $user_session ['entiteadmin_id'];
		$racine = $emplacement->getRacine ( $entiteadmin_id );
		
		$sites = $emplacement->getParent ( $entiteadmin_id, $id, $racine ["id"] );
		
		$chemin = "";
		foreach ( $sites as $item ) {
			$tab = explode ( "-", $item ["path"] );
			unset ( $tab [0] );
			foreach ( $tab as $cle => $valeur ) {
				$emp = new Model_Emplacement ( $valeur );
				if ($cle == 1)
					$chemin .= $emp->get ( "nom" );
				else
					$chemin .= " >  " . $emp->get ( "nom" );
			}
		}
		return $chemin;
	}
}
