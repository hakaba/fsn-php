<?php
class Controller_Requeteur extends Yab_Controller_Action {

	public function actionIndex() {
		$session = $this->getSession();
		$sitefouille_id = $session->has('sitefouille_id') ? $session->get('sitefouille_id') : null ;
		
		$requeteur = new Model_Requeteur();
			
		$categories = $requeteur->getTablesRequeteur();
		$this->_view->set('listeCategories', $categories);
		//var_dump($categories); die();

		if (isset($_POST["listerRequetes"]))
			$listerRequetes = $_POST["listerRequetes"];
		else 
			$listerRequetes = '';		
		if ($listerRequetes == "lister") {
			$listeRequetes = $requeteur->getListeRequetes();
			$this->_view->set('requetes_index', $listeRequetes);
			$this->_view->set('listerRequetes', $listerRequetes);
			return;
		}
		
		if (isset($_POST["categorieprincipale_Technique"])) {
			$categorie_requeteur = $_POST["categorieprincipale_Technique"] ;
			//$_SESSION["categorie_requeteur"] = $categorie_requeteur ;
		}
		else {
			/*
			if (isset($_SESSION["categorie_requeteur"]))
				$categorie_requeteur = $_SESSION["categorie_requeteur"] ;
			else
			*/
				$categorie_requeteur = null ;
		}
		$this->_view->set('categorie_requeteur', $categorie_requeteur);

		if (isset($_POST["select_Technique"]) && isset($_POST["from_Technique"]) && isset($_POST["inner_Technique"]) && isset($_POST["where_Technique"]) && !is_null($categorie_requeteur)) {
			//if (isset($_POST["selectTechniqueArea"]) && isset($_POST["fromTechniqueArea"]) && isset($_POST["whereTechniqueArea"]) && !is_null($categorie_requeteur)) {
			// les champs *TechniqueArea ne sont pas disponibles via $_POST tant qu'ils ne sont pas éditables : prop('disabled', true) cf. requeteur.js
			$requeteur = "requete_" . $categorie_requeteur;
			$entite = ucfirst($categorie_requeteur);
			
			if (!empty($_POST["select_Technique"]) && !empty($_POST["from_Technique"]) && !empty($_POST["where_Technique"])) {
				$session[$requeteur] = $_POST["select_Technique"] . ' ' . $_POST["from_Technique"] . ' ' . $_POST["inner_Technique"] . ' ' . $_POST["where_Technique"];
				//$session[$requeteur] = $_POST["selectTechniqueArea"] . ' ' . $_POST["fromTechniqueArea"] . ' ' . $_POST["whereTechniqueArea"];
				// echo "<prev>"; print_r($session[$requeteur]); echo "</prev>";die;

			}
			else{
				$session[$requeteur] = "";
			}
			//var_dump($entite); die();
			$this->forward($entite, 'index' );
		}
	}
	
	public function actionTraduireRequeteur() {
		$registry = $this->getRegistry();
		// appel fichier internationalisation
		$i18n = $this->getRegistry()->get('i18n');
		$filter_html = new Yab_Filter_Html();
		$filter_no_html = new Yab_Filter_NoHtml();
		try {
			echo $filter_no_html->filter($i18n->say($_POST["donnee"]));
		} catch ( Exception $e ) {
			echo $_POST["donnee"];
		}
	}
	
	/**
	 * Fonction qui permet l'appel ajax au service web
	 */
	public function actionAjaxTableReflect() {
		$table = $_POST["table"];
		$url = 'http://'.Yab_Loader::getInstance()->getConfig()['ip'].':'.Yab_Loader::getInstance()->getConfig()['port'].'/'.$table.'/reflect';
		//$url = 'http://109.26.75.172:'.Yab_Loader::getInstance()->getConfig()['port'].'/'.$table.'/reflect';
		$header = array('Content-Type: text/xml;charset=utf-8');
		$data = '';

		$resource = curl_init();
		curl_setopt($resource, CURLOPT_URL, $url);
		curl_setopt($resource, CURLOPT_HTTPHEADER, $header);
		curl_setopt($resource, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($resource, CURLOPT_POST, 1);
		curl_setopt($resource, CURLOPT_POSTFIELDS, $data);
	
		$result = curl_exec($resource);
	
		curl_close($resource);
		
		// Envoie du resultat a l'ajax
		echo($result);
		die();
	}
	
	public function actionEnregistrerRequete() {
		$titre = addslashes($_POST["titre"]);
		$description = addslashes($_POST["description"]);
		$critere = addslashes($_POST["critere"]);
		$requete = addslashes($_POST["requete"]);
		$modifiee = $_POST["modifiee"];	
	
		/*
		var_dump($titre);
		var_dump($description);
		var_dump($critere);
		var_dump($requete);
		var_dump($modifiee);
		var_dump($user_id);
		die();
		*/

		$requeteur = new Model_Requeteur();
		
		$titreGet = $requeteur->getTitreRequeteur($titre); // vérifier si une requête avec ce titre existe déjà
		if (!empty($titreGet[0]['titre'])){ // le titre existe déjà (non vide)
			// echo("confirm(Veuillez confirmer la modification de la requête : ".$titre.")");
			$id_requete = $titreGet[0]['id'];
			$result = $requeteur->enregistrerRequeteur($id_requete,$titre,$description,$critere,$requete,$modifiee);	
		}
		else { // sinon on peut insérer la requête en base
			$result = $requeteur->enregistrerRequeteur(null,$titre,$description,$critere,$requete,$modifiee);	
		}
		
		if ($result == "1")
			echo("La requête a bien été enregistrée.");
		else
			echo($result);
	}	

	public function actionGetFsnCategorie() {
		$moduleFonctionnel = $_POST["moduleFonctionnel"];

		$where = 'categorie_object = "' . $moduleFonctionnel . '" ';
		if (isset($_POST["categorieType"])) {
			$categorieType = $_POST["categorieType"];
			$where .= ' and categorie_type = "' . $categorieType . '" ';
		}
		else 
			$categorieType = '';
		
		if (isset($_POST["categorieColonne"])) {
			$categorieColonne = $_POST["categorieColonne"];
		}
		else
			$categorieColonne = "categorie_value";
					
		$fsn_categorie = new Model_Fsn_Categorie();
		$result = $fsn_categorie->fetchAll()->where($where)->orderBy($categorieColonne) ;
		$result = $result->toArray();
		//var_dump($result); die();
		
		$categorieKeyArray = array();
		foreach($result as $fc){
			array_push($categorieKeyArray, $fc[$categorieColonne]);
		}
		$categorieKeyJSON = json_encode($categorieKeyArray);
		echo($categorieKeyJSON);
	}
	
	public function actionGetRequete() {
		$id = $_POST["id"];
		$colonne = $_GET["colonne"];
		$requeteur = new Model_Requeteur($id);
		$result = $requeteur->get($colonne);
		echo($result);
		/*
		$requeteur = new Model_Requeteur();
		$result = $requeteur->getRequete($id);
		var_dump($result); die();
		echo($result);
		*/
		/*
		$JSONresult = json_encode($result);
		var_dump($JSONresult); die();
		echo($JSONresult);
		*/
	}
	
	public function actionDeleteRequete() {
		$id = $_POST["id"];
		$requeteur = new Model_Requeteur($id);
		$result = $requeteur->delete();
		if($result){
			echo "La réquête a été supprimée avec succès";
		}
		else{
			echo "Un problème est survenu lors de la suppression de la réquête\nVeuillez réessayer ultérieurement";
		}
	}

	/*
	public function actionGetRequeteTitre() {
		$id = $_POST["id"];
		$requeteur = new Model_Requeteur($id);
		$result = $requeteur->get('titre');
		echo($result);
	}

	public function actionGetRequeteDescription() {
		$id = $_POST["id"];
		$requeteur = new Model_Requeteur($id);
		$result = $requeteur->get('description');
		echo($result);
	}

	public function actionGetRequeteCritere() {
		$id = $_POST["id"];
		$requeteur = new Model_Requeteur($id);
		$result = $requeteur->get('critere');
		echo($result);
	}

	public function actionGetRequeteRequete() {
		$id = $_POST["id"];
		$requeteur = new Model_Requeteur($id);
		$result = $requeteur->get('requete');
		echo($result);
	}
	*/
	
	/*
	public function actionListerRequetes() {
		$requeteur = new Model_Requeteur();
		$listeRequetes = $requeteur->getListeRequetes();
		$this->_view->set('$requetes_index', $listeRequetes);
		$this->_view->set('$listerRequetes',TRUE);
		$this->forward('Requeteur', 'index');
	}
	*/
	
}
?>
