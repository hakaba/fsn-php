<?php

class Controller_Association_Er extends Yab_Controller_Action {

	public function actionIndex() {

		$association_er = new Model_Association_Er();

		$association_ers = $association_er->fetchAll();

		$this->_view->set('association_ers', $association_ers);
	}

	public function actionAdd() {

		$association_er = new Model_Association_Er();

		$form = new Form_Association_Er($association_er);

		if($form->isSubmitted() && $form->isValid()) {

			$association_er->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'association_er as been added');

			$this->forward('Association_Er', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$association_er = new Model_Association_Er($this->_request->getParams());

		$form = new Form_Association_Er($association_er);

		if($form->isSubmitted() && $form->isValid()) {

			$association_er->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'association_er as been edited');

			$this->forward('Association_Er', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$association_er = new Model_Association_Er($this->_request->getParams());

		$association_er->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'association_er as been deleted');

		$this->forward('Association_Er', 'index');

	}

}