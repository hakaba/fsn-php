<?php

class Controller_Dashboard extends Yab_Controller_Action {

	public function actionIndex() {

    $session = $this->getSession(); 
    $sitefouille_id = $session->has('sitefouille_id') ? $session->get('sitefouille_id') : null ;
    $oa_id = $session->has('oa_id') ? $session->get('oa_id') : null ;
    
    // Controle l'existance sitefouille_id en session, sinon re-envoie vers la liste pour choisir  
    if (!empty($sitefouille_id) ) {
      
      // re-initialisation des redirections
      if ( $session->has('forward_controller') ) { $this->getSession()->rem('forward_controller'); }
      if ( $session->has('forward_action') ) { $this->getSession()->rem('forward_action'); }
      
      $this->forward('Dashboard', 'Show', array('sitefouille_id' => $sitefouille_id) );
    
    } else {
      $this->getSession()->set('flash_title', 'Suivi de chantiers'); 
      $this->getSession()->set('flash_status', 'warning'); 
      $this->getSession()->set('flash_message', 'Veuillez choisir un site de fouille');
      
      $this->getSession()->set('forward_controller', 'Dashboard_Sitefouille');
      $this->getSession()->set('forward_action', 'Show');
      
      $this->forward('Dashboard_Sitefouille', 'List');    
    }
    return ; 
		
	}

  /**
 * Neutralisation des actions "parasites"
 * redirection vers page d'accueil   
 */      
  public function actionNoAction() {
    
      $flash_message = 'flash_no_action' ;
      $flash_status = 'alert' ;
      $this->getSession()->set('flash_message', $flash_message);
      $this->getSession()->set('flash_status', $flash_status);
		
    	$this->forward('Index', 'index');
      
	}
  
  public function actionShow() {

		$session = $this->getSession();
    $sitefouille_id = $session->get('sitefouille_id'); 
    
    $sitefouille = new Model_Sitefouille($sitefouille_id);

    $this->getSession()->set('title_precision', $sitefouille->get('nom'));
		
    // $this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

    public function actionParticipations() {

        $session = $this->getSession();
        $sitefouille_id = $session->get('sitefouille_id');
        
        // Mantis 345 : En cas de tous sites, empêcher l'utilisation du Dashboard
        if ((is_null($sitefouille_id)) || (empty($sitefouille_id))) {
        	$this->getSession()->set('flash_title', 'Suivi de chantiers');
        	$this->getSession()->set('flash_status', 'warning');
        	$this->getSession()->set('flash_message', 'Veuillez choisir un site de fouille');
        	return;
        }
        
        $sitefouille = new Model_Sitefouille($sitefouille_id);

        $this->getSession()->set('title_precision', $sitefouille->get('nom'));

        // $this->_view->set('helper_form', new Yab_Helper_Form($form));

    }

    public function actionInterventions() {

        $session = $this->getSession();
        $sitefouille_id = $session->get('sitefouille_id');

        // Mantis 345 : En cas de tous sites, empêcher l'utilisation du Dashboard
        if ((is_null($sitefouille_id)) || (empty($sitefouille_id))) {
        	$this->getSession()->set('flash_title', 'Suivi de chantiers');
        	$this->getSession()->set('flash_status', 'warning');
        	$this->getSession()->set('flash_message', 'Veuillez choisir un site de fouille');
        	return;
        }
        
        $sitefouille = new Model_Sitefouille($sitefouille_id);

        $this->getSession()->set('title_precision', $sitefouille->get('nom'));

        // $this->_view->set('helper_form', new Yab_Helper_Form($form));

    }

    public function actionUs() {

        $session = $this->getSession();
        $sitefouille_id = $session->get('sitefouille_id');

        // Mantis 345 : En cas de tous sites, empêcher l'utilisation du Dashboard
        if ((is_null($sitefouille_id)) || (empty($sitefouille_id))) {
        	$this->getSession()->set('flash_title', 'Suivi de chantiers');
        	$this->getSession()->set('flash_status', 'warning');
        	$this->getSession()->set('flash_message', 'Veuillez choisir un site de fouille');
        	$this->forward('Index', 'index');
        	return;
        }
        
        $sitefouille = new Model_Sitefouille($sitefouille_id);

        $this->getSession()->set('title_precision', $sitefouille->get('nom'));

        // $this->_view->set('helper_form', new Yab_Helper_Form($form));

    }

    public function actionElementrecueilli() {

        $session = $this->getSession();
        $sitefouille_id = $session->get('sitefouille_id');

        // Mantis 345 : En cas de tous sites, empêcher l'utilisation du Dashboard
        if ((is_null($sitefouille_id)) || (empty($sitefouille_id))) {
        	$this->getSession()->set('flash_title', 'Suivi de chantiers');
        	$this->getSession()->set('flash_status', 'warning');
        	$this->getSession()->set('flash_message', 'Veuillez choisir un site de fouille');
        	$this->forward('Index', 'index');
        	return;
        }
        
        $sitefouille = new Model_Sitefouille($sitefouille_id);

        $this->getSession()->set('title_precision', $sitefouille->get('nom'));

        // $this->_view->set('helper_form', new Yab_Helper_Form($form));

    }
  
  /**
   * Appel au Plugin Zenphoto : Communication FSN => Zenphoto
   * Test simple de demande du numéro de version Zenphoto   
   */     
  public function actionZenphotoVersion($layout=false) {

    // Enable / Disable le Layout
		$this->getLayout()->setEnabled($layout);
    
    // Définie un autre Layout, que celui par défaut (declaré dans le config.ini)    
    // $this->getLayout()->setFile('View/layout_ajax.html');
    
    $zenphoto = new Plugin_Zenphoto();

    $ZenphotoVersion = $zenphoto->getVersion() ;
    
    print $ZenphotoVersion ;
	}
  
}