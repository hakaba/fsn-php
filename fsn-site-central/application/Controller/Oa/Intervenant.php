<?php

class Controller_Oa_Intervenant extends Yab_Controller_Action {

	public function actionIndex() {

		$oa_intervenant = new Model_Oa_Intervenant();

		$oa_intervenants = $oa_intervenant->fetchAll();

		$this->_view->set('oa_intervenants', $oa_intervenants);
	}

	public function actionAdd() {

		$oa_intervenant = new Model_Oa_Intervenant();

		$form = new Form_Oa_Intervenant($oa_intervenant);

		if($form->isSubmitted() && $form->isValid()) {

			$oa_intervenant->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'oa_intervenant as been added');

			$this->forward('Oa_Intervenant', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$oa_intervenant = new Model_Oa_Intervenant($this->_request->getParams());

		$form = new Form_Oa_Intervenant($oa_intervenant);

		if($form->isSubmitted() && $form->isValid()) {

			$oa_intervenant->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'oa_intervenant as been edited');

			$this->forward('Oa_Intervenant', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$oa_intervenant = new Model_Oa_Intervenant($this->_request->getParams());

		$oa_intervenant->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'oa_intervenant as been deleted');

		$this->forward('Oa_Intervenant', 'index');

	}

}