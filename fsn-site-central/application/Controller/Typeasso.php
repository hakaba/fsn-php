<?php

class Controller_Typeasso extends Yab_Controller_Action {

	public function actionIndex() {

		$typeasso = new Model_Typeasso();

		$typeassos = $typeasso->fetchAll();

		$this->_view->set('typeassos', $typeassos);
	}

	public function actionAdd() {

		$typeasso = new Model_Typeasso();

		$form = new Form_Typeasso($typeasso);

		if($form->isSubmitted() && $form->isValid()) {

			$typeasso->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'typeasso as been added');

			$this->forward('Typeasso', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$typeasso = new Model_Typeasso($this->_request->getParams());

		$form = new Form_Typeasso($typeasso);

		if($form->isSubmitted() && $form->isValid()) {

			$typeasso->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'typeasso as been edited');

			$this->forward('Typeasso', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$typeasso = new Model_Typeasso($this->_request->getParams());

		$typeasso->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'typeasso as been deleted');

		$this->forward('Typeasso', 'index');

	}

}