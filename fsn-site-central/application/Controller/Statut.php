<?php

class Controller_Statut extends Yab_Controller_Action {

	public function actionIndex() {

		$statut = new Model_Statut();

		$statuts = $statut->fetchAll();

		$this->_view->set('statuts', $statuts);
	}

	public function actionAdd() {

		$statut = new Model_Statut();

		$form = new Form_Statut($statut);

		if($form->isSubmitted() && $form->isValid()) {

			$statut->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'statut as been added');

			$this->forward('Statut', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$statut = new Model_Statut($this->_request->getParams());

		$form = new Form_Statut($statut);

		if($form->isSubmitted() && $form->isValid()) {

			$statut->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'statut as been edited');

			$this->forward('Statut', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$statut = new Model_Statut($this->_request->getParams());

		$statut->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'statut as been deleted');

		$this->forward('Statut', 'index');

	}

}