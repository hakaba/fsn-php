<?php

class Controller_Classe extends Yab_Controller_Action {

	public function actionIndex() {

		$classe = new Model_Classe();

		$classes = $classe->fetchAll();

		$this->_view->set('classes', $classes);
	}

	public function actionAdd() {

		$classe = new Model_Classe();

		$form = new Form_Classe($classe);

		if($form->isSubmitted() && $form->isValid()) {

			$classe->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'classe as been added');

			$this->forward('Classe', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$classe = new Model_Classe($this->_request->getParams());

		$form = new Form_Classe($classe);

		if($form->isSubmitted() && $form->isValid()) {

			$classe->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'classe as been edited');

			$this->forward('Classe', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$classe = new Model_Classe($this->_request->getParams());

		$classe->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'classe as been deleted');

		$this->forward('Classe', 'index');

	}

}