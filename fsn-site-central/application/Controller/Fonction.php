<?php

class Controller_Fonction extends Yab_Controller_Action {

	public function actionIndex() {

		$fonction = new Model_Fonction();

		$fonctions = $fonction->fetchAll();

		$this->_view->set('fonctions', $fonctions);
	}

	public function actionAdd() {

		$fonction = new Model_Fonction();

		$form = new Form_Fonction($fonction);

		if($form->isSubmitted() && $form->isValid()) {

			$fonction->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'fonction as been added');

			$this->forward('Fonction', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$fonction = new Model_Fonction($this->_request->getParams());

		$form = new Form_Fonction($fonction);

		if($form->isSubmitted() && $form->isValid()) {

			$fonction->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'fonction as been edited');

			$this->forward('Fonction', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$fonction = new Model_Fonction($this->_request->getParams());

		$fonction->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'fonction as been deleted');

		$this->forward('Fonction', 'index');

	}

}