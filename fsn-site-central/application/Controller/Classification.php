<?php

class Controller_Classification extends Yab_Controller_Action {

	public function actionIndex() {

		$classification = new Model_Classification();

		$classifications = $classification->fetchAll();

		$this->_view->set('classifications', $classifications);
	}

	public function actionAdd() {

		$classification = new Model_Classification();

		$form = new Form_Classification($classification);

		if($form->isSubmitted() && $form->isValid()) {

			$classification->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'classification as been added');

			$this->forward('Classification', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$classification = new Model_Classification($this->_request->getParams());

		$form = new Form_Classification($classification);

		if($form->isSubmitted() && $form->isValid()) {

			$classification->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'classification as been edited');

			$this->forward('Classification', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$classification = new Model_Classification($this->_request->getParams());

		$classification->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'classification as been deleted');

		$this->forward('Classification', 'index');

	}

}