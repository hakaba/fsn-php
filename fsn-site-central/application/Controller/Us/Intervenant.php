<?php

class Controller_Us_Intervenant extends Yab_Controller_Action {

	public function actionIndex() {

		$us_intervenant = new Model_Us_Intervenant();

		$us_intervenants = $us_intervenant->fetchAll();

		$this->_view->set('us_intervenants', $us_intervenants);
	}

	public function actionAdd() {

		$us_intervenant = new Model_Us_Intervenant();

		$form = new Form_Us_Intervenant($us_intervenant);

		if($form->isSubmitted() && $form->isValid()) {

			$us_intervenant->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'us_intervenant as been added');

			$this->forward('Us_Intervenant', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$us_intervenant = new Model_Us_Intervenant($this->_request->getParams());

		$form = new Form_Us_Intervenant($us_intervenant);

		if($form->isSubmitted() && $form->isValid()) {

			$us_intervenant->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'us_intervenant as been edited');

			$this->forward('Us_Intervenant', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$us_intervenant = new Model_Us_Intervenant($this->_request->getParams());

		$us_intervenant->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'us_intervenant as been deleted');

		$this->forward('Us_Intervenant', 'index');

	}

}