<?php

class Controller_Us_Oa extends Yab_Controller_Action {

	public function actionIndex() {

		$us_oa = new Model_Us_Oa();

		$us_oas = $us_oa->fetchAll();

		$this->_view->set('us_oas', $us_oas);
	}

	public function actionAdd() {

		$us_oa = new Model_Us_Oa();

		$form = new Form_Us_Oa($us_oa);

		if($form->isSubmitted() && $form->isValid()) {

			$us_oa->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'us_oa as been added');

			$this->forward('Us_Oa', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$us_oa = new Model_Us_Oa($this->_request->getParams());

		$form = new Form_Us_Oa($us_oa);

		if($form->isSubmitted() && $form->isValid()) {

			$us_oa->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'us_oa as been edited');

			$this->forward('Us_Oa', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$us_oa = new Model_Us_Oa($this->_request->getParams());

		$us_oa->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'us_oa as been deleted');

		$this->forward('Us_Oa', 'index');

	}

}