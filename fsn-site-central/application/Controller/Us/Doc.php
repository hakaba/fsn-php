<?php

class Controller_Us_Doc extends Yab_Controller_Action {

	public function actionIndex() {

		$us_doc = new Model_Us_Doc();		

		$us_docs = $us_doc->fetchAll();		

		$this->_view->set('us_docs', $us_docs);
	}

	public function actionAdd() {

		$us_doc = new Model_Us_Doc();

		$form = new Form_Us_Doc($us_doc);

		if($form->isSubmitted() && $form->isValid()) {

			$us_doc->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'us_doc as been added');

			$this->forward('Us_Doc', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$us_doc = new Model_Us_Doc($this->_request->getParams());

		$form = new Form_Us_Doc($us_doc);

		if($form->isSubmitted() && $form->isValid()) {

			$us_doc->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'us_doc as been edited');

			$this->forward('Us_Doc', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$us_doc = new Model_Us_Doc($this->_request->getParams());

		$us_doc->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'us_doc as been deleted');

		$this->forward('Us_Doc', 'index');

	}

}