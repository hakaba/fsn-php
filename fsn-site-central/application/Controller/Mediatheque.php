<?php

class Controller_Mediatheque extends Yab_Controller_Action {

    const BASE_ALBUM = "/mediatheque/";

	public function actionAjaxFileTree() {
        $url = $_SERVER['HTTP_HOST'].str_replace("/public","",Yab_Loader::getInstance()->getRequest()->getBaseUrl()).self::BASE_ALBUM;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL,$url."ws.php?format=json&method=pwg.categories.getImages&cat_id=".(isset($_POST['dir']) && $_POST['dir']?$_POST['dir']:0));
        $files_result = json_decode(curl_exec($ch),true);
        curl_setopt($ch, CURLOPT_URL,$url."ws.php?format=json&method=pwg.categories.getList&cat_id=".(isset($_POST['dir']) && $_POST['dir']?$_POST['dir']:0));
        $albums_result = json_decode(curl_exec($ch),true);
        curl_close($ch);
        $files = ($files_result['stat']=="ok")?$files_result['result']['images']:[];
        $albums = [];
        if($albums_result['stat'] == "ok") {
            $albums = $albums_result['result']['categories'];
            if($albums[0]['id'] == $_POST['dir'])
                unset($albums[0]);
        }
        if( !empty($files) || !empty($albums) ) {
            $linked_imgs = [];
            if(isset($_POST['entity']) && isset($_POST['id'])){
                $model_n = "Model_".$_POST['entity']."ZpwgImages";
                $model = new $model_n();
                $function = "get".$_POST['entity']."Images";
                $links = $model->$function($_POST['id']);
                foreach($links as $l)
                    $linked_imgs[] = $l['id'];
            }

            echo "<ul class='jqueryFileTree'>";

            foreach( $files as $file ) {
                echo    "<li class='file ext_".preg_replace('/^.*\./', '', $file['file'])."' fileid='".$file['id'].
                        "' path='".$file['element_url']."' file-title='".$file['name']."'>
                            <input type='checkbox' ".(in_array($file['id'],$linked_imgs)?'checked':'').
                            " onclick='$(this).prop(\"checked\",!$(this).prop(\"checked\")).next().click()'/>
                            <a rel='" . $_POST['dir']."_".$file['id'] . "' onclick='last_file = this'>" . $file['file'] . "</a>
                        </li>";
            }

            foreach( $albums as $key => $album ) {
                echo    "<li class='directory collapsed'>
                            <a rel='" . $album['id'] . "'>" . $album['name'] . "</a>
                        </li>";
            }

            echo "</ul>";
        }
    }

    public function actionEdit($entity, $entity_id, $layout=false){

        $this->getLayout()->setEnabled($layout);

        $this->_view->set('album_path',self::BASE_ALBUM);
        $this->_view->set('entity', $entity);
        $this->_view->set('entity_id', $entity_id);

        if (!$layout) { $this->_view->setFile('View/mediatheque/edit.html'); }
    }

    public function actionShow($entity, $entity_id, $layout=false){
        $this->getLayout()->setEnabled($layout);
        $url = str_replace("/public","",Yab_Loader::getInstance()->getRequest()->getBaseUrl()).self::BASE_ALBUM;

        $model_n = "Model_".$entity."ZpwgImages";
        $model = new $model_n();
        $function = "get".$entity."Images";
        $medias = $model->$function($entity_id)->toArray();
        foreach($medias as $key => $media)
            $medias[$key]['element_url'] = $url.$media['path'];
        $this->_view->set('medias',$medias);
        $this->_view->set('album_path',self::BASE_ALBUM);

        if (!$layout) { $this->_view->setFile('View/mediatheque/show.html'); }
    }

    public function actionAjaxAddMedia(){
        if(isset($_POST['entity']) && isset($_POST['entity_id']) && isset($_POST['media_id'])){
            $model_n = "Model_".$_POST['entity']."ZpwgImages";
            $model = new $model_n();
            $function = "add".$_POST['entity']."Images";
            try{
                $model->$function($_POST['entity_id'],$_POST['media_id']);
                return;
            }catch(Exception $e){
                echo("sql insert error");
                return;
            }
        }
        echo "attributes entity, entity_id and media_id are needed.";
    }

    public function actionAjaxRemMedia(){
        if(isset($_POST['entity']) && isset($_POST['entity_id']) && isset($_POST['media_id'])){
            $model_n = "Model_".$_POST['entity']."ZpwgImages";
            $model = new $model_n();
            $function = "rem".$_POST['entity']."Images";
            try {
                $model->$function($_POST['entity_id'], $_POST['media_id']);
                return;
            }catch(Exception $e){
                echo("sql delete error");
                return;
            }
        }
        echo "attributes entity, entity_id and media_id are needed.";
    }

}