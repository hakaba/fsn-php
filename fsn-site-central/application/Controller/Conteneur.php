<?php

class Controller_Conteneur extends Yab_Controller_Action {

	private function getTree($id=null) {
		$emplacement = new Model_Emplacement();

		$user_session = $this->getSession()->get('session') ;
		$entiteadmin_id = $user_session['entiteadmin_id'] ;
		
		$racine=$emplacement->getRacine($entiteadmin_id);
		
		$sites = $emplacement->getParent($entiteadmin_id, null, $racine["id"]);
		$organisme=$emplacement->getNomOrganisme($entiteadmin_id);
		$organisme=$organisme->toArray();
		$racine=$organisme[0]["nom"];
		
		$table[0]["label"]=$racine;
		$table[0]["id"]=0;
		$table[0]["expanded"]="true";
		$conteneur = new Model_Conteneur();
		$compt=0;
		$memo=0;
		$tree=array();
		foreach ($sites as $site){
			if ($memo==$site["level"])
				$compt++;
			else {
				$compt=1;
				$memo=$site["level"];
			}

			$tree[$site["level"]]["emp".$site["id"]]=$compt;
			$conteneurs = $conteneur->getConteneur($site["id"]);


			$tab=explode("-", $site["path"]);

			if ($site["level"]==1) {
				$table[0]["items"][$compt]["label"]=$site["nom"];
				$table[0]["items"][$compt]["id"]="emp".$site["id"];

				$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][0]["label"]="Conteneurs";
				$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][0]["id"]=0;

				foreach ($conteneurs as $cle => $item) {
					$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][0]["items"][$cle]["label"]=$item["nom"];
					$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][0]["items"][$cle]["id"]="con".$item["id"];
				}
			} else if ($site["level"]==2) {
				$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$compt]["label"]=$site["nom"];
				$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$compt]["id"]="emp".$site["id"];

				$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$tree[2]["emp".$tab[2]]]["items"][0]["label"]="Conteneurs";
				$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$tree[2]["emp".$tab[2]]]["items"][0]["id"]=0;

				foreach ($conteneurs as $cle => $item) {
					$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$tree[2]["emp".$tab[2]]]["items"][0]["items"][$cle]["label"]=$item["nom"];
					$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$tree[2]["emp".$tab[2]]]["items"][0]["items"][$cle]["id"]="con".$item["id"];
				}
			} else if ($site["level"]==3) {
				$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$tree[2]["emp".$tab[2]]]["items"][$compt]["label"]=$site["nom"];
				$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$tree[2]["emp".$tab[2]]]["items"][$compt]["id"]="emp".$site["id"];

				$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$tree[2]["emp".$tab[2]]]["items"][$tree[3]["emp".$tab[3]]]["items"][0]["label"]="Conteneurs";
				$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$tree[2]["emp".$tab[2]]]["items"][$tree[3]["emp".$tab[3]]]["items"][0]["id"]=0;

				foreach ($conteneurs as $cle => $item) {
					$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$tree[2]["emp".$tab[2]]]["items"][$tree[3]["emp".$tab[3]]]["items"][0]["items"][$cle]["label"]=$item["nom"];
					$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$tree[2]["emp".$tab[2]]]["items"][$tree[3]["emp".$tab[3]]]["items"][0]["items"][$cle]["id"]="con".$item["id"];
				}
			} else if ($site["level"]==4) {
				$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$tree[2]["emp".$tab[2]]]["items"][$tree[3]["emp".$tab[3]]]["items"][$compt]["label"]=$site["nom"];
				$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$tree[2]["emp".$tab[2]]]["items"][$tree[3]["emp".$tab[3]]]["items"][$compt]["id"]="emp".$site["id"];

				$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$tree[2]["emp".$tab[2]]]["items"][$tree[3]["emp".$tab[3]]]["items"][$tree[4]["emp".$tab[4]]]["items"][0]["label"]="Conteneurs";
				$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$tree[2]["emp".$tab[2]]]["items"][$tree[3]["emp".$tab[3]]]["items"][$tree[4]["emp".$tab[4]]]["items"][0]["id"]=0;

				foreach ($conteneurs as $cle => $item) {
					$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$tree[2]["emp".$tab[2]]]["items"][$tree[3]["emp".$tab[3]]]["items"][$tree[4]["emp".$tab[4]]]["items"][0]["items"][$cle]["label"]=$item["nom"];
					$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$tree[2]["emp".$tab[2]]]["items"][$tree[3]["emp".$tab[3]]]["items"][$tree[4]["emp".$tab[4]]]["items"][0]["items"][$cle]["id"]="con".$item["id"];
				}
			}  else if ($site["level"]==5) {
				$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$tree[2]["emp".$tab[2]]]["items"][$tree[3]["emp".$tab[3]]]["items"][$tree[4]["emp".$tab[4]]]["items"][$compt]["label"]=$site["nom"];
				$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$tree[2]["emp".$tab[2]]]["items"][$tree[3]["emp".$tab[3]]]["items"][$tree[4]["emp".$tab[4]]]["items"][$compt]["id"]="emp".$site["id"];

				$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$tree[2]["emp".$tab[2]]]["items"][$tree[3]["emp".$tab[3]]]["items"][$tree[4]["emp".$tab[4]]]["items"][$tree[5]["emp".$tab[5]]]["items"][0]["label"]="Conteneurs";
				$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$tree[2]["emp".$tab[2]]]["items"][$tree[3]["emp".$tab[3]]]["items"][$tree[4]["emp".$tab[4]]]["items"][$tree[5]["emp".$tab[5]]]["items"][0]["id"]=0;

				foreach ($conteneurs as $cle => $item) {
					$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$tree[2]["emp".$tab[2]]]["items"][$tree[3]["emp".$tab[3]]]["items"][$tree[4]["emp".$tab[4]]]["items"][$tree[5]["emp".$tab[5]]]["items"][0]["items"][$cle]["label"]=$item["nom"];
					$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$tree[2]["emp".$tab[2]]]["items"][$tree[3]["emp".$tab[3]]]["items"][$tree[4]["emp".$tab[4]]]["items"][$tree[5]["emp".$tab[5]]]["items"][0]["items"][$cle]["id"]="con".$item["id"];
				}
			} else if ($site["level"]==6) {
				$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$tree[2]["emp".$tab[2]]]["items"][$tree[3]["emp".$tab[3]]]["items"][$tree[4]["emp".$tab[4]]]["items"][$tree[5]["emp".$tab[5]]]["items"][$compt]["label"]=$site["nom"];
				$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$tree[2]["emp".$tab[2]]]["items"][$tree[3]["emp".$tab[3]]]["items"][$tree[4]["emp".$tab[4]]]["items"][$tree[5]["emp".$tab[5]]]["items"][$compt]["id"]="emp".$site["id"];

				$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$tree[2]["emp".$tab[2]]]["items"][$tree[3]["emp".$tab[3]]]["items"][$tree[4]["emp".$tab[4]]]["items"][$tree[5]["emp".$tab[5]]]["items"][$tree[6]["emp".$tab[6]]]["items"][0]["label"]="Conteneurs";
				$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$tree[2]["emp".$tab[2]]]["items"][$tree[3]["emp".$tab[3]]]["items"][$tree[4]["emp".$tab[4]]]["items"][$tree[5]["emp".$tab[5]]]["items"][$tree[6]["emp".$tab[6]]]["items"][0]["id"]=0;

				foreach ($conteneurs as $cle => $item) {
					$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$tree[2]["emp".$tab[2]]]["items"][$tree[3]["emp".$tab[3]]]["items"][$tree[4]["emp".$tab[4]]]["items"][$tree[5]["emp".$tab[5]]]["items"][$tree[6]["emp".$tab[6]]]["items"][0]["items"][$cle]["label"]=$item["nom"];
					$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$tree[2]["emp".$tab[2]]]["items"][$tree[3]["emp".$tab[3]]]["items"][$tree[4]["emp".$tab[4]]]["items"][$tree[5]["emp".$tab[5]]]["items"][$tree[6]["emp".$tab[6]]]["items"][0]["items"][$cle]["id"]="con".$item["id"];
				}
			}  else if ($site["level"]==7) {
				$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$tree[2]["emp".$tab[2]]]["items"][$tree[3]["emp".$tab[3]]]["items"][$tree[4]["emp".$tab[4]]]["items"][$tree[5]["emp".$tab[5]]]["items"][$tree[6]["emp".$tab[6]]]["items"][$compt]["label"]=$site["nom"];
				$table[0]["items"][$tree[1]["emp".$tab[1]]]["items"][$tree[2]["emp".$tab[2]]]["items"][$tree[3]["emp".$tab[3]]]["items"][$tree[4]["emp".$tab[4]]]["items"][$tree[5]["emp".$tab[5]]]["items"][$tree[6]["emp".$tab[6]]]["items"][$compt]["id"]="emp".$site["id"];
			}
		}
		return $table;
	}

	public function actionAjaxGetTree() {
		$this->getLayout()->setEnabled(false) ;
		$this->_view->setEnabled(false) ;
		$table=$this->getTree();
		echo json_encode($table);
	}

	public function actionGestion($type_id=null) {
		$this->getSession ()->set('titre', 'GESTION DES CONTENEURS');
		// $this -> getLayout() -> setFile("View/layout_fsn.html");

		$conteneur = new Model_Conteneur();
		$user_session = $this->getSession()->get("session") ;
		$entiteadmin_id = $user_session["entiteadmin_id"] ;
		$conteneurs = $conteneur->getAll($entiteadmin_id);
		if ($type_id!=null)
			$conteneurs = $conteneurs->where("conteneur.type_id='".$type_id."'");

		$type=new Model_Fsn_Categorie();
		$types=$type->getListByType('conteneur_type', 'conteneur');

		$this->_view->set('conteneurs', $conteneurs);
		$this->_view->set('types', $types);
		$this->_view->set('selected_type', $type_id);
	}

	public function actionDetail() {
		$conteneur = new Model_Conteneur($this->_request->getParams());
		$user_session = $this->getSession()->get("session") ;
		$entiteadmin_id = $user_session["entiteadmin_id"] ;
		$emplacement = new Model_Emplacement();
		$racine=$emplacement->getRacine($entiteadmin_id);
		$emplacements = $emplacement->getParent($entiteadmin_id, $conteneur->get('emplacement_id'), $racine["id"]);
		
		$nom_emplacement="";
		foreach ($emplacements as $item ) {
			$tab=explode("-", $item["path"]);
			unset($tab[0]);
			foreach ($tab as $cle => $valeur){
				$emp=new Model_Emplacement($valeur);
				if ($cle==1)
					$nom_emplacement.=$emp->get("nom") ;
				else
					$nom_emplacement.=" >  ". $emp->get("nom") ;

			}
		}

		$form = new Form_Detailconteneur($conteneur);

		$er=new Model_Elementrecueilli();
		$ers=$er->getByConteneur($conteneur->get('id'));

		$this->getSession ()->set('titre', "DETAIL D'UN CONTENEUR");
		// $this -> getLayout() -> setFile("View/layout_fsn.html");
		$this->_view->set('emplacement', $nom_emplacement);
		$this->_view->set('ers', $ers);
		$this->_view->set('form', $form);
	}

	public function actionFormulaire() {
		$conteneur = new Model_Conteneur($this->_request->getParams());

		$form = new Form_Conteneur($conteneur);

		$nom_emplacement="";
		$message="";

		if($form->isSubmitted() && $form->isValid()) {
			try {

				$conteneur->populate($form->getValues())->save();

				$this->getSession()->set('flash', 'conteneur as been edited');

				$this->forward('Conteneur', 'gestion');

			} catch(Yab_Exception $e) {

				$message="Le nom ou le mot de passe est déjà utilisé";

			}
		}
		if($form->isSubmitted() && $form->getElement('emplacement_id')->getValue()!=null) {
			$emplacement=new Model_Emplacement($form->getElement('emplacement_id')->getValue());
			$nom_emplacement=$emplacement->get('nom');
		} elseif ($form->isSubmitted() && $form->getElement('emplacement_id')->getValue()==null) {
			$nom_emplacement="";
		} else {
			$emplacement=new Model_Emplacement($conteneur->get('emplacement_id'));
			$nom_emplacement=$emplacement->get('nom');
		}

		$this->getSession ()->set("titre", "MODIFICATION  D'UN CONTENEUR");
		// $this -> getLayout() -> setFile("View/layout_fsn.html");
		$this->_view->set('emplacement', $nom_emplacement);
		$this->_view->set('message', $message);
		$this->_view->set('form', $form);
	}

	public function actionIndex() {

		$conteneur = new Model_Conteneur();

		$conteneurs = $conteneur->fetchAll();

		$this->_view->set('conteneurs_index', $conteneurs);
	}

	public function actionAjouter() {

		$conteneur = new Model_Conteneur();

		$form = new Form_Conteneur($conteneur);

		$this->getSession ()->set("titre", "CREATION  D'UN CONTENEUR");
		// $this -> getLayout() -> setFile("View/layout_fsn.html");
		$this->_view->set('form', $form);
		$this->_view->set('emplacement', '');
	}

	public function actionShow() {

		$conteneur = new Model_Conteneur($this->_request->getParams());

		$form = new Form_Conteneur($conteneur);
		$nom_emplacement = "";

		if($form->getElement('emplacement_id')->getValue()!=null) {
			$emplacement=new Model_Emplacement($form->getElement('emplacement_id')->getValue());
			$nom_emplacement=$emplacement->get('nom');
		}

		$this->_view->set('nom_emplacement', $nom_emplacement);
		$this->_view->set('form_cont', $form);
	}

	public function actionAdd() {

		$conteneur = new Model_Conteneur();

		$user_session = $this->getSession()->get('session') ;
		$entiteadmin_id = $user_session['entiteadmin_id'] ;
		$conteneur->set('entiteadmin_id', $entiteadmin_id);
		$form = new Form_Conteneur($conteneur);
		$formvalue = $form->getValues();
		
		$nom_emplacement = "";
		$message = "";
		$errors_messages = "";
		
		if($form->isSubmitted()) {
			try{
				//Trois param($donnees, $field, $fieldMessage, $errors_messages)
				Plugin_Fonctions::novirgule($formvalue['nom'], 'nom', 'novirguleinfield', $errors_messages);
				
				if(!empty($errors_messages) || !$form->isValid()) throw new Exception();
				else{					
					$conteneur->populate($form->getValues())->save();
					
					// jfb 2016-04-20 correctif fiche mantis 219 : historisation du conteneur début
					$id_conteneur = $conteneur->get('id');
					$formvalue['id'] = $id_conteneur;
					$historisation = new Plugin_Historique();
					$formvalue = $form->getTypedValues($formvalue);
					$historisation->addhistory('Conteneur', self::MODE_CREATE, $formvalue);
					// jfb 2016-04-20 correctif fiche mantis 219 : historisation du conteneur fin
				
					$this->getSession()->set('flash', 'conteneur as been added');
					$this->forward('Conteneur', 'index'); 
				}
			} catch(Exception $e){
				$this->getSession()->set('flash_status', 'warning');
				$this->getSession()->setFlashErrors($form->getErrors());
				$this->getSession()->setFlashErrors($errors_messages);
			}

		}

		if($form->getElement('emplacement_id')->getValue()!=null) {
				$emplacement=new Model_Emplacement($form->getElement('emplacement_id')->getValue());
				$nom_emplacement=$emplacement->get('nom');
		}

		$this->getSession ()->set("titre", "CREATION  D'UN CONTENEUR");
		$this->_view->set('form_cont', $form);
		$this->_view->set('emplacement', $nom_emplacement);
		$this->_view->set('message', $message);

	}

	public function actionEdit() {

		$conteneur = new Model_Conteneur($this->_request->getParams());

		$form = new Form_Conteneur($conteneur);
		$formvalue = $form->getValues();
		
		$nom_emplacement="";
		$message="";
		$errors_messages="";
		
		if($form->isSubmitted() ) {
			try{
				//Trois param($donnees, $field, $fieldMessage, $errors_messages)
				Plugin_Fonctions::novirgule($formvalue['nom'], 'nom', 'novirguleinfield', $errors_messages);
				
				if(!empty($errors_messages) || !$form->isValid()) throw new Exception();
				else{					
					$conteneur->populate($form->getValues())->save();
					
					// jfb 2016-04-20 correctif fiche mantis 219 : historisation du conteneur début
					$id_conteneur = $conteneur->get('id');
					$formvalue['id'] = $id_conteneur;
					$historisation = new Plugin_Historique();
					$formvalue = $form->getTypedValues($formvalue);
					$historisation->addhistory('Conteneur', self::MODE_UPDATE, $formvalue);
					// jfb 2016-04-20 correctif fiche mantis 219 : historisation du conteneur fin
				
					$this->getSession()->set('flash', 'conteneur as been edited');
					$this->forward('Conteneur', 'index');
				}
			} catch(Exception $e){
				$this->getSession()->set('flash_status', 'warning');
				$this->getSession()->setFlashErrors($form->getErrors());
				$this->getSession()->setFlashErrors($errors_messages);
			}
		}

		if($form->getElement('emplacement_id')->getValue()!=null) {
			$emplacement=new Model_Emplacement($form->getElement('emplacement_id')->getValue());
			$nom_emplacement=$emplacement->get('nom');
		}

		$this->getSession ()->set('titre', 'GESTION DES CONTENEURS');
		$this->_view->set('form_cont', $form);
		$this->_view->set('emplacement', $nom_emplacement);
		$this->_view->set('message', $message);

	}

	public function actionDelete() {

		$conteneur = new Model_Conteneur($this->_request->getParams());

		// jfb 2016-04-20 correctif fiche mantis 219 : récupération du formulaire début
		$form = new Form_Conteneur($conteneur);
		$formvalue = $form->getValues();
		$id_conteneur = $conteneur->get('id');
		$formvalue['id'] = $id_conteneur;
		// jfb 2016-04-20 correctif fiche mantis 219 : récupération du formulaire fin
		
		// jfb 2016-04-18 correctif fiche mantis 99 début
		$message = ''; // init. à blanc message de deletion
		
		try {
			$conteneur->delete(); // try delete - avant historisation (cf. mantis 219)
					
			// jfb 2016-04-20 correctif fiche mantis 219 : historisation du conteneur début
			$historisation = new Plugin_Historique();
			$formvalue = $form->getTypedValues($formvalue);
			$historisation->addhistory('Conteneur', self::MODE_DELETE, $formvalue);
			// jfb 2016-04-20 correctif fiche mantis 219 : historisation du conteneur fin
				
			$message='conteneur supprimé';
		} catch (Exception $e) {
			$message = 'Suppression conteneur impossible';
		}
				
		//$this->getSession()->set('flash', 'conteneur as been deleted');
		$this->getSession()->set('flash_title', '');
		$this->getSession()->set('flash_status', 'info');
		$this->getSession()->set('flash_message', $message);
		// jfb 2016-04-18 correctif fiche mantis 99 fin
					
		$this->forward('Conteneur', 'index');

	}
	
	public function actionGenererQRCode() {
		$emplacement = new Model_Emplacement();
		$conteneur = new Model_Conteneur();
	
		$user_session = $this->getSession()->get('session') ;
		$entiteadmin_id = $user_session['entiteadmin_id'] ;
		
		$racine=$emplacement->getRacine($entiteadmin_id);
		$id=(isset($_GET["id"])  and $_GET["id"]!=0 ) ? $_GET["id"] : $racine["id"];
	
		$loader = Yab_Loader::getInstance();
		$config = $loader->getConfig();
		$url = "http://" . $config["ip"] . ":" . $config["port"] . $config["context"] . "/conteneur/code" ;
		
	
		//$emps = $emplacement->getEnfant($id) ;
		$emps = $emplacement->getParent($entiteadmin_id, null, $id);
		
		$emp_courant = $emplacement->getEmplacementById($id) ;
		
		$cnts = array() ;
		$cnt_courant = $conteneur->getConteneur($emp_courant["id"]) ; 
		
		if (count($cnt_courant) != 0)
			$cnts[] = $cnt_courant ;
		
		foreach($emps as $emp) {
			$c = $conteneur->getConteneur($emp["id"]) ;
			if (count($c) != 0)
				$cnts[] = $c; 
		}
		
		$this->getLayout()->setEnabled(false) ;
		$this->_view->set("url", $url) ;
		$this->_view->set("emps", $emps) ;
		$this->_view->set("emp_courant", $emp_courant) ;
		$this->_view->set("cnts", $cnts) ;
	}
	
	public function actionGenererQRCodeGroupe() {
		$conteneur = new Model_Conteneur();
		
		$loader = Yab_Loader::getInstance();
		$config = $loader->getConfig();
		$url = "http://" . $config["ip"] . ":" . $config["port"] . $config["context"] . "/conteneur/code" ;
		
		$where = "" ;
		for ($i = 0 ; $i < count($_POST["qrcodes"]) ; $i++) {
			if ($i == 0)
				$where .= "id = '" . $_POST["qrcodes"][$i] . "'" ;
			else
				$where .= " || id = '" . $_POST["qrcodes"][$i] . "'" ;
		}
		
		$cnts = $conteneur->fetchAll()->where($where)->toArray() ;
		
		$this->getLayout()->setEnabled(false) ;
		$this->_view->set("url", $url) ;
		$this->_view->set("cnts", $cnts) ;
		$this->_view->set("conteneur", $conteneur) ;
	}

}