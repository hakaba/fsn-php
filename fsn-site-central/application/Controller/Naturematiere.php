<?php

class Controller_Naturematiere extends Yab_Controller_Action {

	public function actionIndex() {

		$naturematiere = new Model_Naturematiere();

		$naturematieres = $naturematiere->fetchAll();

		$this->_view->set('naturematieres', $naturematieres);
	}

	public function actionAdd() {

		$naturematiere = new Model_Naturematiere();

		$form = new Form_Naturematiere($naturematiere);

		if($form->isSubmitted() && $form->isValid()) {

			$naturematiere->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'naturematiere as been added');

			$this->forward('Naturematiere', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$naturematiere = new Model_Naturematiere($this->_request->getParams());

		$form = new Form_Naturematiere($naturematiere);

		if($form->isSubmitted() && $form->isValid()) {

			$naturematiere->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'naturematiere as been edited');

			$this->forward('Naturematiere', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$naturematiere = new Model_Naturematiere($this->_request->getParams());

		$naturematiere->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'naturematiere as been deleted');

		$this->forward('Naturematiere', 'index');

	}

}