<?php

class Controller_Phasechrono extends Yab_Controller_Action {

	public function actionIndex() {

		$phasechrono = new Model_Phasechrono();
		$phasechronos = $phasechrono->getVisiblePhases();
		$this->_view->set('phasechronos_index', $phasechronos);
	}

	public function actionAdd() {
		
		// appel fichier internationalisation
		$registry = Yab_Loader::getInstance() -> getRegistry();
		$i18n = $registry -> get('i18n');
		$filter_no_html = new Yab_Filter_NoHtml();
		
		$mode = self::ACTION_MODE_CREATE ;
		$errors_messages = '';

		// jfb 2016-06 mantis 318 début : contrôle site de fouille sélectionné
		$loader = Yab_Loader::getInstance();
		$session = $loader->getSession();
		$sitefouille_id = $session->get('sitefouille_id');
		if (empty($sitefouille_id)) {
			$this->getSession()->set('flash_status', 'warning');
			$this->getSession()->setFlashError('keysf', 'Veuillez sélectionner un site de fouille !');
			$this->forward('Phasechrono', 'index');
		}
		// jfb 2016-06 mantis 318 fin
		
		$phasechrono = new Model_Phasechrono();
		$form = new Form_Phasechrono($phasechrono);
		$formvalue = $form->getValues();
		
		$loader = Yab_Loader::getInstance();
		$session = $loader->getSession();
		$courant_sitefouille_id = $session->get('sitefouille_id');
		
		if($form->isSubmitted()){	

			// Ajout du GUUID 
			$generateGuuid = new Plugin_Guuid() ;
			$guuid = $generateGuuid->GetUUID() ;
			$form->setElement('id', array('type'=>'text', 'value' => $guuid ) ) ;

			try{
				
				$formvalue['id'] = $guuid;
				$formvalue['sitefouille_id']= $courant_sitefouille_id;
				// Control champs requis
				function isNumber($idElt, &$formvalue, &$errors_messages, $message){
					$eltvalue = addslashes($formvalue[$idElt]);
					if(!is_numeric($eltvalue)){
						$errors_messages[$idElt] = $message;
					}
				}
				function isUnique($idElt, &$formvalue, &$phasechrono, &$errors_messages, $message){
					$eltvalue = addslashes($formvalue[$idElt]);
					if(!empty($eltvalue)){	
						$req = $phasechrono->fetchAll()->where(addslashes($idElt).' ="'.$eltvalue.'" and sitefouille_id ="'.$formvalue['sitefouille_id'].'" ');
						$nb_row = $req->count();
						if($nb_row > 0){
							$errors_messages[$idElt] = $message;
						}
					}
				}
				
				if($form->getElement('identification')->getErrors()){
					$errors_messages['identification'] = $filter_no_html->filter( $i18n -> say('verror_requisAll') );
				}
				
				isNumber('datatdebbas_aaaa', $formvalue, $errors_messages, $filter_no_html->filter( $i18n -> say('phasechrono_requisDatatdebbas_aaaa') ));	
				isNumber('datatdebhaut_aaaa', $formvalue, $errors_messages, $filter_no_html->filter( $i18n -> say('phasechrono_requisDatatdebhaut_aaaa') ));	
				isNumber('datatfinbas_aaaa', $formvalue, $errors_messages, $filter_no_html->filter( $i18n -> say('phasechrono_requisDatatfinbas_aaaa') ));	
				isNumber('datatfinhaut_aaaa', $formvalue, $errors_messages, $filter_no_html->filter( $i18n -> say('phasechrono_requisDatatfinhaut_aaaa') ));	
				
				isUnique('identification', $formvalue, $phasechrono , $errors_messages, $filter_no_html->filter( $i18n -> say('phasechrono_uniciteIdentification') ));
				
				//Trois param($donnees, $field, $fieldMessage, $errors_messages)
				Plugin_Fonctions::novirgule($formvalue['identification'], 'identification', 'novirguleinfield', $errors_messages);
				
				if(!empty($errors_messages)) throw new Exception();
				
				$phasechrono->populate($formvalue)->save();	
						
				// Historisation de la modif
				$id_phasechrono = $phasechrono->get('id');
				$formvalue['id'] = $id_phasechrono ;
				$historisation = new Plugin_Historique() ;
				
				$formvalue = $form->getTypedValues($formvalue);
				$historisation->addhistory('phasechrono', self::MODE_CREATE, $formvalue) ;
				
				$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); 	$this->getSession()->set('flash_message', 'phasechrono ajoutée');

				$this->forward('Phasechrono', 'index');
			}catch(Exception $e){
				$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'warning'); $this->getSession()->set('flash_message', '');
				$this->getSession()->setFlashErrors($errors_messages);
			}
		}
		
		//$this->_view->set('helper_form', new Yab_Helper_Form($form));
		$this->_view->set('helper_model_phasechrono', $phasechrono);
		$this->_view->set('helper_form', $form);
		$this->_view->set('courant_sitefouille_id', $courant_sitefouille_id);

	}
	
	public function actionShow(){
		
		$mode = self::ACTION_MODE_SHOW ;

		$phasechrono = new Model_Phasechrono($this->_request->getParams());
        $form = new Form_Phasechrono($phasechrono);
		$formvalue= $form->getValues();
		
		$courant_sitefouille_id = $phasechrono->get('sitefouille_id');
		
		/* JFB 2016-08-26 Début : Mise en commentaire de l'historisation de l'action show de la phase chrono
		// Historisation de la modif
		$id_phasechrono = $phasechrono->get('id');
		$formvalue['id'] = $id_phasechrono ;
		$historisation = new Plugin_Historique() ;
		$historisation->addhistory('Phasechrono',$mode,$formvalue) ;
		 * JFB 2016-08-26 Fin */	
		
		//$this->_view->set('helper_form', new Yab_Helper_Form($form));
		$this->_view->set('helper_form', $form);
		$this->_view->set('helper_model_phasechrono', $phasechrono);
		$this->_view->set('courant_sitefouille_id', $courant_sitefouille_id);
	}

	public function actionEdit() {
				
		// appel fichier internationalisation
		$registry = Yab_Loader::getInstance() -> getRegistry();
		$i18n = $registry -> get('i18n');
		$filter_no_html = new Yab_Filter_NoHtml();
		
		$mode = self::ACTION_MODE_UPDATE ;
		$errors_messages = '';
		
		$phasechrono = new Model_Phasechrono($this->_request->getParams());
		$form = new Form_Phasechrono($phasechrono);
		$formvalue = $form->getValues();
		$id = $phasechrono->get('id');
		
		$courant_sitefouille_id = $phasechrono->get('sitefouille_id');

		if($form->isSubmitted()) {
			
			try{
				
				$formvalue['sitefouille_id'] = $courant_sitefouille_id;
				
				// Control champs requis
				function isNumber($idElt, &$formvalue, &$errors_messages, $message){
					$eltvalue = addslashes($formvalue[$idElt]);
					if($eltvalue !== null && !is_numeric($eltvalue)){
						$errors_messages[$idElt] = $message;
					}
				}
				function isUnique($idElt, &$formvalue, &$phasechrono, &$id, &$errors_messages, $message){
					$eltvalue = addslashes($formvalue[$idElt]);
					if(!empty($eltvalue)){		
						$req = $phasechrono->fetchAll()->where(addslashes($idElt).' ="'.$eltvalue.'" and id !="'.$id.'" and sitefouille_id ="'.$formvalue['sitefouille_id'].'"');
						$nb_row = $req->count();
						if($nb_row > 0){
							$errors_messages[$idElt] = $message;
						}
					}
				}
				
				if($form->getElement('identification')->getErrors()){
					$errors_messages['identification'] = $filter_no_html->filter( $i18n -> say('verror_requisAll') );
				}
				isNumber('datatdebbas_aaaa', $formvalue, $errors_messages, $filter_no_html->filter( $i18n -> say('phasechrono_requisDatatdebbas_aaaa') ));	
				isNumber('datatdebhaut_aaaa', $formvalue, $errors_messages, $filter_no_html->filter( $i18n -> say('phasechrono_requisDatatdebhaut_aaaa') ));	
				isNumber('datatfinbas_aaaa', $formvalue, $errors_messages, $filter_no_html->filter( $i18n -> say('phasechrono_requisDatatfinbas_aaaa') ));	
				isNumber('datatfinhaut_aaaa', $formvalue, $errors_messages, $filter_no_html->filter( $i18n -> say('phasechrono_requisDatatfinhaut_aaaa') ));	
				
				isUnique('identification', $formvalue, $phasechrono , $id, $errors_messages, $filter_no_html->filter( $i18n -> say('phasechrono_uniciteIdentification') ));
				
				//Trois param($donnees, $field, $fieldMessage, $errors_messages)
				Plugin_Fonctions::novirgule($formvalue['identification'], 'identification', 'novirguleinfield', $errors_messages);
				
				if(!empty($errors_messages)) throw new Exception();
				
				$phasechrono->populate($formvalue)->save();	
						
				// Historisation de la modif
				$id_phasechrono = $phasechrono->get('id');
				$formvalue['id'] = $id_phasechrono ;
				$historisation = new Plugin_Historique() ;
				
				$formvalue = $form->getTypedValues($formvalue);
				$historisation->addhistory('phasechrono', self::MODE_UPDATE, $formvalue) ;
				
				$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); 	$this->getSession()->set('flash_message', 'phasechrono modifiée');

				$this->forward('Phasechrono', 'index');
			}catch(Exception $e){
				$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'warning'); $this->getSession()->set('flash_message', '');
				$this->getSession()->setFlashErrors($errors_messages);
			}    
		}

		// $this->_view->set('helper_form', new Yab_Helper_Form($form));
		$this->_view->set('helper_model_phasechrono', $phasechrono);
		$this->_view->set('helper_form', $form);
		$this->_view->set('courant_sitefouille_id', $courant_sitefouille_id);

	}

	public function actionDelete() {

		$phasechrono = new Model_Phasechrono($this->_request->getParams());
		$form = new Form_Phasechrono($phasechrono);
		$formvalue = $form->getValues();
		
		$formvalue['id'] = $phasechrono->get('id');		
		$formvalue['sitefouille_id'] = $phasechrono->get('sitefouille_id');

// jfb 2016-04-18 correctif fiche mantis 99 début
		$message = ''; // init. à blanc message de deletion
		
		try {
			$phasechrono->delete(); // try delete avant historisation
				
			// historisation début
			$historisation = new Plugin_Historique();		
			$formvalue = $form->getTypedValues($formvalue);
        	$historisation->addhistory('Phasechrono', self::MODE_DELETE, $formvalue);
        	// historisation fin
        	         	     
        	$message='phasechrono supprimée';
        } catch (Exception $e) {
       		$message = 'Suppression phasechrono impossible';
       	}
        	
        //$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'phasechrono as been deleted');
       	$this->getSession()->set('flash_title', '');
       	$this->getSession()->set('flash_status', 'info');
       	$this->getSession()->set('flash_message', $message);
// jfb 2016-04-18 correctif fiche mantis 99 fin
       	
		$this->forward('Phasechrono', 'index');

	}

}