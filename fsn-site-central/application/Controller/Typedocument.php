<?php

class Controller_Typedocument extends Yab_Controller_Action {

	public function actionIndex() {

		$typedocument = new Model_Typedocument();

		$typedocuments = $typedocument->fetchAll();

		$this->_view->set('typedocuments', $typedocuments);
	}

	public function actionAdd() {

		$typedocument = new Model_Typedocument();

		$form = new Form_Typedocument($typedocument);

		if($form->isSubmitted() && $form->isValid()) {

			$typedocument->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'typedocument as been added');

			$this->forward('Typedocument', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$typedocument = new Model_Typedocument($this->_request->getParams());

		$form = new Form_Typedocument($typedocument);

		if($form->isSubmitted() && $form->isValid()) {

			$typedocument->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'typedocument as been edited');

			$this->forward('Typedocument', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$typedocument = new Model_Typedocument($this->_request->getParams());

		$typedocument->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'typedocument as been deleted');

		$this->forward('Typedocument', 'index');

	}

}