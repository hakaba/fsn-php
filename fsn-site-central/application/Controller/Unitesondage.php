<?php

class Controller_Unitesondage extends Yab_Controller_Action {
	
	public function actionIndex() {
		
		$unite_sondage = new Model_Unitesondage ();
		
		$session = $this->getSession();
		$requeteur = "requete_" . "unitesondage";
		$requeteurSession = $session->has($requeteur) ? $session->get($requeteur) : null;
		
		$sitefouille_id = $session->has('sitefouille_id') ? $session->get('sitefouille_id') : null;
		if(empty($sitefouille_id) ) {
			$session->set ( 'forward_controller', 'index' );
			$session->set ( 'forward_action', 'index' );
			$this->forward('Sitefouille', 'Select', array(1) );
		}
		
		if (!is_null($requeteurSession) && !empty($requeteurSession)) {
			$unites_sondage = $unite_sondage->getListunitesondage ($sitefouille_id)->where("id IN (" . $requeteurSession . ") " ) ;
		}
		else {
			$unites_sondage = $unite_sondage->getListunitesondage ($sitefouille_id);
		}
		
		$this->_view->set ( 'tranchee_index', $unites_sondage );
		
		$session[$requeteur] = ''; // la requête étant consommée, on l'efface
		
		//$loader = Yab_Loader::getInstance ();
		//$config = $loader->getConfig ();
		//$this->_view->set ( 'config', $config );
	}
	
	
	public function actionAdd() {
		
		$loader = Yab_Loader::getInstance();
		$session = $loader->getSession();
		$sitefouille_id = $session->get('sitefouille_id');
		if (empty($sitefouille_id)) {
			$this->getSession()->set('flash_status', 'warning');
			$this->getSession()->setFlashError('keysf', 'Veuillez sélectionner un site de fouille !');
			$this->forward('Unitesondage', 'index');
		}
		
		$mode = self::ACTION_MODE_CREATE;
		
		$usondage = new Model_Unitesondage();
		$form_add = new Form_Unitesondage($usondage, $mode);
		
		$form_add->setElement('numero_ident', array(
				'type' => 'text',
				'id' => 'numero_ident',
				'label' => $form_add->getElement('identification')->get('label'),
				'value' => isset($_POST['numero_ident']) ? $_POST['numero_ident'] : $form_add->getElement('identification')->get('numero'),
				'validators' => array('NotEmpty','Int'),
				'errors' => array('NotEmpty' => array('Le champ identification ne doit pas être vide'),
						'Int' => array('Ce champs doit être un entier')),
		));
		
		if(isset($_POST)) {
			//echo "_POST is set; length = ".count($_POST)."<br>";
			if(count($_POST)>1) {
				// the form has been submitted:
				$formvalues = $form_add->getValues();
				$errors_messages = array();
				$exist_us = $usondage->getUsondageByIdentification($form_add->getElement('identification')->get('prefix') . "." . intval($_POST['numero_ident']))->toArray();
				if(!empty($exist_us)) {
					$errors_messages[$form_add->getElement('identification')->get('id')] = "l'identifiant doit être unique";
				}else {
					$form_add->getElement('identification')->set('value',$form_add->getElement('identification')->get('prefix') . "." . intval($_POST['numero_ident']));
				}
				if (empty($errors_messages)) {
					$transformDateFrUs = new Plugin_DateFrUs();
					if($formvalues['date_debut'] && $formvalues['date_debut'] != '')
						$formvalues['date_debut'] = $transformDateFrUs->convertitDateFRUs($formvalues['date_debut']);
					else
						unset($formvalues['date_debut']);
					if($formvalues['date_fin'] && $formvalues['date_fin'] != '')
						$formvalues['date_fin'] = $transformDateFrUs->convertitDateFRUs($formvalues['date_fin']);
					else
						unset($formvalue['fin']);
					//$formvalues['date_debut'] = date("Y-m-d", strtotime(str_replace('/', '-', $formvalues['date_debut'])));
					//$formvalues['date_fin'] = date("Y-m-d", strtotime(str_replace('/', '-', $formvalues['date_fin'])));

					//echo "everything is OK! <br>".print_r($_POST)."<br>".print_r($formvalues);
						
					unset($formvalues['numero_ident']);
					// Ajout du GUUID
					$generateGuuid = new Plugin_Guuid();
					$guuid = $generateGuuid -> GetUUID();
					//$form->setElement('id', array('type'=>'text', 'value' => $guuid ) ) ;
					$formvalues['id'] = $guuid;
					$formvalues['identification'] = $form_add->getElement('identification')->get('value');
								
					$formvalues = $form_add -> setNullValues($formvalues);
					$usondage -> populate($formvalues) -> save();
					
					$this->getSession()->set('flash_title', '');
					$this->getSession()->set('flash_status', 'info');
					$this->getSession()->set('flash_message', 'Unite Sondage ajoutée');
					$this->forward('Unitesondage', 'Index', array('id' => $guuid));
						
				} else {
					$this->getSession()->set('flash_title', '');
					$this->getSession()->set('flash_status', 'info');
					$this->getSession()->set('flash_message', 'Verifiez vos entrées: valid='.$form_add->isValid().'<br>empty='.empty($errors_messages));
				}
			}
		} else {
			//echo "_POST is not set<br>";
		}
		
		$this->_view->set('form_add', $form_add);
		
	}
	
	
	public function actionShow($id = null) {
		
		$mode = self::ACTION_MODE_SHOW;
		
		$params = $this->_request->getParams();
		$unitesondage_id = $params[0];
		
		$unite_sondage = new Model_Unitesondage( $this->_request->getParams () );
		$form = new Form_Unitesondage($unite_sondage, $mode);
		//$formvalues = $form->getValues();
		$id_unite_sondage = $unite_sondage->get ('id');
		
		$tranchee = null;
		$i18n = Yab_Loader::getInstance()->getRegistry()->get('i18n');
		if($unite_sondage->getType($id_unite_sondage) == 'fenetre') {
			// obtenir la tranchee dans laquelle elle se trouve:
			$tranchee = $unite_sondage->getUsondageByIdentification($unite_sondage->get('numero_tranchee'));
			$form -> setElement('tranchee', array(
					'type' => 'text',
					'id' => 'tranchee',
					'label' => $i18n -> say('tranchee'),
					'placeholder' => $i18n -> say('tranchee'),
					'tooltip' => $i18n -> say('tranchee'),
					'value' => 'Identification : '.$tranchee->toRow()->get("identification").' Nom : '.$tranchee->toRow()->get("nom"),
					'validators' => array(),
					'errors' => array(),
			));
		} else {
			// obtenir la liste des fenetres qui se trouvent dans cette tranchee:
			$fenetres = $unite_sondage->getListFenetres($id_unite_sondage);
			if($fenetres -> count() > 0) {
				$form -> setElement('fenetres', array(
						'type' => 'ol',
						'id' => 'fenetres',
						'label' => $i18n -> say('fenetres'),
						'tooltip' => $i18n -> say('fenetres'),
						'value' => $fenetres,
						'errors' => array()
				));
			}
		}
		
		$session = $this->getSession();
		$session['unitesondage_id'] = $unitesondage_id;
		
		$mlog = new Model_Log();
		$logs = $mlog -> getListLog($session->get('unitesondage_id'));
		
		$mtroncon = new Model_Troncon();
		$troncons = $mtroncon -> getListTroncon($session->get('unitesondage_id'));
		
		$this->_view->set('usondage_id', $id_unite_sondage);
		$this->_view->set('form_show', $form);
		$this->_view->set('logs', $logs);
		$this->_view->set('troncons', $troncons);
		
	}
	
	
	public function actionEdit() {
		
		$mode = self::ACTION_MODE_UPDATE;
		
		$params = $this->_request->getParams();
		$unitesondage_id = $params[0];
		$session = $this->getSession();
		$session['unitesondage_id'] = $unitesondage_id;
		//echo $unitesondage_id."<br>";
		
		$usondage = new Model_Unitesondage($params);
		$form_edit = new Form_Unitesondage($usondage, $mode);
		
		if(count($_POST)>1) {
			// the following test should be $form_edit->isValid() but that returns false :(
			if($form_edit->hasElement('identification')) {
				echo "form is valid!<br>";
				$formvalues = $form_edit->getValues();
				$formvalues['date_debut'] = date("Y-m-d", strtotime(str_replace('/', '-', $formvalues['date_debut'])));
				$formvalues['date_fin'] = date("Y-m-d", strtotime(str_replace('/', '-', $formvalues['date_fin'])));
				$formvalues = $form_edit->setNullValues($formvalues);
				print_r($formvalues);
				$usondage->populate($formvalues)->save();
				
				$this->getSession()->set('flash_title', '');
				$this->getSession()->set('flash_status', 'info');
				$this->getSession()->set('flash_message', 'Unite Sondage modifiée');
				$this->forward('Unitesondage', 'Index');
			} else {
				echo "form is not valid!<br>";
			}
		} else {
		    echo "form not submitted!<br>";
			//print_r($_SESSION);
		}
		
		$this->_view->set('form_edit', $form_edit);
		
	}
	
	
	public function actionDelete() {
		$unitesondage = new Model_Unitesondage( $this->_request->getParams () );
		$form = new Form_Unitesondage( $unitesondage );
		$formvalues = $form->getValues ();
		
		$formvalues ['id'] = $unitesondage->get ( 'id' );
		$sf_id = $formvalues ['id']; // jfb 2016-04-22 fiche mantis 231
		
		// jfb 2016-05-02 mantis 257 début
		//$fsn_unitesondage_id = $unitesondage->get ( 'fsn_unitesondage_id' );
		//$formvalues ['fsn_unitesondage_id'] = ( int ) $fsn_unitesondage_id; // jfb 2016-05-03 mantis 259
		// jfb 2016-05-02 mantis 257 fin
		 
		// jfb 2016-04-18 correctif fiche mantis 99 début
		$message = ''; // init. à blanc message de deletion
		
		try {
			$unitesondage->delete (); // try delete avant historisation
		
			// historisation début
			$historisation = new Plugin_Historique ();
			$formvalues = $form->getTypedValues ( $formvalues );
			$historisation->addhistory ( 'Unitesondage', self::MODE_DELETE, $formvalues );
			// historisation fin
				
			$message = 'Unite sondage supprimé';
		} catch ( Exception $e ) {
			$message = 'Suppression Unite sondage impossible: '.$e->getMessage();
		}
		
		$this->getSession ()->set ( 'flash_title', '' );
		$this->getSession ()->set ( 'flash_status', 'info' );
		$this->getSession ()->set ( 'flash_message', $message );
		
		$this->forward ( 'Unitesondage', 'index' );
	}
}