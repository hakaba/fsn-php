<?php

class Controller_Topo_Er extends Yab_Controller_Action {

	public function actionIndex() {

		$topo_er = new Model_Topo_Er();

		$topo_ers = $topo_er->fetchAll();

		$this->_view->set('topo_ers', $topo_ers);
	}

	public function actionAdd() {

		$topo_er = new Model_Topo_Er();

		$form = new Form_Topo_Er($topo_er);

		if($form->isSubmitted() && $form->isValid()) {

			$topo_er->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'topo_er as been added');

			$this->forward('Topo_Er', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$topo_er = new Model_Topo_Er($this->_request->getParams());

		$form = new Form_Topo_Er($topo_er);

		if($form->isSubmitted() && $form->isValid()) {

			$topo_er->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'topo_er as been edited');

			$this->forward('Topo_Er', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$topo_er = new Model_Topo_Er($this->_request->getParams());

		$topo_er->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'topo_er as been deleted');

		$this->forward('Topo_Er', 'index');

	}

}