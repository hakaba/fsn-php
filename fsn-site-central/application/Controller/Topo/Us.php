<?php

class Controller_Topo_Us extends Yab_Controller_Action {

	public function actionIndex() {

		$topo_us = new Model_Topo_Us();

		$topo_uses = $topo_us->fetchAll();

		$this->_view->set('topo_uses_index', $topo_uses);
	}
	
	public function actionList($layout=false, $us_id) {
            
        $this->getLayout()->setEnabled($layout);
        
        $topo_us = new Model_Topo_Us();

        $topo_uses = $topo_us->getListTopoUs()->where('us_id = "'.$us_id.'" ' ) ;;

        $this->_view->set('topo_uses_list', $topo_uses);
        
        if (!$layout) { $this->_view->setFile('View/topo/us/list_simple.html') ; }
    }

	public function actionAdd() {

		$topo_us = new Model_Topo_Us();

		$form = new Form_Topo_Us($topo_us);

		if($form->isSubmitted() && $form->isValid()) {

			$topo_us->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'topo_us as been added');

			$this->forward('Topo_Us', 'index');

		}

		$this->_view->set('helper_form_add', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$topo_us = new Model_Topo_Us($this->_request->getParams());

		$form = new Form_Topo_Us($topo_us);

		if($form->isSubmitted() && $form->isValid()) {

			$topo_us->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'topo_us as been edited');

			$this->forward('Topo_Us', 'index');

		}

		$this->_view->set('helper_form_edit', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$topo_us = new Model_Topo_Us($this->_request->getParams());

		$topo_us->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'topo_us as been deleted');

		$this->forward('Topo_Us', 'index');

	}

}