<?php

class Controller_Topo_Sf extends Yab_Controller_Action {

	public function actionIndex() {

		$topo_sf = new Model_Topo_Sf();

		$topo_sfs = $topo_sf->fetchAll();

		$this->_view->set('topo_sfs', $topo_sfs);
	}

	public function actionAdd() {

		$topo_sf = new Model_Topo_Sf();

		$form = new Form_Topo_Sf($topo_sf);

		if($form->isSubmitted() && $form->isValid()) {

			$topo_sf->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'topo_sf as been added');

			$this->forward('Topo_Sf', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$topo_sf = new Model_Topo_Sf($this->_request->getParams());

		$form = new Form_Topo_Sf($topo_sf);

		if($form->isSubmitted() && $form->isValid()) {

			$topo_sf->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'topo_sf as been edited');

			$this->forward('Topo_Sf', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$topo_sf = new Model_Topo_Sf($this->_request->getParams());

		$topo_sf->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'topo_sf as been deleted');

		$this->forward('Topo_Sf', 'index');

	}

}