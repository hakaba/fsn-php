<?php

class Controller_Intervenant extends Yab_Controller_Action {

	public function actionIndex() {

		$intervenant = new Model_Intervenant();
		/* jfb 2016-06-06 mantis 285 début
		$intervenants = $intervenant->fetchAll();
		// jfb 2016-06-06 mantis 285 fin */
		$intervenants = $intervenant->getListIntervenantsIndex(); // jfb 2016-06-06 mantis 285
		$this->_view->set('intervenants_index', $intervenants);
	}

	public function actionAdd() {
		
		// appel fichier internationalisation
		$registry = Yab_Loader::getInstance() -> getRegistry();
		$i18n = $registry -> get('i18n');
		$filter_no_html = new Yab_Filter_NoHtml();
			
		$mode = self::ACTION_MODE_CREATE ;
		$errors_messages = '';

		$intervenant = new Model_Intervenant();
		$form = new Form_Intervenant($intervenant);
        $formvalue= $form->getValues();
		
		if($form->isSubmitted()) {
			
			// Ajout du GUUID 
			$generateGuuid = new Plugin_Guuid() ;
			$guuid = $generateGuuid->GetUUID() ;
			$form->setElement('id', array('type'=>'text', 'value' => $guuid ) ) ;
		
			try{
				
				$formvalue['id']= $guuid;
				
				// Control champs requis
				function isNumber($idElt, &$formvalue, &$form, &$errors_messages, $message){
					$eltvalue = addslashes($formvalue[$idElt]);
					if(!empty($eltvalue)){
						if($form->getElement($idElt)->getErrors()) $errors_messages[$idElt] = $message;
					}
				}
				function isUniqueByTwoParam($idElt1, $idElt2, &$formvalue, &$intervenant, &$errors_messages, $message){
					$eltvalue1 = addslashes($formvalue[$idElt1]);
					$eltvalue2 = addslashes($formvalue[$idElt2]);
					if(!empty($eltvalue1)){		
						$req = $intervenant->fetchAll()->where(addslashes($idElt1).' ="'.$eltvalue1.'" and '.$idElt2.' ="'.$eltvalue2.'" ');
						$nb_row = $req->count();
						if($nb_row > 0){
							$errors_messages[$idElt1] = $message;
						}
					}
				}
				
				if($form->getElement('nom')->getErrors()){
					$errors_messages['nom'] = $filter_no_html->filter( $i18n -> say('verror_requisAll') );
				}
				
				isNumber('cp', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('intervenant_requisCp') ));	
				isUniqueByTwoParam('nom', 'prenom', $formvalue, $intervenant, $errors_messages, $filter_no_html->filter( $i18n -> say('intervenant_uniciteNomPrenom') ));
				
				//Trois param($donnees, $field, $fieldMessage, $errors_messages)
				Plugin_Fonctions::novirgule($formvalue['nom'], 'nom', 'novirguleinfield', $errors_messages);
				Plugin_Fonctions::novirgule($formvalue['prenom'], 'prenom', 'novirguleinfield', $errors_messages);
				
				if(!empty($errors_messages)) throw new Exception();
	
				$intervenant->populate($formvalue)->save();	
				
				// Historisation de la modif
				$id_intervenant = $intervenant->get('id');
				$formvalue['id'] = $id_intervenant ;
				$historisation = new Plugin_Historique() ;
				
				$formvalue = $form->getTypedValues($formvalue);
				$historisation->addhistory('intervenant', self::MODE_CREATE, $formvalue) ;
					  
				$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'intervenant ajouté');
				
				$this->forward('Intervenant', 'index');
			}catch(Exception $e){
				$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'warning'); $this->getSession()->set('flash_message', '');
				$this->getSession()->setFlashErrors($errors_messages);
			}
		}
		//$this->_view->set('helper_form', new Yab_Helper_Form($form));
		$this->_view->set('helper_form', $form);
	}
	
	public function actionShow(){
		
		$mode = self::ACTION_MODE_SHOW ;
		$organisme_id_name = '';
		$metier_id_name = '';
		
		$intervenant = new Model_Intervenant($this->_request->getParams());
        $form = new Form_Intervenant($intervenant);
		$formvalue= $form->getValues();
		
		function getFieldValueById(&$intervenant, $table, $tablecolum, $eltId, $valsearch, &$retour){
			$req = $intervenant->getTable($table)->fetchAll()->where(addslashes($tablecolum).'= "'.$intervenant->get($eltId).'" ');	
			$nb_row = $req->count();
			if($nb_row > 0){
				foreach($req as $row){
					$retour = $row[$valsearch];
				}
			} else $retour = '';
		}
		
		getFieldValueById($intervenant, 'Model_Fsn_Organisme', 'id', 'organisme_id', 'nom', $organisme_id_name);
		getFieldValueById($intervenant, 'Model_Metier', 'id', 'metier_id', 'metier', $metier_id_name);

		$this->_view->set('organisme_id_name',$organisme_id_name) ;
		$this->_view->set('metier_id_name',$metier_id_name) ;
				
		//$this->_view->set('helper_form', new Yab_Helper_Form($form));
		$this->_view->set('helper_form', $form);
	}

	public function actionEdit() {
		
		// appel fichier internationalisation
		$registry = Yab_Loader::getInstance() -> getRegistry();
		$i18n = $registry -> get('i18n');
		$filter_no_html = new Yab_Filter_NoHtml();
			
		$mode = self::ACTION_MODE_UPDATE ;
		$errors_messages = '';
		
		$intervenant = new Model_Intervenant($this->_request->getParams());
		$form = new Form_Intervenant($intervenant);
		$formvalue = $form->getValues();
		$id = $this->_request->getParams();

		if($form->isSubmitted()) {	
			try{		
			
				// Control champs requis
				function isNumber($idElt, &$formvalue, &$form, &$errors_messages, $message){
					$eltvalue = addslashes($formvalue[$idElt]);
					if(!empty($eltvalue)){
						if($form->getElement($idElt)->getErrors()) $errors_messages[$idElt] = $message;
					}
				}
				function isUniqueByTwoParam($idElt1, $idElt2, &$formvalue, &$intervenant, &$id, &$errors_messages, $message){
					$eltvalue1 = addslashes($formvalue[$idElt1]);
					$eltvalue2 = addslashes($formvalue[$idElt2]);
					if(!empty($eltvalue1)){		
						$req = $intervenant->fetchAll()->where(addslashes($idElt1).' ="'.$eltvalue1.'" and '.$idElt2.' ="'.$eltvalue2.'" and id !="'.$id[0].'" ');
						$nb_row = $req->count();
						if($nb_row > 0){
							$errors_messages[$idElt1] = $message;
						}
					}
				}
				
				if($form->getElement('nom')->getErrors()){
					$errors_messages['nom'] = $filter_no_html->filter( $i18n -> say('verror_requisAll') );
				}
				isNumber('cp', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('intervenant_requisCp') ));	
				isUniqueByTwoParam('nom', 'prenom', $formvalue, $intervenant, $id, $errors_messages, $filter_no_html->filter( $i18n -> say('intervenant_uniciteNomPrenom') ));
			
				//Trois param($donnees, $field, $fieldMessage, $errors_messages)
				Plugin_Fonctions::novirgule($formvalue['nom'], 'nom', 'novirguleinfield', $errors_messages);
				Plugin_Fonctions::novirgule($formvalue['prenom'], 'prenom', 'novirguleinfield', $errors_messages);
				
				if(!empty($errors_messages)) throw new Exception();
	
				$intervenant->populate($formvalue)->save();	
				
				// Historisation de la modif
				$id_intervenant = $intervenant->get('id');
				$formvalue['id'] = $id_intervenant ;
				$historisation = new Plugin_Historique() ;
				
				$formvalue = $form->getTypedValues($formvalue);
				$historisation->addhistory('intervenant', self::MODE_UPDATE, $formvalue) ;
					  
				$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'intervenant modifié');
				
				$this->forward('Intervenant', 'index');
			}catch(Exception $e){
				$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'warning'); $this->getSession()->set('flash_message', '');
				$this->getSession()->setFlashErrors($errors_messages);
			}
		}

		//$this->_view->set('helper_form', new Yab_Helper_Form($form));
		$this->_view->set('helper_form', $form);
	}

	public function actionDelete() {

		$intervenant = new Model_Intervenant($this->_request->getParams());
		$form = new Form_Intervenant($intervenant);
		$formvalue = $form->getValues();
		
		$formvalue['id'] = $intervenant->get('id');
		
// jfb 2016-04-18 correctif fiche mantis 99 début
		$message = ''; // init. à blanc message de deletion
		
		try {
			$intervenant->delete(); // try delete avant historisation
			
			// historisation début
			$historisation = new Plugin_Historique();		
			$formvalue = $form->getTypedValues($formvalue);
        	$historisation->addhistory('Intervenant', self::MODE_DELETE, $formvalue);
        	// historisation fin

        	$message='intervenant supprimé';
        } catch (Exception $e) {
       		$message = 'Suppression intervenant impossible';
       	}
        	 
		//$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'intervenant as been deleted');
       	$this->getSession()->set('flash_title', '');
       	$this->getSession()->set('flash_status', 'info');
       	$this->getSession()->set('flash_message', $message);
// jfb 2016-04-18 correctif fiche mantis 99 fin
       	
		$this->forward('Intervenant', 'index');
	}
}