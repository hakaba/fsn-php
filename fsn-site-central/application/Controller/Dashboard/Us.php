<?php

class Controller_Dashboard_Us extends Yab_Controller_Action {

	public function actionIndex() {

		$session = $this->getSession();
		$sitefouille_id = $session->has('sitefouille_id') ? $session->get('sitefouille_id') : null ;

		if (!empty($sitefouille_id) ) {
			$this->forward('Dashboard_Us', 'List', array('sitefouille_id' => $sitefouille_id) );
		} else {
			$this->getSession()->set('flash_title', 'Suivi de chantiers');
			$this->getSession()->set('flash_status', 'warning');
			$this->getSession()->set('flash_message', 'Veuillez choisir un site de fouille');

			$this->getSession()->set('forward_controller', 'Dashboard_Us');
			$this->getSession()->set('forward_action', 'List');

			$this->forward('Dashboard_Sitefouille', 'List');
		}
		return ;

	}

	public function actionList() {

		$session = $this->getSession();
		$sitefouille_id = $session->get('sitefouille_id') ;

		$us = new Model_Us();

		// $uses = $us->fetchAll()->where('sitefouille_id = "'.$sitefouille_id.'" ' ) ;
		$uses = $us->getUsDetail() ;

		$this->_view->set('uses', $uses);

		// re-initialisation des redirections
		if ( $session->has('forward_controller') ) { $this->getSession()->rem('forward_controller'); }
		if ( $session->has('forward_action') ) { $this->getSession()->rem('forward_action'); }

		/*
		 * Par defaut, la view affiche sera application/views/dashboard/us/list.html
		 * => convention d'arborescence deduit du nom du controller
		 * => convention du nom de fichier deduit du nom de l'action
		 *    (si Majuscule � l'interieur du nom de l'action, alors remplac� par un _ dans nom fichier html )
		 */

	}

	public function actionCount($layout=false) {

		$session = $this->getSession();
		$sitefouille_id = $session->get('sitefouille_id');

		$this->getLayout()->setEnabled($layout);

		$us = new Model_Us();

		$uses = $us->fetchAll()->where('sitefouille_id = "'.$sitefouille_id.'" ' ) ;

		$us_count = $uses->count() ;

		print $us_count ;

		return ;

	}

	public function actionShow() {

		$this->getLayout()->setEnabled(false);

		$us = new Model_Us($this->_request->getParams());

		$mode = 'show';

		$form = new Form_Us($us, $mode);

		$this->_view->set('form', $form);

	}

	public function actionUsTimeline($layout=false) {

		// true : le layout (page des menus ) s'affiche, false pas de layout (page des menus )
		$this->getLayout()->setEnabled($layout);

		$us = new Model_Us();

		$uses = $us->countErbyUs();

		$this->_view->set('uses', $uses);

		$this->_view->setFile('View/dashboard/us/us_timeline.html');

	}

	public function actionUsNetwork($layout=false, $height='400px') {

		// true : le layout (page des menus ) s'affiche, false pas de layout (page des menus )
		$this->getLayout()->setEnabled($layout);

		$us = new Model_Us();

		// $network_racine = $us->getRacineUs() ;
		 
		$network_racine = $us->getSuperpositionUs() ;
		$network_racines = $network_racine->where('sp.posterieur_id NOT IN ( SELECT DISTINCT enf.anterieur_id FROM superposition enf )') ;
		$this->_view->set('network_racines', $network_racines );

		$network_branche = $us->getSuperpositionUs() ;
		$network_branches = $network_branche->where('sp.posterieur_id IN ( SELECT DISTINCT enf.anterieur_id FROM superposition enf )') ;

		$this->_view->set('network_branches', $network_branches );

		$network_anteroposterite = $us->getSuperpositionUs() ;
		$this->_view->set('network_anteroposterite', $network_anteroposterite );

		$network_synchronisation = $us->getSynchronisationUs() ;
		$this->_view->set('network_synchronisation', $network_synchronisation );

		$this->_view->set('option_height', $height );
		/*
		 * Par defaut, la view affiche sera application/views/dashboard/us/us_network.html
		 * => convention d'arborescence deduit du nom du controller
		 * => convention du nom de fichier deduit du nom de l'action
		 *    (si Majuscule � l'interieur du nom de l'action, alors remplac� par un _ dans nom fichier html )
		 */

	}
  
	public function actionUsNetworkCytoscape($layout=false) {
    // $this->getLayout ()->setFile ( "View/layout_ajax.html" );
		// true : le layout (page des menus ) s'affiche, false pas de layout (page des menus )
		$this->getLayout()->setEnabled($layout);

		$us = new Model_Us();
    
		$network_anteroposterite = $us->getSuperpositionUs() ;
		$this->_view->set('network_anteroposterite', $network_anteroposterite );

		$network_synchronisation = $us->getSynchronisationUs() ;
		$this->_view->set('network_synchronisation', $network_synchronisation );

    	$fsn_categorie = new Model_Fsn_Categorie();
		$us_contexte = $fsn_categorie->fetchAll()->where('categorie_type = "us_contexte" ' );
		$this->_view->set('us_contexte', $us_contexte);
		/*
		 * Par defaut, la view affiche sera application/views/dashboard/us/us_network_cytoscape.html
		 * => convention d'arborescence deduit du nom du controller
		 * => convention du nom de fichier deduit du nom de l'action
		 *    (si Majuscule � l'interieur du nom de l'action, alors remplac� par un _ dans nom fichier html )
		 */
     $this->_view->setFile('View/dashboard/us/us_network_cola.html');

	}
	
	public function actionTotal($layout=false,$render=null){
		$this->getLayout()->setEnabled($layout);
			
		$session = $this->getSession();
		$sitefouille_id = $session->get('sitefouille_id')  ;
		$sitefouilles = new Model_Sitefouille($sitefouille_id) ;
		$sitefouille_info = $sitefouilles->fetchAll()->toRow() ;
		$this->_view->set('sitefouille_info', $sitefouille_info);
		
		$us = new Model_Us();
		$total = $us->getUsTotal();
		$this->_view->set('total_us', $total);
		$encours = $us->getUsEncours();
		$this->_view->set('encours_us', $encours);
		
		$this->_view->setFile('View/dashboard/us/total.html');
	}
	
	public function actionPhase($layout=false,$render=null){
		$this->getLayout()->setEnabled($layout);
			
		$session = $this->getSession();
		$sitefouille_id = $session->get('sitefouille_id')  ;
		$sitefouilles = new Model_Sitefouille($sitefouille_id) ;
		$sitefouille_info = $sitefouilles->fetchAll()->toRow() ;
		$this->_view->set('sitefouille_info', $sitefouille_info);
		
		$us = new Model_Us();
		$ustrati = $us->getUsPhase();
		$this->_view->set('phase_us', $ustrati);
		
		$this->_view->setFile('View/dashboard/us/phase.html');
	}
	
	public function actionContexte($layout=false,$render=null){
		 
		$this->getLayout()->setEnabled($layout);
		 
		$session = $this->getSession();
		$sitefouille_id = $session->get('sitefouille_id')  ;
		$sitefouilles = new Model_Sitefouille($sitefouille_id) ;
		$sitefouille_info = $sitefouilles->fetchAll()->toRow() ;
		$this->_view->set('sitefouille_info', $sitefouille_info);
		
		$us = new Model_Us();
		$ustrati = $us->getUsContexte();
		$this->_view->set('contexte_us', $ustrati);
		
		$this->_view->setFile('View/dashboard/us/contexte.html');
	}
	
	public function actionConsistance($layout=false,$render=null){
			
		$this->getLayout()->setEnabled($layout);
			
		$session = $this->getSession();
		$sitefouille_id = $session->get('sitefouille_id')  ;
		$sitefouilles = new Model_Sitefouille($sitefouille_id) ;
		$sitefouille_info = $sitefouilles->fetchAll()->toRow() ;
		$this->_view->set('sitefouille_info', $sitefouille_info);
			
		$us = new Model_Us();
		$ustrati = $us->getUsConsistance();
		$this->_view->set('consistance_us', $ustrati);
	
		$this->_view->setFile('View/dashboard/us/consistance.html');
	}
	
	public function actionMethode($layout=false,$render=null){
			
		$this->getLayout()->setEnabled($layout);
			
		$session = $this->getSession();
		$sitefouille_id = $session->get('sitefouille_id')  ;
		$sitefouilles = new Model_Sitefouille($sitefouille_id) ;
		$sitefouille_info = $sitefouilles->fetchAll()->toRow() ;
		$this->_view->set('sitefouille_info', $sitefouille_info);
			
		$us = new Model_Us();
		$ustrati = $us->getUsMethode();
		$this->_view->set('methode_us', $ustrati);

		$this->_view->setFile('View/dashboard/us/methode.html');
	}
	
	public function actionHomogeneite($layout=false,$render=null){
			
		$this->getLayout()->setEnabled($layout);
			
		$session = $this->getSession();
		$sitefouille_id = $session->get('sitefouille_id')  ;
		$sitefouilles = new Model_Sitefouille($sitefouille_id) ;
		$sitefouille_info = $sitefouilles->fetchAll()->toRow() ;
		$this->_view->set('sitefouille_info', $sitefouille_info);
			
		$us = new Model_Us();
		$ustrati = $us->getUsHomogeneite();
		$this->_view->set('homogeneite_us', $ustrati);
	
		$this->_view->setFile('View/dashboard/us/homogeneite.html');
	}
	
	public function actionUsNetworkModel($layout=false) {

		// true : le layout (page des menus ) s'affiche, false pas de layout (page des menus )
		$this->getLayout()->setEnabled($layout);

		$us = new Model_Us();

		$network_anteroposterite = $us->getSuperpositionUs() ;
		$this->_view->set('network_anteroposterite', $network_anteroposterite );

		$network_synchronisation = $us->getSynchronisationUs() ;
		$this->_view->set('network_synchronisation', $network_synchronisation );

		/*
		 * Par defaut, la view affiche sera application/views/dashboard/us/us_network.html
		 * => convention d'arborescence deduit du nom du controller
		 * => convention du nom de fichier deduit du nom de l'action
		 *    (si Majuscule � l'interieur du nom de l'action, alors remplac� par un _ dans nom fichier html )
		 */


	}
	
	public function actionHarrisGraph($layout=false) {

		// true : le layout (page des menus ) s'affiche, false pas de layout (page des menus )
		$this->getLayout()->setEnabled($layout);

		$us = new Model_Us();
	
	
	}
		
	public function actionExport($sitefouille_id=null, $export_type='xls', $export_mode=null){

		$session = $this->getSession();

		$sitefouille_id = (isset($_GET['sitefouille_id']) && !empty($_GET['sitefouille_id']) ) ? $_GET['sitefouille_id'] : $session->get('sitefouille_id');
		$export_type = (isset($_GET['export_type']) && !empty($_GET['export_type']) ) ? $_GET['export_type'] : 'xls' ;
		$export_mode = (isset($_GET['export_mode']) && !empty($_GET['export_mode']) ) ? $_GET['export_mode'] : null ;

		$sitefouille = new Model_Sitefouille(array($sitefouille_id));
		$sitefouille_nomabrege = $sitefouille->get('nomabrege');
		$name_file = 'liste_US_'.$sitefouille_nomabrege ;

		$us = new Model_Us();
		$liste_us = $us->getUsInfosExport() ;
		// transformation de l'objet en tableau
		$tab_us_sitefouille_data = array() ;
		foreach($liste_us as $us_value){
			$tab_us_sitefouille_data[] = $us_value->toArray();
			$tab_us_sitefouille_header = array_keys($us_value->toArray() );
		}
		// Definition du tableau de valeur
		$tab_sitefouille[] = array(
      'title' => 'liste_us',
      'header' => $tab_us_sitefouille_header,
      'data' => $tab_us_sitefouille_data,
		);

		if (!empty($export_mode) ) {
			// l'ecport se fait en mode global # US + ER
			$elementrecueilli = new Model_Elementrecueilli();
			$liste_er = $elementrecueilli->getErInfosExport() ;
			// transformation de l'objet en tableau
			$tab_er_sitefouille_data = array() ;
			foreach($liste_er as $er_value){
				$tab_er_sitefouille_data[] = $er_value->toArray();
				$tab_er_sitefouille_header = array_keys($er_value->toArray() );
			}
			$tab_sitefouille[] = array(
        'title' => 'liste_er',
        'header' => $tab_er_sitefouille_header,
        'data' => $tab_er_sitefouille_data,
			);

			// l'ecport se fait en mode global # US + ER + EP (element poterie ))
			$elementpoterie = new Model_Elementpoterie();
			$liste_ep = $elementpoterie->getEpInfosExport() ;
			$nb_elementpoterie = $liste_ep->count() ;
			//  vrif qu'il existe un element receuilli qui soit un element poterie
			if ( !empty($nb_elementpoterie) ){
				// transformation de l'objet en tableau
				$tab_ep_sitefouille_data = array() ;
				foreach($liste_ep as $ep_value){
					$tab_ep_sitefouille_data[] = $ep_value->toArray();
					$tab_ep_sitefouille_header = array_keys($ep_value->toArray() );
				}
				$tab_sitefouille[] = array(
          'title' => 'liste_ep',
          'header' => $tab_ep_sitefouille_header,
          'data' => $tab_ep_sitefouille_data,
				);
			}


		}

		$base_url = $this->getRequest()->getBaseUrl() ;
		$dirname = 'tmp/' ;

		$export_file_zip = $name_file.'.zip' ;

		if ( $export_type=='csv' ) {
			$export_file_csv = $name_file.'.csv' ;

			$export_liste_us_csv = new Yab_File_Csv($export_file_csv);
			$export_liste_us_csv->setDatas($tab_us_sitefouille_data);
			$export_liste_us_csv->write() ;
			$this->_response->download($export_liste_us_csv);


		} else if ( $export_type=='excel' || $export_type=='xls' ) {

			$this->_exportExcel($name_file, $tab_sitefouille );

		}
		return ;
	}


	/**
	 * Fonction pour transformer un tableau de valeur en Classeur Excel
	 * @param array( title, header, data )
	 * @result excel file
	 */
	private function _exportExcel($name_file,$export_data) {

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		foreach($export_data as $export_key => $export_value ){

			$title = $export_value['title'] ;
			$header = $export_value['header'] ;
			$data = $export_value['data'] ;
			// test si plusieur element du tableau $export_data # ajout onglet feuille excel
			if($export_key > 0 ) {
				$objWorksheet = $objPHPExcel->createSheet();
				$objPHPExcel->setActiveSheetIndex($export_key);
			}
			$NewWorkSheet = $objPHPExcel->getActiveSheet();
			$NewWorkSheet->setTitle($title) ;
			$first_cell = 'A1' ;
			if(!empty($header) ){
				// Recupere le nom des entetes de colonnes
				$first_letter = PHPExcel_Cell::stringFromColumnIndex(0);
				$last_letter = PHPExcel_Cell::stringFromColumnIndex(count($header)-1);
				$header_range = "{$first_letter}1:{$last_letter}1";
				$NewWorkSheet->getStyle($header_range)->getFont()->setBold(true);
				$NewWorkSheet->getStyle($header_range)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
				$NewWorkSheet->getStyle($header_range)->getFill()->getStartColor()->setRGB('AAAAAA');
				// Insert le nom des entetes de colonnes
				$NewWorkSheet->fromArray($header);
				$first_cell = 'A2' ;

			}
			// Insert les donn�es � partir de la ligne 2
			$NewWorkSheet->fromArray($data,null,$first_cell);

		}
		$export_name_file = $name_file.".xls" ;
		$ExcelWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="'. $export_name_file .'"');
		$ExcelWriter->save('php://output');


		return ;
	}

	private function _exportCsv($namefile,$data) {

		// initialisation du temps de traitement PHP, avec ajustement du temps de traitement
		$max_execution_time_ini = ini_get('max_execution_time') ;
		$memory_limit_ini = ini_get('memory_limit');
		if ( $max_execution_time_ini != 0 ) {
			// si max_execution_time = 0 (illimite), l'appel a ete fait par un batch
			ini_set('max_execution_time', 300);
		}
		$tab_entete_csv = array_keys($data[0]);
		 
		// ob_start();
		$fh = fopen($namefile, 'w');
		$ligne="";
		fputcsv($fh,$tab_entete_csv,";");
		foreach($data as $value_csv){
			fputcsv($fh,$value_csv,";");
		}
		fclose($fh);
		// ob_end_flush();

		//$config = Yab_Loader::getInstance()->getConfig();
		return ;

	}

	private function _create_zip_file($dir,$name_file,$name_zip_file){
		// compression du fichier export
		//$zip = new ZipArchive();
		if ($zip->open($dir.$name_zip_file) == TRUE) {
			if($zip->open($dir.$name_zip_file, ZipArchive::CREATE) == TRUE) {
				$zip->addFromString($name_file, file_get_contents($dir.$name_file));
			}
			$zip->close();
		}
		if (file_exists($dir.$name_zip_file) && filesize($dir.$name_zip_file) != 0 ) {
			return;
		} else {
			$this->_create_zip_file($dir,$name_file,$name_zip_file) ;
		}
	}

	/**
	 * fonction r�cursive pour d�finir une arborescence de l'US la plus r�cente (post�rieure) � l'US la plus ancienne(ant�rieure)
	 * @params : $td => tableau � traiter,
	 * @params : $parent => va etre apel� pour chercher les element d�pendant de l'�l�ment pr�c�demment trait�
	 * @params : $appelid => variable qui si d�finie d�finira l'id comme racine et cherchera les �lement enfants a partir de ce point
	 *
	 */
	private function _superposition( $list_anteroposterite, $parent=0, $level=0, $newtab = array()) {

		global $newtab;
		$newtab = array() ;
		$newtab_child = array() ;

		//Pour chaque �l�ment du tableau...
		foreach($list_anteroposterite as $key => $value) {
			$us_id = $value["anterieur_id"] ;
			$identification = $value["us_anterieur"] ;
			$us_parent_id = $value["posterieur_id"] ;
			$us_level = $value["level_anterieur"];

			/**
			 * Definition de 2 elements du tableau
			 */
			// Niveau n
			$newtab[$value["posterieur_id"]] = array(
        "us_id" => $value["posterieur_id"],
        "identification" => $value["us_posterieur"], 
        "us_parent_id" => 0,
			);
			$level_p = !empty( $newtab[$value["posterieur_id"]]['level'] ) ? $newtab[$value["posterieur_id"]]['level'] : $value["level_posterieur"] ;


			// Niveau n+1
			$newtab[$value["anterieur_id"]] = array(
        "us_id" => $value["anterieur_id"],
        "identification" => $value["us_anterieur"], 
        "us_parent_id" => $value["posterieur_id"]
			);
			//      "level" => $value["level_anterieur"],
			$level_a = !empty( $newtab[$value["anterieur_id"]]['level'] ) ? $newtab[$value["anterieur_id"]]['level'] : $value["level_anterieur"] ;
			 
			 
			// print "<pre> anterieur_id : ".$value["anterieur_id"]."; parent : ".$parent."; level : ".$level."</pre>" ;
			//si la fonction d�ja boucl� une premiere fois on cherche les objets enfant au parent appel�
			if( $value["anterieur_id"] == $parent ) {
				$newtab[$value["posterieur_id"]]['level'] = $level_p + $level ;
				$newtab[$value["anterieur_id"]]['level'] = $level_a + $level ;
				$level = $level + 1 ;
				$newtab_child = $this->_superposition($list_anteroposterite, $value["posterieur_id"], $level, $newtab); // et on rappelle la fonction

			} else {
				$newtab[$value["posterieur_id"]]['level'] = $level_p ;
				$newtab[$value["anterieur_id"]]['level'] = $level_a ;
			}

			// On merge le tableau
			$newtab = array_merge($newtab, $newtab_child);
		}

		return $newtab ;
	}

	/**
	 * fonction r�cursive pour d�finir une arborescence de l'US la plus r�cente (post�rieure) � l'US la plus ancienne(ant�rieure)
	 * @params : $td => tableau � traiter,
	 * @params : $parent => va etre apel� pour chercher les element d�pendant de l'�l�ment pr�c�demment trait�
	 * @params : $appelid => variable qui si d�finie d�finira l'id comme racine et cherchera les �lement enfants a partir de ce point
	 *
	 */
	private function _tree( $list_anteroposterite, $parent=0, $level=0, $newtab = array()) {

		global $newtab;
		$newtab = array() ;
		$newtab_child = array() ;

		// array to build the final hierarchy
		$tree = array(
        'children' => array(),
        'path'     => array()
		);

		// index array that references the inserted nodes
		// $index = array(0=>&$tree);
		$index = array();

		//Pour chaque �l�ment du tableau...
		foreach($list_anteroposterite as $key => $val) {

			// pick the parent node inside the tree by using the index
			$parent = &$index[$val['posterieur_id']] ;
			$parent = !empty( $parent ) ? $parent : array();
			$parent_path = !empty( $parent['path'] ) ? $parent['path'] : array();

			// append node to be inserted to the children array

			$node = array(
          'us_id' => $val['anterieur_id'],
          'identification' => $val['us_anterieur'],
          'us_parent_id' => $val['posterieur_id'],
          'posterieur_id' => $val['posterieur_id'],
          'path'      => $parent_path + array( $val['anterieur_id'] ), 
			);
			$level = &$parent['children'][$val['anterieur_id']]['level'] ;
			$node['level'] = $level + 1 ;
			$parent['children'][$val['anterieur_id']] = $node;
			$index_parent = &$parent['children'][$val['anterieur_id']] ;
			// insert/update reference to recently inserted node inside the tree
			$index[$val['anterieur_id']] = $node + $index_parent ;

		}
		 
		return $index ;
	}

}