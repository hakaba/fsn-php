<?php

class Controller_Dashboard_Sitefouille extends Yab_Controller_Action {

	public function actionIndex() {

        $session = $this->getSession(); 
        $sitefouille_id = $session->has('sitefouille_id') ? $session->get('sitefouille_id') : null ;
        $oa_id = $session->has('oa_id') ? $session->get('oa_id') : null ;
        
        // Controle l'existance sitefouille_id en session, sinon re-envoie vers la liste pour choisir  
        if (!empty($sitefouille_id) ) {
          
          // re-initialisation des redirections
          /*
          if ( $session->has('forward_controller') ) { $this->getSession()->rem('forward_controller'); }
          if ( $session->has('forward_action') ) { $this->getSession()->rem('forward_action'); }
          */
          $this->forward('Dashboard_Sitefouille', 'Show', array('sitefouille_id' => $sitefouille_id) );
        
        } else {
          $this->getSession()->set('flash_title', 'Suivi de chantiers'); 
          $this->getSession()->set('flash_status', 'warning'); 
          $this->getSession()->set('flash_message', 'Veuillez choisir un site de fouille');
          
          $this->getSession()->set('forward_controller', 'Dashboard_Sitefouille');
          $this->getSession()->set('forward_action', 'Show');
          
          $this->forward('Dashboard_Sitefouille', 'List');    
        }
        return ;
        
	}
    
    public function actionSetSessionValue($sitefouille_id, $oa_id=null) {

        $parameters = $this->_request->getParams();
        $session = $this->getSession();
    
        $session->set('sitefouille_id',$sitefouille_id );
        
        // remplace oa_id si nouveau, sinon test si oa_id existe dej� 
        $oa_id = (!empty($oa_id)) ? $oa_id : ( $session->has('oa_id') ? $session->get('oa_id') : null ) ;
        $session->set('oa_id',$oa_id);
        
        $forward_controller = $session->has('forward_controller') ? $session->get('forward_controller') : 'Dashboard_Sitefouille' ;
        $forward_action = $session->has('forward_action')? $session->get('forward_action') : 'Show' ;
    
        $this->forward($forward_controller, $forward_action);
    		
	}    
    
    public function actionList() {

		$sitefouille = new Model_Sitefouille();

		$sitefouilles = $sitefouille->fetchAll();

		$this->_view->set('sitefouilles', $sitefouilles);
	}
    
    public function actionShow() {

    $this->forward('Dashboard', 'Show');
    /*
		$session = $this->getSession();
    $sitefouille_id = $session->get('sitefouille_id'); 
    
    $sitefouille = new Model_Sitefouille($sitefouille_id);

    $this->getSession()->set('title_precision', $sitefouille->get('nom'));
		*/
    
	}
    
    public function actionIntervention() {
	
    $session = $this->getSession();
    $sitefouille_id = $session->get('sitefouille_id'); 
    
    /* Liste des intervenants */
    $intervenant = new Model_Intervenant();
    $intervenants = $intervenant->getIntervenantsActifs()->orderBy(array('nom'=> 'ASC') );
    
    $groups = array();
    $last_key = 0;
    foreach($intervenants as $key => $value ) {
      $group_intervenant = array (
        'id' => hash('crc32', $value->get('id') ) ,
        'content' => $nom = $value->get('nom').' '.$value->get('prenom') ,
        'value' => $value->get('id') ,
      );  
      $groups[$key] = $group_intervenant ;
      $last_key = $key+1 ;
    }
    $group_intervenant = array (
        'id' => hash('crc32', 'NA' ) ,
        'content' => 'Non Attribu&eacute;e',
        'value' => 'NA' ,
      );
    $groups[$last_key] = $group_intervenant ;  
    
    //$this->_view->set('intervenants', $planning_groups); 
    $this->_view->set('intervenants', $groups);
    
    /* Liste des intervations par us */
    $us_intervenant = new Model_Us();
    $us_intervenant_detail = $us_intervenant->getUsIntervenantsDetail()->where('sitefouille_id = "'.$sitefouille_id.'" ' ) ;
    
    $interventions = array() ;
    foreach($us_intervenant_detail as $key => $value ) {
      
      $us_id = $value->get('us_id') ;
      $intervention_id = $value->get('intervention_id') ;
      $intervenant_id = ( $value->has('intervenant_id') ) ? $value->get('intervenant_id') : 'NA' ;
      // $content = '<small><span data-toggle="tooltip" data-placement="right" title="'.$value->get('typeinterv').'"> '.$value->get('identification').'</span>';
      
      $content = '<small>';
       
      //$content .= ' <span id="detail_'.$us_id.'"><a href="#" target="new">d&eacute;tail US</span>' ; 
      if ( !empty($intervention_id) ) {
        $content .= ' <span id="detail_'.$us_id.'">';
        // $content .= ' <a href="'.$this->getRequest('intervention_us', 'edit', array($intervention_id) ) .'" target="_blank" title="d&eacute;tail US '.$value->get('identification').'">'.$value->get('identification').'</a></span>' ;
        $content .= '<a href="#" onClick="showDetailIntervention(\''.$intervention_id.'\')" >'.$value->get('identification').'</a></span>' ;
      } else {
        $params = array(
          'intervenant_id'=>null,
          'us_id'=>$us_id,          
          ) ;
        $content .= ' <span id="detail_'.$us_id.'">';
        $content .= ' <a href="'.$this->getRequest('intervention_us', 'add', $params ) .'" target="_blank" title="d&eacute;tail US '.$value->get('identification').'">'.$value->get('identification').'</a></span>' ;      
      }      
      $content .= ' <span>type '.$value->get('typeinterv').'</span>' ;
      //$content .= ' <span id="detail_'.$us_id.'"><a href="'.$this->getRequest('us', 'edit', array($us_id) ) .'" target="new">d&eacute;tail US</span>' ;
      $content .= '</small>';
      $inter_date_deb = ( $value->has('usi_debut') && $value->get('usi_debut') != '0000-00-00' ) ? $value->get('usi_debut') : null /*$value->get('us_debut')*/ ;
      // Si il n'y a pas de date de fin, la representation est un point
      $inter_date_fin = ( $value->has('usi_fin') && $value->get('usi_fin') != '0000-00-00' ) ? $value->get('usi_fin') : null ;
      
      
      $us_intervenants = array (
        'id' => $key, 
        'group' => hash('crc32', $intervenant_id ) ,
        'content' => $content
      );
        if ($inter_date_deb){
            $us_intervenants['start'] = $inter_date_deb;
            if($inter_date_fin)
                $us_timeline['end'] = $inter_date_fin;
        }else if($inter_date_fin)
            $us_intervenants['start'] = $inter_date_fin;

        if(isset($us_intervenants['start']))
            $interventions[] = $us_intervenants;
    }


    
    $this->_view->set('us_intervenants', $interventions);
    // $this->_view->set('us_intervenant_detail', $us_intervenant_detail);
    
    
    /* Definition des borne max et min du planning */    
	  $us_first_date = $us_intervenant->getFirstUsDateDeb()->where('sitefouille_id = "'.$sitefouille_id.'" ' )->toRow() ;
    $us_first_date_debut = (!empty($us_first_date['debut']) ) ? $us_first_date['debut'] : '2015-01-01' ;
    $this->_view->set('us_first_date', explode('-',$us_first_date_debut )); 
    
    $us_last_date = $us_intervenant->getFirstUsDateDeb()->where('sitefouille_id = "'.$sitefouille_id.'" ' )->toRow() ;
    $us_last_date_fin = (!empty($us_last_date['fin']) ) ? $us_last_date['fin'] : '2015-01-01' ;
    $this->_view->set('us_last_date', explode('-', $us_last_date_fin )); 

    $this->getLayout()->setEnabled(false);
    $this->_view->setFile('View/dashboard/sitefouille/intervention.html');

	} 
    
    public function actionShowParticipation() {

    $parameters = $this->_request->getParams();
    $session = $this->getSession();
    $session->set('sitefouille_id',$parameters[0]   );
    $session->set('oa_id',$parameters[1]   );

    $oa_id = $session->has('oa_id') ? $session->get('oa_id') : null ;
    
    if (!empty($oa_id) ) {      
      echo $this->getResponse('Dashboard_Sitefouille', 'Participation' );
    } else {
      echo $this->getResponse('Dashboard_Oa', 'Index');    
    }
    
    return ;
		
	}
    
	public function actionParticipation() {
	
    $session = $this->getSession();
    $sitefouille_id = $session->get('sitefouille_id');
    $oa_id = $session->get('oa_id');
    
    /* Liste des intervenants */
    $intervenant = new Model_Intervenant();
    
    $intervenants = $intervenant->fetchAll()->orderBy(array('nom'=> 'ASC') );
    
    $groups = array();
    
    foreach($intervenants as $key => $value ) {
      $group_intervenant = array (
        'id' => hash('crc32', $value->get('id') ) ,
        'content' => $nom = $value->get('nom').' '.$value->get('prenom') ,
        'value' => $value->get('id') ,
      );  
      $groups[$key] = $group_intervenant ;
      $last_key = $key+1 ;
    }
    $group_intervenant = array (
        'id' => hash('crc32', 'NA' ) ,
        'content' => 'Non Attribu&eacute;e',
        'value' => 'NA' ,
      );
    $groups[$last_key] = $group_intervenant ;  
    
    //$this->_view->set('intervenants', $planning_groups); 
    $this->_view->set('intervenants', $groups);
    
    /* Liste des participations par oa */
    $oa_intervenant = new Model_Oa();
    $oa_intervenant_detail = $oa_intervenant->getOaIntervenantsDetail() ;
    
    $participations = array() ;
    foreach($oa_intervenant_detail as $key => $value ) {
      
      $oa_id = $value->get('oa_id') ;
      $participation_id = $value->get('participation_id') ;
      $intervenant_id = ( $value->has('intervenant_id') ) ? $value->get('intervenant_id') : 'NA' ;

      // $content = '<small><span data-toggle="tooltip" data-placement="right" title="'.$value->get('typeinterv').'"> '.$value->get('identification').'</span>';
      $content = '<small>';
       
      //$content .= ' <span id="detail_'.$us_id.'"><a href="#" target="new">d&eacute;tail US</span>' ; 
      if ( !empty($participation_id) ) {
        $content .= ' <span id="detail_'.$oa_id.'" data-toggle="tooltip" data-placement="right" title="d&eacute;tail participation">';
        // $content .= ' <a href="'.$this->getRequest('Dashboard_Participation', 'show', array($participation_id) ) .'" >'.$value->get('identification').'</a></span>' ;
        $content .= '<a href="#" onClick="showDetailParticipation(\''.$participation_id.'\')" >'.$value->get('identification').'</a></span>' ;
      } else {
        $params = array(
          'intervenant_id'=>null,
          'oa_id'=>$oa_id,          
          ) ;
        $content .= ' <span id="detail_'.$oa_id.'">';
        // $content .= ' <a href="'.$this->getRequest('participation', 'add', $params ) .'" target="_blank" title="d&eacute;tail participation '.$value->get('identification').'">'.$value->get('identification').'</a></span>' ;
        $content .= '</span>' ;      
      }    
      $content .= ' <span>role '.$value->get('role').'</span>' ;
      //$content .= ' <span id="detail_'.$us_id.'"><a href="'.$this->getRequest('us', 'edit', array($us_id) ) .'" target="new">d&eacute;tail US</span>' ;
      $content .= '</small>';
      $part_date_deb = ( $value->has('part_debut') && $value->get('part_debut') != '0000-00-00' ) ? $value->get('part_debut') : null ;
      // Si il n'y a pas de date de fin, la representation est un point
      $part_date_fin = ( $value->has('part_fin') && $value->get('part_fin') != '0000-00-00' ) ? $value->get('part_fin') : null ;
      
      
      $oa_intervenants = array (
        'id' => $key, 
        'group' => hash('crc32', $intervenant_id ) ,
        'content' => $content
      );
        if ($part_date_deb){
            $oa_intervenants['start'] = $part_date_deb;
            if($part_date_fin)
                $us_timeline['end'] = $part_date_fin;
        }else if($part_date_fin)
            $oa_intervenants['start'] = $part_date_fin;

        if(isset($oa_intervenants['start']))
            $participations[] = $us_timeline;

    }
    var_dump($participations);
        die;
    
    $this->_view->set('oa_participations', $participations);
    // $this->_view->set('us_intervenant_detail', $us_intervenant_detail);
    
    
    /* Definition des borne max et min du planning */ 
		$oas = $oa_intervenant->fetchAll()->where('oa.id = "'.$oa_id.'" ' )->toRow() ;   
	  $oa_date_debut = $oas['debut'] ;
    $oa_date_fin = $oas['fin'] ;
    
    $this->_view->set('oa_date_debut', explode('-', $oa_date_debut) ); 
    $this->_view->set('oa_date_fin', explode('-', $oa_date_fin ) );
    
    $this->getLayout()->setEnabled(false);
        
    //$this->_view->setFile('View/dashboard/sitefouille/participation.html');

	}  

    public function actionGetMap($layout=false, $sitefouille_id=null, $render_type='json' ) {
	
    $this->getLayout()->setEnabled($layout);
    
    $session = $this->getSession();
    $sitefouille_id = (!empty($sitefouille_id) ) ? $sitefouille_id : $session->get('sitefouille_id');
    
    $sitefouille = new Model_Sitefouille($sitefouille_id);

		$sitefouilles = $sitefouille->fetchAll()->toRow() ;
    // $adresse # array( 'street', 'city', 'county', 'state', 'country', 'postalcode' ) => Adresse du site recherche
    $adresse = $sitefouilles['adresse'].' '.$sitefouilles['commune'].' '.$sitefouilles['departement'].',france' ;
    
    $OpenStreetMap = new Plugin_OpenStreetMap() ;
    
    $map = $OpenStreetMap->searchOsm($adresse, $render_type) ;
    
    if ( $render_type == 'xml') {
      // 
    } elseif ($render_type == 'json' || $render_type == 'jsonv2') {
      //
    } elseif ($render_type == 'html') {
      //
    } 
     	
    return $map ;

	}
  
    public function actionSetPOI($sitefouille_id=null, $lat, $lon, $alt=0 ){
    
    $OpenStreetMap = new Plugin_OpenStreetMap() ;
    $error = false ;
    $message = '' ;
    if (!empty($lat) ) {
      $lat = $OpenStreetMap->setLat($lat)->getLat() ;
    } else {
      $error = true ;
      $message .= 'Veuillez pr&eacute;ciser la Longitude du site de fouille <br>' ;  
    }
    if (!empty($lon) ) {
      $lon = $OpenStreetMap->setLon($lon)->getLon() ;
    } else {
      $error = true ;
      $message .= 'Veuillez pr&eacute;ciser la Longitude du site de fouille' ;
    }
    if ( $error ) {
      $this->getSession()->set('flash_title', 'Coordonn&eacute;es incomplete '); 
      $this->getSession()->set('flash_status', 'warning');       
      $this->getSession()->set('flash_message', $message );
    } 

  
  }

	public function actionStatistiques($layout=false) {
	
    $this->getLayout()->setEnabled($layout);
     	
    $this->_view->setFile('View/dashboard/sitefouille/statistiques.html');

	}  

    public function actionCloudpoints($layout=false) {
	
    $this->getLayout()->setEnabled($layout);
     	
    $this->_view->setFile('View/dashboard/sitefouille/cloud_points.html');

	} 
  	
    public function actionUs() {
	
    $this->getLayout()->setEnabled(false);
     	
    $this->_view->setFile('View/dashboard/sitefouille/us.html');

	}

	public function actionDashboard($layout=false) {
	
    $this->getLayout()->setEnabled($layout);
    
    $session = $this->getSession();
    $sitefouille_id = $session->get('sitefouille_id'); 
    
    $sitefouille = new Model_Sitefouille($sitefouille_id);    
    $this->_view->set('sitefouille', $sitefouille );
    
    $topo_sitefouille = $sitefouille->getTopoSfs() ;
    $this->_view->set('topo_sitefouille', $topo_sitefouille );
     	
    $this->_view->setFile('View/dashboard/sitefouille/dashboard.html');

	}
  
}