<?php

class Controller_Dashboard_Participation extends Yab_Controller_Action {

	public function actionIndex() {

		$participation = new Model_Participation();

		$participations = $participation->fetchAll();

		$this->_view->set('participations', $participations);
	}
  
public function actionExport($oa_id=null, $export_type='xls', $export_mode=null){
    
    $session = $this->getSession(); 
    
    $sitefouille_id = (isset($_GET['sitefouille_id']) && !empty($_GET['sitefouille_id']) ) ? $_GET['sitefouille_id'] : $session->get('sitefouille_id');
    $oa_id = (isset($_GET['oa_id']) && !empty($_GET['oa_id']) ) ? $_GET['oa_id'] : $session->get('oa_id');
    $export_type = (isset($_GET['export_type']) && !empty($_GET['export_type']) ) ? $_GET['export_type'] : 'xls' ;
    $export_mode = (isset($_GET['export_mode']) && !empty($_GET['export_mode']) ) ? $_GET['export_mode'] : null ;
    
    $oa = new Model_Oa(array($oa_id));
    $oa_identification = $oa->get('identification');
    $name_file = 'liste_Participation_'.$oa_identification ;

    
		$participation = new Model_Participation();

		$participations = $participation->getListParticipation();

    // transformation de l'objet en tableau 
    $tab_participations_data = array() ;
    foreach($participations as $participation_value){
      $tab_participations_data[] = $participation_value->toArray();
      $tab_participations_header = array_keys($participation_value->toArray() );
    }
    // Definition du tableau de valeur
    $tab_participations[] = array(
      'title' => 'liste_participations',
      'header' => $tab_participations_header,
      'data' => $tab_participations_data,
    );
    /*
    print '<pre>';
    print_r($tab_participations) ;
    print '</pre>';
    die ;
    */
 
    $base_url = $this->getRequest()->getBaseUrl() ;
    $dirname = 'tmp/' ;
    
    $export_file_zip = $name_file.'.zip' ;
    
    if ( $export_type=='csv' ) {
      $export_file_csv = $name_file.'.csv' ;

      $export_participations_csv = new Yab_File_Csv($export_file_csv);
      $export_participations_csv->setDatas($tab_participations_data);
      $export_participations_csv->write() ;
      $this->_response->download($export_participations_csv);
      
            
    } else if ( $export_type=='excel' || $export_type=='xls' ) {
      
      $this->_exportExcel($name_file, $tab_participations );
    
    }
    return ;
    
	}
  
	public function actionAdd() {
    /*
    print '<pre>';
    print_r($_POST);
    print '</pre>';
    return;
    */
    $this->getLayout()->setEnabled(false);
    
    if (isset($_POST) && !empty($_POST))
		{
      // Ajout du GUUID 
      $generateGuuid = new Plugin_Guuid() ;
      $guuid = $generateGuuid->GetUUID() ;
      
      		
      $oa_intervenant = array(
        'oa_id' => $guuid,  
        'intervenant_id' => $intervenant_id,
        'debut' => $part_date_deb,
        'fin' => $part_date_fin,  
      );
    }
		$participation = new Model_Participation();

    $mode = 'add' ;

		$form = new Form_Participation($participation, $mode);

		if($form->isSubmitted() && $form->isValid()) {
    
      // Ajout du GUUID 
      $generateGuuid = new Plugin_Guuid() ;
      $guuid = $generateGuuid->GetUUID() ;
      $form->setElement('id', array('type'=>'text', 'value' => $guuid ) ) ;

			$participation->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'participation as been added');

			$this->forward('Dashboard_Sitefouille', 'show', array('#participation') );

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionShow() {
    
    $this->getLayout()->setEnabled(false);
    
    $participation = new Model_Participation($this->_request->getParams());
    
    $mode = 'show' ;

		$form = new Form_Participation($participation, $mode);

    $this->_view->set('form', $form);
		
	}
  
	public function actionEdit() {
    
    $this->getLayout()->setEnabled(false);
		
    $participation = new Model_Participation($this->_request->getParams());

    $mode = 'edit' ;

		$form = new Form_Participation($participation, $mode);

		if($form->isSubmitted() && $form->isValid()) {

			$participation->populate($form->getValues())->save();

			// $this->getSession()->set('flash', 'participation as been edited');

			// $this->forward('Participation', 'index');
      //return ;

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));
    
	}

	public function actionDelete() {

		$participation = new Model_Participation($this->_request->getParams());

		$participation->delete();

		$this->getSession()->set('flash', 'participation as been deleted');

		$this->forward('Participation', 'index');

	}
  /**
   * Mise a jour d'une Participation 
   */
  public function actionUpdate() {
  
    $this->getLayout()->setEnabled(false);
    
    $session = $this->getSession();
    $sitefouille_id = $session->get('sitefouille_id');
    $oa_id = $session->get('oa_id');
  
    if (isset($_POST) && !empty($_POST)) {
      
    	print '<pre>';
      print_r($_POST);
      print '</pre>';
      
      /*
      foreach($_POST as $key => $value)                                        
			{
      
      }
      */
      
    }
     
     // $this->_SaveParticipation() ;
    
    // $this->_SaveParticipation() ;   
    

	}
  
  private function _SaveParticipation(){

    if (isset($_POST) && !empty($_POST))
		{
      print '<pre>';
      print_r($_POST);
      print '</pre>';
      
      /*
      foreach($_POST as $key => $value)                                        
			{
      
      }
      */
      
    }
  
  }
  /*
  * Fonction pour transformer un tableau de valeur en Classeur Excel 
  * @param array( title, header, data )
  * @result excel file    
  */
  private function _exportExcel($name_file,$export_data) {
  
    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();
    
    foreach($export_data as $export_key => $export_value ){

      $title = $export_value['title'] ;
      $header = $export_value['header'] ;
      $data = $export_value['data'] ;
      // test si plusieur element du tableau $export_data # ajout onglet feuille excel
      if($export_key > 0 ) {
        $objWorksheet = $objPHPExcel->createSheet();  
        $objPHPExcel->setActiveSheetIndex($export_key);
      }   
      $NewWorkSheet = $objPHPExcel->getActiveSheet();
      $NewWorkSheet->setTitle($title) ;
      $first_cell = 'A1' ;     
      if(!empty($header) ){
        // Recupere le nom des entetes de colonnes
        $first_letter = PHPExcel_Cell::stringFromColumnIndex(0);
        $last_letter = PHPExcel_Cell::stringFromColumnIndex(count($header)-1); 
        $header_range = "{$first_letter}1:{$last_letter}1";
        $NewWorkSheet->getStyle($header_range)->getFont()->setBold(true);
        $NewWorkSheet->getStyle($header_range)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $NewWorkSheet->getStyle($header_range)->getFill()->getStartColor()->setRGB('AAAAAA');
        // Insert le nom des entetes de colonnes
        $NewWorkSheet->fromArray($header);
        $first_cell = 'A2' ;
        
      }
      // Insert les données à partir de la ligne 2
      $NewWorkSheet->fromArray($data,null,$first_cell);
    
    }
    $export_name_file = $name_file.".xls" ;
    $ExcelWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'. $export_name_file .'"');
    $ExcelWriter->save('php://output');
    
    
    return ;   
  }
  

}