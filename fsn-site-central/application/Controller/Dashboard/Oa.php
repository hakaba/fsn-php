<?php

class Controller_Dashboard_Oa extends Yab_Controller_Action {

	public function actionIndex($layout=true) {

    $session = $this->getSession(); 
    $sitefouille_id = $session->has('sitefouille_id') ? $session->get('sitefouille_id') : null ;

    if (!empty($sitefouille_id) ) {
    
      // re-initialisation des redirections
      if ( $session->has('forward_controller') ) { $this->getSession()->rem('forward_controller'); }
      if ( $session->has('forward_action') ) { $this->getSession()->rem('forward_action'); }
      
      $this->forward('Dashboard_Oa', 'List', array('layout' => $layout) );
    } else {
      $this->getSession()->set('flash_title', 'Suivi de chantiers'); 
      $this->getSession()->set('flash_status', 'warning'); 
      $this->getSession()->set('flash_message', 'Veuillez choisir un site de fouille');
      
      $this->getSession()->set('forward_controller', 'Dashboard_Oa');
      $this->getSession()->set('forward_action', 'List');
      
      $this->forward('Dashboard_Sitefouille', 'List');    
    }
    return ; 
    
	}

	public function actionList($layout=true) {

    $this->getLayout()->setEnabled($layout);
    
    $oa = new Model_Oa();

		// $oas = $oa->fetchAll();
    $oas = $oa->getOaDetail();

		$this->_view->set('oas', $oas);

	}

	public function actionCalendar($layout=false) {

    $this->getLayout()->setEnabled($layout);
    
    $session = $this->getSession();
    $sitefouille_id = $session->get('sitefouille_id');
    $oa_id = $session->get('oa_id');
    
    /* Liste des intervenants */
    $intervenant = new Model_Intervenant();
    $intervenants = $intervenant->fetchAll()->orderBy(array('nom'=> 'ASC') );
    $this->_view->set('intervenants', $intervenants);
    
    /* Definition des borne max et min du planning */ 
		$participation = new Model_Participation();
    $participations = $participation->getFirstParticipationDateDeb()->toRow();
    $oa_date_debut = $participations->get('debut') ;
    $this->_view->set('oa_date_debut', $oa_date_debut ); 
    
    /* Liste des participations par oa */
    $oa_intervenant = new Model_Oa();
    $oa_intervenant_detail = $oa_intervenant->getOaIntervenantsDetail() ;
    
    $participations = array() ;
    foreach($oa_intervenant_detail as $key => $value ) {
      
      $participation_id = $value->get('participation_id') ;
      $oa_id = $value->get('oa_id') ;
      $intervenant_id = ( $value->has('intervenant_id') ) ? $value->get('intervenant_id') : 'NA' ;
      $role_id = $value->get('role_id') ;
      $role = $value->get('role') ;
      $part_date_deb = ( $value->has('part_debut') && $value->get('part_debut') != '0000-00-00' ) ? $value->get('part_debut') : $value->get('oa_debut') ;
      // Si il n'y a pas de date de fin, la representation est un point
      $part_date_fin = ( $value->has('part_fin') && $value->get('part_fin') != '0000-00-00' ) ? $value->get('part_fin') : '' ;
      $title = $value->get('nom')." ".$value->get('prenom') ;
      
      $oa_intervenants = array (
        'id' => $participation_id, 
        'title' => $title,
        'role_id' =>  $role_id,
        'role' =>  $role,
        'start' => $part_date_deb
      );
      // Si il n'y a pas de date de fin, la representation est un point
      if(!empty($part_date_fin) ) { 
        $oa_intervenants['end']= $part_date_fin ; 
      }
      
      if ( !empty($participation_id) ) {
        $url = $this->getRequest()->getBaseUrl().$this->getRequest('Dashboard_Participation', 'edit', array($participation_id) )->getUri() ;
        // $oa_intervenants['url']= $url ;
      }
      
      $participations[$key] = $oa_intervenants ;
        
    }

		$this->_view->set('participations', $participations);

	}
	
    public function actionPlanning($layout=false) {

        $this->getLayout()->setEnabled($layout);
        $moa = new Model_Oa();
        $sitefouille_id = Yab_Loader::getInstance()->getSession()->get('sitefouille_id');
        $oas = $moa->getOaDetail()->where('sitefouille_id = "'.$sitefouille_id.'"')->setKey('id')->setValue('identification')->toArray();
        $this->_view->set('oas', $oas);
        $this->_view->setFile('View/dashboard/participation/planning.html');

    }

	public function actionParticipationsAjax($render='calendar') {
    $this->getLayout()->setEnabled(false);
    
    $session = $this->getSession();
    $oa_id = isset($_POST['oa_id'])?$_POST['oa_id']:null;

    /* Liste des participations par oa */
    $oa_intervenant = new Model_Oa();
    $oa_intervenant_detail = $oa_intervenant->getOaIntervenantsDetail($oa_id) ;
    
    $participations = array() ;
    foreach($oa_intervenant_detail as $key => $value ) {
      $participation_id = $value->get('participation_id') ;
      $role_id = $value->get('role_id') ;
      $role = $value->get('role') ;
      $title = $value->get('nom')." ".$value->get('prenom') ;
      
      if ($render == 'planning') {
        $value->set('format','%d/%m/%Y') ;
        $part_date_deb = ( $value->has('part_debut') && $value->get('part_debut') != '0000-00-00' ) ? $value->get('part_debut','Date') : $value->get('oa_debut','Date') ;
        // Si il n'y a pas de date de fin, la representation est un point
        $part_date_fin = ( $value->has('part_fin') && $value->get('part_fin') != '0000-00-00' ) ? $value->get('part_fin','Date') : '' ;
        /*$ganttColor = array(
          0 => '',
          1 => 'ganttRed',
          2 => 'ganttBlue',
          3 => 'ganttGreen',
          4 => 'ganttOrange',
          5 => '',
          999 => '', 
        );*/
        
        $oa_intervenants = array (        
          'name' => ' ',
          'desc' => $title,
          'values' => array(
            'from' => $part_date_deb,
            'to' => $part_date_fin,
            'label' => $role,
            'role_id' =>  $role_id,
            'customClass' => 'ganttBlue',

          ),
        );
      } else {
        $value->set('format','%Y-%m-%d') ;
        $part_date_deb = ( $value->has('part_debut') && $value->get('part_debut') != '0000-00-00' ) ? $value->get('part_debut','Date') : $value->get('oa_debut','Date') ;
        // Si il n'y a pas de date de fin, la representation est un point
        $part_date_fin = ( $value->has('part_fin') && $value->get('part_fin') != '0000-00-00' ) ? $value->get('part_fin','Date') : '' ;

        // $render = 'calendar'      
        $oa_intervenants = array (
          'id' => $participation_id, 
          'title' => $title,
          'role_id' =>  $role_id,
          'role' =>  $role,
          'start' => $part_date_deb
        );
        // Si il n'y a pas de date de fin, la representation est un point
        if(!empty($part_date_fin) ) { 
          $oa_intervenants['end']= $part_date_fin ; 
        }
        
        if ( !empty($participation_id) ) {
          $url = $this->getRequest()->getBaseUrl().$this->getRequest('Dashboard_Participation', 'edit', array($participation_id) )->getUri() ;
          // $oa_intervenants['url']= $url ;
        }
      
      }
      
      
      $participations[$key] = $oa_intervenants ;
        
    }

		$participation_participations = json_encode($participations, JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE) ;

        print $participation_participations ;
	}
	
    
	public function actionAdd() {

		$oa = new Model_Oa();

		$form = new Form_Oa($oa);

		if($form->isSubmitted() && $form->isValid()) {

			$oa->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'oa as been added');

			$this->forward('Oa', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$oa = new Model_Oa($this->_request->getParams());

		$form = new Form_Oa($oa);

		if($form->isSubmitted() && $form->isValid()) {

			$oa->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'oa as been edited');

			$this->forward('Oa', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$oa = new Model_Oa($this->_request->getParams());

		$oa->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'oa as been deleted');

		$this->forward('Oa', 'index');

	}

}