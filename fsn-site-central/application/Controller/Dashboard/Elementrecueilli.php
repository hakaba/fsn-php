<?php

class Controller_Dashboard_Elementrecueilli extends Yab_Controller_Action {

	public function actionIndex() {

    $session = $this->getSession(); 
    $sitefouille_id = $session->has('sitefouille_id') ? $session->get('sitefouille_id') : null ;

    if (!empty($sitefouille_id) ) {
    
      // re-initialisation des redirections
      if ( $session->has('forward_controller') ) { $this->getSession()->rem('forward_controller'); }
      if ( $session->has('forward_action') ) { $this->getSession()->rem('forward_action'); }
      
      $this->forward('Dashboard_Elementrecueilli', 'List', array('sitefouille_id' => $sitefouille_id) );
    } else {
      $this->getSession()->set('flash_title', 'Suivi de chantiers'); 
      $this->getSession()->set('flash_status', 'warning'); 
      $this->getSession()->set('flash_message', 'Veuillez choisir un site de fouille');
      
      $this->getSession()->set('forward_controller', 'Dashboard_Elementrecueilli');
      $this->getSession()->set('forward_action', 'List');
      
      $this->forward('Dashboard_Sitefouille', 'List');    
    }
    return ; 

	}

	public function actionList() {

		$elementrecueilli = new Model_Elementrecueilli();

		// $elementrecueillis = $elementrecueilli->fetchAll();
    $elementrecueillis = $elementrecueilli->getErDetail();

		$this->_view->set('elementrecueillis', $elementrecueillis);
	}
    
  public function actionCount($layout=false) {

    $this->getLayout()->setEnabled($layout);
    
		$elementrecueilli = new Model_Elementrecueilli();
    
    // le filtre sur sitefouille_id est automatique dans listSdfEr() 
		$elementrecueillis = $elementrecueilli->listSdfEr() ;
    
    $er_count = $elementrecueillis->count() ;

    print $er_count ;

		return ;
    
	}

  public function actionTraitement($layout=false) {
  
    $this->getLayout()->setEnabled($layout);
    
    $elementrecueilli = new Model_Elementrecueilli();

		$elementrecueillis = $elementrecueilli->listSdfEr()->where('sitefouille_id = "'.$sitefouille_id.'" ' ) ;
    
    $er_count = $elementrecueillis->count() ;

    print $er_count ;

		return ;
    
	}
  
  public function actionErTimeline($layout=false) {
	
    // true : le layout (page des menus ) s'affiche, false pas de layout (page des menus )
    $this->getLayout()->setEnabled($layout);
    
    $elementrecueilli = new Model_Elementrecueilli();

		$elementrecueillis = $elementrecueilli->listSdfEr() ;
    
    $this->_view->set('elementrecueillis', $elementrecueillis);
    
    $this->_view->setFile('View/dashboard/elementrecueilli/er_timeline.html');
    

	}

  public function actionStatutNetwork($layout=false ,$render=null) {
  
    $this->getLayout()->setEnabled($layout);

		$fsn_categorie = new Model_Fsn_Categorie();

		$fsn_categories = $fsn_categorie->fetchAll()->where('categorie_type = "elementrecueilli_statut" ')->orderBy('ordre') ;

    $this->_view->set('statuts', $fsn_categories);    

	}
  public function actionIsole($layout=false){
  	$this->getLayout()->setEnabled($layout);
  	$session = $this->getSession(); 
    $sitefouille_id = $session->get('sitefouille_id')  ;
    $sitefouilles = new Model_Sitefouille($sitefouille_id) ;
    $sitefouille_info = $sitefouilles->fetchAll()->toRow() ;    
    $this->_view->set('sitefouille_info', $sitefouille_info);    

	$elementrecueilli = new Model_Elementrecueilli();
	$elementrecueillis = $elementrecueilli->getErIsoleCounts() ;
    $this->_view->set('isole_elementrecueillis', $elementrecueillis);
    $this->_view->setFile('View/dashboard/elementrecueilli/isole.html');
    
  }
  public function actionIntact($layout=false){
  	
  	$this->getLayout()->setEnabled($layout);
  	
  	$session = $this->getSession(); 
    $sitefouille_id = $session->get('sitefouille_id')  ;
    $sitefouilles = new Model_Sitefouille($sitefouille_id) ;
    $sitefouille_info = $sitefouilles->fetchAll()->toRow() ;    
    $this->_view->set('sitefouille_info', $sitefouille_info);   
  	
  	$elementrecueilli = new Model_Elementrecueilli();
	$elementrecueillis = $elementrecueilli->getErIntactCounts();
  	
  	$this->_view->set('intact_elementrecueillis',$elementrecueillis);
  	$this->_view->setFile('View/dashboard/elementrecueilli/intact.html');
  }
public function actionStatut($layout=false ,$render=null) {
  
    $this->getLayout()->setEnabled($layout);
    
    $session = $this->getSession(); 
    $sitefouille_id = $session->get('sitefouille_id')  ;
    $sitefouilles = new Model_Sitefouille($sitefouille_id) ;
    $sitefouille_info = $sitefouilles->fetchAll()->toRow() ;    
    $this->_view->set('sitefouille_info', $sitefouille_info);    
    
	$elementrecueilli = new Model_Elementrecueilli();
	$elementrecueillis = $elementrecueilli->getErStatutAndNullCounts() ;
    $this->_view->set('statut_elementrecueillis', $elementrecueillis);

    switch($render){
    	case"pie":
    		$this->_view->setFile('View/dashboard/elementrecueilli/statut_pie.html');
    		break;
    	case"bar":
    		$this->_view->setFile('View/dashboard/elementrecueilli/statut_bar.html');
    		break;
    	case"list":
    		$this->_view->setFile('View/dashboard/elementrecueilli/statut_liste.html');
    		break;
    	default:
    		$this->_view->setFile('View/dashboard/elementrecueilli/statut.html');
    		break;
    }
}
  
  public function actionStatutSuivi($layout=false ,$render=null) {
  
    $this->getLayout()->setEnabled($layout);
    
    $session = $this->getSession(); 
    $sitefouille_id = $session->get('sitefouille_id')  ;
    $sitefouilles = new Model_Sitefouille($sitefouille_id) ;
    $sitefouille_info = $sitefouilles->fetchAll()->toRow() ;    
    $this->_view->set('sitefouille_info', $sitefouille_info);
    
    $statut = new Model_Statut();
    $statuts = $statut->fetchAll()->where('visible = 1 ' );
    $this->_view->set('statuts', $statuts);    

		$elementrecueilli = new Model_Elementrecueilli();
		$elementrecueillis = $elementrecueilli->listSdfEr() ;
    // $this->_view->set('statut_elementrecueillis', $elementrecueillis);

    $statut_suivi = array() ;
    foreach($elementrecueillis as $key_er => $value_er){
      $er_id = $value_er->get('er_id');
      $er_statut_id = $value_er->get('statut_id');
      $statut_suivi[$key_er] = $value_er->toArray() ;
      foreach($statuts as $key_statut => $value_statut){
        $statut_id = $value_statut->get('id'); 
        $active = ( $er_statut_id == $statut_id ) ?  'actif' : '' ;
        $statut_suivi[$key_er]['statut'][$statut_id] = array( 
          'libelle' => $value_statut->get('statut'),
          'active' => $active 
        ) ;      
      }
    
    }
    $this->_view->set('statut_suivi', $statut_suivi);
    
    // Par defaut la view est View/Dashboard/Elementrecueilli/Statut.html
    if ($render=='excel' || $render=='xls' ) {

      // Export Excel

    } else {
      $this->_view->setFile('View/dashboard/elementrecueilli/statut_suivi.html');
    }  
	}
	
	public function actionClasse($layout=false,$render=null){
		
		$this->getLayout()->setEnabled($layout);
		
		$session = $this->getSession();
		$sitefouille_id = $session->get('sitefouille_id');
		$sitefouilles = new Model_Sitefouille($sitefouille_id);
		$sitefouille_info = $sitefouilles->fetchAll()->toRow();
		$this->_view->set('sitefouille_info',$sitefouille_info);
	
		$elementrecueilli = new Model_Elementrecueilli();
		$elementrecueillis = $elementrecueilli->getErClasseAndNullCounts();
		$this->_view->set('classe_elementrecueillis',$elementrecueillis);
	
		// Par defaut la view est View/Dashboard/Elementrecueilli/Classe.html
		switch($render){
			case "pie":
				$this->_view->setFile('View/dashboard/elementrecueilli/classe_pie.html');
				break;
			case "bar":
				$this->_view->setFile('View/dashboard/elementrecueilli/classe_bar.html');
				break;
			case "list":
				$this->_view->setFile('View/dashboard/elementrecueilli/classe_liste.html');
				break;
			default:
				$this->_view->setFile('View/dashboard/elementrecueilli/classe.html');
				break;
		}		
	}
public function actionProduction($layout=false,$render=null){
  	
  	$this->getLayout()->setEnabled($layout);
  	
  	$session = $this->getSession();
  	$sitefouille_id = $session->get('sitefouille_id')  ;
  	$sitefouilles = new Model_Sitefouille($sitefouille_id) ;
  	$sitefouille_info = $sitefouilles->fetchAll()->toRow() ;
  	$this->_view->set('sitefouille_info', $sitefouille_info);
  	
  	$elementpoterie = new Model_Elementpoterie();
  	$elementpoteries = $elementpoterie->getEpProduction() ;
  	$this->_view->set('production_elementpoteries', $elementpoteries);
  	
  	if ($render=='pie') {
  		$this->_view->setFile('View/dashboard/elementrecueilli/production_pie.html');
  	} else if($render=='bar'){
  		$this->_view->setFile('View/dashboard/elementrecueilli/production_bar.html');
  	} else if($render=='list'){
  		$this->_view->setFile('View/dashboard/elementrecueilli/production_liste.html');
  	}
  	else {
  		$this->_view->setFile('View/dashboard/elementrecueilli/production.html');
  	}
  }
  
  public function actionForme($layout=false,$render=null){
  	 
  	$this->getLayout()->setEnabled($layout);
  	 
  	$session = $this->getSession();
  	$sitefouille_id = $session->get('sitefouille_id')  ;
  	$sitefouilles = new Model_Sitefouille($sitefouille_id) ;
  	$sitefouille_info = $sitefouilles->fetchAll()->toRow() ;
  	$this->_view->set('sitefouille_info', $sitefouille_info);
  	 
  	$elementpoterie = new Model_Elementpoterie();
  	$elementpoteries = $elementpoterie->getEpForme() ;
  	$this->_view->set('production_elementpoteries', $elementpoteries);
  	 
  	if ($render=='pie') {
  		$this->_view->setFile('View/dashboard/elementrecueilli/forme_pie.html');
  	} else if($render=='bar'){
  		$this->_view->setFile('View/dashboard/elementrecueilli/forme_bar.html');
  	} else if($render=='list'){
  		$this->_view->setFile('View/dashboard/elementrecueilli/forme_liste.html');
  	}
  	else {
  		$this->_view->setFile('View/dashboard/elementrecueilli/forme.html');
  	}
  }
  
  public function actionClassification($layout=false,$render=null){
  
  	$this->getLayout()->setEnabled($layout);
  
  	$session = $this->getSession();
  	$sitefouille_id = $session->get('sitefouille_id')  ;
  	$sitefouilles = new Model_Sitefouille($sitefouille_id) ;
  	$sitefouille_info = $sitefouilles->fetchAll()->toRow() ;
  	$this->_view->set('sitefouille_info', $sitefouille_info);
  
  	$elementpoterie = new Model_Elementpoterie();
  	$elementpoteries = $elementpoterie->getEpClassification() ;
  	$this->_view->set('classification_elementpoteries', $elementpoteries);
  
  	if ($render=='pie') {
  		$this->_view->setFile('View/dashboard/elementrecueilli/classification_pie.html');
  	} else if($render=='bar'){
  		$this->_view->setFile('View/dashboard/elementrecueilli/classification_bar.html');
  	} else if($render=='list'){
  		$this->_view->setFile('View/dashboard/elementrecueilli/classification_liste.html');
  	}
  	else {
  		$this->_view->setFile('View/dashboard/elementrecueilli/classification.html');
  	}
  }
	public function actionCaracteristique($layout=false,$render=null){
		$this->getLayout()->setEnabled($layout);
		
		$session = $this->getSession();
		$sitefouille_id=$session->get('sitefouille_id');
		$sitefouilles = new Model_Sitefouille($sitefouille_id);
		$sitefouille_info = $sitefouilles->fetchAll()->toRow();
		$this->_view->set('sitefouille_info',$sitefouille_info);
		
		$elementpoterie = new Model_Elementpoterie();
		$elementpoteries = $elementpoterie->getEpCaracteristiqueCounts();
		$this->_view->set('caracteristique_elementpoteries',$elementpoteries);
		
		switch($render){
			case "pie":
				$this->_view->setFile('View/dashboard/elementrecueilli/caracteristique_pie.html');
				break;
			case "bar":
				$this->_view->setFile('View/dashboard/elementrecueilli/caracteristique_bar.html');
				break;
			case"list":
				$this->_view->setFile('View/dashboard/elementrecueilli/caracteristique_liste.html');
				break;
			default:
				$this->_view->setFile('View/dashboard/elementrecueilli/caracteristique.html');
				break;
		}
	}
	
public function actionEtat($layout=false ,$render=null) {
  
	$this->getLayout()->setEnabled($layout);

    $session = $this->getSession(); 
    $sitefouille_id = $session->get('sitefouille_id')  ;
    $sitefouilles = new Model_Sitefouille($sitefouille_id) ;
    $sitefouille_info = $sitefouilles->fetchAll()->toRow() ;    
    $this->_view->set('sitefouille_info', $sitefouille_info);    
      
	$elementrecueilli = new Model_Elementrecueilli();		
	$elementrecueillis = $elementrecueilli->getErEtatAndNullCounts() ;
    $this->_view->set('etat_elementrecueillis', $elementrecueillis);

    switch($render){
    	case"pie":
    		$this->_view->setFile('View/dashboard/elementrecueilli/etat_pie.html');
    		break;
    	case"bar":
    		$this->_view->setFile('View/dashboard/elementrecueilli/etat_bar.html');
    		break;
    	case"list":
    		$this->_view->setFile('View/dashboard/elementrecueilli/etat_liste.html');
    		break;
    	default:
    		$this->_view->setFile('View/dashboard/elementrecueilli/etat.html');
    		break;
    }
}
	
	public function actionNaturematiere($layout=false,$render=null){
		 
		$this->getLayout()->setEnabled($layout);
		 
		$session = $this->getSession();
		$sitefouille_id = $session->get('sitefouille_id');
		$sitefouilles = new Model_Sitefouille($sitefouille_id);
		$sitefouille_info = $sitefouilles->fetchAll()->toRow();
		$this->_view->set('sitefouille_info',$sitefouille_info);
		 
		$elementrecueilli = new Model_Elementrecueilli();
		$elementrecueillis = $elementrecueilli->getErNatureMatierePrincipaleCounts();
		$this->_view->set('naturematiere_elementrecueillis',$elementrecueillis);
		
		
		switch($render){
			case"pie":
				$this->_view->setFile('View/dashboard/elementrecueilli/naturematiere_pie.html');
				break;
			case"bar":
				$this->_view->setFile('View/dashboard/elementrecueilli/naturematiere_bar.html');
				break;
			case"list":
				$this->_view->setFile('View/dashboard/elementrecueilli/naturematiere_liste.html');
				break;
			default:
				$this->_view->setFile('View/dashboard/elementrecueilli/naturematiere.html');
				break;
		}
	}
	
	public function actionFonction($layout = false,$render = null){
		$this->getLayout()->setEnabled($layout);

    $session = $this->getSession(); 
    $sitefouille_id = $session->get('sitefouille_id')  ;
    $sitefouilles = new Model_Sitefouille($sitefouille_id) ;
    $sitefouille_info = $sitefouilles->fetchAll()->toRow() ;    
    $this->_view->set('sitefouille_info', $sitefouille_info);    
		
		$elementrecueilli = new Model_Elementrecueilli();
		$elementrecueillis = $elementrecueilli->getErFonctionAndNullCounts();
		$this->_view->set('fonction_elementrecueillis',$elementrecueillis);
		
		switch($render){
			case"pie":
				$this->_view->setFile('View/dashboard/elementrecueilli/fonction_pie.html');
				break;
			case"bar":
				$this->_view->setFile('View/dashboard/elementrecueilli/fonction_bar.html');
				break;
			case"list":
				$this->_view->setFile('View/dashboard/elementrecueilli/fonction_liste.html');
				break;
			default:
				$this->_view->setFile('View/dashboard/elementrecueilli/fonction.html');
				break;	
		}
	}
	
	public function actionAjaxDatation(){
		$this->getLayout()->setEnabled(false);
		$session = $this->getSession();
		
		$sitefouille_id = $session->get('sitefouille_id')  ;
		$sitefouilles = new Model_Sitefouille($sitefouille_id) ;
		$sitefouille_info = $sitefouilles->fetchAll()->toRow() ;
		$this->_view->set('sitefouille_info', $sitefouille_info);
		
		$this->_view->setEnabled(false);
		// Variable à false par défaut.
		$estCertaine = (isset($_POST['estCertaine'])) ? $_POST['estCertaine'] : false;
		$estParFin = (isset($_POST['estParFin'])) ? $_POST['estParFin'] : false;
		$intervalle = (isset($_POST['intervalle'])) ? $_POST['intervalle'] : 200;
		$finHisto = (isset($_POST['finHisto'])) ? $_POST['finHisto'] : 2016;
		$debutHisto = (isset($_POST['debutHisto'])) ? $_POST['debutHisto'] : 0;
		
		
		
		$elementrecueilli = new Model_Elementrecueilli();
		// Par datation certaines et par date de fin
		
		$elementrecueillis = null;
		
		if($estCertaine == "true" && $estParFin == "true"){
			$elementrecueillis = $elementrecueilli->getErDatationFinCertain();
			
		}
		// Par datation estimée et par date de fin
		if($estCertaine == "false" && $estParFin == "true"){
			$elementrecueillis = $elementrecueilli->getErDatationFinEstime();
		}
		// Par datation certaine et par date de début
		if($estCertaine == "true" && $estParFin == "false"){
			$elementrecueillis = $elementrecueilli->getErDatationDebutCertain();
			
		}
		// Par datation estimée et par date de début
		if($estCertaine == "false" && $estParFin == "false"){
			$elementrecueillis = $elementrecueilli->getErDatationDebutEstime();
			
		}
		foreach($elementrecueillis as $er_datation) {
			if($er_datation->get('datation') != "NA") {
				$data[] = array($er_datation->get('datation'),$er_datation->get('nb_er'));
			}
		}
		$tab_er = array();
		for ($i = $debutHisto; $i <= $finHisto; $i=$i+$intervalle) {
			$nb_er = 0;
			for ($j = 0; $j < count($data); $j++) {
				if ($data[$j][0] >= $i && $data[$j][0] < $i + $intervalle ) {
					$nb_er = $nb_er + $data[$j][1];
				}	
			}
			if ($nb_er != 0) {
				$tab_er[] = array($i,$nb_er);
			}
		}	
		echo json_encode($tab_er, JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
	}
	
	public function actionDatation($layout = false, $render = null){
		$this->getLayout()->setEnabled($layout);
		
		$session = $this->getSession();
		
		
		$sitefouille_id = $session->get('sitefouille_id')  ;
   	 	$sitefouilles = new Model_Sitefouille($sitefouille_id) ;
    	$sitefouille_info = $sitefouilles->fetchAll()->toRow() ;    
   	 	$this->_view->set('sitefouille_info', $sitefouille_info);    
		
   	 	$elementrecueilli = new Model_Elementrecueilli();
   	 	$elementrecueillis = $elementrecueilli->getErDatationFinEstime();
   	 	$this->_view->set('datation_elementrecueillis',$elementrecueillis);
   	 	
   	 	// Mantis 361 début
   	 	$elementrecueillis = $elementrecueilli->getErDatationDebutEstime();
   	 	$this->_view->set('datation_elementrecueillis_DE',$elementrecueillis);
   	 	$elementrecueillis = $elementrecueilli->getErDatationFinCertain();
   	 	$this->_view->set('datation_elementrecueillis_FC',$elementrecueillis);
   	 	$elementrecueillis = $elementrecueilli->getErDatationDebutCertain();
   	 	$this->_view->set('datation_elementrecueillis_DC',$elementrecueillis);
   	 	// Mantis 361 fin
   	 	
		if($render == 'bar'){
			$this->_view->setFile('View/dashboard/elementrecueilli/datation_bar.html');
		}
		else{
			$this->_view->setFile('View/dashboard/elementrecueilli/datation.html');
		}	

	}
	
	public function actionAdd() {

		$elementrecueilli = new Model_Elementrecueilli();

    $mode = 'add' ;
    
		$form = new Form_Elementrecueilli($elementrecueilli, $mode);

		if($form->isSubmitted() && $form->isValid()) {

			$elementrecueilli->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'elementrecueilli as been added');

			$this->forward('Dashboard_Elementrecueilli', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$elementrecueilli = new Model_Elementrecueilli($this->_request->getParams());

    $mode = 'edit' ;
    
		$form = new Form_Elementrecueilli($elementrecueilli, $mode);


		if($form->isSubmitted() && $form->isValid()) {

			$elementrecueilli->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'elementrecueilli as been edited');

			$this->forward('Dashboard_Elementrecueilli', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionShow() {

		$elementrecueilli = new Model_Elementrecueilli($this->_request->getParams());

    $mode = 'edit' ;
    
		$form = new Form_Elementrecueilli($elementrecueilli, $mode);


		if($form->isSubmitted() && $form->isValid()) {

			$elementrecueilli->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'elementrecueilli as been edited');

			$this->forward('Dashboard_Elementrecueilli', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}
/*
	public function actionDelete() {

		$elementrecueilli = new Model_Elementrecueilli($this->_request->getParams());

		$elementrecueilli->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'elementrecueilli as been deleted');

		$this->forward('Dashboard_Elementrecueilli', 'index');

	}
*/

}