<?php
class Controller_Fai extends Yab_Controller_Action {
	public function filesUploadForm($name, $destination, $tabExtenssions = null) {
		if (isset ( $_POST ["btSubmit"] )) {
			foreach ( $_FILES [$name] ['error'] as $cle => $valeur ) {
				if (! is_uploaded_file ( $_FILES [$name] ['tmp_name'] [$cle] )) { // Vérifie que le fichier a été téléchargé par HTTP POST
					$error = "Downloading files does not succeed.";
				} else if ($valeur > 0) {
					$error = "Erreur lors du transfert du fichier : " . $_FILES ['photos'] ['tmp_name'] [$cle];
				}
			}
			if (! isset ( $error )) {
				foreach ( $_FILES [$name] ['error'] as $cle => $valeur ) {
					$extension_upload = strtolower ( substr ( strrchr ( $_FILES [$name] ['name'] [$cle], '.' ), 1 ) );
					if (($tabExtenssions != null and in_array ( $extension_upload, $tabExtenssions )) or $tabExtenssions == null) {
						$nom = $destination . "/" . $_FILES [$name] ['name'] [$cle] /*. $cle . ".{$extension_upload}"*/;
						$resultat = move_uploaded_file ( $_FILES [$name] ['tmp_name'] [$cle], $nom );
						if ($resultat) {
						}
					}
				}
			}
		}
	}
	public function listdir($dir, $format = null) {
		$list = array ();
		if (! file_exists ( $dir ))
			mkdir ( $dir );
		if ($dossier = opendir ( $dir )) {
			while ( false !== ($fichier = readdir ( $dossier )) ) {
				if ($fichier != '.' && $fichier != '..') {
					if ($format != null) {
						if (stripos ( $fichier, $format ))
							$list [] = $fichier;
					} else {
						$list [] = $fichier;
					}
				}
			}
			closedir ( $dossier );
		}
		return $list;
	}
	
	public function actionPhotoexportform() {
		$extensions_valides = array (
				'jpg',
				'jpeg',
				'gif',
				'png' 
		);
		$this->filesUploadForm ( "photos", "photo", $extensions_valides );
		$this->forward ( 'Fai', 'index' );
	}
	
	public function actionGestionphoto() {
		$fai = new Model_Fai ( $this->_request->getParams () );
		$form = new Form_Fai ( $fai );
		$id = $this->_request->getParams ();
		$this->_view->set ( 'photogrammetries', $this->listdir ( "../photogrammetrie/fai/" . $form->getElement ( 'identification' )->get ( 'value' ), "" ) );
		$this->_view->set ( 'helper_form', $form );
		$this->_view->set ( 'id', $id [0] );
	}
	
	public function actionUnzip($id = null, $file = null) {
		$fai = new Model_Fai ( array($id) );
		$form = new Form_Fai ( $fai );
		$dir = "../photogrammetrie/fai/" . $form->getElement ( 'identification' )->get ( 'value' ).'/'.$file;
		$models = array_merge($this->listdir($dir, '.wrl'), $this->listdir($dir, '.x3d'));
		$loader = Yab_Loader::getInstance();
		$config = $loader->getConfig();
		$this->_view->set ( 'nomZip', $file);
		$this->_view->set ( 'models', $models );
		$this->_view->set ( 'helper_form', $form );
		$this->_view->set ( 'id', $id);
	}
	
	public function actionModel($id = null, $file = null, $filename = null) {
		$loader = Yab_Loader::getInstance();
		$config = $loader->getConfig();
		ob_clean();
		$fai = new Model_Fai ( array($id) );
		$form = new Form_Fai ( $fai );
		$in = $config['localisation']."/photogrammetrie/fai/" . $form->getElement ( 'identification' )->get ( 'value' ).'/'.$file.'/'.$filename;
		$ext = strtolower(pathinfo($in, PATHINFO_EXTENSION));
		if ($ext=='wrl' or $ext=='x3d') {
			$out = sys_get_temp_dir()."/".$form->getElement ( 'identification' )->get ( 'value' ).'-'.$file.'-'.$filename.'-'.time().'.html';
			exec("aopt -i '$in' -N '$out'");
			$content = file_get_contents($out);
			unlink($out);
			echo $content;
		} else
			header("location:/photogrammetrie/fai/" . $form->getElement('identification')->get('value').'/'.$file.'/'.$filename);
			die();
	}
	
	public function actionDownloadmodel($id = null, $file = null) {
		$fai = new Model_Fai ( array($id) );
		$form = new Form_Fai ( $fai );
		$dir = "../photogrammetrie/fai/" . $form->getElement ( 'identification' )->get ( 'value' ).'/'.$file;
		$tmp_file = sys_get_temp_dir()."/".$file.'-'.time().".zip";
		$zip = new ZipArchive();
		if ($zip->open($tmp_file, ZipArchive::CREATE) == TRUE) {
			$fichiers = scandir($dir.'/');
			unset($fichiers[0], $fichiers[1]);
			foreach($fichiers as $f) {
				if (!$zip->addFile($dir.'/'.$f, $f)) {
					die('Impossible d&#039;ajouter &quot;'.$f.'&quot;.<br/>');
				}
			}
			$zip->close();
		} else {
			die('Erreur, impossible de créer l&#039;archive.');
		}
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-type: application/octet-stream");
		header('Content-Disposition: attachment; filename="'.$file.'.zip"');
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ".filesize($tmp_file));
		ob_clean();
		$content = file_get_contents($tmp_file);
		unlink($tmp_file);
		echo $content;
		die();
	}
	
	public function actionExportmodeleform() {
		$fai = new Model_Fai ( $_POST ['id'] );
		$form = new Form_Fai ( $fai );
		$extensions_valides = array ('zip');
		$path = "../photogrammetrie/fai/". $form->getElement ( 'identification' )->get ( 'value' );
		$this->filesUploadForm ( "photos", $path, $extensions_valides );
		$this->unzip($path);
		$this->forward ( 'Fai', 'gestionphoto', array ($_POST ['id']));
	}
	
	public function actionUnlinkmodel($faiid = null, $model = null) {
		$fai = new Model_Fai ( $faiid );
		$form = new Form_Fai ( $fai );
		$this->rrmdir ( "../photogrammetrie/fai/" . $form->getElement ( 'identification' )->get ( 'value' ) . "/" . $model );
		$this->forward ( 'Fai', 'gestionphoto', array ($faiid));
	}
	
	public function unzip($path) {
		$zips = $this->listdir($path, "zip");
		foreach ($zips as $z) {
			$zip = new ZipArchive();
			$zip_file = $path.'/'.$z;
			if ($zip->open($zip_file) === TRUE) {
				$zip->extractTo($path.'/'.basename($z, 'zip').'/');
				$zip->close();
				unlink($zip_file);
			} else {
				unlink($zip_file);
				die( "Échec de l'extraction de $z : fichier zip invalide.");
			}
		}
	}
	
	public function actionIndex() {
		$fai = new Model_Fai ();
		$fais = $fai->getFaiDetail ();
		$this->_view->set ( 'fais_index', $fais );
		
		$loader = Yab_Loader::getInstance ();
		$config = $loader->getConfig ();
		$this->_view->set ( 'config', $config );
	}
	public function actionChangeIdentification() {
		$newInterfait = new Model_Interfait ();
		$form_interfait = new Form_Interfait ( $newInterfait );
		
		$newFaiUS = new Model_Fai_Us ();
		$param_id = $this->_request->getParams ();
		
		if (isset ( $param_id ) && ! empty ( $param_id )) {
			$fai = new Model_Fai ( $this->_request->getParams () );
			$courant_sitefouille_id = $fai->get ( 'sitefouille_id' );
			$fai_id = $fai->get ( 'id' );
			$usesAssociees = $newFaiUS->getFai_us ()->where ( 'fai_id = "' . $param_id [0] . '" ' );
			$faiAssociees = $newInterfait->getFai_fai ()->where ( 'fai_id ="' . $param_id [0] . '" ' );
			
			$this->_view->set ( 'courant_sitefouille_id', $courant_sitefouille_id );
			$this->_view->set ( 'fai_id', $fai_id );
			$this->_view->set ( 'faitsAssociees', $faiAssociees );
			$this->_view->set ( 'usesAssociees', $usesAssociees );
		}
		
		$this->_view->set ( 'helper_model_interfait', $newInterfait );
		$this->_view->set ( 'helper_form_interfait', $form_interfait );
		$this->_view->setFile ( 'View/interfait/changeFaiIdentification.html' );
	}
	public function actionAdd() {
		
		// appel fichier internationalisation
		$registry = Yab_Loader::getInstance ()->getRegistry ();
		$i18n = $registry->get ( 'i18n' );
		$filter_no_html = new Yab_Filter_NoHtml ();
		
		$mode = self::ACTION_MODE_CREATE;
		$errors_messages = '';
		
		// jfb 2016-06 mantis 318 début : contrôle site de fouille sélectionné
		$loader = Yab_Loader::getInstance();
		$session = $loader->getSession();
		$sitefouille_id = $session->get('sitefouille_id');
		if (empty($sitefouille_id)) {
			$this->getSession()->set('flash_status', 'warning');
			$this->getSession()->setFlashError('keysf', 'Veuillez sélectionner un site de fouille !');
			$this->forward('Fai', 'index');
		}
		// jfb 2016-06 mantis 318 fin
		
		$fai = new Model_Fai ();
		$form = new Form_Fai ( $fai );
		
		$newFaiUS = new Model_Fai_Us ();
		$form_fai_us = new Form_Fai_Us ( $newFaiUS );
		
		$newInterfait = new Model_Interfait ();
		$form_interfait = new Form_Interfait ( $newInterfait );
		
		$sitefouille = new Model_Sitefouille ();
		$sitefouilles = $sitefouille->fetchAll ();
		
		$formvalue = $form->getValues ();
		
		$session = Yab_Loader::getInstance ()->getSession ();
		$courant_sitefouille_id = $session->get ( 'sitefouille_id' );
		
		$types = $fai->getTable ( 'Model_Fsn_Categorie' )->fetchAll ()->where ( 'id= "' . $formvalue ['typefai_id'] . '" ' );
		$nb_row = $types->count ();
		
		// Affichage categorie_value by typefai_id
		if ($nb_row > 0) {
			foreach ( $types as $type ) {
				$categ_value = $type ['categorie_value'];
			}
		} else
			$categ_value = '';
		
		$formFX = "empty";
		
		if ($form->isSubmitted ()) {
			
			$categ_value = $formvalue ['typefai_id'];
			
			// Recuperation typefai_id by categorie_value
			if ($_POST ['typefai_id'] !== null && $_POST ['typefai_id'] != '') {
				$categories = $fai->getTable ( 'Model_Fsn_Categorie' )->fetchAll ()->setKey ( 'id' )->where ( 'categorie_value="' . $categ_value . '" and categorie_type like "fai%" ' );
				$nb_row = $categories->count ();
				
				if ($nb_row > 0) {
					foreach ( $categories as $typeid => $categorie ) {
						$formvalue ['typefai_id'] = $typeid;
						$form->getElement ( 'typefai_id' )->set ( 'value', $typeid );
					}
					$formFX = "set";
				} else {
					$formFX = "empty";
					$errors_messages ['typefai_idrelations-fai_us'] = $filter_no_html->filter ( $i18n->say ( 'fai_typeInList' ) );
				}
			}
			
			// Recuperation date plancher et plafond du site courant
			$sites = $fai->getSitefouilleByIdSite ( $courant_sitefouille_id );
			foreach ( $sites as $site ) {
				$date_plancher = $site ['datation_debut'];
				$date_plafond = $site ['datation_fin'];
			}
			
			// Ajout du GUUID
			$generateGuuid = new Plugin_Guuid ();
			$guuid = $generateGuuid->GetUUID ();
			
			try {
				
				$formvalue ['id'] = $guuid;
				
				// Control champs requis
				function isNumber($idElt, &$formvalue, &$form, &$errors_messages, $message) {
					$eltvalue = addslashes ( $formvalue [$idElt] );
					if (! empty ( $eltvalue )) {
						if ($form->getElement ( $idElt )->getErrors ())
							$errors_messages [$idElt] = $message;
					}
				}
				function isUnique($idElt, &$formvalue, &$fai, &$errors_messages, $message) {
					$eltvalue = addslashes ( $formvalue [$idElt] );
					if (! empty ( $eltvalue )) {
						$req = $fai->fetchAll ()->where ( addslashes ( $idElt ) . ' ="' . $eltvalue . '" and sitefouille_id ="' . $formvalue ['sitefouille_id'] . '"' );
						$nb_row = $req->count ();
						if ($nb_row > 0) {
							$errors_messages [$idElt] = $message;
						}
					}
				}
				function limiteDatation($idElt, &$formvalue, &$date_plancher, &$date_plafond, &$errors_messages, $message) {
					$eltvalue = addslashes ( $formvalue [$idElt] );
					if (is_numeric ( $eltvalue ) && $eltvalue < $date_plancher || $eltvalue > $date_plafond) {
						$errors_messages [$idElt] = $message;
					}
				}
				
				if ($form->getElement ( 'identification' )->getErrors ()) {
					$errors_messages ['identification'] = $filter_no_html->filter ( $i18n->say ( 'verror_requisAll' ) );
				}
				isNumber ( 'fin_fourc_hte_estim', $formvalue, $form, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_requisFin_fh_aaaa' ) ) );
				isNumber ( 'fin_fourc_bas_estim', $formvalue, $form, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_requisFin_fb_aaaa' ) ) );
				isNumber ( 'deb_fourc_hte_estim', $formvalue, $form, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_requisDebut_fh_aaaa' ) ) );
				isNumber ( 'deb_fourc_bas_estim', $formvalue, $form, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_requisDebut_fb_aaaa' ) ) );
				
				isNumber ( 'fin_fourc_hte_certain', $formvalue, $form, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_requisFin_fh_aaaa' ) ) );
				isNumber ( 'fin_fourc_bas_certain', $formvalue, $form, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_requisFin_fb_aaaa' ) ) );
				isNumber ( 'deb_fourc_hte_certain', $formvalue, $form, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_requisDebut_fh_aaaa' ) ) );
				isNumber ( 'deb_fourc_bas_certain', $formvalue, $form, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_requisDebut_fb_aaaa' ) ) );
				
				isUnique ( 'identification', $formvalue, $fai, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_uniciteIdentification' ) ) );
				
				limiteDatation ( 'fin_fourc_hte_estim', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_limiteDatation_finh' ) ) );
				limiteDatation ( 'fin_fourc_bas_estim', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_limiteDatation_finb' ) ) );
				limiteDatation ( 'deb_fourc_hte_estim', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_limiteDatation_debuth' ) ) );
				limiteDatation ( 'deb_fourc_bas_estim', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_limiteDatation_debutb' ) ) );
				
				limiteDatation ( 'fin_fourc_hte_certain', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_limiteDatation_finh' ) ) );
				limiteDatation ( 'fin_fourc_bas_certain', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_limiteDatation_finb' ) ) );
				limiteDatation ( 'deb_fourc_hte_certain', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_limiteDatation_debuth' ) ) );
				limiteDatation ( 'deb_fourc_bas_certain', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_limiteDatation_debutb' ) ) );
				
				// Caractères spéciaux interdits: Trois param($donnees, $field, $fieldMessage, $errors_messages)
				Plugin_Fonctions::nospecialcharacter ( $formvalue ['identification'], 'identification', 'fai_nopecialcaractere', $errors_messages );
				
				if (! empty ( $errors_messages ))
					throw new Exception ();
					
					$fai->populate ( $formvalue )->save ();
					$this->insertFaiExtension($formvalue["id"]);

					
				// Debut création arborescence multimédia
				foreach ( $sites as $site ) {
					$code_sitefouille = $site ['nomabrege'];
				}
				
				// Recuperation nomabrege de l'organisme
				$user_session = $this->getSession ()->get ( 'session' );
				$entiteadmin_id = $user_session ['entiteadmin_id'];
				
				$organismes = $fai->getEntiteOrganisme ()->where ( 'enti.id="' . $entiteadmin_id . '" ' );
				$nb_row = $organismes->count ();
				
				if ($nb_row > 0) {
					foreach ( $organismes as $organisme ) {
						$arborescence = '../mediatheque/albums/' . $organisme ['nomabrege_organisme'] . '/fai/' . $code_sitefouille . '/' . $formvalue ['identification'];
					}
					
					if (! is_dir ( $arborescence )) {
						mkdir ( $arborescence, 0777, true );
					}
				}
				// Fin création arborescence multimédia
				
				// ajout des relations en bdd
				$historisation = new Plugin_Historique ();
				$generateGuuid = new Plugin_Guuid ();
				$title_relations = [ 
						"fai_us" => "_pfai_us_",
						"interfait" => "_pfai_fai_" 
				];
				$regex_relations = array ();
				foreach ( $title_relations as $table => $title ) {
					$regex_relations [$table] = "#^([a-z0-9_]+)(" . $title . ")([0-9]+)$#";
				}
				
				$relations = array ();
				foreach ( $_POST as $key => $value ) {
					foreach ( $regex_relations as $table => $regex ) {
						if (preg_match ( $regex, $key, $res )) {
							$relations [$table] [$res [3]] [$res [1]] = $value;
						}
					}
				}
				
				foreach ( $relations as $table => $relationsTable ) {
					
					foreach ( $relationsTable as $relation ) {
						
						if ($table == "fai_us") {
							$nomModel = "Model_Fai_Us";
							$nomForm = "Form_Fai_Us";
							$controller_courant = 'Fai_Us';
						} else {
							$nomModel = "Model_Interfait";
							$nomForm = "Form_Interfait";
							$controller_courant = 'Interfait';
						}
						
						$model = new $nomModel ();
						$form_courant = new $nomForm ( $model );
						
						$relation ['fai_id'] = $fai->get ( 'id' );
						$relation ['id'] = $generateGuuid->GetUUID ();
						
						$model->populate ( $relation )->save ();
						
						// Historisation des relations
						unset ( $relation ['id'] );
						foreach ( $relation as $key => $value )
							$form_courant->getElement ( $key )->set ( 'value', $value );
						$relation = $form_courant->getTypedValues ( $relation );
						$relation ['id'] = $model->get ( 'id' );
						$historisation->addhistory ( $controller_courant, self::MODE_CREATE, $relation );
					}
				}
				
				// Historisation de la modif
				$id_fai = $fai->get ( 'id' );
				$formvalue ['id'] = $id_fai;
				
				$formvalue = $form->getTypedValues ( $formvalue );
				$historisation->addhistory ( 'Fai', self::MODE_CREATE, $formvalue );
				
				$this->getSession ()->set ( 'flash_title', '' );
				$this->getSession ()->set ( 'flash_status', 'info' );
				$this->getSession ()->set ( 'flash_message', 'fai as been added' );
				
				$this->forward ( 'Fai', 'index' );
			} catch ( Exception $e ) {
				$this->getSession ()->set ( 'flash_title', '' );
				$this->getSession ()->set ( 'flash_status', 'warning' );
				$this->getSession ()->set ( 'flash_message', '' );
				$this->getSession ()->setFlashErrors ( $errors_messages );
				if ($formFX == "set") {
					$formFX = $this->setFXForm($form->getElement("typefai_id")->get("value"));
				}
			}
		}
		
		// $this->_view->set('helper_form', new Yab_Helper_Form($form));
		$this->_view->set ( 'helper_model_fai', $fai );
		$this->_view->set ( 'helper_form', $form );
		$this->_view->set ( 'helper_form_fai_us', $form_fai_us );
		$this->_view->set ( 'helper_model_fai_us', $newFaiUS );
		$this->_view->set ( 'helper_form_interfait', $form_interfait );
		$this->_view->set ( 'helper_model_interfait', $newInterfait );
		$this->_view->set ( 'sitefouilles', $sitefouilles );
		$this->_view->set ( 'courant_sitefouille_id', $courant_sitefouille_id );
		$this->_view->set ( 'categ_value', $categ_value );
		$this->_view->set('formFX', $formFX);
	}
	
	private function updateFaiExtension($id=null) {
		$fxv = new Model_Fai_Extension_Value();
		$fxv->deleteExtension($id);
		$this->insertFaiExtension($id);
		
	}
	
	private function insertFaiExtension($id=null) {
		//print_r($_POST);
		$tab = array();
		foreach ($_POST as $cle => $value) {
			if (preg_match("#^fx_date_#", $cle)) {
				$tab[substr($cle, 8)] = $value;
			} else if (preg_match("#^___fx_#", $cle)) {
				$tab[substr($cle, 6)] = $value;
				//echo $id." " . $value . " ".. "<br>";
			} else if (preg_match("#^fx_#", $cle)) {
				$tab[substr($cle, 3)] = ($value != "on") ? $value : 1 ; 
				//echo $id." " . $value . " ".substr($cle, 3). "<br>";
			}
		}
		foreach ($tab as $cle => $valeur) {
			$insertion = array();
			$insertion["fai_id"] = $id;
			$insertion["extension_id"] = $cle;
			$insertion["extension_value"] = $valeur;
			$fxv = new Model_Fai_Extension_Value();
			
			$fxv->populate($insertion)->save();
			
			
		}
		
	}
	
	public function actionShow() {
		$mode = self::ACTION_MODE_SHOW;
		
		$fai = new Model_Fai ( $this->_request->getParams () );
		$form = new Form_Fai ( $fai );
		$formvalue = $form->getValues ();
		
		$id_fai = $fai->get ( 'id' );
		$fai_us = new Model_Fai_Us ();
		$interfait = new Model_Interfait ();
		
		$associatfai_us = $fai_us->getFai_us ()->where ( 'fai_id = "' . $id_fai . '" ' )->toArray ();
		$associatfai_fai = $interfait->getFai_fai ()->where ( 'fai_id ="' . $id_fai . '" || fai_id_1 ="' . $id_fai . '"' )->toArray ();
		function getFieldValueById(&$fai, $table, $tablecolum, $eltId, $valsearch, &$retour) {
			$req = $fai->getTable ( $table )->fetchAll ()->where ( addslashes ( $tablecolum ) . '= "' . $fai->get ( $eltId ) . '" ' );
			$nb_row = $req->count ();
			if ($nb_row > 0) {
				foreach ( $req as $row ) {
					$retour = $row [$valsearch];
				}
			} else
				$retour = '';
		}
		
		getFieldValueById ( $fai, 'Model_Sitefouille', 'id', 'sitefouille_id', 'nom', $site_name );
		getFieldValueById ( $fai, 'Model_Fsn_Categorie', 'id', 'typefai_id', 'categorie_value', $categ_value );
		getFieldValueById ( $fai, 'Model_Sitefouille', 'id', 'sitefouille_id', 'nomabrege', $codesite );
		
		// Recuperation nomabrege de l'organisme
		$user_session = $this->getSession ()->get ( 'session' );
		$entiteadmin_id = $user_session ['entiteadmin_id'];
		
		$organismes = $fai->getEntiteOrganisme ()->where ( 'enti.id="' . $entiteadmin_id . '" ' );
		$nb_row = $organismes->count ();
		
		if ($nb_row > 0) {
			foreach ( $organismes as $organisme ) {
				$codeorganisme = $organisme ['nomabrege_organisme'];
			}
		} else
			$codeorganisme = "UASD";
			
			// $this->_view->set('helper_form', new Yab_Helper_Form($form));
		$this->_view->set ( 'helper_model_fai', $fai );
		$this->_view->set ( 'helper_form', $form );
		$this->_view->set ( 'site_name', $site_name );
		$this->_view->set ( 'categ_value', $categ_value );
		$this->_view->set ( 'fai_id', $id_fai );
		$this->_view->set ( 'codesite', $codesite );
		$this->_view->set ( 'codeidentification', $formvalue ['identification'] );
		$this->_view->set ( 'codeorganisme', $codeorganisme );
		$this->_view->set ( 'associatfai_us', $associatfai_us );
		$this->_view->set ( 'associatfai_fai', $associatfai_fai );
		$this->_view->set ( 'photogrammetries', $this->listdir ( "../photogrammetrie/fai/" . $form->getElement ( 'identification' )->get ( 'value' ), ".zip" ) );
	}
	public function actionEdit() {
		
		// appel fichier internationalisation
		$registry = Yab_Loader::getInstance ()->getRegistry ();
		$i18n = $registry->get ( 'i18n' );
		$filter_no_html = new Yab_Filter_NoHtml ();
		
		$mode = self::ACTION_MODE_UPDATE;
		$errors_messages = '';
		
		$fai = new Model_Fai ( $this->_request->getParams () );
		$form = new Form_Fai ( $fai );
		$id = $fai->get ( 'id' );
		
		$newFaiUS = new Model_Fai_Us ();
		$form_fai_us = new Form_Fai_Us ( $newFaiUS );
		$form_fai_us->getElement ( 'fai_id' )->set ( 'value', $id );
		
		$newInterfait = new Model_Interfait ();
		$form_interfait = new Form_Interfait ( $newInterfait );
		$form_interfait->getElement ( 'fai_id' )->set ( 'value', $id );
		
		$sitefouille = new Model_Sitefouille ();
		$sitefouilles = $sitefouille->fetchAll ();
		
		$formvalue = $form->getValues ();
		
		$courant_sitefouille_id = $fai->get ( 'sitefouille_id' );
		
		$types = $fai->getTable ( 'Model_Fsn_Categorie' )->fetchAll ()->where ( 'id= "' . $formvalue ['typefai_id'] . '" ' );
		$nb_row = $types->count ();
		
		$usesAssociees = $newFaiUS->getFai_us ()->where ( 'fai_id = "' . $id . '" ' );
		$faitsAssociees = $newInterfait->getFai_fai ()->where ( 'fai_id ="' . $id . '" ' );
		
		// Affichage categorie_value by typefai_id
		if ($nb_row > 0) {
			foreach ( $types as $type ) {
				$categ_value = $type ['categorie_value'];
				$categ_id = $type["id"];
			}
		} else {
			$categ_id = null;
			$categ_value = '';
		}
		
		$formFX = (isset($categ_id)) ? $this->setFXFormFromBDD($categ_id, $id) : "empty"; 
		
		if ($form->isSubmitted ()) {
			
			$categ_value = $formvalue ['typefai_id'];
			
			// Recuperation typefai_id by categorie_value
			if ($_POST ['typefai_id'] !== null && $_POST ['typefai_id'] != '') {
				$categories = $fai->getTable ( 'Model_Fsn_Categorie' )->fetchAll ()->setKey ( 'id' )->where ( 'categorie_value="' . $categ_value . '" and categorie_type like "fai%" ' );
				$nb_row = $categories->count ();
				
				if ($nb_row > 0) {
					foreach ( $categories as $typeid => $categorie ) {
						$formvalue ['typefai_id'] = $typeid;
						$form->getElement ( 'typefai_id' )->set ( 'value', $typeid );
						$formFX = 'set';
					}
				} else {
					$errors_messages ['typefai_idrelations-fai_us'] = $filter_no_html->filter ( $i18n->say ( 'fai_typeInList' ) );
				}
			}
			
			// Recupération date plancher et plafond du site courant
			$sites = $fai->getSitefouilleByIdSite ( $courant_sitefouille_id );
			foreach ( $sites as $site ) {
				$date_plancher = $site ['datation_debut'];
				$date_plafond = $site ['datation_fin'];
			}
			
			try {
				
				// Control champs requis
				function isNumber($idElt, &$formvalue, &$form, &$errors_messages, $message) {
					$eltvalue = addslashes ( $formvalue [$idElt] );
					if (! empty ( $eltvalue )) {
						if ($form->getElement ( $idElt )->getErrors ())
							$errors_messages [$idElt] = $message;
					}
				}
				function isUnique($idElt, &$formvalue, &$fai, &$id, &$errors_messages, $message) {
					$eltvalue = addslashes ( $formvalue [$idElt] );
					if (! empty ( $eltvalue )) {
						$req = $fai->fetchAll ()->where ( addslashes ( $idElt ) . ' ="' . $eltvalue . '" and id !="' . $id . '" and sitefouille_id ="' . $formvalue ['sitefouille_id'] . '" ' );
						$nb_row = $req->count ();
						if ($nb_row > 0) {
							$errors_messages [$idElt] = $message;
						}
					}
				}
				function limiteDatation($idElt, &$formvalue, &$date_plancher, &$date_plafond, &$errors_messages, $message) {
					$eltvalue = addslashes ( $formvalue [$idElt] );
					if (is_numeric ( $eltvalue ) && $eltvalue < $date_plancher || $eltvalue > $date_plafond) {
						$errors_messages [$idElt] = $message;
					}
				}
				
				if ($form->getElement ( 'identification' )->getErrors ()) {
					$errors_messages ['identification'] = $filter_no_html->filter ( $i18n->say ( 'verror_requisAll' ) );
				}
				isNumber ( 'fin_fourc_hte_estim', $formvalue, $form, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_requisFin_fh_aaaa' ) ) );
				isNumber ( 'fin_fourc_bas_estim', $formvalue, $form, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_requisFin_fb_aaaa' ) ) );
				isNumber ( 'deb_fourc_hte_estim', $formvalue, $form, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_requisDebut_fh_aaaa' ) ) );
				isNumber ( 'deb_fourc_bas_estim', $formvalue, $form, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_requisDebut_fb_aaaa' ) ) );
				
				isNumber ( 'fin_fourc_hte_certain', $formvalue, $form, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_requisFin_fh_aaaa' ) ) );
				isNumber ( 'fin_fourc_bas_certain', $formvalue, $form, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_requisFin_fb_aaaa' ) ) );
				isNumber ( 'deb_fourc_hte_certain', $formvalue, $form, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_requisDebut_fh_aaaa' ) ) );
				isNumber ( 'deb_fourc_bas_certain', $formvalue, $form, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_requisDebut_fb_aaaa' ) ) );
				
				isUnique ( 'identification', $formvalue, $fai, $id, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_uniciteIdentification' ) ) );
				
				limiteDatation ( 'fin_fourc_hte_estim', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_limiteDatation_finh' ) ) );
				limiteDatation ( 'fin_fourc_bas_estim', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_limiteDatation_finb' ) ) );
				limiteDatation ( 'deb_fourc_hte_estim', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_limiteDatation_debuth' ) ) );
				limiteDatation ( 'deb_fourc_bas_estim', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_limiteDatation_debutb' ) ) );
				
				limiteDatation ( 'fin_fourc_hte_certain', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_limiteDatation_finh' ) ) );
				limiteDatation ( 'fin_fourc_bas_certain', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_limiteDatation_finb' ) ) );
				limiteDatation ( 'deb_fourc_hte_certain', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_limiteDatation_debuth' ) ) );
				limiteDatation ( 'deb_fourc_bas_certain', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'fai_limiteDatation_debutb' ) ) );
				
				// Caractères spéciaux interdits: Trois param($donnees, $field, $fieldMessage, $errors_messages)
				Plugin_Fonctions::nospecialcharacter ( $formvalue ['identification'], 'identification', 'fai_nopecialcaractere', $errors_messages );
				
				if (! empty ( $errors_messages ))
					throw new Exception ();
				
				$fai->populate ( $formvalue )->save ();
				$this->updateFaiExtension($id);
				
				// Debut création arborescence multimédia
				foreach ( $sites as $site ) {
					$code_sitefouille = $site ['nomabrege'];
				}
				
				// Recuperation nomabrege de l'organisme
				$user_session = $this->getSession ()->get ( 'session' );
				$entiteadmin_id = $user_session ['entiteadmin_id'];
				
				$organismes = $fai->getEntiteOrganisme ()->where ( 'enti.id="' . $entiteadmin_id . '" ' );
				$nb_row = $organismes->count ();
				
				if ($nb_row > 0) {
					foreach ( $organismes as $organisme ) {
						$arborescence = '../mediatheque/albums/' . $organisme ['nomabrege_organisme'] . '/fai/' . $code_sitefouille . '/' . $formvalue ['identification'];
					}
					
					if (! is_dir ( $arborescence )) {
						mkdir ( $arborescence, 0777, true );
					}
				}
				// Fin création arborescence multimédia
				
				// ajout des relations en bdd
				$historisation = new Plugin_Historique ();
				$this->deleteRelationsFai ( $fai->get ( 'id' ) );
				$generateGuuid = new Plugin_Guuid ();
				
				$title_relations = [ 
						"fai_us" => "_pfai_us_",
						"interfait" => "_pfai_fai_" 
				];
				$regex_relations = array ();
				foreach ( $title_relations as $table => $title ) {
					$regex_relations [$table] = "#^([a-z0-9_]+)(" . $title . ")([0-9]+)$#";
				}
				
				$relations = array ();
				foreach ( $_POST as $key => $value ) {
					foreach ( $regex_relations as $table => $regex ) {
						if (preg_match ( $regex, $key, $res )) {
							$relations [$table] [$res [3]] [$res [1]] = $value;
						}
					}
				}
				foreach ( $relations as $table => $relationsTable ) {
					
					foreach ( $relationsTable as $relation ) {
						
						if ($table == "fai_us") {
							$nomModel = "Model_Fai_Us";
							$nomForm = "Form_Fai_Us";
							$controller_courant = 'Fai_Us';
							$relation ['fai_id'] = $fai->get ( 'id' );
						} else {
							$nomModel = "Model_Interfait";
							$nomForm = "Form_Interfait";
							$controller_courant = 'Interfait';
							
							// Si nouvelle relation interfait
							if ($relation ['fai_id_1'] != $fai->get ( 'id' )) {
								$relation ['fai_id'] = $fai->get ( 'id' );
							} else {
								$interfaits = $newInterfait->fetchAll ()->where ( 'fai_id_1 ="' . $fai->get ( 'id' ) . '" ' );
								$nb_row = $interfaits->count ();
								if ($nb_row > 0) {
									foreach ( $interfaits as $interfait )
										$relation ['fai_id'] = $interfait ['fai_id'];
								}
								$relation ['fai_id_1'] = $fai->get ( 'id' );
							}
						}
						
						$model = new $nomModel ();
						$form_courant = new $nomForm ( $model );
						
						$relation ['id'] = $generateGuuid->GetUUID ();
						$model->populate ( $relation )->save ();
						
						// Historisation des relations
						unset ( $relation ['id'] );
						foreach ( $relation as $key => $value )
							$form_courant->getElement ( $key )->set ( 'value', $value );
						$relation = $form_courant->getTypedValues ( $relation );
						$relation ['id'] = $model->get ( 'id' );
						$historisation->addhistory ( $controller_courant, self::MODE_CREATE, $relation );
					}
				}
				
				// Historisation de la modif
				$id_fai = $fai->get ( 'id' );
				$formvalue ['id'] = $id_fai;
				
				$formvalue = $form->getTypedValues ( $formvalue );
				$historisation->addhistory ( 'Fai', self::MODE_UPDATE, $formvalue );
				
				$this->getSession ()->set ( 'flash_title', '' );
				$this->getSession ()->set ( 'flash_status', 'info' );
				$this->getSession ()->set ( 'flash_message', 'fai as been edited' );
				
				$this->forward ( 'Fai', 'index' );
			} catch ( Exception $e ) {
				$this->getSession ()->set ( 'flash_title', '' );
				$this->getSession ()->set ( 'flash_status', 'warning' );
				$this->getSession ()->set ( 'flash_message', '' );
				$this->getSession ()->setFlashErrors ( $errors_messages );
				if ($formFX == "set") {
					$formFX = $this->setFXForm($form->getElement("typefai_id")->get("value"));
				}
			}
		}
		
		// $this->_view->set('helper_form_edit', new Yab_Helper_Form($form));
		$this->_view->set ( 'helper_model_fai', $fai );
		$this->_view->set ( 'helper_form', $form );
		$this->_view->set ( 'helper_form_fai_us', $form_fai_us );
		$this->_view->set ( 'helper_model_fai_us', $newFaiUS );
		$this->_view->set ( 'helper_form_interfait', $form_interfait );
		$this->_view->set ( 'helper_model_interfait', $newInterfait );
		$this->_view->set ( 'sitefouilles', $sitefouilles );
		$this->_view->set ( 'courant_sitefouille_id', $courant_sitefouille_id );
		$this->_view->set ( 'categ_value', $categ_value );
		
		$fai_id = $fai->get ( 'id' );
		$this->_view->set ( 'fai_id', $fai_id );
		
		$this->_view->set ( 'faitsAssociees', $faitsAssociees );
		$this->_view->set ( 'usesAssociees', $usesAssociees );
		$this->_view->set('formFX', $formFX);
	}
	private function deleteRelationsFai($fai_id) {
		$histo = array ();
		$historisation = new Plugin_Historique ();
		
		$m_fai_us = new Model_Fai_Us ();
		$histo ['Fai_Us'] = $m_fai_us->getFai_us ()->where ( 'fai_id = "' . $fai_id . '" ' )->toArray ();
		
		$m_interfait = new Model_Interfait ();
		$histo ['Interfait'] = $m_interfait->getFai_fai ()->where ( 'fai_id = "' . $fai_id . '" || fai_id_1 ="' . $fai_id . '" ' )->toArray ();
		
		foreach ( $histo as $name => $relations ) {
			foreach ( $relations as $relation ) {
				if ($name == "Interfait") {
					$f_interfait = new Form_Interfait ( $m_interfait );
					$f_value = $f_interfait->getValues ();
					
					// On supprime certains elements des formulaires pour appliquer (getTypedValues)
					unset ( $relation ['fai_1_identification'] );
					unset ( $relation ['fai_identification'] );
					
					unset ( $f_value ['typeinterfait_id'] );
					unset ( $f_value ['incertitude'] );
					unset ( $f_value ['precisions'] );
					
					foreach ( $relation as $key => $value ) {
						$f_interfait->getElement ( $key )->set ( 'value', $value );
					}
					$relation = $f_interfait->getTypedValues ( $relation );
				}
				$historisation->addhistory ( $name, self::MODE_DELETE, $relation->getAttributes () );
			}
		}
		
		$m_fai_us->deleteRelations ( $fai_id );
		$m_interfait->deleteRelations ( $fai_id );
		$m_interfait->deleteLastRelations ( $fai_id );
	}
	public function actionDelete() {
		$fai = new Model_Fai ( $this->_request->getParams () );
		$form = new Form_Fai ( $fai );
		$formvalue = $form->getValues ();
		
		$formvalue ['id'] = $fai->get ( 'id' );
		
		// jfb 2016-04-18 correctif fiche mantis 99 début
		$message = ''; // init. à blanc message de deletion
		
		try {
			$fai->delete (); // try delete avant historisation
			                 
			// historisation début
			$historisation = new Plugin_Historique ();
			$formvalue = $form->getTypedValues ( $formvalue );
			$historisation->addhistory ( 'Fai', self::MODE_DELETE, $formvalue );
			// historisation fin
			
			$message = 'fai supprimé';
		} catch ( Exception $e ) {
			$message = 'Suppression fai impossible';
		}
		
		// $this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'fai as been deleted');
		$this->getSession ()->set ( 'flash_title', '' );
		$this->getSession ()->set ( 'flash_status', 'info' );
		$this->getSession ()->set ( 'flash_message', $message );
		// jfb 2016-04-18 correctif fiche mantis 99 fin
		
		$this->forward ( 'Fai', 'index' );
	}
	
	private function faiExtensionSelect(&$tableau, $compt, $itemSelected=null) {
		$div = "";
		$select = "";
		if (isset($tableau[$compt])) {
			$nom = $tableau[$compt]["nom"];
			
			$div .= "<div class='form-group field text' id ='FX".$tableau[$compt]["nom"]."'>";
			$div .= "<label class='col-lg-3 col-md-3 col-sm-6 col-xs-6 control-label' for='".$tableau[$compt]["id"].
			"'>" . $tableau[$compt]["nom"] . "</label>";
			$div .="<div class='col-lg-9 col-md-9 col-sm-6 col-xs-6'>";
			
			$select .= "<select id='". $tableau[$compt]["id"] . "' name='fx_". $tableau[$compt]["id"] ."' class='col-md-8 form-control text'>";
			$selected = ($itemSelected == $tableau[$compt]["valeur_id"]) ? "selected" : null;
			$select.="<option value='null'>Non déterminé</option>";
			$select.="<option value='" . $tableau[$compt]["valeur_id"] ."'".$selected.">".$tableau[$compt]["valeur"]."</option>";
			$selected = null;
			for ($i=$compt+1; $i<count($tableau); $i++) {
				if ($nom == $tableau[$i]["nom"]) {
					$selected = ($itemSelected == $tableau[$i]["valeur_id"]) ? "selected" : null;
					$select .= "<option value='". $tableau[$i]["valeur_id"]."'".$selected.">".$tableau[$i]["valeur"]."</option>";
					$selected = null;
					$tableau[$i]=null;
				}
			}
			$select.="</select>";
			$div .= $select;
			$div .="</div></div>";
		}
		
		return $div;
	}
	
	private function faiExtensionInput($table, $type, $text=null) {
		$div = "";
		$div .= "<div class='form-group field text' id ='FX".$table["id"]."'>";
		$div .= "<label class='col-lg-3 col-md-3 col-sm-6 col-xs-6 control-label' for='".$table["id"].
				"'>" . $table["nom"] . "</label>";
		$div .="<div class='col-lg-9 col-md-9 col-sm-6 col-xs-6'>";
		$div .="<input ".$type."id='" . $table["id"] ."' name='fx_" . $table["id"] ."' value='".$text."' class='form-control checkbox' />"; 
		$div .= "</div>";
		$div .= "</div>";
		return $div;
	}
	
	private function faiExtensionCheckbox($table, $text=null) {
		$div = "";
		$div = "<div>";
		$div .= "<input type='hidden' name='___fx_" . $table["id"] ."' value='0' />";
		$div .= "</div>";
		
		$div .= "<div class='form-group field text' id ='FX".$table["id"]."'>";
		$div .= "<label class='col-lg-3 col-md-3 col-sm-6 col-xs-6 control-label' for='".$table["id"].
		"'>" . $table["nom"] . "</label>";
		$div .="<div class='col-lg-9 col-md-9 col-sm-6 col-xs-6'>";
		$checked = ($text==1) ? "checked='checked'" : "";
		
		$div .="<input  ". $checked." type='checkbox' id='" . $table["id"] ."' name='fx_" . $table["id"] ."' class='col-md-8 form-control text' />";
		$div .= "</div>";
		$div .= "</div>";
		return $div;
	}
	
	private function faiExtensionDate($table, $date=null) {
		$date = (isset($date)) ? $date : date('d/m/Y');
		$div = "<div class='form-group field text' id='FX".$table["id"]."'>";
		$div .= "<label class='col-md-3 control-label' for='".$table["id"].
		"'>" . $table["nom"] . "</label>";
		$div .= "<div class='col-lg-9 col-md-9 col-sm-6 col-xs-6'>";
		$div .= "<input type='text' id='".$table["id"]."' class='datepicker text' readonly='1' name='fx_date_".$table["id"]."' value='".$date."' />";
		$div .= "</div></div>";
		return $div;
		
	}
	
	public function actionAjaxGetFaiExtension($id = null) {
		$this->getLayout ()->setEnabled ( false );
		$this->_view->setEnabled ( false );
		
		$faiExtension = null;
		if (isset ( $_POST ['id'] ))
			$id = $_POST ['id'];
		else if (isset ( $_GET ['id'] ))
			$id = $_GET ['id'];
		if (isset($id)) {
			$fsnCategorie = new Model_Fsn_Categorie();
			$faiExtension = $fsnCategorie->getFaiExtension($id);
		}
		//59089e61-2fe7-f2da-64a1-50173dcea268
		$compt = 0;
		$form = null;
		foreach ($faiExtension as $value) {
			if ($value["valeur_key"] == "59089e61-2fe7-f2da-64a1-50173dcea268" ) {
				$form .= $this->faiExtensionInput($value, "type='text'");
			} else if ($value["valeur_key"] == "3de370a2-999a-3238-b59f-f7628fb18951") {
				$form .= $this->faiExtensionDate($value);
			} else if ($value["valeur_key"] == "05c33c09-8556-9519-73c9-372c5fb496a0") {
				$form .= $this->faiExtensionCheckbox($value);
			} else if ($value["valeur_key"] == "38633cee-86c7-65bf-8949-03d68ad5b94d") {
				$form .= $this->faiExtensionInput($value, "type='number' step='0.01'");
			} else {
				$form .= $this->faiExtensionSelect($faiExtension, $compt);
			}
			$compt++;
		}
		
		if (isset($form))
			echo $form;
		else 
			echo "empty";
		
	}
	
	private function setFXForm($id) {
		$faiExtension = null;
		if (isset($id)) {
			$fsnCategorie = new Model_Fsn_Categorie();
			$faiExtension = $fsnCategorie->getFaiExtension($id);
		}
		$compt = 0;
		$form = null;
		foreach ($faiExtension as $value) {
			if ($value["valeur_key"] == "59089e61-2fe7-f2da-64a1-50173dcea268" ) {
				$text = (isset($_POST["fx_".$value["id"]])) ? $_POST["fx_".$value["id"]] : null;
				$form .= $this->faiExtensionInput($value, "type='text'", $text);
			} else if ($value["valeur_key"] == "3de370a2-999a-3238-b59f-f7628fb18951") {
				$text = (isset($_POST["fx_date_".$value["id"]])) ? $_POST["fx_date_".$value["id"]] : null;
				$form .= $this->faiExtensionDate($value, $text);
			} else if ($value["valeur_key"] == "05c33c09-8556-9519-73c9-372c5fb496a0") {
				$text = (isset($_POST["fx_".$value["id"]])) ? 1 : 0;
				//echo $text; exit;
				$form .= $this->faiExtensionCheckbox($value, $text);
			} else if ($value["valeur_key"] == "38633cee-86c7-65bf-8949-03d68ad5b94d") {
				$text = (isset($_POST["fx_".$value["id"]])) ? $_POST["fx_".$value["id"]] : null;
				$form .= $this->faiExtensionInput($value, "type='number' step='0.01'", $text);
			} else {
				$select = (isset($_POST["fx_".$value["id"]])) ? $_POST["fx_".$value["id"]] : null;
				$form .= $this->faiExtensionSelect($faiExtension, $compt, $select);
			}
			$compt++;
		}
		return $form;
	}
	
	private function setFXFormFromBDD($type, $fai_id) {
		$faiExtension = null;
		if (isset($type)) {
			$fsnCategorie = new Model_Fsn_Categorie();
			$faiExtension = $fsnCategorie->getFaiExtension($type);
		}
		
		$fai = null;
		if (isset($fai_id)) {
			$fxv = new Model_Fai_Extension_Value();
			$fai = $fxv->getFaiExtensionValue($fai_id);
		}
		
		$compt = 0;
		$form = null;
		$tab = array();
		foreach ($fai as $item) {
			$tab[$item["extension_id"]] = $item["extension_value"];
		}
		
		foreach ($faiExtension as $value) {
			$text = (isset($tab[$value["id"]])) ? $tab[$value["id"]] : null;
			if ($value["valeur_key"] == "59089e61-2fe7-f2da-64a1-50173dcea268" ) {
				$form .= $this->faiExtensionInput($value, "type='text'", $text);
			} else if ($value["valeur_key"] == "3de370a2-999a-3238-b59f-f7628fb18951") {
				$form .= $this->faiExtensionDate($value, $text);
			} else if ($value["valeur_key"] == "05c33c09-8556-9519-73c9-372c5fb496a0") {
				$form .= $this->faiExtensionCheckbox($value, $text);
			} else if ($value["valeur_key"] == "38633cee-86c7-65bf-8949-03d68ad5b94d") {
				$form .= $this->faiExtensionInput($value, "type='number' step='0.01'", $text);
			} else {
				$form .= $this->faiExtensionSelect($faiExtension, $compt, $text);
			}
			$compt++;
		}
		
		return $form;
	}
	
}
