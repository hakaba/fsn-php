<?php

class Controller_Synchrotype extends Yab_Controller_Action {

	public function actionIndex() {

		$synchrotype = new Model_Synchrotype();

		$synchrotypes = $synchrotype->fetchAll();

		$this->_view->set('synchrotypes', $synchrotypes);
	}

	public function actionAdd() {

		$synchrotype = new Model_Synchrotype();

		$form = new Form_Synchrotype($synchrotype);

		if($form->isSubmitted() && $form->isValid()) {

			$synchrotype->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'synchrotype as been added');

			$this->forward('Synchrotype', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$synchrotype = new Model_Synchrotype($this->_request->getParams());

		$form = new Form_Synchrotype($synchrotype);

		if($form->isSubmitted() && $form->isValid()) {

			$synchrotype->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'synchrotype as been edited');

			$this->forward('Synchrotype', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$synchrotype = new Model_Synchrotype($this->_request->getParams());

		$synchrotype->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'synchrotype as been deleted');

		$this->forward('Synchrotype', 'index');

	}

}