<?php

class Controller_Er_Traitement extends Yab_Controller_Action {

	public function actionIndex() {

		$er_traitement = new Model_Er_Traitement();

		$er_traitements = $er_traitement->fetchAll();

		$this->_view->set('er_traitements', $er_traitements);
	}

	public function actionAdd() {

		$er_traitement = new Model_Er_Traitement();

		$form = new Form_Er_Traitement($er_traitement);

		if($form->isSubmitted() && $form->isValid()) {

			$er_traitement->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'er_traitement as been added');

			$this->forward('Er_Traitement', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$er_traitement = new Model_Er_Traitement($this->_request->getParams());

		$form = new Form_Er_Traitement($er_traitement);

		if($form->isSubmitted() && $form->isValid()) {

			$er_traitement->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'er_traitement as been edited');

			$this->forward('Er_Traitement', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$er_traitement = new Model_Er_Traitement($this->_request->getParams());

		$er_traitement->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'er_traitement as been deleted');

		$this->forward('Er_Traitement', 'index');

	}

}