<?php

class Controller_Troncon extends Yab_Controller_Action {

	public function actionIndex() {

		$troncon = new Model_Troncon();

		$session = $this->getSession();
		if($session->has('unitesondage_id')) {
			$troncons = $troncon -> getListTroncon($session->get('unitesondage_id'));
		} else {
			$troncons = $troncon->fetchAll();
		}

		$this->_view->set('troncons', $troncons);
	}

	public function actionAdd() {

		$mode = self::ACTION_MODE_CREATE;

		$troncon = new Model_Troncon();
		$form_add = new Form_Troncon($troncon, $mode);

		$form_add->setElement('numero_ident', array(
				'type' => 'text',
				'id' => 'numero_ident',
				'label' => $form_add->getElement('identification')->get('label'),
				'value' => isset($_POST['numero_ident']) ? $_POST['numero_ident'] : $form_add->getElement('identification')->get('numero'),
				'validators' => array('NotEmpty','Int'),
				'errors' => array('NotEmpty' => array('Le champ identification ne doit pas être vide'),
						'Int' => array('Ce champs doit être un entier')),
		));

		if($form_add->isSubmitted() && $form_add->isValid()) {

			$formvalues = $form_add->getValues();
			$troncon->populate($formvalues)->save();
				
			$this->getSession()->set('flash_title', '');
			$this->getSession()->set('flash_status', 'info');
			$this->getSession()->set('flash_message', 'Troncon has been added');
				
			//$this->forward('Log', 'index');

		} else {
			if($form_add->isValid() && !empty($_POST)) {
				$formvalues = $form_add->getValues();
				unset($formvalues['numero_ident']);
				// Ajout du GUUID
				$generateGuuid = new Plugin_Guuid();
				$guuid = $generateGuuid -> GetUUID();
				//$form->setElement('id', array('type'=>'text', 'value' => $guuid ) ) ;
				$formvalues['id'] = $guuid;
				$formvalues['identification'] = $form_add->getElement('identification')->get('value');
				
				$formvalues['date_debut'] = date("Y-m-d", strtotime(str_replace('/', '-', $formvalues['date_debut'])));
				$formvalues['date_fin'] = date("Y-m-d", strtotime(str_replace('/', '-', $formvalues['date_fin'])));

				$formvalues = $form_add -> setNullValues($formvalues);
				$troncon->populate($formvalues)->save();

				$this->getSession()->set('flash_title', '');
				$this->getSession()->set('flash_status', 'info');
				$this->getSession()->set('flash_message', 'troncon has been added');

				//$session = $this->getSession();
			}
		}

		$this->_view->set('form_add', $form_add);

	}

	public function actionShow($id = null) {

		$mode = self::ACTION_MODE_SHOW;

		$session = $this->getSession();

		$params = $this->_request->getParams();
		$troncon_id = $params[0];

		$modeltroncon = new Model_Troncon( $this->_request->getParams () );
		$form = new Form_Troncon($modeltroncon, $mode);
		//$formvalues = $form->getValues();
		$id_troncon = $modeltroncon->get ('id');

		$tranchee = null;
		$i18n = Yab_Loader::getInstance()->getRegistry()->get('i18n');

		$session = $this->getSession();
		$session['troncon_id'] = $troncon_id;

		$this->_view->set('form_show', $form);

	}

	public function actionEdit() {

		$mode = self::ACTION_MODE_UPDATE;

		$params = $this->_request->getParams();
		$troncon_id = $params[0];

		$troncon = new Model_Troncon($params);
		$form = new Form_Troncon($troncon, $mode);
		
		echo "isSubmitted = ".$form->isSubmitted()."  isValid = ".$form->isValid()." empty(POST) = ".empty($_POST);

		if(!empty($_POST) && $form->isValid()) {

			$formvalues = $form->getValues();
			
			$formvalues['date_debut'] = date("Y-m-d", strtotime(str_replace('/', '-', $formvalues['date_debut'])));
			$formvalues['date_fin'] = date("Y-m-d", strtotime(str_replace('/', '-', $formvalues['date_fin'])));
			$formvalues = $form->setNullValues($formvalues);
			$troncon->populate($formvalues)->save();
			
			$this->getSession()->set('flash_title', '');
			$this->getSession()->set('flash_status', 'info');
			$this->getSession()->set('flash_message', 'troncon has been edited');
			if($this->getSession()->has('unitesondage_id')) {
				$this->forward('Unitesondage', 'edit', array($this->getSession()->get('unitesondage_id')));
			} else {
				$this->forward('Unitesondage', 'index');
			}

		}

		$session = $this->getSession();
		$session['troncon_id'] = $troncon_id;

		$this->_view->set('form_edit', $form);

	}

	public function actionDelete() {

		$troncon = new Model_Troncon($this->_request->getParams());
		$form = new Form_Troncon($troncon);
		$formvalues = $form->getValues();

		$formvalues ['id'] = $troncon->get ('id');echo $formvalues['id'];

		$message = '';

		try {
			$troncon->delete();
				
			// historisation début
			$historisation = new Plugin_Historique();
			$formvalues = $form->getTypedValues($formvalues);
			$historisation->addhistory('Troncon', self::MODE_DELETE, $formvalues);
			// historisation fin
			$message = 'Troncon supprimé';
		} catch (Exception $e) {
			$message = 'Troncon n\'a pas pu etre supprimé';
		}

		$this->getSession()->set('flash_title', '');
		$this->getSession()->set('flash_status', 'info');
		$this->getSession()->set('flash_message', $message);

		//$this->forward('Log', 'index');

	}

}