<?php

class Controller_Prod_Precisionprod extends Yab_Controller_Action {

	public function actionIndex() {

		$prod_precisionprod = new Model_Prod_Precisionprod();

		$prod_precisionprods = $prod_precisionprod->fetchAll();

		$this->_view->set('prod_precisionprods', $prod_precisionprods);
	}

	public function actionAdd() {

		$prod_precisionprod = new Model_Prod_Precisionprod();

		$form = new Form_Prod_Precisionprod($prod_precisionprod);

		if($form->isSubmitted() && $form->isValid()) {

			$prod_precisionprod->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'prod_precisionprod as been added');

			$this->forward('Prod_Precisionprod', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$prod_precisionprod = new Model_Prod_Precisionprod($this->_request->getParams());

		$form = new Form_Prod_Precisionprod($prod_precisionprod);

		if($form->isSubmitted() && $form->isValid()) {

			$prod_precisionprod->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'prod_precisionprod as been edited');

			$this->forward('Prod_Precisionprod', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$prod_precisionprod = new Model_Prod_Precisionprod($this->_request->getParams());

		$prod_precisionprod->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'prod_precisionprod as been deleted');

		$this->forward('Prod_Precisionprod', 'index');

	}

}