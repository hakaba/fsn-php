<?php

class Controller_Classe_Fonction extends Yab_Controller_Action {

	public function actionIndex() {

		$classe_fonction = new Model_Classe_Fonction();

		$classe_fonctions = $classe_fonction->fetchAll();

		$this->_view->set('classe_fonctions', $classe_fonctions);
	}

	public function actionAdd() {

		$classe_fonction = new Model_Classe_Fonction();

		$form = new Form_Classe_Fonction($classe_fonction);

		if($form->isSubmitted() && $form->isValid()) {

			$classe_fonction->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'classe_fonction as been added');

			$this->forward('Classe_Fonction', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$classe_fonction = new Model_Classe_Fonction($this->_request->getParams());

		$form = new Form_Classe_Fonction($classe_fonction);

		if($form->isSubmitted() && $form->isValid()) {

			$classe_fonction->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'classe_fonction as been edited');

			$this->forward('Classe_Fonction', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$classe_fonction = new Model_Classe_Fonction($this->_request->getParams());

		$classe_fonction->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'classe_fonction as been deleted');

		$this->forward('Classe_Fonction', 'index');

	}

}