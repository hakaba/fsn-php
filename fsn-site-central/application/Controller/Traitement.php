<?php

class Controller_Traitement extends Yab_Controller_Action {

    public function actionIndex() {
		if(isset($_GET['ER'])) {
			$idERt = str_replace('/','',$_GET['ER']); // on enleve le / dans la variable de l'url
			unset ( $_SESSION ['idER2'] );
			$_SESSION ['idER2'] = $idERt;
			
			// Envoyer le numéro de l'élément recueilli via l'url
			$numeroER = new Model_Elementrecueilli ( $idERt );
		} 
		else {
			// récupération de l'id du ER via la session
			// $numeroER = new Model_Elementrecueilli ( $_SESSION ['idER2'] );
			$numeroER = "";
			$idERt = "";
		}
		
		// Site fouillé
		$session = $this->getSession();
		$sitefouille_id = $session->has('sitefouille_id') ? $session->get('sitefouille_id') :	null ;
		
		// Selection ER
		if (isset($_POST["er_id"])) {
			$er_id = $_POST["er_id"] ;
			$_SESSION['idER2'] = $er_id;
			if(!empty($_POST["er_id"])){
				// Envoyer le numéro de l'élément recueilli via l'url
				$numeroER = new Model_Elementrecueilli ( $er_id );
			}
		} else if (isset($_SESSION['idER2'])){
			$er_id = $_SESSION['idER2'] ;
			if(!empty($er_id)){
				// Envoyer le numéro de l'élément recueilli via l'url
				$numeroER = new Model_Elementrecueilli ( $er_id );
			}
		}
		else
			$er_id = null ;
		
		$elementrecueilli = new Model_Elementrecueilli();
		$ers = $elementrecueilli->getSelectElementrecueilliBySiteFouille($sitefouille_id);
		
		$traitement = new Model_Traitement ();
		
		$this->_view->set ( 'numeroER', $numeroER );
		$this->_view->set ( 'idERt', $idERt );
		
		$this->getSession ()->set ( 'titre', 'GESTION DES TRAITEMENTS' );
		// $this -> getLayout() -> setFile("View/layout_fsn.html");
		$traitements = $traitement->getTraitementDetail ($sitefouille_id);
		$this->_view->set ( 'ers', $ers );
		$this->_view->set ( 'traitements', $traitements );
		$this->_view->set ( 'er_id', $er_id );
	}

    public function actionList() {
        
        $traitement = new Model_Traitement();

        $traitements = $traitement->getListTraitement();
        
        $this->_view->set('traitements', $traitements);
        
    }

	public function actionAdd() {
		
		// appel fichier internationalisation
		$registry = Yab_Loader::getInstance()->getRegistry();
		$i18n = $registry -> get('i18n');
		$filter_no_html = new Yab_Filter_NoHtml();
		
		// jfb 2016-05-04 mantis 260 début
		//$mode = 'add';
		$mode = self::MODE_CREATE;
		// jfb 2016-05-04 mantis 260 fin
		$errors_messages = '';
		
		if (isset ( $_GET['ER'] )) {
			$idERt = str_replace('/','',$_GET['ER']); // on enleve le / dans la variable de l'url
			unset ( $_SESSION ['idER2'] );
			$_SESSION ['idER2'] = $idERt;
		}
		
		$traitement = new Model_Traitement();	
		$form = new Form_Traitement ( $traitement );
		$formvalue = $form->getValues ();
		
		$this->getSession ()->set ( 'titre', 'GESTION DES TRAITEMENTS' );
		// $this -> getLayout() -> setFile("View/layout_fsn.html");
			
		if ($form->isSubmitted () && $form->isValid ()) {
			try{
				
				// Ajout du GUUID
				$generateGuuid = new Plugin_Guuid ();
				$guuid = $generateGuuid->GetUUID ();
				$formvalue ['id'] = $guuid;
				
				$date_debut_save = null; // jfb 2016-04-27 mantis 239 : init. date début
				$date_fin_save = null; // jfb 2016-04-27 mantis 239 : init. date fin
				// Transformation des Dates
				$transformDateFrUs = new Plugin_DateFrUs ();
				if ($formvalue ['debut'] && $formvalue ['debut'] != '')
				{
				    $date_debut_save = $formvalue ['debut']; // jfb 2016-04-27 mantis 239 : sauvegarde date début
					$formvalue ['debut'] = $transformDateFrUs->convertitDateFRUs ( $formvalue ['debut'] );
				}
				else
					$formvalue ['debut'] = null;
				
				if ($formvalue ['fin'] && $formvalue ['fin'] != '')
				{
				    $date_fin_save = $formvalue ['fin']; // jfb 2016-04-27 mantis 239 : sauvegarde date fin
					$formvalue ['fin'] = $transformDateFrUs->convertitDateFRUs ( $formvalue ['fin'] );
				}
				else
					$formvalue ['fin'] = null;
							
				function isUnique($idElt, &$formvalue, &$traitement, &$errors_messages, $message){
					$eltvalue = addslashes($formvalue[$idElt]);
					if(!empty($eltvalue)){	
						$req = $traitement->fetchAll()->where(addslashes($idElt).' ="'.$eltvalue.'" ');
						$nb_row = $req->count();
						if($nb_row > 0){
							$errors_messages[$idElt] = $message;
						}
					}
				}				
				isUnique('nom', $formvalue, $traitement , $errors_messages, $filter_no_html->filter( $i18n -> say('traitement_uniciteNom') ));
				
				//Trois param($donnees, $field, $fieldMessage, $errors_messages)
				Plugin_Fonctions::novirgule($formvalue['nom'], 'nom', 'novirguleinfield', $errors_messages);
				
				if(!empty($errors_messages)) throw new Exception();
				
				$traitement->populate ( $formvalue )->save ();
				
				// Historisation de la modif
				$traitement_id = $traitement->get ( 'id' );
				$formvalue ['id'] = $traitement_id;
                $formvalue ['debut'] = $date_debut_save; // jfb 2016-04-27 mantis 239 : restauration date début
                $formvalue ['fin'] = $date_fin_save; // jfb 2016-04-27 mantis 239 : restauration date fin

				// Mantis 377 : dans fsn_historique les clés type_id et etat_id doivent être en numérique
				if ($formvalue ['type_id'] && $formvalue ['type_id'] != '')
					$formvalue ['type_id'] = ( int ) $formvalue ['type_id'];
				if ($formvalue ['etat_id'] && $formvalue ['etat_id'] != '')
					$formvalue ['etat_id'] = ( int ) $formvalue ['etat_id'];
				
                $historisation = new Plugin_Historique ();
				$historisation->addhistory ( 'Traitement', $mode, $formvalue );
				
				$this->getSession ()->set ( 'flash_title', '' );
				$this->getSession ()->set ( 'flash_status', 'info' );
				$this->getSession ()->set ( 'flash_message', 'traitement ajouté' );
				
				if(!empty($idERt)){
					$this->forward ($this->getRequest('Traitement', 'index')."?ER=".$idERt);
				}
				else{
					$this->forward ( 'Traitement', 'index' );
				}
				
			}catch(Exception $e){
				$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'warning'); $this->getSession()->set('flash_message', '');
				$this->getSession()->setFlashErrors($errors_messages);
			}  
		}
		
		$this->_view->set ( 'helper_form', new Yab_Helper_Form ( $form ) );
	}

	public function actionEdit() {
		
		// appel fichier internationalisation
		$registry = Yab_Loader::getInstance()->getRegistry();
		$i18n = $registry -> get('i18n');
		$filter_no_html = new Yab_Filter_NoHtml();
		
		// jfb 2016-05-04 mantis 260 début
		//$mode = 'edit' ;
		$mode = self::MODE_UPDATE;
		// jfb 2016-05-04 mantis 260 fin
		$errors_messages = '';
		
		$traitement = new Model_Traitement($this->_request->getParams());	
		$form = new Form_Traitement($traitement);
		$id = $traitement->get('id');
		
        $formvalue = $form->getValues();	

		if(isset($_GET['ER']))
		{
			$idERt = str_replace('/','',$_GET['ER']);
			unset($_SESSION['idER2']);
			$_SESSION['idER2'] = $idERt ;
		}
		else{
			// $_SESSION['idER2'] = $traitement->get('er_id');
			$numerosER = $form->getElement('er_id')->get('options')->where('id= "'.$traitement->get('er_id').'"');
			$form->getElement('er_id')->set('options',$numerosER);
		}

        $this->getSession ()->set('titre', 'GESTION DES TRAITEMENTS');
		// $this -> getLayout() -> setFile("View/layout_fsn.html");

		if($form->isSubmitted() && $form->isValid()) {
			
			try{
				$date_debut_save = null; // jfb 2016-04-27 mantis 239 : init. date début
				$date_fin_save = null; // jfb 2016-04-27 mantis 239 : init. date fin
				// Transformation des Dates
				$transformDateFrUs = new Plugin_DateFrUs ();
				if ($formvalue ['debut'] && $formvalue ['debut'] != '')
				{
				    $date_debut_save = $formvalue ['debut']; // jfb 2016-04-27 mantis 239 : sauvegarde date début
					$formvalue ['debut'] = $transformDateFrUs->convertitDateFRUs( $formvalue ['debut'] );
				}
				else
					$formvalue ['debut'] = null;
				
				if ($formvalue ['fin'] && $formvalue ['fin'] != '')
				{
				    $date_fin_save = $formvalue ['fin']; // jfb 2016-04-27 mantis 239 : sauvegarde date fin
					$formvalue ['fin'] = $transformDateFrUs->convertitDateFRUs( $formvalue ['fin'] );
				}
				else
					$formvalue ['fin'] = null;							
				
				function isUnique($idElt, &$formvalue, &$traitement, &$id, &$errors_messages, $message){
					$eltvalue = addslashes($formvalue[$idElt]);
					if(!empty($eltvalue)){		
						$req = $traitement->fetchAll()->where(addslashes($idElt).' ="'.$eltvalue.'" and id !="'.$id.'" ');
						$nb_row = $req->count();
						if($nb_row > 0){
							$errors_messages[$idElt] = $message;
						}
					}
				}
				isUnique('nom', $formvalue, $traitement , $id, $errors_messages, $filter_no_html->filter( $i18n -> say('traitement_uniciteNom') ));
				
				//Trois param($donnees, $field, $fieldMessage, $errors_messages)
				Plugin_Fonctions::novirgule($formvalue['nom'], 'nom', 'novirguleinfield', $errors_messages);
				
				if(!empty($errors_messages)) throw new Exception();
				
				$traitement->populate($formvalue)->save();
				
				// Historisation de la modif
				$traitement_id = $traitement->get('id');
				$formvalue['id'] = $traitement_id ;
                $formvalue ['debut'] = $date_debut_save; // jfb 2016-04-27 mantis 239 : restauration date début
                $formvalue ['fin'] = $date_fin_save; // jfb 2016-04-27 mantis 239 : restauration date fin

				// Mantis 377 : dans fsn_historique les clés type_id et etat_id doivent être en numérique
				if ($formvalue ['type_id'] && $formvalue ['type_id'] != '')
					$formvalue ['type_id'] = ( int ) $formvalue ['type_id'];
				if ($formvalue ['etat_id'] && $formvalue ['etat_id'] != '')
					$formvalue ['etat_id'] = ( int ) $formvalue ['etat_id'];
				
                $historisation = new Plugin_Historique() ;
				$historisation->addhistory('Traitement',$mode,$formvalue) ; 
				
				$this->getSession()->set('flash_title', ''); 
				$this->getSession()->set('flash_status', 'info'); 
				$this->getSession()->set('flash_message', 'traitement modifié');

				if(!empty($idERt)){
					$this->forward ($this->getRequest('Traitement', 'index')."?ER=".$idERt);
				}
				else{
					$this->forward ( 'Traitement', 'index' );
				}
			
			}catch(Exception $e){
				$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'warning'); $this->getSession()->set('flash_message', '');
				$this->getSession()->setFlashErrors($errors_messages);
			} 
		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));
		$this->_view->set('form', $form);

	}
	
	public function actionShow() {
		
		$traitement = new Model_Traitement($this->_request->getParams());
		$form = new Form_Traitement($traitement);
		$formvalue = $form->getValues();
		
		// Recupération des valeurs par leurs id
		function getFieldValueById(&$traitement, $table, $tablecolum, $eltId, $valsearch, &$retour){
			$req = $traitement->getTable($table)->fetchAll()->where(addslashes($tablecolum).'= "'.$traitement->get($eltId).'" ');	
			$nb_row = $req->count();
			if($nb_row > 0){
				foreach($req as $row){
					$retour = $row[$valsearch];
				}
			} else $retour = '';
		}
		
		getFieldValueById($traitement, 'Model_Fsn_Categorie', 'id', 'type_id', 'categorie_value', $type_value);
		getFieldValueById($traitement, 'Model_Fsn_Categorie', 'id', 'etat_id', 'categorie_value', $etat_value);
		getFieldValueById($traitement, 'Model_Elementrecueilli', 'id', 'er_id', 'numero', $er_id_value);
		
		$intervenants = $traitement->getNomPrenomByIntervenantId($traitement->get('intervenant_id'));
		foreach($intervenants as $intervenant){
			$intervenant_value = $intervenant->get('nom_complet');
		}
		
		// $this->_view->set('helper_form', new Yab_Helper_Form($form));
		$this->_view->set('form', $form);			
		$this->_view->set('type_value', $type_value);			
		$this->_view->set('etat_value', $etat_value);			
		$this->_view->set('er_id_value', $er_id_value);			
		$this->_view->set('intervenant_value', $intervenant_value);			

	}

	public function actionDelete() {
		if(isset($_GET['ER']))
		{
			$idERt = str_replace('/','',$_GET['ER']);
			unset($_SESSION['idER2']);
			$_SESSION['idER2'] = $idERt ;
		
		}
		$traitement = new Model_Traitement($this->_request->getParams());
        
		// jfb 2016-05-04 mantis 260 début
        //$mode = 'delete' ;
		$mode = self::MODE_DELETE;
		// jfb 2016-05-04 mantis 260 fin
        $form = new Form_Traitement($traitement);
        $formvalue=$form->getValues();
        
		// jfb 2016-04-27 correctif fiche mantis 248 début
		$message = ''; // init. à blanc message de deletion
		
		try {
        	$traitement->delete();
		
			// Historisation de la modif
        	$traitement_id = $traitement->get('id');
        	$formvalue['id'] = $traitement_id ;

			// Mantis 377 : dans fsn_historique les clés type_id et etat_id doivent être en numérique
			if ($formvalue ['type_id'] && $formvalue ['type_id'] != '')
				$formvalue ['type_id'] = ( int ) $formvalue ['type_id'];
			if ($formvalue ['etat_id'] && $formvalue ['etat_id'] != '')
				$formvalue ['etat_id'] = ( int ) $formvalue ['etat_id'];
				
        	$historisation = new Plugin_Historique() ;
        	$historisation->addhistory('Traitement',$mode,$formvalue) ; 

        	$message='traitement supprimé';
        } catch (Exception $e) {
        	$message = 'Suppression Traitement impossible';
        }
        
		$this->getSession ()->set('titre', 'GESTION DES TRAITEMENTS');
		// $this -> getLayout() -> setFile("View/layout_fsn.html");

		$this->getSession()->set('flash_title', ''); 
		$this->getSession()->set('flash_status', 'info'); 
		//$this->getSession()->set('flash_message', 'Traitement effacé');
		$this->getSession()->set('flash_message', $message);
		// jfb 2016-04-27 correctif fiche mantis 248 fin
		
		if(!empty($idERt)){
			$this->forward ($this->getRequest('Traitement', 'index')."?ER=".$idERt);
		}
		else{
			$this->forward('Traitement', 'index');
		}
	}

}