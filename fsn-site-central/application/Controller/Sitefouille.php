<?php
class Controller_Sitefouille extends Yab_Controller_Action {

	public function filesUploadForm($name, $destination, $tabExtenssions = null) {
		if (isset ( $_POST ["btSubmit"] )) {
			foreach ( $_FILES [$name] ['error'] as $cle => $valeur ) {
				if (! is_uploaded_file ( $_FILES [$name] ['tmp_name'] [$cle] )) { // Vérifie que le fichier a été téléchargé par HTTP POST
					$error = "Downloading files does not succeed.";
				} else if ($valeur > 0) {
					$error = "Erreur lors du transfert du fichier : " . $_FILES ['photos'] ['tmp_name'] [$cle];
				}
			}
			if (! isset ( $error )) {
				foreach ( $_FILES [$name] ['error'] as $cle => $valeur ) {
					$extension_upload = strtolower ( substr ( strrchr ( $_FILES [$name] ['name'] [$cle], '.' ), 1 ) );
					if (($tabExtenssions != null and in_array ( $extension_upload, $tabExtenssions )) or $tabExtenssions == null) {
						$nom = $destination . "/" . $_FILES [$name] ['name'] [$cle] /*. $cle . ".{$extension_upload}"*/;
						$resultat = move_uploaded_file ( $_FILES [$name] ['tmp_name'] [$cle], $nom );
						if ($resultat) {
						}
					}
				}
			}
		}
	}
	
	public function listdir($dir, $format = null) {
		$list = array ();
		if (! file_exists ( $dir ))
			mkdir ( $dir, 0777, true);
		if ($dossier = opendir ( $dir )) {
			while ( false !== ($fichier = readdir ( $dossier )) ) {
				if ($fichier != '.' && $fichier != '..') {
					if ($format != null) {
						if (stripos ( $fichier, $format ))
							$list [] = $fichier;
					} else {
						$list [] = $fichier;
					}
				}
			}
			closedir ( $dossier );
		}
		return $list;
	}
	
	public function actionTestajax() {
		$this->getLayout ()->setEnabled ( false );
		$this->getView ()->setEnabled ( false );
		echo "1";
	}
	public function actionPhotoexportform() {
		$extensions_valides = array (
				'jpg',
				'jpeg',
				'gif',
				'png' 
		);
		$this->filesUploadForm ( "photos", "photo", $extensions_valides );
		$this->forward ( 'Sitefouille', 'confirmation' );
	}
	
	public function actionGestionphoto() {
		$sitefouille = new Model_Sitefouille ( $this->_request->getParams () );
		$form = new Form_Sitefouille ( $sitefouille );
		$id = $this->_request->getParams ();
		$this->_view->set ( 'photogrammetries', $this->listdir ( "../photogrammetrie/sitefouille/" . $form->getElement ( 'nomabrege' )->get ( 'value' ), "" ) );
		$this->_view->set ( 'helper_form', $form );
		$this->_view->set ( 'id', $id [0] );
	}
	
	public function actionGestionphotojson() {
		$sitefouille = new Model_Sitefouille ( $this->_request->getParams () );
		$id = $this->_request->getParams ();
		echo json_encode($this->listdir("../photogrammetrie/sitefouille/" . $sitefouille->get('nomabrege'),""));
		die();
	}
	
	public function actionUnzip($id = null, $file = null) {
        $sitefouille = new Model_Sitefouille ( array($id) );
		$form = new Form_Sitefouille ( $sitefouille );
        $dir = "../photogrammetrie/sitefouille/" . $form->getElement ( 'nomabrege' )->get ( 'value' ).'/'.$file;
        $models = array_merge($this->listdir($dir, '.wrl'), $this->listdir($dir, '.x3d'));
        $loader = Yab_Loader::getInstance();
        $config = $loader->getConfig();
		$this->_view->set ( 'nomZip', $file);
		$this->_view->set ( 'models', $models );
		$this->_view->set ( 'helper_form', $form );
		$this->_view->set ( 'id', $id);
    }
	
	public function actionUnzipjson($id = null, $file = null) {
		$sitefouille = new Model_Sitefouille ( array($id) );
        $dir = "../photogrammetrie/sitefouille/" . $sitefouille->get('nomabrege').'/'.$file;
		$models = array_merge($this->listdir($dir, '.wrl'), $this->listdir($dir, '.x3d'));
		$loader = Yab_Loader::getInstance();
		$config = $loader->getConfig();
		echo json_encode($models);
		die();
	}
	
	public function actionModel($id = null, $file = null, $filename = null) {
		$loader = Yab_Loader::getInstance();
		$config = $loader->getConfig();
		ob_clean();
        $sitefouille = new Model_Sitefouille ( array($id) );
        $in = $config['localisation']."/photogrammetrie/sitefouille/" . $sitefouille->get('nomabrege' ).'/'.$file.'/'.$filename;
        $ext = strtolower(pathinfo($in, PATHINFO_EXTENSION));
		if ($ext=='wrl') {
			$out = $config['localisation']."/photogrammetrie/sitefouille/" . $sitefouille->get('nomabrege' ).'/'.$file.'/'.pathinfo($filename, PATHINFO_FILENAME).".x3d";
			if (!file_exists($out)) {
				exec("aopt -i '$in' -x '$out'");
				unlink($in);
			}
			$this->forward ( 'Sitefouille', 'model', array ($id, $file, pathinfo($filename, PATHINFO_FILENAME).".x3d"));
		} elseif ($ext=='x3d') {
			if (!file_exists($out)) {
				$out = $config['localisation']."/photogrammetrie/sitefouille/" . $sitefouille->get('nomabrege' ).'/'.$file.'/'.pathinfo($filename, PATHINFO_FILENAME).".html";
				$binary = $config['localisation']."/photogrammetrie/sitefouille/" . $sitefouille->get('nomabrege' ).'/'.$file.'/'.pathinfo($filename, PATHINFO_FILENAME)."__bin";
				exec("aopt -i '$in' -G '$binary/is' -N '$out'");
			}
			header("location:/photogrammetrie/sitefouille/" . $sitefouille->get('nomabrege').'/'.$file.'/'.pathinfo($filename, PATHINFO_FILENAME).".html");
		}
		die();
    }
    
    public function actionDownloadmodel($id = null, $file = null) {
    	$sitefouille = new Model_Sitefouille ( array($id) );
    	$form = new Form_Sitefouille ( $sitefouille );
    	$dir = "../photogrammetrie/sitefouille/" . $form->getElement ( 'nomabrege' )->get ( 'value' ).'/'.$file;
    	$tmp_file = sys_get_temp_dir()."/".$file.'-'.time().".zip";
    	$zip = new ZipArchive();
    	if ($zip->open($tmp_file, ZipArchive::CREATE) == TRUE) {
    		$fichiers = scandir($dir.'/');
    		unset($fichiers[0], $fichiers[1]);
    		foreach($fichiers as $f) {
    			if (!$zip->addFile($dir.'/'.$f, $f)) {
    				die('Impossible d&#039;ajouter &quot;'.$f.'&quot;.<br/>');
    			}
    		}
    		$zip->close();
    	} else {
    		die('Erreur, impossible de créer l&#039;archive.');
    	}
    	header("Pragma: public");
    	header("Expires: 0");
    	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    	header("Cache-Control: public");
    	header("Content-Description: File Transfer");
    	header("Content-type: application/octet-stream");
    	header('Content-Disposition: attachment; filename="'.$file.'.zip"');
    	header("Content-Transfer-Encoding: binary");
    	header("Content-Length: ".filesize($tmp_file));
    	ob_clean();
    	$content = file_get_contents($tmp_file);
    	unlink($tmp_file);
    	echo $content;
    	die();
    }
	
	public function actionExportmodeleform() {
		$sitefouille = new Model_Sitefouille ( $_POST ['id'] );
		$form = new Form_Sitefouille ( $sitefouille );
		$extensions_valides = array ('zip');
		$path = "../photogrammetrie/sitefouille/". $form->getElement ( 'nomabrege' )->get ( 'value' );
		$this->filesUploadForm ( "photos", $path, $extensions_valides );
		$this->unzip($path);
		$this->forward ( 'Sitefouille', 'gestionphoto', array ($_POST ['id']));
	}
	
	public function actionUnlinkmodel($sitefouilleid = null, $model = null) {
		$sitefouille = new Model_Sitefouille ( $sitefouilleid );
		$form = new Form_Sitefouille ( $sitefouille );
		$this->rrmdir ( "../photogrammetrie/sitefouille/" . $form->getElement ( 'nomabrege' )->get ( 'value' ) . "/" . $model );
		$this->forward ( 'Sitefouille', 'gestionphoto', array ($sitefouilleid));
	}
	
	public function unzip($path) {
		$zips = $this->listdir($path, "zip");
		foreach ($zips as $z) {
			$zip = new ZipArchive();
			$zip_file = $path.'/'.$z;
			if ($zip->open($zip_file) === TRUE) {
				$zip->extractTo($path.'/'.basename($z, 'zip').'/');
				$zip->close();
				unlink($zip_file);
			} else {
				unlink($zip_file);
				die( "Échec de l'extraction de $z : fichier zip invalide.");
			}
		}
	}
	
	public function actionConfirmation() {
	
	}
	public function actionIndex() {
		$sitefouille = new Model_Sitefouille ();
		
		$session = $this->getSession();
		$requeteur = "requete_" . "sitefouille";
		$requeteurSession = ($session->has($requeteur) && !empty($session->get($requeteur))) ? $session->get($requeteur) : null;
		
		if (!is_null($requeteurSession) && !empty($requeteurSession)) {
			$sitefouilles = $sitefouille->fetchAll ()->where("id IN (" . $requeteurSession . ") " ) ;
		}
		else {
			$sitefouilles = $sitefouille->fetchAll ();
		}
		$this->_view->set ( 'sitefouilles_index', $sitefouilles );
		
		$session[$requeteur] = ''; // la requête étant consommée, on l'efface
		
		$loader = Yab_Loader::getInstance ();
		$config = $loader->getConfig ();
		$this->_view->set ( 'config', $config );
	}
	public function actionAdd() {
		
		// appel fichier internationalisation
		$registry = Yab_Loader::getInstance ()->getRegistry ();
		$i18n = $registry->get ( 'i18n' );
		$filter_no_html = new Yab_Filter_NoHtml ();
		
		$mode = self::ACTION_MODE_CREATE;
		$errors_messages = '';
		
		$sitefouille = new Model_Sitefouille ();
		$form = new Form_Sitefouille ( $sitefouille );
		$formvalue = $form->getValues ();
		
		if ($form->isSubmitted ()) {
			
			// Ajout du GUUID
			$generateGuuid = new Plugin_Guuid ();
			$guuid = $generateGuuid->GetUUID ();
			
			try {
				
				// Recuperation entiteadmin_id dans variables de session
				$user_session = $this->getSession ()->get ( 'session' );
				$entiteadmin_id = $user_session ['entiteadmin_id'];
				$formvalue ['id'] = $guuid;
				$formvalue ['fsn_entiteadmin_id'] = $entiteadmin_id;
				
				// Control champs requis
				function isNumber($idElt, &$formvalue, &$form, &$errors_messages, $message) {
					$eltvalue = addslashes ( $formvalue [$idElt] );
					if (! empty ( $eltvalue )) {
						if ($form->getElement ( $idElt )->getErrors ())
							$errors_messages [$idElt] = $message;
					}
				}
				function isUnique($idElt, &$formvalue, &$sitefouille, &$errors_messages, $message) {
					$eltvalue = addslashes ( $formvalue [$idElt] );
					if (! empty ( $eltvalue )) {
						$req = $sitefouille->fetchAll ()->where ( addslashes ( $idElt ) . ' ="' . $eltvalue . '" ' );
						$nb_row = $req->count ();
						if ($nb_row > 0) {
							$errors_messages [$idElt] = $message;
						}
					}
				}
				
				$errors = $form->getErrors ();
				unset ( $errors ['surfaceestime'] );
				unset ( $errors ['lambert_x'] );
				unset ( $errors ['lambert_y'] );
				unset ( $errors ['altitude'] );
				if ($errors) {
					$errors_messages [] = $filter_no_html->filter ( $i18n->say ( 'verror_requisAll' ) );
				}
				isNumber ( 'surfaceestime', $formvalue, $form, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'sitefouille_requisSurface' ) ) );
				isNumber ( 'lambert_x', $formvalue, $form, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'sitefouille_requisLambertX' ) ) );
				isNumber ( 'lambert_y', $formvalue, $form, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'sitefouille_requisLambertY' ) ) );
				isNumber ( 'altitude', $formvalue, $form, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'sitefouille_requisAltitude' ) ) );
				isNumber ( 'datation_debut', $formvalue, $form, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'sitefouille_requisDatation_debut' ) ) );
				isNumber ( 'datation_fin', $formvalue, $form, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'sitefouille_requisDatation_fin' ) ) );
				
				isUnique ( 'nom', $formvalue, $sitefouille, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'sitefouille_uniciteNom' ) ) );
				isUnique ( 'nomabrege', $formvalue, $sitefouille, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'sitefouille_uniciteNomAbrege' ) ) );
				isUnique ( 'numerosite', $formvalue, $sitefouille, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'sitefouille_uniciteNumeroSite' ) ) );
				
				if ($formvalue ['datation_debut'] == 0 || $formvalue ['datation_fin'] == 0) {
					$errors_messages ['datation_debut'] = $filter_no_html->filter ( $i18n->say ( 'sitefouille_nullDatation' ) );
				}
				if (is_numeric ( $formvalue ['datation_debut'] ) && is_numeric ( $formvalue ['datation_fin'] ) && $formvalue ['datation_debut'] > $formvalue ['datation_fin']) {
					$errors_messages ['datation_debut'] = $filter_no_html->filter ( $i18n->say ( 'sitefouille_superieurDatation' ) );
				}
				if (is_numeric ( $formvalue ['datation_debut'] ) && $formvalue ['datation_debut'] < - 5000) {
					$errors_messages ['datation_debut'] = $filter_no_html->filter ( $i18n->say ( 'sitefouille_limiteDatation_debut' ) );
				}
				if (is_numeric ( $formvalue ['datation_fin'] ) && $formvalue ['datation_fin'] > date ( 'Y' )) {
					$errors_messages ['datation_fin'] = $filter_no_html->filter ( $i18n->say ( 'sitefouille_limiteDatation_fin' ) );
				}
				
				// Caractères spéciaux interdits: Trois param($donnees, $field, $fieldMessage, $errors_messages)
				Plugin_Fonctions::nospecialcharacter ( $formvalue ['nomabrege'], 'nomabrege', 'sitefouille_nopecialcaractere', $errors_messages );
				
				if (! empty ( $errors_messages ))
					throw new Exception ();
				
				$sitefouille->populate ( $formvalue )->save ();
				
				// Debut création arborescence multimédia
				// Recuperation nomabrege de l'organisme
				$user_session = $this->getSession ()->get ( 'session' );
				$entiteadmin_id = $user_session ['entiteadmin_id'];
				
				$organismes = $sitefouille->getEntiteOrganisme ()->where ( 'enti.id="' . $entiteadmin_id . '" ' );
				$nb_row = $organismes->count ();
				
				if ($nb_row > 0) {
					foreach ( $organismes as $organisme ) {
						$arborescence = '../mediatheque/albums/' . $organisme ['nomabrege_organisme'] . '/sitefouille/' . $formvalue ['nomabrege'];
					}
					
					if (! is_dir ( $arborescence )) {
						mkdir ( $arborescence, 0777, true );
					}
				}
				// Fin création arborescence multimédia
				
				// Historisation de la modif
				$id_sitefouille = $sitefouille->get ( 'id' );
				$formvalue ['id'] = $id_sitefouille;
				$formvalue ['fsn_entiteadmin_id'] = ( int ) $entiteadmin_id; // jfb 2016-05-03 mantis 259
				$historisation = new Plugin_Historique ();
				
				$formvalue = $form->getTypedValues ( $formvalue );
				$historisation->addhistory ( 'Sitefouille', self::MODE_CREATE, $formvalue );
				
				// jfb 2016-06-03 mantis 261 début
				// Lorsque l'utilisateur crée un nouveau site, ce site devient le site en cours (alimentation de la variable de session par ce nouveau site)
				$this->getSession ()->set ( 'sitefouille_id', $id_sitefouille );
				// jfb 2016-06-03 mantis 261 fin
				// Mantis 319 début : Le choix doit être mémorisé de session en session lorsque l'utilisateur choisit ou crée un site
				$user_id = Yab_Loader::getInstance()->getSession()->get('session_user_id');
				$user = new Model_User();
				$arrayvalues = array("sitefouille_id" => $id_sitefouille);
				$updateResult = $user->update($arrayvalues, " id = '" . $user_id . "' ");
				// Mantis 319 fin
				
				$this->getSession ()->set ( 'flash_title', '' );
				$this->getSession ()->set ( 'flash_status', 'info' );
				$this->getSession ()->set ( 'flash_message', 'sitefouille as been added' );
				$this->forward ( 'Sitefouille', 'index' );
			} catch ( Exception $e ) {
				$this->getSession ()->set ( 'flash_title', '' );
				$this->getSession ()->set ( 'flash_status', 'warning' );
				$this->getSession ()->set ( 'flash_message', '' );
				$this->getSession ()->setFlashErrors ( $errors_messages );
			}
		}
		// $this->_view->set('helper_form', new Yab_Helper_Form($form));
		$this->_view->set ( 'helper_form', $form );
	}
	public function actionShow($id = null) {
		$mode = self::ACTION_MODE_SHOW;
		
		$sitefouille = new Model_Sitefouille ( $this->_request->getParams () );
		$form = new Form_Sitefouille ( $sitefouille );
		$formvalue = $form->getValues ();
		$id_sitefouille = $sitefouille->get ( 'id' );
		
		$releves = $sitefouille->getTable ( 'Model_Releve' )->fetchAll ()->where ( 'sitefouille_id="' . $id_sitefouille . '"' );
		$nb_row = $releves->count ();
		
		if ($nb_row > 0) {
			$idCroquis = Array ();
			foreach ( $releves as $releve ) {
				$idCroquis [] = $releve->get ( 'croquis_id' );
			}
		} else
			$idCroquis [] = "";
		
		$sitefouille->populate ( $formvalue )->save ();
		
		// Recuperation nomabrege de l'organisme
		$user_session = $this->getSession ()->get ( 'session' );
		$entiteadmin_id = $user_session ['entiteadmin_id'];
		
		$organismes = $sitefouille->getEntiteOrganisme ()->where ( 'enti.id="' . $entiteadmin_id . '" ' );
		$nb_row = $organismes->count ();
		
		if ($nb_row > 0) {
			foreach ( $organismes as $organisme ) {
				$codeorganisme = $organisme ['nomabrege_organisme'];
			}
		} else
			$codeorganisme = "UASD";
			
			// $this->_view->set('helper_form', new Yab_Helper_Form($form));
		$this->_view->set ( 'helper_model', $sitefouille );
		$this->_view->set ( 'helper_form', $form );
		$this->_view->set ( 'idCroquis', $idCroquis );
		$this->_view->set ( 'codesite', $formvalue ['nomabrege'] );
		$this->_view->set ( 'codeorganisme', $codeorganisme );
		$this->_view->set ( 'id', $id );
		$this->_view->set ( 'sf_id', $sitefouille->get('id') );
		$this->_view->set ( 'photogrammetries', $this->listdir ( "../photogrammetrie/sitefouille/" . $form->getElement ( 'nomabrege' )->get ( 'value' ), ".zip" ) );
	}

	public function actionEdit() {
		
		// appel fichier internationalisation
		$registry = Yab_Loader::getInstance ()->getRegistry ();
		$i18n = $registry->get ( 'i18n' );
		$filter_no_html = new Yab_Filter_NoHtml ();
		
		$mode = self::ACTION_MODE_UPDATE;
		$errors_messages = '';
		
		$sitefouille_associe = '';
		
		$sitefouille = new Model_Sitefouille ( $this->_request->getParams () );
		$form = new Form_Sitefouille ( $sitefouille );
		$formvalue = $form->getValues ();
		$id = $this->_request->getParams ();
		
		if ($form->isSubmitted ()) {
			
			try {
				// Blocage modification nomabrege si site rélié à Us/ER
				$lastformvalues = $sitefouille->fetchAll ()->where ( 'id="' . $id [0] . '"' );
				$nb_row = $lastformvalues->count ();
				if ($nb_row > 0) {
					foreach ( $lastformvalues as $lastformvalue ) {
						if ($formvalue ['nomabrege'] != $lastformvalue ['nomabrege']) {
							$uses = $sitefouille->getTable ( 'Model_Us' )->fetchAll ()->where ( 'sitefouille_id="' . $id [0] . '"' );
							$lien_sitefouille_us = $uses->count ();
							
							if ($lien_sitefouille_us > 0) {
								$errors_messages ['nomabrege'] = $filter_no_html->filter ( $i18n->say ( 'sitefouille_associe' ) );
							}
						}
					}
				}
				
				// Control champs requis
				function isNumber($idElt, &$formvalue, &$form, &$errors_messages, $message) {
					$eltvalue = addslashes ( $formvalue [$idElt] );
					if (! empty ( $eltvalue )) {
						if ($form->getElement ( $idElt )->getErrors ())
							$errors_messages [$idElt] = $message;
					}
				}
				function isUnique($idElt, &$formvalue, &$sitefouille, &$id, &$errors_messages, $message) {
					$eltvalue = addslashes ( $formvalue [$idElt] );
					if (! empty ( $eltvalue )) {
						$req = $sitefouille->fetchAll ()->where ( addslashes ( $idElt ) . ' ="' . $eltvalue . '" and id !="' . $id [0] . '" ' );
						$nb_row = $req->count ();
						if ($nb_row > 0) {
							$errors_messages [$idElt] = $message;
						}
					}
				}
				
				$errors = $form->getErrors ();
				unset ( $errors ['surfaceestime'] );
				unset ( $errors ['lambert_x'] );
				unset ( $errors ['lambert_y'] );
				unset ( $errors ['altitude'] );
				if ($errors) {
					$errors_messages [] = $filter_no_html->filter ( $i18n->say ( 'verror_requisAll' ) );
				}
				
				isNumber ( 'surfaceestime', $formvalue, $form, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'sitefouille_requisSurface' ) ) );
				isNumber ( 'lambert_x', $formvalue, $form, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'sitefouille_requisLambertX' ) ) );
				isNumber ( 'lambert_y', $formvalue, $form, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'sitefouille_requisLambertY' ) ) );
				isNumber ( 'altitude', $formvalue, $form, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'sitefouille_requisAltitude' ) ) );
				isNumber ( 'datation_debut', $formvalue, $form, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'sitefouille_requisDatation_debut' ) ) );
				isNumber ( 'datation_fin', $formvalue, $form, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'sitefouille_requisDatation_fin' ) ) );
				
				isUnique ( 'nom', $formvalue, $sitefouille, $id, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'sitefouille_uniciteNom' ) ) );
				isUnique ( 'nomabrege', $formvalue, $sitefouille, $id, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'sitefouille_uniciteNomAbrege' ) ) );
				isUnique ( 'numerosite', $formvalue, $sitefouille, $id, $errors_messages, $filter_no_html->filter ( $i18n->say ( 'sitefouille_uniciteNumeroSite' ) ) );
				
				if ($formvalue ['datation_debut'] == 0 || $formvalue ['datation_fin'] == 0) {
					$errors_messages ['datation_debut'] = $filter_no_html->filter ( $i18n->say ( 'sitefouille_nullDatation' ) );
				}
				if (is_numeric ( $formvalue ['datation_debut'] ) && is_numeric ( $formvalue ['datation_fin'] ) && $formvalue ['datation_debut'] > $formvalue ['datation_fin']) {
					$errors_messages ['datation_debut'] = $filter_no_html->filter ( $i18n->say ( 'sitefouille_superieurDatation' ) );
				}
				if (is_numeric ( $formvalue ['datation_debut'] ) && $formvalue ['datation_debut'] < - 5000) {
					$errors_messages ['datation_debut'] = $filter_no_html->filter ( $i18n->say ( 'sitefouille_limiteDatation_debut' ) );
				}
				if (is_numeric ( $formvalue ['datation_fin'] ) && $formvalue ['datation_fin'] > date ( 'Y' )) {
					$errors_messages ['datation_fin'] = $filter_no_html->filter ( $i18n->say ( 'sitefouille_limiteDatation_fin' ) );
				}
				
				// Caractères spéciaux interdits: Trois param($donnees, $field, $fieldMessage, $errors_messages)
				Plugin_Fonctions::nospecialcharacter ( $formvalue ['nomabrege'], 'nomabrege', 'sitefouille_nopecialcaractere', $errors_messages );
				
				if (! empty ( $errors_messages ))
					throw new Exception ();
				
				$sitefouille->populate ( $formvalue )->save ();
				
				// Debut création arborescence multimédia
				// Recuperation nomabrege de l'organisme
				$user_session = $this->getSession ()->get ( 'session' );
				$entiteadmin_id = $user_session ['entiteadmin_id'];
				
				$organismes = $sitefouille->getEntiteOrganisme ()->where ( 'enti.id="' . $entiteadmin_id . '" ' );
				$nb_row = $organismes->count ();
				
				if ($nb_row > 0) {
					foreach ( $organismes as $organisme ) {
						$arborescence = '../mediatheque/albums/' . $organisme ['nomabrege_organisme'] . '/sitefouille/' . $formvalue ['nomabrege'];
					}
					
					if (! is_dir ( $arborescence )) {
						mkdir ( $arborescence, 0777, true );
					}
				}
				// Fin création arborescence multimédia
				
				// Historisation de la modif
				$id_sitefouille = $sitefouille->get ( 'id' );
				$formvalue ['id'] = $id_sitefouille;
				// jfb 2016-05-02 mantis 257 début
				$fsn_entiteadmin_id = $sitefouille->get ( 'fsn_entiteadmin_id' );
				$formvalue ['fsn_entiteadmin_id'] = ( int ) $fsn_entiteadmin_id; // jfb 2016-05-03 mantis 259
				                                                                 // jfb 2016-05-02 mantis 257 fin
				$historisation = new Plugin_Historique ();
				
				$formvalue = $form->getTypedValues ( $formvalue );
				$historisation->addhistory ( 'Sitefouille', self::MODE_UPDATE, $formvalue );
				
				$this->getSession ()->set ( 'flash_title', '' );
				$this->getSession ()->set ( 'flash_status', 'info' );
				$this->getSession ()->set ( 'flash_message', 'sitefouille as been edited' );
				
				$this->forward ( 'Sitefouille', 'index' );
			} catch ( Exception $e ) {
				$this->getSession ()->set ( 'flash_title', '' );
				$this->getSession ()->set ( 'flash_status', 'warning' );
				$this->getSession ()->set ( 'flash_message', '' );
				$this->getSession ()->setFlashErrors ( $errors_messages );
			}
		}
		
		// $this->_view->set('helper_form', new Yab_Helper_Form($form));
		$this->_view->set ( 'sf_id', $sitefouille->get('id') );
		$this->_view->set ( 'helper_form', $form );
	}
	public function actionSelect($layout = true, $sitefouille_id = null, $controller = null, $action = null) {
		$loader = Yab_Loader::getInstance ();
		$request = $loader->getRequest ();
		$session = $loader->getSession ();
		
		$this->getLayout ()->setEnabled ( $layout );
		
		$sitefouille = new Model_Sitefouille ();
		$form_sitefouille = new Form_SitefouilleSearch ( $sitefouille );
		
		$forward_controller = ! empty ( $controller ) ? $controller : $session->get ( 'forward_controller' );
		$forward_action = ! empty ( $action ) ? $action : $session->get ( 'forward_action' );
		if ($form_sitefouille->isSubmitted ()) {
			
			$formvalue = $form_sitefouille->getValues ();
			
			if ($form_sitefouille->isValid ()) {
				$sitefouille_id = $formvalue ['sitefouille_id'];
				$session = $this->getSession ();
				$session->set ( 'sitefouille_id', $sitefouille_id );
				$this->getSession ()->set ( 'flash_title', '' );
				$this->getSession ()->set ( 'flash_status', 'info' );
				$this->getSession ()->set ( 'flash_message', 'sitefouille as been selected' );
			}
			
			$this->forward ( $forward_controller, $forward_action );
		}
		
		$this->_view->set ( 'form', $form_sitefouille );
	}
	public function actionSelection($layout = false) {
		$this->getLayout ()->setEnabled ( $layout );
		$msf = new Model_Sitefouille ();
		$sf_list = $msf->getVisibleSitefouilles ()->setKey ( 'id' )->setValue ( 'nom_nomabrege' )->toArray ();
		$current_sf = Yab_Loader::getInstance ()->getSession ()->get ( 'sitefouille_id' );
		$id_select = "select_sf";
		
		if (isset ( $_POST [$id_select] )) {
			$this->getSession ()->set ( 'sitefouille_id', $_POST [$id_select] );

			// Mantis 319 début : Le choix doit être mémorisé de session en session lorsque l'utilisateur choisit un site ou Tous sites
			$user_id = Yab_Loader::getInstance()->getSession()->get('session_user_id');
			$user = new Model_User();
			$arrayvalues = array("sitefouille_id" => $_POST[$id_select]);
			$updateResult = $user->update($arrayvalues, " id = '" . $user_id . "' ");
			// Mantis 319 fin

			$this->getSession ()->set ( 'flash_title', '' );
			$this->getSession ()->set ( 'flash_status', 'info' );
			$this->getSession ()->set ( 'flash_message', 'site fouille selectionné' );
			
			// Concerne uniquement les ERs
			if (isset ( $this->getSession ()["us_id"] ))
				unset ( $this->getSession ()["us_id"] );
			if (isset($this->getSession ()["idER2"]))
				unset($this->getSession ()["idER2"]) ;
			
			$exp_url = explode ( '/', $_SERVER ['REQUEST_URI'] );
			foreach ( $exp_url as $index => $segment ) {
				if ($segment == "public") {
					if (! empty ( $exp_url [$index + 3] ))
						$this->forward ( $exp_url [$index + 1], $exp_url [$index + 2] . '/' . $exp_url [$index + 3] );
					else {
						if(empty($exp_url[$index+2]))
							$exp_url[$index+2] = "index";
						$this->forward ( $exp_url [$index + 1], $exp_url [$index + 2] );
					}
				}
			}
		}
		
		$this->_view->set ( 'sf_list', $sf_list );
		$this->_view->set ( 'current_sf', $current_sf );
		$this->_view->set ( 'id_select', $id_select );
		
		if (! $layout) {
			$this->_view->setFile ( 'View/sitefouille/select.html' );
		}
	}
	public function actionVisuObj($layout = false, $sitefouille_id) {
		$this->getLayout ()->setEnabled ( $layout );
		$sitefouille = new Model_Sitefouille ( $sitefouille_id );
		$sitefouilles = $sitefouille->fetchAll ();
		$sitefouille_id = $sitefouille->get ( 'id' );
		$nomabrege = $sitefouille->get ( 'nomabrege' );
		$this->_view->set ( 'nomabrege', $nomabrege );
		$this->_view->set ( 'sitefouilles', $sitefouilles );
	}
	public function actionDelete() {
		$sitefouille = new Model_Sitefouille ( $this->_request->getParams () );
		$form = new Form_Sitefouille ( $sitefouille );
		$formvalue = $form->getValues ();
		
		$formvalue ['id'] = $sitefouille->get ( 'id' );
		$sf_id = $formvalue ['id']; // jfb 2016-04-22 fiche mantis 231
		                            
		// jfb 2016-05-02 mantis 257 début
		$fsn_entiteadmin_id = $sitefouille->get ( 'fsn_entiteadmin_id' );
		$formvalue ['fsn_entiteadmin_id'] = ( int ) $fsn_entiteadmin_id; // jfb 2016-05-03 mantis 259
		                                                                 // jfb 2016-05-02 mantis 257 fin
		                                                                 
		// jfb 2016-04-18 correctif fiche mantis 99 début
		$message = ''; // init. à blanc message de deletion
		
		try {
			$sitefouille->delete (); // try delete avant historisation
			                         
			// historisation début
			$historisation = new Plugin_Historique ();
			$formvalue = $form->getTypedValues ( $formvalue );
			$historisation->addhistory ( 'Sitefouille', self::MODE_DELETE, $formvalue );
			// historisation fin
			
			// jfb 2016-04-22 fiche mantis 231 début : supprimer les lignes dans fsn_historique dont la colonne sitefouille_id est égal à l'id du site supprimé
			$fsn_historique = new Model_Fsn_Historique ();
			$fsn_historique->delete ( "sitefouille_id = '" . $sf_id . "'" );
			// jfb 2016-04-22 fiche mantis 231 fin
					
			// Mantis 319 début : Si l'utilisateur delete le site de fouille sur lequel il se trouve, changer en Tous sites
			$session_sf_id = Yab_Loader::getInstance()->getSession()->get('sitefouille_id');
			if ($session_sf_id == $sf_id) {
				Yab_Loader::getInstance()->getSession()->set('sitefouille_id', "");
				$user_id = Yab_Loader::getInstance()->getSession()->get('session_user_id');
				$user = new Model_User();
				$arrayvalues = array("sitefouille_id" => "");
				$updateResult = $user->update($arrayvalues, " id = '" . $user_id . "' ");
			}
			// Mantis 319 fin
			
			$message = 'sitefouille supprimé';
		} catch ( Exception $e ) {
			$message = 'Suppression sitefouille impossible';
		}
		
		$this->getSession ()->set ( 'flash_title', '' );
		$this->getSession ()->set ( 'flash_status', 'info' );
		$this->getSession ()->set ( 'flash_message', $message );
		
		$this->forward ( 'Sitefouille', 'index' );
	}
	
		/*Georeferencement*/
	public function actionTelechargement($id_sitefouille = null) {

		$sitefouille = new Model_Sitefouille($id_sitefouille);
		$nomabrege_sitefouille = $sitefouille->get('nomabrege'); 
		
		$dir = '../geolocalisation/sitefouille/'.$sitefouille->get('nomabrege').'/ortophoto';
		$dir_carte = '../geolocalisation/sitefouille/'.$sitefouille->get('nomabrege').'/carte';
		
		/*Alternative (code temporaire creation des arborescences) avant la mise en place de la photogrammetrie*/										
		is_dir ( $dir ) ? '' :  mkdir ( $dir, 0777, true );
		is_dir ( $dir_carte ) ? '' :  mkdir ( $dir_carte, 0777, true );
				
		$this->_view->set('dir', $dir);
		$this->_view->set('dir_carte', $dir_carte);
		$this->_view->set('sitefouille', $sitefouille);
		$this->_view->set('id_sitefouille', $id_sitefouille);
		$this->_view->set('nomabrege_sitefouille', $nomabrege_sitefouille);

	}
	
	public function actionDeleteOrthoPhoto($id_sitefouille = null, $rep = null, $image = null){		

		$rep_image = $rep.'/'.$image;
		
		$sitefouille = new Model_Sitefouille($id_sitefouille);		
		$nomabrege_sitefouille = $sitefouille->get('nomabrege');

		$dir = '../geolocalisation/sitefouille/'.$nomabrege_sitefouille.'/'.$rep_image;	
	
		unlink($dir);		
		
		$this->getSession()->set('flash_title','');
		$this->getSession()->set('flash_status','info'); 
		$this->getSession()->set('flash_message', 'suppression effectuée');
		
		$this->forward("Sitefouille", "Telechargement", $sitefouille->getPrimary());
	}

	public function actionDeleteCarte($id_sitefouille = null, $rep = null, $image = null){
		$rep_image = $rep.'/'.$image;
	
		$sitefouille = new Model_Sitefouille($id_sitefouille);
		$nomabrege_sitefouille = $sitefouille->get('nomabrege');
	
		$dir = '../geolocalisation/sitefouille/'.$nomabrege_sitefouille.'/'.$rep_image;
	
		$this->rrmdir($dir);
	
		$this->getSession()->set('flash_title','');
		$this->getSession()->set('flash_status','info');
		$this->getSession()->set('flash_message', 'suppression effectuée');
	
		$this->forward("Sitefouille", "Telechargement", $sitefouille->getPrimary());
	}
	
	private function rrmdir($dir) {
		//echo $dir . '<br>'; // Mantis 395
		if (is_dir($dir)) {
			$objects = scandir($dir);
			foreach ($objects as $object) {
				if ($object != "." && $object != "..") {
					if (filetype($dir."/".$object) == "dir") 
						$this->rrmdir($dir."/".$object); 
					else 
						unlink($dir."/".$object);
				}
			}
			reset($objects);
			rmdir($dir);
		}
	}

	public function actionGeoRef($id_sitefouille = null, $rep = null, $image = null){
		
		$sitefouille = new Model_Sitefouille($id_sitefouille);		
		$nomabrege_sitefouille = $sitefouille->get('nomabrege');
		
		$rep_image = $rep.'/'.$image;
		$img_size = getimagesize(Yab_Loader::getInstance()->getConfig()["localisation"]."/geolocalisation/sitefouille/".$nomabrege_sitefouille."/ortophoto/".$image);
		// Mantis 369 début
		// L'index 0 contient la largeur. L'index 1 contient la hauteur
		/*
		$this->_view->set("image_height", $img_size[0]);
		$this->_view->set("image_width", $img_size[1]);
		*/
		$this->_view->set("image_width", $img_size[0]);
		$this->_view->set("image_height", $img_size[1]);
		// Mantis 369 fin		
		$this->_view->set("image", $rep_image);
		$this->_view->set("nomabrege_sitefouille", $nomabrege_sitefouille);
		$this->_view->set('sitefouille', $sitefouille);
		$this->_view->set('id_sitefouille', $id_sitefouille);

		
	}
	
	public function actionAddOrtophoto($id_sitefouille = null){
		
		$sitefouille = new Model_Sitefouille($id_sitefouille);		

		if(isset($_FILES['photos']))
		{
			$elts = $_FILES['photos']['name'];
			
			$nomabrege_sitefouille = $sitefouille->get('nomabrege');
		
			$dir = '../geolocalisation/sitefouille/'.$nomabrege_sitefouille.'/ortophoto/';	

			foreach($elts as $i => $elt){
				$extensions = array('.png', '.gif', '.jpg', '.jpeg', '.tif');
				$extension = strrchr($_FILES['photos']['name'][$i], '.');				
				
				//Début des vérifications de sécurité...
				if(!in_array($extension, $extensions))
				{
					$message_error = 'Vous devez uploader un fichier de type png, gif, jpg, jpeg ou tif...';
				}
				else{
					$fichier = basename($_FILES['photos']['name'][$i]);
					
					//On formate le nom du fichier
					$fichier = strtr($fichier, 'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
					$fichier = preg_replace('/([^.a-z0-9]+)/i', '-', $fichier);
	 
					if(move_uploaded_file($_FILES['photos']['tmp_name'][$i], $dir . $fichier))
					{
						$message_error = 'Upload effectue avec succes !';				
					}
					else
					{
						$message_error = 'Echec de l\'upload !';
					}
				}
			}
		
		} 
		else $message_error = 'Vous devez uploader un fichier de type png, gif, jpg, jpeg ou tif...';
		
		$this->getSession()->set('flash_title','');
		$this->getSession()->set('flash_status','info'); 
		$this->getSession()->set('flash_message', $message_error);
		
		$this->forward("Sitefouille", "Telechargement", $sitefouille->getPrimary());
	}
	
	public function actionTraitementCoords($id_sitefouille = null, $rep = null, $image = null){		
		
		$rep_image = $rep.'/'.$image;
		$image = explode('.',$image);
		$image = $image[0];
		
		$sitefouille = new Model_Sitefouille($id_sitefouille);
		$nomabrege_sitefouille = $sitefouille->get('nomabrege');
		
		$my_data = json_decode($_POST['tabCoords']);		
		$coords = json_encode($my_data);
		
		if(!empty($coords)){			
			$taille = sizeof($my_data);
			
			$code_translate = "gdal_translate -of VRT -a_srs EPSG:4326 -a_nodata 0 -gcp "; 
			foreach($my_data as $index => $coord){				
				if($index == $taille -1)
					$code_translate .= $coord->x.' '.$coord->y.' '.$coord->lonY.' '.$coord->latX;
				else
					$code_translate .= $coord->x.' '.$coord->y.' '.$coord->lonY.' '.$coord->latX.' -gcp ';
			}		
			
			// Recuperation du chemin dynamique= "..../FSN/"
			$root = dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR;
			
			$strs = explode(DIRECTORY_SEPARATOR, $root);

			$taille_strs = sizeof($strs);
			
			$chemin = '';
			foreach($strs as $i => $ph){
				if($i != $taille_strs -2 && $i != $taille_strs -1)
					$chemin .= $ph.'/'; 
			}
			
			$code_translate .= ' "'.$chemin.'geolocalisation/sitefouille/'.$nomabrege_sitefouille.'/'.$rep_image.'" "'.$chemin.$image.'.vrt"';
			$code_warp = 'gdalwarp -of VRT -t_srs EPSG:4326 "'.$chemin.$image.'.vrt" "'.$chemin.'geolocalisation/sitefouille/'.$nomabrege_sitefouille.'/'.$image.'_gdal.vrt"';
			$code_gdal2tuiles = 'gdal2tiles.py -z 17-23 "'.$chemin.'geolocalisation/sitefouille/'.$nomabrege_sitefouille.'/'.$image.'_gdal.vrt" "'.$chemin.'geolocalisation/sitefouille/'.$nomabrege_sitefouille.'/carte/'.$image.'_tuile'.date('d-m-Y_H-i').'"';
					
			// JFB
			//var_dump($code_translate); die();
			
			exec($code_translate);
			exec($code_warp);
			exec($code_gdal2tuiles);
			
			unLink($chemin.$image.'.vrt');
			unLink($chemin.'geolocalisation/sitefouille/'.$nomabrege_sitefouille.'/'.$image.'_gdal.vrt');
			
			// retour ajax
			echo 'Georeferencement termine';	
		}
	}
	
	private function lire($chemin) {
		// $lines = file('../geolocalisation/sitefouille/CYG/carte/maptiler1/openlayers.html'); 
		$lines = file('../'.$chemin.'/openlayers.html'); 
		
		$count = 0;
		foreach ($lines as $lineNumber => $lineContent)
		{
			
			if (strpos($lineContent, 'var mapBounds') !== false) {
				//echo $lineContent;
				$words = explode('(', $lineContent);
				$words = explode(')', $words[1]);
				$words = explode(',', $words[0]);
				return $words;
			}
				
			$count++;
		}
		
	}
	
	public function actionContours($id=null, $category=null, $nom=null) {
		$sitefouille = new Model_Sitefouille ($id);
		$chemin='geolocalisation/sitefouille/'.$sitefouille->get("nomabrege").'/'.$category.'/'.$nom;

		$coors = $this->lire($chemin); 

		$us = new Model_Us();
		$tab = $us->fetchAll()->where('sitefouille_id="'.$id.'"')->orderBy(array('identification'=>'ASC' ));
	
		$fai = new Model_Fai();
		$tab_fai = $fai->fetchAll()->where('sitefouille_id="'.$id.'"')->orderBy(array('identification'=>'ASC' ));
	
		$this->_view->set('tab',$tab);
		$this->_view->set('fai',$tab_fai);
		$this->_view->set('sf',$sitefouille);
		
		//$latitude1= 48.9355135;
		//$longitude1=2.3565533;
		//$latitude2= 48.935910542;
		//$longitude2=2.3570680819;
		
		$latitude1= $coors[1];
		$longitude1= $coors[0];
		$latitude2= $coors[3];
		$longitude2= $coors[2];
		
		$minZ= 17;
		$maxZ= 23;
				
		$this->_view->set('latitude1',$latitude1);
		$this->_view->set('latitude2',$latitude2);
		$this->_view->set('longitude1',$longitude1);
		$this->_view->set('longitude2',$longitude2);
		$this->_view->set('minZ',$minZ);
		$this->_view->set('maxZ',$maxZ);
		$this->_view->set('chemin',$chemin);
	
	}
	
	
}

