<?php

class Controller_Participation extends Yab_Controller_Action {

	public function actionIndex() {
		$participation = new Model_Participation();

		$participations = $participation->fetchAll();

		$this->_view->set('participations', $participations);
	}
	public function actionListParticipants($layout=false, $oa_id, $v_action=false) {
		$this->getLayout()->setEnabled($layout);

		$participation = new Model_Participation();
		$form_participation = new Form_Participation($participation);

		$oa_participants = $participation->getListParticipation()->where('oa_id = "'.$oa_id.'" ');

		$this->_view->set('form_participation',$form_participation);
		$this->_view->set('oa_participants', $oa_participants);
		$this->_view->set('v_action', $v_action);

		if (!$layout) { $this->_view->setFile('View/participation/list_participants_simple.html') ; }
	}
	public function actionAdd() {
		$participation = new Model_Participation();

		$form = new Form_Participation($participation);

		if($form->isSubmitted() && $form->isValid()) {
    
      // Ajout du GUUID 
      $generateGuuid = new Plugin_Guuid() ;
      $guuid = $generateGuuid->GetUUID() ;
      $form->setElement('id', array('type'=>'text', 'value' => $guuid ) ) ;

			$participation->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'participation as been added');

			$this->forward('Participation', 'index');

		}
		$this->_view->set('helper_form', new Yab_Helper_Form($form));
	}
	public function actionEdit() {
		$participation = new Model_Participation($this->_request->getParams());

		$form = new Form_Participation($participation);

		if($form->isSubmitted() && $form->isValid()) {

			$participation->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'participation as been edited');

			$this->forward('Participation', 'index');

		}
		$this->_view->set('helper_form', new Yab_Helper_Form($form));
	}
	public function actionDelete() {
		$participation = new Model_Participation($this->_request->getParams());

		$participation->delete();

		$this->getSession()->set('flash', 'participation as been deleted');

		$this->forward('Participation', 'index');
	}
}