<?php

class Controller_User extends Yab_Controller_Action {

	public function actionIndex() {

		$user = new Model_User();
		$form = new Form_User($user);

		$users = $user->getVisibleUsers();

		$this->_view->set('users_index', $users);
		$this->_view->set('form', $form);
	}
    
    public function actionListSimple($layout=false, $entiteadmin_id=1 ) {
    
        $this->getLayout()->setEnabled($layout);
    
		$user = new Model_User();

		$users = $user->getUserDetail();

        if( !empty($entiteadmin_id) )
            $users = $users->where('entiteadmin_id = '.$entiteadmin_id ) ;
     
		$this->_view->set('users', $users);


	}

	public function actionShow(){
		$user = new Model_User($this->_request->getParams());
		$mode = self::ACTION_MODE_SHOW ;
		$form_user = new Form_User($user,$mode);
		$this->_view->set('form_user', $form_user);
	}

	public function actionAdd() {
        
        $mode = self::ACTION_MODE_CREATE ;
        
		$errors_messages = '';
		
		$user = new Model_User();
		$form = new Form_User($user);

		if($form->isSubmitted() && $form->isValid()) {
            try{				
				$formvalue = $form->getValues() ;
				$password = $formvalue['mdp'] ;
				$formvalue['mdp'] = sha1(sha1($password).$password) ;
				
				// Recupération session entiteadmin_id
				$entiteadmin = Yab_Loader::getInstance()->getSession()->get('session')['entiteadmin_id'];
				$formvalue['entiteadmin_id'] = $entiteadmin;
				
				//Trois param($donnees, $field, $fieldMessage, $errors_messages)
				Plugin_Fonctions::novirgule($formvalue['login'], 'login', 'novirguleinfield', $errors_messages);
				
				if(!empty($errors_messages)) throw new Exception();

				// jfb 2016-06-14 mantis 302 début : vérification de l'existence du user
				$modeluserlogin = new Model_User();
				$userlogin = $modeluserlogin->getUserLogin($formvalue['login']);
				if (!empty($userlogin[0]['login'])) {
					$this->getSession()->set('flash_title', '');
					$this->getSession()->set('flash_status', 'warning');
					$this->getSession()->set('flash_message', 'Un user avec ce login existe déjà !');
				}
				else {
				// jfb 2016-06-14 mantis 302 fin
				
				$formvalue['sitefouille_id'] = ""; // Mantis 319 : Le nouvel user démarre avec Tous sites
					
				$user->populate($formvalue)->save();			
				
				// Historisation de la modif
				$id_user = $user->get('id');
				$formvalue['id'] = $id_user ;
				$historisation = new Plugin_Historique() ;
				
				$formvalue = $form->getTypedValues($formvalue);
				$historisation->addhistory('User', self::MODE_CREATE, $formvalue) ;
		  
				$this->getSession()->set('flash_title', ''); 
				$this->getSession()->set('flash_status', 'info'); 
				$this->getSession()->set('flash_message', 'user as been added');

				$this->forward('User', 'index');
				
				} // jfb 2016-06-14 mantis 302
			} catch(Exception $e){
				$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'warning'); $this->getSession()->set('flash_message', '');
				$this->getSession()->setFlashErrors($errors_messages);
			}

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$mode = self::ACTION_MODE_UPDATE ;        
      
		$errors_messages = '';
		
		$user = new Model_User($this->_request->getParams());
		$form = new Form_User($user);
		
		$mdpinitial = $user->get('mdp'); // jfb 2016-06-14 mantis 300 : récupération du mot de passe avant édition

		if($form->isSubmitted() && $form->isValid()) {
			try{				
				$formvalue = $form->getValues();
				$password = $formvalue['mdp'];
				if ($password != $mdpinitial) // jfb 2016-06-14 mantis 300 : si le mot de passe a changé, le crypter
					$formvalue['mdp'] = sha1(sha1($password).$password) ;
				
				//Trois param($donnees, $field, $fieldMessage, $errors_messages)
				Plugin_Fonctions::novirgule($formvalue['login'], 'login', 'novirguleinfield', $errors_messages);
				
				if(!empty($errors_messages)) throw new Exception();
				
				$user->populate($formvalue)->save();

				// Historisation de la modif
				$id_user = $user->get('id');
				$formvalue['id'] = $id_user ;
				$historisation = new Plugin_Historique() ;
				
				$formvalue = $form->getTypedValues($formvalue);
				$historisation->addhistory('User', self::MODE_UPDATE, $formvalue) ;
		  
				$this->getSession()->set('flash_title', ''); 
				$this->getSession()->set('flash_status', 'info'); 
				$this->getSession()->set('flash_message', 'user as been edited');

				$this->forward('User', 'index');
			}catch(Exception $e){
				$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'warning'); $this->getSession()->set('flash_message', '');
				$this->getSession()->setFlashErrors($errors_messages);
			} 

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$user = new Model_User($this->_request->getParams());
		$form = new Form_User($user);
		$formvalue = $form->getValues();

		$historisation = new Plugin_Historique();
		$formvalue = $form->getTypedValues($formvalue);
        $historisation->addhistory('User', self::MODE_DELETE, $formvalue);
		
		$user->delete();

		$this->getSession()->set('flash', 'user as been deleted');

		$this->forward('User', 'index');

	}

}