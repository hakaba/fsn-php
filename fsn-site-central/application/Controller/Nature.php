<?php

class Controller_Nature extends Yab_Controller_Action {

	public function actionIndex() {

		$nature = new Model_Nature();

		$natures = $nature->fetchAll();

		$this->_view->set('natures', $natures);
	}

	public function actionAdd() {

		$nature = new Model_Nature();

		$form = new Form_Nature($nature);

		if($form->isSubmitted() && $form->isValid()) {

			$nature->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'nature as been added');

			$this->forward('Nature', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$nature = new Model_Nature($this->_request->getParams());

		$form = new Form_Nature($nature);

		if($form->isSubmitted() && $form->isValid()) {

			$nature->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'nature as been edited');

			$this->forward('Nature', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$nature = new Model_Nature($this->_request->getParams());

		$nature->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'nature as been deleted');

		$this->forward('Nature', 'index');

	}

}