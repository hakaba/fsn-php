<?php

class Controller_Analyser_Elementrecueilli extends Yab_Controller_Action {

	public function actionIndex() {

		$elementrecueilli = new Model_Elementrecueilli();

		$elementrecueillis = $elementrecueilli->fetchAll();

		$this->_view->set('elementrecueillis', $elementrecueillis);
	}

	public function actionAdd() {

		$elementrecueilli = new Model_Elementrecueilli();

		$form = new Form_Elementrecueilli($elementrecueilli);

		if($form->isSubmitted() && $form->isValid()) {
      
      // Ajout du GUUID 
      $generateGuuid = new Plugin_Guuid() ;
      $guuid = $generateGuuid->GetUUID() ;
      $form->setElement('id', array('type'=>'text', 'value' => $guuid ) ) ;

			$elementrecueilli->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); 
      $this->getSession()->set('flash_status', 'info'); 
      $this->getSession()->set('flash_message', 'elementrecueilli as been added');

			$this->forward('Elementrecueilli', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$elementrecueilli = new Model_Elementrecueilli($this->_request->getParams());

		$form = new Form_Elementrecueilli($elementrecueilli);

		if($form->isSubmitted() && $form->isValid()) {

			$elementrecueilli->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'elementrecueilli as been edited');

			$this->forward('Elementrecueilli', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}
/*
	public function actionDelete() {

		$elementrecueilli = new Model_Elementrecueilli($this->_request->getParams());

		$elementrecueilli->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'elementrecueilli as been deleted');

		$this->forward('Elementrecueilli', 'index');

	}
*/

  public function actionErPolar($layout=false) {
  
    $this->getLayout()->setEnabled($layout);
    
    $session = $this->getSession(); 
    $sitefouille_id = $session->get('sitefouille_id')  ;
    $sitefouilles = new Model_Sitefouille($sitefouille_id) ;
    $sitefouille_info = $sitefouilles->fetchAll()->toRow() ;    
    $this->_view->set('sitefouille_info', $sitefouille_info);   
        
    /* Liste des statuts d�finis */
		$er_statut = new Model_Statut();
    $er_statuts = $er_statut->fetchAll();
    $this->_view->set('er_statuts', $er_statuts);
    
    /* Liste des phases chronoliques d�finies */
    $phasechrono = new Model_Phasechrono();
		$phasechronos = $phasechrono->fetchAll();
    $this->_view->set('phasechronos', $phasechronos);
    
    $elementrecueilli = new Model_Elementrecueilli();
    $elementrecueillis = $elementrecueilli->getErStatutCounts() ;
    $this->_view->set('elementrecueillis', $elementrecueillis);

    $this->_view->setFile('View/analyser/elementrecueilli/er_polar.html');

	}
  
  public function actionFonction($layout=false ,$render=null) {
  
    $this->getLayout()->setEnabled($layout);

    $session = $this->getSession(); 
    $sitefouille_id = $session->get('sitefouille_id')  ;
    $sitefouilles = new Model_Sitefouille($sitefouille_id) ;
    $sitefouille_info = $sitefouilles->fetchAll()->toRow() ;    
    $this->_view->set('sitefouille_info', $sitefouille_info);   

    /* Liste des statuts d�finis */
		$fonction = new Model_Fonction();
		$fonctions = $fonction->fetchAll();
		$this->_view->set('fonctions', $fonctions);
    
    /* Liste des phases chronoliques d�finies */
    $phasechrono = new Model_Phasechrono();
		$phasechronos = $phasechrono->fetchAll()->where('sitefouille_id = "'.$sitefouille_id.'" ' )->orderBy(array('datatdebbas_aaaa'=>'ASC' ));
    $this->_view->set('phasechronos', $phasechronos);
        
		$elementrecueilli = new Model_Elementrecueilli();
    $elementrecueillis = $elementrecueilli->getErFonctionCounts() ;
    if (!empty($us_id) ) {
      $elementrecueillis = $elementrecueillis->where('us_id = "'.$us_id.'" ' ) ;   
    }
    $this->_view->set('fonction_elementrecueillis', $elementrecueillis);

    // Par defaut la view est View\Dashboard\Elementrecueilli\fonction.html
    if ($render=='pie') {
      $this->_view->setFile('View/analyser/elementrecueilli/fonction_pie.html');
    } else if($render=='bar'){
      $this->_view->setFile('View/analyser/elementrecueilli/fonction_bar.html');
    } else if($render=='histo3d'){
      $this->_view->setFile('View/analyser/elementrecueilli/fonction_histo3d.html');    
    } else {
      $this->_view->setFile('View/analyser/elementrecueilli/fonction.html');
    }  
	}    
  
  public function actionClasse($layout=false ,$render=null) {
  
    $this->getLayout()->setEnabled($layout);

    $session = $this->getSession(); 
    $sitefouille_id = $session->get('sitefouille_id')  ;
    $sitefouilles = new Model_Sitefouille($sitefouille_id) ;
    $sitefouille_info = $sitefouilles->fetchAll()->toRow() ;    
    $this->_view->set('sitefouille_info', $sitefouille_info);   
    
    /* Liste des statuts d�finis */
		$classe = new Model_Classe();
		$classes = $classe->fetchAll();
		$this->_view->set('classes', $classes);
    
    /* Liste des phases chronoliques d�finies */
    $phasechrono = new Model_Phasechrono();
		$phasechronos = $phasechrono->fetchAll()->where('sitefouille_id = "'.$sitefouille_id.'" ' )->orderBy(array('datatdebbas_aaaa'=>'ASC' ));
    $this->_view->set('phasechronos', $phasechronos);
    
		$elementrecueilli = new Model_Elementrecueilli();
		$elementrecueillis = $elementrecueilli->getErClasseCounts() ;
    if (!empty($us_id) ) {
      $elementrecueillis = $elementrecueillis->where('us_id = "'.$us_id.'" ' ) ;   
    }
    $this->_view->set('classe_elementrecueillis', $elementrecueillis);

    // Par defaut la view est View\Dashboard\Elementrecueilli\fonction.html
    if ($render=='pie') {
      $this->_view->setFile('View/analyser/elementrecueilli/classe_pie.html');
    } else if($render=='bar'){
      $this->_view->setFile('View/analyser/elementrecueilli/classe_bar.html');
    } else if($render=='histo3d'){
      $this->_view->setFile('View/analyser/elementrecueilli/classe_histo3d.html');    
    } else if($render=='polar'){
      $this->_view->setFile('View/analyser/elementrecueilli/classe_polar.html');    
    } else {
      $this->_view->setFile('View/analyser/elementrecueilli/classe.html');
    }  
	}  
}