<?php

class Controller_Analyser_Sitefouille extends Yab_Controller_Action {

	public function actionIndex() {

    $session = $this->getSession(); 
    $sitefouille_id = $session->has('sitefouille_id') ? $session->get('sitefouille_id') : null ;

    if (!empty($sitefouille_id) ) {      
      $this->forward('Analyser_Sitefouille', 'Show', array('sitefouille_id' => $sitefouille_id) );
    } else {
      $this->getSession()->set('flash_title', 'Analyses'); 
      $this->getSession()->set('flash_status', 'warning'); 
      $this->getSession()->set('flash_message', 'Veuillez choisir un site de fouille');
      $this->forward('Analyser_Sitefouille', 'List');    
    }
    return ; 
		
	}

  public function actionList() {

		$sitefouille = new Model_Sitefouille();

		$sitefouilles = $sitefouille->fetchAll();

		$this->_view->set('sitefouilles', $sitefouilles);
	}
    
  public function actionCloudpoints($layout=false) {
  
    $this->getLayout()->setEnabled($layout);
    $session = $this->getSession(); 
    $sitefouille_id = $session->get('sitefouille_id')  ;
    $sitefouilles = new Model_Sitefouille($sitefouille_id) ;
    $sitefouille_info = $sitefouilles->fetchAll()->toRow() ;    
    $this->_view->set('sitefouille_info', $sitefouille_info); 
        
    $us = new Model_Us();
    /**
     * Dans le cas o� l'us est d�fini par plusieurs points topographiques
     * d�termination d'un point m�dian # group by us.id     
     */    
    $topo_us = $us->getAvgTopoUs();    
    $this->_view->set('topo_us', $topo_us );
    
    $topo_us_bornes = $us->getMinMaxTopoUs();
    $this->_view->set('topo_us_bornes', $topo_us_bornes ); 
         	
    $this->_view->setFile('View/analyser/sitefouille/cloud_points.html');

	} 
  
	public function actionAdd() {

		$sitefouille = new Model_Sitefouille();

		$form = new Form_Sitefouille($sitefouille);

		if($form->isSubmitted() && $form->isValid()) {

			// Ajout du GUUID 
      $generateGuuid = new Plugin_Guuid() ;
      $guuid = $generateGuuid->GetUUID() ;
      $form->setElement('id', array('type'=>'text', 'value' => $guuid ) ) ;
      
      $sitefouille->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'sitefouille as been added');

			$this->forward('Sitefouille', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$sitefouille = new Model_Sitefouille($this->_request->getParams());

		$form = new Form_Sitefouille($sitefouille);

		if($form->isSubmitted() && $form->isValid()) {

			$sitefouille->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'sitefouille as been edited');

			$this->forward('Sitefouille', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}
  
  public function actionShow() {

		$sitefouille = new Model_Sitefouille($this->_request->getParams());
    
    $parameters = $this->_request->getParams();
    $session = $this->getSession();
    $session->set('sitefouille_id',$parameters[0]   ); 
        
		$form = new Form_Sitefouille($sitefouille);

		if($form->isSubmitted() && $form->isValid()) {

			$sitefouille->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'sitefouille as been edited');

			$this->forward('Sitefouille', 'index');

		}
    $this->getSession()->set('title_precision', $sitefouille->get('nom'));
		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

  public function actionAnalyseUs($layout=true) {
	  
    if(!$layout){
      $this->getLayout()->setEnabled(false);
    }
    
    $session = $this->getSession();
    $sitefouille_id = $session->get('sitefouille_id'); 
    
    /**
     * Liste des infos us
     * % declare unitestratigraphique(US_ID,IDENTIFICATION,DEBUT,FIN,TAQ,TPQ,CONTEXTE,CONSISTANCE,METHODEFOUILLE,HOMOGENEITE,SITEFOUILLE_NOM,SITEFOUILLE_NOMABREGE).     
     */
    $us_intervenant = new Model_Us();
    $us_base_faits =  $us_intervenant->getUsDetail()->where('sitefouille_id = "'.$sitefouille_id.'" ' ) ;
    
    $facts = array() ;
      /*
      'unitestratigraphique' => array('US_ID','IDENTIFICATION','DEBUT','FIN','TAQ','TPQ','CONTEXTE','CONSISTANCE','METHODEFOUILLE','HOMOGENEITE','SITEFOUILLE_NOM','SITEFOUILLE_NOMABREGE',)
      */
    /*
    foreach($us_base_faits as $values){
    
      $taq_aaaa = $values->get('taq_aaaa') ;
      $taq_mm = $values->get('taq_mm') ;
      $taq_jj = $values->get('taq_jj') ;
      $tpq_aaaa = $values->get('tpq_aaaa') ;
      $tpq_mm = $values->get('tpq_mm') ;
      $tpq_jj = $values->get('tpq_jj') ;
      
      $us_facts_id = $values->get('us_id');        
      $us_facts = array (
        'us_id' => $values->get('us_id'),
        'identification' => $values->get('us_identification'),
        'debut' => $values->get('debut'),
        'fin' => $values->get('fin'),
        'taq' => $taq_aaaa,
        'tpq' => $tpq_aaaa,
        'contexte' => $values->get('contexte'),
        'consistance' => $values->get('consistance'),
        'methodefouille' => $values->get('methodefouille'),
        'homogeneite' => $values->get('homogeneite'),
        'sitefouille_nom' => $values->get('sitefouille_nom'),
        'sitefouille_nomabrege' => $values->get('sitefouille_nomabrege'),
      );
      $facts['us'][] = $us_facts ;
          
    }
     */
     
    /**
     * Superposition des US
     * % declare superposition(anterieur_id,posterieur_id,incertitude,sptype,dureemin,dureemax ).      
     */    
    $superposition = new Model_Superposition();
    $superpositions = $superposition->getAnteroposterite() ;
    // filtre du sitefouille 
    $superpositions = $superpositions->where('usa.sitefouille_id = "'.$sitefouille_id.'" ' ) ;
    $superpositions = $superpositions->where('usp.sitefouille_id = "'.$sitefouille_id.'" ' ) ;
    
    foreach($superpositions as $values){
      $superposition_id = $values->get('us_ante_identification').'_'.$values->get('us_post_identification') ;        
      $us_anterieur = $values->get('us_ante_identification') ;
      $us_posterieur = $values->get('us_post_identification') ;
      $superposition_facts = array (
        'us_anterieur' => $us_anterieur,
        'us_posterieur' => $us_posterieur,
        //'anterieur_id' => $values->get('anterieur_id'),
        // 'posterieur_id' => $values->get('posterieur_id'),
        //'incertitude' => $values->get('incertitude'),
        // 'sptype' => $values->get('sptype'),
        // 'dureemin' => $values->get('dureemin'),
        // 'dureemax' => $values->get('dureemax'),
        //'observation' => $values->get('observation'),
      );
      // $facts['superposition'][$superposition_id] = $superposition_facts ;
      $facts['superposition'][] = $superposition_facts ;
    
    }
    /**
     * Synchronisation des US
     * % declare $synchronisation(us_left_id,us_right_id,incertitude,sptype,dureemin,dureemax ).      
     */
    $synchronisation = new Model_Synchronisation();
    $synchronisations = $synchronisation->getSynchronisation() ;
    // filtre du sitefouille 
    $synchronisations = $synchronisations->where('usl.sitefouille_id = "'.$sitefouille_id.'" ' ) ;
    $synchronisations = $synchronisations->where('usr.sitefouille_id = "'.$sitefouille_id.'" ' ) ;
    
    foreach($synchronisations as $values){
      $synchronisation_id = $values->get('us_left_identification').'_'.$values->get('us_right_identification') ;        
      $us_left = $values->get('us_left_identification') ;
      $us_right = $values->get('us_right_identification') ;
      $synchronisation_facts = array (
        'us_left' => $us_left,
        'us_right' => $us_right,
        //'anterieur_id' => $values->get('anterieur_id'),
        // 'posterieur_id' => $values->get('posterieur_id'),
        //'incertitude' => $values->get('incertitude'),
        // 'sptype' => $values->get('sptype'),
        // 'dureemin' => $values->get('dureemin'),
        // 'dureemax' => $values->get('dureemax'),
        //'observation' => $values->get('observation'),
      );
      // $facts['superposition'][$superposition_id] = $superposition_facts ;
      $facts['synchronisation'][] = $synchronisation_facts ;
    
    }
    
    //$rules = array() ;
    $rules = "findAllconsistance(IDENTIFICATION, CONSISTANCE) :- \r\n";
    $rules .= " findall(IDENTIFICATION, 'Meuble').\r\n" ;
    $rules .= " concat(IDENTIFICATION, '</br>, ', _),\r\n";
	  $rules .= " write(_).\r\n";
    $rules .= "\r\n";  
    /*
    $rules = "findAnterieur(X) :- \r\n";
    $rules .= " findall(X, anterieur(X,Y), _).\r\n" ;
    $rules .= "\r\n";
    */
    // Appel � Prolog
    $analyseProlog = new Plugin_Prolog() ;
    
    $rules = $analyseProlog->stratiProlog() ;
    
    // $request = $analyseProlog->findPosterieur('CYG.1026') ;
    $request = $analyseProlog->incoherenceTemporelle();
    $analyser = $analyseProlog->exec_prolog($facts, $rules, $request) ;
    
    $this->_view->set('analyser', $analyser ); 
    
    $this->_view->setFile('View/analyser/sitefouille/analyse_us.html');

	}
  
  public function actionDashboard($layout=false) {
  
    $this->getLayout()->setEnabled($layout);

    $this->_view->setFile('View/analyser/sitefouille/dashboard.html');

	}
	
}