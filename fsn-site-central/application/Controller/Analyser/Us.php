<?php

class Controller_Analyser_Us extends Yab_Controller_Action {

	public function actionIndex() {

		$us = new Model_Us();

		$uses = $us->fetchAll();

		$this->_view->set('uses', $uses);
	}

	public function actionShow() {

		$this->getLayout()->setEnabled(false);

		$us = new Model_Us($this->_request->getParams());

		$mode = 'show' ;

		$form = new Form_Us($us, $mode);

		$this->_view->set('helper_form', new Yab_Helper_Form($form));
		$this->_view->set('form', $form);

	}

	public function actionVisu() {

		$us = new Model_Us($this->_request->getParams());

		$form = new Form_Us($us);

		if($form->isSubmitted() && $form->isValid()) {

			$us->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'us as been edited');

			$this->forward('Us', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionUsTimeline($layout=false) {

		// true : le layout (page des menus ) s'affiche, false pas de layout (page des menus )
		$this->getLayout()->setEnabled($layout);

		$us = new Model_Us();

		$uses = $us->countErbyUs() ;

		$this->_view->set('uses', $uses);

		$this->_view->setFile('View/analyser/us/us_timeline.html');

	}

    public function actionUsErConcentration($layout=false) {

        // true : le layout (page des menus ) s'affiche, false pas de layout (page des menus )
        $this->getLayout()->setEnabled($layout);

        $us = new Model_Us();

        $uses = $us->countErbyUs() ;

        $this->_view->set('uses', $uses);

        // $this->_view->setFile('View/analyser/us/us_er_concentration.html');

    }
    
	public function actionTaqTpqTimeline($layout=false) {

		// true : le layout (page des menus ) s'affiche, false pas de layout (page des menus )
		$this->getLayout()->setEnabled($layout);

		$us = new Model_Us();

		$formSearch = new Form_UsSearch($us);
    
    $formSearchvalue = $formSearch->getValues() ;

    $identification =(!empty($formSearchvalue['identification']) ) ? $formSearchvalue['identification'] : null ;
    
		if($formSearch->isSubmitted() && $formSearch->isValid() && !empty($identification) ) {

      $list_us_search = '( ' ;
      if (is_array($identification) ) {
        $comma = '' ;
        foreach($identification as $us ) {
          $list_us_search .= $comma.'"'.$us.'"';
          $comma = ', ' ;  
        }   
      } else {
        $list_us_search .= '"'.$identification.'"' ;
      }
      $list_us_search = ') ' ;
			$uses = $us->getUsDetail()->where('us.identification IN '.$list_us_search );	

		} else {
      $uses = null ;
    }

		$this->_view->set('formSearch', $formSearch);

		$this->_view->set('uses', $uses);

		$this->_view->setFile('View/analyser/us/us_taq_tpq_timeline.html');

	}
    
    public function actionPeriodeTimeline($layout=false) {

		// true : le layout (page des menus ) s'affiche, false pas de layout (page des menus )
		$this->getLayout()->setEnabled($layout);

		$us = new Model_Us();

		$formSearch = new Form_UsSearch($us);
    
    $formSearchvalue = $formSearch->getValues() ;

    $identification =(!empty($formSearchvalue['identification']) ) ? $formSearchvalue['identification'] : null ;
    
		if($formSearch->isSubmitted() && $formSearch->isValid() && !empty($identification) ) {

      $list_search_us = '( ' ;
      if (is_array($identification) ) {
        $comma = '' ;
        foreach($identification as $search_us ) {
          $list_search_us .= $comma.'"'.$search_us.'"';
          $comma = ', ' ;  
        }   
      } else {
        $list_search_us .= '"'.$identification.'"' ;
      }
      $list_search_us .= ') ' ;
      
			$uses = $us->getUsDetail()->where('us.identification IN '.$list_search_us );	

		} else {
      $uses = null ;
    }

		$this->_view->set('formSearch', $formSearch);

		$this->_view->set('uses', $uses);

		$this->_view->setFile('View/analyser/us/us_periode_timeline.html');

	}
  
    public function getUsDetailAjax($return_type='json', $render_type='PeriodeTimeline', $identification) {
    $this->getLayout()->setEnabled(false);
    
    $us = new Model_Us();
    
    $identification =(!empty($formSearchvalue['identification']) ) ? $formSearchvalue['identification'] : null ;
    
		if(!empty($identification) ) {

      $list_search_us = '( ' ;
      if (is_array($identification) ) {
        $comma = '' ;
        foreach($identification as $search_us ) {
          $list_search_us .= $comma.'"'.$search_us.'"';
          $comma = ', ' ;  
        }   
      } else {
        $list_search_us .= '"'.$identification.'"' ;
      }
      $list_search_us .= ') ' ;
      
			$uses = $us->getUsDetail()->where('us.identification IN '.$list_search_us );
		} else {
      $uses = null ;
    }
    
    if ( $render_type =='PeriodeTimeline' ) {
    
      $us_data = array(
        'categories' => array(),
        'series' =>array(),
        );
      
      if (!empty($uses) ) {  
        
        foreach($uses as $us_key => $us_value){
          $sitefouille_nom = $us_value->get('sitefouille_nom') ; 
          $us_id = $us_value->get('us_id') ;
          $us_id_ident = hash('crc32', $us_id ) ;
          $identification = $us_value->get('us_identification') ;
          // $nombre_er = $us_value->get('er_count') ; 
          $us_id = $us_value->get('us_id') ;
          $us_content = '<small>';
          $us_content .= ' <span id="detail_'.$us_id.'" data-toggle="tooltip" data-placement="right" title="d&eacute;tail U.S. ">';
          // $us_content .= ' <a href="'.$this->getRequest('Dashboard_Us', 'show', array($us_id) ) .'" target="_blank" title="d&eacute;tail US '.$identification.'">'.$identification.'</a></span>' ;
          $us_content .= ' <a href="#" onClick="showEvent(\''.$us_id.'\')">'.$identification.'</a></span>' ;
          // $us_content .= ' <span class="badge">'.$nombre_er.'</span>';
          $us_content .= '</small>';
          
          /* R�cup�ration des Dates de la phase Chronologique potentiellement renseign�e */    
          $phasechrono_debut = $us_value->has('phasechrono_datatdebbas_aaaa') ? $us_value->get('phasechrono_datatdebbas_aaaa') : null ;
          $us_debut = $us_value->has('tpq_aaaa') ? $us_value->get('tpq_aaaa') : null ;
          $us_debut = !empty($us_debut) ? $us_debut : (!empty($phasechrono_debut) ? intval($phasechrono_debut): -500 ) ;
          
          $phasechrono_fin = $us_value->has('phasechrono_datatfinhaut_aaaa') ? $us_value->get('phasechrono_datatfinhaut_aaaa') : null ;    
          $us_fin = $us_value->has('taq_aaaa') ? $us_value->get('taq_aaaa') : null ;
          $us_fin = !empty($us_fin) ? $us_fin : (!empty($phasechrono_fin) ? intval($phasechrono_fin) : 2015 )  ;     

          $us_timeline_data['categories'][] = $identification  ;
          $us_timeline_data['series'][] =array($us_debut,$us_fin ) ; 
          /*
          array (
            'color' => '#'.substr(hash('crc32', $us_value->get('phasechrono_identification') ), -6 ), 
            'data' => array($us_debut,$us_fin )
          ) ;
          */
                    
        }
      }
    
    } else if($render_type =='TaqTpqTimeline') {
    
    } 
    
    if ( $return_type != 'json' ) {
    
    } else {
      $us_data = json_encode($us_data, JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE) ;
    }     
    //var_dump($us_data) ;
    
  }
  
	public function actionUsPhases($layout=false) {

		// true : le layout (page des menus ) s'affiche, false pas de layout (page des menus )
		$this->getLayout()->setEnabled($layout);

		$session = $this->getSession();
		$sitefouille_id = $session->get('sitefouille_id');

		/* Liste des Phases Chronologique */
		$phasechrono = new Model_Phasechrono();
		$phasechronos = $phasechrono->fetchAll()->where('sitefouille_id = "'.$sitefouille_id.'"' )->orderBy(array('datatdebbas_aaaa' => 'ASC' ) );

		$groups = array();
		$phasechronologiques = array() ;
		foreach($phasechronos as $key => $value ) {

			$datatdebbas_aaaa = $value->has('datatdebbas_aaaa') ? $value->get('datatdebbas_aaaa') : '2015' ;
			// Complete avec des 0 devant la date si < 1000
			$datatdebbas_aaaa = str_pad($datatdebbas_aaaa, 4, '0', STR_PAD_LEFT ) ;
			$datatdebbas_mm = $value->has('datatdebbas_mm') ? $value->get('datatdebbas_mm') : '00' ;
			$datatdebbas_jj = $value->has('datatdebbas_jj') ? $value->get('datatdebbas_jj') : '00' ;
			$datatdebbas_ad = $value->has('datatdebbas_ad') ? $value->get('datatdebbas_ad') : '' ;
			$avant_jc = (!empty($datatdebbas_ad) ) ? '-' : '';
			$phase_date_deb = (!empty($datatdebbas_aaaa) ) ? $avant_jc.$datatdebbas_aaaa.'-01-01' : '2015-01-01' ;

			$datatfinhaut_aaaa = $value->has('datatfinhaut_aaaa') ? $value->get('datatfinhaut_aaaa') : '2100' ;
			// Complete avec des 0 devant la date si < 1000
			$datatfinhaut_aaaa = str_pad($datatfinhaut_aaaa, 4, '0', STR_PAD_LEFT ) ;
			$datatfinhaut_mm = $value->has('datatfinhaut_mm') ? $value->get('datatfinhaut_mm') : '00' ;
			$datatfinhaut_jj = $value->has('datatfinhaut_jj') ? $value->get('datatfinhaut_jj') : '00' ;
			$datatfinhaut_ad = $value->has('datatfinhaut_ad') ? $value->get('datatfinhaut_ad') : '' ;
			$avant_jc = (!empty($datatfinhaut_ad) ) ? '-' : '';
			$phase_date_fin = (!empty($datatdebbas_aaaa) ) ? $avant_jc.$datatfinhaut_aaaa.'-01-01' : '2015-01-01' ;

			$background_phasechronos = array (
        'id' => 'bg'.hash('crc32', $value->get('id') ) , 
        'group' => hash('crc32',  $value->get('id') ) ,
        'content' => $value->get('identification'),
        'value' => $value->get('id'),
        'start' => $phase_date_deb,
        'end' => $phase_date_fin, 
        'type' => 'background',
        'className' => 'style_'.hash('crc32', $value->get('id') ),
			);
			$phasechronologiques[] = $background_phasechronos ;


			$group_phasechrono = array (
        'id' => hash('crc32', $value->get('id') ) ,
        'content' => $nom = $value->get('description'),
        'value' => $value->get('id') ,
        'className' => 'style_'.hash('crc32', $value->get('id') ),
			);
			$groups[$key] = $group_phasechrono ;
			$last_key = $key+1 ;


		}
		$group_phasechrono = array (
        'id' => hash('crc32', 'NA' ) ,
        'content' => 'Non Attribu&eacute;e',
        'value' => 'NA' ,
        'className' => 'style_'.hash('crc32', 'NA' ),
		);
		$groups[$last_key] = $group_phasechrono ;

		//$this->_view->set('intervenants', $planning_groups);
		$this->_view->set('phasechronos', $groups);

		/* Liste des intervations par us */
		$us_phasechrono = new Model_Us();
		$us_phasechrono_detail = $us_phasechrono->getUsPhasechronos() ;

		// $phasechronologiques = array() ;
		foreach($us_phasechrono_detail as $key => $value ) {

			$us_id = $value->get('us_id') ;
			$phase_id = ( $value->has('phase_id') ) ? $value->get('phase_id') : 'NA' ;

			$content = '<small><span>'.$value->get('identification').'</span></small>';
			$datatdebbas_aaaa = $value->has('datatdebbas_aaaa') ? $value->get('datatdebbas_aaaa') : '2015' ;
			// Complete avec des 0 devant la date si < 1000
			$datatdebbas_aaaa = str_pad($datatdebbas_aaaa, 4, '0', STR_PAD_LEFT );
			$datatdebbas_mm = $value->has('datatdebbas_mm') ? $value->get('datatdebbas_mm') : '00' ;
			$datatdebbas_jj = $value->has('datatdebbas_jj') ? $value->get('datatdebbas_jj') : '00' ;
			$datatdebbas_ad = $value->has('datatdebbas_ad') ? $value->get('datatdebbas_ad') : '' ;
			$avant_jc = (!empty($datatdebbas_ad) ) ? '-' : '';
			$datatdebbas = (!empty($datatdebbas_aaaa) ) ? $avant_jc.$datatdebbas_aaaa.'-01-01' : '2015-01-01' ;
			$tpq = $value->has('taq_aaaa') ? $value->get('taq_aaaa') : '0000' ;
			$us_date_deb = ( $tpq != '0000-00-00' ) ? $tpq : $datatdebbas ;
			// Si il n'y a pas de date de fin, la representation est un point
			$inter_date_fin = ( $value->has('taq_aaaa') && $value->get('taq_aaaa') != '0000' ) ? $value->get('taq_aaaa') : '' ;


			$us_phasechronos = array (
        'id' => $key, 
        'group' => hash('crc32', $phase_id ) ,
        'content' => $content,
        'value' => $phase_id,
        'start' => $us_date_deb
			);
			// Si il n'y a pas de date de fin, la representation est un point
			if(!empty($inter_date_fin) ) { $us_phasechronos['end']= $inter_date_fin ; }
			else { $us_phasechronos['type']= 'point'; }
			$phasechronologiques[] = $us_phasechronos ;
		}

		/* Definition des borne max et min du planning */
		$us_first_date = $us_phasechrono->getFirstUsDateDeb()->where('sitefouille_id = "'.$sitefouille_id.'" ' )->toRow() ;
		$us_first_date_debut = (!empty($us_first_date['debut']) ) ? $us_first_date['debut'] : '2015-01-01' ;
		$this->_view->set('us_first_date', explode('-','2015-01-01' ));

		$us_last_date = $us_phasechrono->getFirstUsDateDeb()->where('sitefouille_id = "'.$sitefouille_id.'" ' )->toRow() ;
		$us_last_date_fin = (!empty($us_last_date['fin']) ) ? $us_last_date['fin'] : '2015-01-01' ;
		$this->_view->set('us_last_date', explode('-', $us_last_date_fin ));

		$this->_view->set('us_phasechronos', $phasechronologiques);

		$this->_view->setFile('View/analyser/us/us_phases.html');

	}

	public function actionStratifiant() {

		$base_url = $this->getRequest()->getBaseUrl() ;
		// controle que le site de fouille est selectionn�
		$session = $this->getSession();
		if ( empty($sitefouille_id) ) {
			$sitefouille_id = $session->get('sitefouille_id')  ;
		} else {
			$this->getSession()->set('flash_title', '');
			$this->getSession()->set('flash_status', 'alert');
			$this->getSession()->set('flash_message', 'Veuillez choisir un site de fouille');
			$this->forward('Dashboard_Sitefouille', 'List');
		}

		$sitefouille = new Model_Sitefouille(array($sitefouille_id));
		$sitefouille_nomabrege = $sitefouille->get('nomabrege');
		$name_file = 'liste_US_'.$sitefouille_nomabrege ;

		/**
		 * R�cup�ration des US ayant un lien aontero post�rieur et synchrone
		 */
		$us = new Model_Us();
		$liste_us = $us->getUsDetail() ;

		$liste_phases_us = $us->getUsPhases() ;
		$phases_us = array() ;
		foreach($liste_phases_us as $phase_us_value){
			$phase_identification = $phase_us_value->get('phase_identification') ;
			$phase_ordre = $phase_us_value->get('phase_ordre') ;
			$taq_aaaa = $phase_us_value->get('taq_aaaa') ;
			$tpq_aaaa = $phase_us_value->get('tpq_aaaa') ;
			$us_identification = $phase_us_value->get('us_identification') ;

			$phases_us[$phase_identification]['phase_identification'] = $phase_identification ;
			$phases_us[$phase_identification]['taq_aaaa'] = $taq_aaaa ;
			$phases_us[$phase_identification]['tpq_aaaa'] = $tpq_aaaa ;
			$phases_us[$phase_identification]['phase_ordre'] = $phase_ordre ; // d�fini � l'avance l'enregistrement
			$phases_us[$phase_identification]['us'][$us_identification] = $phase_us_value->toArray() ;
			$phases_us[$phase_identification]['us'][$us_identification]['phase_ordre'] = $phase_ordre ;
		}

		$network_anteroposterite = $us->getSuperpositionUs() ;
		// $this->_view->set('network_anteroposterite', $network_anteroposterite );

		$network_synchronisation = $us->getSynchronisationUs() ;
		// $this->_view->set('network_synchronisation', $network_synchronisation );

		// Appel au stratifiant
		$stratifiant = new Plugin_Stratifiant() ;

		$sdf_stratifiant = $stratifiant->StratiDataSources($sitefouille, $liste_us, $network_anteroposterite, $network_synchronisation, $phases_us ) ;
		var_dump($sdf_stratifiant);
		$dirname = $sdf_stratifiant['info']['stratifiant_path'] ;
		$export_file_zip = $sdf_stratifiant['info']['datasource_path'].'.zip' ;

		$export_files = $sdf_stratifiant['source_files'] ;

		$sdf_stratifiant_zip = $this->_create_zip_file($dirname,$export_files,$export_file_zip);

		header("Content-Type: application/octet-stream");
		header("Content-Type: text/csv; charset=utf-8");
		header('Content-disposition: attachment; filename="'.$dirname.$export_file_zip.'" ');
		header('Location: '.$base_url.'/'.$dirname.$export_file_zip);


		// $this->_response->download($sdf_stratifiant_zip);
		 
		return ;

	}

	private function _create_zip_file($dir,$name_file,$name_zip_file){
		// compression du fichier export
		$zip = new ZipArchive();
		if ($zip->open($dir.$name_zip_file) == TRUE) {
			if($zip->open($dir.$name_zip_file, ZipArchive::CREATE) == TRUE) {
				if(is_array($name_file) ){
					/*
					print '<pre>' ;
					print_r($name_file) ;
					print '</pre>' ;
					*/

					foreach($name_file as $file_key => $file_value ){
						$name = $file_value['name'] ;
						$path = $file_value['path'] ;
						$zip->addFromString($name, file_get_contents($path));
					}
				} else {
					$zip->addFromString($name_file, file_get_contents($dir.$name_file));
				}


			}
			$zip->close();
		}
		if (file_exists($dir.$name_zip_file) && filesize($dir.$name_zip_file) != 0 ) {
			return;
		} else {
			$this->_create_zip_file($dir,$name_file,$name_zip_file) ;
		}
	}

}
