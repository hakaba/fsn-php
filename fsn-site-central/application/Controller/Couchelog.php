<?php

class Controller_Couchelog extends Yab_Controller_Action {

	public function actionIndex() {

		$modelcouchelog = new Model_Couchelog();

		$session = $this->getSession();
		if($session->has('log_id')) {
			$couches = $modelcouchelog -> getListCouches($session->get('log_id'));
		} else {
			$couches = $modelcouchelog->fetchAll();
		}

		$this->_view->set('couches', $couches);
	}

	public function actionAdd() {

		$mode = self::ACTION_MODE_CREATE;

		$modelcouchelog = new Model_Couchelog($this->_request->getParam('log_id'));
		//$modelcouchelog ->set('uslog_id', $this->_request->getParam('log_id'));
		$form_add = new Form_Couchelog($modelcouchelog, $mode);
		
		/*
		$form_add->setElement('numero_ident', array(
				'type' => 'text',
				'id' => 'numero_ident',
				'label' => $form_add->getElement('identification')->get('label'),
				'value' => isset($_POST['numero_ident']) ? $_POST['numero_ident'] : $form_add->getElement('identification')->get('numero'),
				'validators' => array('NotEmpty','Int'),
				'errors' => array('NotEmpty' => array('Le champ identification ne doit pas être vide'),
						'Int' => array('Ce champs doit être un entier')),
		));
		*/

		//echo "isValid = ".$form_add->isValid();
		//echo "isSubmitted = ".$form_add->isSubmitted();
			if(!empty($_POST) && count($_POST)>4) {
				$formvalues = $form_add->getValues();
				//print_r($formvalues);
				//unset($formvalues['numero_ident']);
				// Ajout du GUUID
				$generateGuuid = new Plugin_Guuid();
				$guuid = $generateGuuid -> GetUUID();
				//$form->setElement('id', array('type'=>'text', 'value' => $guuid ) ) ;
				$formvalues['id'] = $guuid;

				$formvalues = $form_add -> setNullValues($formvalues);
				$modelcouchelog->populate($formvalues)->save();

				$this->getSession()->set('flash_title', '');
				$this->getSession()->set('flash_status', 'info');
				$this->getSession()->set('flash_message', 'couche has been added');

				//$session = $this->getSession();
				//$this->forward('Log', 'index');
			}

		$this->_view->set('form_add', $form_add);

	}

	public function actionEdit() {

		$mode = self::ACTION_MODE_UPDATE;
		
		$couche = new Model_Couchelog($this->_request->getParams());
		$form = new Form_Couchelog($couche, $mode);

		if(!empty($_POST) && count($_POST)>3) {

			$couche -> populate($form->getValues())->save();
			$this->getSession()->set('flash_title', '');
			$this->getSession()->set('flash_status', 'info');
			$this->getSession()->set('flash_message', 'couche has been edited');
			//$this->forward('Couchelog', 'index');

		}

		$this->_view->set('form_edit', $form);

	}

	public function actionDelete() {

		$modelcouchelog = new Model_Couchelog($this->_request->getParams());
		$form = new Form_Couchelog($modelcouchelog);
		$formvalues = $form->getValues();

		$formvalues ['id'] = $log->get ('id');//echo $formvalues['id'];

		$message = '';

		try {
			$modelcouchelog->delete();
				
			// historisation début
			$historisation = new Plugin_Historique();
			$formvalues = $form->getTypedValues($formvalues);
			$historisation->addhistory('Couchelog', self::MODE_DELETE, $formvalues);
			// historisation fin
			$message = 'Couche supprimé';
		} catch (Exception $e) {
			$message = 'Couche n\'a pas pu etre supprimé';
		}

		$this->getSession()->set('flash_title', '');
		$this->getSession()->set('flash_status', 'info');
		$this->getSession()->set('flash_message', $message);

		//$this->forward('Log', 'index');

	}

}