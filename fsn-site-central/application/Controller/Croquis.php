<?php

class Controller_Croquis extends Yab_Controller_Action {

	public function actionIndex() {

		$croquis = new Model_Croquis();

		$croquis = $croquis->fetchAll();

		$this->_view->set('croquis', $croquis);
	}

	public function actionAdd() {

		$croquis = new Model_Croquis();

		$form = new Form_Croquis($croquis);

		if($form->isSubmitted() && $form->isValid()) {

			$croquis->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'croquis as been added');

			$this->forward('Croquis', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$croquis = new Model_Croquis($this->_request->getParams());

		$form = new Form_Croquis($croquis);

		if($form->isSubmitted() && $form->isValid()) {

			$croquis->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'croquis as been edited');

			$this->forward('Croquis', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$croquis = new Model_Croquis($this->_request->getParams());

		$croquis->delete();

		$this->getSession()->set('flash', 'croquis as been deleted');

		$this->forward('Croquis', 'index');

	}

}