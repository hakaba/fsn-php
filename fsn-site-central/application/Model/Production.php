<?php

class Model_Production extends Yab_Db_Table {

	protected $_name = 'production';

	public function getElementpoteries() {

		return $this->getTable('Model_Elementpoterie')->search(array('production_id' => $this->get('id')));

	}

}