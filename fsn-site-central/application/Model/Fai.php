<?php

class Model_Fai extends Yab_Db_Table {

	protected $_name = 'fai';

	public function getSitefouille() {

		return new Model_Sitefouille($this->get('sitefouille_id'));

	}

	public function getTypefai() {

		return new Model_Typefai($this->get('typefai_id'));

	}

	public function getFaiUses() {

		return $this->getTable('Model_Fai_Us')->search(array('fai_id' => $this->get('id')));

	}

	public function getInterfaits() {

		return $this->getTable('Model_Interfait')->search(array('fai_id' => $this->get('id')));

	}
	
	public function getAllFaiFromSf($sf_id){
		$db = $this->getTable()->getAdapter();
		return $db->prepare("SELECT * FROM fai WHERE fai.sitefouille_id = '".$sf_id."' ORDER BY identification;");
	}
	
	public function getBySitefouilleAll($sitefouille_id = null) {
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();
		
		$where = (!is_null($sitefouille_id) && !empty($sitefouille_id)) ? " AND sitefouille_id = '$sitefouille_id' " : "" ;
			 
		$sql = "
		SELECT id, identification
		FROM fai
		WHERE 1 $where
		ORDER BY identification
		" ;
		 
		return $db->prepare ( $sql ) ;
	}
	
	public function getFaiDetail(){
    
    	$db = $this->getTable()->getAdapter();

		$loader = Yab_Loader::getInstance();
		$session = $loader->getSession();
    	$sitefouille_id = $session->get('sitefouille_id');
		$where = (!empty($sitefouille_id)) ? " AND fai.sitefouille_id = '$sitefouille_id' " : "" ;
		$requeteur = "requete_" . "fai";
		$requeteurSession = $session->has($requeteur) ? $session->get($requeteur) : null;
		$where .= (!is_null($requeteurSession) && !empty($requeteurSession)) ? " AND fai.id IN (" . $requeteurSession . ") " : "";
		$session[$requeteur] = ''; // la requête étant consommée, on l'efface
		
   		$sql = "SELECT
                fai.sitefouille_id,
                sdf.nom AS sitefouille_nom,
                sdf.nomabrege AS sitefouille_nomabrege,
                fai.id AS fai_id, 
                fai.identification AS fai_identification, 
                fai.description,
                fai.typefai_id,
                categ.categorie_value as categorie_value,
                fai.fin_fourc_hte_estim,
                fai.fin_fourc_bas_estim,
                fai.deb_fourc_hte_estim,
                fai.deb_fourc_bas_estim, 
				fai.fin_fourc_hte_certain,
                fai.fin_fourc_bas_certain,
                fai.deb_fourc_hte_certain,
                fai.deb_fourc_bas_certain
            FROM fai
            INNER JOIN sitefouille sdf ON sdf.id = fai.sitefouille_id
            LEFT JOIN fsn_categorie categ ON categ.id = fai.typefai_id
    		WHERE 1 $where
            ORDER BY fai.identification 
    		";
        
    	$result = $db->prepare($sql);
    
    //if (!empty($sitefouille_id) ) { $result = $result->where('fai.sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
    
    	return $result ;
  
  }
  
	public function getSitefouilleByIdSite($site) {

		$db = $this->getTable()->getAdapter();
		$nom_complet = $db->prepare('SELECT *, id, CONCAT_WS(" - ", nom, nomabrege) AS nom_nomabrege
      								FROM sitefouille where id="'.$site.'"');
		return $nom_complet;

	}
	
	public function getEntiteOrganisme(){
		
		$db = $this->getTable()->getAdapter();
		$sql = "SELECT orga.nomabrege nomabrege_organisme
				FROM fsn_entiteadmin enti
				INNER JOIN fsn_organisme orga on orga.id = enti.organisme_id
			";
				
		$result = $db->prepare($sql);
		return $result ;
	}
	
	// Récuperer les categories type pour fai
	public function getOnlyFaiType(){
		
		$db = $this->getTable()->getAdapter();
		$sql = "SELECT *
				FROM fsn_categorie
				WHERE categorie_type in (SELECT categorie_key FROM fsn_categorie WHERE categorie_type = 'fai-typefai_id' ) and visible = 1
			";
				
		$result = $db->prepare($sql);
		return $result ;
	}
}