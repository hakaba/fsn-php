<?php

class Model_Nature extends Yab_Db_Table {

	protected $_name = 'nature';

	public function getOastatut() {

		return new Model_Oastatut($this->get('oastatut_id'));

	}

	public function getOas() {

		return $this->getTable('Model_Oa')->search(array('nature_id' => $this->get('id')));

	}

}