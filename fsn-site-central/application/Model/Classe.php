<?php

class Model_Classe extends Yab_Db_Table {

	protected $_name = 'classe';

	public function getClasseFonctions() {

		return $this->getTable('Model_Classe_Fonction')->search(array('classe_id' => $this->get('id')));

	}

	public function getClasseNaturematieres() {

		return $this->getTable('Model_Classe_Naturematiere')->search(array('classe_id' => $this->get('id')));

	}

	public function getElementrecueillis() {

		return $this->getTable('Model_Elementrecueilli')->search(array('classe_id' => $this->get('id')));

	}

}