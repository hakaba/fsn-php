<?php

class Model_Croquis extends Yab_Db_Table {

	protected $_name = 'croquis';

	public function getCroquisImports() {

		return $this->getTable('Model_Croquis_Import')->search(array('croquis_id' => $this->get('id')));

	}

	public function getReleves() {

		return $this->getTable('Model_Releve')->search(array('croquis_id' => $this->get('id')));

	}

	public function getUses() {

		return $this->getTable('Model_Us')->search(array('croquis_id' => $this->get('id')));

	}

}