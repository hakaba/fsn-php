<?php

class Model_Phasechrono extends Yab_Db_Table {

	protected $_name = 'phasechrono';

	public function getEpisodeurbain() {

		return new Model_Episodeurbain($this->get('episodeurbain_id'));

	}

	public function getSitefouille() {

		return new Model_Sitefouille($this->get('sitefouille_id'));

	}

	public function getVisiblePhases($sf_id = null){
		/* jfb 2016-06 mantis 318 début
		if(!$sf_id)
			$sf_id = Yab_Loader::getInstance()->getSession()->get('sitefouille_id');
		$db = $this->getTable()->getAdapter();
		return $db->prepare("SELECT * FROM phasechrono WHERE sitefouille_id = '".$sf_id."'");
		 * jfb 2016-06 mantis 318 fin */
		// jfb 2016-06 mantis 318 début
		$loader = Yab_Loader::getInstance();
		$session = $loader->getSession();
		if (!$sf_id) {
			$sf_id = $session->has('sitefouille_id') ? $session->get('sitefouille_id') : null ;
		}
		$db = $this->getTable()->getAdapter();
		$where = (!is_null($sf_id) && !empty($sf_id)) ? " AND sitefouille_id = '$sf_id' " : "" ;

		$requeteur = "requete_" . "phasechrono";
		$requeteurSession = $session->has($requeteur) ? $session->get($requeteur) : null;
		$where .= (!is_null($requeteurSession) && !empty($requeteurSession)) ? " AND id IN (" . $requeteurSession . ") " : "";
		$session[$requeteur] = ''; // la requête étant consommée, on l'efface
		
		$sql = "SELECT * FROM phasechrono WHERE 1 $where" ;
		return $db->prepare($sql);
		// jfb 2016-06 mantis 318 fin
	}
	
	public function getSitefouilleByIdSite($site) {

		$db = $this->getTable()->getAdapter();
		$nom_complet = $db->prepare('SELECT *, id, CONCAT_WS(" - ", nom, nomabrege) AS nom_nomabrege
      								FROM sitefouille where id="'.$site.'"');
		return $nom_complet;

	}

}