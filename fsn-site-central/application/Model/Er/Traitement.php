<?php

class Model_Er_Traitement extends Yab_Db_Table {

	protected $_name = 'er_traitement';

	public function getTraitement() {

		return new Model_Traitement($this->get('traitement_id'));

	}

	public function getElementrecueilli() {

		return new Model_Elementrecueilli($this->get('elementrecueilli_id'));

	}

}