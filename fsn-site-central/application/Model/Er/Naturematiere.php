<?php

class Model_Er_Naturematiere extends Yab_Db_Table {

	protected $_name = 'er_naturematiere';

	public function getEr() {

		return new Model_Er($this->get('er_id'));

	}

	public function getNaturematiere() {

		return new Model_Naturematiere($this->get('naturematiere_id'));

	}

}