<?php

class Model_Er_Doc extends Yab_Db_Table {

	protected $_name = 'er_doc';

	public function getEr() {

		return new Model_Er($this->get('er_id'));

	}

	public function getDoc() {

		return new Model_Doc($this->get('doc_id'));

	}

}