<?php
class Model_Fai_Us extends Yab_Db_Table {

	protected $_name = 'fai_us';
	
	 public function getFai_us() {

    $db = $this->getTable()->getAdapter();
    $loader = Yab_Loader::getInstance();
    
    $sql = 'SELECT 
		faius.fai_id,
		faius.us_id,
        us.identification AS us_identification,
        faius.incertitude
		FROM fai_us faius
		INNER JOIN us ON us.id = faius.us_id
		ORDER BY us_identification
    ';
        
    $result = $db->prepare($sql);
    
    return $result ;  

	}
	
	public function deleteRelations($fai_id){
        $this->delete( "fai_id = '".$fai_id."'" );
    }
	
}