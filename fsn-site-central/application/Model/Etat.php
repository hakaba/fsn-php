<?php

class Model_Etat extends Yab_Db_Table {

	protected $_name = 'etat';

	public function getElementrecueillis() {

		return $this->getTable('Model_Elementrecueilli')->search(array('etat_id' => $this->get('id')));

	}

	public function getTraitements() {

		return $this->getTable('Model_Traitement')->search(array('etat_id' => $this->get('id')));

	}

}