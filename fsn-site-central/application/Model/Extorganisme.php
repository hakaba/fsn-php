<?php

class Model_Extorganisme extends Yab_Db_Table {

	protected $_name = 'extorganisme';

	public function getIntervenants() {

		return $this->getTable('Model_Intervenant')->search(array('extorganisme_id' => $this->get('id')));

	}

}