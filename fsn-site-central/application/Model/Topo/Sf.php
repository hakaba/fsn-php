<?php

class Model_Topo_Sf extends Yab_Db_Table {

	protected $_name = 'topo_sf';

	public function getSitefouille() {

		return new Model_Sitefouille($this->get('sitefouille_id'));

	}

	public function getTopo() {

		return new Model_Topo($this->get('topo_id'));

	}

}