<?php

class Model_Interpretation extends Yab_Db_Table {

	protected $_name = 'interpretation';

	public function getUses() {

		return $this->getTable('Model_Us')->search(array('interpretation_id' => $this->get('id')));

	}

}