<?php

class Model_Typeunitesondage extends Yab_Db_Table {

	protected $_name = 'type_unitesondage';

	
	public function getUnitesondages() {

		return $this->getTable('Model_Unitesondage')->search(array('type' => $this->get('id')));

	}

}