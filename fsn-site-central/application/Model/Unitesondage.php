<?php

class Model_Unitesondage extends Yab_Db_Table {
	
	protected $_name = 'unitesondage';
	
	
	public function getType($us_id) {
		$db = $this->getTable()->getAdapter();
		$sql = "SELECT `type` FROM unitesondage WHERE id = '".$us_id."'";
		
		$result = $db->prepare($sql)->toRow() ;
		return $result == ""? "tranchee": $result ;
	}
	
	public function getListunitesondage($sf_id) {
		$db = $this->getTable()->getAdapter();
		// jfb 2016-06 mantis 318
		$loader = Yab_Loader::getInstance();
		$session = $loader->getSession();
		if (!$sf_id) {
			$sf_id = $session->has('sitefouille_id') ? $session->get('sitefouille_id') : null ;
		}
		$where = (!is_null($sf_id) && !empty($sf_id)) ? " AND unitesondage.sitefouille_id = '$sf_id' " : "" ;
	
		$requeteur = "requete_" . "unitesondage";
		$requeteurSession = $session->has($requeteur) ? $session->get($requeteur) : null;
		$where .= (!is_null($requeteurSession) && !empty($requeteurSession)) ? " AND unitesondage.id IN (" . $requeteurSession . ") " : "";
		$session[$requeteur] = ''; // la requête étant consommée, on l'efface
	
		// jfb 2016-06 mantis 318 Correction de la requête ci-après : WHERE unitesondage.sitefouille_id = "'.$sf_id.'"
		$sql = "SELECT
		unitesondage.id AS us_id,
		unitesondage.identification AS identification,
		unitesondage.nom AS us_nom,
		unitesondage.type,
		unitesondage.date_debut,
		unitesondage.date_fin,
		unitesondage.description
		FROM unitesondage
		WHERE 1 $where
		ORDER BY unitesondage.nom
		";
		return $db->prepare($sql);
	}
	
	public function getUsondageByIdentification($ident){
		$db = $this->getTable()->getAdapter();
		return $db->prepare("SELECT * FROM unitesondage WHERE id = '".$ident."'");
	}
	
	public function getListFenetres($id_tranchee) {
		$db = $this->getTable()->getAdapter();
		return $db->prepare("SELECT * FROM unitesondage WHERE numero_tranchee = '".$id_tranchee."'");
	}
	
	public function getListContours($id_usondage) {
		$db = $this->getTable()->getAdapter();
		return $db->prepare("SELECT * FROM contour_unite_sondage WHERE unitesondage_id = '".$id_usondage."'");
	}
	
	public function getListTroncons($id_usondage) {
		$db = $this->getTable()->getAdapter();
		return $db->prepare("SELECT * FROM troncon WHERE unitesondage_id = '".$id_usondage."'");
	}
	
	public function getNextSequenceUsondageId($sitefouille_id) {
		$db = $this->getTable()->getAdapter();
		$sql = 'SELECT MAX(CAST(compteur AS UNSIGNED)) + 1 AS next_compteur
                FROM (
                SELECT
                    sitefouille.id AS sitefouille_id,
                    sitefouille.nom,
                    sitefouille.nomabrege,
                    SUBSTRING_INDEX(unitesondage.identification, ".", 1) AS prefixe,
                    SUBSTRING_INDEX(unitesondage.identification, ".", -1) AS compteur,
                    unitesondage.id AS usondage_id,
                    unitesondage.identification
                    FROM unitesondage
                    INNER JOIN sitefouille ON sitefouille.id = unitesondage.sitefouille_id
                    WHERE sitefouille_id = "'.$sitefouille_id.'"
                ) AS T';
	
		$result = $db->prepare($sql)->toRow() ;
		return $result == ""? 1: $result ;
	}
	
}