<?php

class Model_Elementrecueilli extends Yab_Db_Table {

	protected $_name = 'elementrecueilli';

	public function getByConteneur($id) {
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();
	
		$sql = "SELECT
					id,
					numero,
					description
				FROM elementrecueilli
				where conteneur_id='".$id."'";
	
		$result = $db->prepare ( $sql ) ;
	
		return $result->toArray() ;
	}
	
	public function getUs() {

		return new Model_Us($this->get('us_id'));

	}

	public function getIntervenant() {

		return new Model_Intervenant($this->get('intervenant_id'));

	}

	public function getElementpoterie() {

		return new Model_Elementpoterie($this->get('elementpoterie_id'));

	}

	public function getStatut() {

		return new Model_Statut($this->get('statut_id'));

	}

	public function getEmplacement() {

		return new Model_Emplacement($this->get('emplacement_id'));

	}

	public function getFonction() {

		return new Model_Fonction($this->get('fonction_id'));

	}

	public function getClasse() {

		return new Model_Classe($this->get('classe_id'));

	}

	public function getEtat() {

		return new Model_Etat($this->get('etat_id'));

	}

	public function getErTraitements() {
		
		return $this->getTable('Model_Er_Traitement')->search(array('elementrecueilli_id' => $this->get('id')));

	}
	

    public function getErFonctionCounts() {

		$db = $this->getTable()->getAdapter();
    $loader = Yab_Loader::getInstance();
    $session = $loader->getSession();
    
    $sql = 'SELECT 
      	CASE 
      		WHEN phch.id IS NULL THEN "0"
      		ELSE phch.id 
      	END
      	AS phasechrono_id,
      	CASE
      		WHEN phch.identification IS NULL THEN "NA"
      		ELSE phch.identification 
      	END AS phasechrono_identification ,
      	er.fonction_id,
      	fn.fonction,
        count(*) AS nb_er
      FROM elementrecueilli er
      INNER JOIN us ON us.id = er.us_id
      -- LEFT JOIN phase_us pus ON pus.us_id = us.id
      LEFT JOIN phasechrono phch ON phch.id = us.phase_id      
      INNER JOIN sitefouille sdf ON sdf.id = us.sitefouille_id
      LEFT JOIN fsn_categorie fcf ON fcf.id = er.fonction_id AND fcf.categorie_type = "er_fonction"
      INNER JOIN fonction fn ON fn.id = er.fonction_id       
      GROUP BY 
        phch.id,
      	phch.identification,
        er.fonction_id,
      	fn.fonction
      ORDER BY phch.datatdebbas_aaaa, fn.fonction
    ';
        
    $result = $db->prepare($sql);
    
    $sitefouille_id = $session->get('sitefouille_id');
    if (!empty($sitefouille_id) ) { $result = $result->where('us.sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
    
    return $result ;
      
	}
	
	public function getErClasseCounts() {

		$db = $this->getTable()->getAdapter();
    $loader = Yab_Loader::getInstance();
    $session = $loader->getSession();
    
    $sql = 'SELECT 
      	CASE 
      		WHEN phch.id IS NULL THEN "0"
      		ELSE phch.id 
      	END
      	AS phasechrono_id,
      	CASE
      		WHEN phch.identification IS NULL THEN "NA"
      		ELSE phch.identification 
      	END AS phasechrono_identification ,
      	er.classe_id,
      	cl.classe,
        count(*) AS nb_er
      FROM elementrecueilli er
      INNER JOIN us ON us.id = er.us_id
      -- LEFT JOIN phase_us pus ON pus.us_id = us.id
      LEFT JOIN phasechrono phch ON phch.id = us.phase_id      
      INNER JOIN sitefouille sdf ON sdf.id = us.sitefouille_id
      -- LEFT JOIN fsn_categorie fcc ON fcc.id = er.classe_id AND fcc.categorie_type = "er_classe"
      INNER JOIN classe cl ON cl.id = er.classe_id       
      GROUP BY 
        phch.id,
      	phch.identification,
        er.classe_id,
      	cl.classe
      ORDER BY phch.datatdebbas_aaaa, cl.classe
    ';
        
    $result = $db->prepare($sql);
    
    $sitefouille_id = $session->get('sitefouille_id');
    if (!empty($sitefouille_id) ) { $result = $result->where('us.sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
    
    return $result ;
      
	}

	public function getErStatutCounts() {

		$db = $this->getTable()->getAdapter();
    $loader = Yab_Loader::getInstance();
    $session = $loader->getSession();
    
    $sql = 'SELECT 
      	er.statut_id,
    		CASE 
    			WHEN st.statut IS NULL
    			THEN "NA"
    			ELSE st.statut
    		END AS statut,
      	-- fcs.categorie_value as statut,
        -- st.statut,
        count(*) AS nb_er
      FROM elementrecueilli er
      INNER JOIN us ON us.id = er.us_id
      INNER JOIN sitefouille sdf ON sdf.id = us.sitefouille_id
   	  -- INNER JOIN fsn_categorie fcs ON fcs.id = er.statut_id AND fcs.categorie_type = "elementrecueilli_statut"
      -- INNER JOIN fsn_categorie fcs ON fcs.id = er.statut_id AND fcs.categorie_type = "elementrecueilli_statut"
      INNER JOIN statut st ON st.id = er.statut_id 
      GROUP BY 
        er.statut_id,
      	-- fcs.categorie_value
        st.statut        
      ORDER BY
        st.ordre 
    ';
        
    $result = $db->prepare($sql);
    
    $sitefouille_id = $session->get('sitefouille_id');
    if (!empty($sitefouille_id) ) { $result = $result->where('us.sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
    
    return $result ;
      
	}

	public function getErEtatCounts() {

		$db = $this->getTable()->getAdapter();
    $loader = Yab_Loader::getInstance();
    $session = $loader->getSession();
    
    $sql = 'SELECT
    		CASE 
    			WHEN etat.etat IS NULL
    			THEN "NA"
    			ELSE etat.etat
    		END AS etat,
      	er.etat_id,
        COUNT(*) AS nb_er
      FROM elementrecueilli er
      INNER JOIN us ON us.id = er.us_id
      INNER JOIN sitefouille sdf ON sdf.id = us.sitefouille_id
      LEFT JOIN fsn_categorie fce ON fce.id = er.etat_id AND fce.categorie_type = "er_etat"
      INNER JOIN etat ON etat.id = er.etat_id  
      GROUP BY 
        er.etat_id,
      	etat.etat
    	ORDER BY etat.ordre
    ';
        
    $result = $db->prepare($sql);
    
    $sitefouille_id = $session->get('sitefouille_id');
    if (!empty($sitefouille_id) ) { $result = $result->where('us.sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
    
    return $result ;
      
	}
  
	public function listSdfEr() {

		$db = $this->getTable()->getAdapter();
    $loader = Yab_Loader::getInstance();
    $session = $loader->getSession();
    
    $sql = 'SELECT 
      	sdf.nom,
      	sdf.nomabrege,
      	us.identification,
      	er.us_id, 
      	er.id AS er_id, 
      	er.intervenant_id, 
      	er.elementisole, 
      	er.numero, 
      	er.description, 
      	er.intact, 
      	er.datedecouverte,
      	er.elementpoterie_id, 
      	er.precisionfonction, 
      	er.statut_id, 
      	er.quantite, 
      	er.poids, 
      	er.longueur, 
      	er.largeur, 
      	er.hauteur, 
      	er.diametre, 
      	er.incertitude, 
      	er.emplacement_id, 
      	er.fonction_id, 
      	er.classe_id, 
      	er.etat_id
      FROM elementrecueilli er
      INNER JOIN us ON us.id = er.us_id
      INNER JOIN sitefouille sdf ON sdf.id = us.sitefouille_id
      ORDER BY us.identification, er.numero 
    ';
        
    $result = $db->prepare($sql);
    
    $sitefouille_id = $session->get('sitefouille_id');
    if (!empty($sitefouille_id) ) { $result = $result->where('us.sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
    
    return $result ;  

	}

	public function getErDetail() {
    
		$db = $this->getTable()->getAdapter();
    $loader = Yab_Loader::getInstance();
    $session = $loader->getSession();
    
    $sql = 'SELECT 
      	sdf.nom,
      	sdf.nomabrege,      	
      	er.us_id, 
      	us.identification AS us_identification,
      	er.id AS er_id, 
      	er.numero,
      	er.intervenant_id, 
      	er.elementisole,       	 
      	er.description, 
      	er.intact, 
      	er.datedecouverte,
      	er.elementpoterie_id, 
      	er.precisionfonction, 
      	er.statut_id,
      	-- st.statut, 
      	CASE 
  	  		WHEN st.statut IS NULL
  	  		THEN "NA"
  	  		ELSE st.statut
    		END AS statut,
      	-- er.quantite, 
      	CASE 
      		WHEN er.quantite = 0 OR er.quantite IS NULL
      		THEN 1
      		ELSE er.quantite
  		  END AS quantite,      	
      	er.poids, 
      	er.longueur, 
      	er.largeur, 
      	er.hauteur, 
      	er.diametre, 
      	er.incertitude, 
      	er.emplacement_id,
      	er.fonction_id, 
      	-- fn.fonction,
      	CASE 
  	  		WHEN fn.fonction IS NULL
  	  		THEN "NA"
  	  		ELSE fn.fonction
    		END AS fonction,
      	-- fcs.categorie_key,
      	-- fcs.categorie_value,
      	er.classe_id,
      	-- cla.classe, 
      	CASE 
  	  		WHEN cla.classe IS NULL
  	  		THEN "NA"
  	  		ELSE cla.classe
    		END AS classe,
      	-- fcc.categorie_key,
      	-- fcc.categorie_value,
      	er.etat_id,
      	-- fce.categorie_key,
      	-- fce.categorie_value,
      	-- etat.etat
      	CASE 
  	  		WHEN etat.etat IS NULL
  	  		THEN "NA"
  	  		ELSE etat.etat
    		END AS etat
      FROM elementrecueilli er
      INNER JOIN us ON us.id = er.us_id
      INNER JOIN sitefouille sdf ON sdf.id = us.sitefouille_id
      -- LEFT JOIN fsn_categorie fcs ON fcs.id = er.statut_id AND fcs.categorie_type = "er_statut"
      LEFT JOIN statut st ON st.id = er.statut_id       
      -- LEFT JOIN fsn_categorie fcf ON fcf.id = er.fonction_id AND fcf.categorie_type = "er_fonction"
      LEFT JOIN fonction fn ON fn.id = er.fonction_id       
      -- LEFT JOIN fsn_categorie fcc ON fcc.id = er.classe_id AND fcc.categorie_type = "er_classe"
      LEFT JOIN classe cla ON cla.id = er.classe_id       
      -- LEFT JOIN fsn_categorie fce ON fce.id = er.classe_id AND fce.categorie_type = "er_etat"
      LEFT JOIN etat ON etat.id = er.etat_id          
    ';
        
    $result = $db->prepare($sql);
    
    $sitefouille_id = $session->get('sitefouille_id');
    if (!empty($sitefouille_id) ) { $result = $result->where('us.sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
    
    return $result ;  

	}

	public function getErInfosExport() {
    
		$db = $this->getTable()->getAdapter();
        $loader = Yab_Loader::getInstance();
        $session = $loader->getSession();
    
        $sitefouille_id = $session->get('sitefouille_id');
        if (!empty($sitefouille_id) ) { 
          $sql_filter = "WHERE us.sitefouille_id = '".$sitefouille_id."' " ;  
        } else {
          $sql_filter = "" ;    
        }
            
        $sql = "SELECT 
                    sitefouille_nom,
                    sitefouille_nomabrege,
                    us_identification,
                    numero,
                    elementisole,
                    description, 
                    intact, 
                    datedecouverte, 
                    CONCAT_WS('-',datationdebreal_aaaa,datationdebreal_mm,datationdebreal_jj) AS datationdebreal,
                    datationdebreal_ad,
                    CONCAT_WS('-',datationfinutil_aaaa,datationfinutil_mm,datationfinutil_jj) AS datationfinutil,
                    datationfinutil_ad,
                    elementpoterie, 
                    precisionfonction, 
                    statut,
                    quantite,
                    poids, 
                    longueur, 
                    largeur, 
                    hauteur,
                    diametre,
                    incertitude,
                    emplacement_nom,
                    emplacement_type,
                    fonction,
                    classe, 
                    etat
                FROM (
                    SELECT 
                        -- sdf.sitefouille_id,
                        sdf.nom AS sitefouille_nom,
                        sdf.nomabrege AS sitefouille_nomabrege,
                        -- us.id AS us_id, 
                        us.identification AS us_identification,
                        -- er.id AS er_id, 
                        er.numero,
                        -- er.intervenant_id, 
                        er.elementisole,         
                        er.description, 
                        er.intact, 
                        er.datedecouverte,
                        CASE 
                            WHEN datationdebreal_aaaa = 0 OR datationdebreal_aaaa IS NULL
                            THEN '0000'
                            ELSE datationdebreal_aaaa
                        END AS datationdebreal_aaaa,
                        CASE 
                            WHEN datationdebreal_mm = 0 OR datationdebreal_mm IS NULL
                            THEN '00'
                            ELSE datationdebreal_mm
                        END AS datationdebreal_mm,
                        -- er.datationdebreal_jj,
                        CASE 
                            WHEN datationdebreal_jj = 0 OR datationdebreal_jj IS NULL
                            THEN '00'
                            ELSE datationdebreal_jj
                        END AS datationdebreal_jj,
                        er.datationdebreal_ad,
                        CASE 
                            WHEN datationfinutil_aaaa = 0 OR datationfinutil_aaaa IS NULL
                            THEN '0000'
                            ELSE datationfinutil_aaaa
                        END AS datationfinutil_aaaa,
                        -- er.datationfinutil_mm,
                        CASE 
                            WHEN datationfinutil_mm = 0 OR datationfinutil_mm IS NULL
                            THEN '00'
                            ELSE datationfinutil_mm
                        END AS datationfinutil_mm,
                        -- er.datationfinutil_jj,
                        CASE 
                            WHEN datationfinutil_jj = 0 OR datationfinutil_jj IS NULL
                            THEN '00'
                            ELSE datationfinutil_jj
                        END AS datationfinutil_jj,
                        er.datationfinutil_ad,
                        -- er.elementpoterie_id,
                        CASE
                            WHEN er.elementpoterie_id IS NULL THEN 0
                            ELSE 1 
                        END AS elementpoterie, 
                        er.precisionfonction, 
                        -- er.statut_id,
                        st.statut,
                        CASE
                            WHEN er.quantite IS NULL THEN 1
                            ELSE er.quantite 
                        END AS quantite,
                        er.poids, 
                        er.longueur, 
                        er.largeur, 
                        er.hauteur, 
                        er.diametre, 
                        er.incertitude, 
                        -- er.emplacement_id,
                        emp.nom AS emplacement_nom,
                        fcemp.categorie_value AS emplacement_type,
                        -- er.fonction_id, 
                        fn.fonction,
                        -- fcs.categorie_key,
                        -- fcs.categorie_value,
                        -- er.classe_id,
                        cla.classe, 
                        -- fcc.categorie_key,
                        -- fcc.categorie_value,
                        -- er.etat_id,
                        -- fce.categorie_key,
                        -- fce.categorie_value,
                        etat.etat
                    FROM elementrecueilli er
                    INNER JOIN us ON us.id = er.us_id
                    INNER JOIN sitefouille sdf ON sdf.id = us.sitefouille_id
                    -- LEFT JOIN fsn_categorie fcs ON fcs.id = er.statut_id AND fcs.categorie_type = 'er_statut'
                    INNER JOIN statut st ON st.id = er.statut_id       
                    -- LEFT JOIN fsn_categorie fcf ON fcf.id = er.fonction_id AND fcf.categorie_type = 'er_fonction'
                    INNER JOIN fonction fn ON fn.id = er.fonction_id       
                    -- LEFT JOIN fsn_categorie fcc ON fcc.id = er.classe_id AND fcc.categorie_type = 'er_classe'
                    LEFT JOIN classe cla ON cla.id = er.classe_id       
                    -- LEFT JOIN fsn_categorie fce ON fce.id = er.classe_id AND fce.categorie_type = 'er_etat'
                    LEFT JOIN etat ON etat.id = er.etat_id
                    LEFT JOIN emplacement emp ON emp.id = er.emplacement_id 
                    LEFT JOIN fsn_categorie fcemp ON fcemp.id = emp.type_id
                        ".$sql_filter."
                  ) T
                  ORDER BY
                    sitefouille_nom,
                    us_identification,
                    numero        
        ";
            
        $result = $db->prepare($sql);
        
        return $result ;  

	}

	public function getErNatureMatierePrincipaleCounts(){
		$db = $this->getTable()->getAdapter();
		$loader = Yab_Loader::getInstance();
		$session = $loader->getSession();
		
		$sql ='SELECT 
				CASE WHEN categorie_value IS NULL THEN "NA"
				ELSE categorie_value
				END AS naturematiere,
               
				COUNT(*) AS nb_er
				from elementrecueilli er
				INNER JOIN us ON us.id = er.us_id
      			INNER JOIN sitefouille sdf ON sdf.id = us.sitefouille_id
				RIGHT JOIN er_naturematiere nature ON nature.er_id = er.id AND nature.principal = 1
				INNER JOIN fsn_categorie fc ON fc.id = nature.naturematiere_id AND fc.categorie_type ="elementrecueilli_naturematiere"
				GROUP BY categorie_value';
		
		$result = $db->prepare($sql);
		$sitefouille_id = $session->get('sitefouille_id');
		if (!empty($sitefouille_id) ) 
		{ 
			$result = $result->where('us.sitefouille_id = "'.$sitefouille_id.'" ' );
		}
    
    	return $result ;
      
	}
	
	public function getErIsoleCounts(){
		$db = $this->getTable()->getAdapter();
		$loader = Yab_Loader::getInstance();
		$session = $loader->getSession();
		
		$sql="SELECT er.elementisole AS isole,
				COUNT(*) AS nb_er 
				FROM elementrecueilli er
				INNER JOIN us ON us.id = er.us_id
				INNER JOIN sitefouille sdf ON sdf.id = us.sitefouille_id
				GROUP BY er.elementisole
				";
		
		$result = $db->prepare($sql);
		$sitefouille_id = $session->get('sitefouille_id');
		if (!empty($sitefouille_id) )
		{
			$result = $result->where('us.sitefouille_id = "'.$sitefouille_id.'" ' );
		}
		
		return $result ;
	}
	public function getErIntactCounts(){
		$db = $this->getTable()->getAdapter();
		$loader = Yab_Loader::getInstance();
		$session = $loader->getSession();
		 
		$sql ='SELECT er.intact AS intact,
				COUNT(*) AS nb_er
				FROM elementrecueilli er
				INNER JOIN us ON us.id = er.us_id
				INNER JOIN sitefouille sdf ON sdf.id = us.sitefouille_id
				GROUP BY er.intact';
		 
		$result = $db->prepare($sql);
		 
		$sitefouille_id = $session->get('sitefouille_id');
		if (!empty($sitefouille_id) ) { $result = $result->where('us.sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
		 return $result;
	}
	
	public function getErDatationDebutCertain(){
		$db = $this->getTable()->getAdapter();
		$loader = Yab_Loader::getInstance();
		$session = $loader->getSession();
		
		$sql = 'SELECT 
				CASE
					WHEN er.datationdebreal_certain IS NULL THEN "NA"
					ELSE er.datationdebreal_certain
					END
					AS datation,
				COUNT(*) AS nb_er 
				FROM elementrecueilli er
				INNER JOIN us ON us.id = er.us_id
      			INNER JOIN sitefouille sdf ON sdf.id = us.sitefouille_id
				GROUP BY  er.datationdebreal_certain';
		$result = $db->prepare($sql);
				
		$sitefouille_id = $session->get('sitefouille_id');
		if(!empty($sitefouille_id)){
			$result = $result->where('us.sitefouille_id = "'.$sitefouille_id.'" ');
			
		}
		
		return $result;
		
	}
	
	public function getErDatationFinCertain(){
		$db = $this->getTable()->getAdapter();
		$loader = Yab_Loader::getInstance();
		$session = $loader->getSession();
		$sql = 'SELECT
				CASE
					WHEN er.datationfinutil_certain IS NULL THEN "NA"
					ELSE er.datationfinutil_certain
				END				
				AS datation, 
				COUNT(*) AS nb_er
				FROM elementrecueilli er
				INNER JOIN us ON us.id = er.us_id
      			INNER JOIN sitefouille sdf ON sdf.id = us.sitefouille_id
				GROUP BY  er.datationfinutil_certain';
		$result = $db->prepare($sql);
		
		$sitefouille_id = $session->get('sitefouille_id');
		if(!empty($sitefouille_id)){
			$result = $result->where('us.sitefouille_id = "'.$sitefouille_id.'" ');
		
		}
		return $result;
	}
	
	public function getErDatationDebutEstime(){
		$db = $this->getTable()->getAdapter();
		$loader = Yab_loader::getInstance();
		$session = $loader->getSession();
		$sql = 'SELECT
				CASE
				WHEN er.datationdebreal_estim IS NULL THEN "NA"
				ELSE er.datationdebreal_estim
				END 
				AS datation,
				COUNT(*) AS nb_er
				FROM elementrecueilli er
				INNER JOIN us ON us.id = er.us_id
      			INNER JOIN sitefouille sdf ON sdf.id = us.sitefouille_id
				GROUP BY  er.datationdebreal_estim';
		$result = $db->prepare($sql);
		
		$sitefouille_id = $session->get('sitefouille_id');
		if(!empty($sitefouille_id)){
			$result = $result->where('us.sitefouille_id = "'.$sitefouille_id.'" ');
		
		}
		return $result;
	}
	
	public function getErDatationFinEstime(){
		$db = $this->getTable()->getAdapter();
		$loader = Yab_loader::getInstance();
		$session = $loader->getSession();
		
		$sql = 'SELECT
				CASE
				WHEN er.datationfinutil_estim IS NULL THEN "NA"
				ELSE er.datationfinutil_estim
				END
				AS datation,
				COUNT(*) AS nb_er
				FROM elementrecueilli er
				INNER JOIN us ON us.id = er.us_id
      			INNER JOIN sitefouille sdf ON sdf.id = us.sitefouille_id
				GROUP BY  er.datationfinutil_estim';
		$result = $db->prepare($sql);
		
		$sitefouille_id = $session->get('sitefouille_id');
		if(!empty($sitefouille_id)){
			$result = $result->where('us.sitefouille_id = "'.$sitefouille_id.'" ');
		
		}
		return $result;
	}
	
	public function importFileElementrecueilli($importfile) {
  
    return true ;
    
  }
  
  	public function getTypesAssociations() {
  		$db = $this->getTable()->getAdapter();
  		$loader = Yab_Loader::getInstance();
  		$session = $loader->getSession();
  
  		$sql = "SELECT id, categorie_value
  				FROM fsn_categorie
  				WHERE categorie_object = 'association_er' AND categorie_type = 'elementrecueilli_association_type'
  				" ;
  
  		$result = $db->prepare($sql);
  
	  	return $result ;
  
  	}
  	
  	public function estAssocie($idER) {
  		$db = $this->getTable()->getAdapter();
  		$loader = Yab_Loader::getInstance();
  		$session = $loader->getSession();
  	
  		$sql = "SELECT er_nouveau_id
  				FROM association_er
  				WHERE er_ancien_id = '" . $idER . "'
  				" ;
  	
  		$result = $db->prepare($sql);
  		$result = $result->toArray() ;
  		if (isset($result[0]))
  			return $result[0] ;
  		else
  			return $result ;
  	
  	}
  	
  	public function getAssociations($idER) {
  		$db = $this->getTable()->getAdapter();
  		$loader = Yab_Loader::getInstance();
  		$session = $loader->getSession();
  		 
  		$sql = "
  				SELECT asso.*, 
					er.numero as er_ancien_nom, 
					er1.numero as er_nouveau_nom,
					typeasso.categorie_value as typeasso_nom,
  				traitement.nom as nomSignificatifTR,traitement.debut, traitement.fin, traitement.lieu,
  				typetraitement.categorie_value as traitement_nom
				FROM association_er as asso
					LEFT JOIN elementrecueilli as er ON asso.er_ancien_id = er.id
					LEFT JOIN elementrecueilli as er1 ON asso.er_nouveau_id = er1.id
					LEFT JOIN fsn_categorie as typeasso ON asso.typeasso_id = typeasso.id
  					LEFT JOIN traitement ON asso.traitement_id = traitement.id
					LEFT JOIN fsn_categorie as typetraitement ON traitement.type_id = typetraitement.id
				WHERE asso.er_nouveau_id = '" . $idER . "' or  asso.er_ancien_id = '" . $idER . "'
				ORDER BY er.numero
  				" ;
  		 
  		$result = $db->prepare($sql);
  		 
  		return $result ;
  		 
  	}
  	
  	public function getTraitements($idER) {
  		$db = $this->getTable()->getAdapter();
  		$loader = Yab_Loader::getInstance();
  		$session = $loader->getSession();
  			
  		$sql = "
  				SELECT traitement.*, fsn_categorie.categorie_value type
  				FROM traitement
  				LEFT JOIN fsn_categorie ON traitement.type_id = fsn_categorie.id
  				WHERE traitement.er_id = '$idER'  
  				" ;
  			
  		$result = $db->prepare($sql);
  			
  		return $result ;
  			
  	}
	
	public function getABothTraitements($idER_CURRENT, $idER_ASSOCIE) {
  		$db = $this->getTable()->getAdapter();
  		$loader = Yab_Loader::getInstance();
  		$session = $loader->getSession();
  			
  		$sql = "
  				SELECT traitement.*, fsn_categorie.categorie_value type
  				FROM traitement
  				LEFT JOIN fsn_categorie ON traitement.type_id = fsn_categorie.id
  				WHERE traitement.er_id = '$idER_CURRENT' OR traitement.er_id = '$idER_ASSOCIE'
  				" ;
  		echo '<script>console.log($sql)</script>';
  		$result = $db->prepare($sql);
  			
  		return $result ;
  			
  	}
  	
  	public function getByEmplacement($id) {
  		$db = $this->getTable ()->getAdapter ();
  		$loader = Yab_Loader::getInstance ();
  		$session = $loader->getSession ();
  	
  		$sql = "SELECT
					er.id as er_id,
					er.numero,
					er.description,
  					con.id as con_id,
  					con.nom
				FROM elementrecueilli as er
  				left join conteneur as con on er.conteneur_id=con.id
				where con.emplacement_id='".$id."'";
  	
  		$result = $db->prepare ( $sql ) ;
  	
  		return $result->toArray() ;
  	}

  	
  	public function getByUS($id) {
  		$db = $this->getTable ()->getAdapter ();
  		$loader = Yab_Loader::getInstance ();
  		$session = $loader->getSession ();
  	
  		$sql = "SELECT
					id,
					numero,
					description
				FROM elementrecueilli
				where us_id = '".$id."'";
  	
  		$result = $db->prepare ( $sql ) ;
  	
  		return $result->toArray() ;
  	}

  	public function getAll($sitefouille_id = null, $us_id = null, $filtre_el = null, $tri = null) {
  		$db = $this->getTable ()->getAdapter ();
  		$loader = Yab_Loader::getInstance ();
  		$session = $loader->getSession ();
  	
  		$filtre_sf = ($sitefouille_id == null) ? " AND us.sitefouille_id = sitefouille.id" : " AND us.sitefouille_id = '$sitefouille_id' " ;
  		$filtre_us = ($us_id == null) ? " AND us.id = er.us_id" : " AND us.id = '$us_id' " ;
  		 
  		switch ($filtre_el) {
  			case 1 : $filtre = " AND elementisole = '1' " ;
  			break ;
  			case 2 : $filtre = " AND elementisole = '0' " ;
  			break ;
  			case 3 : $filtre = " AND elementpoterie_id IS NOT NULL " ;
  			break ;
  			case 4 : $filtre = " AND elementpoterie_id IS NULL " ;
  			break ;
  			default : $filtre = "" ;
  			break;
  		}
  		
  		$requeteur = "requete_" . "elementrecueilli";
  		$requeteurSession = $session->has($requeteur) ? $session->get($requeteur) : null;
  		$whereRequeteur = (!is_null($requeteurSession) && !empty($requeteurSession)) ? " AND er.id IN (" . $requeteurSession . ") " : "";
  		//$session[$requeteur] = ''; // la requête étant consommée, on l'efface
  	
  		if (is_null($tri) || $tri == "numero")
  			$tri = "tri" ;
  	
  		$sql = "
  		SELECT CAST(SUBSTR(er.numero, LOCATE('.', er.numero,LOCATE('.',identification,1)+1)+1) AS UNSIGNED) tri,
  		CAST(SUBSTR(identification, LOCATE('.', identification,1)+1) AS UNSIGNED) trius,
  		er.id, er.numero, er.us_id, er.description, er.emplacement_id, er.conteneur_id,
  		itv.nom, itv.prenom,
  		st.statut,
  		fonc.fonction,
  		classe.classe,
  		us.identification,
  		nm.naturematiere as nmp,
		sitefouille.nomabrege
  		FROM elementrecueilli er
  		left join intervenant itv on itv.id = er.intervenant_id
  		left join classe on er.classe_id=classe.id
  		left join statut st on er.statut_id=st.id
  		left join fonction fonc on er.fonction_id=fonc.id
  		left join us on er.us_id=us.id
  		left join sitefouille on us.sitefouille_id=sitefouille.id
  		left join er_naturematiere as enm
  		left join naturematiere as nm on enm.naturematiere_id=nm.id
  		on er.id=enm.er_id
  		and enm.principal = 1
  		WHERE 1 = 1 $filtre_sf $filtre_us $filtre $whereRequeteur
  		GROUP BY er.id
  		ORDER BY nomabrege, trius, $tri
  		" ;
  	
  		return $db->prepare ( $sql ) ;
  	}
  	

  	public function getNextSequenceErId($us_id, $isole) {
  	
  		$db = $this->getTable()->getAdapter();
  	
  		$sql = 'SELECT  MAX(CAST(compteur AS UNSIGNED)) + 1 AS next_compteur
                FROM (
                SELECT
                    us.id AS us_id,
                    us.identification,
                    SUBSTRING_INDEX(er.numero, ".", 1) AS prefixe,
                    SUBSTRING_INDEX(er.numero, ".", -1) AS compteur,
                    er.id AS er_id,
                    er.numero
                    FROM elementrecueilli as er
                    INNER JOIN us ON us.id = er.us_id
                    WHERE us_id = "'.$us_id.'" and
                    er.elementisole="'.$isole.'"
                ) AS T';
  	
  		$result = $db->prepare($sql)->toRow() ;
  	
  		return $result == ""? 1: $result ;
  	
  	}

  	
  	public function deleteFromErNM($id) {
  		$db = $this->getTable ()->getAdapter ();
  		$loader = Yab_Loader::getInstance ();
  		$session = $loader->getSession ();
  		 
  		$sql ='DELETE from er_naturematiere where er_id="'.$id.'"';
  		 
  		$result = $db->prepare ( $sql ) ;
  		 
  		return $result->execute() ;
  	}
  	
	public function getEntiteOrganisme(){
		
		$db = $this->getTable()->getAdapter();
		$sql = "SELECT orga.nomabrege nomabrege_organisme
				FROM fsn_entiteadmin enti
				INNER JOIN fsn_organisme orga on orga.id = enti.organisme_id
			";
				
		$result = $db->prepare($sql);
		return $result ;
	}
	
	public function getCategorieValues() {
  		$db = $this->getTable()->getAdapter();
  		$loader = Yab_Loader::getInstance();
  		$session = $loader->getSession();
  
  		$sql = "SELECT id, categorie_value
  				FROM fsn_categorie" ;
  
  		$result = $db->prepare($sql);
  
	  	return $result ;
  	}
	// Retourne le resultat d'une requete comprenant aussi le nombre d'ER dont l'etat est NULL
  	public function getErEtatAndNullCounts(){
  		$db = $this->getTable()->getAdapter();
  		$loader = Yab_Loader::getInstance();
  		$session = $loader->getSession();
  		 
  		$sql = 'SELECT
				CASE
  				WHEN etat.etat IS NULL THEN "NA"
  				ELSE etat.etat
  				END AS etat,
  				CASE
  				WHEN er.etat_id IS NULL THEN 0
  				ELSE er.etat_id
  				END AS etat_id,
  				COUNT(*) AS nb_er
  				FROM etat 
  				RIGHT JOIN elementrecueilli er ON etat.id = er.etat_id
  				INNER JOIN us ON us.id = er.us_id
  				INNER JOIN sitefouille sdf ON sdf.id = us.sitefouille_id
  				GROUP BY etat.etat
  				ORDER BY etat.ordre';
  		 
  		$result = $db->prepare($sql);
  		 
  		$sitefouille_id = $session->get('sitefouille_id');
  		if (!empty($sitefouille_id) ) { $result = $result->where('us.sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
  		 
  		return $result ;
  	}
  	// Retourne le resultat d'une requete comprenant aussi le nombre d'ER dont le statut est NULL
  	public function getErStatutAndNullCounts(){
  		
  		$db = $this->getTable()->getAdapter();
  		$loader = Yab_Loader::getInstance();
  		$session = $loader->getSession();
  		
  		$sql = 'SELECT 
  				CASE
  				WHEN st.statut IS NULL THEN "NA"
  				ELSE st.statut
  				END AS statut,
  				CASE 
  				WHEN er.statut_id IS NULL THEN 0
  				ELSE er.statut_id
  				END AS statut_id,
  				COUNT(*) AS nb_er
  				FROM statut st
  				RIGHT JOIN elementrecueilli er ON er.statut_id = st.id
  				INNER JOIN us ON us.id = er.us_id
  				INNER JOIN sitefouille sdf ON sdf.id = us.sitefouille_id
  				GROUP BY st.statut
  				ORDER BY st.statut';
  		$result = $db->prepare($sql);
  		
  		$sitefouille_id = $session->get('sitefouille_id');
  		if (!empty($sitefouille_id) ) { $result = $result->where('us.sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
  			
  		return $result ;
  	}
  	// Retourne le resultat d'une requete comprenant aussi le nombre d'ER dont la classe est NULL
  	public function getErClasseAndNullCounts(){
  		 
  		$db = $this->getTable()->getAdapter();
  		$loader = Yab_Loader::getInstance();
  		$session = $loader->getSession();
  		 
  		$sql = 'SELECT
  				cl.id AS classe_id,
				CASE WHEN cl.classe IS NULL THEN "NA"
				ELSE cl.classe
				END AS classe,
				CASE WHEN er.classe_id IS NULL THEN 0
				ELSE er.classe_id
				END AS classe_id,
				COUNT(*) AS nb_er
				FROM classe cl
				RIGHT JOIN elementrecueilli er  ON cl.id = er.classe_id
				INNER JOIN us ON us.id = er.us_id
				INNER JOIN sitefouille sdf ON sdf.id = us.sitefouille_id
				GROUP BY cl.classe
  				ORDER BY cl.ordre';
  		 
  		$result = $db->prepare($sql);
  		 
  		$sitefouille_id = $session->get('sitefouille_id');
  		if (!empty($sitefouille_id) ) { $result = $result->where('us.sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
  		 
  		return $result ; 
  	}
  	// Retourne le resultat d'une requete comprenant aussi le nombre d'ER donc la fonction est NULL
  	public function getErFonctionAndNullCounts(){
  		$db = $this->getTable()->getAdapter();
  		$loader = Yab_Loader::getInstance();
  		$session = $loader->getSession();
  		
  		$sql ='SELECT
  				CASE
  				WHEN fc.fonction IS NULL THEN "NA"
  				ELSE fc.fonction
  				END AS fonction,
  				CASE 
  				WHEN er.fonction_id IS NULL THEN 0
  				ELSE er.fonction_id
  				END AS fonction_id,
  				COUNT(*) AS nb_er
  				FROM fonction fc
  				RIGHT JOIN elementrecueilli er ON fc.id = er.fonction_id
  				INNER JOIN us ON us.id = er.us_id
  				INNER JOIN sitefouille sdf ON sdf.id = us.sitefouille_id
  				GROUP BY fc.fonction
  				ORDER BY fc.ordre';
  		
  		$result = $db->prepare($sql);
  			
  		$sitefouille_id = $session->get('sitefouille_id');
  		if (!empty($sitefouille_id) ) { $result = $result->where('us.sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
  			
  		return $result ;
  	}
	
	// Liste de choix er sur index traitements
	public function getSelectElementrecueilliBySiteFouille($sitefouille_id=null){
		$db = $this->getTable()->getAdapter();
		$where = (!is_null($sitefouille_id) && !empty($sitefouille_id)) ? " WHERE sitefouille_id = '$sitefouille_id' " : "";
		
		$sql = $db->prepare('SELECT er.id as id, er.numero as numero
						FROM us
						INNER JOIN elementrecueilli er ON er.us_id = us.id '.$where
						.' ORDER BY er.numero'); // Mantis 322
		return $sql;
	}
}
