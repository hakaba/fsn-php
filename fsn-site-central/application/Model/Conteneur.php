<?php

class Model_Conteneur extends Yab_Db_Table {

	protected $_name = 'conteneur';

	
	
	public function getConteneur($id) {
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();
	
		$sql = "SELECT
					conteneur.id,
					conteneur.nom,
					emplacement_id
				FROM conteneur
				where emplacement_id='".$id."'";
	
		$result = $db->prepare ( $sql ) ;
	
		return $result->toArray() ;
	}
	
	public function getAll($entiteadmin) {
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();
	
		$sql = "SELECT 
					conteneur.id, 
					conteneur.nom, 
					conteneur.description, 
					conteneur.codebarre, 
					categorie_key,
					categorie_value, 
					emplacement.nom AS emplacement, 
					conteneur.type_id, 
					emplacement_id
				FROM conteneur 
				left join fsn_categorie as ref on type_id=ref.id 
				left join emplacement on emplacement_id=emplacement.id
				where emplacement.entiteadmin_id='".$entiteadmin."'";
	
		$result = $db->prepare ( $sql ) ;
	
		return $result ;
	}
	
	public function getType() {

		return new Model_Type($this->get('type_id'));

	}

	public function getEntiteadmin() {

		return new Model_Fsn_Entiteadmin($this->get('entiteadmin_id'));

	}

	public function getEmplacement() {
	
		return new Model_Emplacement($this->get('emplacement_id'));
	
	}
	
	public function getElementrecueillis() {

		return $this->getTable('Model_Elementrecueilli')->search(array('conteneur_id' => $this->get('id')));

	}
	
	public function getTypeLibelle($id) {
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();
	
		$sql = "
				SELECT categorie_value
				FROM fsn_categorie
				WHERE id = '$id'
				" ;
	
		$result = $db->prepare ( $sql ) ;
		
		$type = $result->toArray() ;
		if (count($type) != 0)
			return $type[0] ;
		else
			return "" ; 
	}

}