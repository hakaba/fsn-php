<?php

class Model_FaiZpwgImages extends Yab_Db_Table {

	protected $_name = 'fai_zpwg_images';

	public function getFaiImages($fai_id) {
		$db = $this->getTable()->getAdapter();
		$sql = "SELECT zi.id, zi.file, zi.name, zi.path
				FROM ".$this->_name." uzi
				INNER JOIN zpwg_images zi ON uzi.zpwg_images_id = zi.id
				WHERE uzi.fai_id = '".$fai_id."'";
		$result = $db->prepare($sql);
		return $result ;
	}

	public function addFaiImages($fai_id,$media_id) {
		$db = $this->getTable()->getAdapter();
		$generateGuuid = new Plugin_Guuid();
		$guuid = $generateGuuid->GetUUID();
		$sql = "INSERT INTO ".$this->_name." (id,zpwg_images_id, fai_id) VALUES ('".$guuid."','".$media_id."','".$fai_id."')";
		$result = $db->query($sql);
		return $result;
	}

	public function remFaiImages($fai_id,$media_id) {
		$db = $this->getTable()->getAdapter();
		$sql = "DELETE FROM ".$this->_name." WHERE zpwg_images_id = '".$media_id."' AND fai_id = '".$fai_id."'";
		$result = $db->query($sql);
		return $result;
	}

}