<?php

class Model_ElementrecueilliZpwgImages extends Yab_Db_Table {

	protected $_name = 'elementrecueilli_zpwg_images';

	public function getElementrecueilliImages($er_id) {
		$db = $this->getTable()->getAdapter();
		$sql = "SELECT zi.id, zi.file, zi.name, zi.path
				FROM ".$this->_name." uzi
				INNER JOIN zpwg_images zi ON uzi.zpwg_images_id = zi.id
				WHERE uzi.elementrecueilli_id = '".$er_id."'";
		$result = $db->prepare($sql);
		return $result ;
	}

	public function addElementrecueilliImages($er_id,$media_id) {
		$db = $this->getTable()->getAdapter();
		$generateGuuid = new Plugin_Guuid();
		$guuid = $generateGuuid->GetUUID();
		$sql = "INSERT INTO ".$this->_name." (id,zpwg_images_id, elementrecueilli_id) VALUES ('".$guuid."','".$media_id."','".$er_id."')";
		$result = $db->query($sql);
		return $result;
	}

	public function remElementrecueilliImages($er_id,$media_id) {
		$db = $this->getTable()->getAdapter();
		$sql = "DELETE FROM ".$this->_name." WHERE zpwg_images_id = '".$media_id."' AND elementrecueilli_id = '".$er_id."'";
		$result = $db->query($sql);
		return $result;
	}

}