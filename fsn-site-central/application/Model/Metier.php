<?php

class Model_Metier extends Yab_Db_Table {

	protected $_name = 'metier';

	public function getIntervenants() {

		return $this->getTable('Model_Intervenant')->search(array('metier_id' => $this->get('id')));

	}

}