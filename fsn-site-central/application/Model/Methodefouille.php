<?php

class Model_Methodefouille extends Yab_Db_Table {

	protected $_name = 'methodefouille';

	public function getUses() {

		return $this->getTable('Model_Us')->search(array('methodefouille_id' => $this->get('id')));

	}

}