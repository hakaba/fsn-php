<?php

class Model_Episodeurbain extends Yab_Db_Table {

	protected $_name = 'episodeurbain';

	public function getPhasechronos() {

		return $this->getTable('Model_Phasechrono')->search(array('episodeurbain_id' => $this->get('id')));

	}

}