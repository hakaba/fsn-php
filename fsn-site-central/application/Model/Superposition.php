<?php

class Model_Superposition extends Yab_Db_Table {

	protected $_name = 'superposition';

	public function getAnterieur() {

		return new Model_Anterieur($this->get('anterieur_id'));

	}

	public function getPosterieur() {

		return new Model_Posterieur($this->get('posterieur_id'));

	}

	public function getSptype() {

		return new Model_Sptype($this->get('sptype_id'));

	}
	
    public function getAnteroposterite() {

    $db = $this->getTable()->getAdapter();
    $loader = Yab_Loader::getInstance();
    
    $sql = 'SELECT 
        sp.anterieur_id, 
        usa.identification AS us_ante_identification,
        sp.posterieur_id, 
        usp.identification AS us_post_identification,
        sp.incertitude, 
        sp.sptype_id, 
        sptype.sptype,
        sp.observation,
        sp.duree_min_certain, 
        sp.duree_max_certain, 
        sp.duree_min_estim, 
        sp.duree_max_estim,
        sp.incertitude
      FROM superposition sp
      INNER JOIN us usa ON usa.id = sp.anterieur_id
      INNER JOIN us usp ON usp.id = sp.posterieur_id
      LEFT JOIN sptype ON sptype.id = sp.sptype_id
    ';
        
    $result = $db->prepare($sql);
    
    return $result ;  

	}

    public function deleteRelations($us_id){
        $this->delete( "anterieur_id = '".$us_id."' OR posterieur_id = '".$us_id."'" );
    }
  
}