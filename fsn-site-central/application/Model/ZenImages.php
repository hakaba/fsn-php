<?php

class Model_ZenImages extends Yab_Db_Table {

	protected $_name = 'zen_images';

	public function getAlbumImages($album_id = null) {
		$db = $this->getTable()->getAdapter();
		$sql = "SELECT zi.id, zi.filename, zi.albumid, zi.title, za.folder
					FROM zen_images zi
					INNER JOIN zen_albums za ON zi.albumid = za.id
					WHERE za.id ".($album_id?"= ".$album_id:"IS NULL")." ORDER BY zi.filename";
		$result = $db->prepare($sql);
		return $result ;
	}

}