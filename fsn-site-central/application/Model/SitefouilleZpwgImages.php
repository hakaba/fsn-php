<?php

class Model_SitefouilleZpwgImages extends Yab_Db_Table {

	protected $_name = 'sitefouille_zpwg_images';

	public function getSitefouilleImages($sf_id) {
		$db = $this->getTable()->getAdapter();
		$sql = "SELECT zi.id, zi.file, zi.name, zi.path
				FROM ".$this->_name." uzi
				INNER JOIN zpwg_images zi ON uzi.zpwg_images_id = zi.id
				WHERE uzi.sitefouille_id = '".$sf_id."'";
		$result = $db->prepare($sql);
		return $result ;
	}

	public function addSitefouilleImages($sf_id,$media_id) {
		$db = $this->getTable()->getAdapter();
		$generateGuuid = new Plugin_Guuid();
		$guuid = $generateGuuid->GetUUID();
		$sql = "INSERT INTO ".$this->_name." (id,zpwg_images_id, sitefouille_id) VALUES ('".$guuid."','".$media_id."','".$sf_id."')";
		$result = $db->query($sql);
		return $result;
	}

	public function remSitefouilleImages($sf_id,$media_id) {
		$db = $this->getTable()->getAdapter();
		$sql = "DELETE FROM ".$this->_name." WHERE zpwg_images_id = '".$media_id."' AND sitefouille_id = '".$sf_id."'";
		$result = $db->query($sql);
		return $result;
	}

}