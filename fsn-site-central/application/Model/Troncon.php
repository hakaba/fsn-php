<?php

class Model_Troncon extends Yab_Db_Table {

	protected $_name = 'troncon';

	public function getListTroncon($unitesondage_id) {
		$db = $this->getTable()->getAdapter();
		// jfb 2016-06 mantis 318
		$loader = Yab_Loader::getInstance();
		$session = $loader->getSession();

		$where = (!is_null($unitesondage_id) && !empty($unitesondage_id)) ? " AND troncon.unitesondage_id = '$unitesondage_id' " : "" ;

		/*
		 $requeteur = "requete_" . "unitesondage";
		 $requeteurSession = $session->has($requeteur) ? $session->get($requeteur) : null;
		 $where .= (!is_null($requeteurSession) && !empty($requeteurSession)) ? " AND unitesondage.id IN (" . $requeteurSession . ") " : "";
		 $session[$requeteur] = ''; // la requête étant consommée, on l'efface
		 */

		// jfb 2016-06 mantis 318 Correction de la requête ci-après : WHERE unitesondage.sitefouille_id = "'.$unitesondage_id.'"
		$sql = "SELECT
		troncon.id,
		unitesondage_id,
		troncon.identification,
		nom,
		fai_id,
		fai.identification as fai_nom,
		troncon.description,
		troncon.date_debut,
		troncon.date_fin,
		troncon.longueur,
		troncon.largeur,
		troncon.profondeur
		FROM troncon
		LEFT JOIN fai ON troncon.fai_id=fai.id
		WHERE 1 $where
		";
		return $db->prepare($sql);
	}

	public function getUslogByIdentification($ident){
		$db = $this->getTable()->getAdapter();
		return $db->prepare("SELECT * FROM troncon WHERE id = '".$ident."';");
	}

	public function getNextSequenceTronconId($unitesondage_id) {
		$db = $this->getTable()->getAdapter();
		$sql = 'SELECT MAX(CAST(compteur AS UNSIGNED)) + 1 AS next_compteur
                FROM (
                SELECT
                    unitesondage.id AS unitesondage_id,
                    unitesondage.nom,
                    SUBSTRING_INDEX(troncon.identification, ".", 1) AS prefixe,
                    SUBSTRING_INDEX(troncon.identification, ".", -1) AS compteur,
                    troncon.id AS usondage_id,
                    troncon.identification
                    FROM troncon
                    INNER JOIN unitesondage ON unitesondage.id = troncon.unitesondage_id
                    WHERE unitesondage_id = "'.$unitesondage_id.'"
                ) AS T';

		$result = $db->prepare($sql)->toRow() ;
		return $result == ""? 1: $result ;
	}
	
	public function getIdentificationFAI($id_fai) {
		$db = $this->getTable('fai')->getAdapter();
		return $db->prepare("SELECT identification FROM fai WHERE id = '".$id_fai."';");
	}

}