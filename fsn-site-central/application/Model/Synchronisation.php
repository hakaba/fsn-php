<?php

class Model_Synchronisation extends Yab_Db_Table {

	protected $_name = 'synchronisation';

	public function getSynchro1() {

		return new Model_Us($this->get('id'));

	}

	public function getSynchro2() {

		return new Model_Us($this->get('id'));

	}

	public function getSynchrotype() {

		return new Model_Synchrotype($this->get('synchrotype_id'));

	}
  
    public function getSynchronisation() {

    $db = $this->getTable()->getAdapter();
    $loader = Yab_Loader::getInstance();
    
    $sql = 'SELECT
        sc.synchro1_id,
        usl.identification AS us_left_identification,
        sc.synchro2_id,
        usr.identification AS us_right_identification,
        sc.incertitude,
        sc.synchrotype_id,
        synchrotype.synchrotype,
        sc.observation
      FROM synchronisation sc
      INNER JOIN us usl ON usl.id = sc.synchro1_id
      INNER JOIN us usr ON usr.id = sc.synchro2_id
      LEFT JOIN synchrotype ON synchrotype.id = sc.synchrotype_id
    ';
        
    $result = $db->prepare($sql);
    
    return $result ;  

    }
    
    public function ListSynchronisation() {

        $db = $this->getTable()->getAdapter();
        $loader = Yab_Loader::getInstance();
        
        $sql = 'SELECT 
                    syn.synchro1_id AS us_left_id, 
                    us_1.identification as us_left_identification,
                    syn.synchro2_id AS us_right_id, 
                    us_2.identification as us_right_identification,
                    syn.incertitude, 
                    syn.synchrotype_id,
                    fc.categorie_key, 
                    fc.categorie_value,
                    syn.observation
                FROM synchronisation syn
                INNER JOIN us us_1 ON us_1.id = syn.synchro1_id
                INNER JOIN us us_2 ON us_2.id = syn.synchro2_id
                INNER JOIN fsn_categorie fc ON fc.id = syn.synchrotype_id
                ORDER BY us_1.identification,
                    us_2.identification                       
        ';
            
        $result = $db->prepare($sql);
        
        return $result ;  

   }
  
    public function ListUsSynchronisation() {

        $db = $this->getTable()->getAdapter();
        $loader = Yab_Loader::getInstance();

        $sql = 'SELECT sy.synchro1_id,
            usa.identification AS us_identification,
            sy.synchro2_id,
            usb.identification AS us_synchro_identification,
            sy.incertitude,
            sy.synchrotype_id,
            synchrotype.synchrotype,
            sy.observation
            FROM synchronisation sy
            left JOIN synchrotype ON synchrotype.id = sy.synchrotype_id
            left JOIN us usa ON usa.id = sy.synchro1_id
            left JOIN us usb ON usb.id = sy.synchro2_id
            ';
            
        $result = $db->prepare($sql);
        
        return $result ;  

   }

    public function deleteRelations($us_id){
        $this->delete( "synchro1_id = '".$us_id."' OR synchro2_id = '".$us_id."'" );
    }
}