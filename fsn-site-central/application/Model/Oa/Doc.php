<?php

class Model_Oa_Doc extends Yab_Db_Table {

	protected $_name = 'oa_doc';

	public function getOa() {

		return new Model_Oa($this->get('oa_id'));

	}

	public function getDoc() {

		return new Model_Doc($this->get('doc_id'));

	}

}