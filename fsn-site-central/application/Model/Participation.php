<?php

class Model_Participation extends Yab_Db_Table {

	protected $_name = 'participation';

	public function getIntervenant() {
		return new Model_Intervenant($this->get('intervenant_id'));
	}
	public function getOa() {
		return new Model_Oa($this->get('oa_id'));
	}
	public function getRole() {
		return new Model_Role($this->get('role_id'));
	}
    public function getFirstParticipationDateDeb(){
      $db = $this->getTable()->getAdapter();
      $loader = Yab_Loader::getInstance();
      $session = $loader->getSession();

      $sql = 'SELECT MIN(debut) as debut, fin FROM participation';

      $result = $db->prepare($sql);

      $oa_id = $session->get('oa_id');
      if (!empty($oa_id) ) { $result = $result->where('oa_id = "'.$oa_id.'" ' ) ;  }

      return $result ;
    }
    public function getLastParticipationDateFin(){
      $db = $this->getTable()->getAdapter();
      $loader = Yab_Loader::getInstance();
      $session = $loader->getSession();

      $sql = 'SELECT debut, MAX(fin) as fin FROM participation';

      $result = $db->prepare($sql);

      $oa_id = $session->get('oa_id');
      if (!empty($oa_id) ) { $result = $result->where('oa_id = "'.$oa_id.'" ' ) ;  }

      return $result ;
    }
    public function getListParticipation(){
      $db = $this->getTable()->getAdapter();
      $loader = Yab_Loader::getInstance();
      $session = $loader->getSession();

      $sql = 'SELECT
          CONCAT_WS(" ", inter.nom, inter.prenom) AS intervenant_nom_prenom,
          inter.titre,
          pa.intervenant_id,
          pa.role_id,
          pa.oa_id,
          pa.debut AS debut,
          pa.fin AS fin,
          role.role AS role
        FROM participation pa
        INNER JOIN intervenant inter ON inter.id = pa.intervenant_id
        INNER JOIN oa ON oa.id = pa.oa_id
        INNER JOIN role ON role.id = pa.role_id';

      $result = $db->prepare($sql);

      return $result ;
    }

    public function getListParticipationHisto(){ // jfb 2016-04-29 mantis 250
    	$db = $this->getTable()->getAdapter();
    	$loader = Yab_Loader::getInstance();
    	$session = $loader->getSession();
    
    	$sql = "SELECT
           intervenant_id
          ,oa_id
          ,DATE_FORMAT(debut,'%d/%m/%Y') as debut
          ,DATE_FORMAT(fin,'%d/%m/%Y') as fin
    	  ,role_id
          ,id
    	FROM participation";
    
    	$result = $db->prepare($sql);
    
    	return $result ;
    }
    
	public function deleteRelations($oa_id){
        $this->delete( "oa_id = '".$oa_id."'" );
    }
}