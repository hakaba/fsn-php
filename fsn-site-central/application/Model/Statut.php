<?php

class Model_Statut extends Yab_Db_Table {

	protected $_name = 'statut';

	public function getElementrecueillis() {

		return $this->getTable('Model_Elementrecueilli')->search(array('statut_id' => $this->get('id')));

	}

}