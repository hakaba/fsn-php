<?php

class Model_Contour_Us extends Yab_Db_Table {

	protected $_name = 'contour_us';

	public function getUs() {

		return new Model_Us($this->get('us_id'));

	}

}