<?php

class Model_Profil_Ihm extends Yab_Db_Table {

	protected $_name = 'profil_ihm';

	public function getProfil() {

		return new Model_Profil($this->get('profil_id'));

	}

	public function getIhm() {

		return new Model_Ihm($this->get('ihm_id'));

	}

}