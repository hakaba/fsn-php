<?php

class Model_Emplacement extends Yab_Db_Table {

	protected $_name = 'emplacement';

	public function getElementrecueillis() {

		return $this->getTable('Model_Elementrecueilli')->search(array('emplacement_id' => $this->get('id')));

	}

	public function getNomOrganisme($entiteadmin_id) {
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();

		$sql = "SELECT  fsn_organisme.id, nom
				from fsn_entiteadmin left join  fsn_organisme
				on organisme_id=fsn_organisme.id where fsn_entiteadmin.id='".$entiteadmin_id."'";

		$result = $db->prepare ( $sql ) ;

		return $result;
	}



	public function getEnfant($id) {
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();
                $where_parent = "";
                if($id)
                    $where_parent = " WHERE emplacement.parent_id = '".$id."' ";

		$sql = "SELECT emplacement.id, emplacement.nom, categorie_key, count(enfant.parent_id) nb_enfant
			FROM emplacement left join fsn_categorie as ref on type_id=ref.id left join emplacement as enfant on emplacement.id=enfant.parent_id
			".$where_parent."  group by emplacement.id";

		$result = $db->prepare ( $sql ) ;

		return $result->toArray() ;
	}

	public function getSite($id) {
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();

		$sql = "SELECT emplacement.id, nom, categorie_key
			FROM emplacement left join fsn_categorie as ref on type_id=ref.id
			WHERE entiteadmin_id = '".$id."' and categorie_key='site'";

		$result = $db->prepare ( $sql ) ;

		return $result->toArray() ;
	}

	public function getEmplacementTypeAll() {
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();

		$sql = "SELECT id, categorie_value
			FROM fsn_categorie
			WHERE categorie_type = 'emplacement_type'" ;

		$result = $db->prepare ( $sql ) ;

		return $result->toArray() ;
	}

	public function getEmplacementType($label) {
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();

		$sql = "SELECT fsn_categorie.id, fsn_categorie.categorie_value
			FROM fsn_categorie, emplacement
			WHERE emplacement.type_id = fsn_categorie.id";

		$result = $db->prepare( $sql ) ;
		
		if (count($result) == 0)
			return "" ;
		else
			$emplacement_result = $result->toArray() ;
		 	return $emplacement_result[0]['id'] ;
	}

	public function getTypeEnfant($parent_id) {
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();

		if ($parent_id==0 ) {
			$sql = "SELECT id, categorie_value
					FROM fsn_categorie
					WHERE categorie_type = 'emplacement_type' AND categorie_key = 'site'
					" ;
		}  else {
			$sql = "SELECT reference.id, reference.categorie_value
				FROM emplacement left join fsn_categorie_relation as relation on type_id=relation.element_b left join fsn_categorie as reference on reference.id=relation.element_a
				WHERE emplacement.id='".$parent_id."' group by reference.id";
		}
		$result = $db->prepare ( $sql ) ;

		return $result->toArray() ;
	}

	public function getDescriptionById($id) {
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();

		$sql = "SELECT description
		FROM emplacement
		WHERE id = '$id'" ;

		$result = $db->prepare ( $sql ) ;

		return $result->toArray() ;
	}

	public function getRacine($admin_id) {
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();

		$sql = "SELECT id, nom
		FROM emplacement
		WHERE entiteadmin_id='".$admin_id."' and parent_id is null";

		$result = $db->prepare ( $sql ) ;
		$emplacement_result = $result->toArray();

		return $emplacement_result[0];
	}

	public function getVerifRacine($admin_id) {
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();

		$sql = "SELECT id, nom
		FROM emplacement
		WHERE entiteadmin_id='".$admin_id."' and parent_id is null";

		$result = $db->prepare ( $sql ) ;

		return $result;
	}



	public function getEmplacementById($id) {
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();

		$sql = "SELECT emplacement.id, emplacement.nom, emplacement.description, fsn_categorie.categorie_value as type
		FROM emplacement, fsn_categorie
		WHERE emplacement.id = '$id' AND emplacement.type_id = fsn_categorie.id" ;

		$result = $db->prepare ( $sql ) ;
		$emplacement_result = $result->toArray() ;
		return $emplacement_result[0] ;
	}


	public function getParent($id, $emplacement_id=null, $racine=null) {

		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();

		$sql="";

		if (is_null($emplacement_id)) {
			$sql = "SELECT
                        hi.id,
                        hi.nom,
                        hierarchy_emplacement_sys_connect_by_path('-', hi.id) AS path,
                        parent_id,
                        level
                    FROM (
                        SELECT 
                            hierarchy_emplacement_connect_by_prior_with_level(id, 10) AS id,
                            CAST(@level AS SIGNED) AS level 
                        FROM ( 
                            SELECT 
                                @start_with := '".$racine."',
                                @id := @start_with,
                                @level := 0 
                             ) vars, emplacement
                         WHERE @id IS NOT NULL
                       ) ho
                    JOIN emplacement hi ON hi.id = ho.id 
                        AND hi.entiteadmin_id='".$id."' 
                    ORDER BY level";
		} else {
			$sql ="SELECT 
			         hi.id, 
			         hi.nom, 
			         hierarchy_emplacement_sys_connect_by_path('-', hi.id) AS path, 
			         parent_id, 
			         level 
			       FROM    ( 
			         SELECT 
			             hierarchy_emplacement_connect_by_prior_with_level(id, 10) AS id, 
			             CAST(@level AS SIGNED) AS level 
			         FROM    ( 
			             SELECT  @start_with := '".$racine."', 
			             @id := @start_with, 
			             @level := 0 
                     ) vars, emplacement 
                     WHERE @id IS NOT NULL 
                  ) ho 
                  JOIN emplacement hi ON hi.id = ho.id 
                    AND hi.entiteadmin_id='".$id."' 
                    AND hi.id='".$emplacement_id."'";
		}
		$result = $db->prepare ( $sql ) ;

		return $result ->toArray() ;
	}

}