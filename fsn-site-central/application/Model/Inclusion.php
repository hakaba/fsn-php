<?php

class Model_Inclusion extends Yab_Db_Table {

	protected $_name = 'inclusion';

	public function getInclusion1() {

		return new Model_Inclusion1($this->get('inclusion1_id'));

	}

	public function getInclusion2() {

		return new Model_Inclusion2($this->get('inclusion2_id'));

	}

	public function getInclusions($us_id){
		$db = $this->getTable()->getAdapter();

		$sql = "SELECT
		inc.inclusion1_id,
		inc.inclusion2_id,
		usa.identification AS inclus_identification
		FROM inclusion inc
		 INNER JOIN us usa ON usa.id = inc.inclusion1_id
		 INNER JOIN us usb ON usb.id = inc.inclusion2_id
		WHERE inclusion2_id ='".$us_id."'";

		$result = $db->prepare($sql);

		return $result ;
	}

	public function deleteRelations($us_id){
		$this->delete( "inclusion2_id = '".$us_id."'");
	}

}