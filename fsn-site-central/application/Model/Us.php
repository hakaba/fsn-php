<?php

class Model_Us extends Yab_Db_Table {

	protected $_name = 'us';

	public function getInterpretation() {

		return new Model_Interpretation($this->get('interpretation_id'));

	}

	public function getContexte() {

		return new Model_Contexte($this->get('contexte_id'));

	}

	public function getConsistance() {

		return new Model_Consistance($this->get('consistance_id'));

	}

	public function getMethodefouille() {

		return new Model_Methodefouille($this->get('methodefouille_id'));

	}

	public function getHomogeneite() {

		return new Model_Homogeneite($this->get('homogeneite_id'));

	}

	public function getSitefouille() {

		return new Model_Sitefouille($this->get('sitefouille_id'));

	}
	
	public function getPhase() {

		return new Model_Phasechrono($this->get('phase_id'));

	}
	
	public function getElementrecueillis() {

		return $this->getTable('Model_Elementrecueilli')->search(array('us_id' => $this->get('id')));

	}

	public function getTopoUses() {

		return $this->getTable('Model_Topo_Us')->search(array('us_id' => $this->get('id')));

	}

	public function getUsDocs() {

		return $this->getTable('Model_Us_Doc')->search(array('us_id' => $this->get('id')));

	}

	public function getUsIntervenants() {

		return $this->getTable('Model_Us_Intervenant')->search(array('us_id' => $this->get('id')));

	}

	public function getUsOas() {

		return $this->getTable('Model_Us_Oa')->search(array('us_id' => $this->get('id')));

	}
  
    public function getUsIntervenantsDetail() {
  
    $db = $this->getTable()->getAdapter();
    $loader = Yab_Loader::getInstance();
    
    $sql = 'SELECT 
      	us.id AS us_id, 
      	us.identification, 
      	us.description, 
      	us.debut AS us_debut, 
      	us.fin AS us_fin,
      	us.commentaire, 
      	us.occupation, 
      	us.couleur, 
      	us.interpretation, 
      	us.contexte_id, 
      	us.consistance_id, 
      	us.methodefouille_id, 
      	elementmineral_tuiles, 
      	elementmineral_brique, 
      	elementorganique_charbon, 
      	elementorganique_cendre, 
      	elementmineral_platre, 
      	descriptionorganique, 
      	descriptionmineral, 
      	materielisole, 
      	materielconserve_poterie, 
      	materielconserve_osanimal, 
      	materielconserve_construc, 
      	homogeneite_id, 
      	sitefouille_id,
        CASE 
      		WHEN inte.id IS NULL THEN "NA"
      		ELSE inte.id 
      	END AS intervenant_id, 
      	inte.titre, 
      	inte.nom, 
      	inte.prenom, 
      	inte.adresse, 
      	inte.cp, 
      	inte.ville, 
      	inte.telephone, 
      	inte.email, 
      	inte.organisme_id, 
      	inte.commentaire, 
      	inte.metier_id,
        usi.id AS intervention_id,        
        CASE 
      		WHEN usi.debut IS NULL THEN "0000-00-00"
      		ELSE usi.debut 
      	END AS usi_debut, 
        CASE 
      		WHEN usi.fin IS NULL THEN "0000-00-00"
      		ELSE usi.fin
      	END AS usi_fin,
      	ti.typeinterv
      FROM us 
      INNER JOIN sitefouille sf ON sf.id = us.sitefouille_id
      INNER JOIN intervention_us usi ON  usi.us_id = us.id
      INNER JOIN intervenant inte ON inte.id = usi.intervenant_id
      -- LEFT JOIN fsn_categorie fsnc ON fsnc.id = usi.type_id AND fsnc.categorie_type = "intervention_type"
      INNER JOIN typeintervention ti ON ti.id = usi.type_id
      ORDER BY usi_debut, us.identification     
    ';
    
    $result = $db->prepare($sql);
    
    $loader = Yab_Loader::getInstance();
    $session = $loader->getSession();
    $sitefouille_id = $session->get('sitefouille_id');
    if (!empty($sitefouille_id) ) { $result = $result->where('sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
    
    return $result ;
    
  
  }
  
    public function getFirstUsDateDeb(){

    $db = $this->getTable()->getAdapter();
    $loader = Yab_Loader::getInstance();
    $session = $loader->getSession();
    
    $sql = 'SELECT MIN(debut) as debut, fin FROM us';
        
    $result = $db->prepare($sql);
    
    $sitefouille_id = $session->get('sitefouille_id');
    if (!empty($sitefouille_id) ) { $result = $result->where('sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
    
    return $result ;  
  
  }
  
    public function getLastUsDateFin(){

    $db = $this->getTable()->getAdapter();
    $loader = Yab_Loader::getInstance();
    $session = $loader->getSession();
    
    $sql = 'SELECT debut, MAX(fin) as fin FROM us';
        
    $result = $db->prepare($sql);
    
    $sitefouille_id = $session->get('sitefouille_id');
    if (!empty($sitefouille_id) ) { $result = $result->where('sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
    
    return $result ;  
  
  }
  
    public function getNextSequenceUsId($sitefouille_id) {
          
      $db = $this->getTable()->getAdapter();
      
      $sql = 'SELECT MAX(CAST(compteur AS UNSIGNED)) + 1 AS next_compteur
                FROM (
                SELECT 
                    sitefouille.id AS sitefouille_id,
                    sitefouille.nom,
                    sitefouille.nomabrege,
                    SUBSTRING_INDEX(us.identification, ".", 1) AS prefixe,
                    SUBSTRING_INDEX(us.identification, ".", -1) AS compteur,    
                    us.id AS us_id, 
                    us.identification
                    FROM us
                    INNER JOIN sitefouille ON sitefouille.id = us.sitefouille_id
                    WHERE sitefouille_id = "'.$sitefouille_id.'"
                ) AS T';
      
      $result = $db->prepare($sql)->toRow() ;
      
      return $result == ""? 1: $result ;
      
  }  
  
    public function getUsDetail(){
    
    $db = $this->getTable()->getAdapter();
    //$loader = Yab_Loader::getInstance();
    //$session = $loader->getSession();
    $sql = 'SELECT
                us.sitefouille_id,
                sdf.nom AS sitefouille_nom,
                sdf.nomabrege AS sitefouille_nomabrege,
                us.id AS us_id, 
                us.identification AS us_identification, 
                er.nb_er,
                us.description,
                us.debut,
                us.fin,
                pc.identification AS phasechrono_identification,
                pc.datatdebbas_aaaa AS phasechrono_datatdebbas_aaaa,
                pc.datatdebhaut_aaaa AS phasechrono_datatdebhaut_aaaa, 
                pc.datatfinbas_aaaa AS phasechrono_datatfinbas_aaaa,
                pc.datatfinhaut_aaaa AS phasechrono_datatfinhaut_aaaa,
                us.commentaire,
                us.occupation,
                us.couleur,
                us.interpretation,
                us.contexte_id,
                cntx.categorie_key AS contexte_key,
                cntx.categorie_value AS contexte,
                us.consistance_id,
                consist.categorie_key AS consistance_key,
                consist.categorie_value AS consistance,
                us.methodefouille_id,
                methode.categorie_key AS methodefouille_key,
                methode.categorie_value AS methodefouille,
                us.elementmineral_tuiles,
                us.elementmineral_brique,
                us.elementorganique_charbon,
                us.elementorganique_cendre,
                us.elementmineral_platre,
                us.descriptionorganique,
                us.descriptionmineral,
                us.materielisole,
                us.materielconserve_poterie,
                us.materielconserve_osanimal,
                us.materielconserve_construc,
                us.homogeneite_id,
                homogen.categorie_key AS homogeneite_key,
                homogen.categorie_value AS homogeneite,
                us.taq_certain,
                us.tpq_certain,
                us.inst_org_plaf_certain,
                us.inst_org_planch_certain,
                us.duree_min_certain,
                us.duree_max_certain,
                us.taq_estim,
                us.tpq_estim,
                us.inst_org_plaf_estim,
                us.inst_org_planch_estim,
                us.duree_min_estim,
                us.duree_max_estim
            FROM us
            INNER JOIN fsn_categorie cntx ON cntx.id = us.contexte_id
            INNER JOIN fsn_categorie consist ON consist.id = us.consistance_id
            INNER JOIN fsn_categorie homogen ON homogen.id = us.homogeneite_id
            INNER JOIN fsn_categorie methode ON methode.id = us.methodefouille_id
            INNER JOIN sitefouille sdf ON sdf.id = us.sitefouille_id
            RIGHT JOIN phasechrono pc ON pc.id = us.phase_id
            LEFT JOIN (
                SELECT 
                    er.us_id,
                    count(*) AS nb_er 
                FROM elementrecueilli er
                GROUP BY er.us_id ) AS er ON er.us_id = us.id
            ORDER BY us.identification 
    ';
    $result = $db->prepare($sql);

    $sitefouille_id = Yab_Loader::getInstance()->getSession()->get('sitefouille_id');
    if (!empty($sitefouille_id) ) { $result = $result->where('us.sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
    
    return $result ;
  
  }

    public function getListUs($sf_id){
    	$db = $this->getTable()->getAdapter();
    	
    	// jfb 2016-06 mantis 318
		$loader = Yab_Loader::getInstance();
		$session = $loader->getSession();
    	if (!$sf_id) {
			$sf_id = $session->has('sitefouille_id') ? $session->get('sitefouille_id') : null ;
		}
    	$where = (!is_null($sf_id) && !empty($sf_id)) ? " AND us.sitefouille_id = '$sf_id' " : "" ;

    	$requeteur = "requete_" . "us";
    	$requeteurSession = $session->has($requeteur) ? $session->get($requeteur) : null;
    	$where .= (!is_null($requeteurSession) && !empty($requeteurSession)) ? " AND us.id IN (" . $requeteurSession . ") " : "";
    	$session[$requeteur] = ''; // la requête étant consommée, on l'efface
    	 
    	// jfb 2016-06 mantis 318 Correction de la requête ci-après : WHERE us.sitefouille_id = "'.$sf_id.'"
    	$sql = "SELECT
                us.id AS us_id,
                us.identification AS us_identification,
                us.debut,
                us.fin,
                us.description,
                us.interpretation,
                er.nb_er,
                sdf.nomabrege AS sitefouille_nomabrege
                FROM us
                LEFT JOIN phasechrono pc ON pc.id = us.phase_id
                INNER JOIN sitefouille sdf ON sdf.id = us.sitefouille_id
                LEFT JOIN (
                            SELECT
                                er.us_id,
                                count(*) AS nb_er
                            FROM elementrecueilli er
                            GROUP BY er.us_id
                          ) AS er ON er.us_id = us.id
                WHERE 1 $where
                ORDER BY us.identification
                ";
    	return $db->prepare($sql);
    }

    public function getSynchronisationUs(){

    $db = $this->getTable()->getAdapter();
    $loader = Yab_Loader::getInstance();
    $session = $loader->getSession();
    
    $sql = 'SELECT 
      	sn.synchro1_id,
      	usl.identification AS us_left_synchro,
        ctxl.contexte AS us_left_contexte,
        -- ctxl.categorie_key,
        -- ctxl.categorie_value,
        -- ctxl.color,
        CASE 
          -- WHEN incertitude = 1 THEN "pt.�tre synchrone"
          WHEN incertitude = 1 THEN "?"
          ELSE "synchrone avec"
        END AS strati_relation_type, 
      	sn.synchro2_id, 
      	usr.identification AS us_right_synchro,
      	ctxr.contexte AS us_right_contexte,
        -- ctxr.categorie_key,
        -- ctxr.categorie_value,
        -- ctxr.color,        
        sn.incertitude, 
      	sn.synchrotype_id, 
      	snt.synchrotype,
      	-- fsnc.categorie_key,
      	-- fsnc.categorie_value, 
      	sn.observation,
        2 AS poid 
      FROM synchronisation sn
      INNER JOIN us usl ON usl.id = sn.synchro1_id
      INNER JOIN us usr ON usr.id = sn.synchro2_id
      INNER JOIN synchrotype snt ON snt.id = sn.synchrotype_id 
      LEFT JOIN contexte ctxl ON ctxl.id = usl.contexte_id
      -- LEFT JOIN fsn_categorie ctxl ON ctxl.id = usl.contexte_id 
      LEFT JOIN contexte ctxr ON ctxr.id = usr.contexte_id
      -- LEFT JOIN fsn_categorie ctxr ON ctxr.id = usr.contexte_id 
      -- LEFT JOIN fsn_categorie fsnc ON fsnc.id = sp.sptype_id AND fsnc.categorie_type = "us_synchronisme_type"
      ORDER BY sn.synchro1_id
    ';
        
    $result = $db->prepare($sql);
    
    $sitefouille_id = $session->get('sitefouille_id');
    if (!empty($sitefouille_id) ) { $result = $result->where('usl.sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
    
    return $result ;  
  
  }
  
    public function getSuperpositionUs(){

    $db = $this->getTable()->getAdapter();
    $loader = Yab_Loader::getInstance();
    $session = $loader->getSession();
    
    $sql = 'SELECT 
      	sp.anterieur_id,
      	usa.identification AS us_anterieur,
        ante.nb_link AS us_anterieur_link,
        ctxa.contexte as us_anterieur_contexte,
        -- ctxa.categorie_key,
        -- ctxa.categorie_value,
        -- ctxa.color,  
      	sp.posterieur_id,
        CASE 
          -- WHEN incertitude = 1 THEN "peut-�tre sous"
          WHEN incertitude = 1 THEN "?"
          ELSE "sous"
        END AS strati_relation_type,   
      	usp.identification AS us_posterieur,
        post.nb_link AS us_posterieur_link,
        ctxp.contexte as us_posterieur_contexte,
      	-- ctxp.categorie_key,
        -- ctxp.categorie_value,
        -- ctxp.color,
        sp.incertitude, 
      	sp.sptype_id,
        spt.sptype,
        -- fsnc.categorie_key,
        -- fsnc.categorie_value,
      	sp.observation,
        1 as level_posterieur,
        ( post.nb_link * ante.nb_link ) AS poid, 
        2 as level_anterieur
      FROM superposition sp
      INNER JOIN us usa ON usa.id = sp.anterieur_id
      INNER JOIN us usp ON usp.id = sp.posterieur_id
      INNER JOIN sptype spt ON spt.id = sp.sptype_id
      LEFT JOIN contexte ctxa ON ctxa.id = usa.contexte_id
      -- LEFT JOIN fsn_categorie ctxa ON ctxa.id = usa.contexte_id
      LEFT JOIN contexte ctxp ON ctxp.id = usp.contexte_id
      -- LEFT JOIN fsn_categorie ctxp ON ctxp.id = usp.contexte_id
      -- LEFT JOIN fsn_categorie fsnc ON fsnc.id = sp.sptype_id AND fsnc.categorie_type = "us_superposition_type"
      LEFT JOIN (
        SELECT 
      		ant.anterieur_id, -- enfant 
      		count(*) AS nb_link
      	FROM superposition ant
      	GROUP BY ant.anterieur_id
      ) ante ON ante.anterieur_id = sp.anterieur_id
      LEFT JOIN (
        SELECT 
      		pos.posterieur_id, -- parent 
      		count(*) AS nb_link
      	FROM superposition pos
      	GROUP BY pos.posterieur_id
      ) post ON post.posterieur_id = sp.posterieur_id  
      ORDER BY post.nb_link DESC
    ';
        
    $result = $db->prepare($sql);
    
    $sitefouille_id = $session->get('sitefouille_id');
    if (!empty($sitefouille_id) ) { $result = $result->where('usa.sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
    
    return $result ;  
  
  }

    /**
     * 
     */
    public function getUsPhases() {

	$db = $this->getTable()->getAdapter();
    $loader = Yab_Loader::getInstance();
    $session = $loader->getSession();
    
    $sql = 'SELECT
      CASE 
    		WHEN phch.identification IS NULL THEN "NA"
    		ELSE phch.identification 
    	END AS phase_identification,
    	phch.datatdebbas_aaaa,
    	phch.datatfinhaut_aaaa,
      0 AS phase_ordre,
    	us.sitefouille_id,
    	sdf.nom AS sitefouille_nom,
    	sdf.nomabrege AS sitefouille_nomabrege,
      us.id AS us_id, 
    	us.identification AS us_identification,
      contexte.contexte, 	
    	us.taq_aaaa,
      us.tpq_aaaa
    FROM us
    INNER JOIN sitefouille sdf ON sdf.id = us.sitefouille_id
    INNER JOIN contexte ON contexte.id = us.contexte_id   
    LEFT JOIN phase_us pus ON pus.us_id = us.id
    LEFT JOIN phasechrono phch ON phch.id = pus.phase_id
    GROUP BY
    	phch.datatdebbas_aaaa,
    	phch.datatfinhaut_aaaa,
    	us.sitefouille_id,
    	sdf.nom,
    	sdf.nomabrege,
      us.id, 
    	us.identification
     ORDER BY
      phch.datatdebbas_aaaa,
      us.identification        
    ';
        
    $result = $db->prepare($sql);
    
    $sitefouille_id = $session->get('sitefouille_id');
    if (!empty($sitefouille_id) ) { $result = $result->where('us.sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
    
    return $result ;  

	}
  
    /**
     * 
     */
    public function getUsPhasechronos() {

		$db = $this->getTable()->getAdapter();
    $loader = Yab_Loader::getInstance();
    $session = $loader->getSession();
    
    $sql = 'SELECT 
      	us.id AS us_id, 
      	us.identification, 
      	us.description, 
      	us.debut AS us_debut, 
      	us.fin AS us_fin, 
      	us.instformdeb_aaaa, 
      	us.instformdeb_mm, 
      	us.instformdeb_jj, 
      	us.instformdeb_ad, 
      	us.instformfin_aaaa, 
      	us.instformfin_mm, 
      	us.instformfin_jj, 
      	us.instformfin_ad, 
      	us.dureeinstformdeb, 
      	us.dureeinstformfin, 
      	us.commentaire, 
      	us.occupation, 
      	us.couleur, 
      	us.interpretation, 
      	us.contexte_id, 
      	us.consistance_id, 
      	us.methodefouille_id, 
      	elementmineral_tuiles, 
      	elementmineral_brique, 
      	elementorganique_charbon, 
      	elementorganique_cendre, 
      	elementmineral_platre, 
      	descriptionorganique, 
      	descriptionmineral, 
      	materielisole, 
      	materielconserve_poterie, 
      	materielconserve_osanimal, 
      	materielconserve_construc, 
      	homogeneite_id, 
      	us.sitefouille_id,
      	CASE 
    		  WHEN phch.id IS NULL THEN "NA"
    		  ELSE phch.id
        END AS phase_id,
        CASE 
    		  WHEN phch.identification IS NULL THEN "NA"
    		  ELSE phch.identification
        END AS phase_identification,
      	phch.description AS phase_description,
        phch.datatdebbas_aaaa,
      	phch.datatdebbas_mm,
      	phch.datatdebbas_jj,
      	phch.datatdebbas_ad,
      	phch.datatfinhaut_aaaa,
      	phch.datatfinhaut_mm,
      	phch.datatfinhaut_jj,
      	phch.datatfinhaut_ad        
      FROM us 
      LEFT JOIN sitefouille sf ON sf.id = us.sitefouille_id
      LEFT JOIN phase_us pus ON pus.us_id = us.id
      LEFT JOIN phasechrono phch ON phch.id = pus.phase_id
      ORDER BY
        phch.datatdebbas_aaaa,
        us.identification        
    ';
        
    $result = $db->prepare($sql);
    
    $sitefouille_id = $session->get('sitefouille_id');
    if (!empty($sitefouille_id) ) { $result = $result->where('us.sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
    
    return $result ;  

	}
    
  
    public function countErbyUs() {

		$db = $this->getTable()->getAdapter();
    $loader = Yab_Loader::getInstance();
    $session = $loader->getSession();
    
    $sql = 'SELECT 
      	sdf.nom,
      	sdf.nomabrege,
      	er.us_id, 
      	us.identification,
      	us.debut,
      	us.fin,
      	us.methodefouille_id,
      	count(er.id) AS er_count
      FROM us      
      INNER JOIN sitefouille sdf ON sdf.id = us.sitefouille_id
      INNER JOIN elementrecueilli er ON er.us_id = us.id
      GROUP BY
      	sdf.nom,
      	sdf.nomabrege,
      	er.us_id, 
      	us.identification,
      	us.debut,
      	us.fin,
      	us.methodefouille_id
    ';
        
    $result = $db->prepare($sql);
    
    $sitefouille_id = $session->get('sitefouille_id');
    if (!empty($sitefouille_id) ) { $result = $result->where('sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
    
    return $result ;  

	}

  /**
   * Repartition des US par coordonn�es latitude/longitude et
   * altitude (profondeur))
   */    
    public function getAvgTopoUs(){

    $db = $this->getTable()->getAdapter();
    $loader = Yab_Loader::getInstance();
    $session = $loader->getSession();
    
    /**
     * Dans le cas o� l'us est d�fini par plusieur point topographique
     * d�finition d'un point m�dian # group by us.id     
     */
    $sql = 'SELECT 
      	us.id AS us_id, 
      	us.identification,
        us.methodefouille_id, 
        fc.categorie_key,
        fc.categorie_value,
      	(SUM( topo.x ) / count(*) ) AS x,
      	(SUM( topo.y ) / count(*) ) AS y,
      	(SUM( topo.z )  / count(*) ) AS z      	
      FROM us
      INNER JOIN topo_us tus ON tus.us_id = us.id
      INNER JOIN topographie topo ON topo.id = tus.topo_id
      INNER JOIN fsn_categorie fc ON fc.id = us.methodefouille_id 
      GROUP BY us.id, us.identification, us.methodefouille_id
    ';
        
    $result = $db->prepare($sql);
    
    $sitefouille_id = $session->get('sitefouille_id');
    if (!empty($sitefouille_id) ) { $result = $result->where('sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
    
    return $result ;  
  
  }    
  
    public function getGlobalTopoUs($us_id=null){

    $db = $this->getTable()->getAdapter();
    $loader = Yab_Loader::getInstance();
    $session = $loader->getSession();
    
    $sql = 'SELECT 
      	us.id AS us_id, 
      	us.identification, 
      	topo.x AS x,
      	topo.y AS y,
      	topo.z AS z      	
      FROM us
      INNER JOIN topo_us tus ON tus.us_id = us.id
      INNER JOIN topographie topo ON topo.id = tus.topo_id
    ';
        
    $result = $db->prepare($sql);
    
    $sitefouille_id = $session->get('sitefouille_id');
    if (!empty($sitefouille_id) ) { $result = $result->where('sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
    
    if (!empty($us_id) ) { $result = $result->where('us.id = "'.$us_id.'" ' ) ;  }

    return $result ;  
  
  } 
  
    public function getMinMaxTopoUs(){

    $db = $this->getTable()->getAdapter();
    $loader = Yab_Loader::getInstance();
    $session = $loader->getSession();
    
    $sql = 'SELECT 
      	MIN(topo.x) AS min_x,
      	MAX(topo.x) AS max_x,
      	MIN(topo.y) AS min_y,
      	MAX(topo.y) AS max_y,
      	MIN(topo.z) AS min_z,
      	MAX(topo.z) AS max_z      	
      FROM us
      INNER JOIN topo_us tus ON tus.us_id = us.id
      INNER JOIN topographie topo ON topo.id = tus.topo_id
    ';
        
    $result = $db->prepare($sql);
    
    $sitefouille_id = $session->get('sitefouille_id');
    if (!empty($sitefouille_id) ) { $result = $result->where('sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
    
    return $result ;  
  
  } 
  
    public function getUsInfosExport(){

    $db = $this->getTable()->getAdapter();
    $loader = Yab_Loader::getInstance();
    $session = $loader->getSession();
    
    $sitefouille_id = $session->get('sitefouille_id');
    if (!empty($sitefouille_id) ) { 
      $sql_filter = "WHERE us.sitefouille_id = '".$sitefouille_id."' " ;  
    } else {
      $sql_filter = "" ;    
    }
    
    $sql = "SELECT
      	sitefouille_nom,
      	sitefouille_nomabrege,
        us_identification, 
      	description, 
      	us_debut_fouille, 
      	us_fin_fouille,  
      	CONCAT_WS('-',instformdeb_aaaa,instformdeb_mm,instformdeb_jj) AS instformdeb,
      	instformdeb_ad, 
      	CONCAT_WS('-',instformfin_aaaa,instformfin_mm,instformfin_jj) AS instformfin,
      	instformfin_ad, 
        taq_aaaa,
        tpq_aaaa,
      	dureeinstformdeb, 
      	dureeinstformfin, 
      	commentaire, 
      	occupation, 
      	couleur, 
      	interpretation, 
      	contexte,
      	consistance, 
      	methodefouille,
      	elementmineral_tuiles, 
      	elementmineral_brique, 
      	elementorganique_charbon, 
      	elementorganique_cendre, 
      	elementmineral_platre, 
      	descriptionorganique, 
      	descriptionmineral, 
      	materielisole, 
      	materielconserve_poterie, 
      	materielconserve_osanimal, 
      	materielconserve_construc, 
      	homogeneite
      FROM (
      	SELECT
            	sdf.nom AS sitefouille_nom,
            	sdf.nomabrege AS sitefouille_nomabrege,
            	us.identification AS us_identification, 
            	us.debut AS us_debut_fouille, 
            	us.fin AS us_fin_fouille, 
            	us.description,
            	-- us.instformdeb_aaaa, 
            	-- us.instformdeb_mm, 
            	-- us.instformdeb_jj, 
            	CASE instformdeb_aaaa
            		WHEN 0 OR instformdeb_aaaa IS NULL
            		THEN '0000'
            		ELSE instformdeb_aaaa
        		END AS instformdeb_aaaa,
        		CASE instformdeb_mm
        			WHEN 0 OR instformdeb_mm IS NULL
            		THEN '00'
            		ELSE instformdeb_mm
        		END AS instformdeb_mm,
        		CASE instformdeb_jj
        			WHEN 0 OR instformdeb_jj IS NULL
            		THEN '00'
            		ELSE instformdeb_jj
        		END AS instformdeb_jj,
            	us.instformdeb_ad, 
            	
            	-- us.instformfin_aaaa, 
            	-- us.instformfin_mm, 
            	-- us.instformfin_jj, 
            	CASE 
            		WHEN instformdeb_aaaa = 0 OR instformfin_aaaa IS NULL
            		THEN '0000'
            		ELSE instformfin_aaaa
        		END AS instformfin_aaaa,
        		CASE 
        			WHEN instformfin_mm = 0 OR instformfin_mm IS NULL
            		THEN '00'
            		ELSE instformfin_mm
        		END AS instformfin_mm,
        		CASE 
        			WHEN instformfin_jj = 0 OR instformfin_jj IS NULL
            		THEN '00'
            		ELSE instformfin_jj
        		END AS instformfin_jj,
            	
            	us.instformfin_ad, 
            	CASE 
            		WHEN taq_aaaa = 0 OR taq_aaaa IS NULL THEN '0000'
            		WHEN NULL THEN '0000'
            		ELSE taq_aaaa
        		END AS taq_aaaa,
            	CASE 
            		WHEN tpq_aaaa = 0 OR tpq_aaaa IS NULL
            		THEN '0000'
            		ELSE tpq_aaaa
        		END AS tpq_aaaa,
            	us.dureeinstformdeb, 
            	us.dureeinstformfin, 
            	us.commentaire, 
            	us.occupation, 
            	us.couleur, 
            	us.interpretation, 
            	-- us.contexte_id, 
            	contexte.contexte,
            	-- us.consistance_id,
            	consistance.consistance, 
            	-- us.methodefouille_id, 
            	methodefouille.methodefouille,
            	us.elementmineral_tuiles, 
            	us.elementmineral_brique, 
            	us.elementorganique_charbon, 
            	us.elementorganique_cendre, 
            	us.elementmineral_platre, 
            	us.descriptionorganique, 
            	us.descriptionmineral, 
            	us.materielisole, 
            	us.materielconserve_poterie, 
            	us.materielconserve_osanimal, 
            	us.materielconserve_construc, 
            	-- us.homogeneite_id,
            	homogeneite.homogeneite
            FROM us
            INNER JOIN contexte ON contexte.id = us.contexte_id
            INNER JOIN consistance ON consistance.id = us.consistance_id
            INNER JOIN homogeneite ON homogeneite.id = us.homogeneite_id
            INNER JOIN methodefouille ON methodefouille.id = us.methodefouille_id
            INNER JOIN sitefouille sdf ON sdf.id = us.sitefouille_id
            ".$sql_filter."
       ) AS T
       ORDER BY
        sitefouille_nom,
        us_identification
    ";
        
    $result = $db->prepare($sql);
    
    return $result ;  
  
  }
  
    public function importFileUs($importfile) {
  
    return true ;
    
  }

	public function getAllUsFromSf($sf_id){
		$db = $this->getTable()->getAdapter();
		return $db->prepare("SELECT *, CONCAT_WS(':', identification, SUBSTR(description, 1, 10)) AS identification_desc FROM us WHERE us.sitefouille_id = '".$sf_id."' ORDER BY identification;");
	}
	
	public function getById($id) {
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();
	
		$sql = "SELECT 
				id, 
				identification
				FROM us
				where id = '".$id."'";
	
		$result = $db->prepare ( $sql ) ;

		return $result->toArray()[0];
	}
	
	public function getBySitefouilleAll($sitefouille_id = null) {
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();
		
		$where = (!is_null($sitefouille_id) && !empty($sitefouille_id)) ? " AND sitefouille_id = '$sitefouille_id' " : "" ;
			 
		$sql = "
		SELECT id, identification
		FROM us
		WHERE 1 $where
		ORDER BY identification
		" ;
		 
		return $db->prepare ( $sql ) ;
	}

    public function getUsByIdentification($ident){
        $db = $this->getTable()->getAdapter();
        return $db->prepare("SELECT * FROM us WHERE us.identification = '".$ident."';");
    }
		
	public function getEntiteOrganisme(){
		
		$db = $this->getTable()->getAdapter();
		$sql = "SELECT orga.nomabrege nomabrege_organisme
				FROM fsn_entiteadmin enti
				INNER JOIN fsn_organisme orga on orga.id = enti.organisme_id
			";
				
		$result = $db->prepare($sql);
		return $result ;
	}
	
	public function getUsTotal(){
		
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();
		
		$total = $db->prepare("select count(*) as total from us");
		$sitefouille_id = $session->get('sitefouille_id');
		if (!empty($sitefouille_id) ) { 
				$total = $total->where('sitefouille_id = "'.$sitefouille_id.'" ' );  
		}
		return $total ;
	}
	
	public function getUsEncours(){
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();
		$sitefouille_id = $session->get('sitefouille_id');
		$where = (!empty($sitefouille_id)) ? " AND sitefouille_id = '".$sitefouille_id."'" : "" ;
		$sql="select count(*) as encours from us where (fin > now() or (fin is null and debut is not null))".$where;
		//echo var_dump($sql);
		$encours = $db->prepare($sql);
		
// 		if (!empty($sitefouille_id) ) {
// 			$encours = $encours->where('sitefouille_id = "'.$sitefouille_id.'" ' );
// 		}
		return $encours;
	}
	
	public function getUsPhase(){
		
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();
		
		$sql="SELECT us.identification
				, CASE WHEN pc.identification IS NULL THEN 'Non déterminé' ELSE pc.identification END AS phase
				, datatfinhaut_aaaa, pc.sitefouille_id, count( * ) AS nb_us
		FROM us
		LEFT JOIN phasechrono pc ON us.phase_id = pc.id
		GROUP BY pc.identification
		ORDER BY datatfinhaut_aaaa";
		$result = $db->prepare($sql);
		
				
		$sitefouille_id = $session->get('sitefouille_id');
		if (!empty($sitefouille_id) ) { $result = $result->where('us.sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
		return $result ;
	}
	
	public function getUsContexte(){
		
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();
		
		$sql="SELECT contexte_id, CASE WHEN contexte.contexte IS NULL 
		THEN 'Non déterminé' ELSE contexte.contexte END AS contexte, count( * ) AS nb_us
		FROM us LEFT JOIN contexte ON us.contexte_id = contexte.id
		GROUP BY contexte.contexte";
		$result = $db->prepare($sql);
		
		$sitefouille_id = $session->get('sitefouille_id');
		if (!empty($sitefouille_id) ) { $result = $result->where('us.sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
		return $result ;
	}
	
	public function getUsConsistance(){
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();
		
		$sql="SELECT consistance_id, CASE WHEN consistance.consistance IS NULL 
		THEN 'Non déterminé' ELSE consistance.consistance END AS consistance, count( * ) AS nb_us
		FROM us LEFT JOIN consistance ON us.consistance_id = consistance.id
		GROUP BY consistance.consistance";
		$result = $db->prepare($sql);
		
		$sitefouille_id = $session->get('sitefouille_id');
		if (!empty($sitefouille_id) ) { $result = $result->where('us.sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
		return $result ;	
	}
	
	public function getUsMethode(){
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();
		
		$sql="SELECT methodefouille_id, CASE WHEN methodefouille.methodefouille IS NULL 
		THEN 'Non déterminé' ELSE methodefouille.methodefouille END AS methode, count( * ) AS nb_us
		FROM us LEFT JOIN methodefouille ON us.methodefouille_id = methodefouille.id
		GROUP BY methodefouille.methodefouille";
		$result = $db->prepare($sql);
		
		$sitefouille_id = $session->get('sitefouille_id');
		if (!empty($sitefouille_id) ) { $result = $result->where('us.sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
		return $result ;
	}
	
	public function getUsHomogeneite(){
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();
		
		$sql="SELECT homogeneite_id, CASE WHEN homogeneite.homogeneite IS NULL 
		THEN 'Non déterminé' ELSE homogeneite.homogeneite END AS homogeneite, count( * ) AS nb_us
		FROM us LEFT JOIN homogeneite ON us.homogeneite_id = homogeneite.id
		GROUP BY homogeneite.homogeneite";
		$result = $db->prepare($sql);
		
		$sitefouille_id = $session->get('sitefouille_id');
		if (!empty($sitefouille_id) ) { $result = $result->where('us.sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
		return $result ;
	}
		
}

