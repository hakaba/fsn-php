<?php

class Model_Naturematiere extends Yab_Db_Table {

	protected $_name = 'naturematiere';

	public function getClasseNaturematieres() {

		return $this->getTable('Model_Classe_Naturematiere')->search(array('naturematiere_id' => $this->get('id')));

	}

	public function getErNaturematieres() {

		return $this->getTable('Model_Er_Naturematiere')->search(array('naturematiere_id' => $this->get('id')));

	}

	public function getNaturematiereFonctions() {

		return $this->getTable('Model_Naturematiere_Fonction')->search(array('naturematiere_id' => $this->get('id')));

	}

}