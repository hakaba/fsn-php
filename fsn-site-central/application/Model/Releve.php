<?php

class Model_Releve extends Yab_Db_Table {

	protected $_name = 'releve';

	public function getCroquis() {

		return new Model_Croquis($this->get('croquis_id'));

	}

	public function getSitefouille() {

		return new Model_Sitefouille($this->get('sitefouille_id'));

	}

}