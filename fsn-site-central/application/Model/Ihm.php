<?php

class Model_Ihm extends Yab_Db_Table {

	protected $_name = 'ihm';

	public function getProfilIhms() {

		return $this->getTable('Model_Profil_Ihm')->search(array('ihm_id' => $this->get('Id')));

	}

}