<?php

class Model_Log extends Yab_Db_Table {

	protected $_name = 'log';

	public function getListLog($unitesondage_id) {
		$db = $this->getTable()->getAdapter();
		// jfb 2016-06 mantis 318
		$loader = Yab_Loader::getInstance();
		$session = $loader->getSession();
		
		$where = (!is_null($unitesondage_id) && !empty($unitesondage_id)) ? " AND `log`.unitesondage_id = '$unitesondage_id' " : "" ;

		/*
		$requeteur = "requete_" . "unitesondage";
		$requeteurSession = $session->has($requeteur) ? $session->get($requeteur) : null;
		$where .= (!is_null($requeteurSession) && !empty($requeteurSession)) ? " AND unitesondage.id IN (" . $requeteurSession . ") " : "";
		$session[$requeteur] = ''; // la requête étant consommée, on l'efface
		*/

		// jfb 2016-06 mantis 318 Correction de la requête ci-après : WHERE unitesondage.sitefouille_id = "'.$unitesondage_id.'"
		$sql = "SELECT
		id,
		unitesondage_id,
		identification,
		nom,
		description
		FROM `log`
		WHERE 1 $where
		";
		return $db->prepare($sql);
	}

	public function getUslogByIdentification($ident){
		$db = $this->getTable()->getAdapter();
		return $db->prepare("SELECT * FROM `log` WHERE id = '".$ident."';");
	}

	public function getNextSequenceUslogId($unitesondage_id) {
		$db = $this->getTable()->getAdapter();
		$sql = 'SELECT MAX(CAST(compteur AS UNSIGNED)) + 1 AS next_compteur
                FROM (
                SELECT
                    unitesondage.id AS unitesondage_id,
                    unitesondage.nom,
                    SUBSTRING_INDEX(`log`.identification, ".", 1) AS prefixe,
                    SUBSTRING_INDEX(`log`.identification, ".", -1) AS compteur,
                    `log`.id AS usondage_id,
                    `log`.identification
                    FROM `log`
                    INNER JOIN unitesondage ON unitesondage.id = `log`.unitesondage_id
                    WHERE unitesondage_id = "'.$unitesondage_id.'"
                ) AS T';

		$result = $db->prepare($sql)->toRow() ;
		return $result == ""? 1: $result ;
	}

}