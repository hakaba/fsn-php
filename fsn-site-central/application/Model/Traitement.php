<?php

class Model_Traitement extends Yab_Db_Table {

	protected $_name = 'traitement';

	public function getType() {

		return new Model_Type($this->get('type_id'));

	}
	
	public function getElementrecueilli() {
	
		return new Model_Elementrecueilli($this->get('elementrecueilli_id'));
	
	}
	
	public function getEtat() {

		return new Model_Etat($this->get('etat_id'));

	}

	public function getIntervenant() {

		return new Model_Intervenant($this->get('intervenant_id'));

	}

	public function getErTraitements() {

		return $this->getTable('Model_Er_Traitement')->search(array('traitement_id' => $this->get('id')));

	}
	
	public function getListTraitement()
	{
		$db = $this->getTable()->getAdapter();
        
        $sql ="SELECT 
                er.numero,
        		tr.nom,
        		tr.ref_labo,
                tr.er_id,
                tr.id AS traitement_id, 
                tr.debut, 
                tr.fin, 
                tr.lieu, 
                tr.type_id, 
                fct.categorie_value AS traitement_type,
                tr.observation, 
                tr.etat_id,
                fce.categorie_value AS traitement_etat, 
                tr.intervenant_id, 
                inter.prenom AS intervenant_prenom,
                inter.nom AS intervenant_nom                
            FROM traitement tr
            INNER JOIN elementrecueilli er ON er.id = tr.er_id
            INNER JOIN fsn_categorie fct ON fct.id = tr.type_id
            INNER JOIN fsn_categorie fce ON fce.id = tr.etat_id
            LEFT JOIN intervenant inter ON inter.id = er.intervenant_id
            ORDER BY 
                er.numero,
                tr.debut 
        ";
        
        $result = $db->prepare($sql);
        
        return $result ;        

	}
    
	public function getTraitementDetail($sitefouille_id=null) {
		
		$db = $this->getTable()->getAdapter();
		$where = empty($sitefouille_id) ? '' : ' where sf.id = "'.$sitefouille_id.'"';
		
		$sql = 'SELECT
		tr.nom,
        tr.ref_labo,
      	tr.id,
      	tr.debut,
      	tr.fin,
      	tr.lieu,
      	tr.type_id,
      	tr.observation,
        tr.etat_id,
      	tr.intervenant_id,
      	tr.er_id,
		fct.categorie_key AS type_key,
      	fct.categorie_value AS traitement_type,
		fce.categorie_key AS etat_key,
      	fce.categorie_value AS traitement_etat,  
      	concat(inte.prenom, " ", upper(inte.nom) ) AS intervenant_nom_complet,
		er.numero AS numero_ER
      FROM traitement tr
      INNER JOIN fsn_categorie fct ON fct.id = tr.type_id AND fct.categorie_type = "traitement_type"
	  INNER JOIN elementrecueilli er ON er.id = tr.er_id
	  INNER JOIN us ON us.id = er.us_id
	  INNER JOIN sitefouille sf ON sf.id = us.sitefouille_id
      INNER JOIN intervenant inte ON inte.id = tr.intervenant_id 
	  INNER JOIN fsn_categorie fce ON fce.id = tr.etat_id AND fce.categorie_type = "traitement_etat" ';
		
		if (isset($_GET['ER']) or !empty($_SESSION['idER2']))
		{
			unset($tmp);
			$tmp = $_SESSION['idER2'];
			$sql .= 'INNER JOIN elementrecueilli err ON err.id = tr.er_id AND err.id = "'.$tmp.'" ';		
		}	

    	$loader = Yab_Loader::getInstance();
    	$session = $loader->getSession();
		$requeteur = "requete_" . "traitement";
		$requeteurSession = $session->has($requeteur) ? $session->get($requeteur) : null;
		$where .= (!is_null($requeteurSession) && !empty($requeteurSession)) ? " AND tr.id IN (" . $requeteurSession . ") " : "";
		$session[$requeteur] = ''; // la requête étant consommée, on l'efface
		
		$sql .= $where.' ORDER BY tr.nom ';
		$result = $db->prepare($sql);
		return $result ;
	}
	
	public function getNomPrenomByIntervenantId($idIntervenant) {

		$db = $this->getTable()->getAdapter();
		$sql = $db->prepare('SELECT *, id, CONCAT_WS(" - ", nom, prenom) AS nom_complet
      			FROM intervenant where id="'.$idIntervenant.'"');
		return $sql;
	}
	
}