<?php

class Model_Couchelog extends Yab_Db_Table {

	protected $_name = 'couche_log';

	public function getListCouches($log_id) {
		$db = $this->getTable()->getAdapter();
		// jfb 2016-06 mantis 318
		$loader = Yab_Loader::getInstance();
		$session = $loader->getSession();

		$where = (!is_null($log_id) && !empty($log_id)) ? " AND couche_log.log_id = '$log_id' " : "" ;

		// jfb 2016-06 mantis 318 Correction de la requête ci-après : WHERE unitesondage.sitefouille_id = "'.$unitesondage_id.'"
		$sql = "SELECT
		couche_log.id,
		couche_log.log_id,
		couche_log.num_sequence,
		couche_log.epaisseur,
		couche_log.couleur,
		couche_log.us_id,
		couche_log.description,
		us.identification as us_nom
		FROM couche_log
		LEFT JOIN us ON couche_log.us_id=us.id
		WHERE 1 $where
		";
		return $db->prepare($sql);
	}

	public function getCouchelogByIdentification($ident){
		$db = $this->getTable()->getAdapter();
		return $db->prepare("SELECT * FROM couche_log WHERE id = '".$ident."';");
	}

	public function getNextSequenceCouchelogId($log_id) {
		$db = $this->getTable()->getAdapter();
		$sql = 'SELECT MAX(CAST(compteur AS UNSIGNED)) + 1 AS next_compteur
                FROM (
                SELECT
                    log.id AS log_id,
                    log.nom,
                    SUBSTRING_INDEX(couche_log.identification, ".", 1) AS prefixe,
                    SUBSTRING_INDEX(couche_log.identification, ".", -1) AS compteur,
                    couche_log.id AS usondage_id,
                    couche_log.identification
                    FROM couche_log
                    INNER JOIN log ON log.id = couche_log.log_id
                    WHERE unitesondage_id = "'.$log_id.'"
                ) AS T';

		$result = $db->prepare($sql)->toRow() ;
		return $result == ""? 1: $result ;
	}

}