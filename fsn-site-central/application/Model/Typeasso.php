<?php

class Model_Typeasso extends Yab_Db_Table {

	protected $_name = 'typeasso';

	public function getAssociationErs() {

		return $this->getTable('Model_Association_Er')->search(array('typeasso_id' => $this->get('id')));

	}

}