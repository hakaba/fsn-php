<?php

class Model_Classification extends Yab_Db_Table {

	protected $_name = 'classification';

	public function getElementpoteries() {

		return $this->getTable('Model_Elementpoterie')->search(array('classification_id' => $this->get('id')));

	}

}