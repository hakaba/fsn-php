<?php

class Model_Naturematiere_Fonction extends Yab_Db_Table {

	protected $_name = 'naturematiere_fonction';

	public function getNaturematiere() {

		return new Model_Naturematiere($this->get('naturematiere_id'));

	}

	public function getFonction() {

		return new Model_Fonction($this->get('fonction_id'));

	}

}