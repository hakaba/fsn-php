<?php

class Model_Fsn_Organisme extends Yab_Db_Table {

	protected $_name = 'fsn_organisme';

	public function getType() {

		return new Model_Type($this->get('type_id'));

	}

	public function getCorrespondant() {

		return new Model_Correspondant($this->get('correspondant_id'));

	}
  
  public function getOrganismeDetail() {
      
    $db = $this->getTable()->getAdapter();

    $sql = 'SELECT 
      	fo.id, 
      	fo.nom, 
      	fo.nomabrege, 
      	fo.type_id,
      	fc.categorie_key,
      	fc.categorie_value AS organisme_type, 
      	fo.adresse1,
        fo.adresse2, 
      	fo.cp, 
      	fo.ville, 
      	fo.pays, 
      	fo.correspondant_id,
        concat(inte.prenom, " ", upper(inte.nom) ) AS correspondant_nom_complet,   
      	inte.prenom AS correspondant_prenom,
      	inte.nom AS correspondant_nom,
      	inte.telephone,
      	inte.email,
      	fo.description
      FROM fsn_organisme fo
      INNER JOIN fsn_categorie fc ON fc.id = fo.type_id AND fc.categorie_type = "organisme_type"
      LEFT JOIN intervenant inte ON inte.id = fo.correspondant_id ';
    
    $result = $db->prepare($sql);
    
    $loader = Yab_Loader::getInstance();
    $session = $loader->getSession();
    $requeteur = "requete_" . "fsn_organisme";
    $requeteurSession = $session->has($requeteur) ? $session->get($requeteur) : null;
    if (!is_null($requeteurSession) && !empty($requeteurSession)) {
    	$result = $result->where("fo.id IN (" . $requeteurSession . ") " ) ;
    }
    $session[$requeteur] = ''; // la requête étant consommée, on l'efface
    
    return $result ;
  }

}