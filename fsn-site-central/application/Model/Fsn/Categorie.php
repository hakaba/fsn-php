<?php

class Model_Fsn_Categorie extends Yab_Db_Table {

	protected $_name = 'fsn_categorie';
  
  public function getTermeThesaurus(){
  
    $db = $this->getTable()->getAdapter();
    $loader = Yab_Loader::getInstance();
    $session = $loader->getSession();
    
    $sql = 'SELECT categorie_value AS phrase
      FROM fsn_categorie
      WHERE categorie_type = "fai_terme"
    ';
    
    $result = $db->prepare($sql);
    
    return $result ;    
  
  }
  
  public function getMaxOrdre($type) {
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();
		
		$sql = "SELECT max(ordre) maxordre 
				FROM fsn_categorie 
				WHERE categorie_type='" . $type . "'
        ORDER BY visible desc, ordre";
		
		$result = $db->prepare ( $sql );
		$result = $result->toArray ();
		$maxordre = $result [0] ["maxordre"];
		
		if (empty ( $maxordre ))
			$maxordre = 0;
		
		return $maxordre;
	}
	
  public function getListByType($type) {
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();
		
		$sql = 'SELECT *
        FROM fsn_categorie
        WHERE categorie_type = "' . $type . '"
				GROUP BY categorie_value
        ORDER BY visible desc, ordre
    ';
		
		$result = $db->prepare ( $sql ) ;
		
		return $result;
	}
	
  public function getListReference() {
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();
		
		$sql = 'SELECT 
        			categorie_value AS nom, 
        			categorie_key AS reference
      			FROM fsn_categorie
      			WHERE categorie_type = "categorie_type"
			      GROUP BY categorie_key
				ORDER BY visible desc, ordre;
    			';
		
		$result = $db->prepare ( $sql );
		
		return $result;
	}
  
  	
	public function isExiste($type, $key) {
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();
	
		$sql = "SELECT id, categorie_value
				FROM fsn_categorie
				WHERE categorie_type='" . $type . "'
        AND categorie_key = '" . $key . "' ;" ;
	
		$result = $db->prepare ( $sql );
		return $result->toArray ();
	}
	
	public function referenceExiste($type, $value) {
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();
	
		$sql = "SELECT id, categorie_value
				FROM fsn_categorie
				WHERE categorie_type='" . $type . "'
        AND categorie_value = '" . $value . "' ;" ;
	
		$result = $db->prepare ( $sql );
		return $result->toArray ();
	}

	public function getRelations() {
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();
	
		$sql = "SELECT
        	fcr.id,
        	fcr.element_a,
        	fca.categorie_type AS element_a_type,
        	fca.categorie_key AS element_a_key,
        	fca.categorie_value AS element_a_value,
        	fcr.type_relation,
        	-- fcr.type_relation,
        	fctr.categorie_key AS type_relation_key,
        	fctr.categorie_value AS type_relation_value,
        	fcr.element_b,
        	fcb.categorie_type AS element_b_type,
        	fcb.categorie_key AS element_b_key,
          fcb.categorie_value AS element_b_value,
        	fcr.visible,
        	fcr.ordre,
        	fcr.description
        FROM fsn_categorie_relation fcr
        INNER JOIN fsn_categorie fctr ON fctr.id = fcr.type_relation
        INNER JOIN fsn_categorie fca ON fca.id = fcr.element_a
        INNER JOIN fsn_categorie fcb ON fcb.id = fcr.element_b
        ORDER BY fcr.visible desc, fcr.ordre    
			" ;
	
		$result = $db->prepare ( $sql );
		return $result ;
	}

	public function getTypesRelations($relations) {
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();
		$types = array() ;
		foreach ($relations as $relation) {
			$sql = "SELECT fsr.type_relation
				FROM fsn_categorie as fsc, fsn_categorie_relation as fsr
				WHERE fsc.id = fsr.element_b
					AND fsr.element_b = '" . $relation["element_b"] . "';" ;
			
			$result = $db->prepare ( $sql );
			$type_relation_result = $result->toArray() ;
			$types[$relation["element_b"]] = $type_relation_result[0]["type_relation"];
		}
		return $types ;
	}
	
	public function getTypesRelationsAll($element_b) {
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();
	
		$sql = "SELECT 
      	DISTINCT(fcr.type_relation), 
      	fcr.type_relation ,
      	fctr.categorie_key AS type_relation_key,
      	fctr.categorie_value AS type_relation_value,
        fcr.id,
      	fcr.element_a, 
      	fca.categorie_type AS categorie_type_a,
      	fca.categorie_key AS categorie_key_a,      	
      	fca.categorie_value AS categorie_value_a,
      	fcr.element_b, 
      	fcb.categorie_type AS categorie_type_b,
      	fcb.categorie_key AS categorie_key_b ,
      	fcb.categorie_value AS categorie_value_b ,
      	fcr.visible, 
      	fcr.ordre, 
      	fcr.description
      FROM fsn_categorie_relation fcr
      INNER JOIN fsn_categorie fctr ON fctr.id = fcr.type_relation
      INNER JOIN fsn_categorie fca ON fca.id = fcr.element_a 
      INNER JOIN fsn_categorie fcb ON fcb.id = fcr.element_b
        AND fcb.categorie_key = (
      		SELECT fc.categorie_type
      		FROM fsn_categorie fc
      		WHERE fc.id = '".$element_b."'	 
      	)         
      WHERE fcr.visible = 1     
    ";
	
		$result = $db->prepare ( $sql );
	
		return $result;
	}

	public function getTypeById($element_b) {
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();
	
		$sql = "
				SELECT
  				fsc.id,
  				fsc.categorie_type
				FROM fsn_categorie_relation as fsr
				INNER JOIN fsn_categorie fsc ON fsc.id = fsr.element_a
				WHERE fsr.element_b = '" . $element_b . "'
				ORDER BY fsr.visible desc, fsr.ordre
			" ;	
		
		$result = $db->prepare ( $sql );
		$result = $result->toArray ();
		
		if (isset($result[0]))
			$categorie_type = $result[0]["categorie_type"] ;
		else
			$categorie_type = "" ; 
		
		return $categorie_type ;
	}
	
	public function getOrdreRMax($element_b, $element_a_type) {
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();
	
		$sql = "SELECT MAX(fsr.ordre) ordreMax
			FROM fsn_categorie_relation as fsr, fsn_categorie fsc
			WHERE fsr.element_b = '". $element_b . "'
			AND fsc.id = fsr.element_a
			AND fsc.categorie_type = '". $element_a_type . "'
			" ;
	
		$result = $db->prepare ( $sql );
		$result = $result->toArray ();
		$maxordre = $result [0] ["ordreMax"];
	
		if (empty ( $maxordre ))
			$maxordre = 0;
	
		return $maxordre;
	}
	
	public function getListReferenceHeritee($element_b) {
		
    $db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();
	
		$sql = "
			SELECT   			
  			DISTINCT(fcr.element_a ),
  			fcr.id,
        fca.categorie_type,
  			fca.categorie_key AS reference,
        fca.categorie_value AS nom,
  			fcr.type_relation,
  			fcr.type_relation,
  			-- fctr.categorie_key ,
  			-- fctr.categorie_value,
  			fcr.element_b,
  			fcb.categorie_type,
  			fcb.categorie_key,
  			fcr.visible,
  			fcr.ordre,
  			fcr.description
			FROM fsn_categorie_relation fcr
			INNER JOIN fsn_categorie fca ON fca.id = fcr.element_a
			INNER JOIN fsn_categorie fcb ON fcb.id = fcr.element_b AND fcb.categorie_key = (
			SELECT fc.categorie_type
			FROM fsn_categorie fc
			WHERE fc.id = '". $element_b . "'
			)
			-- INNER JOIN fsn_categorie fctr ON fctr.id = fcr.type_relation
			" ;
	
		$result = $db->prepare ( $sql );
	
		return $result;
	}
	
	public function getFaiExtension($type_id){
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();
	
		$sql = "select id, nom, valeur_id, valeur, valeur_key from fai_extension where type_id='".$type_id."'" ;
		
		$result = $db->prepare ( $sql );
		$result = $result->toArray ();
		return $result;
	}
	
}