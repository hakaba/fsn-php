<?php

class Model_Fsn_Admin_Menu extends Yab_Db_Table {

	protected $_name = 'fsn_admin_menu';

	public function getMenu() {

		return new Model_Menu($this->get('menu_id'));

	}

	public function getParentMenu() {

		return new Model_Parent_Menu($this->get('parent_menu_id'));

	}

	public function getHtml() {

		return new Model_Html($this->get('html_id'));

	}

}