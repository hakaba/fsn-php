<?php

class Model_Fsn_Admin_Profil extends Yab_Db_Table {

	protected $_name = 'fsn_admin_profil';

	public function getProfil() {

		return new Model_Profil($this->get('profil_id'));

	}

	public function getUser() {

		return new Model_User($this->get('user_id'));

	}

	public function getGroup() {

		return new Model_Group($this->get('group_id'));

	}

}