<?php

class Model_Fsn_Categorie_Color extends Yab_Db_Table {

	protected $_name = 'fsn_categorie_color';

	public function getCategorie() {

		return new Model_Categorie($this->get('categorie_id'));

	}

}