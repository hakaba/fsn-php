<?php

class Model_Fsn_Categorie_Relation extends Yab_Db_Table {

	protected $_name = 'fsn_categorie_relation';

  
  public function getElementA() {

		return $this->getTable('Model_Fsn_Categorie')->search(array('element_a' => $this->get('id')));

	}

  public function getTypeRelationId() {

		return $this->getTable('Model_Fsn_Categorie')->search(array('type_relation' => $this->get('id')));

	}
  
  public function getElementB() {

		return $this->getTable('Model_Fsn_Categorie')->search(array('element_b' => $this->get('id')));

	}
   
  /*
  public function preSave() {
    // Historisation de la modification
    $formvalue_relation = $this ;
    // $relation_id = $this->get('id' ) ; 
    // $formvalue_relation['id'] = $relation_id ;
    $historisation = new Plugin_Historique() ;
    $historisation->addhistory('Fsn_Categorie_Relation','edit',$formvalue_relation) ;
       
      print '<pre>';
      print_r($this);
      print '</pre>';

	}
  
  public function preDelete() {
    // Historisation de la modification
    $formvalue_relation = $this ;
    $relation_id = $this->get('id' ) ; 
    $formvalue_relation['id'] = $relation_id ;
    $historisation = new Plugin_Historique() ;
    $historisation->addhistory('Fsn_Categorie_Relation','edit',$formvalue_relation) ;
       
      print '<pre>';
      print_r($this);
      print '</pre>';

	}
  */
  
  public function getListRelations(){
  
		$db = $this->getTable()->getAdapter ();
	
		$sql = "SELECT
        	fcr.id,
        	fcr.element_a,
          fca.id AS element_a_id,
          fca.categorie_type AS element_a_type,
        	fca.categorie_key AS element_a_key,
        	fca.categorie_value AS element_a_value,
        	fcr.type_relation_id,
        	fctr.categorie_key AS type_relation,
          fctr.categorie_key AS type_relation_key,
        	fctr.categorie_value AS type_relation_value,
        	fcr.element_b,
        	fcb.id AS element_b_id,
          fcb.categorie_type AS element_b_type,
        	fcb.categorie_key AS element_b_key,
          fcb.categorie_value AS element_b_value,
        	fcr.visible,
        	fcr.ordre,
        	fcr.description
        FROM fsn_categorie_relation fcr
        INNER JOIN fsn_categorie fctr ON fctr.id = fcr.type_relation_id
        INNER JOIN fsn_categorie fca ON fca.id = fcr.element_a
        INNER JOIN fsn_categorie fcb ON fcb.id = fcr.element_b
        ORDER BY fcr.visible desc, fcr.ordre    
			" ;
	
		$result = $db->prepare ( $sql );
		return $result ;
    
	}

  public function getListRacines(){

    $db = $this->getTable()->getAdapter();

    /**
     * Rattache tous les parents "racines" à un pseudo-élément
     * element_b racine
     */              
    $sql = 'SELECT 
      	DISTINCT fcr.element_b AS element_a_id, 
        fcb.categorie_type AS element_a_type,
      	fcb.categorie_key AS element_a_key,	
      	fcb.categorie_value AS element_a_value,
        "depend_de" AS type_relation,
        0 AS element_b_id, 
      	"relation" AS element_b_type,
      	"relation_parent" AS element_b_key,	
      	"Elément Racine" AS element_b_value,
      	fcb.visible AS visible, 
      	1 AS ordre, 
      	"Elément racine des relations" description
      FROM fsn_categorie_relation fcr
      INNER JOIN fsn_categorie fctr ON fctr.id = fcr.type_relation_id  
      INNER JOIN fsn_categorie fcb ON fcb.id = fcr.element_b
      WHERE fcr.element_b NOT IN (
      	SELECT enfant.element_a
      	FROM fsn_categorie_relation enfant
      )
      ORDER BY fcr.element_b ASC 
    ';
        
    $result = $db->prepare($sql);
    
    return $result ;  
  
  }

  public function getMaxOrdre($type) {
  	$db = $this->getTable ()->getAdapter ();
  	$loader = Yab_Loader::getInstance ();
  	$session = $loader->getSession ();
  
  	$sql = "SELECT max(fsc.ordre) maxordre
				FROM fsn_categorie as fsc, fsn_categorie_relation as fsr
				WHERE fsr.type_relation = fsc.id AND fsc.categorie_type = '" . $categorie_type . "'
  			AND element_a = '" . $element_a . "'
        ORDER BY visible desc, ordre";
  
  	$result = $db->prepare ( $sql );
  	$result = $result->toArray ();
  	$maxordre = $result [0] ["maxordre"];
  
  	if (empty ( $maxordre ))
  		$maxordre = 0;
  
  	return $maxordre;
  }

  public function getCategorieTypeRelation($categorie_element_b_id){

    $db = $this->getTable()->getAdapter();
    
    /**
     * Donne tous les types de relation (hierarchique) possible avec la 
     * la categorie d'appartenance avec un element_b parent
     */              
    $sql = 'SELECT 
        fcr.type_relation_id, 
      	-- fcr.type_relation ,
        fctr.categorie_key AS type_relation,
      	fctr.categorie_key AS type_relation_key,
      	fctr.categorie_value AS type_relation_value,
        fcr.id,
      	fcr.element_a, 
      	fca.id AS element_a_id,
        fca.categorie_type AS element_a_type,
      	fca.categorie_key AS element_a_key,
      	fca.categorie_value AS element_a_value,
      	fcr.element_b, 
      	fcb.id AS element_b_id,
        fcb.categorie_type AS element_b_type,
      	fcb.categorie_key AS element_b_key,
        fcb.categorie_value AS element_b_value,
      	fcr.visible, 
      	fcr.ordre, 
      	fcr.description
      FROM fsn_categorie_relation fcr
      INNER JOIN fsn_categorie fctr ON fctr.id = fcr.type_relation_id
      INNER JOIN fsn_categorie fca ON fca.id = fcr.element_a  
      INNER JOIN fsn_categorie fcb ON fcb.id = fcr.element_b AND fcb.categorie_key = (
      	SELECT fc.categorie_type
      	FROM fsn_categorie fc
      	WHERE fc.id = '.$categorie_element_b_id.'	 
      )
        
      ORDER BY fcr.element_b ASC 
    ';
        
    $result = $db->prepare($sql);
    
    return $result ;  
  
  }
  
}

