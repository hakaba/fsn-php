<?php

class Model_Oa extends Yab_Db_Table {

	protected $_name = 'oa';
    
    public function preDelete() {
        
        $oa_id = $this->get('id') ;
        
        // Suppression des Documents associé
        $oa_doc = new Model_Oa_Doc();
        $oa_doc->delete( 'oa_id = "'.$oa_id.'" ' );
    }

	public function getSitefouille() {
		return new Model_Sitefouille($this->get('sitefouille_id'));
	}

	public function getOastatut() {
		return new Model_Oastatut($this->get('oastatut_id'));
	}

	public function getNature() {
		return new Model_Nature($this->get('nature_id'));
	}

	public function getEntiteadmin() {
		return new Model_Entiteadmin($this->get('entiteadmin_id'));
	}

	public function getOaDocs() {
		return $this->getTable('Model_Oa_Doc')->search(array('oa_id' => $this->get('id')));
	}

	public function getParticipations() {
		return $this->getTable('Model_Participation')->search(array('oa_id' => $this->get('id')));
	}

	public function getUsOas() {
		return $this->getTable('Model_Us_Oa')->search(array('oa_id' => $this->get('id')));
	}
  
    public function getOaIntervenantsDetail($oa_id) {
  
    $db = $this->getTable()->getAdapter();
    $loader = Yab_Loader::getInstance();
    
    $sql = 'SELECT 
    		oa.id AS oa_id, 
    		oa.sitefouille_id, 
    		oa.identification, 
    		oa.arretedesignation, 
    		oa.arreteprescription, 
    		oa.debut AS oa_debut, 
    		oa.fin AS oa_fin, 
    		oa.oastatut_id, 
    		oas.oastatut,
    		oa.nature_id,
    		nat.nature, 
    		oa.conditionfouille, 
    		oa.organismeratachement, 
    		oa.maitriseouvrage, 
    		oa.surfacefouille, 
    		oa.commentaire,
    		CASE 
      		WHEN inte.id IS NULL THEN "NA"
      		ELSE inte.id 
      	END AS intervenant_id, 
      	inte.titre, 
      	inte.nom, 
      	inte.prenom, 
      	inte.adresse, 
      	inte.cp, 
      	inte.ville, 
      	inte.telephone, 
      	inte.email, 
      	inte.organisme_id, 
      	inte.commentaire, 
      	inte.metier_id,
      	part.id AS participation_id,
      	CASE 
      		WHEN part.debut IS NULL THEN "0000-00-00"
      		ELSE part.debut 
      	END AS part_debut, 
        CASE 
      		WHEN part.fin IS NULL THEN "0000-00-00"
      		ELSE part.fin
      	END AS part_fin,
      	part.role_id,
      	role.role
      	
      FROM oa 
      INNER JOIN sitefouille sdf ON sdf.id = oa.sitefouille_id
      INNER JOIN oastatut oas ON oas.id = oa.oastatut_id
      INNER JOIN nature nat ON nat.id = oa.nature_id
      INNER JOIN participation part ON part.oa_id = oa.id
      INNER JOIN intervenant inte ON inte.id = part.intervenant_id
      INNER JOIN role ON role.id = part.role_id
        WHERE oa.id = "'.$oa_id.'"';

    return $db->prepare($sql);

  }
    public function getOaDetail() {
  
    $db = $this->getTable()->getAdapter();
    
    $sql = 'SELECT
                oa.id AS id,
                oa.id AS oa_id, 
                oa.sitefouille_id,
                sdf.nom AS sdf_nom,
                sdf.nomabrege AS sdf_nomabrege, 
                oa.identification, 
                oa.arretedesignation, 
                oa.arreteprescription, 
                oa.debut AS oa_debut, 
                oa.fin AS oa_fin, 
                oa.oastatut_id,
                oa.conditionfouille, 
                oa.organismeratachement, 
                oa.maitriseouvrage, 
                oa.surfacefouille, 
                oa.commentaire,
                oa.entiteadmin_id,
                fo.nom
          FROM oa 
          INNER JOIN sitefouille sdf ON sdf.id = oa.sitefouille_id
          INNER JOIN fsn_entiteadmin fe ON fe.id = oa.entiteadmin_id
          INNER JOIN fsn_organisme fo ON fo.id = fe.organisme_id
    ';
    
    $result = $db->prepare($sql);
    
    $loader = Yab_Loader::getInstance();
    $session = $loader->getSession();
    
    $sitefouille_id = $session->has('sitefouille_id') ? $session->get('sitefouille_id') : null ;
    if (!empty($sitefouille_id) ) { $result = $result->where('sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
    
    $oa_id = $session->has('oa_id')? $session->get('oa_id') : '';
    if (!empty($oa_id) ) { $result = $result->where('oa.id = "'.$oa_id.'" ' ) ;  } 
    
    $requeteur = "requete_" . "oa";
    $requeteurSession = $session->has($requeteur) ? $session->get($requeteur) : null;
    if (!is_null($requeteurSession) && !empty($requeteurSession)) {
    	$result = $result->where("oa.id IN (" . $requeteurSession . ") " ) ;
    }
    $session[$requeteur] = ''; // la requête étant consommée, on l'efface
    
    return $result ;
  }
  
	public function getSitefouilleByIdSite($site) {

		$db = $this->getTable()->getAdapter();
		$nom_complet = $db->prepare('SELECT *, id, CONCAT_WS(" - ", nom, nomabrege) AS nom_nomabrege
      								FROM sitefouille where id="'.$site.'"');
		return $nom_complet;

	}
}