<?php

class Model_Sitefouille extends Yab_Db_Table {

	protected $_name = 'sitefouille';

	public function getOas() {

		return $this->getTable('Model_Oa')->search(array('sitefouille_id' => $this->get('id')));

	}

	public function getTopoSfs() {

		return $this->getTable('Model_Topo_Sf')->search(array('sitefouille_id' => $this->get('id')));

	}

	public function getUses() {

		return $this->getTable('Model_Us')->search(array('sitefouille_id' => $this->get('id')));

	}

	public function getVisibleSitefouilles($entiteadmin_id = null){
		if(!$entiteadmin_id)
			$entiteadmin_id = Yab_Loader::getInstance()->getSession()->get('session')['entiteadmin_id'];
		$db = $this->getTable()->getAdapter();
		$sql = "SELECT *,CONCAT_WS(\" - \", nomabrege , nom) AS nom_nomabrege FROM sitefouille WHERE fsn_entiteadmin_id = '".$entiteadmin_id."' ORDER BY nom_nomabrege";
		$result = $db->prepare($sql);
		return $result ;
	}
	
	public function getEntiteOrganisme(){
		
		$db = $this->getTable()->getAdapter();
		$sql = "SELECT orga.nomabrege nomabrege_organisme
				FROM fsn_entiteadmin enti
				INNER JOIN fsn_organisme orga on orga.id = enti.organisme_id
			";
				
		$result = $db->prepare($sql);
		return $result ;
	}

}