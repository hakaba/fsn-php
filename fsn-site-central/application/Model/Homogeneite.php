<?php

class Model_Homogeneite extends Yab_Db_Table {

	protected $_name = 'homogeneite';

	public function getUses() {

		return $this->getTable('Model_Us')->search(array('homogeneite_id' => $this->get('id')));

	}

}