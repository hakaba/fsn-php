<?php

class Model_Interfait extends Yab_Db_Table {

	protected $_name = 'interfait';

	public function getTypeinterfait() {

		return new Model_Typeinterfait($this->get('typeinterfait_id'));

	}

	public function getFai() {

		return new Model_Fai($this->get('fai_id'));

	}
	
	public function getFai_fai() {

		$db = $this->getTable()->getAdapter();
		$loader = Yab_Loader::getInstance();
			$sql = 'SELECT 
				faifai.fai_id,
				faifai.fai_id_1,
				ffai.identification AS fai_1_identification,
				lfai.identification AS fai_identification,
				faifai.incertitude,
				faifai.typeinterfait_id,
				faifai.precisions
				FROM interfait faifai
				INNER JOIN fai ffai ON ffai.id = faifai.fai_id_1
				INNER JOIN fai lfai ON lfai.id = faifai.fai_id
				ORDER BY fai_1_identification, fai_identification
			';

			$result = $db->prepare($sql);
			return $result ;  
	}
	
	public function deleteRelations($fai_id){
		$this->delete( "fai_id = '".$fai_id."' " );
	}
	public function deleteLastRelations($fai_id){
		$this->delete( "fai_id_1 = '".$fai_id."' " );
	}
	
	// Récuperer les categories type pour fai
	public function getOnlyFaiType(){
		
		$db = $this->getTable()->getAdapter();
		$sql = "SELECT *
				FROM fsn_categorie
				WHERE categorie_type in (SELECT categorie_key FROM fsn_categorie WHERE categorie_type = 'fai-typefai_id' ) and visible = 1
			";
				
		$result = $db->prepare($sql);
		return $result ;
	}
}