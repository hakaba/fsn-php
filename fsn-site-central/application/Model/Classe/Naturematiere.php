<?php

class Model_Classe_Naturematiere extends Yab_Db_Table {

	protected $_name = 'classe_naturematiere';

	public function getClasse() {

		return new Model_Classe($this->get('classe_id'));

	}

	public function getNaturematiere() {

		return new Model_Naturematiere($this->get('naturematiere_id'));

	}

}