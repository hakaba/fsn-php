<?php

class Model_Classe_Fonction extends Yab_Db_Table {

	protected $_name = 'classe_fonction';

	public function getClasse() {

		return new Model_Classe($this->get('classe_id'));

	}

	public function getFonction() {

		return new Model_Fonction($this->get('fonction_id'));

	}

}