<?php

class Model_Fonction extends Yab_Db_Table {

	protected $_name = 'fonction';

	public function getClasseFonctions() {

		return $this->getTable('Model_Classe_Fonction')->search(array('fonction_id' => $this->get('id')));

	}

	public function getElementrecueillis() {

		return $this->getTable('Model_Elementrecueilli')->search(array('fonction_id' => $this->get('id')));

	}

	public function getNaturematiereFonctions() {

		return $this->getTable('Model_Naturematiere_Fonction')->search(array('fonction_id' => $this->get('id')));

	}

}