<?php

class Model_Synchrotype extends Yab_Db_Table {

	protected $_name = 'synchrotype';

	public function getSynchronisations() {

		return $this->getTable('Model_Synchronisation')->search(array('synchrotype_id' => $this->get('id')));

	}

	public function getVisibleSynchrotype(){
		$db = $this->getTable()->getAdapter();
		$loader = Yab_Loader::getInstance();

		$sql = 'SELECT * FROM synchrotype WHERE visible = 1';

		$result = $db->prepare($sql);

		return $result ;
	}

}