<?php

class Form_Metier extends Yab_Form {

	public function __construct(Model_Metier $metier) {

		$this->set('method', 'post')->set('name', 'form_metier')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('metier', array(
			'type' => 'text',
			'id' => 'metier',
			'label' => 'metier',
			'value' => $metier->has('metier') ? $metier->get('metier') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('visible', array(
			'type' => 'text',
			'id' => 'visible',
			'label' => 'visible',
			'value' => $metier->has('visible') ? $metier->get('visible') : '1',
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

	}

}