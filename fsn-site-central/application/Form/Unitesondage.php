<?php

class Form_Unitesondage extends Yab_Form {

	public function __construct(Model_Unitesondage $unitesondage, $mode = 'add') {
		$this->set('method', 'post')->set('name', 'form_unitesondage')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');
		
		$loader = Yab_Loader::getInstance();
		$registry = $loader -> getRegistry();
		$session = $loader ->getSession();

		// appel fichier internationalisation
		$i18n = $registry -> get('i18n');
		$sf = new Model_Sitefouille($unitesondage->has('sitefouille_id') ? $unitesondage->get('sitefouille_id') : $session->get('sitefouille_id'));
		$filter_no_html = new Yab_Filter_NoHtml();

		$this->setElement('sitefouille_id', array(
				'type' => 'hidden',
				'id' => 'sitefouille_id',
				'label' => $filter_no_html->filter( $i18n -> say('sitefouille_id') ),
				'placeholder' => $filter_no_html->filter( $i18n -> say('sitefouille_id') ),
				'tooltip' => $filter_no_html->filter( $i18n -> say('sitefouille_tooltip') ),
				'value' => $sf->get('id'),
				'intitule' => $sf->get('nomabrege')." - ".$sf->get('nom'),
				'readonly' => true,
				'errors' => array(),
		));
		
		$value_identifiant = $unitesondage->has('identification') ? $unitesondage->get('identification') : $sf->get('nomabrege').'.'.$unitesondage->getNextSequenceUsondageId($sf->get('id'));
		$exp_ident = explode(".",$value_identifiant);
		$prefix_identidiant = isset($exp_ident[0])?$exp_ident[0]:null;
		$numero_usondage = isset($exp_ident[1])?$exp_ident[1]:null;
		
		$this->setElement('id', array(
				'type' => 'hidden',
				'id' => 'id_usondage',
				'value' => $unitesondage->has('id') ? $unitesondage->get('id') : null,
				'readonly' => true,
				'validators' => array(),
				'errors' => array()
		));
		
		$this->setElement('identification', array(
				'type' => 'hidden',
				'id' => 'identification',
				'prefix' => $prefix_identidiant,
				'label' => $i18n->say('identification'),
				'placeholder' => $i18n->say('identification'),
				'tooltip' => $filter_no_html->filter( $i18n -> say('unitesondage_tooltip') ),
				'value' => $unitesondage->has('identification') ? $unitesondage->get('identification') : $prefix_identidiant.'.'.$numero_usondage,
				'numero' => $numero_usondage,
				'validators' => array('NotEmpty'),
				'errors' => array('NotEmpty' => array('Le champ identification ne doit pas être vide') ),
		));
		
		$this->setElement('nom', array(
				'type' => 'text',
				'id' => 'nom',
				'label' => $filter_no_html->filter( $i18n -> say('unitesondage_nom') ),
				'placeholder' => $filter_no_html->filter( $i18n -> say('unitesondage_nom') ),
				'tooltip' => $filter_no_html->filter( $i18n -> say('unitesondage_tooltip') ),
				'value' => $unitesondage->has('nom') ? $unitesondage->get('nom') : null,
				'validators' => array('NotEmpty'),
				'errors' => array('Veuillez indiquer un nom'),
		));

		/*
		$this->setElement('numerosite', array(
				'type' => 'text',
				'id' => 'numerosite',
				'label' => $filter_no_html->filter( $i18n -> say('numerosite') ),
				'placeholder' => $filter_no_html->filter( $i18n -> say('numerosite') ),
				'value' => $unitesondage->has('numerosite') ? $unitesondage->get('numerosite') : null,
				'maxlength' => 11,
				'validators' => array('NotEmpty'),
				'errors' => array(),
		));*/
		
		$this->setElement('type', array(
				'type' => 'select',
				'id' => 'type_unitesondage',
				'label' => $filter_no_html->filter( $i18n -> say('type') ),
				'placeholder' => $filter_no_html->filter( $i18n -> say('type') ),
				'value' => $unitesondage->has('type') ? $unitesondage->get('type') : null,
				'options' => $unitesondage->getTable('Model_Typeunitesondage')
					->fetchAll()->setKey('categorie_value')->setValue('categorie_value'),
				'validators' => array(),
				'errors' => array(),
		));
		
		$this->setElement('numero_tranchee', array(
				'type' => 'select',
				'id' => 'numero_tranchee',
				'label' => $filter_no_html->filter( $i18n -> say('numero_tranchee') ),
				'placeholder' => $filter_no_html->filter( $i18n -> say('numero_tranchee') ),
				'value' => $unitesondage->has('numero_tranchee') ? $unitesondage->get('numero_tranchee') : null,
				'options' => $unitesondage->getTable('unitesondage')->fetchAll()->where("type='tranchee'")->setKey('id')->setValue('identification'),
				'validators' => array('NotEmpty'),
				'errors' => array(),
		));

		$this->setElement('description', array(
				'type' => 'textarea',
				'rows' => '4',
				'id' => 'description',
				'label' => $filter_no_html->filter( $i18n -> say('description') ),
				'placeholder' => $filter_no_html->filter( $i18n -> say('description') ),
				'value' => $unitesondage->has('description') ? $unitesondage->get('description') : null,
				'style' => 'resize:none',
				'validators' => array(),
				'errors' => array(),
		));
		
		$this->setElement('largeur', array(
				'type' => 'text',
				'id' => 'largeur',
				'label' => $filter_no_html->filter( $i18n -> say('largeur') ),
				'tooltip' => $filter_no_html->filter( $i18n -> say('largeur') ),
				'placeholder' => $filter_no_html->filter( $i18n -> say('largeur') ),
				'value' => $unitesondage->has('largeur') ? $unitesondage->get('largeur') : null,
				'maxlength' => 4,
				'validators' => array('NotEmpty'),
				'errors' => array(),
		));
		
		$this->setElement('longueur', array(
				'type' => 'text',
				'id' => 'longueur',
				'label' => $filter_no_html->filter( $i18n -> say('longueur') ),
				'tooltip' => $filter_no_html->filter( $i18n -> say('longueur') ),
				'placeholder' => $filter_no_html->filter( $i18n -> say('longueur') ),
				'value' => $unitesondage->has('longueur') ? $unitesondage->get('longueur') : null,
				'maxlength' => 4,
				'validators' => array('NotEmpty'),
				'errors' => array(),
		));
		
		$this->setElement('profondeur', array(
				'type' => 'text',
				'id' => 'profondeur',
				'label' => $filter_no_html->filter( $i18n -> say('profondeur') ),
				'tooltip' => $filter_no_html->filter( $i18n -> say('profondeur') ),
				'placeholder' => $filter_no_html->filter( $i18n -> say('profondeur') ),
				'value' => $unitesondage->has('profondeur') ? $unitesondage->get('profondeur') : null,
				'maxlength' => 4,
				'validators' => array('NotEmpty'),
				'errors' => array(),
		));
		
		$this->setElement('date_debut', array(
				'type' => 'text',
				'id' => 'date_debut',
				'label' => $filter_no_html->filter( $i18n -> say('debut_sondage') ),
				'placeholder' => '01/01/1900',
				'class' => 'form-control datepicker',
				'tooltip' => $filter_no_html->filter( $i18n -> say('date_debut_tooltip') ),
				'value' => $unitesondage->has('date_debut') ? $unitesondage->get('date_debut') : '01/01/1900',
				'validators' => array('Date'=>array('format' => 'dd/mm/yyyy')),
				'errors' => array(),
		));
		
		$this->setElement('date_fin', array(
				'type' => 'text',
				'id' => 'date_fin',
				'label' => $filter_no_html->filter( $i18n -> say('fin_sondage') ),
				'placeholder' => '01/01/2016',
				'class' => 'form-control datepicker',
				'tooltip' => $filter_no_html->filter( $i18n -> say('date_fin_tooltip') ),
				'value' => $unitesondage->has('date_fin') ? $unitesondage->get('date_fin') : '01/01/2016',
				'validators' => array('Date'=>array('format' => 'dd/mm/yyyy')),
				'errors' => array(),
		));

	}

}