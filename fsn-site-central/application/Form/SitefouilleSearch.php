<?php

class Form_SitefouilleSearch extends Yab_Form {

	public function __construct(Model_Sitefouille $sitefouille) {
            
        $loader = Yab_Loader::getInstance() ;    
        $registry = Yab_Loader::getInstance() -> getRegistry();
        $session = Yab_Loader::getInstance() ->getSession();
        $sitefouille_id = $session->has('sitefouille_id') ? $session->get('sitefouille_id') : null ; // jfb 2016-06-22 mantis 318
        
        // appel fichier internationalisation
        $i18n = $registry -> get('i18n');
        $filter_no_html = new Yab_Filter_NoHtml();

		$this->set('method', 'post')->set('name', 'form_sitefouille_search')->set('action', $loader->getRequest('Sitefouille','Select' ) )->set('role', 'form');

		$this->setElement('sitefouille_id', array(
			'type' => 'select',
			'id' => 'sitefouille_id',
			'title' => $sitefouille_id,
			'placeholder' => $filter_no_html->filter( $i18n -> say('make_a_choice').' le '.$i18n -> say('sitefouille')),
			'tooltip' => $filter_no_html->filter( $i18n -> say('make_a_choice').' le '.$i18n -> say('sitefouille_tooltip')),
			'onchange' => 'submit(); ',
			'value' => $session->has('sitefouille_id') ? $session->get('sitefouille_id') : null,
			'fake_options' => array(''=>$filter_no_html->filter( $i18n -> say('make_a_choice').' le '.$i18n -> say('sitefouille')),),
			'options' => $sitefouille->getTable('Model_Sitefouille')->fetchAll()->setKey('id')->setValue('nom'),
			'errors' => array(),
		));

	}

}