<?php

class Form_Fai_Extension extends Yab_Form {

	public function __construct($faiExtension) {
		
		// appel fichier internationalisation
		$registry = Yab_Loader::getInstance() -> getRegistry();
		$i18n = $registry -> get('i18n');
		$filter_no_html = new Yab_Filter_NoHtml();
		
		$this->set('method', 'post')->set('action', '')->set('role', 'form');
		
		$this->setElement('fai_id', array(
			'type' => 'select',
			'id' => 'fai_id',
			'label' => 'fai_id',
			'value' => $fai_us->has('fai_id') ? $fai_us->get('fai_id') : null,
			'fake_options' => array(),
			'options' => $fai_us->getTable('Model_Fai')->fetchAll()->setKey('id')->setValue('identification'),
			'errors' => array(),
		));
		
		$loader = Yab_Loader::getInstance();
		// $us = $fai_us->getTable('Model_Us')->getAllUsFromSf($loader->getSession()->get('sitefouille_id'))->setKey('id')->setValue('identification_desc');
		$us = $fai_us->getTable('Model_Us')->getAllUsFromSf($loader->getSession()->get('sitefouille_id'))->setKey('id')->setValue('identification');
										
		$this->setElement('us_id', array(
			'type' => 'select',
			'id' => 'us_id',
			'label' => $filter_no_html->filter( $i18n -> say('us_id') ),
			'value' => $fai_us->has('us_id') ? $fai_us->get('us_id') : null,
			'fake_options' => array(),
			'options' => $us,
			'errors' => array(),
		));

		$this->setElement('incertitude', array(
			'type' => 'text',
			'id' => 'incertitude',
			'label' => $filter_no_html->filter( $i18n -> say('incertitude') ),
			'value' => $fai_us->has('incertitude') ? $fai_us->get('incertitude') : null,
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

	}

}
