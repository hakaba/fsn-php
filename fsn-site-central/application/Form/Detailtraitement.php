<?php

class Form_Detailtraitement extends Yab_Form {

	public function __construct(Model_Traitement $traitement, $mode='add') {

		$this->set('method', 'post')->set('name', 'form_detailtraitement')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');
    
    $loader = Yab_Loader::getInstance();
    $session = $loader->getSession();
    $sitefouille_id = $session->has('sitefouille_id') ? $session->get('sitefouille_id') : '' ;
     
    //Appel a la connexion BDD
    $registry = Yab_Loader::getInstance()->getRegistry(); 
    $db = Yab_Loader::getInstance()->getRegistry()->get('db');
    
    // Liste des intervenants
    $intervenant_tmp = $traitement->get('intervenant_id');
    $liste_intervenants = $db->prepare('SELECT 
      	id, 
      	CONCAT_WS(" ", nom, prenom) AS nom_complet 
      FROM intervenant
      WHERE id="'.$intervenant_tmp.'"
    ');
    
    $type_tmp=$traitement->get('type_id');
    
    $type_id_nom = $db->prepare('SELECT 
    		id,
      	categorie_key   
      FROM fsn_categorie
      WHERE id= "'.$type_tmp.'" 
    ');
  
    $etat_tmp=$traitement->get('etat_id');
    
    $etat_id_nom = $db->prepare('SELECT
    		id,
      	categorie_key
      FROM fsn_categorie
      WHERE id= "'.$etat_tmp.'"
    ');
    
    
   if (isset($tmp))
		{
			unset($tmp);
			$tmp = $_SESSION['idER2'];
			
		}
		else
		$tmp = $_SESSION['idER2'];
		
    $liste_elementrecueilli = $db->prepare('SELECT
      	id,numero
      FROM elementrecueilli
      WHERE id= "'.$tmp.'" 
    ');
       
  	  $this->setElement('er_id', array(
    		'type' => 'select',
    		'id' => 'elementrecueilli_id',
    		'label' => 'Element recueilli',
  	  		'readonly' =>'true',
     		'value' => $traitement->has('elementrecueilli_id') ? $traitement->get('elementrecueilli_id') : null,
//    		'fake_options' => array('' => '-- Choix Element recueilli' ),
     		'options' => $liste_elementrecueilli->setKey('id')->setValue('numero'),
    		'errors' => array(),
  	  		'class'=>'color select_style',
 	   ));
  	  $this->setElement('type_id', array(
  	  		'type' => 'select',
  	  		'id' => 'type_id',
  	  		'label' => 'Type',
  	  		'class'=>'color select_style',
  	  		'value' => $traitement->has('type_id') ? $traitement->get('type_id') : null,
      //'fake_options' => array(''=> ' -- Choisir le Type'),
      		'fake options' => array(),
  	  		'options' => $type_id_nom->setKey('id')->setValue('categorie_key'),
			//'options' => $traitement->getTable('Model_Fsn_Categorie')->fetchAll()->where("categorie_type = 'traitement_type' ")->setKey('id')->setValue('categorie_value'),
  	  		'readonly' =>'true',
  	  		'errors' => array(),
  	  ));
		$this->setElement('debut', array(
			'type' => 'text',
			'id' => 'debut',
			'readonly' =>'true',
			'class' => 'datepicker',
			'label' => 'D&eacutebut',
			'class' => 'datepicker',
			'value' => ($traitement->has('debut') and $traitement->get("debut")!="0000-00-00") ? date('d/m/Y', strtotime($traitement->get('debut'))): '' ,
			'validators' => array(),
			'errors' => array(),
			'class'=>'color select_style',
		));

		$this->setElement('fin', array(
			'type' => 'text',
			'id' => 'fin',
			'readonly' =>'true',
			'label' => 'Fin',
			'class' => 'datepicker',
				'value' => ($traitement->has('fin') and $traitement->get("fin")!="0000-00-00") ? date('d/m/Y', strtotime($traitement->get('fin'))) : '' ,
			'validators' => array(),
			'errors' => array(),
			'class'=>'color select_style',
		));

		$this->setElement('lieu', array(
			'type' => 'text',
			'id' => 'lieu',
			'readonly' =>'true',
			'label' => 'Lieu',
			'value' => $traitement->has('lieu') ? $traitement->get('lieu') : null,
			'validators' => array(),
			'errors' => array(),
			'class'=>'color select_style',
		));

		$this->setElement('observation', array(
			'type' => 'textarea',
      'rows' => '3',
			'id' => 'observation',
			'readonly' =>'true',
			'label' => 'Observation',
			'value' => $traitement->has('observation') ? $traitement->get('observation') : null,
			'validators' => array(),
			'errors' => array(),
			'class'=>'color select_style',
		));

		$this->setElement('etat_id', array(
			'type' => 'select',
			'id' => 'etat_id',
			'readonly' =>'true',
			'label' => 'Etat',
			'value' => $traitement->has('etat_id') ? $traitement->get('etat_id') : null,
     		 'fake_options' => array(),
			'options' => $etat_id_nom->setKey('id')->setValue('categorie_key'),
			'errors' => array(),
			'class'=>'color select_style',
		));
		
		
		$this->setElement('intervenant_id', array(
			'type' => 'select',
			'readonly' =>'true',
			'id' => 'intervenant_id',
			'label' => 'Intervenant',
			'value' => $traitement->has('intervenant_id') ? $traitement->get('intervenant_id') : null,
			'fake_options' => array(),
      		'options' => $liste_intervenants->setKey('id')->setValue('nom_complet'),
			//'options' => $traitement->getTable('Model_Intervenant')->fetchAll()->setKey('id')->setValue('titre'),
			'errors' => array(),
			'class'=>'color select_style',
		));
		
		

	}

}