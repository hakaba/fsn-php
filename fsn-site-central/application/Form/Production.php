<?php

class Form_Production extends Yab_Form {

	public function __construct(Model_Production $production) {

		$this->set('method', 'post')->set('name', 'form_production')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('numero', array(
			'type' => 'text',
			'id' => 'numero',
			'label' => 'numero',
			'value' => $production->has('numero') ? $production->get('numero') : null,
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('production', array(
			'type' => 'text',
			'id' => 'production',
			'label' => 'production',
			'value' => $production->has('production') ? $production->get('production') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('visible', array(
			'type' => 'text',
			'id' => 'visible',
			'label' => 'visible',
			'value' => $production->has('visible') ? $production->get('visible') : '1',
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

	}

}