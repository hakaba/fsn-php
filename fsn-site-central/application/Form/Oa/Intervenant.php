<?php

class Form_Oa_Intervenant extends Yab_Form {

	public function __construct(Model_Oa_Intervenant $oa_intervenant) {

		$this->set('method', 'post')->set('name', 'form_oa_intervenant')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('debut', array(
			'type' => 'text',
			'id' => 'debut',
			'label' => 'debut',
			'value' => $oa_intervenant->has('debut') ? $oa_intervenant->get('debut') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('fin', array(
			'type' => 'text',
			'id' => 'fin',
			'label' => 'fin',
			'value' => $oa_intervenant->has('fin') ? $oa_intervenant->get('fin') : null,
			'validators' => array(),
			'errors' => array(),
		));

	}

}