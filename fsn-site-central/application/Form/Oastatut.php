<?php

class Form_Oastatut extends Yab_Form {

	public function __construct(Model_Oastatut $oastatut) {

		$this->set('method', 'post')->set('name', 'form_oastatut')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('oastatut', array(
			'type' => 'text',
			'id' => 'oastatut',
			'label' => 'oastatut',
			'value' => $oastatut->has('oastatut') ? $oastatut->get('oastatut') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('visible', array(
			'type' => 'text',
			'id' => 'visible',
			'label' => 'visible',
			'value' => $oastatut->has('visible') ? $oastatut->get('visible') : '1',
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

	}

}