<?php

class Form_Fonction extends Yab_Form {

	public function __construct(Model_Fonction $fonction) {

		$this->set('method', 'post')->set('name', 'form_fonction')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('fonction', array(
			'type' => 'text',
			'id' => 'fonction',
			'label' => 'fonction',
			'value' => $fonction->has('fonction') ? $fonction->get('fonction') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('precision', array(
			'type' => 'text',
			'id' => 'precision',
			'label' => 'precision',
			'value' => $fonction->has('precision') ? $fonction->get('precision') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('visible', array(
			'type' => 'text',
			'id' => 'visible',
			'label' => 'visible',
			'value' => $fonction->has('visible') ? $fonction->get('visible') : '1',
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

	}

}