<?php

class Form_Contexte extends Yab_Form {

	public function __construct(Model_Contexte $contexte) {

		$this->set('method', 'post')->set('name', 'form_contexte')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('contexte', array(
			'type' => 'text',
			'id' => 'contexte',
			'label' => 'contexte',
			'value' => $contexte->has('contexte') ? $contexte->get('contexte') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('visible', array(
			'type' => 'text',
			'id' => 'visible',
			'label' => 'visible',
			'value' => $contexte->has('visible') ? $contexte->get('visible') : '1',
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

	}

}