<?php

class Form_Log extends Yab_Form {

	public function __construct(Model_Log $log, $mode = 'add') {
		$this->set('method', 'post')->set('name', 'form_log')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$loader = Yab_Loader::getInstance() ;
		$registry = Yab_Loader::getInstance() -> getRegistry();
		$session = Yab_Loader::getInstance() ->getSession();

		// appel fichier internationalisation
		$i18n = $registry -> get('i18n');
		$us = new Model_Unitesondage($log->has('unitesondage_id') ? $log->get('unitesondage_id') : $session->get('unitesondage_id'));
		$filter_no_html = new Yab_Filter_NoHtml();

		$this->setElement('unitesondage_id', array(
				'type' => 'hidden',
				'id' => 'unitesondage_id',
				'label' => $filter_no_html->filter( $i18n -> say('unitesondage_id') ),
				'placeholder' => $filter_no_html->filter( $i18n -> say('unitesondage_id') ),
				'value' => $us->get('id'),
				'intitule' => $us->get('identification'),
				'readonly' => true,
				'errors' => array(),
		));

		$value_identifiant = $log->has('identification') ? $log->get('identification') : $us->get('nom').'.'.$log->getNextSequenceUslogId($us->get('id'));
		$exp_ident = explode(".",$value_identifiant);
		$prefix_identidiant = isset($exp_ident[0])?$exp_ident[0]:null;
		$numero_usondage = isset($exp_ident[1])?$exp_ident[1]:null;
		
		$this->setElement('id', array(
				'type' => 'hidden',
				'id' => 'id_log',
				'value' => $log->has('id') ? $log->get('id') : null,
				'readonly' => true,
				'validators' => array(),
				'errors' => array()
		));

		$this->setElement('identification', array(
				'type' => 'text',
				'id' => 'identification',
				'prefix' => $prefix_identidiant,
				'label' => $i18n->say('identification'),
				'placeholder' => $i18n->say('identification'),
				'tooltip' => $filter_no_html->filter( $i18n -> say('unitesondage_tooltip') ),
				'value' => $log->has('identification') ? $log->get('identification') : str_replace(" ", "_", $prefix_identidiant).".".$numero_usondage,
				'numero' => $numero_usondage,
				'validators' => array('NotEmpty'),
				'errors' => array('NotEmpty' => array('Le champ identification ne doit pas être vide') ),
		));

		$this->setElement('nom', array(
				'type' => 'text',
				'id' => 'nom',
				'label' => $filter_no_html->filter( $i18n -> say('unitesondage_nom') ),
				'placeholder' => $filter_no_html->filter( $i18n -> say('unitesondage_nom') ),
				'tooltip' => $filter_no_html->filter( $i18n -> say('unitesondage_tooltip') ),
				'value' => $log->has('nom') ? $log->get('nom') : null,
				'validators' => array('NotEmpty'),
				'errors' => array(),
		));

		$this->setElement('description', array(
				'type' => 'textarea',
				'rows' => '3',
				'id' => 'description',
				'label' => $filter_no_html->filter( $i18n -> say('description') ),
				'placeholder' => $filter_no_html->filter( $i18n -> say('description') ),
				'value' => $log->has('description') ? $log->get('description') : null,
				'validators' => array(),
				'errors' => array(),
		));

	}

}