<?php

class Form_Fsn_Organisme extends Yab_Form {

	public function __construct(Model_Fsn_Organisme $fsn_organisme, $mode='add') {
			
		$registry = Yab_Loader::getInstance() -> getRegistry();
		
		// appel fichier internationalisation
		$i18n = $registry -> get('i18n');
		
		// gestion des accentuations / date / ....
		$session = Yab_Loader::getInstance()->getSession();
    	$session->set('format','%d/%m/%Y') ;
		$filter_no_html = new Yab_Filter_NoHtml();
		
		// profil utilisateur Super Admin
		$user_info = $session -> get('session');
		$login = $user_info['login'];
		
		
		$this->set('method', 'post')->set('name', 'form_fsn_organisme')->set('action', '')->set('role', 'form');

		$this->setElement('nom', array(
			'type' => 'text',
			'id' => 'nom',
			'label' => $filter_no_html->filter( $i18n -> say('nom')),
			'placeholder' => $filter_no_html->filter( $i18n -> say('nom')),
			'value' => $fsn_organisme->has('nom') ? $fsn_organisme->get('nom') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));
		
		$this->setElement('nomabrege', array(
			'type' => 'text',
			'id' => 'nomabrege',
			'label' => $filter_no_html->filter( $i18n -> say('nomabrege')),
			'placeholder' => $filter_no_html->filter( $i18n -> say('nomabrege')),
			'value' => $fsn_organisme->has('nomabrege') ? $fsn_organisme->get('nomabrege') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));
		
		/*$this->setElement('type_id', array(
			'type' => 'select',
			'id' => 'type_id',
			'label' => $filter_no_html->filter( $i18n -> say('type_id')),
			'placeholder' => $filter_no_html->filter( $i18n -> say('type_id')),
			'value' => $fsn_organisme->has('type_id') ? $fsn_organisme->get('type_id') : null,
            'fake_options' => array(''=> ' -- Choisir le Type'),
			'options' => $fsn_organisme->getTable('Model_Fsn_Categorie')->fetchAll()->where('categorie_type = "organisme_type" ')->setKey('id')->setValue('categorie_value'),
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));*/
    
		$this->setElement('adresse1', array(
			'type' => 'text',
			'id' => 'adresse1',
			'label' => $filter_no_html->filter( $i18n -> say('adresse1')),
			'placeholder' => $filter_no_html->filter( $i18n -> say('adresse1')),
			'value' => $fsn_organisme->has('adresse1') ? $fsn_organisme->get('adresse1') : null,
			'validators' => array(),
			'errors' => array(),
		));
        
		$this->setElement('adresse2', array(
			'type' => 'text',
			'id' => 'adresse2',
			'label' => $filter_no_html->filter( $i18n -> say('adresse2')),
			'placeholder' => $filter_no_html->filter( $i18n -> say('adresse2')),
			'value' => $fsn_organisme->has('adresse2') ? $fsn_organisme->get('adresse2') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('cp', array(
			'type' => 'text',
			'id' => 'cp',
			'label' => $filter_no_html->filter( $i18n -> say('cp')),
			'placeholder' => '93200',
			'value' => $fsn_organisme->has('cp') ? $fsn_organisme->get('cp') : '',
			'validators' => array('Int'),
			'needed' => false,
			'errors' => array(),
		));

		$this->setElement('ville', array(
			'type' => 'text',
			'id' => 'ville',
			'label' => $filter_no_html->filter( $i18n -> say('ville')),
			'placeholder' => $filter_no_html->filter( $i18n -> say('ville')),
			'value' => $fsn_organisme->has('ville') ? $fsn_organisme->get('ville') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('pays', array(
			'type' => 'text',
			'id' => 'pays',
			'label' => $filter_no_html->filter( $i18n -> say('pays')),
			'placeholder' => $filter_no_html->filter( $i18n -> say('pays')),
			'value' => $fsn_organisme->has('pays') ? $fsn_organisme->get('pays') : 'France',
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));
		
		$this->setElement('telephone', array(
			'type' => 'text',
			'id' => 'telephone',
			'label' => $filter_no_html->filter( $i18n -> say('telephone')),
			'placeholder' => '0101010101',
			'value' => $fsn_organisme->has('telephone') ? $fsn_organisme->get('telephone') : null,
			'validators' => array(),
			'errors' => array(),
		));
    
		$this->setElement('fax', array(
			'type' => 'text',
			'id' => 'fax',
			'label' => $filter_no_html->filter( $i18n -> say('fax')),
			'placeholder' => $filter_no_html->filter( $i18n -> say('fax')),
			'value' => $fsn_organisme->has('fax') ? $fsn_organisme->get('fax') : null,
			'validators' => array(),
			'errors' => array(),
		));
    
		$this->setElement('email', array(
			'type' => 'text',
			'id' => 'email',
			'label' => $filter_no_html->filter( $i18n -> say('email')),
			'placeholder' => 'email@domain.fr',
			'value' => $fsn_organisme->has('email') ? $fsn_organisme->get('email') : null,
			'validators' => array('Email'),
      		'needed' => false,
			'errors' => array(),
		));
    
		$this->setElement('url', array(
			'type' => 'text',
			'id' => 'url',
			'label' => $filter_no_html->filter( $i18n -> say('url')),
			'placeholder' => $filter_no_html->filter( $i18n -> say('url')),
			'value' => $fsn_organisme->has('url') ? $fsn_organisme->get('url') : null,
			'validators' => array(),
			'errors' => array(),
		));
    
        
		/*$organisme_id = $fsn_organisme->has('id') ? $fsn_organisme->get('id') : null ; 
		if (!empty($organisme_id) ) {
		  
		  $intervenant = new Model_Intervenant();
		  $list_correspondants = $intervenant->getListIntervenants()->where('inte.organisme_id = '.$organisme_id ) ;
		  $nb_correspondants = $list_correspondants->count() ;
		  if (!empty($nb_correspondants) ) {      
			$this->setElement('correspondant_id', array(
				'type' => 'select',
				'id' => 'correspondant_id',
				'label' => $filter_no_html->filter( $i18n -> say('correspondant_id')),
				'placeholder' => $filter_no_html->filter( $i18n -> say('correspondant_id')),
				'value' => $fsn_organisme->has('correspondant_id') ? $fsn_organisme->get('correspondant_id') : null,
				'fake_options' => array(''=> ' -- Choisir un Correspondant'),
				'options' => $list_correspondants->setKey('id')->setValue('nom_complet'),
				'validators' => array(),
				'errors' => array(),
			));
		  }
		} */

		$this->setElement('description', array(
			'type' => 'textarea',
			'rows' => '3',
			'id' => 'description',
			'label' => $filter_no_html->filter( $i18n -> say('description')),
			'placeholder' => $filter_no_html->filter( $i18n -> say('description')),
			'value' => $fsn_organisme->has('description') ? $fsn_organisme->get('description') : null,
			'validators' => array(),
			'errors' => array(),
		));
    
		/* Seul le Super Utilisateur - Administrateur Technique peut creer une Antitie Administrative */
		//if($login == 'root') {
			
			/*$fsn_entiteadmin = new Model_Fsn_Entiteadmin();
		    
			if(!empty($organisme_id))
				$entiteadmin_organisme = $fsn_entiteadmin->getEntiteAdministrativeDetail()->where('fe.organisme_id = '.$organisme_id )->count() ;
			else
				$entiteadmin_organisme = "" ;
		    
		    $entiteadmin_exist = (!empty($entiteadmin_organisme) ) ? 1 : 0 ;
		    $readonly = (!empty($entiteadmin_exist) ) ? true : false ; 
		    $type = (!empty($entiteadmin_exist) ) ? 'hidden' : 'checkbox' ;
		
			$this->setElement('entiteadmin', array(
				'type' => $type,
				'id' => 'entiteadmin',
				'label' => $filter_no_html->filter( $i18n -> say('entiteadmin')),
				'placeholder' => $filter_no_html->filter( $i18n -> say('entiteadmin')),
				'value' => $entiteadmin_exist,
		  		'readonly' => $readonly,
				'validators' => array(),
				'errors' => array(),
			));*/
		//}
		
		/* Personnalisation des champs suivant le formulaire add / edit / show */
		foreach($this->getElements() as $element) {

			if($mode == 'show') {
				$element->set('readonly', 'readonly');				
			} else {
				continue;
			}
				
		}

	}

}
