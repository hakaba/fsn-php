<?php
class Form_Fsn_Historique extends Yab_Form {
	public function __construct(Model_Fsn_Historique $fsn_historique, $mode = 'add') {
			
		$registry = Yab_Loader::getInstance()->getRegistry();
		// appel fichier internationalisation
		$i18n = $registry -> get('i18n');
		// gestion des accentuations / date / ....
		$session = Yab_Loader::getInstance()->getSession();
    	
		$filter_no_html = new Yab_Filter_NoHtml();
		$filter_html = new Yab_Filter_Html();
		$this->set('format','%d/%m/%Y') ;
        
		$date = date('d/m/Y H:i:s');
		
		$this->set ( 'method', 'post' )
            ->set ( 'name', 'form_historique    ' )
            ->set ( 'action', '' )
            ->set ( 'role', 'form' )
            ->set ( 'class', 'form-horizontal' );
		
		$this->setElement ( 'sitefouille_id', array (
				'type' => 'text',
				'id' => 'sitefouille_id',
				'label' => $filter_no_html->filter( $i18n -> say('sitefouille_id')), 
				'placeholder' => $filter_no_html->filter( $i18n -> say('sitefouille_id')),
				'value' => $fsn_historique->has ( 'sitefouille_id' )? $fsn_historique->get ( 'sitefouille_id' ) : null,
				'readonly' => 'readonly',
				'validators' => array (),
				'errors' => array () 	
		) );
		
		$this->setElement ( 'categorie_object', array (
				'type' => 'text',
				'id' => 'categorie_object',
				'label' => $filter_no_html->filter( $i18n -> say('categorie_object')), 
				'placeholder' => $filter_no_html->filter( $i18n -> say('categorie_object')),
				'value' => $fsn_historique->has ( 'categorie_object' ) ? $fsn_historique->get ( 'categorie_object' ) : null,
				'readonly' => 'readonly',
				'validators' => array (
						'NotEmpty' 
				),
				'errors' => array (
						'NotEmpty' => array (
								'IS_EMPTY' => "L'objet ne peut pas être vide" 
						) 
				) 
		) );
		
		$this->setElement ( 'login', array (
				'type' => 'text',
				'id' => 'login',
				'label' => $filter_no_html->filter( $i18n -> say('login')), 
				'placeholder' => $filter_no_html->filter( $i18n -> say('login')),
				'value' => $fsn_historique->has ( 'login' ) ? $fsn_historique->get ( 'login' ) : null,
				'readonly' => 'readonly',
				'validators' => array (
						'NotEmpty' 
				),
				'errors' => array () 
		) );
		
		$this->setElement ( 'type_action', array (
				'type' => 'select',
				'id' => 'type_action',
				'label' => $filter_no_html->filter( $i18n -> say('type_action')), 
				'placeholder' => $filter_no_html->filter( $i18n -> say('type_action')),
				'value' => $fsn_historique->has ( 'type_action' ) ? $fsn_historique->get ( 'type_action' ) : 'import',
				'options' => array (
						'import' => 'Import',
						'create' => 'Création ',
						'edit' => 'Modification',
						'delete' => 'Suppression' 
				),
				'readonly' => 'readonly',
				'validators' => array (
						'NotEmpty' 
				),
				'errors' => array () 
		) );
		
		$this->setElement ( 'date_historique', array (
				'type' => 'text',
				'id' => 'date_historique',
				'label' => $filter_no_html->filter( $i18n -> say('date_historique')), 
				'placeholder' => $filter_no_html->filter( $i18n -> say('date_historique')),
				'value' => $fsn_historique->has ( 'date_historique' ) ? $fsn_historique->get ( 'date_historique',$filter_html) : $date,
				'readonly' => 'readonly',
				'validators' => array (
						'NotEmpty' 
				),
				'errors' => array () 
		) );
		
		$data_modified = $fsn_historique->has ( 'data_modified' ) ? $fsn_historique->get ( 'data_modified' ) : null;
		$data_modified = json_decode ( $data_modified );
		$data_modified_table = ! empty ( $data_modified ) ? $this->arrayToHtmlTable ( $data_modified ) : null;

		
		$pre_html = '<div class="form-group field text" id="Fdata_modified">';
		$post_html = '</div><div class="form-group field text" id="Fdata_visu">';
	//	$post_html .= '<label class="col-lg-3 control-label" for="data_visu">Donn&eacute;es Modifi&eacute;es</label>';
	//	$post_html .= '<div id="data_visu" class="col-lg-9"><span class="glyphicon glyphicon-zoom-in"  style="font-size: 18px; cursor:pointer;" ></span></div>';
		$post_html .= '</div>';
		
		$this->setElement ( 'data_modified', array (
				'type' => 'hidden',
				'id' => 'data_modified',
				'label' => $filter_no_html->filter( $i18n -> say('data_modified')), 
				'placeholder' => $filter_no_html->filter( $i18n -> say('data_modified')),
				'rows' => '10',
				
			//	 'value' => $fsn_historique->has('data_modified') ? $fsn_historique->get('data_modified') : null,
				'value' => $fsn_historique->has ( 'data_modified' ) ? $data_modified_table : null,
				'readonly' => 'readonly',
				'validators' => array (
						'NotEmpty' 
				),
				'errors' => array (),
				'pre_html' => $pre_html,
				'post_html' => $post_html 
		) );
		
	/*	$this->setElement ( 'description', array (
				'type' => 'textarea',
        'rows' => '3',
				'id' => 'description',
				'label' => 'Descrisption',
				'rows' => '5',
				'value' => $fsn_historique->has ( 'description' ) ? $fsn_historique->get ( 'description' ) : null,
				'errors' => array () 
		) );  */
		
    	foreach($this->getElements() as $element) {
        
          if($mode == 'show'){
            $element->set('readonly','readonly');
            $element->set('disable',true);
            
            if( $element->get('id') == 'debut' || $element->get('id') == 'fin' ) {
               if ( $element->has('class') ) { $element->rem('class') ; }
                
            } 
            
          } 
          
      }
      
      
    }
		
	
	private function arrayToHtmlTable($array) {
		$registry = Yab_Loader::getInstance ()->getRegistry ();
		$i18n = $registry->get ( 'i18n' );
		
		$_foreignUs=array(
				"contexte_id" => array(
						"foreignTable" => "fsn_categorie",
						"fieldPrimary" => "id",
						"fieldReference" => "categorie_value"
				),
				"consistance_id" => array(
						"foreignTable" => "fsn_categorie",
						"fieldPrimary" => "id",
						"fieldReference" => "categorie_value"
				),
				"methodefouille_id" => array(
						"foreignTable" => "fsn_categorie",
						"fieldPrimary" => "id",
						"fieldReference" => "categorie_value"
				),
				"homogeneite_id" => array(
						"foreignTable" => "fsn_categorie",
						"fieldPrimary" => "id",
						"fieldReference" => "categorie_value"
				),
				"sitefouille_id" => array(
						"foreignTable" => "sitefouille",
						"fieldPrimary" => "id",
						"fieldReference" => "nom"
				),
				"us_id" => array(
						"foreignTable" => "us",
						"fieldPrimary" => "id",
						"fieldReference" => "identification"
				),
				"intervenant_id" => array(
						"foreignTable" => "intervenant",
						"fieldPrimary" => "id",
						"fieldReference" => "nom"
				),
				"statut_id" => array(
						"foreignTable" => "fsn_categorie",
						"fieldPrimary" => "id",
						"fieldReference" => "categorie_value"
				),
				"fonction_id" => array(
						"foreignTable" => "fsn_categorie",
						"fieldPrimary" => "id",
						"fieldReference" => "categorie_value"
				),
				"classe_id" => array(
						"foreignTable" => "fsn_categorie",
						"fieldPrimary" => "id",
						"fieldReference" => "categorie_value"
				),
				"etat_id" => array(
						"foreignTable" => "fsn_categorie", // "etat" -- jfb 2016-04-13
						"fieldPrimary" => "id",
						"fieldReference" => "categorie_value" // "etat" -- jfb 2016-04-13
				),
				"typefai_id" => array(
						"foreignTable" => "fsn_categorie",
						"fieldPrimary" => "id",
						"fieldReference" => "categorie_value"
				),
				"metier_id" => array(
						"foreignTable" => "metier",
						"fieldPrimary" => "id",
						"fieldReference" => "metier"
				),
				"fai_id" => array(
						"foreignTable" => "fai",
						"fieldPrimary" => "id",
						"fieldReference" => "identification"
				),
				"fai_id_1" => array(
						"foreignTable" => "fai",
						"fieldPrimary" => "id",
						"fieldReference" => "identification"
				),
				"type_relation_id" => array(
						"foreignTable" => "fsn_categorie",
						"fieldPrimary" => "id",
						"fieldReference" => "categorie_value"
				),
				"element_a" => array(
						"foreignTable" => "fsn_categorie",
						"fieldPrimary" => "id",
						"fieldReference" => "categorie_value"
				),
				"element_b" => array(
						"foreignTable" => "fsn_categorie",
						"fieldPrimary" => "id",
						"fieldReference" => "categorie_value"
				),
				"oastatut_id" => array(
						"foreignTable" => "fsn_categorie",
						"fieldPrimary" => "id",
						"fieldReference" => "categorie_value"
				),
				"nature_id" => array(
						"foreignTable" => "fsn_categorie",
						"fieldPrimary" => "id",
						"fieldReference" => "categorie_value"
				),
				"oa_id" => array(
						"foreignTable" => "oa",
						"fieldPrimary" => "id",
						"fieldReference" => "identification"
				),
				"role_id" => array(
						"foreignTable" => "fsn_categorie",
						"fieldPrimary" => "id",
						"fieldReference" => "categorie_value"
				),
				"emplacement_id" => array(
						"foreignTable" => "emplacement",
						"fieldPrimary" => "id",
						"fieldReference" => "nom"
				),
				"conteneur_id" => array(
						"foreignTable" => "conteneur",
						"fieldPrimary" => "id",
						"fieldReference" => "nom"
				),
				"organisme_id" => array(
						"foreignTable" => "fsn_organisme", // "fsn_categorie" -- jfb 2016-04-12
						"fieldPrimary" => "id",
						"fieldReference" => "nom"
				),
				"type_id" => array(
						"foreignTable" => "fsn_categorie", // "typeintervention" -- jfb 2016-04-13
						"fieldPrimary" => "id",
						"fieldReference" => "categorie_value" // Ramener le type d'intervention "typeinterv" -- jfb 2016-04-13
				),
				"entiteadmin_id" => array(
						"foreignTable" => "fsn_entiteadmin",
						"fieldPrimary" => "id",
						"fieldReference" => "description"
				),
				"synchrotype_id" => array(
					"foreignTable" => "synchrotype",
					"fieldPrimary" => "id",
					"fieldReference" => "synchrotype" // Ramener le type de Synchronisation
			    ),
			    "synchro1_id" => array(
						"foreignTable" => "us",
						"fieldPrimary" => "id",			
						"fieldReference" => "identification"				
				),
				"synchro2_id" => array(
						"foreignTable" => "us",
						"fieldPrimary" => "id",
						"fieldReference" => "identification"
				),
				"correspondant_id" => array(
						"foreignTable" => "intervenant",
						"fieldPrimary" => "id",
						"fieldReference" => "nom"
				),
				"inclusion1_id" => array(
						"foreignTable" => "us",
						"fieldPrimary" => "id",
						"fieldReference" => "identification"
				),
				"inclusion2_id" => array(
						"foreignTable" => "us",
						"fieldPrimary" => "id",
						"fieldReference" => "identification"
				),
				"anterieur_id" => array(
						"foreignTable" => "us",
						"fieldPrimary" => "id",
						"fieldReference" => "identification"
				),
				"posterieur_id" => array(
						"foreignTable" => "us",
						"fieldPrimary" => "id",
						"fieldReference" => "identification"
				)				
			);

		$objForeign = new Model_Fsn_Historique();
		foreach ($array as $key => $data){
			foreach ($_foreignUs as $champ => $info){
				if ($key == $champ && $data !=""){
					//echo($champ); echo(' '); echo($info["foreignTable"]); echo(' '); echo($info["fieldPrimary"]); echo(' '); echo($data); echo(' '); echo($info["fieldReference"]); echo(' '); //jfb
					$result = $objForeign->foreignKey($info["foreignTable"],$info["fieldPrimary"],$data, $info["fieldReference"]);
					//if (empty($result)) echo('empty dollar result'); else echo($result); //jfb
					$result = !empty($result) ? $result->toArray() : array($info['fieldReference']);
					$array->$key = $result[0][$info["fieldReference"]];
				}
				
			}
			
		}
		$table = '<div class="table-responsive"><table name="data_modified" class="table table-bordered table-condensed">';
		foreach ( $array as $key_data => $value_data ) {
			if (is_array ( $value_data ) || is_object ( $value_data )) {
				$table .= '<tr><th>' . $i18n->say ( $key_data ) . '</th><td>' . $this->arrayToHtmlTable ( $value_data ) . '</td></tr>';
			} elseif ($this->isJSON ( $value_data )) {
				
				$table .= '<tr><th>' . $i18n->say ( $key_data ) . '</th><td>' . $this->arrayToHtmlTable ( json_decode ( $value_data ) ) . '</td></tr>';
			} else {
				$table .= '<tr><th>' . $i18n->say ( $key_data ) . '</th><td>' . $value_data . '</td></tr>';
			}
		}
		$table .= '</table></div>';
		
		return $table;
	}
	private function isJSON($string) {
		return is_string ( $string ) && is_object ( json_decode ( $string ) ) && (json_last_error () == JSON_ERROR_NONE) ? true : false;
	}

}
