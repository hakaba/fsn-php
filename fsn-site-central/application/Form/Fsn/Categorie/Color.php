<?php

class Form_Fsn_Categorie_Color extends Yab_Form {

	public function __construct(Model_Fsn_Categorie_Color $fsn_categorie_color, $mode='add') {

    // Definition entete formulaire
    $this->set('method', 'post')->set('name', 'form_fsn_categorie_color')->set('action', '')->set('role', 'form');
    
    // Appel a la connexion BDD
    $registry = Yab_Loader::getInstance()->getRegistry(); 
    $db = Yab_Loader::getInstance()->getRegistry()->get('db');
    
    if($mode = 'add' ) {
      // Pre-Selection du categorie_type
      $select_categories = $db->prepare('SELECT 
        	id, 
        	categorie_type, 
        	categorie_key, 
        	categorie_value, 
        	visible, 
        	ordre, 
        	description
        FROM fsn_categorie
      ')->where('categorie_type = "categorie_type"');
  
  		$this->setElement('categorie_type', array(
  			'type' => 'select',
  			'id' => 'categorie_type',
  			'label' => 'categorie_type',
  			//'value' => $fsn_categorie->has('categorie_type') ? $fsn_categorie->get('categorie_type') : null,
  			'options' => $select_categories->setKey('categorie_key')->setValue('categorie_value'),        
  			'onchange' => 'submit()',
        'validators' => array('NotEmpty'),
  			'errors' => array(),
  		));
    
    } else {
    
      $categorie_id = $fsn_categorie_color->has('categorie_id') ? $fsn_categorie_color->get('categorie_id') : null ;
      // Indentification de categorie_type
      $select_categories = $db->prepare('SELECT 
        	id, 
        	categorie_type, 
        	categorie_key, 
        	categorie_value, 
        	visible, 
        	ordre, 
        	description
        FROM fsn_categorie
      ')->where('id = '.$categorie_id);
  
  		$this->setElement('categorie_type', array(
  			'type' => 'select',
  			'id' => 'categorie_type',
  			'label' => 'categorie_type',
  			'value' => $fsn_categorie->has('categorie_type') ? $fsn_categorie->get('categorie_type') : null,
  			'options' => $select_categories->setKey('categorie_key')->setValue('categorie_value'),        
  			'readonly' => true,
        'validators' => array('NotEmpty'),
  			'errors' => array(),
  		));
          
    }
    
    
    if (!$this->getElement('categorie_type')->getValue()) {
  	// Impose un choix pr�alable (� la siute du formulaire ) du Partenariats � d�finir.
  
      return $this ;    
    } else {
    
      $type_categories = $this->getElement('categorie_type')->getValue() ;
      
      $categories = $db->prepare('SELECT 
        	id as categorie_id, 
        	categorie_type, 
        	categorie_key, 
        	categorie_value, 
        	visible, 
        	ordre, 
        	description
        FROM fsn_categorie
      ');
      
      $categories->where('categorie_type = :typecategories')->bind(':typecategories', $type_categories) ;
      
      $this->setElement('categorie_id', array(
  			'type' => 'select',
  			'id' => 'categorie_id',
  			'label' => 'categorie_id',
  			'value' => $fsn_categorie_color->has('categorie_id') ? $fsn_categorie_color->get('categorie_id') : null,
  			'options' => $categories->setKey('categorie_id')->setValue('categorie_value'),        
        'fake_option' => array('','Veuillez faire un choix'),
        'validators' => array('NotEmpty'),
  			'errors' => array(),
  		));
    
  		$this->setElement('color', array(
  			'type' => 'text',
  			'id' => 'color',
  			'label' => 'color',
         'class' => 'color', 
  			'value' => $fsn_categorie_color->has('color') ? $fsn_categorie_color->get('color') : 'FFFFFF',
  			'validators' => array('NotEmpty'),
  			'errors' => array(),
  		));

     }
     
     
     
	}

}