<?php

class Form_Fsn_Categorie_Relation extends Yab_Form {

	public function __construct(Model_Fsn_Categorie_Relation $fsn_categorie_relation, $mode = "add", $element_a_id = null ) {

		// Definition entete formulaire
    $this->set('method', 'post')->set('name', 'form_fsn_categorie_relation')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');
    
    $registry = Yab_Loader::getInstance()->getRegistry(); 
    // appel fichier internationalisation
    $i18n = $registry -> get('i18n');
    $filter_no_html = new Yab_Filter_NoHtml();
    
    // Appel a la connexion BDD    
    $db = $registry->get('db');
    
    $fsn_categorie = new Model_Fsn_Categorie();
    if ( $mode == "config" ) {
      // Pre-Selection de la categorie_type
      $select_element = $fsn_categorie->fetchAll()
        ->where('categorie_type = "categorie_type" ')
        ->where('visible = 1');
      
      $filtre_element_a = $select_element ;
      $filtre_element_b = $select_element ;
      
    } else {
      $filtre_element_a = $fsn_categorie->fetchAll()
        ->where('categorie_type IN (SELECT DISTINCT fca.categorie_key AS element_a_type FROM fsn_categorie_relation fcr
          INNER JOIN fsn_categorie fca ON fca.id = fcr.element_a AND fca.categorie_type = "categorie_type"
           ) ')
        ->where('visible = 1');
      $filtre_element_b = $fsn_categorie->fetchAll()
        ->where('categorie_type IN (SELECT DISTINCT fcb.categorie_key AS element_b_type FROM fsn_categorie_relation fcr
          INNER JOIN fsn_categorie fcb ON fcb.id = fcr.element_b AND fcb.categorie_type = "categorie_type"
           ) ')
        ->where('visible = 1');
    }    
    
		$this->setElement('element_a', array(
			'type' => 'select',
			'id' => 'element_a',
			'label' => $filter_no_html->filter( $i18n -> say('element_a')),
			'placeholder' => $filter_no_html->filter( $i18n -> say('element_a')),
			'value' => $fsn_categorie_relation->has('element_a') ? $fsn_categorie_relation->get('element_a') : $element_a_id,
			'options' => $filtre_element_a->setKey('id')->setValue('categorie_value'),
			'fake_options' => array('' => 'Choisir une valeur',),
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));
    if(!empty($element_a_id) ) {
      $this->getElement('element_a')->set('readonly', 'readonly') ;
      
      // 1 - identification du categorie_type de cette categorie
      $element_a_categorie = new Model_Fsn_Categorie($element_a_id);      
      $element_a_categories = $element_a_categorie->fetchAll();
      $element_a_categorie_type = $element_a_categories->getValue('categorie_type');
      
      // 2 - identification id de cette categorie( # categorie_type)      
      $fsn_categorie = new Model_Fsn_Categorie();      
      $fsn_categories = $fsn_categorie->fetchAll()->where('categorie_key = "'.$element_a_categorie_type.'"') ;
      $id_categorie = $fsn_categories->getValue('id'); 
    }
    
    // Pre-Selection de la categorie_type
    $select_type_relation = $fsn_categorie->fetchAll()
      ->where('categorie_type = "categorie_relation" ')
      ->where('visible = 1');   
    
    /*      
		$this->setElement('type_relation', array(
			'type' => 'hidden',
			'id' => 'type_relation',
			'label' => 'type_relation',
			'value' => $fsn_categorie_relation->has('type_relation') ? $fsn_categorie_relation->get('type_relation') : 'depend_de',
			'options' => $select_type_relation->setKey('categorie_key')->setValue('categorie_value'),
			'fake_options' => array('' => 'Choisir une valeur',),
      'validators' => array(''),
			'errors' => array(),      
		));
    */
    
		$this->setElement('type_relation_id', array(
			'type' => 'select',
			'id' => 'type_relation_id',
			'label' => $filter_no_html->filter( $i18n -> say('type_relation_id')),
            'placeholder' => $filter_no_html->filter( $i18n -> say('type_relation_id')),
			'value' => $fsn_categorie_relation->has('type_relation_id') ? $fsn_categorie_relation->get('type_relation_id') : 41,
			'options' => $select_type_relation->setKey('id')->setValue('categorie_value'),
			'fake_options' => array('' => 'Choisir une valeur',),
      'validators' => array('Int','NotEmpty'),
			'errors' => array(),      
		));
        
		$this->setElement('element_b', array(
			'type' => 'select',
			'id' => 'element_b',
			'label' => $filter_no_html->filter( $i18n -> say('element_b')),
            'placeholder' => $filter_no_html->filter( $i18n -> say('element_b')),
			'value' => $fsn_categorie_relation->has('element_b') ? $fsn_categorie_relation->get('element_b') : null,
            'options' => $filtre_element_b->setKey('id')->setValue('categorie_value'),
            'fake_options' => array('' => 'Choisir une valeur',),
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('visible', array(
			'type' => 'checkbox',
			'id' => 'visible',
			'label' => $filter_no_html->filter( $i18n -> say('visible')),
            'placeholder' => $filter_no_html->filter( $i18n -> say('visible')),
			'value' => $fsn_categorie_relation->has('visible') ? $fsn_categorie_relation->get('visible') : 1,
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('ordre', array(
			'type' => 'text',
			'id' => 'ordre',
			'label' => $filter_no_html->filter( $i18n -> say('ordre')),
            'placeholder' => $filter_no_html->filter( $i18n -> say('ordre')),
			'value' => $fsn_categorie_relation->has('ordre') ? $fsn_categorie_relation->get('ordre') : 1,
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('description', array(
			'type' => 'textarea',
      'rows' => '3',
			'id' => 'description',
			'label' => 'description',
			'value' => $fsn_categorie_relation->has('description') ? $fsn_categorie_relation->get('description') : '',
			'validators' => array(),
			'errors' => array(),
		));

	}

}