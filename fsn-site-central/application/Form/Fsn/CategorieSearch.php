<?php

class Form_Fsn_CategorieSearch extends Yab_Form {

	public function __construct(Model_Fsn_Categorie $fsn_categorie, $mode = 'add') {

    // Definition entete formulaire
    $this->set('method', 'post')
          ->set('name', 'formCategorieSearch')
          ->set('action', '')                           
          ->set('role', 'form')                         
          ->set('class', 'form-horizontal ref_gestion_taille_select');    

		$select_categories = $fsn_categorie->fetchAll()->where('categorie_type = "categorie_type"');
    
    $this->setElement('categorie_type', array(
			'type' => 'select',
			'id' => 'categorie_type',
			'label' => "S�l�ction d'une table de r�f�rence",
      'class' => 'color select_style', 
			'value' => $fsn_categorie->has('categorie_type') ? $fsn_categorie->get('categorie_type') : '',
			'options' => $select_categories->setKey('categorie_key')->setValue('categorie_value'),
      'fake_options' => array('' => '-- Veuillez faire un choix'),        
			'onchange' => 'submit()',
      'validators' => array('NotEmpty'),
			'errors' => array('NotEmpty' => array('IS_EMPTY' => 'Module Fonctionnel ne peut �tre vide') ),
		));

	}

}

 