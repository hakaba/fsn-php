<?php

class Form_Fsn_Admin_Group extends Yab_Form {

	public function __construct(Model_Fsn_Admin_Group $fsn_admin_group) {

		$this->set('method', 'post')->set('name', 'form_fsn_admin_group')->set('action', '')->set('role', 'form');

		$this->setElement('group_name', array(
			'type' => 'text',
			'id' => 'group_name',
			'label' => 'group_name',
			'value' => $fsn_admin_group->has('group_name') ? $fsn_admin_group->get('group_name') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('group_label', array(
			'type' => 'text',
			'id' => 'group_label',
			'label' => 'group_label',
			'value' => $fsn_admin_group->has('group_label') ? $fsn_admin_group->get('group_label') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('is_active', array(
			'type' => 'text',
			'id' => 'is_active',
			'label' => 'is_active',
			'value' => $fsn_admin_group->has('is_active') ? $fsn_admin_group->get('is_active') : '1',
			'validators' => array('Int'),
			'errors' => array(),
		));

		$this->setElement('parameter', array(
			'type' => 'textarea',
      'rows' => '3',
			'id' => 'parameter',
			'label' => 'parameter',
			'value' => $fsn_admin_group->has('parameter') ? $fsn_admin_group->get('parameter') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('comment', array(
			'type' => 'text',
			'id' => 'comment',
			'label' => 'comment',
			'value' => $fsn_admin_group->has('comment') ? $fsn_admin_group->get('comment') : null,
			'validators' => array(),
			'errors' => array(),
		));

	}

}