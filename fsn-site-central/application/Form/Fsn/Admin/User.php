<?php

class Form_Fsn_Admin_User extends Yab_Form {

  /**
   * Constructeur
   * 
   * @param Model_Fsn_Admin_User $fsn_admin_user
   * @param type $mode add|edit|show
   */
	public function __construct(Model_Fsn_Admin_User $fsn_admin_user, $mode = 'add') {

		$this->set('method', 'post')->set('name', 'form_fsn_admin_user')->set('action', '')->set('role', 'form');

		$this->setElement('user_login', array(
			'type' => 'text',
			'id' => 'user_login',
			'label' => 'user_login',
			'value' => $fsn_admin_user->has('user_login') ? $fsn_admin_user->get('user_login') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('user_password', array(
			'type' => 'password',
			'id' => 'user_password',
			'label' => 'user_password',
			'value' => $fsn_admin_user->has('user_password') ? $fsn_admin_user->get('user_password') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('user_name', array(
			'type' => 'text',
			'id' => 'user_name',
			'label' => 'user_name',
			'value' => $fsn_admin_user->has('user_name') ? $fsn_admin_user->get('user_name') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('user_email', array(
			'type' => 'text',
			'id' => 'user_email',
			'label' => 'user_email',
			'value' => $fsn_admin_user->has('user_email') ? $fsn_admin_user->get('user_email') : null,
			'validators' => array('Email'),
			'errors' => array(),
		));

		$this->setElement('is_active', array(
			'type' => 'checkbox',
			'id' => 'is_active',
			'label' => 'is_active',
			'value' => $fsn_admin_user->has('is_active') ? $fsn_admin_user->get('is_active') : 1,
			'validators' => array(),
			'errors' => array(),
		));
		
		$this->setElement('comment', array(
			'type' => 'textarea',
      'rows' => '3',
			'id' => 'comment',
			'label' => 'comment',
			'value' => $fsn_admin_user->has('comment') ? $fsn_admin_user->get('comment') : $mode ,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('color', array(
			'type' => 'text',
			'id' => 'color',
			'label' => 'color',
			'class' => 'color',
			'value' => $fsn_admin_user->has('color') ? $fsn_admin_user->get('color') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('parameter', array(
			'type' => 'textarea',
      'rows' => '3',
			'id' => 'parameter',
			'label' => 'parameter',
			'value' => $fsn_admin_user->has('parameter') ? $fsn_admin_user->get('parameter') : null,
			'validators' => array(),
			'errors' => array(),
		));
		
    $this->setElement('ldap_info', array(
			'type' => 'hidden',
			'id' => 'ldap_info',
			'label' => 'ldap_info',
			'value' => $fsn_admin_user->has('ldap_info') ? $fsn_admin_user->get('ldap_info') : '',
			// 'readonly' => 'readonly',
			'validators' => array(),
			'errors' => array(),
		));
    		
		$this->setElement('last_login', array(
			'type' => 'hidden',
			'id' => 'last_login',
			'label' => 'last_login',
			'value' => $fsn_admin_user->has('last_login') ? $fsn_admin_user->get('last_login') : 'CURRENT_TIMESTAMP',
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));
    
    		
		 // Regles d'affichage
    foreach($this->getElements() as $element) {

        if($mode == 'add')
          continue;

        if($mode == 'edit') {
           if ( $element->get('name') == 'user_login' ) {
              $element->set('readonly', 'readonly');
              $element->set('style', 'background-color:lightgrey; color:grey; cursor: no-drop;');
            }
           if ( $element->get('name') == 'last_login' ) {
              $element->set('type', 'text');
              $element->set('readonly', 'readonly');
              $element->set('style', 'background-color:lightgrey; color:grey; cursor: no-drop;');
            }
           if ( $element->get('name') == 'ldap_info' ) {
               $link = "
                        <a href='".Yab_Loader::getInstance()->getRequest()->getBaseUrl()."/COP_ADMIN_USER/LdapInfo?userlogin=".$cop_admin_user->get('user_login')."' title='Informations LDAP' rel='gb_page_fs[]' id='aPopLdapInfo'>
                            <p style='font-size: 12px;'>Information LDAP</p>
                        </a>          
                   ";
               
              $element->set('type', 'textarea');
              $element->set('pre_html', $link);
              $element->set('value', $this->getLdapInfo($cop_admin_user));
            }
         }

        if($mode == 'show') {
          $element->set('readonly', 'readonly');
          $element->set('disabled', 'disabled');
        }

    }

	}
  /**
     * Recupere les infos LDAP en BDD
     * 
     * @param Model_COP_ADMIN_USER $cop_admin_user model admin_user
     * @param string $type type de retour souhait� : txt(default)|html|obj
     * @return undefined selon l'argument pass� 
     */
    public function getLdapInfo($cop_admin_user, $type = "txt") {
        $ldap_info = $cop_admin_user->has('ldap_info') ? $cop_admin_user->get('ldap_info') : null;
        $ldap_info = base64_decode($ldap_info) ;
        if ( $ldap_info != serialize(false) ) {
            $ldap_info = unserialize($ldap_info);
        }
        
        switch ($type) {
            case "txt":
                $txt_ldap_info = "";
                foreach ($ldap_info as $key => $value) {
                    if(!is_int($key)) {
                        $txt_ldap_info .= $key."\n";
                        $txt_ldap_info .= print_r($value, true)."\n";
                        $txt_ldap_info .= "----------------------------------------------\n";
                    }
                }
                $ldap_info = str_replace("\nArray\n(", "", $txt_ldap_info);
                $ldap_info = str_replace(")\n\n", "", $ldap_info);
                break;
            
            case "html":

                break;
            
            case "obj":
                break;

        }

        return $ldap_info;
    }
}