<?php

class Form_Fsn_Entiteadmin extends Yab_Form {

	public function __construct(Model_Fsn_Entiteadmin $fsn_entiteadmin) {

		$this->set('method', 'post')->set('name', 'form_fsn_entiteadmin')->set('action', '')->set('role', 'form');

		$registry = Yab_Loader::getInstance()->getRegistry(); 
		$i18n = $registry->get('i18n');
		$db = Yab_Loader::getInstance()->getRegistry()->get('db');
		// MANTIS : 0000459
		$list_organismes = $db->prepare('
		  SELECT 
			fo.id, 
			fo.nom, 
			fe.organisme_id,
			fo.type_id,
			fc.categorie_value
		  FROM fsn_organisme fo
		  INNER JOIN fsn_categorie fc ON fc.id = fo.type_id
		  LEFT JOIN fsn_entiteadmin fe ON fe.organisme_id = fo.id
		')->where('fe.organisme_id is NULL ');
			
		$this->setElement('organisme_id', array(
			'type' => 'select',
			'id' => 'organisme_id',
			'label' => 'organisme_id',
			'value' => $fsn_entiteadmin->has('organisme_id') ? $fsn_entiteadmin->get('organisme_id') : null,
			'fake_options' => array(''=> ' -- Choisir un Organisme'),
			'options' => $list_organismes->setKey('id')->setValue('nom'),
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('description', array(
			'type' => 'textarea',
			'rows' => '3',
			'id' => 'description',
			'label' => 'description',
			'value' => $fsn_entiteadmin->has('description') ? $fsn_entiteadmin->get('description') : null,
			'validators' => array(),
			'errors' => array(),
		));
	}
}