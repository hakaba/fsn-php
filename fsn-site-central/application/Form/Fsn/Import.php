<?php

class Form_Fsn_Import extends Yab_Form {

	public function __construct(Model_Fsn_Historique $fsn_historique) {

		$registry = Yab_Loader::getInstance() -> getRegistry();
		// $request = Yab_Loader::getInstance()->getRequest() ;
		
		// appel fichier internationalisation
		$i18n = $registry -> get('i18n');
		// gestion des accentuations / date / ....
		$session = Yab_Loader::getInstance()->getSession();
    	$session->set('format','%d/%m/%Y') ;
		$filter_html = new Yab_Filter_NoHtml();
		
		$db = Yab_Loader::getInstance() -> getRegistry() -> get('db');
		
		$user_info = $session -> get('session');
		$login = $user_info['login'];
		$entiteadmin_id = !empty($user_info['entiteadmin_id']) ? $user_info['entiteadmin_id'] : 1 ; // jfb 2016-04-22
		
		$date = date('d/m/Y H:i:s');
		$action = 'import';
		$post_html = '';

		$this -> set('method', 'post') -> set('action', '') -> set('name', 'FormFreeRequest') -> set('id', 'FormFreeRequest') -> set('role', 'form') -> set('class', 'form-horizontal');
		
		/* module fonctiopnnel -  */
		$list_modules = $db -> prepare('
	      SELECT
	        categorie_key,
	        categorie_value
	      FROM fsn_categorie
	      WHERE categorie_type = "categorie_object"
	      AND categorie_key NOT IN ("categorie_object", "categorie_type")
	      AND visible = 1
	      ORDER BY ordre
	
	    ') -> setKey('categorie_key') -> setValue('categorie_value');

		$this -> setElement('module', array(
			'type' => 'select', 
			'id' => 'module', 
			'label' => $filter_html->filter( $i18n -> say('module') ), 
			'placeholder' => $filter_html->filter( $i18n -> say('module') ), 
			'value' => $fsn_historique -> has('module') ? $fsn_historique -> get('module') : null, 
			'options' => array(
				// jfb 2016-04-22 début
				'association_er' => 'Association élément recueilli', 
				'fai_us' => 'Association FAI / US', 
				'conteneur' => 'Conteneur', 
				'contour_fai' => 'Contour FAI', 
				'contour_sitefouille' => 'Contour site de fouille', 
				'contour_us' => 'Contour US', 
				'elementrecueilli' => 'Elément recueilli', 
				'emplacement' => 'Emplacement', 
				'fai' => 'Fait archéologique', 
				'inclusion' => 'Inclusion des US', 
				'intervenant' => 'Intervenant', 
				'intervention_us' => 'Intervention', 
				'interfait' => 'Lien inter FAI', 
				'oa' => 'Opération archéologique', 
				'fsn_organisme' => 'Organisme', 
				'participation' => 'Participation', 
				'phasechrono' => 'Phase chronologique', 
				'fsn_categorie' => 'Référence', 
				'fsn_categorie_relation' => 'Relation entre références', 
				'sitefouille' => 'Site de fouille', 
				'superposition' => 'Superposition des US',
				'synchronisation' => 'Synchronisation des US', 
				'traitement' => 'Traitement élément recueilli', 
				'us' => 'Unité Stratigraphique'
				//'fsn_entiteadmin' => 'Entité administrative', 
				// jfb 2016-04-22 fin
			),
		// 'options' => $list_modules,
			'fake_options' => array('' => '-- ' . $i18n -> say('make_a_choice') . ' un module ', ), 
			'tooltip' => '', 
			//'onchange' => 'submit();',
		//'validators' => array('NotEmpty'),
			'errors' => array(), 
		));

		/* Choix du module fonctionnel - pr�alable */

		if (!$this -> getElement('module') -> getValue()) {
			$upload_max_filesize = ini_get('upload_max_filesize');
			$tooltip = 'importfile_tooltip';
			
			$this -> setElement('importfile', array(
					'type' => 'file',
					'label' => $filter_html->filter( $i18n -> say('importfile')),
					'placeholder' => $filter_html->filter( $i18n -> say('importfile')),
					'id' => 'importfile',
					'tooltip' => $filter_html->filter( $i18n -> say($tooltip, array($upload_max_filesize)) ),
					'value' => $fsn_historique -> has('importfile') ? $fsn_historique -> get('importfile') : null,
					'validators' => array(
							'Upload' => array(
									'destination' => dirname(YAB_PATH) . DIRECTORY_SEPARATOR . 'tmp',
									'override' => true),
					),
			));
			return $this;
		} else {
			$module = $this -> getElement('module') -> getValue();

			//       $post_html .= '<label class="col-lg-2 control-label" for="'.$module.'"></label>';
			//       $post_html .= '<div class="col-lg-9">';
			//       $post_html .= '<div  class="alert alert-warning" >';
			//       $post_html .= $i18n->say('importfile_'.$module);
			//       $post_html .= '</div></div>';

			// $post_html = '<div style="margin:0 5%; padding: 5px ; background-color: #FFEEC7; ">'.$i18n->say('importfile_'.$module).'</div>' ;
			$this -> getElement('module') -> set('type', 'text') -> set('readonly', 'readonly') -> rem('tooltip') -> set('post_html', $post_html);

		}
		$upload_max_filesize = ini_get('upload_max_filesize');
		$tooltip = 'importfile_tooltip';

		$this -> setElement('importfile', array(
			'type' => 'file', 
			'label' => $filter_html->filter( $i18n -> say('importfile')), 
			'placeholder' => $filter_html->filter( $i18n -> say('importfile')), 
			'id' => 'importfile', 
			'tooltip' => $filter_html->filter( $i18n -> say($tooltip, array($upload_max_filesize)) ), 
			'value' => $fsn_historique -> has('importfile') ? $fsn_historique -> get('importfile') : null, 
			'validators' => array(
				'Upload' => array(
					'destination' => dirname(YAB_PATH) . DIRECTORY_SEPARATOR . 'tmp', 
					'override' => true), 
				), 
			));

		/* Chargement du fichier - pr�alable �tape 2
		 if (!$this->getElement('importfile')->getValue()) {
		 return $this ;
		 } else {
		 $importfile = $this->getElement('importfile')->getValue() ;
		 $this->getElement('importfile')->set('type', 'text')->set('readonly', 'readonly');

		 }
		 */
		$this -> setElement('firstlineheader', array(
			'type' => 'checkbox', 
			'label' => $filter_html->filter( $i18n -> say('firstlineheader')), 
			'placeholder' => $filter_html->filter( $i18n -> say('firstlineheader')), 
			'class' => 'text-justify', 
			'value' => $fsn_historique -> has('firstlineheader') ? $fsn_historique -> get('firstlineheader') : 1, 
			'needed' => false, 
		));

		$this -> setElement('type_action', array(
			'type' => 'text', 
			'label' => $filter_html->filter( $i18n -> say('type_action')), 
			'placeholder' => $filter_html->filter( $i18n -> say('type_action')), 
			'value' => $fsn_historique -> has('type_action') ? $fsn_historique -> get('type_action') : $action, 
			'validators' => array('NotEmpty'), 
			'readonly' => 'readonly', 
			'errors' => array(), 
		));

		$this -> setElement('date_historique', array(
			'type' => 'text', 
			'label' => $filter_html->filter( $i18n -> say('date_historique')), 
			'placeholder' => $filter_html->filter( $i18n -> say('date_historique')), 
			'value' => $fsn_historique -> has('date_historique') ? $fsn_historique -> get('date_historique', 'Date') : $date, 
			'validators' => array('NotEmpty'), 
			'readonly' => 'readonly', 
			'errors' => array(), 
		));

		$this -> setElement('login', array(
			'type' => 'text', 
			'label' => $filter_html->filter( $i18n -> say('login')), 
			'placeholder' => $filter_html->filter( $i18n -> say('login')), 
			'value' => $fsn_historique -> has('login') ? $fsn_historique -> get('login') : $login, 
			'validators' => array('NotEmpty'), 
			'readonly' => 'readonly', 
			'errors' => array(), 
		));
		
		/*
		 $this->setElement('data_modify', array(
		 'type' => 'hidden',
		 'label' => 'data_modify',
		 'rows' => '10',
		 'value' => $fsn_historique->has('data_modify') ? $fsn_historique->get('data_modify') : 'No info',
		 'validators' => array('NotEmpty'),
		 'readonly' => 'readonly',
		 'errors' => array(),
		 ));
		 */
		$this -> setElement('description', array(
			'type' => 'textarea',
      'rows' => '3', 
			'label' => $i18n -> say('description'), 
			'placeholder' => $i18n -> say('description'), 
			'rows' => '5', 
			'value' => $fsn_historique -> has('description') ? $fsn_historique -> get('description') : null, 
		));

		// jfb 2016-04-22 début
		$this->setElement('entiteadmin', array(
				'type' => 'text',
				'label' => $filter_html->filter( $i18n -> say('entiteadmin_id')),
				'placeholder' => $filter_html->filter( $i18n -> say('entiteadmin_id')),
				'value' => $fsn_historique -> has('entiteadmin_id') ? $fsn_historique -> get('entiteadmin_id') : $entiteadmin_id,
				'validators' => array('NotEmpty'),
				'readonly' => 'readonly',
				'errors' => array(),
		));
		// jfb 2016-04-22 fin
		
		/* Personnalisation des champs suivant le module */
		foreach ($this->getElements() as $element) {

			if ($element -> get('name') == 'module' && $fsn_historique -> has('module')) {
				$element -> set('type', 'text');
				$element -> set('readonly', 'readonly');

			}

		}
	}
}
