<?php

class Form_Profil extends Yab_Form {

	public function __construct(Model_Profil $profil) {
		
		// appel fichier internationalisation
		$registry = Yab_Loader::getInstance()->getRegistry();
        $i18n = $registry -> get('i18n');
        $filter_no_html = new Yab_Filter_NoHtml();
		
		$this->set('method', 'post')->set('name', 'form_profil')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('Nomprofil', array(
			'type' => 'text',
			'id' => 'Nomprofil',
			'label' => $filter_no_html->filter( $i18n -> say('Nomprofil') ),
			'value' => $profil->has('Nomprofil') ? $profil->get('Nomprofil') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('Description', array(
			'type' => 'text',
			'id' => 'Description',
			'label' => $filter_no_html->filter( $i18n -> say('Description') ),
			'value' => $profil->has('Description') ? $profil->get('Description') : null,
			'validators' => array(),
			'errors' => array(),
		));

	}

}