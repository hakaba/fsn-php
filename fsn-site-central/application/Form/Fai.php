<?php

class Form_Fai extends Yab_Form {

	public function __construct(Model_Fai $fai) {
				
		// appel fichier internationalisation
		$registry = Yab_Loader::getInstance()->getRegistry();
        $i18n = $registry -> get('i18n');
        $filter_no_html = new Yab_Filter_NoHtml();
		
		$this->set('method', 'post')->set('name', 'form_fai')->set('action', '')->set('role', 'form');	
        
		$session = Yab_Loader::getInstance()->getSession();
		$sf = new Model_Sitefouille($fai->has('sitefouille_id') ? $fai->get('sitefouille_id') : $session->get('sitefouille_id'));
		
		$this->setElement('sitefouille_id', array(
			'type' => 'hidden',
			'id' => 'sitefouille_id',
			'label' => $filter_no_html->filter( $i18n -> say('sitefouille_id') ),
			'value' => $sf->get('id'),
			'intitule' => $sf->get('nomabrege')." - ".$sf->get('nom'),
			'validators' => array('NotEmpty'),
			'readonly' => true,
			'errors' => array(),
		));
		
		//'options' =>  $fai->getTable('Model_Fsn_Categorie')->fetchAll()->setKey('id')->setValue('categorie_value')->where('categorie_type like "fai%" and visible = 1'),
		$this->setElement('typefai_id', array(
			'type' => 'select',
			'id' => 'typefai_id',
			'label' => $filter_no_html->filter( $i18n -> say('fai_type') ),
			'value' => $fai->has('typefai_id') ? $fai->get('typefai_id') : null,
			'fake_options' => array(),
			'options' =>  $fai->getOnlyFaiType()->setKey('id')->setValue('categorie_value'),
			'validators' => array('Int'),
			'errors' => array(),
		));
				
		$this->setElement('description', array(
			'type' => 'textarea',
			'rows' => '3',
			'id' => 'description',
			'label' => $filter_no_html->filter( $i18n -> say('fai_description') ),
			'placeholder' => $filter_no_html->filter( $i18n -> say('description') ),
			'value' => $fai->has('description') ? $fai->get('description') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('identification', array(
			'type' => 'text',
			'id' => 'identification',
			'label' => $filter_no_html->filter( $i18n -> say('identification') ),
			'placeholder' => $filter_no_html->filter( $i18n -> say('identification') ),
			'value' => $fai->has('identification') ? $fai->get('identification') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));
		
		$this->setElement('deb_fourc_hte_certain', array(
			'type' => 'text',
			'id' => 'deb_fourc_hte_certain',
			'label' => 'deb_fourc_hte_certain',
			'placeholder' => 2000,
			'value' => $fai->has('deb_fourc_hte_certain') ? $fai->get('deb_fourc_hte_certain') : null,
			'validators' => array('Int'),
			'errors' => array(),
		));

		$this->setElement('deb_fourc_bas_certain', array(
			'type' => 'text',
			'id' => 'deb_fourc_bas_certain',
			'label' => 'deb_fourc_bas_certain',
			'placeholder' => 2000,
			'value' => $fai->has('deb_fourc_bas_certain') ? $fai->get('deb_fourc_bas_certain') : null,
			'validators' => array('Int'),
			'errors' => array(),
		));

		$this->setElement('fin_fourc_hte_certain', array(
			'type' => 'text',
			'id' => 'fin_fourc_hte_certain',
			'label' => 'fin_fourc_hte_certain',
			'placeholder' => 2000,
			'value' => $fai->has('fin_fourc_hte_certain') ? $fai->get('fin_fourc_hte_certain') : null,
			'validators' => array('Int'),
			'errors' => array(),
		));

		$this->setElement('fin_fourc_bas_certain', array(
			'type' => 'text',
			'id' => 'fin_fourc_bas_certain',
			'label' => 'fin_fourc_bas_certain',
			'placeholder' => 2000,
			'value' => $fai->has('fin_fourc_bas_certain') ? $fai->get('fin_fourc_bas_certain') : null,
			'validators' => array('Int'),
			'errors' => array(),
		));

		$this->setElement('deb_fourc_hte_estim', array(
			'type' => 'text',
			'id' => 'deb_fourc_hte_estim',
			'label' => $filter_no_html->filter( $i18n -> say('fai_debut_fh_aaaa') ),
			'placeholder' => 2000,
			'value' => $fai->has('deb_fourc_hte_estim') ? $fai->get('deb_fourc_hte_estim') : null,
			'validators' => array('Int'),
			'errors' => array(),
		));

		$this->setElement('deb_fourc_bas_estim', array(
			'type' => 'text',
			'id' => 'deb_fourc_bas_estim',
			'label' => $filter_no_html->filter( $i18n -> say('fai_debut_fb_aaaa') ),
			'placeholder' => 2000,
			'value' => $fai->has('deb_fourc_bas_estim') ? $fai->get('deb_fourc_bas_estim') : null,
			'validators' => array('Int'),
			'errors' => array(),
		));

		$this->setElement('fin_fourc_hte_estim', array(
			'type' => 'text',
			'id' => 'fin_fourc_hte_estim',
			'label' => $filter_no_html->filter( $i18n -> say('fai_fin_fh_aaaa') ),
			'placeholder' => 2000,
			'value' => $fai->has('fin_fourc_hte_estim') ? $fai->get('fin_fourc_hte_estim') : null,
			'validators' => array('Int'),
			'errors' => array(),
		));

		$this->setElement('fin_fourc_bas_estim', array(
			'type' => 'text',
			'id' => 'fin_fourc_bas_estim',
			'label' => $filter_no_html->filter( $i18n -> say('fai_fin_fb_aaaa') ),
			'placeholder' => 2000,
			'value' => $fai->has('fin_fourc_bas_estim') ? $fai->get('fin_fourc_bas_estim') : null,
			'validators' => array('Int'),
			'errors' => array(),
		));
		
	}

}

