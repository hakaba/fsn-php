<?php

class Form_Superposition extends Yab_Form {

	public function __construct(Model_Superposition $superposition, $sf_id = null, $mode='add' ) {

		$this->set('method', 'post')->set('name', 'form_superposition')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		if(!$sf_id)
			$sf_id = Yab_Loader::getInstance()->getSession()->get('sitefouille_id');
		$us = $superposition->getTable('Model_Us')->getAllUsFromSf($sf_id)->setKey('id')->setValue('identification');

		$this->setElement('anterieur_id', array(
			'type' => 'text',
			'id' => 'anterieur_id',
			'label' => 'US antérieur',
			'value' => null,
			'options' => $us,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));
		$this->setElement('posterieur_id', array(
			'type' => 'text',
			'id' => 'posterieur_id',
			'label' => 'US posterieur',
			'value' => null,
			'options' => $us,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('incertitude', array(
			'type' => 'checkbox',
			'id' => 'incertitude',
			'label' => 'Incertitude',
			'value' => false,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('sptype_id', array(
			'type' => 'select',
			'id' => 'sptype_id',
			'label' => 'Type',
			'value' => $superposition->has('sptype_id') ? $superposition->get('sptype_id') : null,
			'fake_options' => array(null => 'Non déterminé'),
			'needed' => false,
			'options' => $superposition->getTable('Model_Sptype')->getVisibleSptype()->setKey('id')->setValue('sptype'),
			'errors' => array(),
		));

		$this->setElement('duree_min_certain', array(
			'type' => 'text',
			'id' => 'duree_min_certain',
			'label' => 'Dur&eacute;e Min Certaine',
			'value' => $superposition->has('duree_min_certain') ? $superposition->get('duree_min_certain') : null,
			'validators' => array('Int'),
			'errors' => array(),
		));

		$this->setElement('duree_max_certain', array(
			'type' => 'text',
			'id' => 'duree_max_certain',
			'label' => 'Dur&eacute;e Max Certaine',
			'value' => $superposition->has('duree_max_certain') ? $superposition->get('duree_max_certain') : null,
			'validators' => array('Int'),
			'errors' => array(),
		));

		$this->setElement('duree_min_estim', array(
			'type' => 'text',
			'id' => 'duree_min_estim',
			'label' => 'Dur&eacute;e Min Estim&eacute;e',
			'value' => $superposition->has('duree_min_estim') ? $superposition->get('duree_min_estim') : null,
			'validators' => array('Int'),
			'errors' => array(),
		));

		$this->setElement('duree_max_estim', array(
			'type' => 'text',
			'id' => 'duree_max_estim',
			'label' => 'Dur&eacute;e Max Estim&eacute;e',
			'value' => $superposition->has('duree_max_estim') ? $superposition->get('duree_max_estim') : null,
			'validators' => array('Int'),
			'errors' => array(),
		));

		$this->setElement('observation', array(
			'type' => 'area',
			'id' => 'observation',
			'label' => 'Observations...',
			'value' => $superposition->has('observation') ? $superposition->get('observation') : null,
			'validators' => array(),
			'errors' => array(),
			'maxlength' => 100
		));

	}

}