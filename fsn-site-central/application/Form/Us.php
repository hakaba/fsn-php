<?php

class Form_Us extends Yab_Form {

	public function __construct(Model_Us $us, $mode = 'add') {

		$this->set('method', 'post')
		  ->set('name','form_us')
		  ->set('action', '')
		  ->set('role', 'form')
		  ->set('class', 'form-horizontal');          

        $loader = Yab_Loader::getInstance();
        $session = $loader->getSession();
        
        $filter_no_html = new Yab_Filter_NoHtml();
        $filter_html = new Yab_Filter_Html();
        $filter_html->set('format','%d/%m/%Y') ;
        
        $registry = $loader->getRegistry(); 
        $i18n = $registry->get('i18n');
		$sf = new Model_Sitefouille($us->has('sitefouille_id') ? $us->get('sitefouille_id') : $session->get('sitefouille_id'));
        $datation_au_plus_ancien = $sf->get('datation_debut') ;
        $datation_au_plus_recent = $sf->get('datation_fin') ;



        /*
         * Date par defaut : date saisie dans les infos du site de fouille
         * Date_Planché ($datation_au_plus_ancien) :
         * Date_Plafond ($datation_au_plus_recent) :
         * $sitefouille_id = $us->get('sitefouille_id') ;
         * $sitefouille = new Model_Sitefouille($sitefouille_id);
         * $datation_au_plus_ancien = $sitefouille->get('datation_debut') ;
         * $datation_au_plus_recent = $sitefouille->get('datation_fin') ;
         */

         $this->setElement('sitefouille_id', array(
            'type' => 'hidden',
            'id' => 'sitefouille_id',
            'label' => $i18n->say('sitefouille_id'),
		 	'value' => $sf->get('id'),
		 	'intitule' => $sf->get('nomabrege')." - ".$sf->get('nom'),
            'fake_options' => array(),
            'errors' => array(),
            'readonly' => true,
        ));

		$this->setElement('phase_id', array(
			'type' => 'select',
			'id' => 'phase_id',
			'label' => $i18n->say('phase_id'),
			'value' => $us->has('phase_id') ? $us->get('phase_id') : null,
			'fake_options' => array(null => 'Non déterminé'),
			'options' => $us->getTable('Model_Phasechrono')->getVisiblePhases($sf->get('id'))->setKey('id')->setValue('identification'),
			'needed' => false,
		));

		$value_identifiant = $us->has('identification') ? $us->get('identification') : $sf->get('nomabrege').'.'.$us->getNextSequenceUsId($sf->get('id'));
		$exp_ident = explode(".",$value_identifiant);
		$prefix_identidiant = isset($exp_ident[0])?$exp_ident[0]:null;
		$numero_us = isset($exp_ident[1])?$exp_ident[1]:null;

		$this->setElement('identification', array(
			'type' => 'hidden',
			'id' => 'identification',
			'prefix' => $prefix_identidiant,
			'numero' => $numero_us,
			'label' => $i18n->say('identification'),
			'placeholder' => $i18n->say('identification'),
			'value' => $value_identifiant,
			'validators' => array('NotEmpty'),
			'errors' => array('NotEmpty' => array('Le champ identification ne doit pas être vide') ),
		));

		$this->setElement('description', array(
			'type' => 'textarea',
			'id' => 'description',
			'label' => $i18n->say('description'),
      		'placeholder' => $i18n->say('description'),
      		'rows' => '3',
			'value' => $us->has('description') ? $us->get('description') : '',
      		'validators' => array(),
      		'errors' => array(),
      		'validators' => array()
		));

        $this->setElement('debut', array(
			'type' => 'text',
			'id' => 'debut',
			'label' => $filter_no_html->filter($i18n->say('us_debut')),
			'placeholder' => $filter_no_html->filter($i18n->say('debut')),
			'class' => 'form-control datepicker',
            'value' => $us->has('debut') && $us->get('debut')? date('d/m/Y', strtotime($us->get('debut'))) : null,
			'validators' => array('Date'=>array('format' => 'dd/mm/yyyy')),
			'errors' => array(),
		));

		//,'GreaterThanDate' => array('than'=>$this->getElement('debut')->get('value')
		$this->setElement('fin', array(
			'type' => 'text',
			'id' => 'fin',
			'label' => $i18n->say('us_fin'),
            'placeholder' => $i18n->say('fin'),
            'class' => 'form-control datepicker',
            'date-language' => 'fr',
            'value' => $us->has('fin') && $us->get('fin')? date('d/m/Y', strtotime($us->get('fin'))) : null,
			'validators' => array('Date'=>array('format' => 'dd/mm/yyyy')
									/*,'GreaterThanDate' => array('than'=>$this->getElement('debut')->get('value'))*/
			),
			'errors' => array(),
		));

		$this->setElement('commentaire', array(
			'type' => 'textarea',
			'rows' => '3',
			'id' => 'commentaire',
			'label' => $i18n->say('commentaire'),
			'value' => $us->has('commentaire') ? $us->get('commentaire') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('occupation', array(
			'type' => 'text',
			'id' => 'occupation',
			'label' => $i18n->say('occupation'),
			'value' => $us->has('occupation') ? $us->get('occupation') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('couleur', array(
			'type' => 'text',
			'id' => 'couleur',
			'label' => $i18n->say('couleur'),
			'value' => $us->has('couleur') ? $us->get('couleur') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('interpretation', array(
			'type' => 'textarea',
      		'rows' => '3',
			'id' => 'interpretation',
			'label' => $i18n->say('interpretation'),
			'value' => $us->has('interpretation') ? $us->get('interpretation') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('contexte_id', array(
			'type' => 'select',
			'id' => 'contexte_id',
			'label' => $i18n->say('contexte_id'),
			'value' => $us->has('contexte_id') ? $us->get('contexte_id') : null,
			'fake_options' => array(null => 'Non déterminé'),
			'needed' => false,
			'options' => $us->getTable('Model_Contexte')->fetchAll()->setKey('id')->setValue('contexte'),
		));

		$this->setElement('consistance_id', array(
			'type' => 'select',
			'id' => 'consistance_id',
			'label' => $i18n->say('consistance_id'),
			'value' => $us->has('consistance_id') ? $us->get('consistance_id') : null,
			'fake_options' => array(null => 'Non déterminé'),
			'needed' => false,
			'options' => $us->getTable('Model_Consistance')->fetchAll()->setKey('id')->setValue('consistance'),
			'validators' => array(),
            'errors' => array(),
		));

		$this->setElement('methodefouille_id', array(
			'type' => 'select',
			'id' => 'methodefouille_id',
			'label' => $i18n->say('methodefouille_id'),
			'value' => $us->has('methodefouille_id') ? $us->get('methodefouille_id') : null,
			'fake_options' => array(null => 'Non déterminé'),
			'needed' => false,
			'options' => $us->getTable('Model_Methodefouille')->fetchAll()->setKey('id')->setValue('methodefouille'),
			'validators' => array(),
            'errors' => array(),
		));

		$this->setElement('elementmineral_tuiles', array(
			'type' => 'checkbox',
			'id' => 'elementmineral_tuiles',
			'label' => $i18n->say('elementmineral_tuiles'),
			'value' => $us->has('elementmineral_tuiles') && $us->get('elementmineral_tuiles')!="" ? $us->get('elementmineral_tuiles') : "null",
			'validators' => array(),
			'errors' => array(),
			'needed' => false,
			'class' => "tribox form-control"
		));

		$this->setElement('elementmineral_brique', array(
			'type' => 'checkbox',
			'id' => 'elementmineral_brique',
			'label' => $i18n->say('elementmineral_brique'),
			'value' => $us->has('elementmineral_brique') && $us->get('elementmineral_brique')!="" ? $us->get('elementmineral_brique') : "null",
			'validators' => array(),
			'errors' => array(),
			'needed' => false,
			'class' => "tribox form-control"
		));

		$this->setElement('elementorganique_charbon', array(
			'type' => 'checkbox',
			'id' => 'elementorganique_charbon',
			'label' => $i18n->say('elementorganique_charbon'),
			'value' => $us->has('elementorganique_charbon') && $us->get('elementorganique_charbon')!="" ? $us->get('elementorganique_charbon') : "null",
			'validators' => array(),
			'errors' => array(),
			'needed' => false,
			'class' => "tribox form-control"
		));

		$this->setElement('elementorganique_cendre', array(
			'type' => 'checkbox',
			'id' => 'elementorganique_cendre',
			'label' => $i18n->say('elementorganique_cendre'),
			'value' => $us->has('elementorganique_cendre') && $us->get('elementorganique_cendre')!="" ? $us->get('elementorganique_cendre') : "null",
			'validators' => array(),
			'errors' => array(),
			'needed' => false,
			'class' => "tribox form-control"
		));

		$this->setElement('elementmineral_platre', array(
			'type' => 'checkbox',
			'id' => 'elementmineral_platre',
			'label' => $i18n->say('elementmineral_platre'),
			'value' => $us->has('elementmineral_platre') && $us->get('elementmineral_platre')!="" ? $us->get('elementmineral_platre') : "null",
			'validators' => array(),
			'errors' => array(),
      		'needed' => false,
			'class' => "tribox form-control"
		));

		$this->setElement('descriptionorganique', array(
			'type' => 'textarea',
      		'rows' => '3',
			'id' => 'descriptionorganique',
			'label' => $i18n->say('descriptionorganique'),
			'value' => $us->has('descriptionorganique') ? $us->get('descriptionorganique') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('descriptionmineral', array(
			'type' => 'textarea',
      		'rows' => '3',
			'id' => 'descriptionmineral',
			'label' => $i18n->say('descriptionmineral'),
			'value' => $us->has('descriptionmineral') ? $us->get('descriptionmineral') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('materielisole', array(
			'type' => 'checkbox',
			'id' => 'materielisole',
			'label' => $i18n->say('materielisole'),
			'value' => $us->has('materielisole') && $us->get('materielisole')!="" ? $us->get('materielisole') : "null",
			'validators' => array(),
			'errors' => array(),
			'needed' => false,
			'class' => "tribox form-control"
		));

		$this->setElement('materielconserve_poterie', array(
			'type' => 'checkbox',
			'id' => 'materielconserve_poterie',
			'label' => $i18n->say('materielconserve_poterie'),
			'value' => $us->has('materielconserve_poterie') && $us->get('materielconserve_poterie')!="" ? $us->get('materielconserve_poterie') : "null",
			'validators' => array(),
			'errors' => array(),
			'needed' => false,
			'class' => "tribox form-control"
		));

		$this->setElement('materielconserve_osanimal', array(
			'type' => 'checkbox',
			'id' => 'materielconserve_osanimal',
			'label' => $i18n->say('materielconserve_osanimal'),
			'value' => $us->has('materielconserve_osanimal') && $us->get('materielconserve_osanimal')!="" ? $us->get('materielconserve_osanimal') : "null",
			'validators' => array(),
			'errors' => array(),
			'needed' => false,
			'class' => "tribox form-control"
		));

		$this->setElement('materielconserve_construc', array(
			'type' => 'checkbox',
			'id' => 'materielconserve_construc',
			'label' => $i18n->say('materielconserve_construc'),
			'value' => $us->has('materielconserve_construc') && $us->get('materielconserve_construc')!="" ? $us->get('materielconserve_construc') : "null",
			'validators' => array(),
			'errors' => array(),
			'needed' => false,
			'class' => "tribox form-control"
		));

		$this->setElement('homogeneite_id', array(
			'type' => 'select',
			'id' => 'homogeneite_id',
			'label' => $i18n->say('homogeneite_id'),
			'value' => $us->has('homogeneite_id') ? $us->get('homogeneite_id') : null,
			'fake_options' => array(null => 'Non déterminé'),
			'needed' => false,
			'options' => $us->getTable('Model_Homogeneite')->fetchAll()->setKey('id')->setValue('homogeneite'),
			'errors' => array(),
		));

        $this->setElement('taq_certain', array(
            'type' => 'text',
            'id' => 'taq_certain',
            'label' => $i18n->say('taq_certain'),
            'placeholder' => $i18n->say('taq_certain'),
            'tooltip' => $i18n->say('taq_certain_tooltip'),
            'value' => $us->has('taq_certain') ? $us->get('taq_certain') : $datation_au_plus_recent,
            'validators' => array('Int', 'NotEmpty',
				'LowerThan' => array('than' => $datation_au_plus_recent)),
            'errors' => array(
                'NotEmpty' => array(
                    'IS_EMPTY' => "Indiquer le ".$i18n->say('taq_certain'),
                )),
        ));


		$this->setElement('inst_org_planch_certain', array(
			'type' => 'text',
			'id' => 'inst_org_planch_certain',
			'label' => $i18n->say('inst_org_planch_certain'),
			'placeholder' => $i18n->say('inst_org_planch_certain'),
			'tooltip' => $i18n->say('inst_org_planch_certain_tooltip'),
			'value' => $us->has('inst_org_planch_certain') ? $us->get('inst_org_planch_certain') : $datation_au_plus_ancien,
			'validators' => array('Int','NotEmpty',
								'GreaterThan' => array('than' => $datation_au_plus_ancien)),
			'errors' => array(),
		));


        $this->setElement('inst_org_plaf_certain', array(
            'type' => 'text',
            'id' => 'inst_org_plaf_certain',
            'label' => $i18n->say('inst_org_plaf_certain'),
            'placeholder' => $i18n->say('inst_org_plaf_certain'),
            'tooltip' => $i18n->say('inst_org_plaf_certain_tooltip'),
            'value' => $us->has('inst_org_plaf_certain') ? $us->get('inst_org_plaf_certain') : null,
            'validators' => array('Int', 'GreaterThan' => array('than'=> $this->getElement('inst_org_planch_certain')->get('value'),
																'thanf'=> $this->getElement('inst_org_planch_certain')->get('label'))),
            'errors' => array(),
            'needed' => false,
        ));

		$this->setElement('tpq_certain', array(
			'type' => 'text',
			'id' => 'tpq_certain',
			'label' => $i18n->say('tpq_certain'),
			'placeholder' => $i18n->say('tpq_certain'),
			'tooltip' => $i18n->say('tpq_certain_tooltip'),
			'value' => $us->has('tpq_certain') ? $us->get('tpq_certain') : null,
			'validators' => array('Int',
				'LowerThan' => array('than' => $this->getElement('taq_certain')->get('value'),
										'thanf' => $this->getElement('taq_certain')->get('label')),
				'GreaterThan' => array('than' => $this->getElement('inst_org_plaf_certain')->get('value'),
										'thanf' => $this->getElement('inst_org_plaf_certain')->get('label'))),
			'errors' => array(),
			'needed' => false,
		));


		if ($this->getElement('tpq_certain')->get('value') && $this->getElement('inst_org_plaf_certain')->get('value')){
			$interval_max_certain = $this->getElement('taq_certain')->get('value') - $this->getElement('inst_org_planch_certain')->get('value');
			$validator_duree_max_certain = array('Int',
				'LowerThan' => array('than' => $interval_max_certain)
			);
		}else{
			$validator_duree_max_certain = array('Int');
		}
		$this->setElement('duree_max_certain', array(
			'type' => 'text',
			'id' => 'duree_max_certain',
			'label' => $i18n->say('duree_max_certain'),
			'placeholder' => $i18n->say('duree_max_certain'),
			'tooltip' => $i18n->say('duree_max_certain_tooltip'),
			'value' => $us->has('duree_max_certain') ? $us->get('duree_max_certain') : null,
			'needed' => false,
			'validators' => $validator_duree_max_certain,
			'errors' => array(),
		));

		$valmax_min_duree_certain = null;
		if ($this->getElement('tpq_certain')->get('value') && $this->getElement('inst_org_plaf_certain')->get('value')){
			$valmax_min_duree_certain = $this->getElement('tpq_certain')->get('value') - $this->getElement('inst_org_plaf_certain')->get('value');

		}
		if ($this->getElement('duree_max_certain')->get('value')){
			$val_duree_max_certain = $this->getElement('duree_max_certain')->get('value');
			if(($valmax_min_duree_certain && $valmax_min_duree_certain < $val_duree_max_certain)
				|| !$valmax_min_duree_certain)
				$valmax_min_duree_certain = $val_duree_max_certain;
		}

		if($valmax_min_duree_certain){
			$validator_duree_min_certain = array('Int',
				'LowerThan' => array('than' => $valmax_min_duree_certain),
				'GreaterThan' => array('than' => 0));
		}
		else{
			$validator_duree_min_certain = array('Int',
				'GreaterThan' => array('than' => 0));
		}


        $this->setElement('duree_min_certain', array(
            'type' => 'text',
            'id' => 'duree_min_certain',
            'label' => $i18n->say('duree_min_certain'),
            'placeholder' => $i18n->say('duree_min_certain'),
            'tooltip' => $i18n->say('duree_min_certain_tooltip'),
            'value' => $us->has('duree_min_certain') ? $us->get('duree_min_certain') : null,
            'needed' => false,
			'validators' => $validator_duree_min_certain,
            'errors' => array(),
        ));



        $this->setElement('taq_estim', array(
            'type' => 'text',
            'id' => 'taq_estim',
            'label' => $i18n->say('taq_estim'),
            'placeholder' => $i18n->say('taq_estim'),
            'tooltip' => $i18n->say('taq_estim_tooltip'),
            'value' => $us->has('taq_estim') ? $us->get('taq_estim') : null,
            'validators' => array('Int'),
            'errors' => array(),
            'needed' => false,
        ));

        $this->setElement('tpq_estim', array(
            'type' => 'text',
            'id' => 'tpq_estim',
            'label' => $i18n->say('tpq_estim'),
            'placeholder' => $i18n->say('tpq_estim'),
            'tooltip' => $i18n->say('tpq_estim_tooltip'),
            'value' => $us->has('tpq_estim') ? $us->get('tpq_estim') : null,
            'validators' => array('Int'),
            'errors' => array(),
            'needed' => false,
        ));

        $this->setElement('inst_org_plaf_estim', array(
            'type' => 'text',
            'id' => 'inst_org_plaf_estim',
            'label' => $i18n->say('inst_org_plaf_estim'),
            'placeholder' => $i18n->say('inst_org_plaf_estim'),
            'tooltip' => $i18n->say('inst_org_plaf_estim_tooltip'),
            'value' => $us->has('inst_org_plaf_estim') ? $us->get('inst_org_plaf_estim') : null,
            'validators' => array('Int'),
            'errors' => array(),
            'needed' => false,
        ));

        $this->setElement('inst_org_planch_estim', array(
            'type' => 'text',
            'id' => 'inst_org_planch_estim',
            'label' => $i18n->say('inst_org_planch_estim'),
            'placeholder' => $i18n->say('inst_org_planch_estim'),
            'tooltip' => $i18n->say('inst_org_planch_estim_tooltip'),
            'value' => $us->has('inst_org_planch_estim') ? $us->get('inst_org_planch_estim') : null,
            'validators' => array('Int'),
            'errors' => array(),
            'needed' => false,
        ));

        $this->setElement('duree_min_estim', array(
            'type' => 'text',
            'id' => 'duree_min_estim',
            'label' => $i18n->say('duree_min_estim'),
            'placeholder' => $i18n->say('duree_min_estim'),
            'tooltip' => $i18n->say('duree_min_estim_tooltip'),
            'value' => $us->has('duree_min_estim') ? $us->get('duree_min_estim') : null,
            'validators' => array('Int'),
            'errors' => array(),
            'needed' => false,
        ));

        $this->setElement('duree_max_estim', array(
            'type' => 'text',
            'id' => 'duree_max_estim',
            'label' => $i18n->say('duree_max_estim'),
            'placeholder' => $i18n->say('duree_max_estim'),
            'tooltip' => $i18n->say('duree_max_estim_tooltip'),
            'value' => $us->has('duree_max_estim') ? $us->get('duree_max_estim') : null,
            'validators' => array('Int'),
            'errors' => array(),
            'needed' => false,
        ));

		$this->setElement('croquis_id', array(
			'type' => 'hidden',
			'id' => 'croquis_id',
			'label' => $i18n->say('croquis'),
			'value' => $us->has('croquis_id') && $us->get('croquis_id') ? $us->get('croquis_id') : null,
			'validators' => array(),
			'errors' => array(),
		));
        
    
    foreach($this->getElements() as $element) {
    
      if($mode == 'show'){
        $element->set('readonly','readonly');
        $element->set('disable',true);
        
        if( $element->get('id') == 'debut' || $element->get('id') == 'fin' ) {
           if ( $element->has('class') ) { $element->rem('class') ; }
            
        } 
        
      } else if($mode == 'edit'){
          if( $element->get('id') == 'identification' ) {
              $element->set('readonly','readonly');            
        }           
          
      }
      
      
    }

	}

}