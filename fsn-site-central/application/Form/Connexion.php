<?php

class Form_Connexion extends Yab_Form {

	public function __construct(Model_User $user, $mode='show' ) {
	    
        $registry = Yab_Loader::getInstance()->getRegistry();
        // appel fichier internationalisation
        $i18n = $registry -> get('i18n');
        // gestion des accentuations / date / ....
        $session = Yab_Loader::getInstance()->getSession();
        $session->set('format','%d/%m/%Y') ;
        $filter_no_html = new Yab_Filter_NoHtml();

		$this->set('method', 'post')
		  ->set('name', 'form_login')
		  ->set('action', '')
		  ->set('role', 'form') ;
    
		$this->setElement('login', array(
			'type' => 'text',
			'id' => 'login',
			'label' => $filter_no_html->filter( $i18n -> say('login')),
      'placeholder' => $filter_no_html->filter( $i18n -> say('login')),
            // 'placeholder' => 'Code utilisateur',       
			'autofocus' => true,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('mdp', array(
			'type' => 'password',
			'id' => 'mdp',
			'label' => $filter_no_html->filter( $i18n -> say('mdp')), 
            'placeholder' => $filter_no_html->filter( $i18n -> say('mdp')),
            // 'placeholder' => 'mots de passe',
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

	}

}