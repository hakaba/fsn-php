<?php

class Form_Interpretation extends Yab_Form {

	public function __construct(Model_Interpretation $interpretation) {

		$this->set('method', 'post')->set('name', 'form_interpretation')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('interpretation', array(
			'type' => 'text',
			'id' => 'interpretation',
			'label' => 'interpretation',
			'value' => $interpretation->has('interpretation') ? $interpretation->get('interpretation') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('visible', array(
			'type' => 'text',
			'id' => 'visible',
			'label' => 'visible',
			'value' => $interpretation->has('visible') ? $interpretation->get('visible') : '1',
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

	}

}