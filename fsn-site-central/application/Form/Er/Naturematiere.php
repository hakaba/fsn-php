<?php

class Form_Er_Naturematiere extends Yab_Form {

	public function __construct($nm_id, $principal=0, $mode='add') {

		$loader = Yab_Loader::getInstance();
		$registry = $loader->getRegistry();
		$i18n = $registry->get('i18n');
		
		$this->set('method', 'post')->set('name', 'form_er_naturematiere')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$er_naturematiere = new Model_Er_Naturematiere();
		
		$this->setElement('naturematiere_list', array(
				'type' => 'select',
				'id' => 'naturematiere_list',
				'label' => $i18n->say('elementrecueilli_naturematiere'),
				'class' => 'select2_multiple form-control',
				'value' => $nm_id, //$er_naturematiere->has('naturematiere_id') ? $er_naturematiere->get('naturematiere_id') : null,
				'fake_options' => array(),
				'options' => $er_naturematiere->getTable('Model_Naturematiere')->fetchAll()->setKey('id')->setValue('naturematiere'),
				'errors' => array(),
				'multiple' => true,
				'needed' => false,
		));
		
		$this->setElement('naturematiere_principal', array(
			'type' => 'select',
			'id' => 'naturematiere_principal',
			'label' => 'Nature matière prépondérante',
			'value' => $principal, //$er_naturematiere->has('principal') ? $er_naturematiere->get('principal') : null,
			'fake_options' => array(),
			'options' => array(),
			'options' => array(),
			'errors' => array(),
			'needed' => true,
		));

		if (!empty($nm_id)) {
			$where = '';
			$compt=0;
				
			foreach ($nm_id as $cle => $value) {
				if ($compt>0)
					$where.=' or ';
				$where.='id = "' . $value . '"';
				$compt++;
			}
			$this->getElement('naturematiere_principal')->set('options',
					$er_naturematiere->getTable('Model_Naturematiere')->fetchAll()->where($where)->setKey('id')->setValue('naturematiere'));
		}
		
		if($mode == 'show'){
			foreach($this->getElements() as $element) {
		
				if ($element->get('type') == 'select' or $element->get('type') == 'checkbox')
					$element->set('disabled',true);
				else
					$element->set('readonly','readonly');
			}
		
		}
		
	}

}