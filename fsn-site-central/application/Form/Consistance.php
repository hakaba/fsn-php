<?php

class Form_Consistance extends Yab_Form {

	public function __construct(Model_Consistance $consistance) {

		$this->set('method', 'post')->set('name', 'form_consistance')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('consistance', array(
			'type' => 'text',
			'id' => 'consistance',
			'label' => 'consistance',
			'value' => $consistance->has('consistance') ? $consistance->get('consistance') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('visible', array(
			'type' => 'text',
			'id' => 'visible',
			'label' => 'visible',
			'value' => $consistance->has('visible') ? $consistance->get('visible') : '1',
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

	}

}