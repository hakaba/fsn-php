<?php

class Form_Statut extends Yab_Form {

	public function __construct(Model_Statut $statut) {

		$this->set('method', 'post')->set('name', 'form_statut')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('statut', array(
			'type' => 'text',
			'id' => 'statut',
			'label' => 'statut',
			'value' => $statut->has('statut') ? $statut->get('statut') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('visible', array(
			'type' => 'text',
			'id' => 'visible',
			'label' => 'visible',
			'value' => $statut->has('visible') ? $statut->get('visible') : '1',
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

	}

}