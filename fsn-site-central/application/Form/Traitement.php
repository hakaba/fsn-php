<?php

class Form_Traitement extends Yab_Form {

	public function __construct(Model_Traitement $traitement, $mode='add') {

		$this->set('method', 'post')->set('name', 'form_traitement')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');
    
		$loader = Yab_Loader::getInstance();
		$session = $loader->getSession();
		$registry = Yab_Loader::getInstance() -> getRegistry();
		$i18n = $registry -> get('i18n');
		$sitefouille_id = $session->has('sitefouille_id') ? $session->get('sitefouille_id') : '' ;
		 
		//Appel a la connexion BDD
		$registry = Yab_Loader::getInstance()->getRegistry(); 
		$db = Yab_Loader::getInstance()->getRegistry()->get('db');
		
		// Liste des intervenants
		$liste_intervenants = $db->prepare('SELECT 
			id, 
			CONCAT_WS(" ", nom, prenom) AS nom_complet 
		  FROM intervenant
		  ORDER BY nom
		');
			
		$req = 'SELECT
			id,numero
		  FROM elementrecueilli ';		

		if (isset($_GET['ER']) || !empty($_SESSION['idER2']))
		{
			unset($tmp);
			$tmp = $_SESSION['idER2'];	
			$req .= 'WHERE id= "'.$tmp.'" ;';
		}
			
		$liste_elementrecueilli = $db->prepare($req);

		$var1=$db->prepare('SELECT
			id,numero
		  FROM elementrecueilli
				WHERE id= 1
		  ORDER BY numero
		');
    
  	  $this->setElement('er_id', array(
    		'type' => 'select',
    		'id' => 'elementrecueilli_id',
    		'label' => 'Element recueilli',  	  		
     		'value' => $traitement->has('elementrecueilli_id') ? $traitement->get('elementrecueilli_id') : null,
		//'fake_options' => array('' => '-- Choix Element recueilli' ),
     		'options' => $liste_elementrecueilli->setKey('id')->setValue('numero'),
    		'errors' => array(),
 	   ));

  	  $this->setElement('nom', array(
  	  		'type' => 'text',
  	  		'id' => 'nom',
  	  		'label' => 'Nom',
  	  		'value' => $traitement->has('nom') ? $traitement->get('nom') : null,
  	  		'validators' => array('NotEmpty'),
  	  		'maxlength' => 50,
  	  		'errors' => array('NotEmpty' => array('Le champ nom ne doit pas être vide') ),
  	  ));
  	  
  	  $this->setElement('ref_labo', array(
			'type' => 'text',
			'id' => 'ref_labo',
			'label' => 'Reference laboratoire',
			'value' => $traitement->has('ref_labo') ? $traitement->get('ref_labo') : null,
			'validators' => array(),
  	  		'maxlength' => 50,
			'errors' => array(),
	  ));
  	  
  	  $this->setElement('type_id', array(
  	  		'type' => 'select',
  	  		'id' => 'type_id',
  	  		'label' => 'Type',
  	  		'value' => $traitement->has('type_id') ? $traitement->get('type_id') : null,
      //'fake_options' => array(''=> ' -- Choisir le Type'),
      		'fake options' => array(),
			'options' => $traitement->getTable('Model_Fsn_Categorie')->fetchAll()->where('categorie_type = "traitement_type" ')->setKey('id')->setValue('categorie_value'),
  	  		'errors' => array(),
  	  ));
  	  
		$this->setElement('debut', array(
			'type' => 'text',
			'id' => 'debut',
			'label' => 'Début',
			'placeholder' => $i18n->say("date_placeholder_format"),
			'class' => 'datepicker',
			'value' => $traitement->has('debut') && $traitement->get("debut") ? date('d/m/Y', strtotime($traitement->get('debut'))): null ,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('fin', array(
			'type' => 'text',
			'id' => 'fin',
			'label' => 'Fin',
			'class' => 'datepicker',
			'placeholder' => $i18n->say("date_placeholder_format"),
			'value' => $traitement->has('fin') && $traitement->get("fin") ? date('d/m/Y', strtotime($traitement->get('fin'))) : null ,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('lieu', array(
			'type' => 'text',
			'id' => 'lieu',
			'label' => 'Lieu',
			'value' => $traitement->has('lieu') ? $traitement->get('lieu') : null,
			'validators' => array(),
			'errors' => array(),
		));
		
		
		
		$this->setElement('observation', array(
			'type' => 'textarea',
			'rows' => '3',
			'id' => 'observation',
			'label' => 'Observation',
			'value' => $traitement->has('observation') ? $traitement->get('observation') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('etat_id', array(
			'type' => 'select',
			'id' => 'etat_id',
			'label' => 'Etat',
			'value' => $traitement->has('etat_id') ? $traitement->get('etat_id') : null,
			'fake_options' => array(),
			'options' => $traitement->getTable('Model_Fsn_Categorie')->fetchAll()->where('categorie_type = "traitement_etat" ')->setKey('id')->setValue('categorie_value'),
			'errors' => array(),
		));
		
		
		$this->setElement('intervenant_id', array(
			'type' => 'select',
			'id' => 'intervenant_id',
			'label' => 'Intervenant',
			'value' => $traitement->has('intervenant_id') ? $traitement->get('intervenant_id') : null,
			'fake_options' => array(),
			'options' => $liste_intervenants->setKey('id')->setValue('nom_complet'),
			//'options' => $traitement->getTable('Model_Intervenant')->fetchAll()->setKey('id')->setValue('titre'),
			'errors' => array(),
		));

	}

}
