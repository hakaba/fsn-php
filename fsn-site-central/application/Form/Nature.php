<?php

class Form_Nature extends Yab_Form {

	public function __construct(Model_Nature $nature) {

		$this->set('method', 'post')->set('name', 'form_nature')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('nature', array(
			'type' => 'text',
			'id' => 'nature',
			'label' => 'nature',
			'value' => $nature->has('nature') ? $nature->get('nature') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('oastatut_id', array(
			'type' => 'select',
			'id' => 'oastatut_id',
			'label' => 'oastatut_id',
			'value' => $nature->has('oastatut_id') ? $nature->get('oastatut_id') : null,
			'fake_options' => array(),
			'options' => $nature->getTable('Model_Oastatut')->fetchAll()->setKey('id')->setValue('oastatut'),
			'errors' => array(),
		));

		$this->setElement('visible', array(
			'type' => 'text',
			'id' => 'visible',
			'label' => 'visible',
			'value' => $nature->has('visible') ? $nature->get('visible') : '1',
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

	}

}