<?php

class Form_Intervention_Us extends Yab_Form {

	public function __construct(Model_Intervention_Us $intervention_us, $mode='add') {
            
        $this->set('method', 'post')
          ->set('name','form_intervention_us')
          ->set('action', '')
          ->set('role', 'form')
          ->set('class', 'form-horizontal');
    
        $loader = Yab_Loader::getInstance();
        $session = $loader->getSession();
        
        $filter_no_html = new Yab_Filter_NoHtml();
        $filter_html = new Yab_Filter_Html();
        $session->set('format','%d/%m/%Y') ;

        $registry = $loader->getRegistry();
        $i18n = $registry->get('i18n');
        
        $sitefouille_id = $session->has('sitefouille_id') ? $session->get('sitefouille_id') : '' ;
		
		//Appel a la connexion BDD
		$db = Yab_Loader::getInstance()->getRegistry()->get('db');
		
		//$intervenants = $intervention_us->getTable('Model_Intervenant')->getAllNomsComplets()->setKey('id')->setValue('nom_complet');
		$intervenants = $db->prepare('SELECT 
			id, 
			CONCAT_WS(" ", nom, prenom) AS nom_complet 
			FROM intervenant
			ORDER BY nom
		');
		
		// $intervenants = $intervenants->toArray();
		// asort($intervenants);
		$this->setElement('intervenant_id', array(
			'type' => 'select',
			'id' => 'intervenant_id',
			'label' => $filter_no_html->filter($i18n->say('intervenant_id')),
			'placeholder' => $filter_no_html->filter($i18n->say('intervenant_id')),
			'value' => $intervention_us->has('intervenant_id') ? $intervention_us->get('intervenant_id') : null,
			'fake_options' => array('' => '-- Choix intervenant' ),
			//'options' => $intervention_us->setKey('id')->setValue('nom_complet'),
			'options' => $intervenants->setKey('id')->setValue('nom_complet'),
			'errors' => array(),
		));
    
        $liste_us = $intervention_us->getTable('Model_Us')->fetchAll() ;
        if(!empty($sitefouille_id) ) {
          $liste_us =  $liste_us->where('sitefouille_id = "'.$sitefouille_id.'" ' ) ;
        }      
        
		$this->setElement('us_id', array(
			'type' => 'select',
			'id' => 'us_id',
			'label' => $filter_no_html->filter($i18n->say('us')),
            'placeholder' => $filter_no_html->filter($i18n->say('us')),
			'value' => $intervention_us->has('us_id') ? $intervention_us->get('us_id') : null,
			'fake_options' => array('' => '-- Choix US' ),
			'options' => $liste_us->setKey('id')->setValue('identification'),
			'errors' => array(),
		));
		
		$this->setElement('debut', array(
			'type' => 'text',
			'id' => 'debut',
			'label' => $filter_no_html->filter($i18n->say('debut')),
			'placeholder' => $filter_no_html->filter($i18n->say('debut')),
			'class' => 'form-control datepicker',
			'value' => $intervention_us->has('debut') ? date('d/m/Y', strtotime($intervention_us->get('debut'))) : null,
			'validator' => array('Date'=>array('format' => 'dd/mm/yyyy')),
			'errors' => array(),
		));

		$this->setElement('fin', array(
			'type' => 'text',
			'id' => 'fin',
			'label' => $filter_no_html->filter($i18n->say('fin')),
            'placeholder' => $filter_no_html->filter($i18n->say('fin')),
			'class' => 'form-control datepicker',
			'value' => $intervention_us->has('fin') ? date('d/m/Y', strtotime($intervention_us->get('fin'))) : null,
			'validator' => array('Date'=>array('format' => 'dd/mm/yyyy')),
			'errors' => array(),
		));    
    
		$this->setElement('type_id', array(
			'type' => 'select',
			'id' => 'type_id',
			'label' => $filter_no_html->filter($i18n->say('typeinterv')),
            'placeholder' => $filter_no_html->filter($i18n->say('typeinterv')),
			'value' => $intervention_us->has('type_id') ? $intervention_us->get('type_id') : null,
			'fake_options' => 'Non déterminé',
			'needed' => false,
			'options' => $intervention_us->getTable('Model_Typeintervention')->fetchAll()->setKey('id')->setValue('typeinterv'),
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));
  
    foreach($this->getElements() as $element) {
    
      if($mode == 'show'){
        $element->set('readonly','readonly');
        $element->set('disable',true);
      }
      
    }
  
  }

}