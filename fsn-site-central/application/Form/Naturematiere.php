<?php

class Form_Naturematiere extends Yab_Form {

	public function __construct(Model_Naturematiere $naturematiere) {

		$this->set('method', 'post')->set('name', 'form_naturematiere')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('naturematiere', array(
			'type' => 'text',
			'id' => 'naturematiere',
			'label' => 'naturematiere',
			'value' => $naturematiere->has('naturematiere') ? $naturematiere->get('naturematiere') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('visible', array(
			'type' => 'text',
			'id' => 'visible',
			'label' => 'visible',
			'value' => $naturematiere->has('visible') ? $naturematiere->get('visible') : '1',
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

	}

}