<?php

class Form_Troncon extends Yab_Form {

	public function __construct(Model_Troncon $troncon, $mode = 'add') {
		$this->set('method', 'post')->set('name', 'form_troncon')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$loader = Yab_Loader::getInstance() ;
		$registry = $loader -> getRegistry();
		$session = $loader ->getSession();

		// appel fichier internationalisation
		$i18n = $registry -> get('i18n');
		$us = new Model_Unitesondage($troncon->has('unitesondage_id') ? $troncon->get('unitesondage_id') : $session->get('unitesondage_id'));
		$filter_no_html = new Yab_Filter_NoHtml();

		$this->setElement('unitesondage_id', array(
				'type' => 'hidden',
				'id' => 'unitesondage_id',
				'label' => $filter_no_html->filter( $i18n -> say('unitesondage_id') ),
				'placeholder' => $filter_no_html->filter( $i18n -> say('unitesondage_id') ),
				'value' => $us->get('id'),
				'intitule' => $us->get('identification'),
				'readonly' => true,
				'errors' => array(),
		));

		$value_identifiant = $troncon->has('identification') ? $troncon->get('identification') : $us->get('nom').'.'.$troncon->getNextSequenceTronconId($us->get('id'));
		$exp_ident = explode(".",$value_identifiant);
		$prefix_identidiant = isset($exp_ident[0])?$exp_ident[0]:null;
		$numero_usondage = isset($exp_ident[1])?$exp_ident[1]:null;

		$this->setElement('id', array(
				'type' => 'hidden',
				'id' => 'id_troncon',
				'value' => $troncon->has('id') ? $troncon->get('id') : null,
				'readonly' => true,
				'validators' => array(),
				'errors' => array()
		));

		$this->setElement('identification', array(
				'type' => 'text',
				'id' => 'identification',
				'prefix' => $prefix_identidiant,
				'label' => $i18n->say('identification'),
				'placeholder' => $i18n->say('identification'),
				'tooltip' => $filter_no_html->filter( $i18n -> say('unitesondage_tooltip') ),
				'value' => $troncon->has('identification') ? $troncon->get('identification') : str_replace(" ", "_", $prefix_identidiant).".troncon.".$numero_usondage,
				'numero' => $numero_usondage,
				'validators' => array('NotEmpty'),
				'errors' => array('NotEmpty' => array('Le champ identification ne doit pas être vide') ),
		));

		$this->setElement('nom', array(
				'type' => 'text',
				'id' => 'nom',
				'label' => $filter_no_html->filter( $i18n -> say('unitesondage_nom') ),
				'placeholder' => $filter_no_html->filter( $i18n -> say('unitesondage_nom') ),
				'tooltip' => $filter_no_html->filter( $i18n -> say('unitesondage_tooltip') ),
				'value' => $troncon->has('nom') ? $troncon->get('nom') : null,
				'validators' => array('NotEmpty'),
				'errors' => array(),
		));
		
		$this->setElement('fai_id', array(
				'type' => 'select',
				'id' => 'fai_id',
				'label' => $filter_no_html->filter( $i18n -> say('fai') ),
				'tooltip' => $filter_no_html->filter( $i18n -> say('fai') ),
				'value' => $troncon->has('fai_id') ? $troncon->get('fai_id') : null,
				'options' => $troncon->getTable('fai')->fetchAll()->where('sitefouille_id="'.$session->get('sitefouille_id').'"')->setKey('id')->setValue('identification'),
				'fake_options' => array(null => 'Aucun FAI'),
				'validators' => array(),
				'errors' => array(),
				'needed' => false
		));

		$this->setElement('description', array(
				'type' => 'textarea',
				'rows' => '3',
				'id' => 'description',
				'label' => $filter_no_html->filter( $i18n -> say('description') ),
				'placeholder' => $filter_no_html->filter( $i18n -> say('description') ),
				'value' => $troncon->has('description') ? $troncon->get('description') : null,
				'validators' => array(),
				'errors' => array(),
		));
		
		$this->setElement('largeur', array(
				'type' => 'text',
				'id' => 'largeur',
				'label' => $filter_no_html->filter( $i18n -> say('largeur') ),
				'tooltip' => $filter_no_html->filter( $i18n -> say('largeur') ),
				'placeholder' => $filter_no_html->filter( $i18n -> say('largeur') ),
				'value' => $troncon->has('largeur') ? $troncon->get('largeur') : null,
				'maxlength' => 4,
				'validators' => array('NotEmpty'),
				'errors' => array(),
		));
		
		$this->setElement('longueur', array(
				'type' => 'text',
				'id' => 'longueur',
				'label' => $filter_no_html->filter( $i18n -> say('longueur') ),
				'tooltip' => $filter_no_html->filter( $i18n -> say('longueur') ),
				'placeholder' => $filter_no_html->filter( $i18n -> say('longueur') ),
				'value' => $troncon->has('longueur') ? $troncon->get('longueur') : null,
				'maxlength' => 4,
				'validators' => array('NotEmpty'),
				'errors' => array(),
		));
		
		$this->setElement('profondeur', array(
				'type' => 'text',
				'id' => 'profondeur',
				'label' => $filter_no_html->filter( $i18n -> say('profondeur') ),
				'tooltip' => $filter_no_html->filter( $i18n -> say('profondeur') ),
				'placeholder' => $filter_no_html->filter( $i18n -> say('profondeur') ),
				'value' => $troncon->has('profondeur') ? $troncon->get('profondeur') : null,
				'maxlength' => 4,
				'validators' => array('NotEmpty'),
				'errors' => array(),
		));
		
		$this->setElement('date_debut', array(
				'type' => 'text',
				'id' => 'date_debut',
				'label' => $filter_no_html->filter( $i18n -> say('debut_fouille') ),
				'placeholder' => '01/01/1900',
				'class' => 'form-control datepicker',
				'tooltip' => $filter_no_html->filter( $i18n -> say('debut_fouille') ),
				'value' => $troncon->has('date_debut') ? $troncon->get('date_debut') : '01/01/1900',
				'validators' => array('Date'=>array('format' => 'dd/mm/yyyy')),
				'errors' => array(),
		));
		
		$this->setElement('date_fin', array(
				'type' => 'text',
				'id' => 'date_fin',
				'label' => $filter_no_html->filter( $i18n -> say('fin_fouille') ),
				'placeholder' => '01/01/2016',
				'class' => 'form-control datepicker',
				'tooltip' => $filter_no_html->filter( $i18n -> say('fin_fouille') ),
				'value' => $troncon->has('date_fin') ? $troncon->get('date_fin') : '01/01/2016',
				'validators' => array('Date'=>array('format' => 'dd/mm/yyyy')),
				'errors' => array(),
		));

	}

}