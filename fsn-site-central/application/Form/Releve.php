<?php

class Form_Releve extends Yab_Form {

	public function __construct(Model_Releve $releve) {

		$this->set('method', 'post')->set('action', '')->set('role', 'form');

		$this->setElement('croquis_id', array(
			'type' => 'select',
			'id' => 'croquis_id',
			'label' => 'croquis_id',
			'value' => $releve->has('croquis_id') && $releve->get('croquis_id') ? $releve->get('croquis_id') : null,
			'fake_options' => array(),
			'options' => $releve->getTable('Model_Croquis')->fetchAll()->setKey('id')->setValue('file_name'),
			'errors' => array(),
		));

		$this->setElement('sitefouille_id', array(
			'type' => 'select',
			'id' => 'sitefouille_id',
			'label' => 'sitefouille_id',
			'value' => $releve->has('sitefouille_id') ? $releve->get('sitefouille_id') : null,
			'fake_options' => array(),
			'options' => $releve->getTable('Model_Sitefouille')->fetchAll()->setKey('id')->setValue('nom'),
			'errors' => array(),
		));

	}

}