<?php

class Form_Etattraitement extends Yab_Form {

	public function __construct(Model_Etattraitement $etattraitement) {

		$this->set('method', 'post')->set('name', 'form_etattraitement')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('etat', array(
			'type' => 'text',
			'id' => 'etat',
			'label' => 'etat',
			'value' => $etattraitement->has('etat') ? $etattraitement->get('etat') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('visible', array(
			'type' => 'text',
			'id' => 'visible',
			'label' => 'visible',
			'value' => $etattraitement->has('visible') ? $etattraitement->get('visible') : '1',
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

	}

}