<?php

class Form_Extorganisme extends Yab_Form {

	public function __construct(Model_Extorganisme $extorganisme) {

		$this->set('method', 'post')->set('name', 'form_extorganisme')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('nom', array(
			'type' => 'text',
			'id' => 'nom',
			'label' => 'nom',
			'value' => $extorganisme->has('nom') ? $extorganisme->get('nom') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('adresse', array(
			'type' => 'text',
			'id' => 'adresse',
			'label' => 'adresse',
			'value' => $extorganisme->has('adresse') ? $extorganisme->get('adresse') : null,
			'validators' => array(),
			'errors' => array(),
		));

	}

}