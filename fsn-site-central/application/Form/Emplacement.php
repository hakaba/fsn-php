<?php

class Form_Emplacement extends Yab_Form {

	public function __construct(Model_Emplacement $emplacement, $mode='add') {
		    
        $registry = Yab_Loader::getInstance() -> getRegistry();
        // appel fichier internationalisation
        $i18n = $registry -> get('i18n');
        // gestion des accentuations / date / ....
        $session = Yab_Loader::getInstance()->getSession();
        $session->set('format','%d/%m/%Y') ;
        $filter_no_html = new Yab_Filter_NoHtml();
        // profil utilisateur Super Admin
        $user_info = $session -> get('session');
        $login = $user_info['login'];
        $entiteadmin_id = !empty($user_info['entiteadmin_id']) ? $user_info['entiteadmin_id'] : 1 ;

		$this->set('method', 'post')->set('name', 'form_emplacement')->set('action', '')->set('role', 'form');
        
        $entiteadmin_id = $emplacement->has('entiteadmin_id') ? $emplacement->get('entiteadmin_id') : $entiteadmin_id ;

        $liste_parent = $emplacement->getTable('Model_Emplacement')
            ->fetchAll()
            ->where('entiteadmin_id = "'.$entiteadmin_id.'"')
            ->setKey('id')
            ->setValue('nom') ; 
        
        $this->setElement('parent_id', array(
            'type' => 'select',
            'id' => 'parent_id',
            'label' => $filter_no_html->filter( $i18n -> say('parent_id')),
            'placeholder' => $filter_no_html->filter( $i18n -> say('parent_id')),            
            'value' => $emplacement->has('parent_id') ? $emplacement->get('parent_id') : null,
            'fake_options' => array(''=> ' -- Choisir le '.$filter_no_html->filter( $i18n -> say('parent_id')) ),
            'options' => $liste_parent,
            'errors' => array(),
        ));
        
		$this->setElement('nom', array(
			'type' => 'text',
			'id' => 'nom',
            'label' => $filter_no_html->filter( $i18n -> say('nom')),
            'placeholder' => $filter_no_html->filter( $i18n -> say('nom')),
			'value' => $emplacement->has('nom') ? $emplacement->get('nom') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));
        
        $liste_type = $emplacement->getTable('Model_Fsn_Categorie')
            ->fetchAll()
            ->where('categorie_type = "emplacement_type"')
            ->orderBy(array('ordre'=>'ASC') )
            ->setKey('id')
            ->setValue('categorie_value') ;
        
        $this->setElement('type_id', array(
            'type' => 'select',
            'id' => 'type_id',
            'label' => $filter_no_html->filter( $i18n -> say('type_id')),
            'placeholder' => $filter_no_html->filter( $i18n -> say('type_id')),            
            'value' => $emplacement->has('type_id') ? $emplacement->get('type_id') : null,
            'fake_options' => array(''=> ' -- Choisir le Type'),
            'options' => $liste_type,
            'errors' => array(),
        ));

        $this->setElement('entiteadmin_id', array(
            'type' => 'select',
            'id' => 'entiteadmin_id',
            'label' => $filter_no_html->filter( $i18n -> say('entiteadmin_id')),
            'placeholder' => $filter_no_html->filter( $i18n -> say('entiteadmin_id')),            
            'value' => $emplacement->has('entiteadmin_id') ? $emplacement->get('entiteadmin_id') : $entiteadmin_id,
            // 'fake_options' => array(''=> ' -- Choisir le Type'),
            'options' => $emplacement->getTable('Model_Fsn_Entiteadmin')->getEntiteAdministrativeDetail()->where('fe.id = '.$entiteadmin_id)->setKey('id')->setValue('organisme_nom'),
            'errors' => array(),
        ));
                        
		$this->setElement('description', array(
            'type' => 'textarea',
            'rows' => '3',
            'id' => 'description',
            'label' => $filter_no_html->filter( $i18n -> say('description')),
            'placeholder' => $filter_no_html->filter( $i18n -> say('description')),
			'value' => $emplacement->has('description') ? $emplacement->get('description') : null,
			'validators' => array(),
			'errors' => array(),
		));

       foreach($this->getElements() as $element) {

          if($mode == 'show'){
            $element->set('readonly','readonly');
            $element->set('disable',true);
          }
          
        }
    
    }

}