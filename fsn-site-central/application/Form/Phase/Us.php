<?php

class Form_Phase_Us extends Yab_Form {

	public function __construct(Model_Phase_Us $phase_us) {

		$this->set('method', 'post')->set('name', 'form_phase_us')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('incertitude', array(
			'type' => 'text',
			'id' => 'incertitude',
			'label' => 'incertitude',
			'value' => $phase_us->has('incertitude') ? $phase_us->get('incertitude') : null,
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

	}

}