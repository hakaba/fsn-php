<?php

class Form_Interfait extends Yab_Form {

	public function __construct(Model_Interfait $interfait) {
		
		// appel fichier internationalisation
		$registry = Yab_Loader::getInstance() -> getRegistry();
		$i18n = $registry -> get('i18n');
		$filter_no_html = new Yab_Filter_NoHtml();
		
		$this->set('method', 'post')->set('action', '')->set('role', 'form');
		$db = Yab_Loader::getInstance()->getRegistry()->get('db');
		
		$select_categories = $db->prepare('SELECT 
      		id, 
        	categorie_object, 
        	categorie_type, 
        	categorie_key, 
        	categorie_value, 
        	color, 
        	system, 
        	visible, 
        	ordre, 
        	description
        FROM fsn_categorie
      ') 
	    ->where('categorie_object = "fai"')
		->where('categorie_type = "fai_interfai_type"');
		
		//'options' => $interfait->getTable('Model_Fsn_Categorie')->fetchAll()->setKey('id')->setValue('categorie_value')->where('categorie_type like "fai%" and visible = 1 '),  
		$this->setElement('typeinterfait_id', array(
			'type' => 'select',
			'id' => 'typeinterfait_id',
			'label' => 'typeinterfait_id',
			'value' => $interfait->has('typeinterfait_id') ? $interfait->get('typeinterfait_id') : null,
			'fake_options' => array(),
			'options' => $interfait->getOnlyFaiType()->setKey('id')->setValue('categorie_value'),
			'typeAssociation' => $select_categories->setKey('id')->setValue('categorie_value'),	
			'validators' => array('Int'),
			'errors' => array(),
		));
				
		$this->setElement('fai_id', array(
			'type' => 'text',
			'id' => 'fai_id',
			'label' => 'fai_id',
			'value' => $interfait->has('fai_id') ? $interfait->get('fai_id') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		// sitefouille_id session value
		$loader = Yab_Loader::getInstance();		
		$fai = $interfait->getTable('Model_Fai')->getAllFaiFromSf($loader->getSession()->get('sitefouille_id'))->setKey('id')->setValue('identification');
		
		$this->setElement('fai_id_1', array(
			'type' => 'select',
			'id' => 'fai_id_1',
			'label' => 'fai_id_1',
			'value' => $interfait->has('fai_id_1') ? $interfait->get('fai_id_1') : null,
			'options' => $fai,
			'fake_options' => array(),
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('incertitude', array(
			'type' => 'text',
			'id' => 'incertitude',
			'label' => $filter_no_html->filter( $i18n -> say('incertitude') ),
			'value' => $interfait->has('incertitude') ? $interfait->get('incertitude') : null,
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('precisions', array(
			'type' => 'textarea',
			'id' => 'precisions',
			'label' => $filter_no_html->filter( $i18n -> say('precisions') ),
			'value' => $interfait->has('precisions') ? $interfait->get('precisions') : null,
			'validators' => array(),
			'errors' => array(),
		));

	}

}