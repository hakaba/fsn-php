<?php

class Form_Elementrecueilli extends Yab_Form {

	public function __construct(Model_Elementrecueilli $elementrecueilli, Model_Us $us, $mode='add') {
		$loader = Yab_Loader::getInstance();

		$this->set('method', 'post')->set('name', 'form_elementrecueilli')->set('action', '')->set('role', 'form');

		$registry = $loader->getRegistry();
		$i18n = $registry->get('i18n');
		
		//recupération de l'emplacement choisi
		$emplacement_choisi='';
		
		if ($elementrecueilli->has('emplacement_id')) {
			if ($elementrecueilli->get('emplacement_id') != null) {
				$emplacement = new Model_Emplacement($elementrecueilli->get('emplacement_id'));
				$emplacement_choisi = !$emplacement->has('nom') ? '' : $emplacement->get('nom');
			} else if ($elementrecueilli->get('conteneur_id') != null ) {
				$conteneur = new Model_Conteneur($elementrecueilli->get('conteneur_id'));
				$emplacement_choisi = !$conteneur->has('nom') ? '' : $conteneur->get('nom');
			}
			  
		}
		
		
		$this->setElement('us_id', array(
			'type' => 'hidden',
			'id' => 'us_id',
			'label' => $i18n->say('us_id'),
			'value' => $us->get('id'),
			'errors' => array(),
			'readonly' => true,
		));

		
		$this->setElement('us_identification', array(
				'type' => 'text',
				'id' => 'us_identification',
				'label' => $i18n->say('elementrecueilli_us_id'),
				'value' => $us->get('identification'),
				'validators' => array(),
				'errors' => array(),
				'readonly' => true,
				'needed' => true,
		));

		$this->setElement('intervenant_id', array(
			'type' => 'select',
			'id' => 'intervenant_id',
			'label' => $i18n->say('intervenant_id'),
			'value' => $elementrecueilli->has('intervenant_id') ? $elementrecueilli->get('intervenant_id') : null,
			'fake_options' => array(null => 'Non déterminé'),
			'options' => $elementrecueilli->getTable('Model_Intervenant')->getAllNoms()->setKey('id')->setValue('nom_complet'),
			'errors' => array(),
			'needed' => false,
		));

		$this->setElement('elementisole', array(
			'type' => 'checkbox',
			'id' => 'elementisole',
			'label' => $i18n->say('elementisole'),
			'value' => $elementrecueilli->has('elementisole') ? $elementrecueilli->get('elementisole') : 1,
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
			'needed' => true,
		));

		//traitement des différents numéros
		$isole=$elementrecueilli->has('elementisole') ? $elementrecueilli->get('elementisole') : 1;
		if ($isole==1) {
			$other_isole=0;
			$sufixe='.';
			$other_sufixe='.lot.';
		} else {
			$other_isole=1;
			$sufixe='.lot.';
			$other_sufixe='.';
		}
		
		$value_identifiant = $elementrecueilli->has('numero') ? $elementrecueilli->get('numero') :  $us->get('identification') . $sufixe . $elementrecueilli->getNextSequenceErId($us->get('id'), $isole);
		$other_identifiant =  $us->get('identification') . $other_sufixe . $elementrecueilli->getNextSequenceErId($us->get('id'), $other_isole);
		
		$this->setElement('numero_isole', array(
				'type' => 'hidden',
				'id' => 'numero_isole',
				'label' => 'Numéo',
				'value' => $isole == 1 ? $value_identifiant : $other_identifiant,
				'validators' => array(),
				'errors' => array(),
				'needed' => true,
				'readonly' => true,
		));
		
		$this->setElement('numero_non_isole', array(
				'type' => 'hidden',
				'id' => 'numero_non_isole',
				'label' => 'Numéo',
				'value' => $isole ==  0 ? $value_identifiant : $other_identifiant,
				'validators' => array(),
				'errors' => array(),
				'needed' => true,
				'readonly' => true,
		));
		
		$this->setElement('numero', array(
			'type' => 'text',
			'id' => 'numero',
			'label' => $i18n->say('numero'),
			//'value' => $elementrecueilli->has('numero') ? $elementrecueilli->get('numero') : null,
			'value' => $value_identifiant,
			'validators' => array(),
			'errors' => array(),
			'needed' => true,
				'readonly' => true,
		));

		$this->setElement('description', array(
			'type' => 'textarea',
      		'rows' => '3',
			'id' => 'description',
			'label' => $i18n->say('description'),
			'value' => $elementrecueilli->has('description') ? $elementrecueilli->get('description') : null,
			'validators' => array(),
			'errors' => array(),
			'needed' => false,
		));

		$this->setElement('intact', array(
			'type' => 'checkbox',
			'id' => 'intact',
			'class' => 'tribox form-control',
			'label' => $i18n->say('intact'),
      		'value' => $elementrecueilli->has('intact') ? $elementrecueilli->get('intact') : 'null',
			'validators' => array(),
			'errors' => array(),
      		'needed' => false,
		));

		//date_default_timezone_set('UTC'); -- jfb 2016-04-21 correctif fiche mantis 229
		
		$this->setElement('datedecouverte', array(
			'type' => 'text',
			'id' => 'datedecouverte',
			'label' => $i18n->say('datedecouverte'),
			'class' => 'datepicker',
      		'value' => $elementrecueilli->has('datedecouverte') ? date('d/m/Y', strtotime($elementrecueilli->get('datedecouverte'))) : date('d/m/Y'),
			'validators' => array(),
			'errors' => array(),
			'needed' => true,
			'readonly' => true,
		));


		$this->setElement('elementpoterie_id', array(
			'type' => 'hidden',
			'id' => 'elementpoterie_id',
			'label' => 'elementpoterie_id',
			'value' => $elementrecueilli->has('elementpoterie_id') ? $elementrecueilli->get('elementpoterie_id') : null,
      		'errors' => array(),
      		'needed' => false,
		));

		$this->setElement('precisionfonction', array(
			'type' => 'text',
      		'rows' => '3',
			'id' => 'precisionfonction',
			'label' => $i18n->say('precisionfonction'),
			'value' => $elementrecueilli->has('precisionfonction') ? $elementrecueilli->get('precisionfonction') : null,
			'validators' => array(),
			'errors' => array(),
			'needed' => false,
		));

		$this->setElement('statut_id', array(
			'type' => 'select',
			'id' => 'statut_id',
			'label' => $i18n->say('statut_id'),
			'value' => $elementrecueilli->has('statut_id') ? $elementrecueilli->get('statut_id') : null,
			'fake_options' => array(null => 'Non déterminé'),
			'options' => $elementrecueilli->getTable('Model_Statut')->fetchAll()->setKey('id')->setValue('statut'),
			'errors' => array(),
			'needed' => false,
		));

		$this->setElement('quantite', array(
			'type' => 'text',
			'id' => 'quantite',
			'label' => $i18n->say('quantite'),
			'value' => $elementrecueilli->has('quantite') ? $elementrecueilli->get('quantite') : null,
			'validators' => array('Int'),
			'errors' => array(),
      		'needed' => false,
		));

		$this->setElement('poids', array(
			'type' => 'text',
			'id' => 'poids',
			'label' => $i18n->say('poids'),
			'value' => $elementrecueilli->has('poids') ? $elementrecueilli->get('poids') : null,
			'validators' => array('Float'),
			'errors' => array(),
      		'needed' => false,
		));

		$this->setElement('longueur', array(
			'type' => 'text',
			'id' => 'longueur',
			'label' => $i18n->say('longueur'),
			'value' => $elementrecueilli->has('longueur') ? $elementrecueilli->get('longueur') : null,
			'validators' => array('Float'),
			'errors' => array(),
      		'needed' => false,
		));

		$this->setElement('largeur', array(
			'type' => 'text',
			'id' => 'largeur',
			'label' => $i18n->say('largeur'),
			'value' => $elementrecueilli->has('largeur') ? $elementrecueilli->get('largeur') : null,
			'validators' => array('Float'),
			'errors' => array(),
      		'needed' => false,
		));

		$this->setElement('hauteur', array(
			'type' => 'text',
			'id' => 'hauteur',
			'label' => $i18n->say('hauteur'),
			'value' => $elementrecueilli->has('hauteur') ? $elementrecueilli->get('hauteur') : null,
			'validators' => array('Float'),
			'errors' => array(),
      		'needed' => false,
		));

		$this->setElement('diametre', array(
			'type' => 'text',
			'id' => 'diametre',
			'label' => $i18n->say('diametre'),
			'value' => $elementrecueilli->has('diametre') ? $elementrecueilli->get('diametre') : null,
			'validators' => array('Float'),
			'errors' => array(),
      		'needed' => false,
		));

		$this->setElement('incertitude', array(
			'type' => 'text',
			'id' => 'incertitude',
			'label' => $i18n->say('incertitude'),
			'value' => $elementrecueilli->has('incertitude') ? $elementrecueilli->get('incertitude') : null,
			'validators' => array('Float'),
			'errors' => array(),
      		'needed' => false,
		));

		$this->setElement('emplacement_id', array(
			'type' => 'hidden',
			'id' => 'emplacement_id',
			'label' => 'Emplacement',
			'value' => $elementrecueilli->has('emplacement_id') ? $elementrecueilli->get('emplacement_id') : null,
			'errors' => array(),
      		'needed' => false,
		));

		$this->setElement('conteneur_id', array(
				'type' => 'hidden',
				'id' => 'conteneur_id',
				'label' => 'Conteneur',
				'value' => $elementrecueilli->has('conteneur_id') ? $elementrecueilli->get('conteneur_id') : null,
				'errors' => array(),
				'needed' => false,
		));
		
		$this->setElement('emplacement_choisi', array(
				'type' => 'text',
				'id' => 'emplacement_choisi',
				'label' => 'Emplacement choisi',
				'value' => $emplacement_choisi,
				'validators' => array(),
				'errors' => array(),
				'needed' => false,
		));
		
		$this->setElement('fonction_id', array(
			'type' => 'select',
			'id' => 'fonction_id',
			'label' => $i18n->say('fonction_id'),
			'value' => $elementrecueilli->has('fonction_id') ? $elementrecueilli->get('fonction_id') : null,
			'fake_options' => array(null => 'Non déterminé'),
			'options' => $elementrecueilli->getTable('Model_Fonction')->fetchAll()->setKey('id')->setValue('fonction'),
			'errors' => array(),
			'needed' => false,
		));

		$this->setElement('classe_id', array(
			'type' => 'select',
			'id' => 'classe_id',
			'label' => $i18n->say('classe_id'),
			'value' => $elementrecueilli->has('classe_id') ? $elementrecueilli->get('classe_id') : null,
			'fake_options' => array(null => 'Non déterminé'),
			'options' => $elementrecueilli->getTable('Model_Classe')->fetchAll()->setKey('id')->setValue('classe'),
			'errors' => array(),
			'needed' => false,
		));

		$this->setElement('etat_id', array(
			'type' => 'select',
			'id' => 'etat_id',
			'label' => $i18n->say('etat'),
			'value' => $elementrecueilli->has('etat_id') ? $elementrecueilli->get('etat_id') : null,
			'fake_options' => array(null => 'Non déterminé'),
			'options' => $elementrecueilli->getTable('Model_Etat')->fetchAll()->setKey('id')->setValue('etat'),
			'errors' => array(),
			'needed' => false,
		));

		$this->setElement('datationdebreal_certain', array(
			'type' => 'text',
			'id' => 'datationdebreal_certain',
			'label' => $i18n->say('datationdebreal_certain'),
			'value' => $elementrecueilli->has('datationdebreal_certain') ? $elementrecueilli->get('datationdebreal_certain') : null,
			'validators' => array('Int'),
			'errors' => array(),
			//'errors' => array('Int', array('Le champ doit correspondre � une ann�e.')),
			'needed' => false,
		));

		$this->setElement('datationfinutil_certain', array(
			'type' => 'text',
			'id' => 'datationfinreal_certain',
			'label' => $i18n->say('datationfinutil_certain'),
			'value' => $elementrecueilli->has('datationfinutil_certain') ? $elementrecueilli->get('datationfinutil_certain') : null,
			//'validators' => array('Int,GreaterThan(datationdebreal_certain)'),
			'validators' => array('Int', 'GreaterThan' => array('than'=> $this->getElement('datationdebreal_certain')->get('value'))),
			//'errors' => array('Int' => 'Le champ doit correspondre � une ann�e.'),
			'needed' => false,
		));

		$this->setElement('datationdebreal_estim', array(
			'type' => 'text',
			'id' => 'datationdebreal_estim',
			'label' => $i18n->say('datationdebreal_estim'),
			'value' => $elementrecueilli->has('datationdebreal_estim') ? $elementrecueilli->get('datationdebreal_estim') : null,
			'validators' => array('Int'),
			'errors' => array(),
			'needed' => false,
		));

		$this->setElement('datationfinutil_estim', array(
			'type' => 'text',
			'id' => 'datationfinreal_estim',
			'label' => $i18n->say('datationfinutil_estim'),
			'value' => $elementrecueilli->has('datationfinutil_estim') ? $elementrecueilli->get('datationfinutil_estim') : null,
			'validators' => array('Int', 'GreaterThan' => array('than'=> $this->getElement('datationdebreal_estim')->get('value'))),
			'errors' => array(),
			'needed' => false,
		));

        
       //traitements relatifs à la page de visualisation
		if($mode == 'show'){
       		foreach($this->getElements() as $element) {

       			if ($element->get('type') == 'select' or $element->get('type') == 'checkbox')
       				$element->set('disabled',true);
       			else if ($element->get('id') == 'datedecouverte')
       				$element->set('class', null);
       			else 
            		$element->set('readonly','readonly');
            	
          }
          
        }

	}

}