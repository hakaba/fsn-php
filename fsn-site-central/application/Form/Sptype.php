<?php

class Form_Sptype extends Yab_Form {

	public function __construct(Model_Sptype $sptype) {

		$this->set('method', 'post')->set('name', 'form_sptype')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('sptype', array(
			'type' => 'text',
			'id' => 'sptype',
			'label' => 'sptype',
			'value' => $sptype->has('sptype') ? $sptype->get('sptype') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('visible', array(
			'type' => 'text',
			'id' => 'visible',
			'label' => 'visible',
			'value' => $sptype->has('visible') ? $sptype->get('visible') : '1',
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

	}

}