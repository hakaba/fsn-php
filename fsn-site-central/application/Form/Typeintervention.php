<?php

class Form_Typeintervention extends Yab_Form {

	public function __construct(Model_Typeintervention $typeintervention) {

		$this->set('method', 'post')->set('name', 'form_typeintervention')->set('action', '')->set('role', 'form');

		$this->setElement('typeinterv', array(
			'type' => 'text',
			'id' => 'typeinterv',
			'label' => 'typeinterv',
			'value' => $typeintervention->has('typeinterv') ? $typeintervention->get('typeinterv') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('visible', array(
			'type' => 'text',
			'id' => 'visible',
			'label' => 'visible',
			'value' => $typeintervention->has('visible') ? $typeintervention->get('visible') : '1',
			'validators' => array('Int'),
			'errors' => array(),
		));

	}

}