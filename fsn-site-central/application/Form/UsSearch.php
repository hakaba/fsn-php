<?php

class Form_UsSearch extends Yab_Form {

	public function __construct(Model_Us $us ) {
    
    
    $loader = Yab_Loader::getInstance();
    $session = $loader->getSession();
          
    // Definition entete formulaire
    $this->set('method', 'post')
          ->set('name', 'formUsSearch')
          ->set('action', '')                           
          ->set('role', 'form')                         
          ->set('class', 'form-inline');
          
          
    $sitefouille_id = $session->get('sitefouille_id');
    $order_by = array ('us.identification' => 'ASC') ;     
		$select_us = $us->fetchAll()->orderBy($order_by)->where('us.sitefouille_id = "'.$sitefouille_id.'" ' ) ;
    
    $this->setElement('identification', array(
			'type' => 'select',
      'multiple' => true,
      'size' => 15,
			'id' => 'identification',
			'label' => 'identification',
			'value' => $us->has('identification') ? $us->get('identification') : null,
			'options' => $select_us->setKey('identification')->setValue('identification'),
      'fake_options' => array('' => '-- Veuillez faire un choix'),        
			'onchange' => 'submit()',
      'validators' => array(),
			'errors' => array(),
		));
/*
		$this->setElement('description', array(
			'type' => 'textarea',
      'rows' => '3',
			'id' => 'description',
			'label' => 'description',
			'value' => $us->has('description') ? $us->get('description') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('debut', array(
			'type' => 'text',
			'id' => 'debut',
			'label' => 'debut',
      'class' => 'datepicker',
			'value' => $us->has('debut') ? $us->get('debut') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('fin', array(
			'type' => 'text',
			'id' => 'fin',
			'label' => 'fin',
      'class' => 'datepicker',
			'value' => $us->has('fin') ? $us->get('fin') : null,
			'validators' => array(),
			'errors' => array(),
		));
*/

	}

}