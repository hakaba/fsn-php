<?php

class Form_Classe extends Yab_Form {

	public function __construct(Model_Classe $classe) {

		$this->set('method', 'post')->set('name', 'form_classe')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('classe', array(
			'type' => 'text',
			'id' => 'classe',
			'label' => 'classe',
			'value' => $classe->has('classe') ? $classe->get('classe') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('visible', array(
			'type' => 'text',
			'id' => 'visible',
			'label' => 'visible',
			'value' => $classe->has('visible') ? $classe->get('visible') : '1',
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

	}

}