<?php
 
class Form_Detailconteneur extends Yab_Form {
 
   public function __construct(Model_Conteneur $conteneur) {
 
       $this->set('method', 'post')->set('name', 'form_detailconteneur')->set('action', '')->set('role', 'form');
 
       $this->setElement('nom', array(
           'type' => 'text',
           'id' => 'nom',
           'readonly' =>'true',
           'label' => 'Nom',
           'class'=>'color inputColor input_style',
           'value' => $conteneur->has('nom') ? $conteneur->get('nom') : null,
           'validators' => array('NotEmpty'),
           'errors' => array(),
       ));
 
       $this->setElement('description', array(
           'type' => 'textarea',
           'rows' => '3',
           'id' => 'description',
           'readonly' =>'true',
           'label' => 'description',
               'class' =>'color textarea_style',
               'value' => $conteneur->has('description') ? $conteneur->get('description') : null,
           'validators' => array(),
           'errors' => array(),
       ));
 
       $this->setElement('codebarre', array(
           'type' => 'text',
           'id' => 'codebarre',
           'readonly' =>'true',
               'label' => 'Code-barres',
           'value' => $conteneur->has('codebarre') ? $conteneur->get('codebarre') : null,
           'class' =>'color inputColor input_style',
           'validators' => array(),
           'errors' => array(),
       ));
 
       $this->setElement('emplacement_id', array(
               'type' => 'hidden',
               'readonly' =>'true',
               'id' => 'emplacement_id',
               'label' => 'Emplacement',
               'class'=>'color inputColor input_style',
               'value' => $conteneur->has('emplacement_id') ? $conteneur->get('emplacement_id') : null,
               'validators' => array('NotEmpty'),
               'errors' => array(),
       ));
       
       
       /*$this->setElement('emplacement_id', array(
           'type' => 'select',
           'id' => 'emplacement_id',
           'label' => 'emplacement_id',
           'value' => $conteneur->has('emplacement_id') ? $conteneur->get('emplacement_id') : null,
           'class'=>'white select_style',
           'fake_options' => array(),
           'options' => $conteneur->getTable('Model_Emplacement')->fetchAll()->setKey('id')->setValue('nom'),
           'errors' => array(),
       ));*/
       
       $this->setElement('type_id', array(
               'type' => 'select',
               'id' => 'type_id',
               'label' => 'Type emplacement',
               'value' => $conteneur->has('type_id') ? $conteneur->get('type_id') : null,
               'readonly' =>'true',
               'class'=>'color select_style',
               'fake_options' => array(),
               'options' => $conteneur->getTable('Model_Fsn_Categorie')->fetchAll()->where("'conteneur_type'=categorie_type") ->setKey('id')->setValue('categorie_value'),
               'errors' => array(),
       ));
 
   }
 
}