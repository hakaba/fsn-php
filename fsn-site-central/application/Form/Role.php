<?php

class Form_Role extends Yab_Form {

	public function __construct(Model_Role $role) {

		$this->set('method', 'post')->set('name', 'form_role')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('role', array(
			'type' => 'text',
			'id' => 'role',
			'label' => 'role',
			'value' => $role->has('role') ? $role->get('role') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('visible', array(
			'type' => 'text',
			'id' => 'visible',
			'label' => 'visible',
			'value' => $role->has('visible') ? $role->get('visible') : '1',
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

	}

}