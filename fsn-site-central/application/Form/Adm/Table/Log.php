<?php

class Form_Adm_Table_Log extends Yab_Form {

	public function __construct(Model_Adm_Table_Log $adm_table_log) {

		$this->set('method', 'post')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('nom_table', array(
			'type' => 'text',
			'id' => 'nom_table',
			'label' => 'nom_table',
			'value' => $adm_table_log->has('nom_table') ? $adm_table_log->get('nom_table') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('date_crea', array(
			'type' => 'text',
			'id' => 'date_crea',
			'label' => 'date_crea',
			'value' => $adm_table_log->has('date_crea') ? $adm_table_log->get('date_crea') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('date_modif', array(
			'type' => 'text',
			'id' => 'date_modif',
			'label' => 'date_modif',
			'value' => $adm_table_log->has('date_modif') ? $adm_table_log->get('date_modif') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('user_id', array(
			'type' => 'select',
			'id' => 'user_id',
			'label' => 'user_id',
			'value' => $adm_table_log->has('user_id') ? $adm_table_log->get('user_id') : null,
			'fake_options' => array(),
			'options' => $adm_table_log->getTable('Model_User')->fetchAll()->setKey('id')->setValue('login'),
			'errors' => array(),
		));

		$this->setElement('user', array(
			'type' => 'text',
			'id' => 'user',
			'label' => 'user',
			'value' => $adm_table_log->has('user') ? $adm_table_log->get('user') : null,
			'validators' => array(),
			'errors' => array(),
		));

	}

}