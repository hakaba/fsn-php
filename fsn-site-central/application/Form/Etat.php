<?php

class Form_Etat extends Yab_Form {

	public function __construct(Model_Etat $etat) {

		$this->set('method', 'post')->set('name', 'form_etat')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('etat', array(
			'type' => 'text',
			'id' => 'etat',
			'label' => 'etat',
			'value' => $etat->has('etat') ? $etat->get('etat') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('visible', array(
			'type' => 'text',
			'id' => 'visible',
			'label' => 'visible',
			'value' => $etat->has('visible') ? $etat->get('visible') : '1',
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

	}

}