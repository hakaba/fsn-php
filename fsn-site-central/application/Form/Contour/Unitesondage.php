<?php

class Form_Contour_Unitesondage extends Yab_Form {

	public function __construct(Model_Contour_Unitesondage $contour_unitesondage) {

		$this->set('method', 'post')->set('action', '')->set('role', 'form');

		$this->setElement('unitesondage_id', array(
				'type' => 'select',
				'id' => 'unitesondage_id',
				'label' => 'unitesondage_id',
				'value' => $contour_unitesondage->has('unitesondage_id') ? $contour_unitesondage->get('unitesondage_id') : null,
				'fake_options' => array(),
				'options' => $contour_unitesondage->getTable('Model_Unitesondage')->fetchAll()->setKey('id')->setValue('identification'),
				'errors' => array(),
		));

		$this->setElement('x', array(
				'type' => 'text',
				'id' => 'x',
				'label' => 'x',
				'value' => $contour_unitesondage->has('x') ? $contour_unitesondage->get('x') : null,
				'validators' => array('Float', 'NotEmpty'),
				'errors' => array(),
		));

		$this->setElement('y', array(
				'type' => 'text',
				'id' => 'y',
				'label' => 'y',
				'value' => $contour_unitesondage->has('y') ? $contour_unitesondage->get('y') : null,
				'validators' => array('Float', 'NotEmpty'),
				'errors' => array(),
		));

		$this->setElement('z', array(
				'type' => 'text',
				'id' => 'z',
				'label' => 'z',
				'value' => $contour_unitesondage->has('z') ? $contour_unitesondage->get('z') : null,
				'validators' => array('Float', 'NotEmpty'),
				'errors' => array(),
		));

		$this->setElement('num_sequence', array(
				'type' => 'text',
				'id' => 'num_sequence',
				'label' => 'num_sequence',
				'value' => $contour_unitesondage->has('num_sequence') ? $contour_unitesondage->get('num_sequence') : null,
				'validators' => array('Int', 'NotEmpty'),
				'errors' => array(),
		));

	}

}