<?php

class Form_Contour_Us extends Yab_Form {

	public function __construct(Model_Contour_Us $contour_us) {

		$this->set('method', 'post')->set('action', '')->set('role', 'form');

		$this->setElement('us_id', array(
			'type' => 'select',
			'id' => 'us_id',
			'label' => 'us_id',
			'value' => $contour_us->has('us_id') ? $contour_us->get('us_id') : null,
			'fake_options' => array(),
			'options' => $contour_us->getTable('Model_Us')->fetchAll()->setKey('id')->setValue('identification'),
			'errors' => array(),
		));

		$this->setElement('x', array(
			'type' => 'text',
			'id' => 'x',
			'label' => 'x',
			'value' => $contour_us->has('x') ? $contour_us->get('x') : null,
			'validators' => array('Float', 'NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('y', array(
			'type' => 'text',
			'id' => 'y',
			'label' => 'y',
			'value' => $contour_us->has('y') ? $contour_us->get('y') : null,
			'validators' => array('Float', 'NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('z', array(
			'type' => 'text',
			'id' => 'z',
			'label' => 'z',
			'value' => $contour_us->has('z') ? $contour_us->get('z') : null,
			'validators' => array('Float', 'NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('sequence', array(
			'type' => 'text',
			'id' => 'sequence',
			'label' => 'sequence',
			'value' => $contour_us->has('sequence') ? $contour_us->get('sequence') : null,
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

	}

}