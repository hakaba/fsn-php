<?php

class Form_Contour_Fai extends Yab_Form {

	public function __construct(Model_Contour_Fai $contour_fai) {

		$this->set('method', 'post')->set('action', '')->set('role', 'form');

		$this->setElement('fai_id', array(
			'type' => 'select',
			'id' => 'fai_id',
			'label' => 'fai_id',
			'value' => $contour_fai->has('fai_id') ? $contour_fai->get('fai_id') : null,
			'fake_options' => array(),
			'options' => $contour_fai->getTable('Model_Fai')->fetchAll()->setKey('id')->setValue('description'),
			'errors' => array(),
		));

		$this->setElement('x', array(
			'type' => 'text',
			'id' => 'x',
			'label' => 'x',
			'value' => $contour_fai->has('x') ? $contour_fai->get('x') : null,
			'validators' => array('Float', 'NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('y', array(
			'type' => 'text',
			'id' => 'y',
			'label' => 'y',
			'value' => $contour_fai->has('y') ? $contour_fai->get('y') : null,
			'validators' => array('Float', 'NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('z', array(
			'type' => 'text',
			'id' => 'z',
			'label' => 'z',
			'value' => $contour_fai->has('z') ? $contour_fai->get('z') : null,
			'validators' => array('Float', 'NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('sequence', array(
			'type' => 'text',
			'id' => 'sequence',
			'label' => 'sequence',
			'value' => $contour_fai->has('sequence') ? $contour_fai->get('sequence') : null,
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

	}

}