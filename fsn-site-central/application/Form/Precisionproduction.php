<?php

class Form_Precisionproduction extends Yab_Form {

	public function __construct(Model_Precisionproduction $precisionproduction) {

		$this->set('method', 'post')->set('name', 'form_precisionproduction')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('numero', array(
			'type' => 'text',
			'id' => 'numero',
			'label' => 'numero',
			'value' => $precisionproduction->has('numero') ? $precisionproduction->get('numero') : null,
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('precision', array(
			'type' => 'text',
			'id' => 'precision',
			'label' => 'precision',
			'value' => $precisionproduction->has('precision') ? $precisionproduction->get('precision') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('visible', array(
			'type' => 'text',
			'id' => 'visible',
			'label' => 'visible',
			'value' => $precisionproduction->has('visible') ? $precisionproduction->get('visible') : '1',
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

	}

}