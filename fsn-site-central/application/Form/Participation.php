<?php

class Form_Participation extends Yab_Form {

	public function __construct(Model_Participation $participation, $mode='add') {
		
		// appel fichier internationalisation
        $registry = Yab_Loader::getInstance() -> getRegistry();
        $i18n = $registry -> get('i18n');
        $filter_no_html = new Yab_Filter_NoHtml();
		
		$this->set('method', 'post')->set('name', 'form_participation')->set('action', '')->set('role', 'form');

		//Appel a la connexion BDD
		$db = Yab_Loader::getInstance()->getRegistry()->get('db');

		// Liste des intervenants
		// $liste_intervenants = (new Model_Intervenant())->getTable('Model_Intervenant')->getAllNomsComplets();
		$liste_intervenants = $db->prepare('SELECT 
			id, 
			CONCAT_WS(" ", nom, prenom) AS nom_complet 
			FROM intervenant
			ORDER BY nom
		');

		// Liste des oa
		$liste_oa = $participation->getTable('Model_Oa')->fetchAll() ;
    
		$this->setElement('oa_id', array(
			'type' => 'select',
			'id' => 'oa_id',
			'label' => 'Opération Archéologique',
			'value' => $participation->has('oa_id') ? $participation->get('oa_id') : null,
			'fake_options' => array(),
			'options' => $liste_oa->setKey('id')->setValue('identification'),
      		'onchange' => 'submit()',
			'errors' => array(),
		));
		$oa_debut = null;
		$oa_fin = null;
		if($this->getElement('oa_id')->getValue()){
			$oa_id = $this->getElement('oa_id')->getValue() ;
			$oa_info = $participation->getTable('Model_Oa')->fetchAll()->where('id = "'.$oa_id.'" ')->toRow();
			$oa_debut = $oa_info['debut'] ;
			$oa_fin = $oa_info['fin'] ;
		}

  		$this->setElement('intervenant_id', array(
  			'type' => 'select',
  			'id' => 'intervenant_id',
  			'label' => $filter_no_html->filter( $i18n -> say('intervenant_id') ),
  			'value' => $participation->has('intervenant_id') ? $participation->get('intervenant_id') : null,
  			'fake_options' => array('' => '-- Choix intervenant --' ),
  			'options' => $liste_intervenants->setKey('id')->setValue('nom_complet'),
  			'errors' => array(),
  		));
  		$this->setElement('debut', array(
  			'type' => 'text',
  			'id' => 'debut',
  			'label' => $filter_no_html->filter( $i18n -> say('debut') ),
        	'class' => 'datepicker',
  			'value' => $participation->has('debut') ? $participation->get('debut') : null ,
  			'validators' => array('NotEmpty'),
  			'errors' => array(),
  		));
  		$this->setElement('fin', array(
  			'type' => 'text',
  			'id' => 'fin',
  			'label' => $filter_no_html->filter( $i18n -> say('fin') ),
        	'class' => 'datepicker',
  			'value' => $participation->has('fin') ? $participation->get('fin') : null,
  			'validators' => array('NotEmpty'),
  			'errors' => array(),
  		));
  		$this->setElement('role_id', array(
  			'type' => 'select',
  			'id' => 'role_id',
  			'label' => $filter_no_html->filter( $i18n -> say('role_id') ),
  			'value' => $participation->has('role_id') ? $participation->get('role_id') : null,
  			'fake_options' => array(),
  			'options' => $participation->getTable('Model_Role')->fetchAll()->setKey('id')->setValue('role'),
  			'errors' => array(),
  		));
    foreach($this->getElements() as $element) {
      
      if($mode == 'show'){
        $element->set('readonly','readonly');
        $element->set('disable',true);
      }
    }
  }
}