<?php

class Form_Typedocument extends Yab_Form {

	public function __construct(Model_Typedocument $typedocument) {

		$this->set('method', 'post')->set('name', 'form_typedocument')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('typedoc', array(
			'type' => 'text',
			'id' => 'typedoc',
			'label' => 'typedoc',
			'value' => $typedocument->has('typedoc') ? $typedocument->get('typedoc') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('visible', array(
			'type' => 'text',
			'id' => 'visible',
			'label' => 'visible',
			'value' => $typedocument->has('visible') ? $typedocument->get('visible') : '1',
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

	}

}