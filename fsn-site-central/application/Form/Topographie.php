<?php

class Form_Topographie extends Yab_Form {

	public function __construct(Model_Topographie $topographie) {

		$this->set('method', 'post')->set('name', 'form_topographie')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('x', array(
			'type' => 'text',
			'id' => 'x',
			'label' => 'x',
			'value' => $topographie->has('x') ? $topographie->get('x') : null,
			'validators' => array('Float'),
			'errors' => array(),
		));

		$this->setElement('y', array(
			'type' => 'text',
			'id' => 'y',
			'label' => 'y',
			'value' => $topographie->has('y') ? $topographie->get('y') : null,
			'validators' => array('Float'),
			'errors' => array(),
		));

		$this->setElement('z', array(
			'type' => 'text',
			'id' => 'z',
			'label' => 'z',
			'value' => $topographie->has('z') ? $topographie->get('z') : null,
			'validators' => array('Float'),
			'errors' => array(),
		));

	}

}