<?php

class Form_Sitefouille extends Yab_Form {

	public function __construct(Model_Sitefouille $sitefouille) {
		    
        $loader = Yab_Loader::getInstance() ;    
        $registry = Yab_Loader::getInstance() -> getRegistry();
        $session = Yab_Loader::getInstance() ->getSession();
                
        // appel fichier internationalisation
        $i18n = $registry -> get('i18n');
        $filter_no_html = new Yab_Filter_NoHtml();
        
		$this->set('method', 'post')->set('name', 'form_sitefouille')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');
		$visibles_sf = $sitefouille->getVisibleSitefouilles()->setKey('id')->setValue('nom_nomabrege')->toArray();
		asort($visibles_sf);

		/*$this->setElement('id', array(
			'type' => 'hidden',
			'id' => 'id',
			'label' => $i18n->say('id'),
			'value' => $sitefouille->has('id') ? $sitefouille->get('id') : $session->get('sitefouille_id'),
			'options' => $visibles_sf,
			'validators' => array(),
			'errors' => array(),
		));*/

		$this->setElement('nom', array(
			'type' => 'text',
			'id' => 'nom',
			'label' => $filter_no_html->filter( $i18n -> say('sitefouille_nom') ),
			'placeholder' => $filter_no_html->filter( $i18n -> say('sitefouille_nom') ),
            'tooltip' => $filter_no_html->filter( $i18n -> say('sitefouille_tooltip') ),
            'value' => $sitefouille->has('nom') ? $sitefouille->get('nom') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('nomabrege', array(
			'type' => 'text',
			'id' => 'nomabrege',
			'label' => $filter_no_html->filter( $i18n -> say('sitefouille_nomabrege') ),
            'placeholder' => $filter_no_html->filter( $i18n -> say('sitefouille_nomabrege') ),
            'value' => $sitefouille->has('nomabrege') ? $sitefouille->get('nomabrege') : null,
			'maxlength' => 5,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('numerosite', array(
			'type' => 'text',
			'id' => 'numerosite',
			'label' => $filter_no_html->filter( $i18n -> say('numerosite') ),
            'placeholder' => $filter_no_html->filter( $i18n -> say('numerosite') ),
			'value' => $sitefouille->has('numerosite') ? $sitefouille->get('numerosite') : null,
			'maxlength' => 11,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('description', array(
			'type' => 'textarea',
      		'rows' => '3',
			'id' => 'description',
			'label' => $filter_no_html->filter( $i18n -> say('description') ),
            'placeholder' => $filter_no_html->filter( $i18n -> say('description') ),
			'value' => $sitefouille->has('description') ? $sitefouille->get('description') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('departement', array(
			'type' => 'text',
			'id' => 'departement',
			'label' => $filter_no_html->filter( $i18n -> say('departement') ),
            'placeholder' => '93200',
			'value' => $sitefouille->has('departement') ? $sitefouille->get('departement') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('commune', array(
			'type' => 'text',
			'id' => 'commune',
			'label' => $filter_no_html->filter( $i18n -> say('commune') ),
            'placeholder' => $filter_no_html->filter( $i18n -> say('commune') ),
			'value' => $sitefouille->has('commune') ? $sitefouille->get('commune') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('adresse', array(
			'type' => 'text',
			'id' => 'adresse',
			'label' => $filter_no_html->filter( $i18n -> say('adresse') ),
            'placeholder' => $filter_no_html->filter( $i18n -> say('adresse') ),
			'value' => $sitefouille->has('adresse') ? $sitefouille->get('adresse') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('anneecadastre', array(
			'type' => 'text',
			'id' => 'anneecadastre',
			'label' => $filter_no_html->filter( $i18n -> say('anneecadastre') ),
            'placeholder' => '2000',
			'value' => $sitefouille->has('anneecadastre') ? $sitefouille->get('anneecadastre') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('sectionparcelle', array(
			'type' => 'textarea',
      		'rows' => '3',
			'id' => 'sectionparcelle',
			'label' => $filter_no_html->filter( $i18n -> say('sectionparcelle') ),
            'placeholder' => $filter_no_html->filter( $i18n -> say('sectionparcelle') ),
			'value' => $sitefouille->has('sectionparcelle') ? $sitefouille->get('sectionparcelle') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('proprietaire', array(
			'type' => 'text',
			'id' => 'proprietaire',
			'label' => $filter_no_html->filter( $i18n -> say('proprietaire') ),
            'placeholder' => $filter_no_html->filter( $i18n -> say('proprietaire') ),
			'value' => $sitefouille->has('proprietaire') ? $sitefouille->get('proprietaire') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('protectionjuridique', array(
			'type' => 'text',
			'id' => 'protectionjuridique',
			'label' => $filter_no_html->filter( $i18n -> say('protectionjuridique') ),
            'placeholder' => $filter_no_html->filter( $i18n -> say('protectionjuridique') ),
			'value' => $sitefouille->has('protectionjuridique') ? $sitefouille->get('protectionjuridique') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('surfaceestime', array(
			'type' => 'text',
			'id' => 'surfaceestime',
			'label' => $filter_no_html->filter( $i18n -> say('surfaceestime') ),
            'placeholder' => '200',
            'tooltip' => 'm²',
			'value' => $sitefouille->has('surfaceestime') ? $sitefouille->get('surfaceestime') : null,
			'validators' => array('Int'),
			'errors' => array(),
		));

		$this->setElement('commentaire', array(
			'type' => 'textarea',
      		'rows' => '3',
			'id' => 'commentaire',
			'label' => $filter_no_html->filter( $i18n -> say('commentaire') ),
            'placeholder' => $filter_no_html->filter( $i18n -> say('commentaire') ),
			'value' => $sitefouille->has('commentaire') ? $sitefouille->get('commentaire') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('lambert_x', array(
			'type' => 'text',
			'id' => 'lambert_x',
			'label' => $filter_no_html->filter( $i18n -> say('lambert_x') ),
            'placeholder' => '20.2',
			'value' => $sitefouille->has('lambert_x') ? $sitefouille->get('lambert_x') : null,
			'validators' => array('Float'),
			'errors' => array(),
		));

		$this->setElement('lambert_y', array(
			'type' => 'text',
			'id' => 'lambert_y',
			'label' => $filter_no_html->filter( $i18n -> say('lambert_y') ),
            'placeholder' => '20.1',
			'value' => $sitefouille->has('lambert_y') ? $sitefouille->get('lambert_y') : null,
			'validators' => array('Float'),
			'errors' => array(),
		));

		$this->setElement('altitude', array(
			'type' => 'text',
			'id' => 'altitude',
			'label' => $filter_no_html->filter( $i18n -> say('altitude') ),
            'placeholder' => '20',
			'value' => $sitefouille->has('altitude') ? $sitefouille->get('altitude') : null,
			'validators' => array('Float'),
			'errors' => array(),
		));

		/*$this->setElement('fsn_entiteadmin_id', array(
			'type' => 'hidden',
			'id' => 'fsn_entiteadmin_id',
			'label' => 'fsn_entiteadmin_id',
			'value' => $sitefouille->has('fsn_entiteadmin_id') ? $sitefouille->get('fsn_entiteadmin_id') : '1',
			'fake_options' => array(),
			'options' => $sitefouille->getTable('Model_Fsn_Entiteadmin')->fetchAll()->setKey('id')->setValue('description'),
			'errors' => array(),
		));*/

		$this->setElement('datation_debut', array(
            'type' => 'text',
            'id' => 'datation_debut',
            'label' => $filter_no_html->filter( $i18n -> say('datation_debut') ),
            'placeholder' => '-5000',
            'tooltip' => $filter_no_html->filter( $i18n -> say('datation_debut_tooltip') ),
            'value' => $sitefouille->has('datation_debut') ? $sitefouille->get('datation_debut') : '-5000',
            'validators' => array('Int', 'NotEmpty'),
            'errors' => array(),
        ));

        $this->setElement('datation_fin', array(
            'type' => 'text',
            'id' => 'datation_fin',
            'label' => $filter_no_html->filter( $i18n -> say('datation_fin') ),
            'placeholder' => '2015',
            'tooltip' => $filter_no_html->filter( $i18n -> say('datation_fin_tooltip') ),
            'value' => $sitefouille->has('datation_fin') ? $sitefouille->get('datation_fin') : '2015',
            'validators' => array('Int', 'NotEmpty'),
            'errors' => array(),
        ));
		
	}

}