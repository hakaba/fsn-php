<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Relative speed factor is out of range (Correct range: 10-90).'] = 'Факторът относителна скорост е извън обхват (точен обхват: 10-90).';
$lang['Start position ratio is out of range (Correct range: 0-99).'] = 'Началната позиция съотношение е извън обхват (точен диапазон: 0-99).';
$lang['Permanent'] = 'Постоянен';
$lang['Border Width ratio is out of range (Correct range: 0-10).'] = 'Размерът на панорамата е извън допустимите граници(точен диапазон: 0-10).';
$lang['Border width'] = 'Допустим размер на панорамата [0-10]';
$lang['You are not authorized to change this configuration (Webmaster only).'] = 'Вие не сте упълномощени да промените тази конфигурация (Само системния администратор може).';
$lang['Your configuration is NOT saved due to above reasons.'] = 'Вашата конфигурация не е записана поради горните причини.';
$lang['authors of'] = 'авторите на';
$lang['Your configuration is saved.'] = 'Вашата конфигурация е запазена';
$lang['Mode 360'] = 'Mode 360';
$lang['Picture name substring could not be empty.'] = 'Подимето на картината не може да бъде празно.';
$lang['Panoramas configuration'] = 'Конфигурация на панорамите';
$lang['Remove substring from picture title'] = 'Премахване на подниз от заглавната снимка';
$lang['Rotation direction'] = 'Посоката на въртене';
$lang['Picture name substrings must be different.'] = 'Подимето на картината трябва да бъде различен.';
$lang['Relative speed factor ']['10-90'] = 'Относителна скорост фактор [10-90]';
$lang['Right'] = 'Надясно';
$lang['Start position ']['% between 0-99'] = 'Началната позиция [% между 0-99]';
$lang['Special thanks to:'] = 'Специални благодарности на:';
$lang['Off'] = 'Изключено';
$lang['On'] = 'Включено ';
$lang['Caption Color'] = 'Цвят на текста в описанието';
$lang['Footer Color'] = 'Footer контрол на фона';
$lang['Footer Control Color'] = 'Footer контрол на цветовете';
$lang['Footer display'] = 'Footer дисплей';
$lang['Left'] = 'Наляво';
$lang['Control display'] = 'Контролен дисплей';
$lang['Border color'] = 'Панорамна граница на цветовете';
$lang['Automatic start'] = 'Автоматичен старт';
$lang['Auto'] = 'Автоматично';
$lang['Viewport width ratio is out of range (Correct range: 50-100).'] = 'Viewport широчина е извън обхват (точен обхват: 50-100).';
$lang['Viewport minimal width ']['pixels > 320'] = 'Viewport минимална ширина [пиксела> 320]';
$lang['Viewport width ']['% between 50-100'] = 'Viewport ширина [% между 50-100]';
$lang['Viewport maximal width ']['pixels >= minimal width'] = 'Viewport максимална ширина[пиксели >= минимална широчина]';
$lang['Mode 180 (Alternative left-right restart)'] = 'Режим 180 (алтернативен ляво-дясно рестартиране)';
$lang['Minimal Viewport width is out of range (Correct range: 320/+).'] = 'Минималната Viewport ширина е извън обхват (точен диапазон: 320 / +).';
$lang['Picture name substring to display in Mode 180 '][' != previous substring '] = 'Подимето на снимката да се показва в режим 180[!=предишния подниз]';
$lang['Picture name substring to display in Mode 360'] = 'Подимето на снимката да се показва в режим 360';
$lang['Maximal Viewport width is out of range (Correct range: Minimal/+).'] = 'Максималната видима ширина е извън обхват (точен диапазон: Минимално/ +).';
?>