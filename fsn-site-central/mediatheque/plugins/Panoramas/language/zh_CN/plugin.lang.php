<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2012 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['authors of'] = 'authors of';
$lang['Your configuration is saved.'] = '您的设置已保存。';
$lang['Your configuration is NOT saved due to above reasons.'] = '您的设置因以上原因而未被保存。';
$lang['You are not authorized to change this configuration (Webmaster only).'] = '您没有权限改变此项设置（仅限管理员）。';
$lang['Viewport width ratio is out of range (Correct range: 50-100).'] = '视口宽度数值设置错误（正确范围：50-100）。';
$lang['Viewport width ']['% between 50-100'] = '视口宽度 [% 介于 50-100]';
$lang['Start position ']['% between 0-99'] = '起始位置 [% 介于 0-99]';
$lang['Viewport minimal width ']['pixels > 320'] = '视口最小宽度 [像素 > 320]';
$lang['Viewport maximal width ']['pixels >= minimal width'] = '视口最大宽度 [像素 >= 最小宽度]';
$lang['Start position ratio is out of range (Correct range: 0-99).'] = '起始位置数值设置错误（正确范围：0-99）。';
$lang['Relative speed factor is out of range (Correct range: 10-90).'] = '相对速度系数超出范围（正确范围：10-90）。';
$lang['Picture name substrings must be different.'] = '图片名称子串必须不同。';
$lang['Picture name substring could not be empty.'] = '图片名称子串不能为空。';
$lang['Minimal Viewport width is out of range (Correct range: 320/+).'] = '最小视口宽度设置错误（正确范围：320像素/+）。';
$lang['Maximal Viewport width is out of range (Correct range: Minimal/+).'] = '最大视口宽度设置错误（正确范围：最小视口/+）。';
$lang['Special thanks to:'] = '特别感谢：';
$lang['Rotation direction'] = '卷动方向';
$lang['Right'] = '右';
$lang['Remove substring from picture title'] = '去除图片标题的子串';
$lang['Relative speed factor ']['10-90'] = '相对速度系数';
$lang['Picture name substring to display in Mode 360'] = '360度模式下图片名称子串的显示';
$lang['Picture name substring to display in Mode 180 '][' != previous substring '] = '180度模式下图片名称子串的显示[ != previous substring ]';
$lang['Permanent'] = '永久';
$lang['Panoramas configuration'] = '全景图设置';
$lang['On'] = 'On';
$lang['Off'] = 'Off';
$lang['Mode 360'] = '360度模式';
$lang['Mode 180 (Alternative left-right restart)'] = '180度模式（左右交替）';
$lang['Control display'] = '控制显示';
$lang['Auto'] = '自动';
$lang['Automatic start'] = '自动开始';
$lang['Left'] = '左';
$lang['Footer Color'] = '页脚背景颜色';
$lang['Footer Control Color'] = '页脚控制颜色';
$lang['Footer display'] = '页脚显示';
$lang['Border Width ratio is out of range (Correct range: 0-10).'] = '全景图边缘尺寸超过限制（正确范围：0-10）。';
$lang['Border color'] = '全景图边缘颜色';
$lang['Border width'] = '全景图边缘尺寸 [0-10]';
$lang['Caption Color'] = '描述文字颜色';
?>