<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2012 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Automatic start'] = 'Автоматичний запуск';
$lang['Auto'] = 'Автоматично';
$lang['Control display'] = 'Керування відображенням';
$lang['Left'] = 'Ліворуч';
$lang['Mode 180 (Alternative left-right restart)'] = 'Метод 180(Альтернативний ліворуч-праворуч повторний запуск)';
$lang['Mode 360'] = 'Режим 360';
$lang['Off'] = 'Вимкнути';
$lang['On'] = 'Увімкнути';
$lang['Panoramas configuration'] = 'Конфігурація панорам';
$lang['Permanent'] = 'Постійна';
$lang['Relative speed factor is out of range (Correct range: 10-90).'] = 'Відносний коефіцієнт швидкості поза діапазоном(Правильний діапазон: 10-90).';
$lang['Relative speed factor [10-90]'] = 'Відносний коефіцієнт швидкості [ 10-90 ]';
$lang['Remove substring from picture title'] = 'Видалити підрядок з заголовок зображення';
$lang['Right'] = 'Праворуч';
$lang['Rotation direction'] = 'Напрям обертання';
$lang['Special thanks to:'] = 'Особлива подяка:';
$lang['authors of'] = 'автори';
$lang['Maximal Viewport width is out of range (Correct range: Minimal/+).'] = 'Максимальна ширина області перегляду знаходиться поза діапазону (Правильний вибір: мінімальна/+).';
$lang['Minimal Viewport width is out of range (Correct range: 320/+).'] = 'Мінімальна ширина області перегляду знаходиться поза діапазону (Правильний вибір: 320/+).';
$lang['Picture name substring could not be empty.'] = 'Назва зображення не може бути пустою';
$lang['Picture name substring to display in Mode 180 [ != previous substring ]'] = 'Назва зображення для відображення в режимі 180 [ !=попередній рядок ]';
$lang['Picture name substring to display in Mode 360'] = 'Назва зображення для відображення в режимі 360';
$lang['Picture name substrings must be different.'] = 'Назва зображення повинна бути різною.';
$lang['Start position ratio is out of range (Correct range: 0-99).'] = 'Початкова позиція співвідношення виходить за межі діапазону (правильний діапазон: 0-99).';
$lang['Start position [% between 0-99]'] = 'Початкова позиція [між 0-99 %]';
$lang['Viewport maximal width [pixels >= minimal width]'] = 'Максимальна ширина області перегляду [пікселі>=мінімальна ширина]';
$lang['Viewport minimal width [pixels > 320]'] = 'Мінімальна ширина області перегляду [пікселі>320]';
$lang['Viewport width ratio is out of range (Correct range: 50-100).'] = 'Співвідношення ширини області перегляду поза діапазоном(Правильний діапазон: 50-100).';
$lang['Viewport width [% between 50-100]'] = 'Область перегляду шириною [% між 50-100]';
$lang['You are not authorized to change this configuration (Webmaster only).'] = 'Ви не маєте прав змінити цю конфігурацію (лише для вебмайстра).';
$lang['Your configuration is NOT saved due to above reasons.'] = 'Ваша конфігурація не зберігається в зв\'язку з зазначеним вище причин.';
$lang['Your configuration is saved.'] = 'Ваша конфігурація збережена.';
$lang['Border Width ratio is out of range (Correct range: 0-10).'] = 'Панорамний розмір межі поза рядом (Правильний ряд: 0-10).';
$lang['Border color'] = 'Панорамний колір межі';
$lang['Border width'] = 'Панорамний розмір межі [0-10]';
$lang['Caption Color'] = 'Колір тексту опису';
$lang['Footer Color'] = 'Колір фону нижнього колонтитулу';
$lang['Footer Control Color'] = 'Колір контролю нижнього колонтитулу';
$lang['Footer display'] = 'Відображення нижнього колонтитула';
?>