<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2012 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Picture name substring to display in Mode 180 '][' != previous substring '] = 'Sequência para nomear a foto no modo 180 [!= sequência anterior]';
$lang['Picture name substring to display in Mode 360'] = 'Sequência para nomear a foto no modo 360';
$lang['Picture name substrings must be different.'] = 'As sequência para nomear as fotos devem ser diferentes';
$lang['Remove substring from picture title'] = 'Remove o nome do título da foto';
$lang['Viewport maximal width ']['pixels >= minimal width'] = 'Largura de visualização máxima [pixels >= largura mínima]';
$lang['Viewport minimal width ']['pixels > 320'] = 'Largura de visualização mínima [pixels > 320]';
$lang['Viewport width ']['% between 50-100'] = 'Largura de visualização [% entre 50-100]';
$lang['Viewport width ratio is out of range (Correct range: 50-100).'] = 'Largura de visualização está fora da faixa (Corrija a faixa: 50-100).';
$lang['Relative speed factor ']['10-90'] = 'Fator relativo de velocidade [10-90]';
$lang['Relative speed factor is out of range (Correct range: 10-90).'] = 'Fator relativo de velocidade está fora da faixa (corrija faixa: 10-90)';
$lang['Right'] = 'Direita';
$lang['Rotation direction'] = 'Direção da rotação';
$lang['Special thanks to:'] = 'Agradecimentos especiais para:';
$lang['Start position ']['% between 0-99'] = 'Posição de início [% entre 0-99]';
$lang['Start position ratio is out of range (Correct range: 0-99).'] = 'Posição de Início está fora da faixa (Corrija faixa: 0-99).';
$lang['You are not authorized to change this configuration (Webmaster only).'] = 'Você não pode mudar essa configuração (somente o adminstrador do sítio)';
$lang['Your configuration is NOT saved due to above reasons.'] = 'Sua configuração não foi salva devido as razões abaixo.';
$lang['Your configuration is saved.'] = 'Sua configuração foi salva';
$lang['authors of'] = 'autores de';
$lang['Auto'] = 'Auto';
$lang['Automatic start'] = 'Início automático';
$lang['Border Width ratio is out of range (Correct range: 0-10).'] = 'Tamanho da borda panorâmica fora da faixa (Corrija faixa: 0-10)';
$lang['Border color'] = 'Cor da Borda Panorâmica';
$lang['Border width'] = 'Tamanho da borda panorâmica';
$lang['Caption Color'] = 'Cor do texto de descrição';
$lang['Control display'] = 'Controle da exibição';
$lang['Footer Color'] = 'Cor de fundo do rodapé';
$lang['Footer Control Color'] = 'Controle da cor do rodapé';
$lang['Footer display'] = 'Mostra rodapé';
$lang['Left'] = 'esquerda';
$lang['Maximal Viewport width is out of range (Correct range: Minimal/+).'] = 'Largura máxima Visualização está fora da faixa (corrija faixa: Mínima/+).';
$lang['Minimal Viewport width is out of range (Correct range: 320/+).'] = 'Largura mínima Visualização está fora da faixa (corrija faixa: 320/+).';
$lang['Mode 180 (Alternative left-right restart)'] = 'Modo 180 (reinicia alternativa esquerda-direita)';
$lang['Mode 360'] = 'Modo 360';
$lang['Off'] = 'desligado';
$lang['On'] = 'ligado';
$lang['Panoramas configuration'] = 'Configuração Panoramas';
$lang['Permanent'] = 'Permanente';
$lang['Picture name substring could not be empty.'] = 'Nome da Foto não pode ficar vazio.';
?>