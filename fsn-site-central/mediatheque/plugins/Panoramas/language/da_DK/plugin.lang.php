<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2012 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Rotation direction'] = 'Rotationsretning';
$lang['Special thanks to:'] = 'Særlig tak til:';
$lang['Start position ']['% between 0-99'] = 'Startposition [% mellem 0 og 99]';
$lang['Start position ratio is out of range (Correct range: 0-99).'] = 'Startpositionsforhold er uden for grænserne (korrekt grænse: 0-99).';
$lang['Viewport maximal width ']['pixels >= minimal width'] = 'Maksimal bredde på viewport [pixels >= minimal bredde]';
$lang['Viewport minimal width ']['pixels > 320'] = 'Minimal bredde på viewport [pixels > 320]';
$lang['Viewport width ']['% between 50-100'] = 'Bredde på viewport [% mellem 50 og 100]';
$lang['Viewport width ratio is out of range (Correct range: 50-100).'] = 'Breddeforhold på viewport er uden for grænserne (korrekt grænse: 50-100).';
$lang['You are not authorized to change this configuration (Webmaster only).'] = 'Du er ikke autoriseret til at ændre opsætningen (er kun for webmaster).';
$lang['Your configuration is NOT saved due to above reasons.'] = 'Opsætning er IKKE gemt af ovennævnte årsager.';
$lang['Your configuration is saved.'] = 'Opsætningen er gemt.';
$lang['authors of'] = 'forfattere af';
$lang['Auto'] = 'Automatisk';
$lang['Automatic start'] = 'Automatisk start';
$lang['Control display'] = 'Kontrolvisning';
$lang['Left'] = 'Venstre';
$lang['Maximal Viewport width is out of range (Correct range: Minimal/+).'] = 'Maksimal bredde på viewport er uden for grænserne (korrekt grænse: Minimal/+).';
$lang['Minimal Viewport width is out of range (Correct range: 320/+).'] = 'Minimal bredde på viewport er uden for grænserne (korrekt grænse: 320/+).';
$lang['Mode 180 (Alternative left-right restart)'] = '180-tilstand (alternativ venstre-højre-genstart)';
$lang['Mode 360'] = '360-tilstand';
$lang['Off'] = 'Fra';
$lang['On'] = 'Til';
$lang['Panoramas configuration'] = 'Opsætning af panoramaer';
$lang['Permanent'] = 'Permanent';
$lang['Picture name substring could not be empty.'] = 'Delstrengs billednavn må ikke være tomt';
$lang['Picture name substring to display in Mode 180 '][' != previous substring '] = 'Delstrengs billednavn til visning i 180-tilstand [!= tidligere delstreng]';
$lang['Picture name substring to display in Mode 360'] = 'Delstrengs billednavn til visning i 360-tilstand';
$lang['Picture name substrings must be different.'] = 'Delstrenges billednavne skal være forskellige.';
$lang['Relative speed factor ']['10-90'] = 'Relativ hastighedsfaktor [10-90]';
$lang['Relative speed factor is out of range (Correct range: 10-90).'] = 'Relativ hastighedsfaktor er uden for grænserne (korrekt grænse: 10-90).';
$lang['Remove substring from picture title'] = 'Fjern delstreng fra billedets titel';
$lang['Right'] = 'Højre';
$lang['Border Width ratio is out of range (Correct range: 0-10).'] = 'Størrelse på panoramaramme er uden for grænserne (korrekt interval: 0-10).';
$lang['Border color'] = 'Farve på panoramaramme';
$lang['Border width'] = 'Størrelse på panoramaramme';
$lang['Caption Color'] = 'Beskrivelses tekstfarve';
$lang['Footer Color'] = 'Sidefods baggrundsfarve';
$lang['Footer Control Color'] = 'Sidefods kontrolfarve';
$lang['Footer display'] = 'Visning af sidefod';
?>