<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2012 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Auto'] = 'Αυτόματη';
$lang['Automatic start'] = 'Αυτόματη έναρξη';
$lang['Control display'] = 'Έλεγχος οθόνης';
$lang['Left'] = 'Αριστερά';
$lang['Maximal Viewport width is out of range (Correct range: Minimal/+).'] = 'Μέγιστο πλάτος Viewport είναι εκτός εύρους(σωστό εύρος: Ελάχιστο / +).';
$lang['Minimal Viewport width is out of range (Correct range: 320/+).'] = 'Μέγιστο πλάτος Viewport είναι εκτός εύρους(σωστό εύρος: 320 / +).';
$lang['Mode 180 (Alternative left-right restart)'] = 'Λειτουργία 180 (Εναλλακτική αριστερά-δεξιά επανεκκίνηση)';
$lang['Mode 360'] = 'Λειτουργία 360';
$lang['Off'] = 'Οφφ';
$lang['On'] = 'Ον';
$lang['Panoramas configuration'] = 'Διαμόρφωση Πανοράματος';
$lang['Permanent'] = 'Μόνιμα';
$lang['Picture name substring could not be empty.'] = 'Η υποσυμβολοσειρά ονόματος Εικόνας δεν μπορεί να είναι κενή.';
$lang['Picture name substring to display in Mode 180 '][' != previous substring '] = 'Η υποσυμβολοσειρά ονόματος Εικόναςγια να εμφανιστεί σε λειτουργία 180 [! = προηγούμενη υποσυμβολοσειρά ]';
$lang['Picture name substring to display in Mode 360'] = 'Yποαλφαριθμητικό όνομα εικόνας για να εμφανιστεί σε λειτουργία 360';
$lang['Picture name substrings must be different.'] = 'Το Yποαλφαριθμητικό όνομα εικόνας πρέπει να είναι διαφορετικό.';
$lang['Relative speed factor ']['10-90'] = 'Συντελεστής σχετικής ταχύτητας [10-90]';
$lang['Relative speed factor is out of range (Correct range: 10-90).'] = 'Ο Συντελεστής σχετικής ταχύτητας είναι εκτός εύρους. (Σωστό εύρος: 10-90).';
$lang['Remove substring from picture title'] = 'Κατάργηση υποαλφαριθμητικού από τον τίτλο εικόνας';
$lang['Right'] = 'Δεξιά';
$lang['Rotation direction'] = 'Κατεύθυνση περιστροφής';
$lang['Special thanks to:'] = 'Ιδιαίτερες ευχαριστίες προς:';
$lang['Start position ']['% between 0-99'] = 'Αρχική θέση [% μεταξύ 0-99]';
$lang['Start position ratio is out of range (Correct range: 0-99).'] = 'Αναλογία αρχικής θέσης βρίσκεται εκτός εύρους (Σωστό εύρος: 0-99).';
$lang['You are not authorized to change this configuration (Webmaster only).'] = 'Δεν έχετε δικαίωμα να αλλάξετε αυτή τη ρύθμιση (Διαχειριστής μόνο).';
$lang['Your configuration is NOT saved due to above reasons.'] = 'Οι ρυθμίσεις σας δεν αποθηκεύθηκαν λόγω των παραπάνω λόγων.';
$lang['Your configuration is saved.'] = 'Οι ρυθμίσεις σας αποθηκεύθηκαν.';
$lang['authors of'] = 'συγγραφείς';
$lang['Viewport maximal width ']['pixels >= minimal width'] = 'Viewport μέγιστο πλάτος [pixel >= ελάχιστο πλάτος]';
$lang['Viewport minimal width ']['pixels > 320'] = 'Viewport ελάχιστο πλάτος [pixel > 320]';
$lang['Viewport width ']['% between 50-100'] = 'Viewport πλάτος [% μεταξύ 50-100]';
$lang['Viewport width ratio is out of range (Correct range: 50-100).'] = 'αναλογία πλατους Viewport είναι εκτός εύρους (Σωστό εύρος: 50-100)';
$lang['Footer display'] = 'Εμφάνιση Υποσέλιδου';
$lang['Footer Control Color'] = 'Χρώμα ελέγχου του Υποσέλιδου ';
$lang['Footer Color'] = 'Χρώμα φόντου του Υποσέλιδου ';
$lang['Caption Color'] = 'Χρώμα κειμένου Περιγραφής ';
$lang['Border width'] = 'Μέγεθος του ορίου του Πανοράματος [0-10]';
$lang['Border color'] = 'Χρώμα ορίου Πανοράματος';
$lang['Border Width ratio is out of range (Correct range: 0-10).'] = 'Το μέγεθος των ορίων του Πανοράματος είναι εκτός εμβέλειας (σωστό εύρος: 0-10).';
?>