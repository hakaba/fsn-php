<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Border Width ratio is out of range (Correct range: 0-10).'] = 'Velikost obrobe je izven meja (Pravilno območje: 0-10)';
$lang['Border color'] = 'Barva obrobe panorame';
$lang['Border width'] = 'Velikost obrobe panorame';
$lang['Maximal Viewport width is out of range (Correct range: Minimal/+).'] = 'Maksimalna širina vidnega polja je izven območja (Pravilno območje: Minimalno/+).';
$lang['Minimal Viewport width is out of range (Correct range: 320/+).'] = 'Minimalna širina vidnega polja je izven območja (Pravilno območje: 320/+).';
$lang['Picture name substring to display in Mode 180 '][' != previous substring '] = 'Ime slike podniza za prikaz v načinu 180 [ != predhodni podniz ]';
$lang['Picture name substring to display in Mode 360'] = 'Ime slike podniza za prikaz v načinu 360';
$lang['Picture name substrings must be different.'] = 'Imena slik podnizov morajo biti različna';
$lang['Relative speed factor ']['10-90'] = 'Faktor relativne hitrosti [10-90]';
$lang['Relative speed factor is out of range (Correct range: 10-90).'] = 'Faktor relativne hitrosti je izven območja (Pravilno območje: 10-90)';
$lang['Remove substring from picture title'] = 'Odstrani podniz iz imena slike';
$lang['Right'] = 'Desno';
$lang['Rotation direction'] = 'Smer vrtenja';
$lang['Special thanks to:'] = 'Posebna zahvala:';
$lang['Start position ']['% between 0-99'] = 'Začetno mesto [% med 0-99]';
$lang['Start position ratio is out of range (Correct range: 0-99).'] = 'Mesto je izven območja (Pravilno območje: 0-99)';
$lang['Viewport maximal width ']['pixels >= minimal width'] = 'Maksimalna širina vidnega polja [pixli >= minimalni širini]';
$lang['Viewport minimal width ']['pixels > 320'] = 'Minimalna širina vidnega polja [pixlov > 320]';
$lang['Viewport width ']['% between 50-100'] = 'Širina vidnega polja [% med 50-100]';
$lang['Viewport width ratio is out of range (Correct range: 50-100).'] = 'Razmerje vidnega polja je izven območja (Pravilno območje: 50-100)';
$lang['You are not authorized to change this configuration (Webmaster only).'] = 'Za spremembo teh nastavitev nimate pravic (Samo skrbnik strani)';
$lang['Your configuration is NOT saved due to above reasons.'] = 'Nastavitve niso shranjene zaradi zgornjih vzrokov';
$lang['Your configuration is saved.'] = 'Nastavitve so shranjene';
$lang['authors of'] = 'avtorji';
$lang['Auto'] = 'Samodejno';
$lang['Automatic start'] = 'Samodejni zagon';
$lang['Caption Color'] = 'Barva besedila opisa ';
$lang['Control display'] = 'Prikaži kontrole';
$lang['Footer Color'] = 'Barva ozadja noge';
$lang['Footer Control Color'] = 'Barva kontrolnikov noge';
$lang['Footer display'] = 'Prikaži nogo';
$lang['Left'] = 'Levo';
$lang['Mode 180 (Alternative left-right restart)'] = 'Način 180 (Izmeničen levo-desni restart)';
$lang['Mode 360'] = 'Način 360';
$lang['Off'] = 'Izključeno';
$lang['On'] = 'Vključeno';
$lang['Panoramas configuration'] = 'Nastavitve Panorame';
$lang['Permanent'] = 'Stalno';
$lang['Picture name substring could not be empty.'] = 'Ime slike podniza ne more biti prazno';