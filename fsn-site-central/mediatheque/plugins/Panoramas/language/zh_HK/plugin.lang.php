<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Picture name substring to display in Mode 180 '][' != previous substring '] = '180模式中相片名稱子字串顯示[不等於前面的子字串]';
$lang['Picture name substring to display in Mode 360'] = '360模式中相片名稱子字串顯示';
$lang['Picture name substrings must be different.'] = '相圖片名稱的子字串必須是不一樣的。';
$lang['Relative speed factor ']['10-90'] = '相對速度係數[10-90]';
$lang['Relative speed factor is out of range (Correct range: 10-90).'] = '相對速度的因素超出了範圍(正確範圍：10-90)。';
$lang['Remove substring from picture title'] = '從相片標題中刪除子字串';
$lang['Right'] = '右';
$lang['Rotation direction'] = '旋轉方向';
$lang['Special thanks to:'] = '特別感謝：';
$lang['Start position ']['% between 0-99'] = '開始位置[%在0-99之間]';
$lang['Start position ratio is out of range (Correct range: 0-99).'] = '開始位置超出了範圍(正確範圍:0-99)。';
$lang['Viewport maximal width ']['pixels >= minimal width'] = '視區的最大寬度[像素 >= 最小寬度]';
$lang['Viewport minimal width ']['pixels > 320'] = '視區的最小寬度[像素 > 320]';
$lang['Viewport width ']['% between 50-100'] = '視區寬度在[% 50-100]';
$lang['Viewport width ratio is out of range (Correct range: 50-100).'] = '視區寬度比例超出了範圍 (正確範圍:50-100)。';
$lang['You are not authorized to change this configuration (Webmaster only).'] = '您沒有權限更改此設定（網站管理員權限）。';
$lang['Your configuration is NOT saved due to above reasons.'] = '由於上述原因而沒有儲存你的設定。';
$lang['Your configuration is saved.'] = '你的設定已儲存。';
$lang['authors of'] = '的作者';
$lang['Auto'] = '自動';
$lang['Automatic start'] = '自動起動';
$lang['Border Width ratio is out of range (Correct range: 0-10).'] = '全景邊界尺寸超出了範圍(正確範圍： 0~10)。';
$lang['Border color'] = '全景邊框的顏色';
$lang['Border width'] = '全景邊界尺寸[0-10]';
$lang['Caption Color'] = '描述文字顏色';
$lang['Control display'] = '控制顯示畫面';
$lang['Footer Color'] = '頁尾的背景顏色';
$lang['Footer Control Color'] = '頁尾控制顏色';
$lang['Footer display'] = '顯示頁尾';
$lang['Left'] = '左';
$lang['Maximal Viewport width is out of range (Correct range: Minimal/+).'] = '最大視區寬度超出了範圍(正確範圍：最小/+)。';
$lang['Minimal Viewport width is out of range (Correct range: 320/+).'] = '最小視區的寬度超出了範圍(正確範圍：320/+)。';
$lang['Mode 180 (Alternative left-right restart)'] = '180模式(替代左右重新啟動)';
$lang['Mode 360'] = '360模式';
$lang['Off'] = '關閉';
$lang['On'] = '開啟';
$lang['Panoramas configuration'] = '全景設定';
$lang['Permanent'] = '永久';
$lang['Picture name substring could not be empty.'] = '相片名稱子字串不能為空白。';
?>