<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Auto'] = 'Tự động';
$lang['Automatic start'] = 'Tự khởi động';
$lang['Border color'] = 'Màu viền ';
$lang['Border width'] = 'Kích thước viền ';
$lang['Caption Color'] = 'Màu chữ mô tả';
$lang['Control display'] = 'Hiển thị điều khiển';
$lang['Footer Color'] = 'Màu nền phần chân';
$lang['Footer Control Color'] = 'Màu điều khiển phần chân';
$lang['Footer display'] = 'Hiển thị phần chân';
$lang['Left'] = 'Trái';
$lang['Mode 360'] = 'Chế độ 360';
$lang['Off'] = 'Tắt';
$lang['On'] = 'Mở';
$lang['Relative speed factor ']['10-90'] = 'Nhân tố tốc độ liên quan [10-90]';
$lang['Relative speed factor is out of range (Correct range: 10-90).'] = 'Nhân tố tốc độ liên quan vượt quá giới hạn [10-90].';
$lang['Viewport width ']['% between 50-100'] = 'Chiều rộng viewport [% từ 50-100]';
$lang['Viewport width ratio is out of range (Correct range: 50-100).'] = 'Tỉ lệ chiều rộng viewport vượt quá giới hạn (Miền hợp lệ: 50-100).';
$lang['You are not authorized to change this configuration (Webmaster only).'] = 'Bạn không được phép thay đổi cấu hình này (chỉ Webmaster).';
$lang['Your configuration is NOT saved due to above reasons.'] = 'Cấu hình của bạn không được lưu vì các lý do nêu trên.';
$lang['Your configuration is saved.'] = 'Cấu hình của bạn đã được lưu.';
$lang['authors of'] = 'các tác giả của';
$lang['Border Width ratio is out of range (Correct range: 0-10).'] = 'Kích thước đường viền toàn cảnh vượt quá giới hạn (Miền hợp lệ: 0-10).';
$lang['Maximal Viewport width is out of range (Correct range: Minimal/+).'] = 'Chiều rộng viewport tối đa vượt quá giới hạn (Miền hợp lệ: Tối thiểu/+).';
$lang['Minimal Viewport width is out of range (Correct range: 320/+).'] = 'Chiều rộng viewport tối thiểu vượt quá giới hạn (Miền hợp lệ: 320/+).';
$lang['Mode 180 (Alternative left-right restart)'] = 'Chế độ 180 (Thay thế khởi động lại trái-phải)';
$lang['Panoramas configuration'] = 'Cấu hình toàn cảnh';
$lang['Permanent'] = 'Cố định';
$lang['Picture name substring could not be empty.'] = 'Chuỗi con trong tên ảnh không được bỏ trống.';
$lang['Picture name substring to display in Mode 180 '][' != previous substring '] = 'Chuỗi con trong tên ảnh phải hiển thị trong Chế độ 180 [ khác chuỗi con khác ]';
$lang['Picture name substring to display in Mode 360'] = 'Chuỗi con trong tên ảnh phải hiển thị trong Chế độ 360';
$lang['Picture name substrings must be different.'] = 'Các chuỗi con trong tên ảnh phải khác nhau.';
$lang['Remove substring from picture title'] = 'Xóa chuỗi con từ tiêu đề ảnh';
$lang['Right'] = 'Phải';
$lang['Rotation direction'] = 'Hướng quay';
$lang['Special thanks to:'] = 'Đặc biệt cảm ơn đến:';
$lang['Start position ']['% between 0-99'] = 'Vị trí bắt đầu [% từ 0-99]';
$lang['Start position ratio is out of range (Correct range: 0-99).'] = 'Tỉ lệ vị trí bắt đầu vượt giới hạn (Miền hợp lệ: 0-99).';
$lang['Viewport maximal width ']['pixels >= minimal width'] = 'Chiều rộng viewport tối đa [pixels >= chiều rộng tối thiểu]';
$lang['Viewport minimal width ']['pixels > 320'] = 'Chiều rộng viewport tối thiểu [pixels > 320]';