<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2012 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Automatic start'] = 'Início automático';
$lang['Auto'] = 'Auto';
$lang['Control display'] = 'Controlar a exibição';
$lang['Left'] = 'Esquerda';
$lang['Maximal Viewport width is out of range (Correct range: Minimal/+).'] = 'Largura máxima de visualização está fora da amplititude (amplititude correta: / Minimal +).';
$lang['Minimal Viewport width is out of range (Correct range: 320/+).'] = 'Largura mínima de visualização está fora da amplititude (amplititude correta: 320 / +).';
$lang['Mode 180 (Alternative left-right restart)'] = 'Modo 180 (Recomeço alternativo Esquerda - Direita)';
$lang['Mode 360'] = 'Modo 360';
$lang['Off'] = 'Off';
$lang['On'] = 'On';
$lang['Panoramas configuration'] = 'Configuração de panoramas';
$lang['Permanent'] = 'Permanente';
$lang['Picture name substring could not be empty.'] = 'Sub-denominação da foto não pode ficar vazio';
$lang['Picture name substring to display in Mode 180 [ != previous substring ]'] = 'Sub-denominação da foto a mostrar no modo 180[!= previous substring]';
$lang['Picture name substring to display in Mode 360'] = 'Sub denominação da foto a mostrar no modo 360';
$lang['Picture name substrings must be different.'] = 'Sub-denominações da foto devem ser diferentes';
$lang['Relative speed factor [10-90]'] = 'Factor de velocidade relativa [10-90]';
$lang['Relative speed factor is out of range (Correct range: 10-90).'] = 'O factor de velocidade relativa está fora da amplitude ( Corrija para a amplitude: 10-90)';
$lang['Remove substring from picture title'] = 'Remover sub-denominações do título da foto';
$lang['Right'] = 'Direita';
$lang['Rotation direction'] = 'Direcção de rotação';
$lang['Special thanks to:'] = 'Especial agradecimento a:';
$lang['Start position [% between 0-99]'] = 'Posição de início [% entre 0-99]';
$lang['Start position ratio is out of range (Correct range: 0-99).'] = 'Posição de início fora dos parâmetros (Corrigir para: 0-99)';
$lang['Viewport maximal width [pixels >= minimal width]'] = 'Visionalizar em largura máxima [pixels> = largura mínima]';
$lang['Viewport minimal width [pixels > 320]'] = 'Visionalizar em largura minima  [pixels > 320]';
$lang['Viewport width [% between 50-100]'] = 'Largura de visionalização [% entre 50-100]';
$lang['Viewport width ratio is out of range (Correct range: 50-100).'] = 'Largura de visionalização fora dos parâmetros (Amplitude: 50-100).';
$lang['You are not authorized to change this configuration (Webmaster only).'] = 'Você não está autorizado a alterar esta configuração (Apenas o Webmaster).';
$lang['Your configuration is NOT saved due to above reasons.'] = 'A configuração NÃO foi salva pelas razões supra';
$lang['Your configuration is saved.'] = 'A sua configuração foi salva';
$lang['authors of'] = 'Autores de';
$lang['Border Width ratio is out of range (Correct range: 0-10).'] = 'O tamanho da borda panorâmica está fora de limite (Corrija o limite: 0-10).';
$lang['Border color'] = 'Corda da borda panorâmica';
$lang['Border width'] = ' Tamanho da borda panorâmica [0-10]';
$lang['Caption Color'] = 'Cor do texto de descrição';
$lang['Footer Color'] = 'Cor de fundo do rodapé';
$lang['Footer Control Color'] = 'Cor de controlo do rodapé';
$lang['Footer display'] = 'Comportamento do rodapé';
?>