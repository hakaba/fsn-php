<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['authors of'] = 'نویسندگانِ';
$lang['Rotation direction'] = 'جهت چرخش';
$lang['Your configuration is NOT saved due to above reasons.'] = 'پیکربندی شما به دلایل بالا ذخیره نشد.';
$lang['Your configuration is saved.'] = 'پیکربندی شما ذخیره شد.';
$lang['Right'] = 'راست';
$lang['Special thanks to:'] = 'سپاس ویژه از:';
$lang['Auto'] = 'خودکار';
$lang['Automatic start'] = 'آغاز خودکار';
$lang['Border Width ratio is out of range (Correct range: 0-10).'] = 'اندازه‌ی قاب پانورامیک بیش از حد مجاز است (محدوده درست: 0-10)';
$lang['Border color'] = 'رنگ قاب پانورامیک';
$lang['Border width'] = 'اندازه‌ی قاب پانورامیک [0-10]';
$lang['Caption Color'] = 'رنگ متن توضیحات';
$lang['Control display'] = 'نمایش کنترل';
$lang['Footer Color'] = 'رنگ پس زمینه فوتر';
$lang['Footer Control Color'] = 'رنگ کنترل فوتر';
$lang['Footer display'] = 'نمایش فوتر';
$lang['Left'] = 'چپ';
$lang['Mode 360'] = 'حالت 360';
$lang['Off'] = 'خاموش';
$lang['On'] = 'روشن';
$lang['Panoramas configuration'] = 'پیکربندی پانوراماها';
$lang['Permanent'] = 'همیشگی';
$lang['Maximal Viewport width is out of range (Correct range: Minimal/+).'] = 'حداکثرنمایه دیدبا پهنای خارج از محدوده  
(محدوده جاری: Minimal/+) ';
$lang['Minimal Viewport width is out of range (Correct range: 320/+).'] = 'کمترین نمای دید  با پهنایی خارج محدود
(محدوده جاری: 320/+)';
$lang['Mode 180 (Alternative left-right restart)'] = 'مدل180 (متداول  از چپ به راست شروع شود )
 ';
$lang['Start position ']['% between 0-99'] = 'وضیت شروع [% between 0-99]';
$lang['Start position ratio is out of range (Correct range: 0-99).'] = 'نسبت وضعیت شروع به خارج از محدوده(محدوده: 0-99).';
$lang['Viewport maximal width ']['pixels >= minimal width'] = 'پهنای در حداکثر نمای [pixels >= minimal width]';
$lang['Viewport minimal width ']['pixels > 320'] = 'حداقل پهنا نمای[pixels > 320]';
$lang['Viewport width ']['% between 50-100'] = '  پهنایی نما[% between 50-100]';
$lang['Viewport width ratio is out of range (Correct range: 50-100).'] = '
نسبت عرضی نمای خارج از محدوده (محدوده جاری: 50-100).';
$lang['You are not authorized to change this configuration (Webmaster only).'] = 'شما مجازنیستید به تغییر این تنظیمات (فقط مدیر سایت).';
$lang['Picture name substring to display in Mode 180 '][' != previous substring '] = 'نام تصویر زیر به نمایش در حالت 180 [ != زیر رشته قبل]';
$lang['Picture name substring to display in Mode 360'] = 'نمایش نام تصویرزیر رشته  در حالت 360';
$lang['Picture name substrings must be different.'] = 'نام تصویر زیر رشته باید متفاوت باشد.';
$lang['Relative speed factor ']['10-90'] = 'فاکتور سرعت خویشاوند [10-90 ]';
$lang['Relative speed factor is out of range (Correct range: 10-90).'] = ' فاکتور سرعت خویشاوند خارج از محدوده(محدوده جاری: 10-90).';
$lang['Remove substring from picture title'] = 'حذف زیر رشته از عنوان تصویر';
$lang['Picture name substring could not be empty.'] = ' نام تصویر زیر رشته نمی تواند خالی باشد.';
?>