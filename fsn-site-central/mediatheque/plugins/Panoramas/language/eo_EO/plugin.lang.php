<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Right'] = 'Dekstro';
$lang['Auto'] = 'Aŭto';
$lang['Automatic start'] = 'Automata komenciĝo';
$lang['Control display'] = 'Kontroli surekranigon';
$lang['Left'] = 'Maldekstro';
$lang['Off'] = 'Malŝaltita';
$lang['On'] = 'Ŝaltita';
$lang['Permanent'] = 'Daŭra';
$lang['Special thanks to:'] = 'Specialajn dankojn al:';
$lang['Start position ']['% between 0-99'] = 'Ekpozicio [% inter 0 kaj 99]';
$lang['Start position ratio is out of range (Correct range: 0-99).'] = 'Ekpozicio estas ekstervarieja (datumzono: 0-99)';
$lang['You are not authorized to change this configuration (Webmaster only).'] = 'Vi ne rajtas ŝanĝi tiun agordon (nur retejestro).';
$lang['Your configuration is NOT saved due to above reasons.'] = 'Via agordo NE konserviĝis pro la ĉisupraj kialoj.';
$lang['Your configuration is saved.'] = 'Via agordo konserviĝis.';
$lang['authors of'] = 'aŭtoroj de';
$lang['Border Width ratio is out of range (Correct range: 0-10).'] = 'La grandeco de la panorama bordero estas ekstervarieja (datumzono: 0-10)';
$lang['Border color'] = 'Koloro de la panorama bordero';
$lang['Border width'] = 'Grandeco de la panorama bordero [0-10]';
$lang['Caption Color'] = 'Koloro de la priskriba teksto';
$lang['Footer Color'] = 'Fonkoloro de la paĝopiedo';
$lang['Footer Control Color'] = 'Koloro de la paĝopieda kontrolbutono';
$lang['Footer display'] = 'Vidigo de la paĝopiedo';
$lang['Maximal Viewport width is out of range (Correct range: Minimal/+).'] = 'La maksimuma larĝeco de la videbla parto estas ekstervarieja (datumzono: Minimuma/+).';
$lang['Minimal Viewport width is out of range (Correct range: 320/+).'] = 'La minimuma larĝeco de la videbla parto estas ekstervarieja (datumzono: 320/+).';
$lang['Mode 180 (Alternative left-right restart)'] = 'Reĝimo 180 (Restarto de la dekstralmaldekstra alternado)';
$lang['Mode 360'] = 'Reĝimo 360';
$lang['Panoramas configuration'] = 'Panorama agordo';
$lang['Rotation direction'] = 'Rotacidirekto';
$lang['Picture name substring could not be empty.'] = 'La bildnomaj subĉenoj ne malplenu!';
$lang['Picture name substring to display in Mode 180 '][' != previous substring '] = 'Bildnoma subĉeno laŭ Reĝimo 180 [!=antaŭa subĉeno]';
$lang['Picture name substring to display in Mode 360'] = 'Bildnoma subĉeno laŭ Reĝimo 360';
$lang['Picture name substrings must be different.'] = 'La bildnomaj subĉenoj estu malsamaj.';
$lang['Relative speed factor ']['10-90'] = 'Rapideco-rilatumo [10-90]';
$lang['Relative speed factor is out of range (Correct range: 10-90).'] = 'La rapideco-rilatumo estas ekstervarieja (datumzono: 10-90).';
$lang['Remove substring from picture title'] = 'Forigi la subĉenon el la bildotitoloj.';
$lang['Viewport maximal width ']['pixels >= minimal width'] = 'Maksimuma larĝeco de la videbla parto [rastrumeroj >= minimuma larĝeco]';
$lang['Viewport minimal width ']['pixels > 320'] = 'Minimuma larĝeco de la videbla parto [rastrumeroj > 320]';
$lang['Viewport width ']['% between 50-100'] = 'Relativa larĝeco de la videbla parto [% inter 50 kaj 100]';
$lang['Viewport width ratio is out of range (Correct range: 50-100).'] = 'La relativa larĝeco de la videbla parto ne validas (akcepteblaj valoroj: 50-100).';
?>