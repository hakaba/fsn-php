<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2012 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['authors of'] = 'autori di';
$lang['Auto'] = 'Automatico';
$lang['Automatic start'] = 'Avvio automatico';
$lang['Control display'] = 'Display controlli';
$lang['Left'] = 'Sinistra';
$lang['Maximal Viewport width is out of range (Correct range: Minimal/+).'] = 'La larghezza massima è fuori dimensione (Correggi il range: Minimo/+)';
$lang['Minimal Viewport width is out of range (Correct range: 320/+).'] = 'La larghezza minima è fuori dimensione (Correggi il range: 320/+)';
$lang['Mode 180 (Alternative left-right restart)'] = 'Modo 180° (Riavvio alternato sinistra-destra)';
$lang['Mode 360'] = 'Modo 360°';
$lang['Off'] = 'Off';
$lang['On'] = 'On';
$lang['Panoramas configuration'] = 'Configurazione Panoramas';
$lang['Permanent'] = 'Permanente';
$lang['Picture name substring could not be empty.'] = 'Il sottotitolo non può essere vuoto';
$lang['Picture name substring to display in Mode 180 [ != previous substring ]'] = 'Sottotitolo da mostrare nel modo 180° (diverso dal sottotitolo precedente)';
$lang['Picture name substring to display in Mode 360'] = 'Sottotitolo da mostrare nel modo 360°';
$lang['Picture name substrings must be different.'] = 'Il sottotitolo deve essere diverso';
$lang['Relative speed factor is out of range (Correct range: 10-90).'] = 'Il fattore di velocità relativa è fuori dall\'intervallo';
$lang['Relative speed factor [10-90]'] = 'Fattore di velocità relativa [10-90]';
$lang['Remove substring from picture title'] = 'Rimuovi sottotitolo dal titolo dell\'immagine';
$lang['Right'] = 'Destra';
$lang['Rotation direction'] = 'Direzione rotazione';
$lang['Special thanks to:'] = 'Ringraziamenti speciali a:';
$lang['Start position ratio is out of range (Correct range: 0-99).'] = 'Il rapporto della posizione iniziale è fuori intervallo (Intervallo corretto: 0-99)';
$lang['Start position [% between 0-99]'] = 'Posizione iniziale [% tra 0-99]';
$lang['You are not authorized to change this configuration (Webmaster only).'] = 'Non sei autorizzato a cambiare questa configurazione (Solo Webmaster)';
$lang['Your configuration is NOT saved due to above reasons.'] = 'La tua configurazione non è stata salvata per le ragioni sopra';
$lang['Your configuration is saved.'] = 'La configurazione è stata salvata';
$lang['Viewport maximal width [pixels >= minimal width]'] = 'Larghezza massima viewport [pixel >= larghezza minima]';
$lang['Viewport minimal width [pixels > 320]'] = 'Larghezza minima viewport [pixel > 320]';
$lang['Viewport width ratio is out of range (Correct range: 50-100).'] = 'La larghezza di viewport è fuori intervallo (Range corretto: 50-100)';
$lang['Viewport width [% between 50-100]'] = 'Larghezza viewport [% tra 50 e 100]';
$lang['Footer display'] = 'Visualizza piè di pagina';
$lang['Footer Control Color'] = 'Controllo del colore piè di pagina';
$lang['Footer Color'] = 'Colore di sfondo piè di pagina';
$lang['Caption Color'] = 'Colore testo descrizione';
$lang['Border width'] = 'Bordo dimensioni panoramica [0-10]';
$lang['Border color'] = 'Colore del bordo panoramica';
$lang['Border Width ratio is out of range (Correct range: 0-10).'] = 'Dimensione del bordo panoramica è fuori campo (range corretto: 0-10).';
?>