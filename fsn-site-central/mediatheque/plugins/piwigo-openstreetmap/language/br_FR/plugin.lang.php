<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2015 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['BASELAYER'] = 'Giz ar gartenn';
$lang['ADD_BEFORE'] = 'Lakaat ul lec\'h a-raok';
$lang['ADD_BEFORE_DESC'] = 'E pelec\'h diskwel titouroù al lec\'h.';
$lang['ALWAYS'] = 'diskouez diouzhtu';
$lang['ATTRIMAGERY'] = 'Diskouez giz ar gartenn';
$lang['ATTRIMAGERY_DESC'] = 'Diskouez giz ar gartenn implijet.';
$lang['ZOOM_DESC'] = '1: pell, 18: tost-ha-tost';
$lang['ZOOM'] = 'Faktor zoum';
$lang['ZOOM_IN'] = 'Zoumañ';
$lang['ZOOM_OUT'] = 'Dizoumañ';
$lang['POPUPAUTHOR'] = 'Aozer al luc\'hskeudenn';
$lang['POPUPLINK'] = 'Lakaat ul liamm d\'al luc\'hskeudenn';
$lang['POPUPNAME'] = 'Anv al luc\'hskeudenn';
$lang['POSITION_INDEX_CMAP'] = 'Diskouez ar gartenn e';
$lang['LONGITUDE'] = 'Hedred';
$lang['MAP'] = 'Kartenn';
$lang['EDIT_MAP'] = 'Kartenn';
$lang['EDIT_UPDATE_LOCATION_DESC'] = 'Klikit er gartenn evit hizivaat al lec\'hiadur.';
$lang['FIND_POSITION'] = 'Kavout va lec\'hiadur';
$lang['LATITUDE'] = 'Ledred';
$lang['CENTER_MAP'] = 'Kreizañ ar gartenn amañ';
$lang['CLICKED_MAP'] = 'Kliket ho peus war ar gartenn e';
$lang['G_MAP'] = 'Kefluniadur ar gartenn';
$lang['HEIGHT'] = 'Uhelder ar gartenn';
$lang['GPX_MAP'] = 'Kartenn GPX';
$lang['LINK_MAP'] = 'Liamm d\'ar gartenn-mañ';
$lang['WIDTH'] = 'Ledander ar gartenn';
$lang['SHOW_COORD'] = 'Diskouez ar c\'henvuzulioù';
$lang['DISPLAY_ON_MAP'] = 'Diskwel %s war ur gartenn';
$lang['DISPLAY'] = 'Diskwel ar c\'hefluniadur';