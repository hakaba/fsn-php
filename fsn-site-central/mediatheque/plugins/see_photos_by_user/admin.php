<?php
// +-----------------------------------------------------------------------+
// | See photos by user plugin for piwigo                                  |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2014 - 2016 ddtddt             http://temmii.com/piwigo/ |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
if (!defined('PHPWG_ROOT_PATH'))
    die('Hacking attempt!');
global $template, $conf, $user;

// +-----------------------------------------------------------------------+
// | Check Access and exit when user status is not ok                      |
// +-----------------------------------------------------------------------+
check_status(ACCESS_ADMINISTRATOR);

global $conf, $page;

$template->assign(
  array(
	'SPBU1' => $conf['see_photos_by_user_nbphotos'],
	'SPBU2' => $conf['see_photos_by_user_limit'],
	'SPBU3' => $conf['see_photos_by_user_order'],
	'SPBU4' => $conf['see_photos_by_user_show'],
	'SPBU5' => $conf['see_photos_by_user_color'],
	'SPBU6' => $conf['see_photos_by_user_show_user_home'],
	'SPBU7' => $conf['see_photos_by_user_shape'],
	'SPBA_PATH2'=> get_root_url() . SPBA_PATH,
  )
);

$SPBU3T = array(
  l10n('users in alphabetical order'),
  l10n('users by reverse alphabetical order'),
  l10n('by increasing number of photos'),
  l10n('by decreasing number of photos'),
);

$SPBU3 = array(
  'username ASC',
  'username DESC',
  'PBU ASC',
  'PBU DESC',
);

$template->assign('spbu', $SPBU3);
$template->assign('spbuT', $SPBU3T);

$PAPAB = pwg_db_fetch_assoc(pwg_query("SELECT state FROM " . PLUGINS_TABLE . " WHERE id = 'Photo_add_by';"));
if($PAPAB['state'] == 'active'){
  $SPBU3T2 = array(
    l10n('nothing'),
    l10n('link Specials menu'),
    l10n('bloc menu users'),
  );
  $SPBU32 = array(
    '-1',
    '1',
    '2',
    ); 
}else{
  $SPBU3T2 = array(
    l10n('link Specials menu'),
    l10n('bloc menu users'),
  );
  $SPBU32 = array(
	'1',
	'2',
  );
}

$template->assign('spbu2', $SPBU32);
$template->assign('spbuT2', $SPBU3T2);

$SPBU4T2 = array(
  l10n('users cloud'),
  l10n('cumulus users cloud'),
  l10n('select users box'),
);

$SPBU42 = array(
  '1',
  '2',
  '3'
);

$template->assign('spbu6', $SPBU42);
$template->assign('spbuT6', $SPBU4T2);

$SPBU7 = array(
  'sphere',
  'vcylinder',
  'hcylinder',
  'vring',
  'hring',
);

$SPBUT7 = array(
  l10n('sphere'),
  l10n('vertical cylinder'),
  l10n('horizontal cylinder'),
  l10n('vertical ring'),
  l10n('horizontal ring'),
);

$template->assign('spbu7', $SPBU7);
$template->assign('spbuT7', $SPBUT7);

if (isset($_POST['submitspbu'])) {
  if (is_numeric($_POST['insspbu1'])) {
    conf_update_param('see_photos_by_user_nbphotos', $_POST['insspbu1']);
  } else {
    array_push($page['errors'], l10n('Maximal number users is incorrect !'));
  }
  if (is_numeric($_POST['insspbu2']))
    conf_update_param('see_photos_by_user_limit', $_POST['insspbu2']);
  else {
    array_push($page['errors'], l10n('Minimal number photos for show users is incorrect !'));
  }
  conf_update_param('see_photos_by_user_order', $_POST['insspbu3']);
  conf_update_param('see_photos_by_user_show', $_POST['insspbu4']);
  conf_update_param('see_photos_by_user_color', $_POST['insspbu5']);
  conf_update_param('see_photos_by_user_show_user_home', $_POST['insspbu6']);
  conf_update_param('see_photos_by_user_shape', $_POST['insspbu7']);
  if (!$page['errors']) {
    array_push($page['infos'], l10n('Update Complete'));
  }
  $template->assign(
	array(
	  'SPBU1' => stripslashes($_POST['insspbu1']),
	  'SPBU2' => stripslashes($_POST['insspbu2']),
	  'SPBU3' => stripslashes($_POST['insspbu3']),
	  'SPBU4' => stripslashes($_POST['insspbu4']),
	  'SPBU5' => stripslashes($_POST['insspbu5']),
	  'SPBU6' => stripslashes($_POST['insspbu6']),
	  'SPBU7' => stripslashes($_POST['insspbu7']),
	)
  );
}

$template->set_filenames(array('plugin_admin_content' => dirname(__FILE__).'/admin.tpl'));
$template->assign_var_from_handle('ADMIN_CONTENT','plugin_admin_content');
?>