{combine_script id='jquery.ui.slider' require='jquery.ui' load='footer' path='themes/default/js/ui/minified/jquery.ui.slider.min.js'}
{combine_css path="themes/default/js/ui/theme/jquery.ui.slider.css"}
{combine_script id="farbtastic" load='footer' require="jquery" path=$SPBA_PATH2|@cat:"js/farbtastic/farbtastic.js"}
{combine_css path=$SPBA_PATH2|@cat:"js/farbtastic/farbtastic.css"}

{footer_script}
jQuery(function($) {
// slide
    $('.range').each(function(){
        var cls=$(this).attr('class');
        var elem=$(this).parent();
        var input=elem.find('input');
        var options={};
        elem.append('<div class="uirange"></div>');
        options.slide=function(event,ui){
            elem.find('label span').empty().append(ui.value);
            input.val(ui.value);
        }
        options.value=input.val();
        options.range='min';
        options.min=$(this).data('min');
        options.max=$(this).data('max');
        elem.find('.uirange').slider(options);
        elem.find('label span').empty().append(input.val());
        input.hide();
    });
    
// schow option cumulus if option is select
    $("input[name=insspbu6]:checked").each(function(){
        if($(this).val()==2){
            $('.optionseecolor').show();
        }
   }); 

   $("input[name=insspbu6]").change(function(){
        if($(this).attr("value")==2){
            $('.optionseecolor').show();
        }else{
            $('.optionseecolor').hide();
        }
   }); 

   // init colorpicker
    $('#colorpicker').farbtastic('#hexval');
   
});
{/footer_script}

{html_style}
.uirange{
    margin:10px;
    margin-left: 40px;
}
{/html_style}

<div class="titrePage">
    <h2>{'See photos by user'|@translate}</h2>
</div>

<form method="post">
 <fieldset>
  <legend>{'Configuration'|translate}</legend>
    <p class="input" style="width: 700px;">
        <label for="range"><strong>{'Minimal number photos for show users'|@translate} </strong><span></span></label>
        <input type="text" name="insspbu1" data-min="0" data-max="1000" class="range" value="{$SPBU1}"/>
    </p>
    <p class="input" style="width: 700px;">
        <label for="range"><strong>{'Maximal number users'|@translate} </strong><span></span></label>
        <input type="text" name="insspbu2" data-min="10" data-max="1000" class="range" value="{$SPBU2}"/>
    </p>
    <p>
        <strong>{'Users order'|@translate}</strong> 
        {html_options name="insspbu3" values=$spbu output=$spbuT selected="{$SPBU3}"}
    </p>
    <p>
        <strong>{'Show menu'|@translate}</strong> 
        <div style="padding-left: 100px">{html_radios separator="<br />" name="insspbu4" values=$spbu2 output=$spbuT2 selected="{$SPBU4}"}</div>
    </p>
    <p>
        <strong>{'Show home page users'|@translate}</strong><br />
    <div id="optionsee6" style="padding-left: 100px">{html_radios separator="<br />" style="padding:15px" name="insspbu6" values=$spbu6 output=$spbuT6 selected="{$SPBU6}"}</div>
    </p>
    <div class="optionseecolor" style="display: none">
        <p>
            <strong>{'Choose a color'|@translate}</strong><br />
            <span id="colorpicker"></span>
            <input type="text" id="hexval" readonly title='{'You must use colorpicker'|@translate}' name="insspbu5" value="{$SPBU5}" size="7" maxlength="7">
        </p>
        <p class="optionseecolor" style="display: none">
            <strong>{'Choose a presentation'|@translate}</strong><br />
            {html_options name="insspbu7" values=$spbu7 output=$spbuT7 selected="{$SPBU7}"}
        </p>
    </div>
    <p>
        <input class="submit" type="submit" name="submitspbu" value="{'Submit'|@translate}">
    </p>
 </fieldset>
</form>
    


