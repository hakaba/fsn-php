<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Maximal number users'] = 'Số người dùng tối đa';
$lang['Maximal number users is incorrect !'] = 'Số người dùng tối đa không hợp lệ !';
$lang['Minimal number photos for show users'] = 'Số ảnh tối thiểu cho trình diễn';
$lang['Minimal number photos for show users is incorrect !'] = 'Số ảnh tối thiểu cho trình diễn không hợp lệ !';
$lang['Photos by user'] = 'Ảnh của người dùng';
$lang['Users order'] = 'thứ tự người dùng';
$lang['select other user'] = 'chọn người dùng khác';
$lang['select user'] = 'chọn người dùng';
$lang['users by reverse alphabetical order'] = 'thứ tự người dùng theo ngược bảng chữ cái';
$lang['users in alphabetical order'] = 'thứ tự người dùng theo bảng chữ cái';
$lang['See photos by user'] = 'Xem ảnh theo người dùng';
$lang['by decreasing number of photos'] = 'bằng cách giảm số ảnh';
$lang['by increasing number of photos'] = 'bằng cách tăng số ảnh';
$lang['link Specials menu'] = 'liên kết menu Specials';
$lang['select users box'] = 'chọn hộp người dùng';
$lang['submit'] = 'gửi';
$lang['bloc menu users'] = 'nhóm menu người dùng';
$lang['cumulus users cloud'] = 'đám mây người dùng';
$lang['Choose a color'] = 'Chọn một màu';
$lang['Choose a presentation'] = 'Chọn một trình diễn';
$lang['Show home page users'] = 'Hiện trang chủ người dùng';
$lang['Show menu'] = 'Hiện menu';
$lang['You must use colorpicker'] = 'Bạn phải dùng bộ chọn màu';
$lang['horizontal cylinder'] = 'hình trụ ngang';
$lang['horizontal ring'] = 'hình vòng ngang';
$lang['sphere'] = 'hình cầu';
$lang['users cloud'] = 'mây người dùng';
$lang['vertical cylinder'] = 'hình trụ đứng';
$lang['vertical ring'] = 'hình vòng đứng';