<?php
// +-----------------------------------------------------------------------+
// | See photos by user plugin for piwigo                                  |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2014 - 2016 ddtddt             http://temmii.com/piwigo/ |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['See photos by user'] = 'Voir les photos par utilisateur';
$lang['select user'] = 'selectionner un utilisateur';
$lang['select other user'] = 'selectionner un autre utilisateur';
$lang['submit'] = 'Valider';
$lang['users in alphabetical order'] = 'utilisateurs par ordre alphabétique';
$lang['users by reverse alphabetical order'] = 'utilisateurs par ordre inversement alphabétique';
$lang['by increasing number of photos'] = 'par nombre de photos croissant';
$lang['by decreasing number of photos'] = 'par nombre de photos décroissant';
$lang['Minimal number photos for show users'] = 'Nombre minimal de photos pour montrer les utilisateurs';
$lang['Maximal number users'] = 'nombre maximal d\'utilisateurs';
$lang['Users order'] = 'Ordre des utilisateurs';
$lang['Maximal number users is incorrect !'] = 'Le nombre maximal d\'utilisateurs est incorrect !';
$lang['Minimal number photos for show users is incorrect !'] = 'Le nombre minimal de photos pour montrer les utilisateurs est incorrect !';
$lang['link Specials menu'] = 'lien dans le menu Spéciales';
$lang['select users box'] = 'la boite de sélection d\'utilisateurs';
$lang['bloc menu users'] = 'Le bloc menu des utilisateurs';
$lang['Photos by user'] = 'Photos par utilisateur';
$lang['You must use colorpicker'] = 'Vous devez utiliser le le sélecteur de couleurs';
$lang['users cloud'] = 'nuage d\'utilisateurs';
$lang['cumulus users cloud'] = 'nuage animé d\'utilisateurs';
$lang['Show menu'] = 'Afficher dans le menu';
$lang['Show home page users'] = 'Afficher sur la page d\'accueil des utilisateurs';
$lang['Choose a color'] = 'Choisir une couleur';
$lang['Choose a presentation'] = 'Choisir le type de presentation';
$lang['sphere'] = 'sphere';
$lang['vertical cylinder'] = 'cylindre vertical';
$lang['horizontal cylinder'] = 'cylindre horizontal';
$lang['vertical ring'] = 'anneau vertical';
$lang['horizontal ring'] = 'anneau horizontal';