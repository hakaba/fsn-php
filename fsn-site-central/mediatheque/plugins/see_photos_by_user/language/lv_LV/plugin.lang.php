<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['See photos by user'] = 'Skatīt pēc lietotāja izvēlētus foto';
$lang['Users order'] = 'lietotāju secība';
$lang['select user'] = 'izvēlēties lietotāju';
$lang['select other user'] = 'izvēlēties citu lietotāju';
$lang['submit'] = 'apstiprināt';
$lang['users by reverse alphabetical order'] = 'lietotāji apgrieztā alfabētiskā secībā';
$lang['users in alphabetical order'] = 'lietotāji alfabēta secībā';
$lang['select users box'] = 'izvēlieties lietotāju rūtiņu';
$lang['link Specials menu'] = 'Specials izvēlnes saite';
$lang['by increasing number of photos'] = 'pēc foto skaita pieauguma';
$lang['by decreasing number of photos'] = 'pēc foto skaita samazināšanās';
$lang['bloc menu users'] = 'bloķēt izvēlnes lietotājus';
$lang['Maximal number users is incorrect !'] = 'Kļūdaims maksimālais lietotāju skaits ';
$lang['Minimal number photos for show users is incorrect !'] = 'Kļūdains minimālais lietotājiem rādāmo foto skaits';
$lang['Minimal number photos for show users'] = 'Minimālais lietotājiem rādāmo foto skaits';
$lang['Maximal number users'] = 'Maksimālais lietotāju skaits';
$lang['Photos by user'] = 'Lietotāja Foto';
$lang['users cloud'] = 'lietotāju mākonis';
$lang['cumulus users cloud'] = 'curmulus lietotāju mākonis';
$lang['You must use colorpicker'] = 'Jums jālieto krāsu ņemēju';
$lang['Show menu'] = 'Rādīt izvēlni';
$lang['Show home page users'] = 'Rādīt mājas lapas lietotāju';
$lang['vertical ring'] = 'verikāls aplis';
$lang['vertical cylinder'] = 'vertikāls cilindrs';
$lang['sphere'] = 'sfēra';
$lang['horizontal ring'] = 'horizontāls aplis';
$lang['horizontal cylinder'] = 'horizontāls cilindrs';
$lang['Choose a presentation'] = 'Izvēlaties prezentāciju';
$lang['Choose a color'] = 'Izvēlaties krāsu';