<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['users in alphabetical order'] = 'потребители по азбучен ред';
$lang['users by reverse alphabetical order'] = 'потребители в обратен азбучен ред';
$lang['submit'] = 'потвърди';
$lang['select users box'] = 'избери потребителска кутия';
$lang['select user'] = 'избери потребител';
$lang['select other user'] = 'избор на други потребители';
$lang['link Specials menu'] = 'линк меню Специални';
$lang['by increasing number of photos'] = 'по низходящ номер на снимките';
$lang['by decreasing number of photos'] = 'по възходящ номер на снимките';
$lang['bloc menu users'] = 'блок меню потребители';
$lang['Users order'] = 'списък потребители';
$lang['See photos by user'] = 'Виж снимките по потребител';
$lang['Photos by user'] = 'Снимки по потребител';
$lang['Minimal number photos for show users is incorrect !'] = 'Максималния брой снимки за показване от потребител е неправилен!';
$lang['Minimal number photos for show users'] = 'Максимален брой снимки за показване от потребител';
$lang['Maximal number users is incorrect !'] = 'Максималния брой потребители е неправилен!';
$lang['Maximal number users'] = 'Максимален брой потребители';
$lang['cumulus users cloud'] = 'групови потребители на облак';
$lang['users cloud'] = 'потребители на облак';
$lang['You must use colorpicker'] = 'Трябва да използвате ColorPicker';
$lang['Show menu'] = 'Покажи меню';
$lang['Show home page users'] = 'Покажи началната страница на потребителя';
$lang['vertical ring'] = 'вертикален пръстен';
$lang['vertical cylinder'] = 'вертикален цилиндър';
$lang['sphere'] = 'сфера';
$lang['horizontal ring'] = 'хоризонтален пръстен';
$lang['horizontal cylinder'] = 'хоризонтален цилиндър';
$lang['Choose a presentation'] = 'Избор на преставяне';
$lang['Choose a color'] = 'Избори цвят';