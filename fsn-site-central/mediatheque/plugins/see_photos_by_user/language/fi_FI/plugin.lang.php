<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Maximal number users'] = 'Käyttäjien maksimi määrä';
$lang['Maximal number users is incorrect !'] = 'Maksimimäärä käyttäjiä on virheellinen!';
$lang['Minimal number photos for show users'] = 'Minimimäärä kuvia näytettäväksi';
$lang['Minimal number photos for show users is incorrect !'] = 'Mimimimäärä kuvia näytettäväksi on virheellinen!';
$lang['Photos by user'] = 'Kuvat käyttäjältä';
$lang['See photos by user'] = 'Katso kuvia käyttäjältä';
$lang['Users order'] = 'käyttäjät järestyksessä';
$lang['bloc menu users'] = 'käyttäjävalikon mukaan';
$lang['by decreasing number of photos'] = 'kuvien määrän mukaan vähenevässä järjestyksessä';
$lang['by increasing number of photos'] = 'kuvien määrän mukaan kasvavassa järjestyksessä';
$lang['link Specials menu'] = 'linkki erikoisasetukset valikkoon';
$lang['select other user'] = 'valitse toinen käyttäjä';
$lang['select user'] = 'valitse käyttäjä';
$lang['select users box'] = 'valitse käyttäjät valikko';
$lang['submit'] = 'lähetä';
$lang['users by reverse alphabetical order'] = 'käyttäjät käänteisessä aakkosjärjestyksessä';
$lang['users in alphabetical order'] = 'käyttäjät aakkosjärjestyksessä';
$lang['cumulus users cloud'] = 'cumulus käyttäjäpilvi';
$lang['Show home page users'] = 'Näytä käyttäjän kotisivu';
$lang['users cloud'] = 'käyttäjäpilvi';
$lang['Choose a color'] = 'Valitse väri';
$lang['Choose a presentation'] = 'Valitse esitys';
$lang['Show menu'] = 'Näytä menu';
$lang['You must use colorpicker'] = 'Käytä värinvalitsinta';
$lang['horizontal cylinder'] = 'vaakatasoinen sylinteri';
$lang['horizontal ring'] = 'vaakatasoinen rengas';
$lang['sphere'] = 'pallo';
$lang['vertical cylinder'] = 'pystytasoinen sylinteri';
$lang['vertical ring'] = 'pystytasoinen rengas';