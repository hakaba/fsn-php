<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['See photos by user'] = 'Se foton från användare';
$lang['Maximal number users'] = 'Maximalt antal användare';
$lang['Maximal number users is incorrect !'] = 'Maximalt antal användare är felaktigt!';
$lang['Minimal number photos for show users'] = 'Minsta antal foton att visa användare';
$lang['Minimal number photos for show users is incorrect !'] = 'Minsta antal foton att visa användare är felaktigt!';
$lang['Photos by user'] = 'Foton av användare';
$lang['select user'] = 'välj användare';
$lang['select users box'] = 'välj användarens samling';
$lang['submit'] = 'verkställ';
$lang['users by reverse alphabetical order'] = 'användare i motsatt alfabetisk ordning';
$lang['users in alphabetical order'] = 'användare i alfabetisk ordning';
$lang['bloc menu users'] = 'meny för användarblockering';
$lang['by decreasing number of photos'] = 'genom minskning av antal foton';
$lang['by increasing number of photos'] = 'genom ökning av antal foton';
$lang['link Specials menu'] = 'länka special-meny';
$lang['select other user'] = 'välj annan användare';
$lang['Users order'] = 'användarordning';
$lang['users cloud'] = 'användarens moln';
$lang['You must use colorpicker'] = 'Du måste använda färgväljaren';
$lang['Show menu'] = 'Visa meny';
$lang['Choose a color'] = 'Välj en färg';
$lang['Choose a presentation'] = 'Välj en presentation';
$lang['horizontal cylinder'] = 'horisontell cylinder';
$lang['horizontal ring'] = 'horisontell ring';
$lang['sphere'] = 'sfär';
$lang['vertical cylinder'] = 'vertikal cylinder';
$lang['vertical ring'] = 'vertikal ring';
$lang['Show home page users'] = 'Till användarens startsida';
$lang['cumulus users cloud'] = 'Cumulus användarmoln.';