<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Maximal number users'] = 'Maximale Anzahl von Benutzern';
$lang['Maximal number users is incorrect !'] = 'Maximale Anzahl von Benutzern ist ungültig!';
$lang['See photos by user'] = 'Fotos nach Benutzer anzeigen';
$lang['by decreasing number of photos'] = 'absteigend nach Anzahl der Fotos';
$lang['by increasing number of photos'] = 'aufsteigend nach Anzahl der Fotos';
$lang['select other user'] = 'Anderen Benutzer wählen';
$lang['select user'] = 'Benutzer auswählen';
$lang['submit'] = 'Bestätigen';
$lang['users by reverse alphabetical order'] = 'Benutzer in umgekehrter alphabetischer Reihenfolge';
$lang['users in alphabetical order'] = 'Benutzer in alphabetischer Reihenfolge';
$lang['Users order'] = 'Benutzer-Reihenfolge';
$lang['Minimal number photos for show users is incorrect !'] = 'Minimale Anzahl von Fotos des Benutzers zum Anzeigen ist ungültig!';
$lang['bloc menu users'] = 'Benutzer Menü blockieren';
$lang['link Specials menu'] = 'Spezial-Menü verlinken';
$lang['select users box'] = 'Benutzer-Block wählen';
$lang['Minimal number photos for show users'] = 'Minimale Anzahl von Fotos des Benutzers zum Anzeigen';
$lang['Photos by user'] = 'Fotos nach Benutzer';
$lang['Show home page users'] = 'Benutzer-Startseite anzeigen';
$lang['Show menu'] = 'Menü anzeigen';
$lang['You must use colorpicker'] = 'Sie müssen den Farbwähler benutzen';
$lang['cumulus users cloud'] = 'Cumulus Cloud-Benutzer';
$lang['horizontal cylinder'] = 'Horizontaler Zylinder';
$lang['horizontal ring'] = 'Horizontaler Ring';
$lang['sphere'] = 'Sphäre';
$lang['users cloud'] = 'Cloud-Benutzer';
$lang['vertical cylinder'] = 'Vertikaler Zylinder';
$lang['vertical ring'] = 'Vertikaler Ring';
$lang['Choose a presentation'] = 'Eine Präsentation wählen';
$lang['Choose a color'] = 'Farbe wählen';