<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Maximal number users'] = 'Número máximo de usuários';
$lang['Maximal number users is incorrect !'] = 'Número máximo de usuários está incorreto!';
$lang['Minimal number photos for show users'] = 'Número mínimo de fotos para mostrar usuários';
$lang['Minimal number photos for show users is incorrect !'] = 'Número mínimo de fotos para mostrar usuários está incorreto!';
$lang['See photos by user'] = 'Veja fotos por usuários';
$lang['Users order'] = 'Ordenar usuários';
$lang['bloc menu users'] = 'Bloqueia menu usuários';
$lang['by decreasing number of photos'] = 'Por número decrescente de fotos';
$lang['by increasing number of photos'] = 'Por número crescente de fotos';
$lang['link Specials menu'] = 'Link menus especial';
$lang['select other user'] = 'Selecione outros usuários';
$lang['select user'] = 'Selecione usuário';
$lang['select users box'] = 'Selecione caixa de usuário';
$lang['submit'] = 'Submeta';
$lang['users by reverse alphabetical order'] = 'usuários por ordem alfabética reversa';
$lang['users in alphabetical order'] = 'Usuários por ordem alfabética';
$lang['Photos by user'] = 'Fotos por usuário';
$lang['You must use colorpicker'] = 'Você deve usar o seletor de cores';
$lang['Choose a color'] = 'Escolha uma cor';
$lang['Choose a presentation'] = 'Escolha uma apresentação';
$lang['Show home page users'] = 'Mostrar a página principal do usuário';
$lang['Show menu'] = 'Mostrar menu';
$lang['cumulus users cloud'] = 'usuários das núvens';
$lang['horizontal cylinder'] = 'cilindro horizontal';
$lang['horizontal ring'] = 'anel horizontal';
$lang['sphere'] = 'esfera';
$lang['users cloud'] = 'usuários das núvens';
$lang['vertical cylinder'] = 'cilindro vertical';
$lang['vertical ring'] = 'anel vertical';