<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Maximal number users'] = 'Maksimalt antal brugere';
$lang['Maximal number users is incorrect !'] = 'Maksimalt antal brugere er ikke korrekt!';
$lang['Minimal number photos for show users'] = 'Minimalt antal fotografier der skal vises til brugere';
$lang['Minimal number photos for show users is incorrect !'] = 'Minimalt antal fotografier der skal vises til brugere er ikke korrekt!';
$lang['See photos by user'] = 'Se brugers fotografier';
$lang['by decreasing number of photos'] = 'ved at formindste antallet af fotografier';
$lang['by increasing number of photos'] = 'ved at forøge antallet af fotografier';
$lang['select other user'] = 'vælg en anden bruger';
$lang['select user'] = 'vælg brug';
$lang['submit'] = 'udfør';
$lang['users by reverse alphabetical order'] = 'brugere i omvendt alfabetisk rækkefølge';
$lang['users in alphabetical order'] = 'brugere i alfabetisk rækkefølge';
$lang['Users order'] = 'brugerrækkefølge';
$lang['link Specials menu'] = 'Link i Særligt-menu';
$lang['select users box'] = 'Vælg brugere-boks';
$lang['bloc menu users'] = 'Bloc-brugermenu';
$lang['Photos by user'] = 'Brugers fotografier';
$lang['You must use colorpicker'] = 'Du skal bruge colorpicker';
$lang['Show home page users'] = 'Vis brugers hjemmeside';
$lang['Show menu'] = 'Vis menu';
$lang['cumulus users cloud'] = 'cumulusbrugersky';
$lang['users cloud'] = 'brugersky';
$lang['Choose a color'] = 'Vælg en farve';
$lang['Choose a presentation'] = 'Vælg en præsentation';
$lang['horizontal cylinder'] = 'horisontal cylinder';
$lang['horizontal ring'] = 'horisontal ring';
$lang['sphere'] = 'kugle';
$lang['vertical cylinder'] = 'vertikal cylinder';
$lang['vertical ring'] = 'vertikal ring';