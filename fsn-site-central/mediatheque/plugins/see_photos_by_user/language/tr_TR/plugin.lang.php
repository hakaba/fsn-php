<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['link Specials menu'] = 'Specials menü bağlantısı';
$lang['users by reverse alphabetical order'] = 'alfabetik sıralamanın tersi kullanıcılar';
$lang['users in alphabetical order'] = 'alfabetik sırada kullanıcılar';
$lang['by decreasing number of photos'] = 'fotoğraf sayısını azaltarak';
$lang['bloc menu users'] = 'kullanıcılar blok menüsü';
$lang['by increasing number of photos'] = 'fotoğraf sayısını arttırarak';
$lang['select users box'] = 'kullanıcı kutusunu seç';
$lang['submit'] = 'gönder';
$lang['select user'] = 'kullanıcı seç';
$lang['select other user'] = 'başka kullanıcı seç';
$lang['Minimal number photos for show users is incorrect !'] = 'Kullanıcılara göre görüntülenecek asgari fotoğraf sayısı hatalı!';
$lang['Users order'] = 'kullanıcı sıralaması';
$lang['See photos by user'] = 'Kullanıcılara göre fotoğrafları görüntüle';
$lang['Minimal number photos for show users'] = 'Kullanıcılara göre görüntülenecek asgari fotoğraf sayısı';
$lang['Maximal number users is incorrect !'] = 'Azami kullanıcı sayısı hatalı!';
$lang['Maximal number users'] = 'Azami kullanıcı sayısı';
$lang['Photos by user'] = 'Kullanıcı Fotoğrafları';
$lang['Show home page users'] = 'Ana sayfa kullanıcısını göster';
$lang['cumulus users cloud'] = 'cumulus kullanıcı bulutu';
$lang['Choose a color'] = 'Bir renk seçin';
$lang['Choose a presentation'] = 'Bir sunum seçin';
$lang['Show menu'] = 'Menüyü göster';
$lang['You must use colorpicker'] = 'Renk reçiciyi kullanmalısınız';
$lang['horizontal cylinder'] = 'yatay silindir';
$lang['horizontal ring'] = 'yatay çember';
$lang['sphere'] = 'küre';
$lang['users cloud'] = 'kullanıcı bulutu';
$lang['vertical cylinder'] = 'düşey silindir';
$lang['vertical ring'] = 'düşey çember';