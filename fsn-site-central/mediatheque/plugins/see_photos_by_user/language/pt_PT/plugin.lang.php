<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Maximal number users is incorrect !'] = 'Número máximo de utilizadores é incorreto!';
$lang['Minimal number photos for show users'] = 'Número mínimo de fotos a mostrar aos utilizadores';
$lang['Minimal number photos for show users is incorrect !'] = 'Número mínimo de fotos a mostrar aos utilizadores é incorreto!';
$lang['See photos by user'] = 'Ver fotos por utilizador';
$lang['Users order'] = 'Ordenar utilizadores';
$lang['bloc menu users'] = 'Bloco do menú utilizadores';
$lang['by decreasing number of photos'] = 'Fotos por ordem decrescente';
$lang['by increasing number of photos'] = 'Fotos por ordem crescente';
$lang['Maximal number users'] = 'Número máximo de utilizadores';
$lang['Photos by user'] = 'Fotos por utilizador';
$lang['link Specials menu'] = 'Menú de liks especiais';
$lang['select other user'] = 'Selecionar outro utilizador';
$lang['select user'] = 'Selecionar utilizador';
$lang['select users box'] = 'Caixa de seleção de utilizadores';
$lang['submit'] = 'Submeter';
$lang['users by reverse alphabetical order'] = 'Utilizadores por ordem inversa da alfabética';
$lang['users in alphabetical order'] = 'Utilizadores por ordem alfabética';
$lang['Choose a color'] = 'Escolha uma cor';
$lang['Choose a presentation'] = 'Escolha uma apresentação';
$lang['Show home page users'] = 'Mostrar a página inicial do utilizador';
$lang['Show menu'] = 'Mostrar o menú';
$lang['You must use colorpicker'] = 'Deverá usar o Agarrador de cor';
$lang['cumulus users cloud'] = 'Nuvem cúmulo do utilizadores';
$lang['horizontal cylinder'] = 'Cilindro horizontal';
$lang['horizontal ring'] = 'Anel horizontal';
$lang['sphere'] = 'Esfera';
$lang['users cloud'] = 'Nuvem dos utilizadores';
$lang['vertical cylinder'] = 'Cilindro vertical';
$lang['vertical ring'] = 'Anel vertical';