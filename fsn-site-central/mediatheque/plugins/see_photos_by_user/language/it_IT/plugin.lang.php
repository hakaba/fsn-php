<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['users by reverse alphabetical order'] = 'utenti per ordine alfabetico inverso';
$lang['users in alphabetical order'] = 'utenti in ordine alfabetico';
$lang['Users order'] = 'ordine utenti';
$lang['select user'] = 'seleziona utente';
$lang['submit'] = 'invia';
$lang['select other user'] = 'scegli altro utente';
$lang['by decreasing number of photos'] = 'per numero decrescente di foto';
$lang['by increasing number of photos'] = 'per numero crescente di foto';
$lang['Minimal number photos for show users is incorrect !'] = 'Numero minimo foto per mostra agli utenti non è corretto!';
$lang['See photos by user'] = 'Vedi foto per utente';
$lang['Minimal number photos for show users'] = 'Numero minimo foto da mostrare agli utenti';
$lang['Maximal number users is incorrect !'] = 'Numero massimo utenti non corretto!';
$lang['Maximal number users'] = 'Numero massimo utenti';
$lang['select users box'] = 'Seleziona box utenti';
$lang['link Specials menu'] = 'Link menu Speciale';
$lang['bloc menu users'] = 'Blocco menu utenti';
$lang['Photos by user'] = 'Foto per utente';
$lang['vertical ring'] = 'ring verticale';
$lang['vertical cylinder'] = 'cilindro verticale';
$lang['users cloud'] = 'utenti cloud';
$lang['sphere'] = 'sfera';
$lang['horizontal ring'] = 'ring orizzontale';
$lang['horizontal cylinder'] = 'cilindro orizzontale';
$lang['cumulus users cloud'] = 'utenti ospitati su cloud';
$lang['You must use colorpicker'] = 'Devi utilizzare il colorpicker';
$lang['Show menu'] = 'Mostra menu';
$lang['Show home page users'] = 'Mostra home page utente';
$lang['Choose a presentation'] = 'Scegli una presentazione';
$lang['Choose a color'] = 'Scegli un colore';