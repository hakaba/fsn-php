<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Maximal number users'] = 'Maksimalt antall brukere';
$lang['Maximal number users is incorrect !'] = 'Maksimalt antall brukere er feil!';
$lang['Minimal number photos for show users'] = 'Minimalt antall bilder for valgte brukere';
$lang['Minimal number photos for show users is incorrect !'] = 'Minimalt antall bilder for valgte brukere er feil!';
$lang['Photos by user'] = 'Bilder av bruker';
$lang['See photos by user'] = 'Se bilder til bruker';
$lang['Users order'] = 'brukere bestiller';
$lang['bloc menu users'] = 'blokk meny brukere';
$lang['by decreasing number of photos'] = 'ved å redusere antall bilder';
$lang['by increasing number of photos'] = 'ved å øke antall bilder';
$lang['link Specials menu'] = 'link til spesial meny';
$lang['select other user'] = 'velg annen bruker';
$lang['select user'] = 'velg bruker';
$lang['select users box'] = 'velg brukers boks';
$lang['submit'] = 'legg til';
$lang['users by reverse alphabetical order'] = 'brukere ved omvendt alfabetisk rekkefølge';
$lang['users in alphabetical order'] = 'brukere etter alfabetisk rekkefølge';
$lang['You must use colorpicker'] = 'Du må bruke fargevelger';
$lang['Show home page users'] = 'Vis hjemmeside bruker';
$lang['Show menu'] = 'Vis meny';
$lang['cumulus users cloud'] = 'globular bruker sky';
$lang['users cloud'] = 'bruker sky';
$lang['Choose a color'] = 'Velg en farge';
$lang['Choose a presentation'] = 'Velg en presentasjon';
$lang['horizontal cylinder'] = '
horisontal sylinder';
$lang['horizontal ring'] = 'horisontal ring';
$lang['sphere'] = 'kule';
$lang['vertical cylinder'] = 'vertikal sylinder';
$lang['vertical ring'] = 'vertikal ring';