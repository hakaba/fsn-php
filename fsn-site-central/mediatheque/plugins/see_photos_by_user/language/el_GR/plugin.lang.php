<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['See photos by user'] = 'Δείτε φωτογραφίες από τη μεριά του χρήστη';
$lang['Minimal number photos for show users is incorrect !'] = 'Ο ελάχιστος αριθμός φωτογραφιών για εμφάνιση στους χρήστες είναι λανθασμένος!';
$lang['Minimal number photos for show users'] = 'Ο ελάχιστος αριθμός φωτογραφιών για εμφάνιση στους χρήστες';
$lang['Maximal number users is incorrect !'] = 'Ο Μέγιστος αριθμός χρηστών είναι λανθασμένος!';
$lang['Maximal number users'] = 'Μέγιστος αριθμός χρηστών';
$lang['users in alphabetical order'] = 'χρήστες σε αλφαβητική σειρά';
$lang['users by reverse alphabetical order'] = 'χρήστες με αντίστροφη αλφαβητική σειρά';
$lang['submit'] = 'υποβολή';
$lang['select users box'] = 'επιλέξετε τους χρήστες με κουτί';
$lang['select user'] = 'επιλέξτε χρήστη';
$lang['select other user'] = 'επιλέξτε άλλο χρήστη';
$lang['link Specials menu'] = 'σύνδεσμος Ειδικά μενού';
$lang['by increasing number of photos'] = 'αυξάνοντας τον αριθμό των φωτογραφιών';
$lang['by decreasing number of photos'] = 'μειώνωντας τον αριθμό των φωτογραφιών';
$lang['bloc menu users'] = 'μενού μπλοκαρίσματος χρηστών';
$lang['Users order'] = 'κατάταξη χρηστών';
$lang['Photos by user'] = 'Φωτογραφίες ανά χρήστη';
$lang['vertical ring'] = 'κάθετος δακτύλιος';
$lang['vertical cylinder'] = 'κάθετος κύλινδρος';
$lang['users cloud'] = 'σύννεφο χρηστών';
$lang['sphere'] = 'σφαίρα';
$lang['horizontal ring'] = 'Οριζόντιος δακτύλιος';
$lang['cumulus users cloud'] = 'cumulus users cloud';
$lang['horizontal cylinder'] = 'Οριζόντιος Κύλινδρος';
$lang['You must use colorpicker'] = 'Πρέπει να χρησιμοποιήσετε τον επιλογέα χρωμάτων';
$lang['Show menu'] = 'Προβολή του Μενού';
$lang['Show home page users'] = 'Προβολή της αρχικής σελίδας χρήστη';
$lang['Choose a presentation'] = 'Επιλέξτε μια Παρουσίαση';
$lang['Choose a color'] = 'Επιλέξτε χρώμα';