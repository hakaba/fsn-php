<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['link Specials menu'] = 'посилання на меню Спеціальні';
$lang['select users box'] = 'вікно вибору користувачів';
$lang['bloc menu users'] = 'блок меню користувачів';
$lang['cumulus users cloud'] = '"купчаста хмара" користувачів';
$lang['See photos by user'] = 'Переглянути фотографії від користувача';
$lang['You must use colorpicker'] = 'Ви повинні використовувати colorpicker';
$lang['Show home page users'] = 'Показати домашню сторынку користувача';
$lang['Photos by user'] = 'Photos by user';
$lang['Users order'] = 'Сортування користувачів';
$lang['by increasing number of photos'] = 'по збільшенню кількості фотографій';
$lang['by decreasing number of photos'] = 'по зменшенню кількості фотографій';
$lang['users by reverse alphabetical order'] = 'користувачі по зворотньому алфавітному порядку';
$lang['users cloud'] = 'хмара користувачів';
$lang['Choose a presentation'] = 'Виберіть презентацію';
$lang['select other user'] = 'вибрати іншого користувача';
$lang['submit'] = 'підтвердити';
$lang['horizontal ring'] = 'горизонтальне коло';
$lang['horizontal cylinder'] = 'горизонтальний циліндр';
$lang['Show menu'] = 'Показати меню';
$lang['Minimal number photos for show users is incorrect !'] = 'Мінімальна кількість фотографій для показу користувачам неправильна!';
$lang['Minimal number photos for show users'] = 'Мінімальна кількість фотографій для показу користувачам';
$lang['Maximal number users is incorrect !'] = 'Максимальна кількість користувачів не правильна!';
$lang['Maximal number users'] = 'Максимальна кількість користувачів';
$lang['Choose a color'] = 'Виберіть колір';
$lang['select user'] = 'виберіть користувача';
$lang['sphere'] = 'сфера';
$lang['users in alphabetical order'] = 'користувачі у алфавітному порядку';
$lang['vertical ring'] = 'вертикальне кільце';
$lang['vertical cylinder'] = 'вертикальний циліндр';