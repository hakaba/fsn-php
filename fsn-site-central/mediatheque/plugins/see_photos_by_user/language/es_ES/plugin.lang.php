<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Maximal number users'] = 'Máximo número de usuarios';
$lang['Maximal number users is incorrect !'] = 'Máximo número de usuarios es incorrecto';
$lang['Minimal number photos for show users'] = 'Numero mínimo de fotos a enseñar a los usuarios';
$lang['Minimal number photos for show users is incorrect !'] = 'Numero mínimo de fotos a enseñar a los usuarios es incorrecto';
$lang['See photos by user'] = 'Ver fotos de un usuario';
$lang['Users order'] = 'Orden de los usuarios';
$lang['bloc menu users'] = 'Bloque del menú usuarios';
$lang['by decreasing number of photos'] = 'al disminuir la cantidad de fotos';
$lang['by increasing number of photos'] = 'al incrementar la cantidad de fotos';
$lang['link Specials menu'] = 'Enlace al menú especial ';
$lang['select other user'] = 'Seleccionar otro usuario';
$lang['select user'] = 'Seleccionar usuario';
$lang['select users box'] = 'Seleccionar la casilla de usuarios';
$lang['submit'] = 'Enviar';
$lang['users by reverse alphabetical order'] = 'Usuarios en orden alfabético inverso';
$lang['users in alphabetical order'] = 'Usuarios en orden alfabético';
$lang['Photos by user'] = 'Fotos del usuario';
$lang['cumulus users cloud'] = 'Usuario de nube Cumulus';
$lang['users cloud'] = 'Usuario de nube';
$lang['Choose a color'] = 'Eligir un color ';
$lang['Choose a presentation'] = 'Eligir una presentación';
$lang['Show home page users'] = 'Mostrar la pagina personal del usuario';
$lang['Show menu'] = 'Mostrar menú';
$lang['You must use colorpicker'] = 'Tiene que utilizar el gestor de colores';
$lang['horizontal cylinder'] = 'cilindro horizontal';
$lang['horizontal ring'] = 'Circulo horizontal';
$lang['sphere'] = 'Esfera';
$lang['vertical cylinder'] = 'Cilindro vertical';
$lang['vertical ring'] = 'Circulo vertical';