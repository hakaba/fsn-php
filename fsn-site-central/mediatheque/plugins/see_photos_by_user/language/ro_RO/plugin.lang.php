<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Show'] = 'Arată';
$lang['by decreasing number of photos'] = 'prin micșorarea numărului de fotografii';
$lang['by increasing number of photos'] = 'prin creșterea numărului de fotografii';
$lang['Maximal number users'] = 'Număr maxim de utilizatori';
$lang['Maximal number users is incorrect !'] = 'Număr maxim de utilizatori este incorect';
$lang['Users order'] = 'ordine utilizatori';
$lang['select other user'] = 'selectează un alt utilizator';
$lang['select user'] = 'selectează utilizator';
$lang['users in alphabetical order'] = 'utilizatori în ordine alfabetică';
$lang['users by reverse alphabetical order'] = 'utilziatori in ordine alfabetica inversa';
$lang['submit'] = 'trimite';
$lang['select users box'] = 'selectati caseta de utilizatori';
$lang['link Specials menu'] = 'legatura meniu Special';
$lang['bloc menu users'] = 'Meniu pentru utilizator bloc';
$lang['Photos by user'] = 'Fotografii dupa utilizator';
$lang['See photos by user'] = 'Vezi fotografii dupa utilizator';
$lang['Minimal number photos for show users is incorrect !'] = 'Numarul minim de fotografii ce poate fi vazut de utilizatori este incorect!';
$lang['Minimal number photos for show users'] = 'Numarul minim de fotografii ce poate fi vazut de utilizatori';
?>