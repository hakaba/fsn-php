<?php
/*
  Plugin Name: See photos by user
  Version: 2.8.a
  Description: See photos by user
  Plugin URI: http://piwigo.org/ext/extension_view.php?eid=723
  Author: ddtddt
Author URI: http://temmii.com/piwigo/
*/
// +-----------------------------------------------------------------------+
// | See photos by user plugin for piwigo                                  |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2014 - 2016 ddtddt             http://temmii.com/piwigo/ |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+

if (!defined('PHPWG_ROOT_PATH'))
    die('Hacking attempt!');

define('SPBA_DIR', basename(dirname(__FILE__)));
define('SPBA_PATH', PHPWG_PLUGINS_PATH . SPBA_DIR . '/');
define('SPBA_ADMIN',get_root_url().'admin.php?page=plugin-'.SPBA_DIR);

include_once(SPBA_PATH . 'include/function.see.inc.php');

add_event_handler('loading_lang', 'see_photos_by_user_loading_lang');	  
function see_photos_by_user_loading_lang(){
  load_language('plugin.lang', SPBA_PATH);
  load_language('lang', PHPWG_ROOT_PATH . 'local/', array('no_fallback' => true, 'local' => true));
}


global $conf;

/*init plugin - filter http*/
add_event_handler('loc_end_section_init', 'section_init_SPBA');
function section_init_SPBA(){
  global $tokens, $conf, $template;
  if(strpbrk('user-', $_SERVER['REQUEST_URI'])!=false){
    $testa = explode('user-', $_SERVER['REQUEST_URI']);
    if(!empty($testa[1])){
        $testd = explode('-', $testa[1]);
            if(is_numeric($testd[0])){
                $username= see_username($testd[0]);
                $result=see_userlist_nb_photo();
                $userok = array();
                while ($row = pwg_db_fetch_assoc($result)) {
                    $userok[] = $row['id'];
                }
                if (in_array($testd[0], $userok)) {
                    $me = 'user-' . $testd[0].'-'.$username;
                    $page['section_title'] = '<a href="' . get_absolute_root_url() . '">' . l10n('Home') . '</a>' . $conf['level_separator'] . '<a href="' . get_absolute_root_url() . 'index.php?/user-">' . l10n('Users').'</a>'. $conf['level_separator'] . '<a href="' . get_absolute_root_url() . 'index.php?/' . $me . '">'.$username . '</a>';
                } else {
                    $me = 'user-';
                    $page['section_title'] = '<a href="' . get_absolute_root_url() . '">' . l10n('Home') . '</a>' . $conf['level_separator'] . '<a href="' . get_absolute_root_url() . 'index.php?/user-">' . l10n('Users').'</a>';
                }
            }else{
                $me = 'user-';
            }
    }else{
        $me = 'user-';
    }
    if(isset($me)){
        if (in_array($me, $tokens))
            include(SPBA_PATH . 'pagespba.php');
    }
  }
  $template->set_filename('SPBA', realpath(SPBA_PATH . 'pagespba.tpl'));
  $template->assign_var_from_handle('CONTENT', 'SPBA'); //2.6
}

/*Schow link in menu*/
if ($conf['see_photos_by_user_show']==1) {
    add_event_handler('blockmanager_apply', 'add_link_SPBA');
}

function add_link_SPBA($menu_ref_arr){
    global $conf, $user;
    $menu = & $menu_ref_arr[0];
    if (($block = $menu->get_block('mbSpecials')) != null) {
        $position = (isset($conf['SPBA_position']) and is_numeric($conf['SPBA_position'])) ? $conf['SPBA_position'] : count($block->data) + 1;
        array_splice
                ($block->data, $position - 1, 0, array
            ('user-' =>
            array
                (
                'URL' => make_index_url(array('section' => 'user-')),
                'TITLE' => l10n('See photos by user'),
                'NAME' => l10n('See photos by user')
            )
                )
        );
    }
}

/*schow users menu*/
if ($conf['see_photos_by_user_show']==2) {
    add_event_handler('blockmanager_register_blocks', 'register_users_menubar_blocks');
    add_event_handler('blockmanager_apply', 'users_apply');
}

function register_users_menubar_blocks($menu_ref_arr) {
    $menu = & $menu_ref_arr[0];
    if ($menu->get_id() != 'menubar')
        return;
    $menu->register_block(new RegisteredBlock('mbUsers', 'Users', ('See photos by user')));
}

function users_apply($menu_ref_arr) {
    global $template, $conf, $user;
    $menu = & $menu_ref_arr[0];

    $userslistemenu1 = see_userlist_nb_photo();

    if (pwg_db_num_rows($userslistemenu1)) {
        while ($userslistemenu = pwg_db_fetch_assoc($userslistemenu1)) {
            $items = array(
                'USERSSPBYLID' => $userslistemenu['id'],
                'USERSSPBYL' => $userslistemenu['username'],
                'USERSSPBYLC' => $userslistemenu['PBU'],
            );

            $template->append('userslistemenu1', $items);
        }
    }
    $linkusersliste = get_root_url() . 'index.php?/user-';
    $template->assign('USERSSPBY', $linkusersliste);

    if (($block = $menu->get_block('mbUsers')) != null) {
        $template->set_template_dir(SPBA_PATH);
        $block->template = 'menubar_users.tpl';
    }
}

/*Add admin menu*/
add_event_handler('get_admin_plugin_menu_links', 'SPBA_admin_menu');

function SPBA_admin_menu($menu) {
  $menu[] = array(
    'NAME' => l10n('Photos by user'),
    'URL' => SPBA_ADMIN,
    );
  return $menu;
}

?>