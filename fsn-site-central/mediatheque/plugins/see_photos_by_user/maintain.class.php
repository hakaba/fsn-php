<?php
// +-----------------------------------------------------------------------+
// | See photos by user plugin for piwigo                                  |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2014 - 2016 ddtddt             http://temmii.com/piwigo/ |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
defined('PHPWG_ROOT_PATH') or die('Hacking attempt!');

class see_photos_by_user_maintain extends PluginMaintain
{
  private $installed = false;

  function __construct($plugin_id)
  {
    parent::__construct($plugin_id);
  }

  function install($plugin_version, &$errors=array())
  {
   
  }

  function activate($plugin_version, &$errors=array())
  {
      global $conf;
    if (!isset($conf['see_photos_by_user_nbphotos'])) {
        conf_update_param('see_photos_by_user_nbphotos', '0',true);
    }
    if (!isset($conf['see_photos_by_user_limit'])) {
        conf_update_param('see_photos_by_user_limit', '1000',true);
    }
    if (!isset($conf['see_photos_by_user_order'])) {
        conf_update_param('see_photos_by_user_order', 'username ASC');
    }
    if (!isset($conf['see_photos_by_user_show'])||$conf['see_photos_by_user_show']>2) {
        conf_update_param('see_photos_by_user_show', '1',true);
    }
    if (!isset($conf['see_photos_by_user_show_user_home'])) {
        conf_update_param('see_photos_by_user_show_user_home', '1',true);
    }
    if (!isset($conf['see_photos_by_user_color'])) {
        conf_update_param('see_photos_by_user_color', '#ffffff',true);
    }
    if (!isset($conf['see_photos_by_user_shape'])) {
        conf_update_param('see_photos_by_user_shape', 'sphere',true);
    }
  
  }

  function update($old_version, $new_version, &$errors=array())
  {
    global $conf;
    if (!isset($conf['see_photos_by_user_nbphotos'])) {
        conf_update_param('see_photos_by_user_nbphotos', '0',true);
    }
    if (!isset($conf['see_photos_by_user_limit'])) {
        conf_update_param('see_photos_by_user_limit', '1000',true);
    }
    if (!isset($conf['see_photos_by_user_order'])) {
        conf_update_param('see_photos_by_user_order', 'username ASC');
    }
    if (!isset($conf['see_photos_by_user_show'])||$conf['see_photos_by_user_show']>2) {
        conf_update_param('see_photos_by_user_show', '1',true);
    }
    if (!isset($conf['see_photos_by_user_show_user_home'])) {
        conf_update_param('see_photos_by_user_show_user_home', '1',true);
    }
    if (!isset($conf['see_photos_by_user_color'])) {
        conf_update_param('see_photos_by_user_color', '#ffffff',true);
    }
    if (!isset($conf['see_photos_by_user_shape'])) {
        conf_update_param('see_photos_by_user_shape', 'sphere',true);
    }
  }
  
  function deactivate()
  {
  }

  function uninstall()
  {
    conf_delete_param('see_photos_by_user_nbphotos');
	conf_delete_param('see_photos_by_user_limit');
	conf_delete_param('see_photos_by_user_order');
	conf_delete_param('see_photos_by_user_show');
	conf_delete_param('see_photos_by_user_show_user_home');
	conf_delete_param('see_photos_by_user_color');
	conf_delete_param('see_photos_by_user_shape');
  }
}
?>
