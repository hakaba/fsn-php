{if isset ($gestionA)}
    <form method="post" >
        {html_options name="metalist" options=$gestionA.OPTIONS selected=$gestionA.SELECTED}<input class="submit" name="submitchoixauteur" type="submit" value="{'submit'|@translate}"/>
    </form>
{/if}

{if isset ($gestionB)}
    <h1>{'User'|@translate} {$gestionB.USERUSBU}</h1>
{/if}

{if isset ($gestionC)}
    {combine_script id='jquery.tagcanvas' require="jquery" load='footer' path=$gestionC.SPBA_PATH|@cat:"js/jquery.tagcanvas.min.js"}
    {combine_script id='jquery.see' load='footer' path=$gestionC.SPBA_PATH|@cat:"js/see.js"}
<div id="myCanvasContainer">
     
 <canvas width="600" height="300" id="myCanvas">
   <ul id="ulseepbu" data-shape="{$USERSSPBYSHAPE}" data-color="{$USERSSPBYCOLOR}">
    {foreach from=$userslistecloud1 item=userslistecloud}
        <li>
            <a href = "{$USERSSPBY}{$userslistecloud.USERSSPBYLID}-{$userslistecloud.USERSSPBYL}">{$userslistecloud.USERSSPBYL} <span class="menuInfoCat seepbu">[{$userslistecloud.USERSSPBYLC}]</span></a><br>
        </li>
     {/foreach}
  </ul>
 </canvas>
</div>
{/if}

{if isset ($gestionD)}
<div id="fullTagCloud">
  {foreach from=$userslistecloud1 item=userslistecloud}
      <span><a class="tagLevel{$userslistecloud.USERSSPBYWEIGHT}" href = "{$USERSSPBY}{$userslistecloud.USERSSPBYLID}-{$userslistecloud.USERSSPBYL}" title="[{$userslistecloud.USERSSPBYLC}]">{$userslistecloud.USERSSPBYL}</a></span>
  {/foreach}
</div>
{/if}