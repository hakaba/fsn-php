<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2012 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Use common setting'] = 'Általános beállítások használata';
$lang['This element is a video added with "Embedded Video"'] = 'Ez az elem egy hozzáadott video "Beágyazott video"';
$lang['Unable to contact host server'] = 'Nem sikerült kapcsolódni a host szerverhez';
$lang['Video successfully added. <a href="%s">View</a>'] = 'Video sikeresen hozzáadva. <a href="%s">Megtekintés</a>';
$lang['Video successfully updated'] = 'Video sikeresen frissítve';
$lang['Add a video'] = 'Video hozzáadása';
$lang['Add film effect'] = 'Film effekt hozzáadása';
$lang['Author name'] = 'Szerző neve';
$lang['Author portrait'] = 'Szerző portréja';
$lang['Autoplay'] = 'Automatikus lejátszás';
$lang['Change'] = 'Változtatás';
$lang['Changing the url will reset video description, name and thumbnail'] = 'Az URL változtatásakor törlődik a video leírása, neve, bélyegképe';
$lang['Color'] = 'Szín';
$lang['Common configuration'] = 'Általános beállítások';
$lang['Logo'] = 'Logo';
$lang['Please fill the video URL'] = 'Kérem adja meg a video URL-jét';
$lang['Supported services'] = 'Támogatott szolgáltatások';
$lang['The thumbnail was updated'] = 'A bélyegkép frissítve';
$lang['Upload a new thumbnail'] = 'Új bélyegkép hozzáadása';
$lang['Video URL'] = 'Video URL';
$lang['Video size'] = 'Video mérete';
$lang['Dailymotion player'] = 'Dailymotion lejátszó';
$lang['Get video description'] = 'Video leírás';
$lang['Get video tags'] = 'Video címkék';
$lang['Vimeo player'] = 'Vimeo lejátszó';
$lang['Video properties'] = 'Videó tulajdonságok';
$lang['Try in safe-mode'] = 'Próbálja csökkentett módban';
$lang['Safe-mode'] = 'Csökkentett-mód';
$lang['In safe-mode, the plugin does\'t try to contact the video host, usefull on some websites where remote connections are blocked.'] = 'A csökkentett-módban a plugin nem próbálja felvenni a kapcsolatot a vidó host-al, hasznos egyes honlapoknál, ahol a távoli kapcsolatok le vannak tiltva';
$lang['Show privacy details'] = 'Részletek megjelenítése';
$lang['No privacy option.'] = 'Nincs titoktartási opció.';
$lang['Videos can be private if you use the private permalink.'] = 'A videók lehetnek magán jellegűek, ha a privát permalink-et használod.';
$lang['Videos can be unlisted and private if the gallery website is within the authorized domains (PRO).'] = 'A videók lehetnek titkosak és magán jellegűek, ha a galéria weboldal benne van a hivatalosan jóváhagyott domainek (PRO) között.';
$lang['Videos can be unlisted but not private.'] = 'A videók lehetnek titkosak, de nem magán jellegűek.';
?>