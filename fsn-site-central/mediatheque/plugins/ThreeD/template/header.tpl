<meta property="og:type" content="website">
<meta property="og:title" content="{$TITLE}">
<meta property="og:image" content="{$IMAGE}">
<meta property="og:url" content="{$URL}">
<meta property="og:description" content="{$DESCRIP}">
