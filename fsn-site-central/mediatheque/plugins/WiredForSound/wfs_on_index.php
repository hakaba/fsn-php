<?php

if (!defined('PHPWG_ROOT_PATH')) die('Hacking attempt!');
load_language('plugin.lang', WFS_PATH);

$wfs_conf = explode(',' , $conf['wired_for_sound']);
if (isset($page['category']))
{
  $cat_id = $page['category']['id'];
}
else
{
  switch ($page['section'])
  {
    case 'categories':
      $cat_id = -1 ;
      break;
    case 'tags':
      $cat_id = -2 ;
      break;
    case 'search':
      $cat_id = -3 ;
      break;
    case 'favorites':
      $cat_id = -4 ;
      break;
    case 'recent_pics':
      $cat_id = -5 ;
      break;
    case 'recent_cats':
      $cat_id = -6 ;
      break;
    case 'most_visited':
      $cat_id = -7 ;
      break;
    case 'best_rated':
      $cat_id = -8 ;
      break;
    case 'list':
      $cat_id = -9 ;
      break;
    case 'most_commented':
      $cat_id = -10 ;
      break;
  }
}

if (empty($cat_id)) return;

$q = 'SELECT sound.id AS id, sound.file AS sound, ic.volume AS volume
FROM ' . WFS_SOUNDS_TABLE . ' AS sound
INNER JOIN ' . WFS_IMG_CAT_TABLE . ' AS ic
ON sound.id = ic.sound_id
WHERE ic.cat_id = ' . $cat_id . ' AND ic.image_id IS NULL;';

$result = pwg_db_fetch_assoc(pwg_query($q));

if (is_admin())
{
  $template->assign('mp3_button', array(
    'URL' => PHPWG_ROOT_PATH . 'admin.php?page=plugin&amp;section=' . WFS_DIR . '%2Fadmin%2Fadd_page_on_index.php&amp;catid=' . $cat_id . '&amp;backurl=' . $_SERVER['REQUEST_URI'],
    'ICON' => WFS_PATH . 'template/add_button.png'));
}

if (!empty($result))
{
  if (file_exists($result['sound']))
  {
    if (!empty($result['volume']))
    {
      $wfs_conf[1] = $result['volume'];
    }
    $template->assign('wfs', array(
      'MP3' => WFS_PATH . 'dewplayer.swf?mp3=' . strtr($result['sound'] , array(' ' => '%20')),
      'VOLUME' => $wfs_conf[1],
      'PLAYER_WIDTH' => $wfs_conf[2] - 2,
      'AUTOSTART' => $wfs_conf[3],
      'AUTOREPLAY' => $wfs_conf[4]));
  }
  else
  {
    pwg_query('DELETE FROM ' . WFS_IMG_CAT_TABLE . ' WHERE sound_id = ' . $result['id'] . ';');
    pwg_query('DELETE FROM ' . WFS_SOUNDS_TABLE . ' WHERE id = ' . $result['id'] . ';');
  }
}

$template->set_filenames(array('wfs' => dirname(__FILE__) . '/template/wfs_on_index.tpl'));
$template->concat('PLUGIN_INDEX_ACTIONS', $template->parse('wfs', true));

?>