<?php

if (!defined('PHPWG_ROOT_PATH')) die('Hacking attempt!');

function plugin_install()
{
  global $prefixeTable, $conf;

  $query = 'SHOW TABLES LIKE "' . $prefixeTable . 'wfs_%"';
  $result = pwg_query($query);
  if (!pwg_db_fetch_assoc($result))
  {
    $q = 'CREATE TABLE ' . $prefixeTable . 'wfs_img_cat_sound(
image_id MEDIUMINT( 8 ) UNSIGNED default NULL ,
cat_id MEDIUMINT( 8 ) default NULL ,
sound_id SMALLINT( 5 ) NOT NULL ,
volume SMALLINT ( 3 ) UNSIGNED default NULL ,
INDEX (image_id) ,
INDEX (cat_id),
INDEX (sound_id))
DEFAULT CHARACTER SET utf8;';
    pwg_query($q);

    $q = 'CREATE TABLE ' . $prefixeTable . 'wfs_sounds (
id SMALLINT( 5 ) NOT NULL ,
file VARCHAR( 255 ) NOT NULL ,
PRIMARY KEY (id))
DEFAULT CHARACTER SET utf8;';
    pwg_query($q);
  }

  if (!isset($conf['wired_for_sound']))
  {
    $q = '
INSERT INTO ' . CONFIG_TABLE . ' (param,value,comment)
	VALUES
	("wired_for_sound" , "plugins/WiredForSound/mp3/,100,44,1,0" , "Parametres du plugin WiredForSound");';

    pwg_query($q);
  }
}

function plugin_uninstall()
{
  global $prefixeTable;

	$q = 'DROP TABLE ' . $prefixeTable . 'wfs_img_cat_sound;';
    pwg_query($q);

	$q = 'DROP TABLE ' . $prefixeTable . 'wfs_sounds;';
    pwg_query($q);

	$q = 'DELETE FROM ' . CONFIG_TABLE . ' WHERE param="wired_for_sound" LIMIT 1;';
    pwg_query($q);
}

?>