<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['wfs_actual_file'] = 'Länkad fil';
$lang['wfs_apply_only_this_cat'] = 'Länka bara med nuvarande katalog';
$lang['wfs_delete'] = 'Länka ej';
$lang['wfs_loop'] = 'Autoloop';
$lang['wfs_medium'] = 'Mellan';
$lang['wfs_no_mp3'] = 'Inget ljud tillgängligt i denna katalogen.';
$lang['wfs_no_path'] = 'Katalogen finns inte.';
$lang['wfs_only_this_cat'] = 'Enbart nuvarande katalog';
$lang['wfs_optional'] = 'tillval';
$lang['wfs_path'] = 'Katalogsökväg';
$lang['wfs_player_width'] = 'Bredd på spelaren';
$lang['wfs_show_default_path'] = 'Visa standard katalogen';
$lang['wfs_volume'] = 'Volym';
$lang['wfs_all_cat'] = 'Alla kategorier';
$lang['wfs_associate_to_cat'] = 'Länka till den nuvarande kategorin';
$lang['wfs_associate_to_pic'] = 'Länk till bilden';
$lang['wfs_autostart'] = 'Autostart';
$lang['wfs_back_cat'] = 'Tillbaka till kategorin';
$lang['wfs_back_pic'] = 'Tillbaka till bilden';
$lang['wfs_change_path'] = 'Byt katalog';
$lang['wfs_conf_saved'] = 'Konfigurationen har sparats.';
$lang['wfs_configuration'] = 'Konfiguration';
$lang['wfs_default_directory'] = 'Standard katalogen';
$lang['wfs_default_volume'] = 'Standard volym';
$lang['wfs_file'] = 'Fil';
$lang['wfs_large'] = 'Stor';
$lang['wfs_litle'] = 'Liten';
?>