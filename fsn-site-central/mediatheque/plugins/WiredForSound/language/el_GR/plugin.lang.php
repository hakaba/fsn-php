<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['wfs_volume'] = 'Ένταση';
$lang['wfs_show_default_path'] = 'Δείτε τον προεπιλεγμένο κατάλογο';
$lang['wfs_player_width'] = 'Πλάτος του Player ';
$lang['wfs_path'] = 'Διαδρομή κατηγορίας';
$lang['wfs_optional'] = 'προαιρετικά';
$lang['wfs_only_this_cat'] = 'Η τρέχουσα κατηγορία μόνο';
$lang['wfs_no_path'] = 'Ο Κατάλογος δεν βρέθηκε.';
$lang['wfs_no_mp3'] = 'Δεν υπάρχει διαθέσιμος ήχος σε αυτόν τον κατάλογο.';
$lang['wfs_medium'] = 'Μεσαίο';
$lang['wfs_loop'] = 'Αυτόματο λουπ';
$lang['wfs_litle'] = 'Μικρό';
$lang['wfs_large'] = 'Μεγάλο';
$lang['wfs_file'] = 'Αρχείο';
$lang['wfs_delete'] = 'Αποσύνδεση';
$lang['wfs_default_volume'] = 'Προεπιλεγμένη κατηγορία';
$lang['wfs_default_directory'] = 'Προεπιλεγμένος κατάλογος';
$lang['wfs_configuration'] = 'Διαμόρφωση';
$lang['wfs_conf_saved'] = 'Η Διαμόρφωση έχει αποθηκευτεί.';
$lang['wfs_change_path'] = 'Αλλάξτε κατηγορία';
$lang['wfs_back_pic'] = 'Πίσω στην εικόνα';
$lang['wfs_back_cat'] = 'Πίσω στην κατηγορία';
$lang['wfs_autostart'] = 'Αυτόματη εκκίνηση';
$lang['wfs_associate_to_pic'] = 'Σύνδεση για την φωτογραφία';
$lang['wfs_associate_to_cat'] = 'Σύνδεση για την τρέχουσα κατηγορία';
$lang['wfs_associate_sound'] = 'Wire for sound - Προσθέστε ήχο';
$lang['wfs_associate_image'] = 'Wire for sound αυτή την εικόνα';
$lang['wfs_associate_category'] = 'Wire for sound για αυτή την κατηγορία';
$lang['wfs_apply_only_this_cat'] = 'Σύνδεση μόνο για την τρέχουσα κατηγορία';
$lang['wfs_all_cat'] = 'Όλες οι κατηγορίες';
$lang['wfs_actual_file'] = 'Τρέχον συνδεδεμένο αρχείο';
?>