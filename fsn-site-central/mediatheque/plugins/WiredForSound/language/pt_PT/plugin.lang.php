<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['wfs_loop'] = 'Auto prender';
$lang['wfs_medium'] = 'Médio';
$lang['wfs_no_mp3'] = 'Este diretório não posui som';
$lang['wfs_no_path'] = 'Diretório não encontrado';
$lang['wfs_only_this_cat'] = 'Apenas a categoria atual';
$lang['wfs_optional'] = 'Opcional';
$lang['wfs_path'] = 'Caminho do diretório';
$lang['wfs_player_width'] = 'Largura de execução';
$lang['wfs_show_default_path'] = 'Mostrar o diretório padrão';
$lang['wfs_volume'] = 'Volume';
$lang['wfs_actual_file'] = 'Arquivo atualmente vinculado(link)';
$lang['wfs_all_cat'] = 'Todas as categorias';
$lang['wfs_apply_only_this_cat'] = 'Vincular(Link)apenas na categoria atual';
$lang['wfs_associate_category'] = 'Linha para sonorizar esta categoria';
$lang['wfs_associate_image'] = 'Linha para sonorizar esta foto';
$lang['wfs_associate_sound'] = 'Linha para sonorizar - Adicionar som';
$lang['wfs_associate_to_cat'] = 'Vincular (Link) a esta categoria';
$lang['wfs_associate_to_pic'] = 'Vincular à foto';
$lang['wfs_autostart'] = 'Auto iniciar';
$lang['wfs_back_cat'] = 'Voltar para a categoria';
$lang['wfs_back_pic'] = 'Voltar para a foto';
$lang['wfs_change_path'] = 'Alterar o diretório';
$lang['wfs_conf_saved'] = 'A configuração foi salva';
$lang['wfs_configuration'] = 'Configuração';
$lang['wfs_default_directory'] = 'Diretório padrão';
$lang['wfs_default_volume'] = 'Volume padrão';
$lang['wfs_delete'] = 'Desvincular';
$lang['wfs_file'] = 'Ficheiro';
$lang['wfs_large'] = 'Grande';
$lang['wfs_litle'] = 'Pequeno';
?>