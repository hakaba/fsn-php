<?php

$lang['wfs_associate_category'] = 'Associer un son à la catégorie';
$lang['wfs_associate_image'] = 'Associer un son à l\'image';

$lang['wfs_associate_sound'] = 'Wired For Sound - Associer un son';
$lang['wfs_actual_file'] = 'Fichier associé actuellement';
$lang['wfs_path'] = 'Dossier';
$lang['wfs_change_path'] = 'Changer de dossier';
$lang['wfs_show_default_path'] = 'Afficher le dossier par defaut';
$lang['wfs_file'] = 'Fichier';
$lang['wfs_volume'] = 'Volume';
$lang['wfs_apply_only_this_cat'] = 'Appliquer à l\'image uniquement dans cette catégorie';
$lang['wfs_associate_to_cat'] = 'Associer le mp3 à la catégorie';
$lang['wfs_associate_to_pic'] = 'Associer le mp3 à l\'image';
$lang['wfs_delete'] = 'Supprimer l\'association existante';
$lang['wfs_back_cat'] = 'Retour à la catégorie';
$lang['wfs_back_pic'] = 'Retour à l\'image';
$lang['wfs_optional'] = 'facultatif';

$lang['wfs_no_mp3'] = 'Aucun mp3 n\'a été trouvé dans ce dossier.';
$lang['wfs_no_path'] = 'Le dossier spécifié n\'existe pas.';
$lang['wfs_only_this_cat'] = 'Uniquement pour cette catégorie';
$lang['wfs_all_cat'] = 'Quelle que soit la catégorie';

$lang['wfs_conf_saved'] = 'Configuration sauvegardée.';

$lang['wfs_configuration'] = 'Configuration';
$lang['wfs_default_directory'] = 'Repertoire par défaut';
$lang['wfs_default_volume'] = 'Volume par défaut';
$lang['wfs_player_width'] = 'Taille du lecteur';
$lang['wfs_litle'] = 'Petit';
$lang['wfs_medium'] = 'Moyen';
$lang['wfs_large'] = 'Grand';
$lang['wfs_autostart'] = 'Autostart';
$lang['wfs_loop'] = 'Lecture continue';

?>