<?php

$lang['wfs_associate_category'] = 'Wire for sound this category';
$lang['wfs_associate_image'] = 'Wire for sound this picture';

$lang['wfs_associate_sound'] = 'Wired For Sound - Add sound';
$lang['wfs_actual_file'] = 'Current linked file';
$lang['wfs_path'] = 'Directory path';
$lang['wfs_change_path'] = 'Change directory';
$lang['wfs_show_default_path'] = 'Show defalt directory';
$lang['wfs_file'] = 'File';
$lang['wfs_volume'] = 'Volume';
$lang['wfs_apply_only_this_cat'] = 'Link only within the current category';
$lang['wfs_associate_to_cat'] = 'Link to the current category';
$lang['wfs_associate_to_pic'] = 'Link to the picture';
$lang['wfs_delete'] = 'Unlink';
$lang['wfs_back_cat'] = 'Back to category';
$lang['wfs_back_pic'] = 'Back to picture';
$lang['wfs_optional'] = 'optional';

$lang['wfs_no_mp3'] = 'No sound available in this directory.';
$lang['wfs_no_path'] = 'Directory not found.';
$lang['wfs_only_this_cat'] = 'Current category only';
$lang['wfs_all_cat'] = 'All categories';

$lang['wfs_conf_saved'] = 'Configuration has been saved.';

$lang['wfs_configuration'] = 'Configuration';
$lang['wfs_default_directory'] = 'Default directory';
$lang['wfs_default_volume'] = 'Default volume';
$lang['wfs_player_width'] = 'Player width';
$lang['wfs_litle'] = 'Little';
$lang['wfs_medium'] = 'Medium';
$lang['wfs_large'] = 'Large';
$lang['wfs_autostart'] = 'Autostart';
$lang['wfs_loop'] = 'Autoloop';

?>