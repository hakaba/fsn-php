<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['wfs_associate_image'] = 'Wire for Sound dieses Bild';
$lang['wfs_loop'] = 'Autoloop';
$lang['wfs_medium'] = 'Medium';
$lang['wfs_no_mp3'] = 'Kein Sound in diesem Verzeichnis verfügbar.';
$lang['wfs_no_path'] = 'Verzeichnis nicht gefunden.';
$lang['wfs_only_this_cat'] = 'Nur aktuelle Kategorie';
$lang['wfs_optional'] = 'optional';
$lang['wfs_path'] = 'Verzeichnispfad';
$lang['wfs_player_width'] = 'Player Breite';
$lang['wfs_show_default_path'] = 'Zeige Standard-Verzeichnis';
$lang['wfs_volume'] = 'Lautstärke';
$lang['wfs_actual_file'] = 'Verknüpfte Datei';
$lang['wfs_all_cat'] = 'Alle Kategorien';
$lang['wfs_apply_only_this_cat'] = 'Nur mit der aktuellen Kategorie verknüpfen';
$lang['wfs_associate_category'] = 'Wire for Sound dies';
$lang['wfs_associate_sound'] = 'Wired For Sound - Sound hinzufügen';
$lang['wfs_associate_to_cat'] = 'Mit aktueller Kategorie verknüpfen';
$lang['wfs_associate_to_pic'] = 'Mit Bild verknüpfen';
$lang['wfs_autostart'] = 'Autostart';
$lang['wfs_back_cat'] = 'Zurück zur Kategorie';
$lang['wfs_back_pic'] = 'Zurück zum Bild';
$lang['wfs_change_path'] = 'Verzeichnis wechseln';
$lang['wfs_conf_saved'] = 'Konfiguration wurde gespeichert';
$lang['wfs_configuration'] = 'Konfiguration';
$lang['wfs_default_directory'] = 'Standard Verzeichnis';
$lang['wfs_default_volume'] = 'Standard Lautstärke';
$lang['wfs_delete'] = 'Verknüpfung entfernen';
$lang['wfs_file'] = 'Datei';
$lang['wfs_large'] = 'Groß';
$lang['wfs_litle'] = 'Klein';
?>