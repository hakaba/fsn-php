<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['wfs_back_cat'] = 'Torna a cap a la categoria';
$lang['wfs_medium'] = 'Mitjana';
$lang['wfs_optional'] = 'opcional';
$lang['wfs_player_width'] = 'Amplada del reproductor';
$lang['wfs_volume'] = 'Volum';
$lang['wfs_all_cat'] = 'Totes les categories';
$lang['wfs_associate_to_cat'] = 'Enllaça a la categoria actual';
$lang['wfs_associate_to_pic'] = 'Enllaça a la imatge';
$lang['wfs_back_pic'] = 'Torna cap a la imatge';
$lang['wfs_change_path'] = 'Canvia el directori';
$lang['wfs_configuration'] = 'Configuració';
$lang['wfs_default_directory'] = 'Directori per defecte';
$lang['wfs_default_volume'] = 'Volum per defecte';
$lang['wfs_file'] = 'Fitxer';
$lang['wfs_large'] = 'Gran';
$lang['wfs_litle'] = 'Petit';