<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['wfs_loop'] = 'Autoloop';
$lang['wfs_medium'] = 'Medio';
$lang['wfs_no_mp3'] = 'Non ci sono suoni in questa cartella';
$lang['wfs_no_path'] = 'Cartella non trovata';
$lang['wfs_only_this_cat'] = 'Solo la categoria corrente';
$lang['wfs_optional'] = 'opzionale';
$lang['wfs_path'] = 'Percorso cartella';
$lang['wfs_player_width'] = 'Larghezza player';
$lang['wfs_show_default_path'] = 'Visualizza cartella predefinita';
$lang['wfs_volume'] = 'Volume';
$lang['wfs_actual_file'] = 'File collegato';
$lang['wfs_all_cat'] = 'Tutte le categorie';
$lang['wfs_apply_only_this_cat'] = 'Collega solo nella categoria corrente';
$lang['wfs_associate_category'] = 'Wire for sound questa categoria';
$lang['wfs_associate_image'] = 'Wire for sound questa foto';
$lang['wfs_associate_sound'] = 'Wired For Sound - aggiungi musica';
$lang['wfs_associate_to_cat'] = 'Link alla categoria';
$lang['wfs_associate_to_pic'] = 'Linka alla foto';
$lang['wfs_autostart'] = 'Avvio automatico';
$lang['wfs_back_cat'] = 'Torna alla categoria';
$lang['wfs_back_pic'] = 'Torna alla foto';
$lang['wfs_change_path'] = 'Cambia cartella';
$lang['wfs_conf_saved'] = 'Configurazione salvata';
$lang['wfs_configuration'] = 'Configurazione';
$lang['wfs_default_directory'] = 'Cartella predefinita';
$lang['wfs_default_volume'] = 'Volume predefinito';
$lang['wfs_delete'] = 'Scollega';
$lang['wfs_file'] = 'File';
$lang['wfs_large'] = 'Grande';
$lang['wfs_litle'] = 'Piccolo';
?>