<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['wfs_large'] = 'Geniş';
$lang['wfs_actual_file'] = 'Geçerli bağlantılı dosya';
$lang['wfs_associate_category'] = 'Bu kategori için ses bağlı';
$lang['wfs_associate_image'] = 'Bu resim için ses bağlı';
$lang['wfs_associate_sound'] = 'Ses Ekle.';
$lang['wfs_delete'] = 'Bağlantıyı kaldır';
$lang['wfs_apply_only_this_cat'] = 'Sadece geçerli kategori içerisinde bağla';
$lang['wfs_autostart'] = 'Otomatik başlangıç';
$lang['wfs_file'] = 'Dosya';
$lang['wfs_litle'] = 'Küçük';
$lang['wfs_loop'] = 'Geri döndür';
$lang['wfs_medium'] = 'Orta';
$lang['wfs_no_mp3'] = 'Bu dizinde mevcut ses yok';
$lang['wfs_no_path'] = 'Dizin bulunamadı.';
$lang['wfs_only_this_cat'] = 'Sadece geçerli kategori';
$lang['wfs_optional'] = 'isteğe bağlı';
$lang['wfs_path'] = 'Dizin yolu';
$lang['wfs_player_width'] = 'Player genişliği';
$lang['wfs_show_default_path'] = 'Varsayılan dizini göster';
$lang['wfs_volume'] = 'Ses';
$lang['wfs_associate_to_cat'] = 'Şimdiki kategoriye bağla';
$lang['wfs_associate_to_pic'] = 'Resme bağla';
$lang['wfs_back_cat'] = 'Kategoriye Dön';
$lang['wfs_back_pic'] = 'Resme Dön';
$lang['wfs_change_path'] = 'Dizini değiştir';
$lang['wfs_conf_saved'] = 'Yapılandırma Kaydedildi';
$lang['wfs_configuration'] = 'Yapılandırma';
$lang['wfs_default_directory'] = 'Varsayılan dizin';
$lang['wfs_default_volume'] = 'Varsayılan ses';
$lang['wfs_all_cat'] = 'Tüm kategoriler';
?>