<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['wfs_no_mp3'] = 'Няма звуков файл в избраната директория';
$lang['wfs_associate_category'] = 'Wired For Sound за тази категория';
$lang['wfs_associate_image'] = 'Wired For Sound за тази снимка';
$lang['wfs_associate_sound'] = 'Wired For Sound - добавете звук';
$lang['wfs_apply_only_this_cat'] = 'Свързване само в текущата директория';
$lang['wfs_actual_file'] = 'Текущ свързан файл';
$lang['wfs_loop'] = 'Autoloop';
$lang['wfs_volume'] = 'Размер';
$lang['wfs_default_volume'] = 'Размер по подразбиране';
$lang['wfs_medium'] = 'Среден';
$lang['wfs_associate_to_cat'] = 'Линк към текущата директория';
$lang['wfs_no_path'] = 'Не намерена директория';
$lang['wfs_only_this_cat'] = 'Текуща категория само';
$lang['wfs_player_width'] = 'Ширина на плейър';
$lang['wfs_path'] = 'Път на директорията';
$lang['wfs_optional'] = 'опция';
$lang['wfs_show_default_path'] = 'Покажи директорията по подразбиране';
$lang['wfs_litle'] = 'Малък';
$lang['wfs_large'] = 'Голям';
$lang['wfs_file'] = 'Файл';
$lang['wfs_delete'] = 'Примахни линк';
$lang['wfs_default_directory'] = 'Директория по подразбиране';
$lang['wfs_configuration'] = 'Конфигурация';
$lang['wfs_conf_saved'] = 'Кофигурацията беше записана.';
$lang['wfs_change_path'] = 'Смяна на директория';
$lang['wfs_back_cat'] = 'Обратно към категорията';
$lang['wfs_back_pic'] = 'Обратно към снимката';
$lang['wfs_autostart'] = 'Автоматично зареждане';
$lang['wfs_associate_to_pic'] = 'Линк към снимката';
$lang['wfs_all_cat'] = 'Всички категории';
?>