<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['wfs_optional'] = 'Opcional';
$lang['wfs_no_path'] = 'Diretório não encontrado';
$lang['wfs_large'] = 'Largo';
$lang['wfs_litle'] = 'Pequeno';
$lang['wfs_file'] = 'Arquivo';
$lang['wfs_all_cat'] = 'Todas as categorias';
$lang['wfs_associate_to_pic'] = 'Lincar com a imagem';
$lang['wfs_back_cat'] = 'Voltar a categoria';
$lang['wfs_back_pic'] = 'Voltar a imagem';
$lang['wfs_change_path'] = 'Mudar o diretório';
$lang['wfs_conf_saved'] = 'Configuração foi salva.';
$lang['wfs_configuration'] = 'Configuração';
$lang['wfs_default_directory'] = 'Diretório inicial';
$lang['wfs_default_volume'] = 'volume inicial';
$lang['wfs_apply_only_this_cat'] = 'Lincar somente com a categoria atual';
$lang['wfs_actual_file'] = 'Arquivo vinculado atual';
$lang['wfs_associate_category'] = 'Ligar para som desta categoria';
$lang['wfs_associate_image'] = 'Ligar para som esta imagem';
$lang['wfs_associate_sound'] = 'Ligada Para Som - Adicionar som';
$lang['wfs_associate_to_cat'] = 'Link para a categoria atual';
$lang['wfs_autostart'] = 'Autoiniciar';
$lang['wfs_delete'] = 'Desligar';
$lang['wfs_loop'] = 'Autoretorno';
$lang['wfs_medium'] = 'Médio';
$lang['wfs_no_mp3'] = 'Nenhum som disponível neste diretório';
$lang['wfs_only_this_cat'] = 'Somente a categoria corrente';
$lang['wfs_path'] = 'Caminho do diretório';
$lang['wfs_player_width'] = 'Tocador';
$lang['wfs_show_default_path'] = 'Mostrar diretório padrão';
$lang['wfs_volume'] = 'Volume';
?>