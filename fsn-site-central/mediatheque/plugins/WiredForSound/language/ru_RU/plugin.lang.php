<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['wfs_associate_category'] = 'Связать со звуком эту категорию';
$lang['wfs_associate_image'] = 'Связать со звуком это изображение';
$lang['wfs_associate_sound'] = 'Связь со звуком - Добавить звук';
$lang['wfs_actual_file'] = 'Текущий прилинкованный файл';
$lang['wfs_all_cat'] = 'Все категории';
$lang['wfs_apply_only_this_cat'] = 'Ссылка только в пределах текущей категории';
$lang['wfs_associate_to_cat'] = 'Ссылка на текущую категорию';
$lang['wfs_associate_to_pic'] = 'Ссылка на изображение';
$lang['wfs_autostart'] = 'Автоматический запуск';
$lang['wfs_back_cat'] = 'Вернуться к категории';
$lang['wfs_back_pic'] = 'Вернуться к изображению';
$lang['wfs_change_path'] = 'Изменить директорию';
$lang['wfs_conf_saved'] = 'Настройки сохранены';
$lang['wfs_configuration'] = 'Настройки';
$lang['wfs_default_directory'] = 'Директория по умолчанию';
$lang['wfs_default_volume'] = 'Громкость по умолчанию';
$lang['wfs_delete'] = 'Отлинковать';
$lang['wfs_file'] = 'Файл';
$lang['wfs_large'] = 'Высокий';
$lang['wfs_litle'] = 'Низкий';
$lang['wfs_loop'] = 'Воспроизводить безостановочно';
$lang['wfs_medium'] = 'Средний';
$lang['wfs_no_mp3'] = 'В этой директории звуковых файлов не найдено';
$lang['wfs_no_path'] = 'Директория не найдена.';
$lang['wfs_only_this_cat'] = 'Только текущая категория';
$lang['wfs_optional'] = 'опционально';
$lang['wfs_path'] = 'Путь директории';
$lang['wfs_player_width'] = 'Ширина плеера';
$lang['wfs_show_default_path'] = 'Отображать директорию по умолчанию';
$lang['wfs_volume'] = 'Громкость';
?>