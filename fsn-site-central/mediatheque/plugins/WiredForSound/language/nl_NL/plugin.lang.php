<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['wfs_path'] = 'Pad naar map';
$lang['wfs_player_width'] = 'Speler breedte';
$lang['wfs_volume'] = 'Volume';
$lang['wfs_associate_to_cat'] = 'Link naar de huidige categorie';
$lang['wfs_associate_to_pic'] = 'Link naar de foto';
$lang['wfs_autostart'] = 'Begin automatisch';
$lang['wfs_default_volume'] = 'Standaard volume';
$lang['wfs_delete'] = 'Ontkoppelen';
$lang['wfs_loop'] = 'Herhalen';
$lang['wfs_medium'] = 'Medium';
$lang['wfs_no_mp3'] = 'Geen geluid beschikbaar in deze map.';
$lang['wfs_no_path'] = 'Map niet gevonden.';
$lang['wfs_only_this_cat'] = 'Alleen huidige categorie';
$lang['wfs_optional'] = 'Optioneel';
$lang['wfs_show_default_path'] = 'Toon standaard directory';
$lang['wfs_litle'] = 'Klein';
$lang['wfs_large'] = 'Groot';
$lang['wfs_file'] = 'Bestand';
$lang['wfs_change_path'] = 'Ga naar de map';
$lang['wfs_default_directory'] = 'Standaard directory';
$lang['wfs_conf_saved'] = 'Instellingen zijn bewaard';
$lang['wfs_configuration'] = 'Instellingen';
$lang['wfs_back_pic'] = 'Terug naar de foto';
$lang['wfs_back_cat'] = 'Terug naar categorie';
$lang['wfs_associate_sound'] = 'Wired-for-sound voeg het geluid toe';
$lang['wfs_associate_image'] = 'Wire-for-sound deze foto';
$lang['wfs_associate_category'] = 'Wire-for-sound deze categorie';
$lang['wfs_apply_only_this_cat'] = 'Alleen koppelen binnen de huidige categorie';
$lang['wfs_all_cat'] = 'Alle categorieën';
$lang['wfs_actual_file'] = 'Huidige gekoppeld bestand';
?>