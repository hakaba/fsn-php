<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['wfs_volume'] = 'Volumen';
$lang['wfs_show_default_path'] = 'Mostrar la carpeta por defecto';
$lang['wfs_player_width'] = 'Tamaño del reproductor';
$lang['wfs_path'] = 'Carpeta';
$lang['wfs_optional'] = 'Opcional';
$lang['wfs_only_this_cat'] = 'Sólo en esta categoría';
$lang['wfs_no_path'] = 'La carpeta especificada no existe';
$lang['wfs_no_mp3'] = 'Ningún MP3 ha sido encontrado en esta carpeta';
$lang['wfs_medium'] = 'Promedio';
$lang['wfs_autostart'] = 'Arranque automático';
$lang['wfs_loop'] = 'Reproducción continua';
$lang['wfs_litle'] = 'Pequeño';
$lang['wfs_large'] = 'Grande';
$lang['wfs_file'] = 'Archivo';
$lang['wfs_delete'] = 'Eliminar la asociación existente';
$lang['wfs_default_volume'] = 'Volumen por defecto';
$lang['wfs_default_directory'] = 'Directorio por defecto';
$lang['wfs_configuration'] = 'Configuración';
$lang['wfs_conf_saved'] = 'Configuración guardada';
$lang['wfs_change_path'] = 'Cambiar de carpeta';
$lang['wfs_back_pic'] = 'Volver a la imagen';
$lang['wfs_back_cat'] = 'Volver a la categoría';
$lang['wfs_associate_to_pic'] = 'Asociar el MP3 a la imagen ';
$lang['wfs_associate_to_cat'] = 'Asociar el MP3 a la categoría ';
$lang['wfs_associate_sound'] = 'Asociar un sonido';
$lang['wfs_associate_image'] = 'Asociar un sonido a la imagen';
$lang['wfs_apply_only_this_cat'] = 'Aplicar a la imagen sólo en esta categoría';
$lang['wfs_associate_category'] = 'Asociar un sonido a la categoría';
$lang['wfs_actual_file'] = 'Archivo actualmente asociado';
$lang['wfs_all_cat'] = 'Cualquier categoría ';
?>