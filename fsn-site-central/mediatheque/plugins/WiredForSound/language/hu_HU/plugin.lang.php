<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['wfs_apply_only_this_cat'] = 'Hozzárendelés csak a jelenlegi albumhoz';
$lang['wfs_loop'] = 'Automatikus ismétlés';
$lang['wfs_only_this_cat'] = 'Csak az aktuális album';
$lang['wfs_associate_category'] = 'Csatolt hang az albumhoz';
$lang['wfs_associate_image'] = 'Csatolt hang a képhez';
$lang['wfs_associate_sound'] = 'Csatolt hang - Hangfájl hozzáadása';
$lang['wfs_delete'] = 'Hivatkozás törlése';
$lang['wfs_actual_file'] = 'Jelenleg csatolt fájl';
$lang['wfs_all_cat'] = 'Összes album';
$lang['wfs_associate_to_cat'] = 'Hivatkozás az aktuális albumhoz';
$lang['wfs_associate_to_pic'] = 'Hivatkozás a képhez';
$lang['wfs_back_cat'] = 'Vissza az albumhoz';
$lang['wfs_change_path'] = 'Könyvtár kiválasztása';
$lang['wfs_optional'] = 'választható';
$lang['wfs_back_pic'] = 'Vissza a képhez';
$lang['wfs_path'] = 'Könyvtár útvonal';
$lang['wfs_show_default_path'] = 'Alapértelmezett könyvtár megjelenítése';
$lang['wfs_volume'] = 'Hangerő';
$lang['wfs_medium'] = 'Közepes';
$lang['wfs_no_mp3'] = 'Ebben a könyvtárban nem található hangfájl.';
$lang['wfs_no_path'] = 'A könyvtár nem található.';
$lang['wfs_player_width'] = 'Lejátszó szélessége';
$lang['wfs_autostart'] = 'Automatikus indulás';
$lang['wfs_conf_saved'] = 'A beállítások mentése sikeres.';
$lang['wfs_configuration'] = 'Beállítások';
$lang['wfs_default_directory'] = 'Alapértelmezett könyvtár';
$lang['wfs_default_volume'] = 'Alapértelmezett hangerő';
$lang['wfs_file'] = 'Fájl';
$lang['wfs_large'] = 'Nagy';
$lang['wfs_litle'] = 'Kicsi';
?>