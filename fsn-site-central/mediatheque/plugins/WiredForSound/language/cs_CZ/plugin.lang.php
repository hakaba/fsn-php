<?php

$lang['wfs_associate_category'] = 'Wire pro ozvučení této kategorie';
$lang['wfs_associate_image'] = 'Wire pro ozvučení této fotky';

$lang['wfs_associate_sound'] = 'Wired For Sound - přidat zvuk';
$lang['wfs_actual_file'] = 'Aktuálně připojený soubor';
$lang['wfs_path'] = 'Cesta ke složce';
$lang['wfs_change_path'] = 'Změnit složku';
$lang['wfs_show_default_path'] = 'Zobraz výchozí složku';
$lang['wfs_file'] = 'Soubor';
$lang['wfs_volume'] = 'Hlasitost';
$lang['wfs_apply_only_this_cat'] = 'Link jen pro tuto kategorii';
$lang['wfs_associate_to_cat'] = 'Link na tuto kategorii';
$lang['wfs_associate_to_pic'] = 'Link na foto';
$lang['wfs_delete'] = 'Unlink';
$lang['wfs_back_cat'] = 'Zpět na kategorii';
$lang['wfs_back_pic'] = 'Zpět na fotografii';
$lang['wfs_optional'] = 'volitelné';

$lang['wfs_no_mp3'] = 'Tato kategorie neobsahuje žádný zvuk.';
$lang['wfs_no_path'] = 'Složka nenalezena.';
$lang['wfs_only_this_cat'] = 'Pouze tato kategorie';
$lang['wfs_all_cat'] = 'Všechny kategorie';

$lang['wfs_conf_saved'] = 'Konfigurace byla uložena.';

$lang['wfs_configuration'] = 'Nastavení';
$lang['wfs_default_directory'] = 'Výchozí složka';
$lang['wfs_default_volume'] = 'Výchozí hlasitost';
$lang['wfs_player_width'] = 'Šířka přehrávače';
$lang['wfs_litle'] = 'Krátká';
$lang['wfs_medium'] = 'Střední';
$lang['wfs_large'] = 'Velká';
$lang['wfs_autostart'] = 'Autostart';
$lang['wfs_loop'] = 'Autoloop';

?>
