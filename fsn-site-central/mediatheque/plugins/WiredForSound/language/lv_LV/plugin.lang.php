<?php

$lang['wfs_associate_category'] = 'Piesaistīt skaņu šai kategorijai';
$lang['wfs_associate_image'] = 'Piesaistīt skaņu šim attēlam';

$lang['wfs_associate_sound'] = 'Skaņa piemeklēta - Pievienot skaņu';
$lang['wfs_actual_file'] = 'Tekošais sasaistītasis fails';
$lang['wfs_path'] = 'Ceļš uz mapi';
$lang['wfs_change_path'] = 'Mainīt mapi';
$lang['wfs_show_default_path'] = 'Parādīt mapi pēc noklusējuma';
$lang['wfs_file'] = 'Fails';
$lang['wfs_volume'] = 'Sakļums';
$lang['wfs_apply_only_this_cat'] = 'Sasaistīt tikai tekošās kategorijas ietvarost';
$lang['wfs_associate_to_cat'] = 'Saite uz tekošo kategoriju';
$lang['wfs_associate_to_pic'] = 'Saite uz attēlu';
$lang['wfs_delete'] = 'Atsaistīt';
$lang['wfs_back_cat'] = 'Atpakaļ uz kategoriju';
$lang['wfs_back_pic'] = 'Atpakaļ uz attēlu';
$lang['wfs_optional'] = 'izvēles';

$lang['wfs_no_mp3'] = 'Šajā mapē nav izmantojamu skaņu.';
$lang['wfs_no_path'] = 'Mape nav atraste.';
$lang['wfs_only_this_cat'] = 'Tikai tekošā kategorija';
$lang['wfs_all_cat'] = 'Visas kategorijas';

$lang['wfs_conf_saved'] = 'Konfigurācija tika saglabāta.';

$lang['wfs_configuration'] = 'Konfigurācija';
$lang['wfs_default_directory'] = 'Mape pēc noklusēšanas';
$lang['wfs_default_volume'] = 'Skaļums pēc noklusēšanas';
$lang['wfs_player_width'] = 'Atskaņotāja platums';
$lang['wfs_litle'] = 'Mazs';
$lang['wfs_medium'] = 'Vidējs';
$lang['wfs_large'] = 'Liels';
$lang['wfs_autostart'] = 'Autostarts';
$lang['wfs_loop'] = 'Autoatkārtojums';

?>