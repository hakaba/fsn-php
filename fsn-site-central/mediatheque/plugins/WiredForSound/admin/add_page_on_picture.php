<?php

if (!defined('PHPWG_ROOT_PATH')) die('Hacking attempt!');
load_language('plugin.lang', WFS_PATH);

$wfs_conf = explode(',' , $conf['wired_for_sound']);
$mp3_path = $wfs_conf[0];

$cat_id = $_GET['catid'];
$img_id = $_GET['imgid'];
$back_url = $_GET['backurl'];
$volume = '';

function get_wfs_from_table()
{
  global $img_id, $cat_id;
  if (empty($cat_id))
  {
    $clause = 'cat_id IS NULL';
  }
	else
  {
    $clause = '(cat_id = ' . $cat_id . ' OR cat_id IS NULL)';
  }
	$q = 'SELECT sound.id AS id, sound.file AS sound, ic.volume AS volume, ic.cat_id AS cat_id
FROM ' . WFS_SOUNDS_TABLE . ' AS sound
INNER JOIN ' . WFS_IMG_CAT_TABLE . ' AS ic
ON sound.id = ic.sound_id
WHERE ic.image_id = ' . $img_id . '  AND ' . $clause . '
ORDER BY ic.cat_id DESC;';
    return pwg_db_fetch_assoc(pwg_query($q));
}

function delete_wfs_from_table($param)
{
  global $img_id, $cat_id;
  if (!empty($param))
  {
    pwg_query('DELETE FROM ' . WFS_IMG_CAT_TABLE . ' WHERE image_id = ' . $img_id . ' AND cat_id = ' . $cat_id . ';');
  }
  else
  {
    pwg_query('DELETE FROM ' . WFS_IMG_CAT_TABLE . ' WHERE image_id = ' . $img_id . ' AND cat_id IS NULL;');
  }
}

// Changement de repertoire
if (isset($_POST['change_path']))
{
  $mp3_path = $_POST['mp3_path'];
  if (strrpos($mp3_path , '/') != (strlen($mp3_path) - 1))
  {
    $mp3_path .= '/';
  }
}

// Suppression de l'association
if (isset($_POST['delete']))
{
  $result = get_wfs_from_table();
  delete_wfs_from_table($result['cat_id']);
}

// Enregistrement de l'association
if (isset($_POST['submit']) and !empty($_POST['mp3_select']))
{
  // Suppression des eventuelles donn�es existantes
  if (!isset($_POST['assign_all']))
  {
    $_POST['assign_all'] = '';
  }
  delete_wfs_from_table($_POST['assign_all']);

  $cat_write = $cat_id;
  if (empty($_POST['assign_all'])) $cat_write = 'NULL';
  if (empty($_POST['volume'])) $_POST['volume'] = 'NULL';

  // Ecriture dans la table
  $q = 'SELECT id FROM ' . WFS_SOUNDS_TABLE . ' WHERE file = "' . $_POST['mp3_select'] . '";';
  $result = pwg_db_fetch_assoc(pwg_query($q));
  if (empty($result))
  {
    $q = 'SELECT IF(MAX(id)+1 IS NULL, 1, MAX(id)+1) AS next_element_id  FROM ' . WFS_SOUNDS_TABLE . ' ;';
    list($next_element_id) = pwg_db_fetch_row(pwg_query($q));
    $q = 'INSERT INTO ' . WFS_SOUNDS_TABLE . ' ( id , file ) VALUES ( ' . $next_element_id . ' , "' . $_POST['mp3_select'] . '");';
    pwg_query($q);
    $result['id'] = $next_element_id;
  }
  $q = 'INSERT INTO ' . WFS_IMG_CAT_TABLE . ' ( image_id , cat_id , sound_id , volume )
VALUES (' . $img_id . ' , ' . $cat_write . ' , ' . $result['id'] . ' , ' . $_POST['volume'] . ');';
  pwg_query($q);
}

// R�cup�ration des donn�es de la table
$result = get_wfs_from_table();

// Bouton de suppression
if (!empty($result))
{
  $template->assign('delete', true);
}

// Dossier mp3
if (isset($result['sound']) and !isset($_POST['change_path']) and !isset($wfs_param[2]))
{
  $mp3_path = substr($result['sound'] , 0 , strrpos($result['sound'] , '/') + 1);
}

// Volume
if (isset($result['volume']))
{
  $volume = $result['volume'];
}

// Liste des mp3 du dossier sp�cifi�
if (is_dir('./' . $mp3_path) and $contents = opendir($mp3_path))
{
  $options[] = '----------------';
  $selected = 0;
  while (($node = readdir($contents)) !== false)
  {
    if (is_file('./' . $mp3_path . $node))
    {
      $extension = strtolower(get_extension($node));
      if ($extension == 'mp3')
      {
        $value = $mp3_path . $node;
        if (isset($result['sound']) and $result['sound'] == $value)
        {
          $selected = $value;
        }
        $options[$value] = $node;
      }
    }
  }
  closedir($contents);
  // Erreur si pas de mp3
  if (count($options) == 1)
  {
    array_push($page['errors'], l10n('wfs_no_mp3'));
  }
  else
  {
    $template->assign('mp3_select', array(
      'OPTIONS' => $options,
      'SELECTED' => $selected));
  }
}
else
{
  array_push($page['errors'], l10n('wfs_no_path'));
}

// Affichage du fichier associ�
if (isset($result['sound']))
{
	if (isset($result['cat_id']))
  {
    $assign_all = l10n('wfs_only_this_cat');
  }
	else
  {
    $assign_all = l10n('wfs_all_cat');
  }
  $template->assign(array(
    'ACTUAL_FILE' => $result['sound'],
    'ASSIGN_ALL' => $assign_all));
}

$template->assign(array(
  'MP3_PATH' => $mp3_path,
  'BACK' => $back_url,
  'VOLUME' => $volume));

$template->set_filename('plugin_admin_content', dirname(__FILE__) . '/../template/add_page_on_picture.tpl');
$template->assign_var_from_handle('ADMIN_CONTENT', 'plugin_admin_content');

?>