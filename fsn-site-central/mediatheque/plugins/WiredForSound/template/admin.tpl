<div class="titrePage">
	<h2>Wired For Sound</h2>
</div>

<form method="post" action="" class="properties"  ENCTYPE="multipart/form-data"> 
<fieldset>
	<legend>{'wfs_configuration'|@translate}</legend>
	<table>
		<tr>
			<td align="right"><br>{'wfs_default_directory'|@translate} &nbsp;&nbsp;</td>
			<td><br>
			<input type="text" size="80" maxlength="255" value="{$DEFAULT_PATH}" name="default_path"/></td>
		</tr>
		<tr>
			<td align="right"><br>{'wfs_default_volume'|@translate} &nbsp;&nbsp;</td>
			<td><br>
			<input type="text" size="3" maxlength="3" value="{$DEFAULT_VOLUME}" name="default_volume"/> %</td>
		</tr>
		<tr>
			<td align="right"><br>{'wfs_player_width'|@translate} &nbsp;&nbsp;</td>
			<td><br>
			<input type="radio" value="44" name="width_player" {$SMALL_PLAYER} />{'wfs_litle'|@translate}
			<input type="radio" value="64" name="width_player" {$MEDIUM_PLAYER} />{'wfs_medium'|@translate}
			<input type="radio" value="202" name="width_player" {$BIG_PLAYER} />{'wfs_large'|@translate}
			</td>
		</tr>
		<tr>
			<td align="right"><br>{'wfs_autostart'|@translate} &nbsp;&nbsp;</td>
			<td><br>
			<input type="radio" value="1" name="autostart" {$AUTOSTART_ON} />On
			<input type="radio" value="0" name="autostart" {$AUTOSTART_OFF} />Off
			</td>
		</tr>
		<tr>
			<td align="right"><br>{'wfs_loop'|@translate} &nbsp;&nbsp;</td>
			<td><br>
			<input type="radio" value="1" name="autoreplay" {$AUTOREPLAY_ON} />On
			<input type="radio" value="0" name="autoreplay" {$AUTOREPLAY_OFF} />Off
			</td>
		</tr>
	</table>
<br>
</fieldset>
<div style="text-align: center;">
  <input class="submit" type="submit" value="{'Submit'|@translate}" name="submit" />
</div>

</form>
