<div class="titrePage">
	<h2>{'wfs_associate_sound'|@translate}</h2>
</div>
<form method="post" action="" class="properties"  ENCTYPE="multipart/form-data"> 
	<table>
		{if isset($ACTUAL_FILE)}
		<tr>
		<td colspan="3" align="center">{'wfs_actual_file'|@translate}: <b>{$ACTUAL_FILE}</b><br><br><br></td>
		</tr>
		{/if}
		<tr>
			<td>{'wfs_path'|@translate} &nbsp;&nbsp;</td>
			<td><input type="text" size="40" maxlength="255" value="{$MP3_PATH}" name="mp3_path"/></td>
			<td><input class="submit" type="submit" value="{'wfs_change_path'|@translate}" name="change_path" /></td>
		</tr>
		
    {if !empty($mp3_select)}
		<tr>
			<td><br>{'wfs_file'|@translate} &nbsp;&nbsp;</td>
			<td><br>
      {html_options name="mp3_select" options=$mp3_select.OPTIONS selected=$mp3_select.SELECTED}
			</td>
		</tr>
		<tr>
			<td><br>{'wfs_volume'|@translate} &nbsp;&nbsp;</td>
			<td><br>
			<input type="text" size="3" maxlength="3" value="{$VOLUME}" name="volume"/> %&nbsp;&nbsp; ({'wfs_optional'|@translate})</td>
		</tr>

		<tr><td colspan="3" align="center"><br><br>
		<input class="submit" type="submit" value="{'wfs_associate_to_cat'|@translate}" name="submit" />
    </td></tr>
    {/if}

		{if isset($delete)}
    <tr><td colspan="3" align="center">
		<br><br><input class="submit" type="submit" value="{'wfs_delete'|@translate}" name="delete" />
    </td></tr>
		{/if}
		
	</table>

</form>
<p><a href="..{$BACK}">{'wfs_back_cat'|@translate}</a></p>
