<?php

if (!defined('PHPWG_ROOT_PATH')) die('Hacking attempt!');
load_language('plugin.lang', WFS_PATH);

$wfs_conf = explode(',' , $conf['wired_for_sound']);
$img_id = $page['image_id'];
if (isset($page['category']['id']))
{
  $cat_id = $page['category']['id'];
  $clause = '(cat_id = ' . $cat_id . ' OR cat_id IS NULL)';
}
else
{
  $cat_id = '';
  $clause = 'cat_id IS NULL';
}

$q = 'SELECT sound.id AS id, sound.file AS sound, ic.volume AS volume
FROM ' . WFS_SOUNDS_TABLE . ' AS sound
INNER JOIN ' . WFS_IMG_CAT_TABLE . ' AS ic
ON sound.id = ic.sound_id
WHERE ic.image_id = ' . $img_id . '  AND ' . $clause . '
ORDER BY ic.cat_id DESC;';

$result = pwg_db_fetch_assoc(pwg_query($q));

if (is_admin())
{
  $template->assign('mp3_button', array(
    'URL' => PHPWG_ROOT_PATH . 'admin.php?page=plugin&amp;section=' . WFS_DIR . '%2Fadmin%2Fadd_page_on_picture.php&amp;catid=' . $cat_id . '&amp;imgid=' . $img_id . '&amp;backurl=' . $_SERVER['REQUEST_URI'],
    'ICON' => WFS_PATH . 'template/add_button.png'));
}

if (isset($result['sound']))
{
  if (file_exists($result['sound']))
  {
		if (!empty($result['volume']))
    {
      $wfs_conf[1] = $result['volume'];
    }
    $template->assign('wfs', array(
      'MP3' => WFS_PATH . 'dewplayer.swf?son=' . strtr($result['sound'] , array(' ' => '%20')),
      'VOLUME' => $wfs_conf[1],
      'PLAYER_WIDTH' => $wfs_conf[2] - 2, // 2px de moins pour la nouvelle version de dewplayer
      'AUTOSTART' => $wfs_conf[3],
      'AUTOREPLAY' => $wfs_conf[4]));
  }
  else
  {
  	pwg_query('DELETE FROM ' . WFS_IMG_CAT_TABLE . ' WHERE sound_id = ' . $result['id'] . ';');
    pwg_query('DELETE FROM ' . WFS_SOUNDS_TABLE . ' WHERE id = ' . $result['id'] . ';');
  }
}

$template->set_filenames(array('wfs' => dirname(__FILE__) . '/template/wfs_on_picture.tpl'));
$template->concat('PLUGIN_PICTURE_ACTIONS', $template->parse('wfs', true));

?>