<?php
/*
Plugin Name: Upload 1 menu
Version: 2.8.a
Description: Add Upload Photos link as menu level 1
Plugin URI: http://piwigo.org/ext/extension_view.php?eid=577
Author: ddtddt
Author URI: http://temmii.com/piwigo/
*/

// +-----------------------------------------------------------------------+
// | Upload 1 menu plugin for piwigo                                       |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2011-2016 ddtddt               http://temmii.com/piwigo/ |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+

if (!defined('PHPWG_ROOT_PATH')) die('Hacking attempt!');

define('U1M_DIR' , basename(dirname(__FILE__)));
define('U1M_PATH' , PHPWG_PLUGINS_PATH . U1M_DIR . '/');

$desac = pwg_db_fetch_assoc(pwg_query("SELECT state FROM " . PLUGINS_TABLE . " WHERE id = 'community';"));

if($desac['state'] != 'active') 
	{
if (script_basename() == 'admin')   
{
  include_once(dirname(__FILE__).'/initadmin.php');
}
	}
else
	{

add_event_handler('blockmanager_register_blocks', 'register_u1m_menubar_blocks');
add_event_handler('blockmanager_apply', 'u1m_apply');

function register_u1m_menubar_blocks( $menu_ref_arr )
{
  $menu = & $menu_ref_arr[0];
  if ($menu->get_id() != 'menubar')
    return;
  $menu->register_block( new RegisteredBlock( 'mbUpload', 'Upload', 'U1M'));
}

function u1m_apply($menu_ref_arr)
{
  global $template;

 $menu = & $menu_ref_arr[0];
 
   global $conf, $user;

  $user_permissions = community_get_user_permissions($user['id']);

  if (count($user_permissions['upload_categories']) == 0 and !$user_permissions ['create_whole_gallery'])
  {
    return;
  }
 
 
 load_language('plugin.lang', COMMUNITY_PATH);
 load_language('lang', PHPWG_ROOT_PATH.'local/', array('no_fallback'=>true, 'local'=>true) );
     //Sending data to the template
	    $template->assign	(
		array	(
        'U1MTITLE'     => l10n('Upload your own photos'),
        'U1MNAME'      => l10n('Upload Photos'),
        'U1MURL' => get_root_url().'index.php?/add_photos',
				)			);

   
    if (($block = $menu->get_block( 'mbUpload' )) != null) {
	$template->set_template_dir(U1M_PATH.'template/');
	$block->template = 'menubar_upload.tpl';
    }
}
	}

?>