<?php
// +-----------------------------------------------------------------------+
// | Upload 1 menu plugin for piwigo                                       |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2011-2016 ddtddt               http://temmii.com/piwigo/ |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
add_event_handler('loc_begin_admin_page', 'U1M_admin_menu' );
function U1M_admin_menu(){
  global $page,$conf;
  load_language('plugin.lang', U1M_PATH);
  $page['errors'][] = l10n('Plugin Community must be installe and activate for use Upload 1 menu');
}

?>