<?php
/*
Plugin Name: File Uploader
Version: 2.7.a
Description: Upload files to the galleries folder and synchronise them with database
Plugin URI: http://piwigo.org/ext/extension_view.php?eid=661
Author: Julien1311
*/

//Check whether we are indeed included by Piwigo.
if (!defined('PHPWG_ROOT_PATH')) die('Hacking attempt!');

if (mobile_theme())
	return;

global $conf;

define('FILE_UPLOADER_DIR' , basename(dirname(__FILE__)));
define('FILE_UPLOADER_PATH' , PHPWG_PLUGINS_PATH.FILE_UPLOADER_DIR.'/');
define('FILE_UPLOADER_ADMIN',   get_root_url() . 'admin.php?page=plugin-'.FILE_UPLOADER_DIR);
define('FILE_UPLOADER_PDF2TAB_PATH' , FILE_UPLOADER_PATH.'/plugin_pdf2tab/');
define('FILE_UPLOADER_PDF2TAB_ABSOLUTE_PATH' , dirname(__FILE__).'/plugin_pdf2tab/');

/* +-----------------------------------------------------------------------+
 * | Plugin admin                                                          |
 * +-----------------------------------------------------------------------+ */

// Add an entry to the plugins menu
add_event_handler('get_admin_plugin_menu_links', 'file_uploader_admin_menu');

function file_uploader_admin_menu($menu) {
	array_push(
		$menu, array(
			'NAME'  => 'File Uploader',
			'URL'   => FILE_UPLOADER_ADMIN,
		)
	);      
	return $menu;
}

/* +-----------------------------------------------------------------------+
 * | Plugin code                                                           |
 * +-----------------------------------------------------------------------+ */
$conf_file_uploader = unserialize($conf['file_uploader']);
if (isset($conf_file_uploader['new_tab']) && $conf_file_uploader['new_tab'] == 1) {
	load_language('plugin.lang', FILE_UPLOADER_PATH);
	include_once(FILE_UPLOADER_PDF2TAB_ABSOLUTE_PATH.'include/thumbnails.inc.php');
	include_once(FILE_UPLOADER_PDF2TAB_ABSOLUTE_PATH.'include/picture.inc.php');
}

	
/* +-----------------------------------------------------------------------+
 * | CSS Style                                                             |
 * +-----------------------------------------------------------------------+ */

add_event_handler('loc_end_page_header', 'file_uploader_css');

function file_uploader_css() {
	global $template;

	if (defined('IN_ADMIN') and IN_ADMIN) {
		$template->append('head_elements', '<link rel="stylesheet" type="text/css" href="'.FILE_UPLOADER_PATH.'admin/admin.css">');
		$template->append('head_elements', '<link rel="stylesheet" type="text/css" href="'.FILE_UPLOADER_PDF2TAB_PATH.'css/admin.css">');
	}
}
?>