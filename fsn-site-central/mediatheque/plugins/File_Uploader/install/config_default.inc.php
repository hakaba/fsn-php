<?php
$file_uploader_galleries_folder = 'file_uploader';
$file_uploader_galleries_folder_name = 'File Uploader';
$file_uploader_galleries_dir = PHPWG_ROOT_PATH.'galleries/'.$file_uploader_galleries_folder.'/';

$file_uploader_allowed_thumbnail_extension = array('jpg', 'jpeg', 'gif', 'png');
	
$file_uploader_destination_folder = array(
	'pdf' => 'pdf',
	'doc' => 'documents',
	'docx' => 'documents',
	'odt' => 'documents',
	'xls' => 'spreadsheets',
	'xlsx' => 'spreadsheets',
	'ods' => 'spreadsheets',
	'ppt' => 'presentations',
	'pptx' => 'presentations',
	'odp' => 'presentations',
	'webm' => 'videos',
	'webmv' => 'videos',
	'ogv' => 'videos',
	'm4v' => 'videos',
	'mp4' => 'videos',
	'flv' => 'videos',
	'mp3' => 'musics',
	'ogg' => 'musics',
	'oga' => 'musics',
	'm4a' => 'musics',
	'webma' => 'musics',
	'fla' => 'musics',
	'wav' => 'musics',
	'others' => 'others',
);

$file_uploader_default_config = array(
	'new_tab' => '1',
	'overwrite' => '0',
	'pdf2tab_extensions' => array('pdf', 'docx', 'doc', 'odt', 'xlsx', 'xls', 'ods', 'pptx', 'ppt', 'odp'),
);

?>