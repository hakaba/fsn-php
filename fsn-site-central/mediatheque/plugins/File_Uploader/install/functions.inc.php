<?php
function file_uploader_install($config) {
	$query = 'INSERT INTO '.CONFIG_TABLE.' (param,value,comment) VALUES ("file_uploader" ,"'.pwg_db_real_escape_string(serialize($config)).'", "File Uploader plugin parameters");';
	pwg_query($query);
}

function file_uploader_update_db($file_uploader_default_config) {
	global $conf;
	
	$config = array();
	
	if (isset($conf['file_uploader'])) {
		$conf_file_uploader = unserialize($conf['file_uploader']);
		foreach ($file_uploader_default_config as $key => $value) {
			if (isset($conf_file_uploader[$key]))
				$config[$key] = $conf_file_uploader[$key];
			else
				$config[$key] = $file_uploader_default_config[$key];
		}
		file_uploader_delete_conf("file_uploader");
		file_uploader_install($config);
	} else {
		file_uploader_install($file_uploader_default_config);
	}
}

function file_uploader_delete_conf($where) {
	$query = 'DELETE FROM ' . CONFIG_TABLE . ' WHERE (param="'.$where.'");';
	pwg_query($query);
}

function file_uploader_folder($file_uploader_galleries_folder, $file_uploader_galleries_dir, $file_uploader_galleries_folder_name) {
	//If the directory does not exist, we create it
	if (!file_exists($file_uploader_galleries_dir))
		if(!mkdir($file_uploader_galleries_dir))
			array_push($page['errors'], l10n('Unable to create folder ').$file_uploader_galleries_dir);
		//If the physical category is not in database, we add it
		else if (pwg_db_num_rows(pwg_query('SELECT id FROM '.CATEGORIES_TABLE.' WHERE dir = "'.$file_uploader_galleries_folder.'";')) == 0){
			$next_id = pwg_db_nextval('id', CATEGORIES_TABLE);
			$category_rank = pwg_db_fetch_assoc(pwg_query('SELECT MAX(rank) FROM '.CATEGORIES_TABLE.';'));
			$category_rank = $category_rank['MAX(rank)'] + 1;
			pwg_query('INSERT INTO '.CATEGORIES_TABLE.' (id, name, comment, dir, rank, status, visible, uppercats, global_rank, site_id) VALUES ('.$next_id.', "'.$file_uploader_galleries_folder_name.'", "Created by the File Uploader plugin", "'.$file_uploader_galleries_folder.'", '.$category_rank.', "private", "true", "'.$next_id.'", '.$category_rank.', 1);');
		}
}
?>