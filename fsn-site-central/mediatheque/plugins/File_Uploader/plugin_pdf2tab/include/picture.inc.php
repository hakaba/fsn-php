<?php
add_event_handler('render_element_content','file_uploader_pdf2tab_add_links_picture',EVENT_HANDLER_PRIORITY_NEUTRAL+10,2);

function file_uploader_pdf2tab_add_links_picture($content, $element_info) {
	global $conf;
	
	$conf_file_uploader = unserialize($conf['file_uploader']);
	
	if (in_array(strtolower(substr(strrchr($element_info['file'], "."),1)), $conf_file_uploader['pdf2tab_extensions'])) {
		$content = file_uploader_pdf2tab_add_link_image($content, $element_info);
		$content = file_uploader_pdf2tab_add_link_beside_image($content, $element_info);
	}
	
	return $content;
}

function file_uploader_pdf2tab_add_link_image($content, $element_info) {
	preg_match('/<\s*img\s+[^>]*?src\s*=\s*(\'|\")(.*?)\\1[^>]*?\/?\s*>/i',$content,$match);
	$content = str_replace($match[0],'<a href="'.$element_info['element_url'].'" target="_blank" title="'.l10n('file_uploader_pdf2tab_tooltip').'">'.$match[0].'</a>',$content);
	
	return $content;
}

function file_uploader_pdf2tab_add_link_beside_image($content, $element_info) {
	$content .= '<p class="imageComment"><a href="'.$element_info['element_url'].'" target="_blank" title="'.l10n('file_uploader_pdf2tab_tooltip').'">'.l10n('file_uploader_pdf2tab_tooltip').'</a>';
	
	return $content;
}
?>