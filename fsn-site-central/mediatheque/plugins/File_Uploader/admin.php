<?php
//Chech whether we are indeed included by Piwigo.
if (!defined('FILE_UPLOADER_PATH')) die('Hacking attempt!');

//Load globals
global $template, $conf, $page;

//Library for tabs
include_once(PHPWG_ROOT_PATH .'admin/include/tabsheet.class.php');

//Load translation files
load_language('plugin.lang', FILE_UPLOADER_PATH);

//Check access and exit when user status is not ok
check_status(ACCESS_ADMINISTRATOR);

//Tab management
if (empty($conf['File_Uploader_tabs'])) {
  $conf['File_Uploader_tabs'] = array('upload', 'configuration', 'help');
}

$page['tab'] = isset($_GET['tab']) ? $_GET['tab'] : $conf['File_Uploader_tabs'][0];

if (!in_array($page['tab'], $conf['File_Uploader_tabs'])) die('Hacking attempt!');	

$tabsheet = new tabsheet();
foreach ($conf['File_Uploader_tabs'] as $tab) {
  $tabsheet->add($tab, l10n(ucfirst($tab)), FILE_UPLOADER_ADMIN.'-'.$tab);
}
$tabsheet->select($page['tab']);
$tabsheet->assign();

include_once(FILE_UPLOADER_PATH.'/admin/admin_'.$page['tab'].'.php');
?>