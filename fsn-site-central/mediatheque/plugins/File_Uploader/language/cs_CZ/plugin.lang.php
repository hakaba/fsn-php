<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Open files in a new tab'] = 'Otevřít soubory v novém panelu';
$lang[': Check to overwrite files during upload if a file with the same name already exists.'] = ': Zaškrtněte pro přepsání souboru při nahrávání, pokud již soubor se stejným názvem existuje.';
$lang['Album:'] = 'Album:';
$lang['No file to upload'] = 'Žádný soubor k nahrání';
$lang['Missing a temporary folder'] = 'Chybí dočasná složka';
$lang['Invalid file name'] = 'Chybný název souboru';
$lang['Help'] = 'Nápověda';
$lang['File to upload:'] = 'Soubor k nahrání:';
$lang['File Uploader Plugin'] = 'File Uploader Plugin';
$lang['Failed to write file to disk'] = 'Soubor se nepodařilo zapsat na disk';
$lang['Donate'] = 'Darovat';
$lang['Configuration'] = 'Konfigurace';
$lang['Choose a file'] = 'Vybrat soubor';
$lang['... or '] = '... nebo';
$lang['Specify a thumbnail for your file'] = 'Zvolte náhled pro Váš soubor';
$lang['Specify a file to upload'] = 'Zvolte soubor pro nahrání';
$lang['Properties'] = 'Vlastnosti';
$lang['Overwrite files without notice'] = 'Přepiš soubory bez upozornění';
$lang['File was only partially uploaded'] = 'Soubor byl nahrán jen částečně';
$lang['File uploaded and synchronized'] = 'Soubor nahrán a synchronizován';
$lang[': Check to open files in a new tab or download them depending on the extension.'] = ': zaškrtni, pokud chcete otevřít soubory v nové záložce nebo je stáhnout v závislosti na nainstalovaném rozšíření.';
$lang['file_uploader_pdf2tab_tooltip'] = 'Otevři soubor v novém panelu';
$lang['File exceeds the upload_max_filesize directive in php.ini'] = 'Soubor překročil hranici upload_max_filesize v php.ini';
$lang['Choose a thumbnail'] = 'Zvol náhled';
$lang['Can\'t upload file to galleries directory'] = 'Nelze nahrát soubor do adresáře galerií';
?>