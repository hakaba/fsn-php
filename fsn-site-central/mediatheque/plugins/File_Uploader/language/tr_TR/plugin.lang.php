<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2012 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['You can use a personnal image or convert your file to an image using a desktop program or a web service such as'] = 'Kişisel bir imaj kullanabileceğiniz gibi dosyanızı bunlar gibi bir masa üstü program veya web hizmeti ile imaja çevirebilirsiniz:';
$lang['The Configuration tab allows you to customize the plugin in two ways:'] = 'Yapılandırma sekmesi eklentiyi iki yolla kişiselleştirmenize olanak tanır:';
$lang['The Upload tab allows you to upload "non picture" files to your gallerie. Fill in the form to upload and synchronize your file.'] = 'Yükleme sekmesi "resim olmayan" dosyaları galerinize yüklemenize olanak tanır. Dosyanızı yükleme ve eşleme için formu doldurun.';
$lang['You can use this plugin with the plugin <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> to add an icon to your "non picture" files.'] = 'Bu eklentiyi <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> eklentisi ile kullanarak "resim olmayan" dosyalara ikon ekleyebilirsiniz.';
$lang['create a new album'] = 'yeni bir albüm yarat';
$lang['Type of file not supported'] = 'Dosya tipi desteklenmiyor';
$lang['Unable to create folder '] = 'Dizin yaratılamadı';
$lang['Upload'] = 'Yükle';
$lang['Upload error'] = 'Yükleme hatası';
$lang['Title:'] = 'Başlık:';
$lang['Thumbnail to upload:'] = 'Yüklenecek küçük resim (thumbnail):';
$lang['There have been errors. See below'] = 'Hatalar var. Aşağıya bakınız:';
$lang['The thumbnail must be a picture'] = 'Küçük resim (thumbnail) bir resim dosyası olmalı';
$lang['Specify a thumbnail for your file'] = 'Dosya için küçük resim (thumbnail) belirtin';
$lang['Piwigo is a free software (opensource software), you can use it for free and this is not going to change. Donations are collected by the Piwigo Foundation (non profit organization) to cover project fees.'] = 'Piwigo bedava yazılımdır (açık kaynak yazılım), serbestçe kullanabilirsiniz ve bu değişmeyecektir. Bağışlarınız Piwigo kuruluşu (ticari olmayan kuruluş) tarafından proje masrafları için toplanmaktadır.';
$lang['Properties'] = 'Özellikler';
$lang['No file to upload'] = 'Yükleyecek dosya yok';
$lang['Open files in a new tab'] = 'PDF dosyaların yeni sekmede aç';
$lang['file_uploader_pdf2tab_tooltip'] = 'Dosyayı yeni sekmede aç';
$lang['Overwrite files without notice'] = 'Uyarmaksızın dosyaların üzerine yaz';
$lang['Specify a file to upload'] = 'Yüklenecek bir dosya seçin';
$lang['Missing a temporary folder'] = 'Geçici dizin eksik';
$lang['Invalid file name'] = 'Geçersiz dosya adı';
$lang['Help'] = 'Yardım';
$lang['File with this name already exists. You should rename this file.'] = 'Aynı isimde dosya zaten var. Dosya adını değiştirmelisiniz.';
$lang['File was only partially uploaded'] = 'Dosya sadece kısmı olarak yüklendi';
$lang['File uploaded and synchronized'] = 'Dosya yüklendi ve eşlendi';
$lang['File upload stopped by extension'] = 'Dosya yüklemesi durduruldu';
$lang['File to upload:'] = 'Yüklenecek dosya:';
$lang['File exceeds the upload_max_filesize directive in php.ini'] = 'Dosya php.ini içindeki upload_max_filesize yönergesindeki değeri aşmaktadır';
$lang['File Uploader Plugin'] = 'Dosya Yükleme Eklentisi';
$lang['Failed to write file to disk'] = 'Diske dosya yazma sırasında hata';
$lang['Donate'] = 'Bağış Yapın';
$lang['Configuration'] = 'Yapılandırma';
$lang['Choose a thumbnail'] = 'Bir küçük resim (thumbnail) seçin';
$lang['Choose a file'] = 'Bir dosya seçin';
$lang['Can\'t upload file to galleries directory'] = 'Dosya galeriler dizinine kopyalanamadı';
$lang['Album:'] = 'Albüm:';
$lang[': Check to overwrite files during upload if a file with the same name already exists.'] = ': Yükleme sırasında aynı isimde dosya varsa üzerine yazılması için işaretleyin.';
$lang['... or '] = '... yada';
$lang[': Check to open files in a new tab or download them depending on the extension.'] = ': Yeni bir sekmede açık dosyaları kontrol edin yada uzantıya bağlı olarak tümünü indirin.';
$lang['file_uploader_error_already_exist'] = 'Bu isimde bir dosya zaten var. Bu dosyanın adını değiştirmelisiniz. Eğer dosyanın üzerine yazmak istiyorsanız yapılandırma sekmesinden yapılandırma ayarlarını değiştirin.';
$lang['file_uploader_upload_max_filesize'] = 'Sunucunuzun transfer maks. dosya boyutu şudur:';
$lang['file_uploader_pdf2tab_hint_text'] = 'Bir uzantı yazın';
$lang['file_uploader_pdf2tab_supported_extensions_description'] = 'Eklenti tarafından indirilecek veya yeni bir sekmede açılacak bir uzantı girin. Uzantılar büyü-küçük harf duyarlıdır.';
?>