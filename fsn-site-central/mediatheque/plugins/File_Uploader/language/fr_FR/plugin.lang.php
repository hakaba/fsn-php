<?php
$lang['File Uploader Plugin'] = 'Plugin File Uploader';

//admin.php
$lang['Upload'] = 'Ajouter';
$lang['Configuration'] = 'Configuration';
$lang['Help'] = 'Aide';

//admin_upload.php
$lang['Specify a file to upload'] = 'Choisir un fichier à ajouter';
$lang['Specify a thumbnail for your file'] = 'Choisir une miniature pour votre fichier';
$lang['Unable to create folder '] = 'Error lors de la création du répertoire ';
$lang['File exceeds the upload_max_filesize directive in php.ini'] = 'Le fichier dépasse la taille upload_max_filesize définie dans php.ini';
$lang['File was only partially uploaded'] = 'Le fichier n\'a été que partiellement transmis';
$lang['No file to upload'] = 'Aucun fichier à transférer';
$lang['Missing a temporary folder'] = 'Aucun répertoire temporaire paramétré';
$lang['Failed to write file to disk'] = 'Echec lors de l\'écriture sur le disque';
$lang['File upload stopped by extension'] = 'Transfert du fichier arrêtée par une extension';
$lang['Upload error'] = 'Erreur lors du transfert';
$lang['Type of file not supported'] = 'Type de fichier non supporté';
$lang['file_uploader_error_already_exist'] = 'Un fichier avec le même nom existe déjà. Vous devriez renommer ce fichier. Si vous voulez remplacer le fichier, modifiez la configuration dans l\'onglet configuration';
$lang['Invalid file name'] = 'Nom de fichier non valide';
$lang['Can\'t upload file to galleries directory'] = 'Copie ver le répertoire galleries impossible';
$lang['The thumbnail must be a picture'] = 'La miniature doit être une image';
$lang['File uploaded and synchronized'] = 'Fichier transféré et synchronisé';
$lang['There have been errors. See below'] = 'Il y a eu des erreurs';

//admin_upload.tpl
$lang['Choose a file'] = 'Choisir un fichier';
$lang['File to upload:'] = 'Fichier à transférer :';
$lang['file_uploader_upload_max_filesize'] = 'Votre serveur impose une taille des fichiers maximale de';
$lang['Choose a thumbnail'] = 'Choisir une miniature';
$lang['Thumbnail to upload:'] = 'Miniature à transférer :';
$lang['You can use a personnal image or convert your file to an image using a desktop program or a web service such as'] = 'Vous pouvez utiliser une image personnelle ou convertir votre fichier en image à l\'aide d\'un programme ou d\'un site web comme';
$lang['Properties'] = 'Propriétés';
$lang['Album:'] = 'Album :';
$lang['Title:'] = 'Titre :';
$lang['... or '] = '... ou ';
$lang['create a new album'] = 'créer un nouvel album';

//admin_configuration.tpl
$lang['Open files in a new tab'] = 'Ouvrir les fichiers dans un nouvel onglet';
$lang['Overwrite files without notice'] = 'Remplacer les fichiers sans avertissement';
$lang['file_uploader_pdf2tab_supported_extensions_description'] = 'Entrer les extensions que le plugin ouvrira dans un nouvel onglet ou téléchargera. Les extensions ne sont pas sensibles à la casse.';
$lang['file_uploader_pdf2tab_hint_text'] = 'Entrer une extension';

//admin_help.tpl
$lang['The Upload tab allows you to upload "non picture" files to your gallerie. Fill in the form to upload and synchronize your file.'] = 'L\'onglet Ajouter permet de transférer des fichier "non images" vers votre galerie. Remplissez le formulaire pour transférer et synchroniser votre fichier.';
$lang['The Configuration tab allows you to customize the plugin in two ways:'] = 'L\'onglet configuration permet de personnaliser l\'extension de deux manières :';
$lang[': Check to open files in a new tab or download them depending on the extension.'] = ' : Cocher pour ouvrir les fichiers dans un nouvel onglet ou les télécharger en fonction de l\'extension.';
$lang[': Check to overwrite files during upload if a file with the same name already exists.'] = ' : Cocher pour remplacer les fichiers pendant le transfert si un fichier avec le même nom existe déjà.';
$lang['You can use this plugin with the plugin <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> to add an icon to your "non picture" files.'] = 'Vous pouvez utiliser ce plugin avec le plugin <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> pour ajouter une icône aux fichiers "non images".';
$lang['Donate'] = 'Dons';
$lang['Piwigo is a free software (opensource software), you can use it for free and this is not going to change. Donations are collected by the Piwigo Foundation (non profit organization) to cover project fees.'] = 'Piwigo est un logiciel libre, son utilisation est gratuite et le restera. Les dons sont récoltés par l\'association Piwigo (loi 1901, organisation à but non lucratif) pour couvrir les dépenses du projet.';


//pdf2tab
//picture.php
$lang['file_uploader_pdf2tab_tooltip'] = 'Ouvrir le fichier dans un nouvel onglet';
?>