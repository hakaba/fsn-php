<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2012 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['You can use this plugin with the plugin <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> to add an icon to your "non picture" files.'] = 'Puede usar el plugin con el plugin  <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> para añadir iconos a los archivos que no sean fotos.';
$lang['create a new album'] = 'Crear un nuevo álbum';
$lang['Missing a temporary folder'] = 'Falta una carpeta temporal';
$lang['No file to upload'] = 'Ningun archivo para subir';
$lang['Open files in a new tab'] = 'Abrir archivos en una nueva pestaña';
$lang['file_uploader_pdf2tab_tooltip'] = 'Abrir archivo en una nueva pestaña';
$lang['Overwrite files without notice'] = 'Sobrescribir archivos sin notificarlo';
$lang['Piwigo is a free software (opensource software), you can use it for free and this is not going to change. Donations are collected by the Piwigo Foundation (non profit organization) to cover project fees.'] = 'Piwigo es un software gratuito (código abierto), puedes usarlo gratis y esto no va a cambiar. Las donaciones son recolectadas por la fundación Piwigo (sin ánimo de lucro) para cubrir los gastos del proyecto.';
$lang['Properties'] = 'Propiedades';
$lang['Specify a file to upload'] = 'Elegir un archivo para subir';
$lang['Specify a thumbnail for your file'] = 'Elegir una miniatura para el archivo';
$lang['The Configuration tab allows you to customize the plugin in two ways:'] = 'La pestaña de configuración permite personalizar el plugin de dos formas:';
$lang['The Upload tab allows you to upload "non picture" files to your gallerie. Fill in the form to upload and synchronize your file.'] = 'La pestaña de subida permite subir otros archivos que no sean fotos a la galeria. Rellena el formulario para subir y sincroniza el archivo.';
$lang['The thumbnail must be a picture'] = 'La miniatura debe ser una foto';
$lang['There have been errors. See below'] = 'Hubo errores, ver debajo.';
$lang['Thumbnail to upload:'] = 'Miniatura a subir:';
$lang['Title:'] = 'Título:';
$lang['Type of file not supported'] = 'Tipo de archivo no permitido';
$lang['Unable to create folder '] = 'Fue imposible crear la carpeta';
$lang['Upload'] = 'Subir';
$lang['Upload error'] = 'Error al subir';
$lang['You can use a personnal image or convert your file to an image using a desktop program or a web service such as'] = 'Puede usar una imagen personal o convertir su archivo en imagen usando un programa de escritorio o un servicio web como';
$lang['... or '] = '...o';
$lang[': Check to overwrite files during upload if a file with the same name already exists.'] = ': marca para sobrescribir los archivos durante la subida si un archivo con el mismo nombre ya existe.';
$lang['Album:'] = 'Album';
$lang['Can\'t upload file to galleries directory'] = 'No se pudo copiar el archivo al directorio de la galeria.';
$lang['Choose a file'] = 'Elige un archivo';
$lang['Choose a thumbnail'] = 'Elige una miniatura';
$lang['Configuration'] = 'Configuración';
$lang['Donate'] = 'Donar';
$lang['Failed to write file to disk'] = 'Error al escribir el archivo en el disco';
$lang['File Uploader Plugin'] = 'Plugin del uploader de ficheros';
$lang['File exceeds the upload_max_filesize directive in php.ini'] = 'El archivo excede el valor upload_max_filesize en php.ini';
$lang['File to upload:'] = 'Archivo a subir';
$lang['File upload stopped by extension'] = 'Subida de archivos parada por la extensión';
$lang['File uploaded and synchronized'] = 'Archivo subido y sincronizado';
$lang['File was only partially uploaded'] = 'El archivo se subió parcialmente';
$lang['File with this name already exists. You should rename this file.'] = 'Un archivo con ese nombre ya existe. Debe renombrar el archivo';
$lang['Help'] = 'Ayuda';
$lang['Invalid file name'] = 'Nombre de archivo inválido';
$lang[': Check to open files in a new tab or download them depending on the extension.'] = 'Compruebe para abrir archivos en una nueva pestaña o descargarlos dependiendo de la extensión.';
$lang['file_uploader_error_already_exist'] = 'Un archivo con el mismo nombre ya existe. Usted debe cambiar el nombre de este archivo. Si desea reemplazar el archivo, cambiar la configuración en la ficha Configuración';
$lang['file_uploader_pdf2tab_hint_text'] = 'Introduzca la extensión';
$lang['file_uploader_pdf2tab_supported_extensions_description'] = 'Introduzca las extensiones que el plugin se abrirá en una nueva pestaña o descargara. Las extensiones no se distingue entre mayúsculas y minúsculas.';
$lang['file_uploader_upload_max_filesize'] = 'Su servidor permite subir archivos con un tamaño máximo de';
?>