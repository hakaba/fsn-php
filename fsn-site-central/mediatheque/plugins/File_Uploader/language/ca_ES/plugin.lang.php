<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Donate'] = 'Donar';
$lang['Configuration'] = 'Configuració';
$lang['Choose a file'] = 'Escull un fitxer';
$lang['Album:'] = 'Àlbum:';
$lang['... or '] = '... o';
$lang['Can\'t upload file to galleries directory'] = 'No s\'ha pogut penjar el fitxer al directori de galeries';
$lang['File uploaded and synchronized'] = 'El fitxer s\'ha penjat i sincronitzat';
$lang['File was only partially uploaded'] = 'El fitxer només s\'ha penjat parcialment';
$lang['Missing a temporary folder'] = 'No s\'ha trobat una carpeta temporal';
$lang['Specify a thumbnail for your file'] = 'Indiqueu una miniatura pel vostre fitxer';
$lang['Upload error'] = 'S\'ha produït un error en penjar el fitxer';
$lang['You can use a personnal image or convert your file to an image using a desktop program or a web service such as'] = 'Podeu utilitzar una imatge personal o convertir un fitxer en una imatge utilitzant un programa d\'escriptori o servei web com per exemple';
$lang['Specify a file to upload'] = 'Indiqueu un fitxer per penjar';
$lang['Thumbnail to upload:'] = 'Miniatura per penjar:';
$lang['Upload'] = 'Penja';
$lang['File to upload:'] = 'Fitxer a penjar:';
$lang['No file to upload'] = 'No hi ha cap fitxer per penjar';
$lang['file_uploader_upload_max_filesize'] = 'El servidor imposa un màxim de mida de fitxer de ';
$lang['create a new album'] = 'crea un nou àlbum';
$lang['file_uploader_pdf2tab_hint_text'] = 'Indiqueu una extensió';
$lang['file_uploader_pdf2tab_tooltip'] = 'Obre el fitxer en una nova pestanya';
$lang['The thumbnail must be a picture'] = 'La miniatura ha de ser una imatge';
$lang['There have been errors. See below'] = 'Hi han hagut errors. Consulteu a continuació';
$lang['Title:'] = 'Titol:';
$lang['Type of file not supported'] = 'Tipus de fitxer no suportat';
$lang['Unable to create folder '] = 'No ha estat possible crear la carpeta';
$lang['Piwigo is a free software (opensource software), you can use it for free and this is not going to change. Donations are collected by the Piwigo Foundation (non profit organization) to cover project fees.'] = 'Piwigo és programari lliure (opensource software), el podeu utilitzar de forma gratuïta per sempre. La Piwigo Foundation (organització sense ànim de lucre)  recull donacions per cobrir les despeses del projecte. ';
$lang['Choose a thumbnail'] = 'Escolliu una miniatura';
$lang['Failed to write file to disk'] = 'Hi ha hagut un error en escriure al disc';
$lang['Help'] = 'Ajuda';
$lang['Invalid file name'] = 'Nom de fitxer no vàlid';
$lang['Open files in a new tab'] = 'Obre fitxers en una nova pestanya';
$lang['Overwrite files without notice'] = 'Sobreescriu els fitxers sense avisar';
$lang['Properties'] = 'Propietats';