<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2012 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Can\'t upload file to galleries directory'] = 'Het bestand kan niet geupload worden naar de galleries-map';
$lang['file_uploader_upload_max_filesize'] = 'Uw server heeft een maximale bestandsgrootte ingesteld van';
$lang['Piwigo is a free software (opensource software), you can use it for free and this is not going to change. Donations are collected by the Piwigo Foundation (non profit organization) to cover project fees.'] = 'Piwigo is een gratis programma (open source software). U kunt het gratis gebruiken en dit zal ook zo blijven. Donaties worden graag tegemoet gezien door de Piwigo Foundation (een non-profit organisatie). De donaties zullen worden gebruikt om de kosten die zijn verbonden aan het project, te dekken.';
$lang['You can use a personnal image or convert your file to an image using a desktop program or a web service such as'] = 'U kunt gebruik maken van een eigen afbeelding of uw bestand omzetten naar een afbeelding met behulp van een desktop-programma of een web service zoals';
$lang['You can use this plugin with the plugin <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> to add an icon to your "non picture" files.'] = 'U kunt deze plugin gebruiken samen met de plugin <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> om een pictogram toe te voegen aan uw "niet-afbeeldingen" bestanden.';
$lang['create a new album'] = 'maak een nieuw album';
$lang['file_uploader_error_already_exist'] = 'Er bestaat al een bestand met deze naam. U dient dit bestand een andere naam te geven. Als u het bestand wilt overschrijven, kunt u dat aangeven bij de instellingen op de Instellingen-tab.';
$lang['file_uploader_pdf2tab_hint_text'] = 'Geef een extensie';
$lang['file_uploader_pdf2tab_supported_extensions_description'] = 'Geef de extensies die PDF2Tab kan afhandelen. NB De extensies zijn hoofdlettergevoelig.';
$lang['file_uploader_pdf2tab_tooltip'] = 'Open het bestand in een nieuw tablad';
$lang['Album:'] = 'Toevoegen aan album:';
$lang['Open files in a new tab'] = 'Open bestanden in een nieuw tabblad';
$lang['Overwrite files without notice'] = 'Overschrijf bestanden zonder waarschuwing';
$lang['Properties'] = 'Eigenschappen';
$lang['Specify a file to upload'] = 'Geef een bestandsnaam om te uploaden';
$lang['Specify a thumbnail for your file'] = 'Geef een klikplaatje voor uw bestand';
$lang['The Configuration tab allows you to customize the plugin in two ways:'] = 'De Instellingen-tab geeft u de mogelijkheid om deze plugin op een tweetal manieren aan te passen naar uw wensen';
$lang['The Upload tab allows you to upload "non picture" files to your gallerie. Fill in the form to upload and synchronize your file.'] = 'De Uplad-tab geeft u de mogelijkheid om "niet-afbeeldingen"-bestanden te uploaden naar uw galerie. Vul het formulier in om uw bestand te uploasden en te synchroniseren.';
$lang['The thumbnail must be a picture'] = 'Het klikplaatje moet een afbeelding zijn';
$lang['There have been errors. See below'] = 'Er zijn fouten opgetreden: Zie hieronder';
$lang['Thumbnail to upload:'] = 'Klikplaatje om te uploaden:';
$lang['Title:'] = 'Bestandstitel:';
$lang['Type of file not supported'] = 'Dit type bestand wordt niet ondersteund';
$lang['Unable to create folder '] = 'Kon de map niet creëren ';
$lang['Upload'] = 'Upload';
$lang['Upload error'] = 'Uploadfout';
$lang['... or '] = '... of';
$lang[': Check to open files in a new tab or download them depending on the extension.'] = ': Zet vinkje om bestanden te openen in een nieuw tabblad of om de bestanden te downloaden afhankelijk van de extensie.   ';
$lang[': Check to overwrite files during upload if a file with the same name already exists.'] = ': Zet vinkje om tijdens de upload bestanden met dezelfde naam te overschrijven.';
$lang['Choose a file'] = 'Kies een bestand';
$lang['Choose a thumbnail'] = 'Kies een klikplaatje';
$lang['Configuration'] = 'Instellingen';
$lang['Donate'] = 'Doneer';
$lang['Failed to write file to disk'] = 'Het bestand kon niet naar de schijf geschreven worden';
$lang['File Uploader Plugin'] = 'File Uploader Plugin';
$lang['File exceeds the upload_max_filesize directive in php.ini'] = 'Het bestand is groter als gedefinieerd door upload_max_filesize in php.ini';
$lang['File to upload:'] = 'Bestand om te uploaden:';
$lang['File upload stopped by extension'] = 'Het uploaden van het bestand werd gestopt door de extensie';
$lang['File uploaded and synchronized'] = 'Bestand is geupload en gesynchroniseerd';
$lang['File was only partially uploaded'] = 'Bestand werd maar gedeeltelijk geupload';
$lang['Help'] = 'Help';
$lang['Invalid file name'] = 'Ongeldige bestandsnaam';
$lang['Missing a temporary folder'] = 'Een tijdelijke map ontbreekt';
$lang['No file to upload'] = 'Geen bestand om te uploaden';
?>