<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['You can use this plugin with the plugin <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> to add an icon to your "non picture" files.'] = 'Pode usar esta extensão com a extensão <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654"> Mídia Icon </ a> para adicionar um ícone aos seus arquivos  "nao foto".';
$lang['create a new album'] = 'Criar um novo album';
$lang['file_uploader_error_already_exist'] = 'Já existe um arquivo com este nome. Deve renomear este arquivo. Se desejar substituir o arquivo, modifique a configuração na aba configuração.';
$lang['file_uploader_pdf2tab_hint_text'] = 'Escreva uma extensão';
$lang['file_uploader_pdf2tab_supported_extensions_description'] = 'Digite as extensões  pretendidas, a extensão abrirá novas abas ou descarrega. As extensões são em minúsculas.';
$lang['file_uploader_pdf2tab_tooltip'] = 'Abrir arquivo em nova aba';
$lang['file_uploader_upload_max_filesize'] = 'O seu server impõe um tamanho de arquivo no máximo´de';
$lang['Open files in a new tab'] = 'Abrir arquivos numa nova aba';
$lang['Overwrite files without notice'] = 'Reescrever arquivos sem informação';
$lang['Piwigo is a free software (opensource software), you can use it for free and this is not going to change. Donations are collected by the Piwigo Foundation (non profit organization) to cover project fees.'] = 'Piwigo é um software livre (software de código aberto), pode usá-lo gratuitamente e isto não vai mudar. As doações são reunidas pela Fundação Piwigo (organização sem fins lucrativos) para cobrir as despesas do projeto.';
$lang['Properties'] = 'Propriedades';
$lang['Specify a file to upload'] = 'Escolha um arquivo para carregar';
$lang['Specify a thumbnail for your file'] = 'Defina uma miniatura para o arquivo';
$lang['The Configuration tab allows you to customize the plugin in two ways:'] = 'A aba Configuração permite-lhe personalizar a extenção de duas maneiras:';
$lang['The Upload tab allows you to upload "non picture" files to your gallerie. Fill in the form to upload and synchronize your file.'] = 'A aba Enviar permite carregar arquivos "não foto" na sua galeria . Preencha o formulário para carregar e sincronizar o seu arquivo.';
$lang['The thumbnail must be a picture'] = 'A miniatura deve ser uma fotografia';
$lang['There have been errors. See below'] = 'Ocorreram erros. Ver abaixo';
$lang['Thumbnail to upload:'] = 'Miniatura para carregar';
$lang['Title:'] = 'Título';
$lang['Type of file not supported'] = 'O tipo de arquivo não é suportado';
$lang['Unable to create folder '] = 'Não é possivel criar a pasta';
$lang['Upload'] = 'Carregar';
$lang['Upload error'] = 'Erro ao carregar';
$lang['You can use a personnal image or convert your file to an image using a desktop program or a web service such as'] = 'Pode usar uma imagem pessoal ou converter o arquivo para uma imagem usando programa do seu computador ou um serviço web, como';
$lang['... or '] = '...ou';
$lang[': Check to open files in a new tab or download them depending on the extension.'] = ': Assinale para abrir arquivos numa nova aba ou descarregá-los, dependendo da extensão.';
$lang[': Check to overwrite files during upload if a file with the same name already exists.'] = ': Assinale para regravar arquivos durante a carga (upload) de um arquivo que já existe com o mesmo nome.';
$lang['Album:'] = 'Album:';
$lang['Can\'t upload file to galleries directory'] = 'Não é possivel carregar o arquivo para o directório galerias';
$lang['Choose a file'] = 'escolha o arquivo';
$lang['Choose a thumbnail'] = 'Escolha uma miniatura';
$lang['Configuration'] = 'Configuração';
$lang['Donate'] = 'Doar';
$lang['Failed to write file to disk'] = 'Falhou a gravação do arquivo no disco';
$lang['File Uploader Plugin'] = 'Extensão para tansferir arquivos';
$lang['File exceeds the upload_max_filesize directive in php.ini'] = 'O arquivo excede a directiva de upload_max_filesize definida no php.ini';
$lang['File to upload:'] = 'Arquivo para carregar:';
$lang['File upload stopped by extension'] = 'Carga de arquivo parada por extenção';
$lang['File uploaded and synchronized'] = 'Arquivo carregado e sincronizado';
$lang['File was only partially uploaded'] = 'O arquivo apenas foi carregado parcialmente';
$lang['Help'] = 'Ajuda';
$lang['Invalid file name'] = 'Nome do arquivo inválido';
$lang['Missing a temporary folder'] = 'Omitida uma pasta temporária';
$lang['No file to upload'] = 'Não existe arquivo para carregar';
?>