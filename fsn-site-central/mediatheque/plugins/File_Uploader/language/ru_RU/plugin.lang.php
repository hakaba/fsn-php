<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['File upload stopped by extension'] = 'Загрузка файлов прервана';
$lang['File uploaded and synchronized'] = 'Файлы загружены и синхронизированы';
$lang['Failed to write file to disk'] = 'Невозможно записать файл на диск';
$lang['File Uploader Plugin'] = 'Плагин Загрузчик файлов';
$lang['Properties'] = 'Свойства';
$lang['Missing a temporary folder'] = 'Отсутствует временная папка';
$lang['Overwrite files without notice'] = 'Перезаписать файлы без уведомления';
$lang['Thumbnail to upload:'] = 'Миниатюра для загрузки:';
$lang['File to upload:'] = 'Файл для загрузки:';
$lang['Donate'] = 'Поддержать';
$lang['create a new album'] = 'создать новый альбом';
$lang['file_uploader_pdf2tab_tooltip'] = 'Открыть файл в новой вкладке';
$lang['Unable to create folder '] = 'Невозможно создать папку';
$lang['Upload'] = 'Загрузка';
$lang['Upload error'] = 'Ошибка загрузки';
$lang['Title:'] = 'Название:';
$lang['Open files in a new tab'] = 'Открыть файлы в новой вкладке';
$lang['No file to upload'] = 'Нет файлов для загрузки';
$lang['Invalid file name'] = 'Неверное имя файла';
$lang['Help'] = 'Помощь';
$lang['Configuration'] = 'Конфигурация';
$lang['Album:'] = 'Альбом:';
$lang['Can\'t upload file to galleries directory'] = 'Не удается загрузить файл в папку галереи';
$lang['Choose a file'] = 'Выберите файл';
$lang['Choose a thumbnail'] = 'Выберите миниатюру';
$lang[': Check to overwrite files during upload if a file with the same name already exists.'] = ': Отметьте, чтобы перезаписать файлы во время загрузки, если файл с таким именем уже существует.';
$lang['... or '] = '... или';
$lang[': Check to open files in a new tab or download them depending on the extension.'] = ': Отметьте, чтобы открыть файлы в новой вкладке или загрузить их в зависимости от расширения.';
$lang['Piwigo is a free software (opensource software), you can use it for free and this is not going to change. Donations are collected by the Piwigo Foundation (non profit organization) to cover project fees.'] = 'Piwigo является свободным программным обеспечением (ПО с открытым исходным кодом), вы можете использовать его бесплатно, и это не изменится. Пожертвования собираются Piwigo Foundation (некоммерческая организация), чтобы покрыть расходы проекта.';
$lang['The Configuration tab allows you to customize the plugin in two ways:'] = 'Вкладка конфигурации позволяет настроить плагин двумя способами:';
$lang['The Upload tab allows you to upload "non picture" files to your gallerie. Fill in the form to upload and synchronize your file.'] = 'На вкладке загрузки можно загружать файлы, не являющиеся изображениями в Галерею. Заполните форму для загрузки и синхронизации файлов.';
$lang['The thumbnail must be a picture'] = 'Миниатюра должна быть изображением';
$lang['There have been errors. See below'] = 'Обнаружены ошибки. См. ниже';
$lang['Type of file not supported'] = 'Тип файла не поддерживается';
$lang['You can use a personnal image or convert your file to an image using a desktop program or a web service such as'] = 'Вы можете использовать свои изображения или конвертировать файл изображения с помощью настольной программы или веб-сервиса, таких как';
$lang['You can use this plugin with the plugin <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> to add an icon to your "non picture" files.'] = 'Вы можете использовать этот плагин с плагином <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654"> Медиа иконка </ a>, чтобы добавить значок для ваших файлов, не являющихся изображениями.';
$lang['file_uploader_error_already_exist'] = 'Файл с этим именем уже существует. Вы должны переименовать этот файл. Если вы хотите перезаписать файл, измените настройки на вкладке конфигурации.';
$lang['File exceeds the upload_max_filesize directive in php.ini'] = 'Файл превышает upload_max_filesize директиву в php.ini';
$lang['File was only partially uploaded'] = 'Файл был загружен лишь частично';
$lang['Specify a file to upload'] = 'Укажите файл для загрузки';
$lang['Specify a thumbnail for your file'] = 'Укажите миниатюру для файла';
$lang['file_uploader_pdf2tab_hint_text'] = 'Введите расширение';
$lang['file_uploader_pdf2tab_supported_extensions_description'] = 'Введите расширения которые плагин откроет в новой вкладке или будет загружать. Расширения чувствительны к регистру.';
$lang['file_uploader_upload_max_filesize'] = 'Ваш сервер накладывает ограничения на максимальный размер файла';
?>