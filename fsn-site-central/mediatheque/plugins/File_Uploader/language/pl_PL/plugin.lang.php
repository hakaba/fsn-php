<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['file_uploader_pdf2tab_hint_text'] = 'Podaj rozszerzenie';
$lang['file_uploader_pdf2tab_supported_extensions_description'] = 'Podaj rozszerzenia, które wtyczka będzie otwierać w nowych zakładkach lub pobierać. Wielkość liter nie ma znaczenia.';
$lang['file_uploader_upload_max_filesize'] = 'Serwer narzuca maksymalny rozmiar pliku';
$lang['The Upload tab allows you to upload "non picture" files to your gallerie. Fill in the form to upload and synchronize your file.'] = 'Zakładka wgrywania pozwala wgrywać do galerii pliki "niegraficzne". Wypełnij formularz, aby wygrać i zsynchronizować plik.';
$lang['The Configuration tab allows you to customize the plugin in two ways:'] = 'Zakładka konfiguracyjna pozwala na dostosowanie wtyczki na dwa sposoby:';
$lang['File upload stopped by extension'] = 'Wgrywanie pliku zatrzymane przez rozszerzenie';
$lang['File uploaded and synchronized'] = 'Plik został wgrany i zsynchronizowany';
$lang['Piwigo is a free software (opensource software), you can use it for free and this is not going to change. Donations are collected by the Piwigo Foundation (non profit organization) to cover project fees.'] = 'Piwigo jest oprogramowaniem darmowym (otwrtoźródłowym), którego możesz używać bezpłatnie i to się nie zmieni. Darowizny zbierane przez Fundację Piwigo (organizacja non profit) służą pokryciu kosztów związanych z projektem.';
$lang['File exceeds the upload_max_filesize directive in php.ini'] = 'Rozmiar pliku przekracza wartość dyrektywy upload_max_filesize w php.ini';
$lang['Upload error'] = 'Błąd wgrywania';
$lang['You can use this plugin with the plugin <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> to add an icon to your "non picture" files.'] = 'Możesz używać tej wtyczki razem z wtyczką <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> do dodawania ikon do plików, które nie są obrazami.';
$lang['create a new album'] = 'stwórz nowy album';
$lang['file_uploader_error_already_exist'] = 'Plik o podanej nazwie już istnieje. Powinieneś zmienić nazwę pliku. Jeżeli chcesz nadpisać plik, dostosuj ustawienia w zakładce konfiguracja.';
$lang['file_uploader_pdf2tab_tooltip'] = 'Otwórz plik w nowej zakładce';
$lang['Open files in a new tab'] = 'Otwórz pliki w nowej zakładce';
$lang['Overwrite files without notice'] = 'Nadpisz pliki bez powiadamiania';
$lang['Properties'] = 'Właściwości';
$lang['Specify a file to upload'] = 'Wskaż plik do wgrania';
$lang['Specify a thumbnail for your file'] = 'Wskaż miniaturę dla wybranego pliku';
$lang['The thumbnail must be a picture'] = 'Miniatura musi być obrazem';
$lang['There have been errors. See below'] = 'Wystąpiły następujące błędy';
$lang['Thumbnail to upload:'] = 'Miniatura do wgrania:';
$lang['Title:'] = 'Tytuł';
$lang['Type of file not supported'] = 'Nieobsługiwany format pliku';
$lang['Unable to create folder '] = 'Nie można utworzyć katalogu';
$lang['Upload'] = 'Wgraj';
$lang['... or '] = '... lub';
$lang['Album:'] = 'Album:';
$lang['Can\'t upload file to galleries directory'] = 'Nie można wgrać pliku do katalogu galerii';
$lang['Choose a file'] = 'Wybierz plik';
$lang['Choose a thumbnail'] = 'Wybierz miniaturę';
$lang['Configuration'] = 'Konfiguracja';
$lang['Donate'] = 'Dotacja';
$lang['Failed to write file to disk'] = 'Nie udało się zapisać pliku na dysku';
$lang['File Uploader Plugin'] = 'Wtyczka File Uploader';
$lang['File to upload:'] = 'Plik do wgrania:';
$lang['File was only partially uploaded'] = 'Plik został wgrany tylko częściowo';
$lang['Help'] = 'Pomoc';
$lang['Invalid file name'] = 'Nieprawidłowa nazwa pliku';
$lang['Missing a temporary folder'] = 'Brak katalogu tymczasowego';
$lang['No file to upload'] = 'Brak pliku do wgrania';
$lang[': Check to open files in a new tab or download them depending on the extension.'] = ': Zaznacz, aby otwierać pliki w nowej zakładce lub pobierać je w zależności od rozszerzenia.';
$lang[': Check to overwrite files during upload if a file with the same name already exists.'] = ': Zaznacz, aby nadpisywać pliki podczas wgrywania, jeżeli plik o takiej samej już istnieje.';
$lang['You can use a personnal image or convert your file to an image using a desktop program or a web service such as'] = 'Możesz wykorzystać swój wizerunek lub przekonwertować plik do obrazu za pomocą programu na komputerze stacjonarnym lub usługi internetowej, takiej jak';
?>