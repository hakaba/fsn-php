<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2012 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['There have been errors. See below'] = 'Υπήρξαν λάθη. δείτε παρακάτω';
$lang['The thumbnail must be a picture'] = 'Η μικρογραφία πρέπει να είναι μια εικόνα';
$lang['The Upload tab allows you to upload "non picture" files to your gallerie. Fill in the form to upload and synchronize your file.'] = 'Η καρτέλα Upload σας επιτρέπει να ανεβάσετε αρχεία "μη εικόνας" στη γκαλερί σας. Συμπληρώστε τη φόρμα για να φορτώσετε και να συγχρονίσετε το αρχείο σας.';
$lang['The Configuration tab allows you to customize the plugin in two ways:'] = 'Η καρτέλα Ρυθμίσεις σας επιτρέπει να προσαρμόσετε το πρόσθετο με δύο τρόπους:';
$lang['Help'] = 'Βοήθεια';
$lang['Specify a thumbnail for your file'] = 'Καθορίστε μια μικρογραφία για το αρχείο σας';
$lang['Specify a file to upload'] = 'Καθορίστε ένα αρχείο για να φορτώσετε';
$lang['Properties'] = 'Ιδιότητες';
$lang['Piwigo is a free software (opensource software), you can use it for free and this is not going to change. Donations are collected by the Piwigo Foundation (non profit organization) to cover project fees.'] = 'Το Piwigo είναι δωρεάν λογισμικό (λογισμικό ανοικτού κώδικα), μπορείτε να το χρησιμοποιήσετε ελεύθερα και αυτό δεν πρόκειται να αλλάξει. Δωρεές συλλέγονται από το Ίδρυμα Piwigo Foundation (μη κερδοσκοπικός οργανισμός) για την κάλυψη των αμοιβών του έργου.';
$lang['Overwrite files without notice'] = 'Αντικατάσταση αρχείων χωρίς προειδοποίηση';
$lang['file_uploader_pdf2tab_tooltip'] = 'Ανοίξτε το αρχείο σε μια νέα καρτέλα';
$lang['Open files in a new tab'] = 'Ανοίξτε το αρχεία σε μια νέα καρτέλα';
$lang['No file to upload'] = 'Δεν υπάρχει αρχείο για να φορτώσετε';
$lang['Missing a temporary folder'] = 'Λείπει ένας προσωρινός φάκελος';
$lang['Invalid file name'] = 'Μη έγκυρο όνομα αρχείου';
$lang['File with this name already exists. You should rename this file.'] = 'Αρχείο με αυτό το όνομα υπάρχει ήδη. Θα πρέπει να μετονομάσετε αυτό το αρχείο.';
$lang['File was only partially uploaded'] = 'Το αρχείο φορτώθηκε μόνο εν μέρει';
$lang['File uploaded and synchronized'] = 'Αποστολή αρχείου και συγχρονισμός';
$lang['File upload stopped by extension'] = 'Η Προσθήκη αρχείου σταμάτησε λόγω της επέκτασης';
$lang['File to upload:'] = 'Αρχείο για μεταφόρτωση:';
$lang['File exceeds the upload_max_filesize directive in php.ini'] = 'Το Αρχείο υπερβαίνει την οδηγία upload_max_filesize στο php.ini';
$lang['File Uploader Plugin'] = 'Πρόσθετο File Uploader';
$lang['Failed to write file to disk'] = 'Απέτυχε εγγραφή του αρχείου στο δίσκο';
$lang['Donate'] = 'Δωρίστε';
$lang['Configuration'] = 'Διαμόρφωση';
$lang['Choose a thumbnail'] = 'Επιλέξτε μια μικρογραφία';
$lang['Choose a file'] = 'Επιλέξτε ένα αρχείο';
$lang['Can\'t upload file to galleries directory'] = 'Δεν είναι δυνατή η αντιγραφή του αρχείου σε κατάλογο γκαλερί ';
$lang['Album:'] = 'Λεύκωμα: ';
$lang[': Check to overwrite files during upload if a file with the same name already exists.'] = ': Ελέγξτε να αντικαταστήσετε τα αρχεία κατά τη διάρκεια της αποστολής, αν ένα αρχείο με το ίδιο όνομα υπάρχει ήδη.';
$lang['... or '] = '.. ή';
$lang['create a new album'] = 'δημιουργήσετε ένα νέο λεύκωμα';
$lang['You can use this plugin with the plugin <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> to add an icon to your "non picture" files.'] = 'Μπορείτε να χρησιμοποιήσετε αυτό το πρόσθετο μαζί με το πρόσθετο <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> για να προσθέσετε ένα εικονίδιο για τα αρχεία "μη εικόνας" σας.';
$lang['You can use a personnal image or convert your file to an image using a desktop program or a web service such as'] = 'Μπορείτε να χρησιμοποιήσετε μια προσωπική εικόνα ή να μετατρέψετε το αρχείο σας σε μια εικόνα χρησιμοποιώντας ένα desktop πρόγραμμα ή μια υπηρεσία web, όπως';
$lang['Upload error'] = 'Σφάλμα Μεταφόρτωσης ';
$lang['Upload'] = 'Ανέβασε';
$lang['Unable to create folder '] = 'Δεν είναι δυνατή η δημιουργία φακέλου';
$lang['Type of file not supported'] = 'Ο Τύπος αρχείου δεν υποστηρίζεται';
$lang['Title:'] = 'Τίτλος:';
$lang['Thumbnail to upload:'] = 'Μικρογραφίες για την αποστολή:';
$lang[': Check to open files in a new tab or download them depending on the extension.'] = ': Ελέγξτε να ανοίξετε τα αρχεία σε μια νέα καρτέλα ή να τα κατεβάσετε ανάλογα με την επέκταση.';
$lang['file_uploader_upload_max_filesize'] = 'Ο διακομιστής σας επιβάλει ένα μέγιστο μέγεθος αρχείου';
$lang['file_uploader_pdf2tab_supported_extensions_description'] = 'Πληκτρολογήστε τις επεκτάσεις που το πρόσθετο θα ανοίξει σε νέες καρτέλες ή κατεβάστε το. Οι επεκτάσεις κάνουν διάκριση πεζών-κεφαλαίων.';
$lang['file_uploader_pdf2tab_hint_text'] = 'Πληκτρολογήστε την επέκταση';
$lang['file_uploader_error_already_exist'] = 'Αρχείο με αυτό το όνομα υπάρχει ήδη. Θα πρέπει να μετονομάσετε αυτό το αρχείο. Εάν θέλετε να αντικαταστήσετε το αρχείο, τροποποιήστε τη διαμόρφωση στην καρτέλα ρυθμίσεων.';
?>