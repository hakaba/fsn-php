<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['file_uploader_upload_max_filesize'] = 'Il tuo server impone una dimensione massima del file di';
$lang['file_uploader_pdf2tab_tooltip'] = 'Aprire il file in una nuova scheda';
$lang['file_uploader_error_already_exist'] = 'Un file con questo nome è già esistente. E\' necessario rinominare il file. Se si desidera sovrascrivere il file, modificare la configurazione della scheda di configurazione.';
$lang['You can use this plugin with the plugin <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> to add an icon to your "non picture" files.'] = 'E\' possibile utilizzare questo plugin con il plug-in <a href="http://it.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> per aggiungere un\'icona al vostro "non immagine" file.';
$lang['create a new album'] = 'creare un nuovo album';
$lang['You can use a personnal image or convert your file to an image using a desktop program or a web service such as'] = 'E\' possibile utilizzare un\'immagine personale o di convertire il file in un immagine con un programma sul computer di un servizio Web ';
$lang['Upload'] = 'Carica';
$lang['Upload error'] = 'Errore caricamento';
$lang['Type of file not supported'] = 'Tipo di file non supportato';
$lang['Unable to create folder '] = 'Impossibile creare la cartella';
$lang['Title:'] = 'Titolo:';
$lang['Thumbnail to upload:'] = 'Miniatura da caricare:';
$lang['There have been errors. See below'] = 'Ci sono stati errori. Vedi sotto';
$lang['The thumbnail must be a picture'] = 'La miniatura deve essere una foto';
$lang['The Upload tab allows you to upload "non picture" files to your gallerie. Fill in the form to upload and synchronize your file.'] = 'La scheda di caricamento consente all\'utente di caricare "non immagine" file sulla tua gallerie. Compila il modulo per caricare e sincronizzare il file.';
$lang['The Configuration tab allows you to customize the plugin in two ways:'] = 'La scheda Configurazione consente di personalizzare il plugin in due modi:';
$lang['Piwigo is a free software (opensource software), you can use it for free and this is not going to change. Donations are collected by the Piwigo Foundation (non profit organization) to cover project fees.'] = 'Piwigo è un software libero (software opensource), puoi utilizzarlo gratuitamente e questo non cambierà. Le donazioni vengono raccolte dalla Fondazione Piwigo (organizzazione non profit) per coprire le spese del progetto.';
$lang['Specify a thumbnail for your file'] = 'Specificare una miniatura per il file';
$lang['Specify a file to upload'] = 'Specificare un file da caricare';
$lang['Properties'] = 'Proprietà';
$lang['No file to upload'] = 'Nessun file da caricare';
$lang['Open files in a new tab'] = 'Apri file in una nuova scheda';
$lang['Overwrite files without notice'] = 'Sovrascrivi file senza preavviso';
$lang['Help'] = 'Aiuto';
$lang['Invalid file name'] = 'Nome del file non valido';
$lang['Missing a temporary folder'] = 'Manca la cartella temporanea';
$lang['File was only partially uploaded'] = 'Il file è stato caricato solo parzialmente';
$lang['File uploaded and synchronized'] = 'File caricato e sincronizzato';
$lang['File upload stopped by extension'] = 'Il caricamento del file è stato interrotto dall\'estensione';
$lang['File exceeds the upload_max_filesize directive in php.ini'] = 'Il file eccede la direttiva upload_max_filesize in php.ini';
$lang['File to upload:'] = 'File da caricare:';
$lang['File Uploader Plugin'] = 'File Uploader Plugin';
$lang['Configuration'] = 'Configurazione';
$lang['Donate'] = 'Donare';
$lang['Failed to write file to disk'] = 'Impossibile scrivere il file su disco';
$lang['Choose a thumbnail'] = 'Scegli una miniatura';
$lang['Choose a file'] = 'Scegli un file';
$lang[': Check to overwrite files during upload if a file with the same name already exists.'] = ': Controllare per sovrascrivere i file durante il caricamento se un file con lo stesso nome esiste già.';
$lang['Album:'] = 'Album:';
$lang['Can\'t upload file to galleries directory'] = 'Non è possibile caricare il file nella directory galleries';
$lang[': Check to open files in a new tab or download them depending on the extension.'] = ': Controllare per aprire i file in una nuova scheda o scaricarli a seconda dell\'estensione.';
$lang['... or '] = '... o';
$lang['file_uploader_pdf2tab_supported_extensions_description'] = 'Digitare le estensioni il plugin si apre in nuove schede o scarica. Le estensioni sono case insensitive.';
$lang['file_uploader_pdf2tab_hint_text'] = 'Inserire in un\'estensione';
?>