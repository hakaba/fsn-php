<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['file_uploader_pdf2tab_hint_text'] = 'Digite uma extensão';
$lang['file_uploader_pdf2tab_supported_extensions_description'] = 'Digite as extensões que o plugin irá abrir em novas abas ou baixar. As extensões não diferenciam maiúsculas de minúsculas.';
$lang['file_uploader_pdf2tab_tooltip'] = 'Abra o arquivo em uma nova aba';
$lang['file_uploader_upload_max_filesize'] = 'Seu servidor limita o tamanho máximo de arquivo em';
$lang['Open files in a new tab'] = 'Abrir arquivos em uma nova guia';
$lang['Overwrite files without notice'] = 'Substituir os arquivos sem aviso prévio';
$lang['Piwigo is a free software (opensource software), you can use it for free and this is not going to change. Donations are collected by the Piwigo Foundation (non profit organization) to cover project fees.'] = 'Piwigo é um software livre (software de código aberto), você pode usá-lo gratuitamente e isso não vai mudar. Doações são coletadas pela Fundação Piwigo (organização sem fins lucrativos) para cobrir as despesas do projeto.';
$lang['Properties'] = 'Propriedades';
$lang['Specify a file to upload'] = 'Especifique um arquivo para enviar';
$lang['Specify a thumbnail for your file'] = 'Especifique uma miniatura para seu arquivo';
$lang['The Configuration tab allows you to customize the plugin in two ways:'] = 'A Guia de Configuração permite que você personalize o plugin de duas maneiras:';
$lang['The Upload tab allows you to upload "non picture" files to your gallerie. Fill in the form to upload and synchronize your file.'] = 'A Guia Enviar permite que você envie "não imagens" para sua Galeria. Preencha o formulário para enviar e sincronizar seu arquivo.';
$lang['The thumbnail must be a picture'] = 'A miniatura deve ser uma imagem';
$lang['There have been errors. See below'] = 'Houve erros. Veja abaixo';
$lang['Thumbnail to upload:'] = 'Miniatura para enviar:';
$lang['Title:'] = 'Título:';
$lang['Type of file not supported'] = 'Tipo de arquivo não suportado';
$lang['Unable to create folder '] = 'Não é possível criar pasta';
$lang['Upload'] = 'Enviar';
$lang['Upload error'] = 'Erro no envio';
$lang['You can use a personnal image or convert your file to an image using a desktop program or a web service such as'] = 'Você pode usar uma imagem pessoal ou converter o arquivo para uma imagem usando um programa de computador ou um serviço web, como';
$lang['You can use this plugin with the plugin <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> to add an icon to your "non picture" files.'] = 'Você pode usar este plugin com o plugin <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654"> Mídia Icon </a> para adicionar um ícone para o seu "arquivo não imagem".';
$lang['create a new album'] = 'criar um novo álbum';
$lang['file_uploader_error_already_exist'] = 'Um arquivo com este nome já existe. Você deve renomeá-lo. Se você deseja substituir o arquivo, modifique a configuração na guia de configuração.';
$lang['... or '] = '... ou';
$lang[': Check to open files in a new tab or download them depending on the extension.'] = ': Marque para abrir arquivos em uma nova aba ou baixá-los, dependendo da extensão.';
$lang[': Check to overwrite files during upload if a file with the same name already exists.'] = ': Marque para sobrescrever arquivos durante a transferência se um arquivo com o mesmo nome já existe.';
$lang['Album:'] = 'Album:';
$lang['Can\'t upload file to galleries directory'] = 'Não é possível enviar arquivo para o diretório galerias';
$lang['Choose a file'] = 'Escolha um arquivo';
$lang['Choose a thumbnail'] = 'Escolha uma miniatura';
$lang['Configuration'] = 'Configuração';
$lang['Donate'] = 'Doar';
$lang['Failed to write file to disk'] = 'Falha ao gravar o arquivo em disco';
$lang['File Uploader Plugin'] = 'Plugin Enviador de Arquivo';
$lang['File exceeds the upload_max_filesize directive in php.ini'] = 'Arquivo excede a directiva upload_max_filesize do php.ini';
$lang['File to upload:'] = 'Arquivo para enviar:';
$lang['File upload stopped by extension'] = 'Envio de arquivos parado por causa da extensão';
$lang['File uploaded and synchronized'] = 'Arquivo enviado e sincronizado';
$lang['File was only partially uploaded'] = 'Arquivo foi apenas parcialmente enviado';
$lang['Help'] = 'Ajuda';
$lang['Invalid file name'] = 'Nome de arquivo inválido';
$lang['Missing a temporary folder'] = 'Faltando uma pasta temporária';
$lang['No file to upload'] = 'Nenhum arquivo para enviar';
?>