<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2012 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Choose a thumbnail'] = 'Vælg et miniaturebillede';
$lang['create a new album'] = 'opret et nyt album';
$lang['You can use this plugin with the plugin <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> to add an icon to your "non picture" files.'] = 'Du kan anvende denne plugin sammen med plugin\'en <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> til at tilføje ikonen til din mappe med filer, der ikke er billeder.';
$lang['No file to upload'] = 'Der er ingen fil at uploade';
$lang['Open files in a new tab'] = 'Åbn filer i et nyt faneblad';
$lang['file_uploader_pdf2tab_tooltip'] = 'Åbn filen i et nyt faneblad';
$lang['Overwrite files without notice'] = 'Overskriv filer uden advarsel';
$lang['Piwigo is a free software (opensource software), you can use it for free and this is not going to change. Donations are collected by the Piwigo Foundation (non profit organization) to cover project fees.'] = 'Piwigo er fri software (open source-software), som du frit kan anvende og vil altid være det. Donationer modtages af Piwigo Foundation (nonprofitnorganisation) med det formål at dække projektomkostninger.';
$lang['Properties'] = 'Egenskaber';
$lang['Specify a file to upload'] = 'Angiv fil der skal uploades';
$lang['Specify a thumbnail for your file'] = 'Angiv miniaturebillede til din fil';
$lang['The Configuration tab allows you to customize the plugin in two ways:'] = 'Fanebladet Opsætning gør det muligt at skræddersy plugin\'en på to måder:';
$lang['The Upload tab allows you to upload "non picture" files to your gallerie. Fill in the form to upload and synchronize your file.'] = 'Fanebladet Upload gør det muligt at uploade andre filer end billeder til dit galleri. Udfyld formularen og synkroniser din fil.';
$lang['The thumbnail must be a picture'] = 'Miniaturebilledet skal være et billede';
$lang['There have been errors. See below'] = 'Der var fejl. Se nedenfor';
$lang['Thumbnail to upload:'] = 'Miniaturebillede der skal uploades:';
$lang['Title:'] = 'Titel:';
$lang['Type of file not supported'] = 'Filtype er ikke understøttet';
$lang['Unable to create folder '] = 'Kunne ikke oprette mappe';
$lang['Upload'] = 'Upload';
$lang['Upload error'] = 'Fejl ved upload';
$lang['You can use a personnal image or convert your file to an image using a desktop program or a web service such as'] = 'Du kan anvende et personligt billede eller konvertere din fil til et billede ved hjælp af et program på din computer eller via en webtjeneste så som';
$lang['... or '] = '... eller';
$lang[': Check to overwrite files during upload if a file with the same name already exists.'] = ': Sæt flueben for at overskrive filer ved upload, hvis en fil med det samme navn allerede findes.';
$lang['Album:'] = 'Album:';
$lang['Can\'t upload file to galleries directory'] = 'Kan ikke kopiere filen til galleri-mappen';
$lang['Choose a file'] = 'Vælg en fil';
$lang['Configuration'] = 'Opsætning';
$lang['Donate'] = 'Donér';
$lang['Failed to write file to disk'] = 'Kunne ikke skrive til disken';
$lang['File Uploader Plugin'] = 'File Uploader Plugin';
$lang['File exceeds the upload_max_filesize directive in php.ini'] = 'Filen overskrider upload_max_filesize-indstillingen i php.ini';
$lang['File to upload:'] = 'Fil der skal uploades:';
$lang['File upload stopped by extension'] = 'Filupload stoppet af udvidelse';
$lang['File uploaded and synchronized'] = 'Fil uploadet og synkroniseret';
$lang['File was only partially uploaded'] = 'Filen blev kun delvist uploadet';
$lang['File with this name already exists. You should rename this file.'] = 'Der findes allerede en fil med dette navn. Du bør omdøbe filen.';
$lang['Help'] = 'Hjælp';
$lang['Invalid file name'] = 'Ugyldigt filnavn';
$lang['Missing a temporary folder'] = 'Mangler en midlertidig mappe';
$lang[': Check to open files in a new tab or download them depending on the extension.'] = ': Sæt flueben for at åbne filer i et nyt faneblad eller for at downloade dem, afhængigt af udvidelsen.';
$lang['file_uploader_pdf2tab_hint_text'] = 'Angiv en udvidelse';
$lang['file_uploader_pdf2tab_supported_extensions_description'] = 'Angiv de udvidelser, som PDF2Tab skal håndtere. Udvidelsernes navne angives uden hensyntagen til små og store bogstaver.';
$lang['file_uploader_error_already_exist'] = 'En fil med dette navn findes allerede. Du bør ændre filens navn. Hvis du ønsker at overskrive filen, skal du i stedet ændre opsætningen på opsætningsfanebladet.';
$lang['file_uploader_upload_max_filesize'] = 'Din server har en maksimal filstørrelse på ';
?>