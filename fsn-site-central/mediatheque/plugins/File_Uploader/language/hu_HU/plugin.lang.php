<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2012 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Open files in a new tab'] = 'Fájlok megnyitása új fülön';
$lang['File exceeds the upload_max_filesize directive in php.ini'] = 'A fájl meghaladja a php.ini-ben megadott upload_max_filesize értéket';
$lang['File was only partially uploaded'] = 'A fájl csak részben került feltöltésre';
$lang['Piwigo is a free software (opensource software), you can use it for free and this is not going to change. Donations are collected by the Piwigo Foundation (non profit organization) to cover project fees.'] = 'A Piwigo egy free szoftver (opensource szoftver), melyet ingyenesen használhat és ez nem fog változni. Az adományok a Piwigo Foundation (non profit szervezet) projekt költségeinek fedezetére fordítódnak.';
$lang['Unable to create folder '] = 'Nem sikerült létrehozni a mappát';
$lang['Can\'t upload file to galleries directory'] = 'Nem lehet a fájlt a galéria könyvtárba másolni';
$lang['Overwrite files without notice'] = 'Fájlok felülírása figyelmeztetés nélkül';
$lang['Specify a file to upload'] = 'Adja meg a feltöltendő fájlt';
$lang['Specify a thumbnail for your file'] = 'Adja meg a bélyegkép fájlt';
$lang['Type of file not supported'] = 'A fájltípus nem támogatott';
$lang['file_uploader_pdf2tab_tooltip'] = 'Nyissa meg a fájlt egy új fülön';
$lang['There have been errors. See below'] = 'Hibák történtek. Lásd alább';
$lang['File upload stopped by extension'] = 'A fájl feltöltést leállította a bővítmény';
$lang['Missing a temporary folder'] = 'Hiányzik az ideiglenes mappa';
$lang['Thumbnail to upload:'] = 'Bélyegkép feltöltése:';
$lang['create a new album'] = 'új album létrehozása';
$lang['Title:'] = 'Cím:';
$lang['Upload'] = 'Feltöltés';
$lang['Upload error'] = 'Feltöltés hiba';
$lang['Properties'] = 'Tulajdonságok';
$lang['... or '] = '... vagy ';
$lang['Album:'] = 'Album:';
$lang['Choose a file'] = 'Fájl kiválasztása';
$lang['Choose a thumbnail'] = 'Bélyegkép kiválasztása';
$lang['Configuration'] = 'Beállítások';
$lang['Donate'] = 'Adomány';
$lang['File Uploader Plugin'] = 'Fájl feltöltő bővítmény';
$lang['File to upload:'] = 'Fájl feltöltése:';
$lang['File uploaded and synchronized'] = 'Fájl feltöltve és szinkronizálva';
$lang['Help'] = 'Súgó';
$lang['Invalid file name'] = 'Érvénytelen fájlnév';
$lang['You can use this plugin with the plugin <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> to add an icon to your "non picture" files.'] = 'A bővítmény alkalmazásakor a "nem kép" alakú fájlokhoz hozzárendelhet ikont a <a href="http://hu.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> bővítmény használatával is.';
$lang[': Check to open files in a new tab or download them depending on the extension.'] = ': Ellenőrizheti a fájlokat egy új fülön, vagy letöltheti őket a kiterjesztéstől függően.';
$lang[': Check to overwrite files during upload if a file with the same name already exists.'] = ': Feltöltéskor felülírja a fájlokat ellenőrzés nélkül amennyiben már létezik fájl ugyanazzal a névvel.';
$lang['The Configuration tab allows you to customize the plugin in two ways:'] = 'A beállítások fülön lehetőség nyílik a bővítmény testreszabására,melynek két módja van:';
$lang['Failed to write file to disk'] = 'Nem sikerült felírni a fájlt a lemezre';
$lang['No file to upload'] = 'Nem lehet a fájlt feltölteni';
$lang['The Upload tab allows you to upload "non picture" files to your gallerie. Fill in the form to upload and synchronize your file.'] = 'A feltöltés fül alatt feltölthet "nem kép" alakú fájlokat a galériába. Töltse fel, majd szinkronizálja a fájlt.';
$lang['The thumbnail must be a picture'] = 'A bélyegképhez szükség van egy képre';
$lang['You can use a personnal image or convert your file to an image using a desktop program or a web service such as'] = 'Használhat saját képet, vagy konvertáljon egy kép fájlt egy asztali alkalmazással, vagy webes szolgáltatással, mint pld.';
$lang['file_uploader_error_already_exist'] = 'Létező fájlnév, nevezze át a fájlt. Amennyiben felül kívánja írni a fájlt, módosítsa a beállításokat a beállítások fülön.';
$lang['file_uploader_pdf2tab_hint_text'] = 'Írja be a kiterjesztést';
$lang['file_uploader_pdf2tab_supported_extensions_description'] = 'Írja be a kiterjesztést, melyet megnyithatnak új lapon, vagy letölthetnek. A kiterjesztés lehet kis- és nagybetű.';
$lang['file_uploader_upload_max_filesize'] = 'A kiszolgáló által engedélyezett max. fájlméret';
?>