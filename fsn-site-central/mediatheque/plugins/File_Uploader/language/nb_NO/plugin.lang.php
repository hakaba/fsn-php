<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['file_uploader_pdf2tab_hint_text'] = 'Skriv inn et filnavn';
$lang['file_uploader_pdf2tab_supported_extensions_description'] = 'Skriv inn filnavnet og programtillegget vil åpne i en nye fane eller laste ned. Filnavnet består av små bokstaver.';
$lang['file_uploader_pdf2tab_tooltip'] = 'Åpne filen i en ny fane';
$lang['file_uploader_upload_max_filesize'] = 'Din server er innstilt til maksimal filstørrelse av';
$lang['Open files in a new tab'] = 'Åpne filer i en ny fane';
$lang['Overwrite files without notice'] = 'Overskrive filer uten varsel';
$lang['Piwigo is a free software (opensource software), you can use it for free and this is not going to change. Donations are collected by the Piwigo Foundation (non profit organization) to cover project fees.'] = 'Piwigo er en gratis programvare (opensource programvare), du kan bruke det gratis, og dette kommer ikke til å forandre seg. Donasjoner er samlet inn av Piwigo Foundation (non profit organisasjon) for å dekke prosjektkostnader.';
$lang['Properties'] = 'Egenskaper';
$lang['Specify a file to upload'] = 'Angi en fil som skal lastes opp';
$lang['Specify a thumbnail for your file'] = 'Angi et miniatyrbilde for filen';
$lang['The Configuration tab allows you to customize the plugin in two ways:'] = 'Kategorien konfigurasjon lar deg tilpasse programtillegget på to måter:';
$lang['The Upload tab allows you to upload "non picture" files to your gallerie. Fill in the form to upload and synchronize your file.'] = 'Opplastings fanen lar deg laste opp " ikke bilde" filer til galleriet. Fyll ut skjemaet for å laste opp og synkronisere filene.';
$lang['The thumbnail must be a picture'] = 'Miniatyrbildet må være et bilde';
$lang['There have been errors. See below'] = 'Det har oppstått en feil. Se nedenfor';
$lang['Thumbnail to upload:'] = 'Miniatyrbilde som skal lastes opp';
$lang['Title:'] = 'Tittel:';
$lang['Type of file not supported'] = 'Filtypen støttes ikke';
$lang['Unable to create folder '] = 'Kan ikke opprette mappe ';
$lang['Upload'] = 'Opplasting';
$lang['Upload error'] = 'Opplastings feil';
$lang['You can use a personnal image or convert your file to an image using a desktop program or a web service such as'] = 'Du kan bruke et personelig bilde eller konvertere filen din til et bilde ved hjelp av et desktop program eller en web-tjeneste som';
$lang['You can use this plugin with the plugin <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> to add an icon to your "non picture" files.'] = 'Du kan bruke dette programtillegget med dette programtillegget <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> å legge til et ikon til "ikke bilde" filer.';
$lang['create a new album'] = 'opprette et nytt album';
$lang['file_uploader_error_already_exist'] = 'Fil med samme navn finnes allerede. Du bør endre navnet på denne filen. Hvis du ønsker å overskrive filen, klikk fanen konfigurasjon for å endre konfigurasjonen.';
$lang['... or '] = '...eller';
$lang[': Check to open files in a new tab or download them depending on the extension.'] = ': Sjekk for å åpne filer i en ny fane eller laste dem ned, avhengig av filtypen.';
$lang[': Check to overwrite files during upload if a file with the same name already exists.'] = ': Sjekk for å overskrive filer under opplasting hvis en fil med det samme navnet finnes allerede.';
$lang['Album:'] = 'Album:';
$lang['Can\'t upload file to galleries directory'] = 'Kan ikke laste opp filen til galleri katalogen';
$lang['Choose a file'] = 'Velg en fil';
$lang['Choose a thumbnail'] = 'Velg et miniatyrbilde';
$lang['Configuration'] = 'Konfigurasjon';
$lang['Donate'] = 'Donere';
$lang['Failed to write file to disk'] = 'Klarte ikke å skrive filen til disk';
$lang['File Uploader Plugin'] = 'Programtillegget fil opplaster';
$lang['File exceeds the upload_max_filesize directive in php.ini'] = 'Fila er større enn upload_max_filesize direktivet i php.ini';
$lang['File to upload:'] = 'Fil som skal lastes opp:';
$lang['File upload stopped by extension'] = 'Filopplasting stoppet av utvidelse';
$lang['File uploaded and synchronized'] = 'Fil lastet opp og synkronisert';
$lang['File was only partially uploaded'] = 'Filen ble kun delvis opplastet';
$lang['Help'] = 'Hjelp';
$lang['Invalid file name'] = 'Ugyldig filnavn';
$lang['Missing a temporary folder'] = 'Mangler en midlertidig mappe';
$lang['No file to upload'] = 'Ingen fil som skal lastes opp';