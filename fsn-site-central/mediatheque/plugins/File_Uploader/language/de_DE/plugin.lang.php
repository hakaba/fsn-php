<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2012 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['create a new album'] = 'Erstelle ein neues Album';
$lang['No file to upload'] = 'Keine Datei zum hochladen';
$lang['Open files in a new tab'] = 'Öffne Datei in neuem TAB';
$lang['file_uploader_pdf2tab_tooltip'] = 'Öffne die Datei in einem neuen TAB';
$lang['Overwrite files without notice'] = 'Überschreibe Datei ohne Warnhinweis';
$lang['Piwigo is a free software (opensource software), you can use it for free and this is not going to change. Donations are collected by the Piwigo Foundation (non profit organization) to cover project fees.'] = 'Piwigo ist eine freie Software (Open Source), Du kannst sie kostenlos benutzen. Piwigo wird stets kostenlos bleiben. Spenden werden von der Piwigo Foundation (nicht kommerzielle Organisation) verwendet um Projektkosten zu decken.';
$lang['Properties'] = 'Optionen';
$lang['Specify a file to upload'] = 'Wähle eine Datei zum Upload';
$lang['Specify a thumbnail for your file'] = 'Wähle ein Vorschaubild für Deine Datei';
$lang['The Configuration tab allows you to customize the plugin in two ways:'] = 'Der TAB - Konfiguration erlaubt zwei Wege das Plugin anzupassen:';
$lang['The Upload tab allows you to upload "non picture" files to your gallerie. Fill in the form to upload and synchronize your file.'] = 'Der TAB - Upload erlaubt dir Dateien in deine Galerie zu laden die keine Bilder sind. Fülle das Formular aus um deine Datei hozuladen und zu synchronisieren.';
$lang['The thumbnail must be a picture'] = 'Das Vorschaubild muss ein Bild sein';
$lang['There have been errors. See below'] = 'Es wurden Fehler gemeldet! Siehe unten';
$lang['Thumbnail to upload:'] = 'Vorschaubild hochladen:';
$lang['Title:'] = 'Titel:';
$lang['Type of file not supported'] = 'Dateityp wird nicht unterstützt';
$lang['Unable to create folder '] = 'Kann Ordner nicht erstellen';
$lang['Upload'] = 'Hochladen';
$lang['Upload error'] = 'Upload Fehler';
$lang['You can use a personnal image or convert your file to an image using a desktop program or a web service such as'] = 'Du kannst ein eigenes Bild wählen oder deine Datei mit einem Desktop-Programm in ein Bild konvertieren oder einen Webdienst verwenden wie';
$lang['You can use this plugin with the plugin <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> to add an icon to your "non picture" files.'] = 'Du kannst mit dem Plugin <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> ein Symbol zu deinen Dateien hinzuzufügen, die keine Bilddateien sind.';
$lang['... or '] = '... oder';
$lang[': Check to overwrite files during upload if a file with the same name already exists.'] = ': Auswählen um Dateien zu überschreiben falls sie schon mit gleichen Namen existieren.';
$lang['Album:'] = 'Album:';
$lang['Can\'t upload file to galleries directory'] = 'Kann Datei nicht in Galerie Ordner kopieren';
$lang['Choose a file'] = 'Wähle eine Datei';
$lang['Choose a thumbnail'] = 'Wähle eine Vorschaubild';
$lang['Configuration'] = 'Konfiguration';
$lang['Donate'] = 'Spende';
$lang['Failed to write file to disk'] = 'Konnte Datei nicht schreiben';
$lang['File Uploader Plugin'] = 'Datei Upload Plugin';
$lang['File exceeds the upload_max_filesize directive in php.ini'] = 'Datei überschreitet die upload_max_filesize Einstellung in php.ini';
$lang['File to upload:'] = 'Datei zum Upload:';
$lang['File upload stopped by extension'] = 'Datei upload gestoppt wegen Dateierweiterung';
$lang['File uploaded and synchronized'] = 'Datei hochgeladen und synchronisiert';
$lang['File was only partially uploaded'] = 'Datei wurde nur teilweise hochgeladen';
$lang['File with this name already exists. You should rename this file.'] = 'Datei mit gleichen Namen bereits vorhanden. Du solltest einen anderen Namen verwenden.';
$lang['Help'] = 'Hilfe';
$lang['Invalid file name'] = 'Ungültiger Dateiname';
$lang['Missing a temporary folder'] = 'Fehlender temporärer Ordner';
$lang[': Check to open files in a new tab or download them depending on the extension.'] = 'Öffne Dateien in einem neuen Tab oder lade diese herunter, abhängig von der installierten Erweiterung';
$lang['file_uploader_pdf2tab_hint_text'] = 'Gib eine Dateierweiterung ein';
$lang['file_uploader_pdf2tab_supported_extensions_description'] = 'Ging eine Dateierweiterung an die PDF2Tab verwenden soll. Bei der Dateierweiterung wird die Groß-/Kleinschreibung beachtet!';
$lang['file_uploader_error_already_exist'] = 'Eine Datei mit dem Namen existiert bereits. Sie sollten die Datei umbenennen. Falls sie die Datei überschreiben wollen, müssen Sie die Konfiguration im Konfigurations-Tab ändern.';
$lang['file_uploader_upload_max_filesize'] = 'Ihr Server erlaubt eine maximale File Größe von';
?>