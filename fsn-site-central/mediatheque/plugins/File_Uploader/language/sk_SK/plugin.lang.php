<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Piwigo is a free software (opensource software), you can use it for free and this is not going to change. Donations are collected by the Piwigo Foundation (non profit organization) to cover project fees.'] = 'Piwigo je bezplatný softvér (opensource softvér), môžete ho použiť zadarmo bez zmeny. Dary sú zaisťované Piwigo Foundation (nezisková organizácia) na krytie poplatkov projektu.';
$lang['The Upload tab allows you to upload "non picture" files to your gallerie. Fill in the form to upload and synchronize your file.'] = 'Karta Nahratia vám umožní nahrať "neobrázkové" súbory do galérie. Vyplňte formulár nahrávania a synchronizujte súbor.';
$lang['You can use a personnal image or convert your file to an image using a desktop program or a web service such as'] = 'Môžete použiť osobný obrázok alebo previesť súbor do snímky pomocou programu alebo webovú službu ako';
$lang['You can use this plugin with the plugin <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> to add an icon to your "non picture" files.'] = 'Tento doplnok môžete použiť s doplnkom  <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> na pridanie ikony pre "neobrázkové" súbory.';
$lang['file_uploader_pdf2tab_supported_extensions_description'] = 'Zadajte prípony doplnku na ​​novej karte alebo stiahnute. Prípony sú malé a veľké písmená.';
$lang['file_uploader_error_already_exist'] = 'Súbor s týmto názvom už existuje. Mali by ste súbor premenovať. Ak chcete súbor prepísať, zmeňte konfiguráciu v záložke konfigurácie.';
$lang['file_uploader_pdf2tab_hint_text'] = 'Zadajte príponu';
$lang['The Configuration tab allows you to customize the plugin in two ways:'] = 'Karta Konfigurácie umožňuje prispôsobiť doplnok dvoma spôsobmi:';
$lang['The thumbnail must be a picture'] = 'Náhľad musí byť obrázok';
$lang['Properties'] = 'Vlastnosti';
$lang['There have been errors. See below'] = 'Boli nájdené chyby. Pozrite nižšie';
$lang['Thumbnail to upload:'] = 'Náhľad na nahratie:';
$lang['create a new album'] = 'vytvoriť nový album';
$lang['file_uploader_pdf2tab_tooltip'] = 'Otvoriť súbor v novom okne';
$lang['file_uploader_upload_max_filesize'] = 'Váš server uloží maximálne veľký súbor';
$lang['Type of file not supported'] = 'Typ súboru nie je podporovaný';
$lang['Unable to create folder '] = 'Nemožno vytvoriť adresár';
$lang['Upload error'] = 'Chyba nahratia';
$lang['Upload'] = 'Nahratie';
$lang['Title:'] = 'Názov:';
$lang['Specify a thumbnail for your file'] = 'Označiť náhľad pre Váš súbor';
$lang['Specify a file to upload'] = 'Označiť súbor na nahratie';
$lang['Overwrite files without notice'] = 'Prepísať súbory bez oznamov';
$lang['Open files in a new tab'] = 'Otvoriť súbory v novom okne';
$lang['No file to upload'] = 'Žiaden súbor na nahratie';
$lang['Missing a temporary folder'] = 'Chýbajúci dočasný adresár';
$lang['Invalid file name'] = 'nesprávne meno súboru';
$lang['Help'] = 'Pomoc';
$lang['File was only partially uploaded'] = 'Súbor bol len čiastočne nahratý';
$lang['File uploaded and synchronized'] = 'Súbor nahratý a synchronizovaný';
$lang['File upload stopped by extension'] = 'Nahrávanie súboru zastavilo rozšírenie';
$lang['File to upload:'] = 'Súbor na nahratie:';
$lang['File exceeds the upload_max_filesize directive in php.ini'] = 'Súbor presahuje upload_max_filesize uvedeného v php.ini';
$lang['File Uploader Plugin'] = 'Doplnok nahrávania súboru';
$lang['Failed to write file to disk'] = 'Chyba pri zapisovaní súboru na disk';
$lang['Donate'] = 'Darovať';
$lang['Configuration'] = 'Nastavenia';
$lang['Choose a thumbnail'] = 'Vybrať náhľad';
$lang['Choose a file'] = 'Vybrať súbor';
$lang['Can\'t upload file to galleries directory'] = 'Nemožno nahrať súbor do adresára galérií.';
$lang['Album:'] = 'Album:';
$lang[': Check to overwrite files during upload if a file with the same name already exists.'] = ':Skontrolovať prepisovanie súborov počas nahrávania, ak už existuje súbor s rovnakým názvom.';
$lang[': Check to open files in a new tab or download them depending on the extension.'] = ':Otvoriť súbory v novej záložke alebo si ich stiahnuť v závislosti na rozšírení.';
$lang['... or '] = '... alebo';
?>