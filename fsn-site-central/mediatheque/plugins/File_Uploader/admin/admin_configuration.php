<?php
//Initialization
$conf_file_uploader = unserialize($conf['file_uploader']);

//Save config
if (isset($_POST['submit'])) { 
	//New parameters	
	foreach ($conf_file_uploader as $file_uploader_checkbox => $value) {
		if(isset($_POST['file_uploader_checkbox'][$file_uploader_checkbox]) && ($_POST['file_uploader_checkbox'][$file_uploader_checkbox] == 1))
			$conf_file_uploader[$file_uploader_checkbox] = 1;
		else
			$conf_file_uploader[$file_uploader_checkbox] = 0;
	}
	
	$conf_file_uploader['pdf2tab_extensions'] = $_POST['file_uploader_pdf2tab_extensions'];
	$conf_file_uploader['pdf2tab_extensions'] = explode(',', $conf_file_uploader['pdf2tab_extensions']);
	
	//Save
	conf_update_param('file_uploader', serialize($conf_file_uploader));
	array_push($page['infos'], l10n('Information data registered in database'));
}

//Parameters of the template
foreach ($conf_file_uploader as $file_uploader_key => $value)
	$file_uploader_checked[$file_uploader_key] = (($conf_file_uploader[$file_uploader_key] == 1) ? 'checked = "checked"' : '');
$template->assign('file_uploader_checked', $file_uploader_checked);

if ($conf_file_uploader['pdf2tab_extensions'][0]=='')
		$conf_file_uploader['pdf2tab_extensions'] = null;
$template->assign('file_uploader_pdf2tab_extensions',$conf_file_uploader['pdf2tab_extensions']);

//Add our template to the global template
$template->set_filenames(
	array(
		'plugin_admin_content_general' => dirname(__FILE__).'/admin_configuration.tpl'
	)
);
 
//Assign the template contents to ADMIN_CONTENT
$template->assign_var_from_handle('ADMIN_CONTENT', 'plugin_admin_content_general');
?>