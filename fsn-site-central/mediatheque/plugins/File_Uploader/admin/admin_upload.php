<?php
if (!defined('PHPWG_ROOT_PATH')) die('Hacking attempt!');

include_once(PHPWG_ROOT_PATH . 'include/functions.inc.php');
include_once(PHPWG_ROOT_PATH . 'admin/include/functions_upload.inc.php');

global $template, $conf, $user, $page;

if (isset($_POST['submit'])) {
	$file_uploader_errors = array();
	$file_uploader_upload_file = array();

	if($_FILES['file_uploader_file']['size'] != 0) {
		$file_uploader_upload_file = file_uploader_upload_file($_FILES['file_uploader_file']);
		if(count($file_uploader_upload_file['errors']) != 0)
			$file_uploader_errors['file'] = $file_uploader_upload_file['errors'];
	} else {
		$file_uploader_errors['file']['no_file'] = l10n('Specify a file to upload');
	}
	
	if($_FILES['file_uploader_thumbnail']['size'] != 0 && $_FILES['file_uploader_file']['size'] != 0) {
		$file_uploader_upload_thumbnail = file_uploader_upload_thumbnail($_FILES['file_uploader_thumbnail'], $file_uploader_upload_file['name_wo_extension'], $file_uploader_upload_file['extension'], $file_uploader_upload_file['folder']);
		if(count($file_uploader_upload_thumbnail['errors']) != 0)
			$file_uploader_errors['thumbnail'] = $file_uploader_upload_thumbnail['errors'];
	} else {
		$file_uploader_errors['thumbnail']['no_file'] = 'Specify a thumbnail for your file';
	}
	
	if (count($file_uploader_errors) == 0) {
		file_uploader_synchronize($file_uploader_upload_file, $_POST['file_uploader'], $file_uploader_upload_thumbnail);
		array_push($page['infos'], l10n('File uploaded and synchronized'));
	} else {
		array_push($page['errors'], l10n('There have been errors. See below'));
		$template->assign('file_uploader_errors', $file_uploader_errors);
		$template->assign('file_uploader', $_POST['file_uploader']);
	}
}

function file_uploader_upload_file($file_uploader_file) {
	require(FILE_UPLOADER_PATH.'install/config_default.inc.php');
	require_once(FILE_UPLOADER_PATH.'install/functions.inc.php');
	
	global $conf;
	$conf_file_uploader = unserialize($conf['file_uploader']);
	
	$file_uploader_file_tmp = $file_uploader_file['tmp_name'];
	$file_uploader_file_title = substr($file_uploader_file['name'], 0, strrpos($file_uploader_file['name'], '.'));
	$file_uploader_file_name = preg_replace('/[^a-zA-Z0-9s.]/', '_', $file_uploader_file['name']);
	$file_uploader_file_name_wo_extension = substr($file_uploader_file_name, 0, strrpos($file_uploader_file_name, '.'));
	$file_uploader_file_extension = strtolower(substr(strrchr($file_uploader_file['name'], '.'),1));
	$file_uploader_file_size = filesize($file_uploader_file['tmp_name']);
	if (isset($file_uploader_destination_folder[$file_uploader_file_extension])) {
		$file_uploader_file_category = $file_uploader_destination_folder[$file_uploader_file_extension];
		$file_uploader_file_directory_full = $file_uploader_galleries_dir.$file_uploader_file_category;
	} else {
		$file_uploader_file_category = $file_uploader_destination_folder['others'];
		$file_uploader_file_directory_full = $file_uploader_galleries_dir.$file_uploader_file_category;
	}
	$file_uploader_file_destination = $file_uploader_file_directory_full.'/'.$file_uploader_file_name;
	$file_uploader_errors = array();
	$return = array();
	
	$return['title'] = $file_uploader_file_title;
	$return['name_wo_extension'] = $file_uploader_file_name_wo_extension;
	$return['extension'] = $file_uploader_file_extension;
	$return['folder'] = $file_uploader_file_category;
	$return['destination'] = $file_uploader_file_destination;
	$return['size'] = $file_uploader_file_size;
	$return['overwrite'] = false;
	
	//Check if the folder file_uploader exists
	file_uploader_folder($file_uploader_galleries_folder, $file_uploader_galleries_dir, $file_uploader_galleries_folder_name);
	
	//If the directory does not exist, we create it
	if (!file_exists($file_uploader_file_directory_full))
		if(!mkdir($file_uploader_file_directory_full))
			$file_uploader_errors['upload_error'] = l10n('Unable to create folder ').$file_uploader_file_directory_full;
		//If the physical category si not in database, we add it
		else if (pwg_db_num_rows(pwg_query('SELECT id FROM '.CATEGORIES_TABLE.' WHERE name = "'.$file_uploader_file_category.'";')) == 0){
			$next_id = pwg_db_nextval('id', CATEGORIES_TABLE);
			$category_rank = pwg_db_fetch_assoc(pwg_query('SELECT MAX(rank) FROM '.CATEGORIES_TABLE.';'));
			$category_rank = $category_rank['MAX(rank)'] + 1;
			$file_uploader_physical_category = pwg_db_fetch_assoc(pwg_query('SELECT id FROM '.CATEGORIES_TABLE.' WHERE dir = "'.$file_uploader_galleries_folder.'";'));
			$insert = array(
				'id' => $next_id,
				'name' => $file_uploader_file_category,
				'id_uppercat' => $file_uploader_physical_category['id'],
				'comment' => 'Created by the File Uploader plugin',
				'dir' => $file_uploader_file_category,
				'rank' => $category_rank,
				'status' => 'private',
				'visible' => boolean_to_string($conf['newcat_default_visible']),
				'uppercats' => $next_id.','.$file_uploader_physical_category['id'],
				'global_rank' => $category_rank,
				'site_id' => '1',
			);
			single_insert(CATEGORIES_TABLE, $insert);
		}
	
	if ($file_uploader_file['error'] !== UPLOAD_ERR_OK) {
		switch ($_FILES['file_uploader_file']['error']) {
			case UPLOAD_ERR_INI_SIZE:
				$file_uploader_errors['upload_error'] = l10n('File exceeds the upload_max_filesize directive in php.ini');
				break;
			case UPLOAD_ERR_PARTIAL:
				$file_uploader_errors['upload_error'] = l10n('File was only partially uploaded');
				break;
			case UPLOAD_ERR_NO_FILE:
				$file_uploader_errors['upload_error'] = l10n('No file to upload');
				break;
			case UPLOAD_ERR_NO_TMP_DIR:
				$file_uploader_errors['upload_error'] = l10n('Missing a temporary folder');
				break;
			case UPLOAD_ERR_CANT_WRITE:
				$file_uploader_errors['upload_error'] = l10n('Failed to write file to disk');
				break;
			case UPLOAD_ERR_EXTENSION:
				$file_uploader_errors['upload_error'] = l10n('File upload stopped by extension');
				break;
			default:
				$file_uploader_errors['upload_error'] = l10n('Upload error');
		}
	} else if (file_exists($file_uploader_file_destination)) {
		if ($conf_file_uploader['overwrite'] == 1)
			$return['overwrite'] = true;
		else
			$file_uploader_errors['already_exist'] = l10n('file_uploader_error_already_exist');
	} else if (!move_uploaded_file($file_uploader_file_tmp, $file_uploader_file_destination)) {
		$file_uploader_errors['move_uploaded_file'] = l10n('Can\'t upload file to galleries directory');
	}
	
	$return['errors'] = $file_uploader_errors;
	
	return $return;
}


function file_uploader_upload_thumbnail($file_uploader_thumbnail, $file_uploader_file_name_wo_extension, $file_uploader_file_extension, $file_uploader_file_folder) {
	require(FILE_UPLOADER_PATH.'install/config_default.inc.php');
	
	$file_uploader_thumbnail_tmp = $file_uploader_thumbnail['tmp_name'];
	$file_uploader_thumbnail_extension = strtolower(substr(strrchr($file_uploader_thumbnail['name'], '.'),1));
	$file_uploader_thumbnail_size = filesize($file_uploader_thumbnail['tmp_name']);
	$file_uploader_thumbnail_directory = $file_uploader_galleries_dir.$file_uploader_file_folder.'/pwg_representative';
	$file_uploader_thumbnail_destination = $file_uploader_thumbnail_directory.'/'.$file_uploader_file_name_wo_extension.'.'.$file_uploader_thumbnail_extension;
	$file_uploader_errors = array();
	$return = array();
	
	$return['extension'] = $file_uploader_thumbnail_extension;
	
	if (!file_exists($file_uploader_thumbnail_directory))
		if(!mkdir($file_uploader_thumbnail_directory))
			$file_uploader_errors['upload_error'] = l10n('Unable to create folder ').$file_uploader_thumbnail_directory;
	
	if ($file_uploader_thumbnail['error'] !== UPLOAD_ERR_OK) {
		switch ($_FILES['file_uploader_thumbnail']['error']) {
			case UPLOAD_ERR_INI_SIZE:
				$file_uploader_errors['upload_error'] = l10n('File exceeds the upload_max_filesize directive in php.ini');
				break;
			case UPLOAD_ERR_PARTIAL:
				$file_uploader_errors['upload_error'] = l10n('File was only partially uploaded');
				break;
			case UPLOAD_ERR_NO_FILE:
				$file_uploader_errors['upload_error'] = l10n('No file to upload');
				break;
			case UPLOAD_ERR_NO_TMP_DIR:
				$file_uploader_errors['upload_error'] = l10n('Missing a temporary folder');
				break;
			case UPLOAD_ERR_CANT_WRITE:
				$file_uploader_errors['upload_error'] = l10n('Failed to write file to disk');
				break;
			case UPLOAD_ERR_EXTENSION:
				$file_uploader_errors['upload_error'] = l10n('File upload stopped by extension');
				break;
			default:
				$file_uploader_errors['upload_error'] = l10n('Upload error');
		} 
	} else if(!in_array($file_uploader_thumbnail_extension, $file_uploader_allowed_thumbnail_extension)) {
		$file_uploader_errors['filetype'] = l10n('The thumbnail must be a picture');
	} else if (!move_uploaded_file($file_uploader_thumbnail_tmp, $file_uploader_thumbnail_destination)) {
		$file_uploader_errors['move_uploaded_file'] = l10n('Can\'t upload file to galleries directory');
	}
	
	$return['errors'] = $file_uploader_errors;
	
	return $return;
}


function file_uploader_synchronize($file_uploader_file, $file_uploader_file_properties, $file_uploader_thumbnail) {
	global $user;
	
	//Current date
	list($dbnow) = pwg_db_fetch_row(pwg_query('SELECT NOW();'));

	//Database registration
	$file_path = pwg_db_real_escape_string($file_uploader_file['destination']);
	$physical_category = pwg_db_fetch_assoc(pwg_query('SELECT id FROM '.CATEGORIES_TABLE.' WHERE name = "'.$file_uploader_file['folder'].'";'));
	$physical_category_id = $physical_category['id'];
	$insert = array(
		'file' => substr(strrchr($file_path, '/'), 1),
		'name' => ($file_uploader_file_properties['title'] != '') ? pwg_db_real_escape_string($file_uploader_file_properties['title']) : $file_uploader_file['title'],
		'comment' => pwg_db_real_escape_string($file_uploader_file_properties['description']),
		'date_available' => $dbnow,
		'path' => $file_path,
		'representative_ext' => $file_uploader_thumbnail['extension'],
		'filesize' => $file_uploader_file['size'],
		'storage_category_id' => $physical_category_id,
		'md5sum' => md5_file($file_path),
		'added_by' => $user['id'],
	);
	
	if ($file_uploader_file['overwrite']) {
		$query = 'SELECT id FROM '.IMAGES_TABLE.' WHERE path = "'.$file_path.'";';
		$image_id = pwg_db_fetch_assoc(pwg_query($query));
		single_update(IMAGES_TABLE, $insert, array('id' => $image_id['id']));
	} else {
		single_insert(IMAGES_TABLE, $insert);
		$image_id = pwg_db_insert_id(IMAGES_TABLE);
	}
	
	if (!is_array($image_id))
		$image_id = array($image_id);
	
	if(isset($file_uploader_file_properties['category']) and count($file_uploader_file_properties['category']) > 0) {
		@associate_images_to_categories(
			$image_id,
			array($file_uploader_file_properties['category'], $physical_category_id['id'])
		);
	}
	
	@fill_caddie($image_id);
}


//Categories
$query = 'SELECT id,name,uppercats,global_rank FROM '.CATEGORIES_TABLE.';';
display_select_cat_wrapper($query, array(), 'category_parent_options');

//Add parameters to template
$upload_max_filesize = min(get_ini_size('upload_max_filesize'), get_ini_size('post_max_size'));
if ($upload_max_filesize == get_ini_size('upload_max_filesize')) {
	$upload_max_filesize = get_ini_size('upload_max_filesize', true);
} else {
	$upload_max_filesize = get_ini_size('post_max_filesize', true);
}
$upload_max_filesize_display = round($upload_max_filesize/1024, 0);

$template->assign(
    array(
		'upload_max_filesize' => $upload_max_filesize,
		'upload_max_filesize_display' => $upload_max_filesize_display,
    )
);

$template->set_filenames(array('plugin_admin_content' => dirname(__FILE__) . '/admin_upload.tpl')); 
$template->assign_var_from_handle('ADMIN_CONTENT', 'plugin_admin_content');
?>