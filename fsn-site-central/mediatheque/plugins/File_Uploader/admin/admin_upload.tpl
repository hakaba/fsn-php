{include file='include/colorbox.inc.tpl'}
{include file='include/add_album.inc.tpl'}

<div class="titrePage">
  <h2>{'File Uploader Plugin'|@translate}</h2>
</div>
<div class="file_uploader_form">
	<form method="post" enctype="multipart/form-data">
		<fieldset>
			<legend>{'Choose a file'|@translate}</legend>
			<p class="file_uploader_center">
				<label>
					<span class="property">{'File to upload:'|@translate}</span>
				</label>
				<input name="file_uploader_file" type="file" value=""{if isset($file_uploader_errors.file)} class="file_uploader_error"{/if}>
				{foreach from=$file_uploader_errors.file item=error_description}<span class="file_uploader_error_description" title="{$error_description}">!</span>{/foreach}
				<input type="hidden" name="MAX_FILE_SIZE" value="{$upload_max_filesize}">
			</p>
			<p class="file_uploader_center">
				{'file_uploader_upload_max_filesize'|@translate} {$upload_max_filesize_display} Kio.
			</p>
		</fieldset>
		<fieldset>
			<legend>{'Choose a thumbnail'|@translate}</legend>
			<p class="file_uploader_center">
				<label>
					<span class="property">{'Thumbnail to upload:'|@translate}</span>
				</label>
				<input name="file_uploader_thumbnail" type="file" value=""{if isset($file_uploader_errors.thumbnail)} class="file_uploader_error"{/if}>
				{foreach from=$file_uploader_errors.thumbnail item=error_description}<span class="file_uploader_error_description" title="{$error_description}">!</span>{/foreach}
			</p>
			<p class="file_uploader_center">
				{'You can use a personnal image or convert your file to an image using a desktop program or a web service such as'|@translate} <a href="http://www.zamzar.com" alt="Zamzar" title="Zamzar">Zamzar</a>.
			</p>
		</fieldset>
		<fieldset>
			<legend>{'Properties'|@translate}</legend>
			<p class="file_uploader_center">
				<label for="file_uploader_title_input">
					<span class="property">{'Title:'|@translate}</span>
				</label>
				<input size="50" id="file_uploader_title_input" type="text" name="file_uploader[title]" value="{$file_uploader.title}"{if isset($file_uploader_errors.title)} class="file_uploader_error"{/if}>
				{if isset($file_uploader_errors.title)}<span class="file_uploader_error_description" title="{$file_uploader_errors.title}">!</span>{/if}
			</p>
			<p class="file_uploader_center">
				<label for="file_uploader_description_input">
					<span class="property">{'Description:'|@translate}</span>
				</label><br />
				<textarea cols="50" rows="5" id="file_uploader_description_input" type="text" name="file_uploader[description]"{if isset($file_uploader_errors.description)} class="file_uploader_error"{/if}>{$file_uploader.description}</textarea>
				{if isset($file_uploader_errors.description)}<span class="file_uploader_error_description" title="{$file_uploader_errors.description}">!</span>{/if}
			</p>
			<p class="file_uploader_center">
				<label for="file_uploader_album_select">
					<span class="property">{'Album:'|@translate}</span>
				</label>
				<select style="width:400px" name="file_uploader[category]" id="albumSelect" size="1">
					{html_options options=$category_parent_options selected=$file_uploader.category}
				</select>
				<br />{'... or '|@translate}<a href="#" class="addAlbumOpen" title="{'create a new album'|@translate}">{'create a new album'|@translate}</a>
			</p>
			</fieldset>
		<p>
			<input class="submit" name="submit" type="submit" value="{'Submit'|@translate}" />
		</p>
	</form>
</div>