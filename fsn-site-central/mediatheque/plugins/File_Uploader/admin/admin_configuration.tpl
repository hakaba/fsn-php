{include file='include/colorbox.inc.tpl'}

{combine_script id='jquery.tokeninput' load='async' require='jquery' path='themes/default/js/plugins/jquery.tokeninput.js'}
{footer_script require='jquery.tokeninput'}
jQuery(document).ready(function() {ldelim}
	jQuery('select[name="file_uploader_pdf2tab_extensions"]').tokenInput(
		[{foreach from=$extensions item=extension name=extensions}{ldelim}"{$extension|@escape:'javascript'}"{rdelim}{if !$smarty.foreach.extensions.last},{/if}{/foreach}],
    {ldelim}
      hintText: '{'file_uploader_pdf2tab_hint_text'|@translate}',
      noResultsText: '',
      searchingText: '',
      newText: '',
      animateDropdown: false,
      preventDuplicates: true,
      allowCreation: true
    }
  );

  jQuery("a.preview-box").colorbox();
});
{literal}
$(document).ready(function(){
	if ($("#file_uploader_pdf2tab_checkbox").is(":checked")) {
			$("#file_uploader_pdf2tab_supported_extensions").css("display","block");
			$("#file_uploader_pdf2tab_supported_extensions_description").css("display","block");
		} else {
			$("#file_uploader_pdf2tab_supported_extensions").css("display","none");
			$("#file_uploader_pdf2tab_supported_extensions_description").css("display","none");
		}

	$("#file_uploader_pdf2tab_checkbox").click(function(){
		if ($("#file_uploader_pdf2tab_checkbox").is(":checked")) {
			$("#file_uploader_pdf2tab_supported_extensions").show("fast");
			$("#file_uploader_pdf2tab_supported_extensions_description").show("fast");
		} else {
			$("#file_uploader_pdf2tab_supported_extensions").hide("fast");
			$("#file_uploader_pdf2tab_supported_extensions_description").hide("fast");
		}
	});
});
{/literal}
{/footer_script}

<div class="titrePage">
  <h2>{'File Uploader Plugin'|@translate}</h2>
</div>

<form method="post" action="" ENCTYPE="multipart/form-data"> 
	<fieldset>
		<legend>{'Configuration'|@translate}</legend>
		<p>
			<label><input type="checkbox" id="file_uploader_pdf2tab_checkbox" name="file_uploader_checkbox[new_tab]" value="1" {$file_uploader_checked.new_tab}> {'Open files in a new tab'|@translate}</label>
		</p>
		<p id="file_uploader_pdf2tab_supported_extensions_description">
			{'file_uploader_pdf2tab_supported_extensions_description'|@translate}
		</p>
		<p id="file_uploader_pdf2tab_supported_extensions">	
			<select name="file_uploader_pdf2tab_extensions">
				{foreach from=$file_uploader_pdf2tab_extensions item=file_uploader_pdf2tab_extension}
					<option value="{$file_uploader_pdf2tab_extension}" class="selected">{$file_uploader_pdf2tab_extension}</option>
				{/foreach}
			</select>
		</p>
		<p>
			<label><input type="checkbox" name="file_uploader_checkbox[overwrite]" value="1" {$file_uploader_checked.overwrite}> {'Overwrite files without notice'|@translate}</label>
		</p>
	</fieldset>
	<p class="file_uploader_center">
		<input class="submit" type="submit" value="{'Submit'|@translate}" name="submit"/>
	</p>
</form>