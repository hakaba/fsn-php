<?php
// +-----------------------------------------------------------------------+
// | See My Photos plugin for piwigo                                       |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2014 - 2016 ddtddt             http://temmii.com/piwigo/ |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+

global $page, $conf, $user;
$forbidden = get_sql_condition_FandF(
  array( 'visible_images' => 'id' ),
  'AND'
);
$page['section'] = 'see_my_photos';
$page['section_title'] =  '<a href="'.get_absolute_root_url().'">'.l10n('Home').'</a>'.$conf['level_separator']. '<a href="'.get_absolute_root_url().'index.php?/see_my_photos">'.l10n('My photos').'</a>';

$query = '
SELECT DISTINCT(id)
 FROM '.IMAGES_TABLE.'
 INNER JOIN '.IMAGE_CATEGORY_TABLE.' AS ic ON id = ic.image_id
 '.get_sql_condition_FandF
  (
    array
      (
        'forbidden_categories' => 'category_id',
        'visible_categories' => 'category_id',
        'visible_images' => 'id'
      ),
    'WHERE'
  ).'
 AND added_by = \''.$user['id'].'\'
 '.$conf['order_by'].';';
$page = array_merge(
 $page,
  array(
  'title' => '<a href="'.duplicate_index_url(array('start'=>0)).'">'.l10n('See photos by author').'</a>',
  'items' => array_from_query($query, 'id'),
  )
 );
?>