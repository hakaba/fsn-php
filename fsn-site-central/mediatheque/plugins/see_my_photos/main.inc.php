<?php
/*
Plugin Name: See My Photos
Version: 2.8.a
Description: See photos I've added
Plugin URI: http://piwigo.org/ext/extension_view.php?eid=722
Author: ddtddt
Author URI: http://temmii.com/piwigo/
*/
// +-----------------------------------------------------------------------+
// | See My Photos plugin for piwigo                                       |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2014 - 2016 ddtddt             http://temmii.com/piwigo/ |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+

if (!defined('PHPWG_ROOT_PATH')) die('Hacking attempt!');


define('SMP_DIR' , basename(dirname(__FILE__)));
define('SMP_PATH' , PHPWG_PLUGINS_PATH . SMP_DIR . '/');

add_event_handler('blockmanager_apply' , 'add_link_SMP');
add_event_handler('loc_end_section_init', 'section_init_SMP');

add_event_handler('loading_lang', 'see_my_photos_loading_lang');	  
function see_my_photos_loading_lang(){
  load_language('plugin.lang', SMP_PATH);
}


function add_link_SMP($menu_ref_arr){
  global $conf, $user;
  $query = '
  SELECT DISTINCT(id)
  FROM '.IMAGES_TABLE.'
  INNER JOIN '.IMAGE_CATEGORY_TABLE.' AS ic ON id = ic.image_id
    '.get_sql_condition_FandF
  (
    array
      (
        'forbidden_categories' => 'category_id',
        'visible_categories' => 'category_id',
        'visible_images' => 'id'
      ),
    'WHERE'
  ).'
  AND added_by = \''.$user['id'].'\';';
  $result = pwg_query($query);
  $row = pwg_db_fetch_assoc($result);
  $nbp=count(array_from_query($query, 'id'));
  if (!is_a_guest() and !empty($row)){ 
    $menu = & $menu_ref_arr[0];
    if (($block = $menu->get_block('mbSpecials')) != null){
	  $position = (isset($conf['SMP_position']) and is_numeric($conf['SMP_position'])) ? $conf['SMP_position'] : count($block->data)+1;
      array_splice($block->data, $position-1, 0, array('see_my_photos' =>
       array(
        'URL' => make_index_url(array('section' => 'see_my_photos')),
        'TITLE' => l10n('My photos'),
        'NAME' => l10n('My photos').' ('.$nbp.')',
      )));
    }
  }
}

function section_init_SMP(){
  global $tokens;
  if (in_array('see_my_photos', $tokens))
    include(SMP_PATH . 'pagessmp.php');
}
?>