<?php

global $_zp_conf_vars;

$root = $_SERVER['DOCUMENT_ROOT'];
require $root.'/library/Spyc/Spyc.php';

// FSN
$dir_config = $root."/config/";
$file_config = $dir_config.'application.yaml';

$ini_array = Spyc::YAMLLoad($file_config);

$conf['dblayer'] = 'mysqli';
$conf['db_base'] = $ini_array['db.schema'];
$conf['db_user'] = $ini_array['db.login'];
$conf['db_password'] = $ini_array['db.password'];
$conf['db_host'] = $ini_array['db.host'];

$prefixeTable = 'zpwg_';

define('PHPWG_INSTALLED', true);
define('PWG_CHARSET', 'utf-8');
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');

?>