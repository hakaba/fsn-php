	function init() {
		var rects = document.querySelectorAll('rect');
		var texts = document.querySelectorAll('text');
		clickSVG(rects);
		clickSVG(texts);
	}
	
	function clickSVG(objs) {
		for (var i = 0; i < objs.length; i++) {
			var obj = objs[i];
			if (obj.addEventListener) {
				obj.addEventListener('click', show, false)
			} else if (obj.attachEvent) {
				obj.attachEvent('onclick', show);
			} else {
				alert('Erreur : click.');
			}
		}
	}
	
	function show(e) {
		var target = e.target;
		var name = target.getAttribute('name');
		if (name == null)
			name = target.textContent;
		//ici pour coder l'appel au détail d'une US.
		//alert(name);
		showUS(name);
	}
	
	function showUS(name) {
		var baseUrl = document.getElementById("baseUrl").value;
		var url = 'http://'+window.location.hostname+baseUrl+'/Stratifiant/showUS?identification='+name;
		//window.location = url;
		window.open(url,"_blank");
	}
	
	//Lancement automatique au chargement de la page.
	//window.addEventListener('load', function load(event) {
		//window.removeEventListener('load', load, false);
		//init();
	//}, false);