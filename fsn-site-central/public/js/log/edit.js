/***********************************************
 *  Author: Joseph Kalule                      *
 *  Date: 06/01/2017                           *
 *  Used for: /log/edit/                       *
 ***********************************************/

$(function() {
	getListeCouches();
});

function getListeCouches() {
	$.ajax({
		url: $("#baseURLfield").val()+"/Couchelog/index",
		type: "POST",
		data: "log_id="+$("#id_log").val()
	}).done(function(response) {
		$("#Couchelogs").html(response);
		addListenersToCoucheForm();
	});
}

function addListenersToCoucheForm() {
	$("#ajouterCouchelogALog").click(function() {
		getNewCoucheForm();
	});
	
	$(".glyphicon-edit").click(function(e) {
		//alert("href = "+$(e.target).parent().attr('href'));
		getEditCoucheForm($(e.target).parent().attr('href'));
		e.preventDefault();
	});
}

var couchesHTML;

function getNewCoucheForm() {
	couchesHTML = $("#Couchelogs").html();
	$.ajax({
		url: $("#baseURLfield").val()+"/Couchelog/add",
		type:"POST",
		data:"log_id="+$("#id_log").val()
	}).done(function(response) {
		$("#modalBody").html(response);
		$("#modalTitle").text("Ajout Couche");
		$("#myModal").modal();
		addListenersToNewCoucheForm();
	});
}

function getEditCoucheForm(editUrl) {
	couchesHTML = $("#Couchelogs").html();
	$.ajax({
		url:editUrl,
		type:"POST"
	}).done(function(response) {
		$("#modalBody").html(response);
		$("#modalTitle").text("Edition Couche");
		$("#myModal").modal();
		addListenersToEditCoucheForm();
	});
}

function addListenersToEditCoucheForm() {
	$("#confirmerEditCouche").click(function(e) {
		//alert("form data = "+$("form[name='form_couche']").serialize());
		$.ajax({
			type: "POST",
			url: $("#baseURLfield").val()+"/Couchelog/edit",
			data: $("form[name='form_couche']").serialize()
		})
		.done(function(d) {
			//alert("success!: "+d);
			$("#myModal").modal('hide');
			getListeCouches();
		})
		.error(function() { alert("error!"); });
		e.preventDefault();
	});
}

function addListenersToNewCoucheForm() {
	$("#annulerAjoutCoucheLog").click(function() {
		$("#Couchelogs").html(couchesHTML);
		$("#myModal").modal('hide');
		addListenersToCoucheForm();
	});
	$("#confirmerAjoutCoucheLog").click(function(e) {
		//alert("form data = "+$("form[name='form_couche']").serialize());
		$.ajax({
			type: "POST",
			url: $("#baseURLfield").val()+"/Couchelog/add",
			data: $("form[name='form_couche']").serialize()
		})
		.done(function(d) {
			//alert("success!: "+d);
			$("#myModal").modal('hide');
			getListeCouches();
		})
		.error(function() { alert("error!"); });
		e.preventDefault();
	});
}