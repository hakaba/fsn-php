/**
 * 2016-11-18 : scripts de conversion export vers PDF pour FSN
 */
window.generatePDF = function generatePDF(id) {
    /*
	var svg = $('#map').find('svg').get(0);
	//alert(svg);
	// you should set the format dynamically, write [width, height] instead of 'a4'
	var pdf = new jsPDF('p', 'pt', 'a4');
	svgElementToPdf(svg, pdf, {
		scale: 72/96, // this is the ratio of px to pt units
		removeInvalid: true // this removes elements that could not be translated to pdf from the source svg
	});
	alert(pdf.output('datauri'));
    pdf.save('contours site fouillé.pdf');
    */
    var format = 'a4';
    var pdfExport = new PDFExport(format);
    //var domNode = document.getElementById('content');
    //var domNode = document.getElementById('map');
    var domNode = document.getElementById(id);
    pdfExport.createPageScreenshot(domNode).then(function () {
        pdfExport.downloadDocument();
    });
}
function convertirPDF(id) {
	//var dieseId = '#' + id;
    //var html = $(dieseId).html();
    //var htmlId = '#html' + id;
    //$(htmlId).val(html);
	//alert("Convertir PDF");
    var html = document.getElementById(id).innerHTML;
    html = html.replace(/<br>/g, '<br/>');
    /*
    if (html.substr(0,4) == "<!--") {
    	html = html.slice(html.indexOf("-->") + 3);
    }
    */
    var htmlId = 'html' + id;
    document.getElementById(htmlId).value = html;
    alert(html);
}
