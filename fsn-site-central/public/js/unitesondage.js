/***********************************************
 *  Author: Joseph Kalule                      *
 *  Date: 03/01/2017                           *
 *  Used for: /unitesondage/index/             *
 ***********************************************/

$(document).ready(
		function() {
			setClickableRow();
			//setDeleteButtons();
		}
);

function setClickableRow() { // rendre la ligne TR cliquable et rediriger vers le href correspondant
    $("table .table-clickable-row").each(function () {
        var redirect = $(this).attr('href')
        $(".table-row-data", $(this)).click(function () {
            window.location.href = redirect;
        })
    })
}

function setDeleteButtons() {
	$("span .glyphicon-remove-circle").each(function() {
		var url = $(this).attr('x');
		if (confirm('Veuillez Cliquer sur "OK" pour confirmer la suppression.')) {
			document.location.href = url ;
		}
	})
}

/*
 * cette methode fait la suppression
 * */
function confirmer(url) {  // jfb 2016-04-13
	//alert('hi');
	if (confirm('Veuillez Cliquer sur "OK" pour confirmer la suppression.')) {
		document.location.href = url ;
	}
}