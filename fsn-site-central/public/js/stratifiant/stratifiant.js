/**
 * Stratifiant
 */
      var EnumCalage = {
        CalageTPQ: 1,
        CalageTAQ: 0
      };
      var EnumTypeData = {
        All: 0,
        Certaines: 1
      };
      var EnumAffichageES = {
        Reduits: 0,
        Complets: 1
      };
      var EnumTypeRelation = {
        Anterieur: 0,
        AntetieurIncertaine: 1,
        Synchorne: 2,
        SynchroneIncertaine: 3
      };
      var EnumTypeUS = {
        Couche: 0,
        Negatif: 1,
        Alteration: 2,
        EnsClos: 3
      }
      var comment = { Message: '', Important: true };
      var Unite = {
    	        Col01USGauche: comment,
    	        Col02USident: 0,
    	        Col03UStype: EnumTypeUS.Alteration,
    	        Col04UStpq: null,
    	        Col05UStpqE: null,
    	        Col06UStaqE: null,
    	        Col07UStaq: null,
    	        Col08UScouleur: null,
    	        Col09USPhasDeb: 0,
    	        Col10USPhasFin: 0,
    	        Col11PhDed: '',
    	        Col12USdroite: 0
    	      };

      var Relation = {
    	        Col01RelGauche: comment,
    	        Col02RelNo: 0,
    	        Col03RelUS1: 0,
    	        Col04RelType: EnumTypeRelation.Anterieur,
    	        Col05RelUS2: 0,
    	        Col06RelDroite: ''
    	      };

      var Phase = {
    	        Co01LegendPhase: '',
    	        Co02OrdPhase: '',
    	        Col03TPQ: '',
    	        Col04TPQDeduit: '',
    	        Col05TAQ: '',
    	        Col06TAQdéduit: ''
    	      };
      var Traitement = {
    	        AffichageES: EnumAffichageES,
    	        Calage: EnumCalage,
    	        Comments: [comment],
    	        DemoDebogage: false,
    	        EcartAxe: 0,
    	        EcartLigne: 0,
    	        GraphTPQTAQ: true,
    	        LargeurSommet: 0,
    	        Marge: 0,
    	        MiseEnPhase: true,
    	        PasGraduation: 0,
    	        TAQabsolu: '',
    	        TitreGraphe: '',
    	        TPQabsolu: '',
    	        TracePalier: true,
    	        TraitementActif: true,
    	        TypeDonnee: EnumTypeData,
    	        VisuTAQTPQSaisi: true
    	      };
      var ustraite = {
    	        Col01IdentUS: "CST.100",
    	        Col02IndiceUS: 0,
    	        Col03IndiceES: 0,
    	        Col04Dspr: 0,
    	        Col05Dspa: 2,
    	        Col06TypeUS: "couche (US maitre)",
    	        Col07ESI: 0
    	     , Col08LignDiag: 1
    	     , Col09ColsDiag: "1"
    	     , Col10Tpq: 1850
    	     , Col11TpqIncert: 1850
    	     , Col12TaqIncert: 1850
    	     , Col13Taq: 1850
    	      , Col14PhasDeb: 5
    	     , Col15PhasFin: 5
    	     , Col16PostTracees: null
    	     , Col17PostATracer: null
    	     , Col18AntTracees: 1
    	     , Col19AntATracer: "0"
    	     , Col20Tracee: null
    	     , Col21RangUS: 0
    	     , NonActive: false
    	      };
      var True = true;
      var False = false;

      var dataIn = {
                Traitement: _.assign({}, Traitement, {
                  TypeDonnee: EnumTypeData.All,
                  Calage: EnumCalage.CalageTPQ,
                  TraitementActif: True,
                  GraphTPQTAQ: True,
                  MiseEnPhase: True,
                  TPQabsolu: -10000,
                  TAQabsolu: 2009,
                  AffichageES: EnumAffichageES.Complets,
                  TitreGraphe: "Données Exemple 1",
                  PasGraduation: 50,
                  EcartLigne: 25,
                  EcartAxe: 12,
                  LargeurSommet: 30,
                  Marge: 60
                }),
                Data: {
                  Unites: [],
                  Relations: [],
                  Phases: []
                }
              };

	function lancerStratifiant() {
		//$('[name*="parametres"]').prop('disabled', true);
		$('#headingTraitement').trigger('click');

		var baseUrl =  document.getElementById("baseUrl").value ;
		var configPort =  document.getElementById("configPort").value ;
		$('#outParametre').html('<img src="http://'+window.location.hostname+baseUrl+'/images/loading.gif">');
		//alert('Lancer ! (appel Ajax)');
		//$("#ok").click();
		
		alimDataIn();
		
		// classe utilisée pour transmettre un fichier en ajax
		var form = new FormData();
		form.append("donnees",JSON.stringify(dataIn));

		var url = 'http://'+window.location.hostname+baseUrl+'/Stratifiant/ajaxStratifiant';
		//var url = $("#serviceurl").val();
		//var sitefouilleId =  document.getElementById("sitefouilleId").value ;
		//var url = 'http://'+window.location.hostname+baseUrl+'/Stratifiant/ajaxTest';
		//var url = 'http://'+window.location.hostname+':'+configPort+'/export/stratifiant-json/'+sitefouilleId;
		//var url = 'http://dev.sogeti.fichestrati.fr:8084/export/stratifiant-json/40ef22e7-2a14-f8e3-52cc-4a20617d5196'; 
		//var url = "<?php echo $this->getRequest('Stratifiant','ajaxStratifiant' ) ; ?>";
        //alert(url);
        //alert('************** dataIn : ');
        //alert(JSON.stringify(dataIn));

		$.ajax({
          async: false, // synchrone
          //async: true, // asynchrone
          url: url,
          //data: JSON.stringify(dataIn),
          //data: "donnees=" + JSON.stringify(dataIn),
          data: form,
          processData : false,
          type: "POST",
          contentType : false,
          //crossDomain: true,
          //dataType: 'jsonp',
          //dataType: 'json',
          //jsonp: false,
          //contentType: "application/json;charset=utf-8",
          success: function (data) {
           	//alert('Appel ajax success');
        	var dataP = JSON.parse(data);
            ShowUnite('#outUnite', dataP.Data.Unites);
            ShowRelation('#outRelation', dataP.Data.Relations);
            ShowPhase('#outPhase', dataP.Data.Phases);
            ShowTraitement("#outParametre", dataP.Traitement);
            ShowUsTraitee("#outUsTraite", dataP.UsTraitee.USTraitees);
            ShowJSon("#outTabTrace", dataP.TableauTrace);
            $("#outGraphe").html(dataP.SVGGraphe);
            $("#outGrapheDatation").html(dataP.SVGGrapheDatation);
            init(); // clickSVG.js
            //$('#headingTraitement').trigger('click');
          },
          error: function (jqXHR, textStatus, errorThrown) {
          	//alert(textStatus);
          	//alert(errorThrown);
            $("#outParametre").html("");
            if (jqXHR.responseJSON) {
              ShowJSon("#outParametre", jqXHR.responseJSON);
            }
            else {
              $("#outParametre").html(jqXHR.responseText);
            }
            $('#outUnite').html("");
            $('#outRelation').html("");
            $('#outPhase').html("");
            console.log("1:", jqXHR, "2:", textStatus, "3:", errorThrown);
          }
        }); // fin $.ajax({
	}
	
	function alimDataIn() {
		if ($('#surdatesplancher').prop('checked'))
			dataIn.Traitement.Calage = EnumCalage.CalageTPQ;
		else
			dataIn.Traitement.Calage = EnumCalage.CalageTAQ;
		
		dataIn.Traitement.EcartAxe = $('#ecartentreaxes').val();
		dataIn.Traitement.EcartLigne = $('#ecartentrelignes').val();
		dataIn.Traitement.LargeurSommet = $('#largeurcadresUS').val();
		dataIn.Traitement.Marge = $('#marge').val();
		dataIn.Traitement.PasGraduation = $('#intervallegraduation').val();
		dataIn.Traitement.TPQabsolu = $('#bornehaute').val();
		dataIn.Traitement.TAQabsolu = $('#bornebasse').val();
		
		dataIn.Traitement.GraphTPQTAQ = $('#graphiqueintervalles').prop('checked');
		dataIn.Traitement.MiseEnPhase = $('#miseenphases').prop('checked');
		dataIn.Traitement.TracePalier = $('#tracepaliers').prop('checked');
		dataIn.Traitement.VisuTAQTPQSaisi = $('#indicationTPQTAQsaisis').prop('checked');
		
		dataIn.Traitement.TitreGraphe = $('#titregraphe').val();
		
		if ($('#traitementdonneesincertaines').prop('checked'))
			dataIn.Traitement.TypeDonnee = EnumTypeData.All;
		else
			dataIn.Traitement.TypeDonnee = EnumTypeData.Certaines;
	}
	
	function recommencerStratifiant() {
		if (confirm('Veuillez Cliquer sur "OK" pour confirmer l\'annulation de votre saisie.')) {
	    	$('#form_selection').trigger('reset');
	    	$('[name*="parametres"]').prop('disabled', false);
		}
	}
	
    function retourStratifiant(url) {
		if (confirm('Veuillez Cliquer sur "OK" pour confirmer l\'abandon de votre saisie.')) {
			document.location.href = url;
		}
    }

    $("#form_selection").on('submit', function(e) {
	    e.preventDefault(); // empêcher le comportement par défaut du navigateur sur le formulaire
	    
		// classe utilisée pour transmettre un fichier en ajax
		var form = new FormData();

    });
    
  	$(function() {

    	//$('#onglets').tabs();
		
		$('#traitementdonneesincertaines').change(function() {
			if ($('#traitementdonneesincertaines').prop('checked')) {
				$('#miseenphases').prop('disabled', false);
			}
			else {
				$('#miseenphases').prop('checked', false);
				$('#miseenphases').prop('disabled', true);
				$('#auplusancien').prop('disabled', true);
				$('#auplusrecent').prop('disabled', true);
			}
		});
		
		$('#miseenphases').change(function() {
			var topChecked = $('#miseenphases').prop('checked');
			$('#auplusancien').prop('disabled', !(topChecked));
			$('#auplusrecent').prop('disabled', !(topChecked));
		});
		
		$('#auplusancien').change(function() {
			var topChecked = $('#auplusancien').prop('checked');
			$('#surdatesplancher').prop('checked', topChecked);
		});
		
		$('#auplusrecent').change(function() {
			var topChecked = $('#auplusrecent').prop('checked');
			$('#surdatesplafond').prop('checked', topChecked);
		});
		
		$('#surdatesplancher').change(function() {
			var topChecked = $('#surdatesplancher').prop('checked');
			$('#auplusancien').prop('checked', topChecked);
		});
		
		$('#surdatesplafond').change(function() {
			var topChecked = $('#surdatesplafond').prop('checked');
			$('#auplusrecent').prop('checked', topChecked);
		});
		
		$('#traitementelementsdatation').change(function() {
			var topChecked = $('#traitementelementsdatation').prop('checked');
			$('#bornehaute').prop('disabled', !(topChecked));
			$('#bornebasse').prop('disabled', !(topChecked));
			$('#surdatesplancher').prop('disabled', !(topChecked));
			$('#surdatesplafond').prop('disabled', !(topChecked));
			$('#tracepaliers').prop('disabled', !(topChecked));
			$('#graphiqueintervalles').prop('disabled', !(topChecked));
			if ($('#graphiqueintervalles').prop('checked')) {
				$('#indicationTPQTAQsaisis').prop('disabled', !(topChecked));
				$('#intervallegraduation').prop('disabled', !(topChecked));
			}
		});

		$('#graphiqueintervalles').change(function() {
			var topChecked = $('#graphiqueintervalles').prop('checked');
			$('#indicationTPQTAQsaisis').prop('disabled', !(topChecked));
			$('#intervallegraduation').prop('disabled', !(topChecked));
		});

	}); // fin $(function() {

    // Adaptation du script de http://stratifiant.fichestrati.fr/StratifiantService/TestService.html début
    // Prérequis de charger le script http://stratifiant.fichestrati.fr/StratifiantService/Scripts/lodash.js (évolution de underscore.js)

      	function ShowUnite(idDiv, data) {
          var Thead = "";

          _.forEach(Unite, function (val, key) {
            Thead += '<th>' + key + '</th>';
          });
          var TBody = "";
          var Col03UStype = '';
          _.forEach(EnumTypeUS, function (val, key) {
            Col03UStype += '<option value="' + val + '">' + key + '</option>';
          });
          var Col03UStypeSelect = '<select readonly>' + Col03UStype + '</select>';

          _.forEach(data, function (uni) {
            var td = '';
            _.forEach(uni, function (val, key) {
              if (val == null) {
                td += '<td></td>';
              }
              else if (key == 'Col03UStype') {
                var value = $(Col03UStypeSelect).find("[value=" + val + "]");
                td += '<td>' + value.text() + '</td>';

              }
              else if (key == 'Col01USGauche') {

                var value = val.Message;
                if (value == null) {
                  td += '<td ></td>';
                } else {
                  td += '<td >' + val.Message + '</td>';
                }
              }
              else {
                td += '<td>' + val + '</td>';
              }
            });
            TBody += '<tr>' + td + '</tr>';
          });
          $(idDiv).html('<table ><thead><tr>' + Thead + '</tr></thead><tbody>' + TBody + '</tbody></table>');
        }
        function ShowRelation(idDiv, data) {
          var Thead = "";

          _.forEach(Relation, function (val, key) {
            Thead += '<th>' + key + '</th>';
          });
          var TBody = "";
          var Col03UStype = '';
          _.forEach(EnumTypeRelation, function (val, key) {
            Col03UStype += '<option value="' + val + '">' + key + '</option>';
          });
          var Col03UStypeSelect = '<select readonly>' + Col03UStype + '</select>';

          _.forEach(data, function (uni) {
            var td = '';
            _.forEach(uni, function (val, key) {
              if (val == null) {
                td += '<td></td>';
              }
              else if (key == 'Col04RelType') {
                var value = $(Col03UStypeSelect).find("[value=" + val + "]");
                td += '<td>' + value.text() + '</td>';

              }
              else if (key == 'Col01RelGauche') {

                var value = val.Message;
                if (value == null) {
                  td += '<td ></td>';
                } else {
                  td += '<td >' + val.Message + '</td>';
                }
              }
              else {
                td += '<td>' + val + '</td>';
              }
            });
            TBody += '<tr>' + td + '</tr>';
          });
          $(idDiv).html('<table ><thead><tr>' + Thead + '</tr></thead><tbody>' + TBody + '</tbody></table>');
        }
        function ShowPhase(idDiv, data) {
          var Thead = "";

          _.forEach(Phase, function (val, key) {
            Thead += '<th>' + key + '</th>';
          });
          var TBody = "";

          _.forEach(data, function (uni) {
            var td = '';
            _.forEach(uni, function (val, key) {
              if (val == null) {
                td += '<td></td>';
              }
              else {
                td += '<td>' + val + '</td>';
              }
            });
            TBody += '<tr>' + td + '</tr>';
          });
          $(idDiv).html('<table ><thead><tr>' + Thead + '</tr></thead><tbody>' + TBody + '</tbody></table>');
        }
        function ShowUsTraitee(idDiv, data) {
          var Thead = "";

          _.forEach(ustraite, function (val, key) {
            Thead += '<th>' + key + '</th>';
          });
          var TBody = "";

          _.forEach(data, function (uni) {
            var td = '';
            _.forEach(uni, function (val, key) {
              if (val == null) {
                td += '<td></td>';
              }
              else {
                td += '<td>' + val + '</td>';
              }
            });
            TBody += '<tr>' + td + '</tr>';
          });
          $(idDiv).html('<table ><thead><tr>' + Thead + '</tr></thead><tbody>' + TBody + '</tbody></table>');
        }
        function ShowTraitement(idDiv, data) {
          var Thead = '<td>Parametre</td><td>Valeur</td>';
          var TBody = '';
          _.forEach(data, function (val, key) {
            if (key != "Comments") { TBody += '<td>' + key + '</td>'; }
            var value = val;
            if (key == "TypeDonnee") {
              _.forEach(EnumTypeData, function (valu, keyu) {
                if (valu == val) {
                  value = keyu;
                }
              });
            } else if (key == "Comments") {

            }
            else if (key == "AffichageES") {
              _.forEach(EnumAffichageES, function (valu, keyu) {
                if (valu == val) {
                  value = keyu;
                }
              });
            }
            else if (key == "Calage") {
              _.forEach(EnumCalage, function (valu, keyu) {
                if (valu == val) {
                  value = keyu;
                }
              });
            }
            if (key != "Comments") {
              TBody += '<td>' + value + '</td>';
              TBody = '<tr>' + TBody + '</tr>';
            }
          });

          var Comments = '';
          _.forEach(data.Comments, function (valu) {
            Comments += valu.Message + '</br>';
          });
          $(idDiv).html('<table class="traitement"><thead><tr>' + Thead + '</tr></thead><tbody>' + TBody + '</tbody></table><div class="comments">' + Comments + '</div>');
        }

        function Transform(data, TBody) {
          _.forEach(data, function (val, key) {
            TBody += '<td>' + key + '</td>';
            var value = val;
            if (typeof val === 'object') {
              value = "";
              value = buildhtml(val, value);
            }
          
            TBody += '<td>' + value + '</td>';
            TBody = '<tr>' + TBody + '</tr>';

            TBody = '<tr>' + TBody + '</tr>';
          });
          return TBody;
        }
        function buildhtml(data, html) {
          if ($.isArray(data)) {
            if (data.length > 0) {
              var Thead = "";  
              _.forEach(data[0], function (val, key) {
                Thead += '<th>' + key + '</th>';
              });
              _.forEach(data, function (uni) {
                var td = '';
                _.forEach(uni, function (val, key) {
                  if (val == null) {
                    td += '<td></td>';
                  }
                  else {
                    td += '<td>' + val + '</td>';
                  }
                });
                TBody += '<tr>' + td + '</tr>';
              });
              html += '<table ><thead><tr>' + Thead + '</tr></thead><tbody>' + TBody + '</tbody></table>';
            }
          }
          else {
            var Thead = '<td>Parametre</td><td>Valeur</td>';
            var TBody = '';
            TBody = Transform(data, TBody);
            html += '<table class="traitement"><thead><tr>' + Thead + '</tr></thead><tbody>' + TBody + '</tbody></table>';
          }
          return html;
        }
        function ShowJSon(idDiv, data) {
          var html = '';
            
          html = buildhtml(data, html);
          $(idDiv).html(html);
        }
		
        $(function () {

          var jsonloader = $("#Json");
          jsonloader.val(JSON.stringify(dataIn));

        }); // fin $(function () {
    
    // Adaptation du script de http://stratifiant.fichestrati.fr/StratifiantService/TestService.html fin
