/**
 * jfb 2016-06 mantis 318
 */
function siteFouilleExistant(sitefouille,url,message) { // jfb 2016-06 mantis 262 et 318
		if (sitefouille) {
			document.location.href = url;
		}
		else {
			if (!message) message = "Veuillez créer ou sélectionner un site de fouille !";
			alert(message);
		}
}