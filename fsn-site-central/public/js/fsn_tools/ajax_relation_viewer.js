var d_relation_viewer
$(function(){
    $("body").append("<div id='dialog-relation-viewer' title='Détail de la Relation'></div>")
    d_relation_viewer = $("#dialog-relation-viewer")
    d_relation_viewer.dialog({
        autoOpen: false,
        height: 300,
        width: 350,
        modal: true,
    });
    $(".table-relation-row").click(function(){
        if(!$(this).attr("ajax"))
            return;
        var row_datas = {}
        $("input[type='hidden']",$(this)).each(function(){
            row_datas[$(this).attr("champs")] = $(this).val();
        })
        $.ajax({
            type:"POST",
            url: $(this).attr("ajax"),
            data: row_datas,
            success: function(returnData) {
                d_relation_viewer.html(returnData);
                d_relation_viewer.dialog( "open" );
            },
            error: function(returnData) {
                alert('Une erreur est survenue.');
            }
        });
    })
})
