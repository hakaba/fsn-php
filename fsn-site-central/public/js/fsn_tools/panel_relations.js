/**
 * Created by Robin COLLAS on 17/12/2015.
 */
var nbadd = 0;
if (typeof txtSaveButton == 'undefined')
    var txtSaveButton = "Save";
if (typeof txtCancelButton == 'undefined')
    var txtCancelButton = "Cancel";

if (typeof txtErrorForm == 'undefined')
    var txtErrorForm = "Une ou plusieurs erreurs ont été détectées";
var trueTable = "<span class='glyphicon glyphicon-ok'></span>";

$.fn.extend({
    isValid: function (new_function) {
        if(jQuery.isFunction(new_function))
            $(this).data("validation",new_function)
        else{
            if(typeof $(this).data("validation") === 'function')
                return $.proxy($(this).data("validation"), $(this))() || false;
            else
                return true;
        }
    }
});

$(function(){

    // ajout des boutons du formulaire et initialisation du tableau de datas
    $(".panel-form").each(function(){
        var panel_form = $(this);
        $(".panel-validation .submit-panel", panel_form)
            .append("<a href='#' class='save-button' title='"+txtSaveButton+"'>"
                +"<span class='glyphicon glyphicon-plus-sign' style='font-size: 35px; margin-right:5px;'></span>"
                +"</a>")
            .append("<a href='#' class='cancel-button' title='"+txtCancelButton+"'>"
                +"<span class='glyphicon glyphicon-exclamation-sign' style='font-size: 35px; margin-right:5px;'></span>"
                +"</a>")

        $(".cancel-button",panel_form).click(function(event){
            event.preventDefault();
            clearimputs(panel_form);
            $(".save-button",$(this).parent()).removeAttr('edit')
            $(".save-button .glyphicon-ok-sign",$(this).parent()).addClass('glyphicon-plus-sign').removeClass('glyphicon-ok-sign');
            $(this).hide();
        }).hide();
        panel_form.data('content',{});
    })

    // mise en place de la fonction du bouton de validation
    $(".panel-form .panel-validation .submit-panel .save-button").click(function(event){
        event.preventDefault();
        var panel_form = $(this);
        while(!panel_form.hasClass('panel-form')) {
            panel_form = panel_form.parent();
        }
        var errors = {};
        var valid = false;
        $(".panel-form-content .field input,textarea,select",panel_form).each(function() {
            valid = $(this).isValid();
            if(!valid || typeof valid === 'string')
                errors[$(this).attr('champs')] = valid;
        })
        if(Object.keys(errors).length > 0){
            var errorMessage = txtErrorForm;
            for (var key in errors){
                if(typeof errors[key] === 'string')
                    errorMessage += "\n\n- "+errors[key]
            }
            alert(errorMessage);
            return;
        }
        if ($(this).attr('edit')) {
            saverow(panel_form);
            $(this).removeAttr('edit')
            $(".glyphicon-ok-sign",$(this)).addClass('glyphicon-plus-sign').removeClass('glyphicon-ok-sign')
            $(".cancel-button",$(this).parent()).hide()
        }
        else
            addrow(panel_form);
    })

    // initialisation des lignes des tableaux
    $(".panel-form").each(function(){
        var panel = $(this)
        var datas = panel.data('content')
        $(".table-panel .table-relation-row",panel).each(function(){
            var row = $(this);
            // activation des bouton de suppression sur les lignes existantes
            $(".del-row-button",row).click(function(event){
                event.preventDefault();
                delete datas[row.attr('id')]
                delrow(row);
            })
            // activation de la modification au click sur une ligne du tableau
            $(".table-row-data",row ).click(function() {
                editrow(row);
            })
            // initialisation des attributs
            $(this).attr('id',"added-relation"+nbadd)
            $("input",$(this)).each(function(){
                $(this).attr("name",$(this).attr("champs")+panel.attr('name')+nbadd)
                $(this).attr("id",$(this).attr("champs")+nbadd)
            })
            datas[row.attr('id')] = getRowInfos(row)
            nbadd++;
        })
        panel.data('content',datas);
    })

    // fonction de création d'une nouvelle ligne dans le tableau à partir des informations du formulaire
    function addrow(panel_form){
        var datas = panel_form.data('content')
        var new_row = "<tr class='table-relation-row' id='added-relation"+nbadd+"'>";
        new_row = new_row + generateContentRow(panel_form)
        new_row = new_row+"</tr>";
        $(".table-panel tbody",panel_form).append(new_row);
        var row = $("#added-relation"+nbadd,panel_form)
        $(".table-row-data",row ).click(function() {
            editrow(row);
        })
        $(".del-row-button",row).click(function(event){
            event.preventDefault();
            delete datas[row.attr('id')]
            delrow(row,event);
        })
        clearimputs(panel_form)
        datas[row.attr('id')] = getRowInfos(row)
        panel_form.data('content',datas)
        nbadd++;
    }

    // fonction de suppression de la ligne du formulaire
    function delrow(row,event){
        row.remove();
    }

    // fonction de renseignement des champs du formaulaire à partir de la ligne
    function editrow(row){
        var panel_form = row;
        while(!panel_form.hasClass('panel-form')) {
            panel_form = panel_form.parent();
        }
        setinputs(panel_form,getRowInfos(row))
        var button = $(".panel-validation .submit-panel a",panel_form)
        $(".glyphicon-plus-sign",button).addClass('glyphicon-ok-sign').removeClass('glyphicon-plus-sign')
        $(".cancel-button",button.parent()).show()
        button.attr('edit',row.attr('id'))
        $("p",button).text(txtEditButton)
    }

    // fonction de sauvegarde des information du formulaire dans la ligne
    function saverow(panel_form){
        var datas = panel_form.data('content')
        var row = $("#"+$(".panel-validation .submit-panel a",panel_form).attr("edit"),panel_form)
        delete datas[$(".panel-validation .submit-panel a",panel_form).attr("edit")]
        row.html(generateContentRow(panel_form))
        row.attr("id","added-relation"+nbadd)
        row = $("#added-relation"+nbadd,panel_form)
        $(".table-row-data",row ).click(function() {
            editrow(row);
        })
        $(".del-row-button",row).click(function(event){
            event.preventDefault();
            delete datas[row.attr('id')]
            delrow(row,event);
        })
        clearimputs(panel_form)
        datas[row.attr('id')] = getRowInfos(row)
        panel_form.data('content',datas)
        nbadd++;
    }


    // ########### fonctions utilitaires ############


    // ajout de la ligne dans le tableau de datas
    function setRowData(row){

    }

    // retourne un tableau associatif des inputs ( id => value )
    function getinputs(panel_form){
        var inputs_values = {};
        $(".panel-form-content .field input,textarea,select",panel_form).each(function() {
            if($(this).attr('type') == 'checkbox')
                inputs_values[$(this).attr('champs')] = $(this).prop('checked')?1:0;
            else if($(this).hasClass('field-key-val'))
                inputs_values[$(this).attr('champs')] = {"key" : ($(this).attr('key'))?$(this).attr('key'):"no_key", "val" : $(this).val()}
            else
                inputs_values[$(this).attr('champs')] = $(this).val();
        })
        return inputs_values;
    }

    // renseigne le formulaire à partir d'un tableau associatif
    function setinputs(panel_form,table){
        $(".panel-form-content .field input,textarea,select",panel_form).each(function() {
            if(table[$(this).attr('champs')]){
                if($(this).attr('type') == 'checkbox')
                    $(this).prop('checked',(table[$(this).attr('champs')] == 1)?1:0)
                else if($(this).hasClass('field-key-val')){
                    $(this).attr('key',table[$(this).attr('champs')]['key'])
                    $(this).val(table[$(this).attr('champs')]['val'])
                }else if($(this).hasClass('datepicker')){
                    $(this).datepicker("update",table[$(this).attr('champs')])
                }
                else
                    $(this).val(table[$(this).attr('champs')])

            }
        })
    }

    // met tous les champs du formulaire à vide
    function clearimputs(panel_form){
        $(".panel-form-content .field input,textarea,select",panel_form).each(function() {
            if($(this).is('select')) {
                $(this).val($("option:first",$(this)).val())
            }
            else if($(this).attr('type') == 'checkbox')
                $(this).prop('checked',false)
            else
                $(this).val("");
            if ($(this).attr('key'))
                $(this).removeAttr('key')
        })
    }

    // création d'un tableau associatif à partir des données d'une ligne
    function getRowInfos(row){
        var infos_values = {};
        $("input",row).each(function(){
            if($(this).attr('val'))
                infos_values[$(this).attr('champs')] = {"key" :  $(this).val(), "val" : $(this).attr('val')};
            else
                infos_values[$(this).attr('champs')] = $(this).val();
        })
        return infos_values
    }

    // génère le contenu d'une ligne en fonction du contenu du formulaire
    function generateContentRow(panel_form){
        var titlePanel = panel_form.attr('name');
        var inputs_values = getinputs(panel_form)
        var contentRow = "";
        $(".table-panel th",panel_form).each(function(){
            contentRow = contentRow + "<td class='table-row-data'>";
            if(inputs_values[$(this).attr('champs')]) {
                if($("#"+$(this).attr('champs')+panel_form.attr('id'),panel_form).is('select')) {
                    contentRow = contentRow + $("#" + $(this).attr('champs')+panel_form.attr('id')+ " option:selected", panel_form).text()
                }
                else if($("#"+$(this).attr('champs')+panel_form.attr('id'),panel_form).attr('type') == 'checkbox')
                    contentRow = contentRow + trueTable;
                else if ($("#"+$(this).attr('champs')+panel_form.attr('id'),panel_form).hasClass('field-key-val'))
                    contentRow = contentRow + $("#"+$(this).attr('champs')+panel_form.attr('id'),panel_form).val()
                else
                    contentRow = contentRow + inputs_values[$(this).attr('champs')];
            }
            if($(this).hasClass('col-action'))
                contentRow = contentRow+"<a href='#' class='del-row-button'><span class='glyphicon glyphicon-remove-circle danger' style='font-size: 18px;' alt='Supprimer' title='Supprimer'></span></a>"
            contentRow = contentRow + "</td>";
        })
        $.each(inputs_values, function(key,value){
            if(value['key'] && value['val'])
                contentRow = contentRow + "<input type='hidden' champs='"+key+"' value='"+value['key']+"' val='"+value['val']+"' name='"+key+""+titlePanel+""+nbadd+"' id='"+key+""+nbadd+"' />"
            else {
                value = new String(value).replace("'", "&apos;").replace("\"", "&quot;")
                contentRow = contentRow + "<input type='hidden' champs='" + key + "' value='" + value + "' name='" + key + "" + titlePanel + "" + nbadd + "' id='" + key + "" + nbadd + "' />"
            }
        })
        return contentRow;
    }
})
