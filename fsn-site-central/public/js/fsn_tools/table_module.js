/**
 * Created by Robin COLLAS on 18/12/2015.
 */

if (typeof sZeroRecords == 'undefined')
    var sZeroRecords = "";
if (typeof sSearch == 'undefined')
    var sSearch = "";
$(document)
    .ready(
    function() {
        var oTableSitefouille;
        setClickableRow();
        oTableSitefouille = $(".table_module")
            .dataTable(
            {
                "bPaginate" : true,
                "bProcessing" : false,
                "bInfo" : false,
                "oLanguage" : {
                    "sZeroRecords" : sZeroRecords,
                    "sSearch" : sSearch
                },
                "sPaginationType" : "full_numbers",
            });
        oSettings = oTableSitefouille.fnSettings();
    });
function setClickableRow() {
    $("table .table-clickable-row").each(function() {
        var redirect = $(this).attr('href')
        $(".table-row-data", $(this)).click(function() {
            window.location.href = redirect;
        })
    })
}