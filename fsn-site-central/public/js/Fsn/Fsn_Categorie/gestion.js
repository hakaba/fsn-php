function ajaxReference(statut, id) {
	var baseUrl =  document.getElementById("baseUrl").value ;
	var xhr = getXMLHttpRequest();
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && xhr.status == 200) {
			selectRef = xhr.responseText;

				document.getElementById('listeRef').innerHTML = selectRef;

			/* On évalue le javascript contenu dans les dom */
			var scripts = document.getElementById('listeRef')
					.getElementsByTagName('script');
			for (var i = 0; i < scripts.length; i++) {
				/*
				 * Sous IE il faut faire un execScript pour que les fonctions
				 * soient définie en globale
				 */
				if (window.execScript) {
					/* On replace les éventuels com' html car IE n'aime pas ça */
					window.execScript(scripts[i].text.replace('<!--', ''));
				}
				/* Sous les autres navigateurs on fait un window.eval */
				else {
					window.eval(scripts[i].text);
				}
			}
		}
	}
	
	if (statut == "liste") {
		xhr.open("POST", baseUrl + "/fsn_categorie/ajaxAfficher", true);
		xhr.setRequestHeader('Content-Type',
				'application/x-www-form-urlencoded');
	} else {
		xhr.open("POST", baseUrl + "/fsn_categorie/ajaxSauver", true);
		xhr.setRequestHeader('Content-Type',
				'application/x-www-form-urlencoded');
	}

	var reference = document.getElementById("reference");
	var nameRef = reference.options[reference.selectedIndex].value;

	if (statut == "liste")
		xhr.send("nameRef=" + nameRef + "&statut=" + statut);
	
	if (statut == "insert") {
		var libelle = document.getElementById("libelleCreer").value;
		var visible = document.getElementById("visibleCreer").checked;
		var ordreMax = document.getElementById("ordreMax").value;
    if (libelle != "") {
      xhr.send("nameRef=" + nameRef + "&libelle=" + libelle + "&visible="
  				+ visible + "&statut=" + statut + "&ordreMax=" + ordreMax);
  		// document.getElementById("libeleCreer").value = '';
  		document.getElementById("visibleCreer").value = '';
    } else {
      alert('Le champ Libellé est vide' ) ;
      
    }
		
	}

	if (statut == "updateLibelle") {
		var libelle = document.getElementById("libelle_" + id).value;

		xhr.send("libelle=" + libelle + "&nameRef=" + nameRef + "&id=" + id
				+ "&statut=" + statut);
	}

	if (statut == "updateVisible") {
		var visible = document.getElementById("visible_" + id).checked;
		var ordreMax = document.getElementById("ordreMax").value;
		xhr.send("visible=" + visible + "&id=" + id + "&statut=" + statut
				+ "&nameRef=" + nameRef + "&ordreMax=" + ordreMax);
	}

	if (statut == "updateOrdre") {

		var ordre = ordreUpdate(id);
		xhr.send("ordre=" + JSON.stringify(ordre) + "&nameRef=" + nameRef
				+ "&id=" + id + "&statut=" + statut);
	}

	if (statut == "delete") {
		if (confirmationSuppression()) {
			xhr.send("nameRef=" + nameRef + "&id=" + id + "&statut=delete");
		}
	}

}

function confirmationSuppression() {
	var answer = confirm("Veuillez confirmer la suppression !")
	if (answer) {
		return true;
	}
}

function affichageTableau() {
	var reference = document.getElementById("reference");
	var nameRef = reference.options[reference.selectedIndex].value;
	if (nameRef != 0) {
		document.getElementById("affichage").style.display = "block";
	} else {
		document.getElementById("affichage").style.display = "none";
	}
}

function confirmationSuppression() {
	var answer = confirm("Veuillez confirmer la suppression !")
	if (answer) {
		return true;
	}
}

function affichageTableau() {
	var reference = document.getElementById("reference");
	var nameRef = reference.options[reference.selectedIndex].value;
	if (nameRef != 0) {
		document.getElementById("affichage").style.display = "block";
	} else {
		document.getElementById("affichage").style.display = "none";
	}
}

function ajoutRelation(vue, action, element_b, element_a, relation_id )
{
	var baseUrl =  document.getElementById("baseUrl").value ;
	var type_relation = '' ;
	var description = '' ;
  var visible = '' ;
  var ordre = new Array() ;
  var reference = '' ;
	var libelle_element_b = document.getElementById("libelle_" + element_b).value ;
  // if ( !relation_id ) { document.getElementById("relation_id_" + element_a).value ; } 
  
	if (vue == "ajaxRelier")
		reference = document.getElementById("selectReference").options[document.getElementById("selectReference").selectedIndex].value;

	if (action == "changerReference") {
    // relation_id = document.getElementById("relation_id_" + element_a).value ;
		reference = document.getElementById("selectReference").options[document.getElementById("selectReference").selectedIndex].value;
  }
		  
	if (action == "updateOrdre") {
    relation_id = document.getElementById("relation_id_" + element_a).value ;
    ordre = ordreRUpdate(element_a) ;
  }
  
  if (action == "ajouter") {
		element_a = document.getElementById("selectListe").value ;
		type_relation = document.getElementById("selectType").value ;
		description = document.getElementById("descriptionR").value ;
		visible = document.getElementById("visibleR").checked ;
	}
	
	if(action == "updateVisible") {
    relation_id = document.getElementById("relation_id_" + element_a).value ;
    visible = document.getElementById("visibleR_" + element_a).checked ;
  }
		
	
	if(action == "updateDescription") {
    relation_id = document.getElementById("relation_id_" + element_a).value ;
    description = document.getElementById("description_" + element_a).value ;
  }
	
    
  $.ajax({
		type:"POST",
		url: baseUrl + "/fsn_categorie/ajaxRelier",
		data:"element_b=" + element_b + "&reference=" + reference + "&ordre=" + JSON.stringify(ordre)
		+ "&action=" + action + "&element_a=" + element_a + "&type_relation=" + type_relation
		+"&visible=" + visible
		+ "&description=" + description
    + "&relation_id=" + relation_id,    
		success: function(returnData) {
  			// Affichage du modal (boite de dialogue)
  			$('#modalTitle').html('Relation catégorie : <b>' + libelle_element_b + '</b>');
  			$('#modalBody').html(returnData);
  			$('#myModal').modal();
  			if(data.error)
    				alert(data.error);
 		},
 		error: function(returnData) {
  			alert('Une erreur est survenue.'); 
 		}
	});
}

function ordreUpdate(id) {
	var ordreA = document.getElementById("ordreA_" + id).value;
	var ordreN = document.getElementById("ordreN_" + id).value;

	var select = document.getElementById("tableReference")
			.getElementsByTagName("select");
	var liste = new Object();

	if (ordreN < ordreA) {
		for (var i = 0; i < select.length; i++)
			if (parseInt(select[i].value) >= ordreN
					&& parseInt(select[i].value) < ordreA
					&& select[i].id.substring(7) != id)
				liste["" + select[i].id.substring(7)] = parseInt(select[i].value) + 1;
	} else if (ordreN > ordreA) {
		for (var i = 0; i < select.length; i++)
			if (parseInt(select[i].value) <= ordreN
					&& parseInt(select[i].value) > ordreA
					&& select[i].id.substring(7) != id)
				liste["" + select[i].id.substring(7)] = parseInt(select[i].value) - 1;
	}
	liste[id] = ordreN;
	return liste;
}

function ordreRUpdate(id) {
	var ordreA = document.getElementById("ordreRA_" + id).value;
	var ordreN = document.getElementById("ordreRN_" + id).value;

	var select = document.getElementById("table_modification_ref")
			.getElementsByTagName("select");
	var liste = new Object();

	if (ordreN < ordreA) {
		for (var i = 0; i < select.length; i++)
			if (parseInt(select[i].value) >= ordreN
					&& parseInt(select[i].value) < ordreA
					&& select[i].id.substring(8) != id)
				liste["" + select[i].id.substring(8)] = parseInt(select[i].value) + 1;
	} else if (ordreN > ordreA) {
		for (var i = 0; i < select.length; i++)
			if (parseInt(select[i].value) <= ordreN
					&& parseInt(select[i].value) > ordreA
					&& select[i].id.substring(8) != id)
				liste["" + select[i].id.substring(8)] = parseInt(select[i].value) - 1;
	}
	liste[id] = ordreN;
	return liste;
}

function afficherDescription(element_a) {
	if (document.getElementById("tr_" + element_a).style.display == "none") {
		document.getElementById("tr_" + element_a).style.display = "table-row" ;
		document.getElementById("btn_description_" + element_a).className = "glyphicon glyphicon-minus-sign" ;
	} else {
		document.getElementById("tr_" + element_a).style.display = "none" ;
		document.getElementById("btn_description_" + element_a).className = "glyphicon glyphicon-plus-sign" ;
	}
}
