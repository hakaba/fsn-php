$(function() {
	//alert("ready!");
	$('form[name="form_unitesondage"]').submit(function(e) {
		takeCareOfUsType(e);
	});
	hideNumeroTrancheeSection();
	addTypeSelectListener();
});

function takeCareOfUsType(e) {
	if($("#type_unitesondage option:selected").text() == "Tranchée") {
		$("#numero_tranchee").val(null);
	} else {
		// type_unitesondage = fenetre
		if($("#numero_tranchee").val() == null || $("#numero_tranchee").val().length()<30) {
			e.preventDefault();
			alert("Impossible de creer une fenetre sans tranchee !");
		}
	}
}

function hideNumeroTrancheeSection() {
	$("#Fnumerotranchee").css("display", "none");
}

function showNumeroTrancheeSection() {
	$("#Fnumerotranchee").css("display", "block");
}

function addTypeSelectListener() {
	$("#type_unitesondage").change(function() {
		if($("#type_unitesondage option:selected").text() == "Tranchée") {
			hideNumeroTrancheeSection();
		} else {
			showNumeroTrancheeSection();
		}
	});
}