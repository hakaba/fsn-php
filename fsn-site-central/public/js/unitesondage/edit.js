/***********************************************
 *  Author: Joseph Kalule                      *
 *  Date: 04/01/2017                           *
 *  Used for: /unitesondage/edit/              *
 ***********************************************/

$(function() {
	getLogsList();
	getTronconsList();
});

function addListenersToLogForm() {
	$("#ajouterLogAUS").click(function() {
		getNewLogForm();
	});
}

function addListenersToTronconForm() {
	$("#ajouterTronconAUS").click(function() {
		getNewTronconForm();
	});
}

function addListenersToNewLogForm() {
	$("#annulerAjoutLog").click(function() {
		$("#Logs").html(logsHTML);
		$("#myModal").modal('hide');
		addListenersToLogForm();
	});
	$("#confirmerAjoutLog").click(function(e) {
		//alert("form data = "+$("form[name='form_log']").serialize()+" baseURL = "+$("#baseURLfield").val());
		$.ajax({
			type: "POST",
			url: $("#baseURLfield").val()+"/Log/add",
			data: $("form[name='form_log']").serialize()
		})
		.done(function(d) {
			//alert("success!: "+d);
			$("#myModal").modal('hide');
			getLogsList();
		})
		.error(function() { alert("error!"); });
		e.preventDefault();
	});
}

function addListenersToNewTronconForm() {
	$("#annulerAjoutTroncon").click(function() {
		$("#Troncons").html(tronconsHTML);
		$("#myModal").modal('hide');
		addListenersToTronconForm();
	});
	$("#confirmerAjoutTroncon").click(function(e) {
		//alert("form data = "+$("form[name='form_log']").serialize()+" baseURL = "+$("#baseURLfield").val());
		$.ajax({
			type: "POST",
			url: $("#baseURLfield").val()+"/Troncon/add",
			data: $("form[name='form_troncon']").serialize()
		})
		.done(function(d) {
			//alert("success!: "+d);
			$("#myModal").modal('hide');
			getTronconsList();
		})
		.error(function() { alert("error!"); });
		e.preventDefault();
	});
}

function getLogsList() {
	$.ajax({
		url: $("#baseURLfield").val()+"/Log/index",
		type: "POST",
		data: "unitesondage_id="+$("#id_usondage").val()
	}).done(function(response) {
		$("#Logs").html(response);
		addListenersToLogForm();
	});
}

function getTronconsList() {
	$.ajax({
		url: $("#baseURLfield").val()+"/Troncon/index",
		type: "POST",
		data: "unitesondage_id="+$("#id_usondage").val()
	}).done(function(response) {
		$("#Troncons").html(response);
		addListenersToTronconForm();
	});
}

var logsHTML;

function getNewLogForm() {
	logsHTML = $("#Logs").html();
	$.ajax({
		url: $("#baseURLfield").val()+"/Log/add",
		type: "POST",
		data: "unitesondage_id="+$("#id_usondage").val()
	}).done(function(response) {
		$("#modalBody").html(response);
		$("#modalTitle").text("Ajout Log");
		$("#myModal").modal();
		addListenersToNewLogForm();
	});
}

var tronconsHTML;

function getNewTronconForm() {
	tronconsHTML = $("#Troncons").html();
	$.ajax({
		url: $("#baseURLfield").val()+"/Troncon/add",
		type: "POST",
		data: "unitesondage_id="+$("#id_usondage").val()
	}).done(function(response) {
		$("#modalBody").html(response);
		$("#modalTitle").text("Ajout Tronçon");
		$("#myModal").modal();
		addListenersToNewTronconForm();
	});
}


/*
 * cette methode fait la suppression
 * */
function confirmer(url) {  // jfb 2016-04-13
	//alert('hi');
	if (confirm('Veuillez Cliquer sur "OK" pour confirmer la suppression.')) {
		//document.location.href = url;
		$.ajax({
			type: "POST", url: url
		})
		.done(function() {
			getLogsList();
		});
	}
}

/*
 * cette methode fait la suppression de troncons
 * */
function confirmerT(url) {  // jfb 2016-04-13
	//alert('hi');
	if (confirm('Veuillez Cliquer sur "OK" pour confirmer la suppression.')) {
		//document.location.href = url;
		$.ajax({
			type: "POST", url: url
		})
		.done(function() {
			getTronconsList();
		});
	}
}